	/*
	*	AMX Mod X Scipt
	*	| Author  : Niko
	*	| Plugin  : Game Adjuster
	*	| Version : 1.0
	*
	*
	*	[ Force Storm Game Rules Adjuster ]
	*	The amxx plugin is to adjust Force Storm's game rules in a convienient way 
	*	But the more significant purpose,however, is to tell the AMXX programmer how to write a plugin for Force Storm
	*	All the codes you see here is in a very standard format
	*	You can try to write your own plugin like this
	*	by using the "MessageSender.inc" and "MetaHook_Msg.h"
	*	PS: the "cdll_dll.h" is from the mp.dll source
	*
	*				Niko 2014.8.21
	*
	*/


	#include <amxmodx>
	#include <fakemeta>
	#include "util.sma"

	#define PLUGIN_NAME			"Force Storm Adjuster"
	#define PLUGIN_VERSION		"0.1"
	#define PLUGIN_AUTHOR		"Niko"


	// ---------------- Globals --------------------
	
	new gmsgMetaHook = 0;
	new gmsgCurWeapon = 0;
	new gmsgHideWeapon = 0;

	enum (+= 1000)
	{
		TASK_NONE = 1000,
		TASK_TIMER,
	};

	new g_fwSpawn;

	new const g_szClassNeedRemove[][] = 
		{
			"func_bomb_target",
			"info_bomb_target",
			"info_vip_start",
			"func_vip_safetyzone",
			"func_escapezone",
			"hostage_entity",
			"monster_scientist",
			"func_hostage_rescue",
			"info_hostage_rescue",
			"item_longjump",
			"func_vehicle"
		};

	new g_iTimeAmount = 0;
	
	new g_fwTimerEnd = 0;
	
	
	
	
	
	
		// ------------- Plugin funcs ----------------------
		
		
		public plugin_precache()
		{
		
			g_fwSpawn = register_forward(FM_Spawn, "fw_Spawn");
			
		}
		
		

		public plugin_init()
		{
		
			register_plugin( PLUGIN_NAME, PLUGIN_VERSION, PLUGIN_AUTHOR);
			
			gmsgMetaHook = get_user_msgid( "MetaHook");
			gmsgCurWeapon = get_user_msgid( "CurWeapon");
			gmsgHideWeapon = get_user_msgid( "HideWeapon" );
			
			g_fwTimerEnd = CreateMultiForward("Fs_TimerEnd", ET_IGNORE );
			
			unregister_forward( FM_Spawn, g_fwSpawn);
			
			server_cmd("bind ^"1^" ^"slot1^"");
			server_cmd("bind ^"2^" ^"slot2^"");
			server_cmd("bind ^"3^" ^"slot3^"");
			server_cmd("bind ^"4^" ^"slot4^"");
			server_cmd("bind ^"5^" ^"slot5^"");
			server_cmd("bind ^"6^" ^"slot6^"");
			server_cmd("bind ^"7^" ^"slot7^"");
			server_cmd("bind ^"8^" ^"slot8^"");
			server_cmd("bind ^"9^" ^"slot9^"");
			server_cmd("bind ^"0^" ^"slot10^"");
			server_cmd("bind ^"b^" ^"bag^"");
			server_cmd("bind ^"y^" ^"chat^"");
			server_cmd("bind ^"u^" ^"chat_team^"");
			server_cmd("bind ^"o^" ^"bag_editor^"");
			server_cmd("bind ^"p^" ^"camera_menu^"");
			server_cmd("bind ^"MWHEELDOWN^" ^"fov_down^"");
			server_cmd("bind ^"MWHEELUP^" ^"fov_up^"");
			
			register_event("HLTV", "event_NewRound", "a", "1=0", "2=0");
			
		}
		
		
		
		
		// ---------------- EngFunc-----------
		
		
		public client_putinserver( id )
		{
		
			//server_cmd( "sv_restart 3" );
			
		}
		
		
		

		// ---------------- Event --------------
		
		
		public event_NewRound()
		{
		
			delete_task( TASK_TIMER );
			g_iTimeAmount = get_cvar_num("mp_roundtime") * 60 + get_cvar_num("mp_freezetime");
			Cl_UpdateClientData( 0, MH_MSG_UPDATE_TIME, g_iTimeAmount );
			set_task( 1.0, "Task_TimerReset", TASK_TIMER, _, _, "b");
			
			server_cmd("exec listenserver.cfg");
			
			for( new id = 1; id < 33; id++ )
			{
				if( !is_user_connected( id ) || is_user_bot( id))
					continue;
					
				message_begin( MSG_ONE, gmsgHideWeapon, _, id)
				write_byte( HIDEHUD_CROSSHAIR );
				message_end();
			}
			
		}
		
		
		

		// --------------- Forward -------------
		
		
		public fw_Spawn( iEnt )
		{
		
			if (!pev_valid(iEnt) )
				return FMRES_IGNORED;
			
			new szClassname[32];
			pev(iEnt, pev_classname, szClassname, charsmax(szClassname));
			
			for( new i = 0; i<sizeof( g_szClassNeedRemove ) ; i++ )
			{
				if( !strcmp( g_szClassNeedRemove[i], szClassname ) )
				{
					REMOVE_ENTITY( iEnt );
				}
			}
			
			return FMRES_IGNORED;
			
		}

		
		
		
		
		// ----------- Task ----------------
		
		
		public Task_TimerReset( iTaskid )
		{
		
			g_iTimeAmount --;
			
			if( !g_iTimeAmount )
			{
				new iFwDummyResult;
				ExecuteForward( g_fwTimerEnd, iFwDummyResult );
				server_cmd( "sv_restart 5" );
				
				Cl_UpdateClientData( 0, MH_MSG_UPDATE_TIME,  0);
				
			}
			else if( !(g_iTimeAmount % 5) )
			{
				// You needn't update the client timer per second by sending the message
				// Because the client is able to update its timer itself
				// What you need to is to recorrect the client's timer about per 5 seconds
				// The purpose is saving the server 's resources
				
				Cl_UpdateClientData( 0, MH_MSG_UPDATE_TIME, g_iTimeAmount);
			}
			
		}


	/* Notepad ++ */
	