
	/*
	*	AMX Mod X Scipt
	*		| Author  : NoName
	*		| Plugin  : Ghost Mode
	*		| Version : 0.1
	*		| Modify  : Niko
	*
	*
	*/
	
	
	#include <amxmodx>
	#include <fakemeta>
	#include <hamsandwich>
	#include "util.sma"
	

	#define PLUGIN_NAME			"Ghost Mode"
	#define PLUGIN_VERSION		"0.2"
	#define PLUGIN_AUTHOR		"NoName"

	#define P_MDL_ENT			"wpn_p_model"
	#define SOUND_BREATHE		"player/breathe2.wav"
	#define GHOST_SPEED			270.0
	
	
	
	new g_iTeam[33];
	new g_iAlpha[33];
	
	new Float:g_flHideStart[33];
	new Float:g_flAlphaModifier[33];
	new Float:g_flNextBreathe[33];
	
	
	new gmsgStatusText = 0;
	

	
	
		public plugin_precache()
		{
			precache_sound( SOUND_BREATHE );
		}
		
		public plugin_init()
		{
			register_plugin ( PLUGIN_NAME,  PLUGIN_VERSION,  PLUGIN_AUTHOR );
			
			RegisterHam( Ham_AddPlayerItem, "player", "HAM_AddPlayerItem");
			RegisterHam( Ham_Spawn, "player", "HAM_Player_Spawn");
			RegisterHam( Ham_Spawn, "player", "HAM_Player_Spawn_Post", 1);
			RegisterHam( Ham_Killed, "player", "HAM_Player_Killed_Post", 1);
			RegisterHam( Ham_Touch, "weaponbox", "HAM_WeaponBox_Touch");
			
			register_forward(FM_PlayerPostThink, "FW_PlayerPostThink", 1);
			register_forward(FM_AddToFullPack, "FW_AddToFullPack", 1);
			
			gmsgStatusText = get_user_msgid("StatusText");
			
			register_message(gmsgStatusText, "message_StatusText");
			
		}
		
		
		
		public client_putinserver( id )
		{
			Cl_SendGameMode( id, GAME_MODE_GHOST );
			
			static iFirstTime = TRUE;
			
			if( iFirstTime )
			{
				server_cmd( "sv_restart 3" );
				iFirstTime = FALSE;
			}
		}
		
		
		
		public message_StatusText( msg_id, msg_dest, msg_entity)
		{
			if ( g_iTeam[msg_entity] == TEAM_CT)
			{
				new sBarString[32];
				get_msg_arg_string(2, sBarString, 32);
				
				if (equal(sBarString, "1 %c1: %p2"))
					set_msg_arg_string(2, "");
			}
		}
		
		public FW_AddToFullPack(es_handle, e, ent, host, hostflags, player, pSet)
		{
			if (ent > 32 || !ent)
				return FMRES_IGNORED;
			
			if (g_iTeam[ent] == TEAM_TERRORIST && is_user_alive(ent))
			{
				set_es(es_handle, ES_RenderMode, kRenderTransAlpha);
				set_es(es_handle, ES_RenderAmt, floatround(g_iAlpha[ent] * g_flAlphaModifier[ent]));
				set_es(es_handle, ES_RenderFx, kRenderFxGlowShell);
			}
			
			return FMRES_IGNORED;
			
		}
		
		public FW_PlayerPostThink(id)
		{
		
			if (pev(id, pev_deadflag) != DEAD_NO)
				return FMRES_IGNORED;
			
			if (g_iTeam[id] == TEAM_TERRORIST)
			{
				set_pev(id, pev_maxspeed, GHOST_SPEED);
				
				new iButton = pev(id, pev_button);
				if(iButton & (IN_BACK | IN_FORWARD | IN_RUN | IN_MOVELEFT | IN_MOVERIGHT | IN_LEFT | IN_RIGHT))
				{
					new Float:flVelocity2D = GetVelocity2D(id);
					flVelocity2D = flVelocity2D > 250.0 ? 250.0 : flVelocity2D;
				
					new Float:amount;
					if (flVelocity2D > 140.0)
						amount = (flVelocity2D - 140.0) / 250.0 * 70.0 + 15.2;
					else
						amount = flVelocity2D / 250.0 * 20.0 + 5.0;
					
					g_iAlpha[id] = floatround(amount);
					g_flAlphaModifier[id] = 1.0;
					g_flHideStart[id] = get_gametime();
					
				}
				else
				{
					g_flAlphaModifier[id] = 1.0 - (get_gametime() - g_flHideStart[id]) / 0.35;
					g_flAlphaModifier[id] = g_flAlphaModifier[id] < 0.0 ? 0.0 : g_flAlphaModifier[id];
					
					if (g_flNextBreathe[id] <= get_gametime())
					{
						emit_sound(id, CHAN_ITEM, SOUND_BREATHE, 1.0, ATTN_NORM, 0, PITCH_NORM);
						g_flNextBreathe[id] = get_gametime() + random_float(3.0, 5.0);
					}
				}
				new iEnt = Get_P_Mdl_Ent( id );
				
				SetRendering( iEnt, kRenderTransAlpha, kRenderFxGlowShell, 0, 0, 0, floatround(g_iAlpha[id] * g_flAlphaModifier[id]) );
				
			}
			
			return FMRES_IGNORED;
		}
		
				
		public HAM_Player_Killed_Post(id, iAttacker, shouldgib)
		{
			if (g_iTeam[id] == TEAM_TERRORIST)
				emit_sound(id, CHAN_ITEM, SOUND_BREATHE, 1.0, ATTN_NORM, SND_STOP, PITCH_NORM);
		}
		


		public HAM_Player_Spawn(id)
		{
			new iTeam = get_pdata_int(id, m_iTeam);
			g_iTeam[id] = iTeam;
		}

		public HAM_Player_Spawn_Post(id)
		{
			if (g_iTeam[id] == TEAM_TERRORIST)
			{
				StripSlot( id, 1 );
				StripSlot( id, 2 );
				StripSlot( id, 4 );
				StripSlot( id, 5 );
				
				MP_GiveWpnItem( id, "knife" );
				
				g_flNextBreathe[id] = get_gametime() + random_float(0.0, 2.0);
				set_pev(id, pev_gravity, 0.8);
			}
			else
			{
				set_pev(id, pev_gravity, 0.9);
			}
			
			if (!get_pdata_bool(id, m_bIsVIP))
			{
				set_pdata_int(id, m_iKevlar, 2);
				set_pev(id, pev_armorvalue, 100.0);
			}
			
			
			return HAM_IGNORED;
		}

		public HAM_WeaponBox_Touch(pWeaponBox, id)
		{
			if (id > 33 || id <= 0)
				return HAM_IGNORED;
			
			if (g_iTeam[id] == TEAM_TERRORIST)
			{
				new pWeapon = get_pdata_cbase(pWeaponBox, m_rgpPlayerItems2[5]);
				
				if (pWeapon <= 0)
					return HAM_SUPERCEDE;
				
				new iId = get_pdata_int(pWeapon, m_iId);
				
				if (iId != CSW_C4)
					return HAM_SUPERCEDE;
				
				return HAM_IGNORED;
			}
			
			return HAM_IGNORED;
		}

		public HAM_AddPlayerItem(id, iEnt)
		{
			if (g_iTeam[id] == TEAM_TERRORIST)
			{
				new iId = get_pdata_int(iEnt, m_iId);
				
				if (iId != CSW_KNIFE && iId != CSW_C4)
					return HAM_SUPERCEDE;
			}
			
			return HAM_IGNORED;
		}
		
		
		
		stock Get_P_Mdl_Ent( id )
		{
			new ent = -1;
			
			while ((ent = engfunc(EngFunc_FindEntityByString, ent, "classname",  P_MDL_ENT )) != 0)
			{
				if( pev( ent, pev_aiment ) == id )
					return ent;
			}
			
			return 0;
		}
		
		
		stock SetRendering(id, Render=kRenderNormal, FX=kRenderFxNone, R=0, G=0, B=0, RenderAmount=0)
		{
			new Float:RenderColor[3];
			RenderColor[0] = float(R);
			RenderColor[1] = float(G);
			RenderColor[2] = float(B);
			
			set_pev(id, pev_renderfx, FX);
			set_pev(id, pev_rendercolor, RenderColor);
			set_pev(id, pev_rendermode, Render);
			set_pev(id, pev_renderamt, float(RenderAmount));
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		