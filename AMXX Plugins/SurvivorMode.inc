
enum NpcType_e
{
	NPC_NORMALZOMBIE = 1,
	NPC_INVISIBLE,
	NPC_BOOMER,
};

native Suv_CreateNpc( iType, iMaxHealth, iDamage, iMaxSpeed, iJumpHeight, iModelIndex, szModelName[] , Float:flMins[3], Float:flMax[3], Float:flSkillTime = 0 );
native Suv_SetRoundEnd();
native Suv_SetNpcDead( iEnt );