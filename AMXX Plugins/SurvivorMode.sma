	/*
	*	AMX Mod X Scipt
	*		| Author  : Niko
	*		| Plugin  : Survivor Mode
	*		| Version : 0.1
	*
	*	[ Survivor Mode ]
	*	On the purpose of making the game running more efficiently
	*	The NPC main codes is put in the module ( SurvivorMode_amxx.dll )
	*	And this plugin construct the game rules of Survivor mode
	*	
	*
	*/



	#include <amxmodx>
	#include <fakemeta>
	#include <hamsandwich>
	#include "SurvivorMode.inc"
	#include "util.sma"

	#define PLUGIN_NAME			"Survivor Mode"
	#define PLUGIN_VERSION		"0.2"
	#define PLUGIN_AUTHOR		"Niko"

	#define NPC_CLASSNAME		"AiNpc"
	

	// Global Part
	
	
	new gmsgScoreInfo = 0;

	new g_sModelIndexNormalZombie = 0;
	new g_sModelIndexInvisibleZb = 0;
	new g_sModelIndexBoomerZb = 0;

	new g_sModelIndexBloodSpraySpr = 0;
	new g_sModelIndexBloodDropSpr = 0;

	new g_iRoundNum = 0;
	new g_iKillsNum = 0;
	new g_iFrags[33];

	enum (+= 1000)
	{
		TASK_NONE = 1000,
		TASK_CREATE_NPC,
		TASK_UPDATE_KILL,
	};
	
	// func declaration
	
	forward Fs_TimerEnd();


		
		
		// Function Part
		
		
		
		public plugin_precache()
		{
			g_sModelIndexBloodSpraySpr = precache_model( "sprites/bloodspray.spr" );
			g_sModelIndexBloodDropSpr = precache_model( "sprites/blood.spr" );
			
			g_sModelIndexNormalZombie = precache_model( "models/survivor/normal.mdl" );
			g_sModelIndexInvisibleZb = precache_model( "models/survivor/invisible_zombi.mdl" );
			g_sModelIndexBoomerZb = precache_model( "models/survivor/boomer_zombie.mdl" );
			
			server_cmd("mp_roundtime 6");
			server_cmd("mp_freezetime 6");
		}
		
		
		public plugin_init()
		{
			register_plugin( PLUGIN_NAME, PLUGIN_VERSION, PLUGIN_AUTHOR);
			
			gmsgScoreInfo = get_user_msgid( "ScoreInfo" );
			
			RegisterHam(Ham_TraceAttack, "info_target", "HAM_TraceAttack");
			RegisterHam(Ham_Killed, "info_target", "HAM_Killed");
			
			register_event("HLTV", "event_NewRound", "a", "1=0", "2=0");
			register_logevent("LogEvent_RoundEnd", 2, "1=Round_End");
			register_logevent("LogEvent_RoundStart",2, "1=Round_Start");
			
			
		}
		
		
		public client_putinserver(id)
		{
			Cl_SendGameMode( id, GAME_MODE_SURVIVOR );
			
			if( g_iRoundNum == 0 )
				server_cmd( "sv_restart 3" );
		}

		// ------------------------------- Event

		public event_NewRound()
		{
			
			g_iRoundNum ++;
			ResetData();
			Cl_UpdateClientData( 0, MH_MSG_UPDATE_SUV_ROUND,   g_iRoundNum );
			
			delete_task( TASK_CREATE_NPC );
			delete_task( TASK_UPDATE_KILL );
			set_task( 0.3, "Task_UpdateKill", TASK_UPDATE_KILL);
		}
		
		
		public LogEvent_RoundStart()
		{
			set_task( 9.0, "Task_CreateNpc" , TASK_CREATE_NPC);
			Cl_UpdateClientData( 0, MH_MSG_SET_COUNTDOWN, 9 );
		}
		
		
		public LogEvent_RoundEnd()
		{
			delete_task( TASK_CREATE_NPC );
			Suv_SetRoundEnd();
			
			new iAlivePlayersNum = 0;
			for( new id = 0; id< 33; id++)
			{
				if( !pev_valid(id) )
					continue;
				if( !is_user_connected(id) )
					continue;
					
				if( pev(id, pev_health) > 0 )
					iAlivePlayersNum ++;
					
			}
			if( !iAlivePlayersNum && g_iRoundNum )
			{
				Cl_UpdateClientData( 0, MH_MSG_SET_SUV_ROUNDEND, 0 );
				g_iRoundNum --;
			}
			else
				Cl_UpdateClientData( 0, MH_MSG_SET_SUV_ROUNDEND, 1 );
		}
		
		
		
		// ------------------------------- Task

		public Task_CreateNpc( iTaskid )
		{
			delete_task( iTaskid );
			MakeNpc( g_iRoundNum );
			set_task( 1.5, "Task_CreateNpc" , iTaskid);
		}
		
		
		public Task_UpdateKill( iTaskid )
		{
			Cl_UpdateClientData( 0, MH_MSG_UPDATE_SUV_KILL, g_iKillsNum );
			new id = iTaskid - TASK_UPDATE_KILL;
			
			if( id == 0 )
			{
				for( id = 1; id < 33; id++ )
				{
					if( !is_user_connected(id) )
						continue;
					
					set_pev( id, pev_frags, g_iFrags[id] );
			
					message_begin( MSG_BROADCAST,  gmsgScoreInfo);
					write_byte(id);
					write_short(pev(id, pev_frags));
					write_short(get_user_deaths(id));
					write_short(0);
					write_short(get_user_team(id));
					message_end();
				}
			}
			else
			{
				set_pev( id, pev_frags, g_iFrags[id] );
			
				message_begin( MSG_BROADCAST,  gmsgScoreInfo);
				write_byte(id);
				write_short(pev(id, pev_frags));
				write_short(get_user_deaths(id));
				write_short(0);
				write_short(get_user_team(id));
				message_end();
			}
		}
		
		
		
		
		// ---------------------------- Stock Func -------
		
		
		
		stock SpawnBlood( const Float:vecOrigin[3], iAmount )
		{
			if(iAmount == 0)
				return;
			
			iAmount *= 2;
			if(iAmount > 255)
				iAmount = 255;
			
			engfunc( EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, vecOrigin );
			write_byte( TE_BLOODSPRITE );
			engfunc(EngFunc_WriteCoord, vecOrigin[0]);
			engfunc(EngFunc_WriteCoord, vecOrigin[1]);
			engfunc(EngFunc_WriteCoord, vecOrigin[2]);
			write_short( g_sModelIndexBloodSpraySpr );
			write_short( g_sModelIndexBloodDropSpr );
			write_byte( 247 );																						// Color
			write_byte(min(max(3, iAmount / 10), 16));																// Amount
			message_end();
		}

		//-----------------------------  Ham Func --------
		
		public HAM_TraceAttack( iEnt, iAttacker, Float:flDamage, Float:flDirection[3], trTraceHandle, damagetype)
		{
			new szClassname[64];
			pev( iEnt, pev_classname, szClassname, charsmax(szClassname));
			
			if( strcmp( szClassname, NPC_CLASSNAME) )
				return HAM_IGNORED;
			
			// Reset Damage
			new iHitGroup = get_tr2( trTraceHandle, TR_iHitgroup);
			new Float:flNewDmg = flDamage;
			
			if( iHitGroup == HIT_HEAD)
				flNewDmg *= random_float( 1.5, 2.0 );
				
			else if( HIT_CHEST <= iHitGroup <= HIT_RIGHTARM)
				flNewDmg *= random_float(0.8, 1.0);
			
			else if( iHitGroup != HIT_GENERIC )
				flNewDmg *= random_float(0.6, 0.8);
				
			SetHamParamFloat( 3, flNewDmg);
			
			// Reset Velocity
			new Float:vecVelocity[3];
			pev( iEnt,  pev_velocity,  vecVelocity);
			vecVelocity[0] *= 0.3;
			vecVelocity[1] *= 0.3;
			set_pev( iEnt, pev_velocity, vecVelocity);
			
			// Spawn Blood
			new Float:vecEndPos[3];
			get_tr2( trTraceHandle,  TR_vecEndPos, vecEndPos);
			SpawnBlood( vecEndPos, floatround( flNewDmg ) );
			
			return HAM_IGNORED
		}

		
		
		public HAM_Killed( iEnt, iAttacker, iGib)
		{
			new szClassname[64];
			pev( iEnt, pev_classname, szClassname, charsmax(szClassname));
			
			if( strcmp( szClassname , NPC_CLASSNAME ) )
				return HAM_IGNORED;
			
			// Jugde if the solid type is "SOLID_NOT"
			// If it is, the npc should have been dead, but the body is still there
			// In this situation, don't kill the npc again, and don't update the kills num either
			// And body will be removed in about 3 sec by the module
			// You don't need to do anything in this case
			
			if( pev(iEnt, pev_solid ) != SOLID_NOT )
			{
				set_pev( iEnt, pev_solid, SOLID_NOT );
				g_iKillsNum ++;
			}
				
			// Why should we make the "Update Client data func" in a "task"
			// Sometimes you kill a big amount of npc in a very short time
			// (example: you launcher rocket,and kill so much npc just in a short time)
			// In this situation, it is not necessary to send message to update the client data every time when a npc is killed
			// Because you can send message in a delay time, to prevent resources wasting
			// So it is a good approach to saving the server's resources  that make it in a task
			
			delete_task( TASK_UPDATE_KILL );
			set_task( 0.3, "Task_UpdateKill", TASK_UPDATE_KILL + iAttacker);
			
			Suv_SetNpcDead( iEnt );
			
			return HAM_SUPERCEDE
		}
		
		
		
		// ---------------------------------------- Foward func ----
		
		// The func "Fs_TimerEnd" is a forward func
		// When the round time have run out, the func will be called
		
		
		public Fs_TimerEnd()
		{
			LogEvent_RoundEnd();
		}
		
		
		
		// ----------------------------------------  Private Func ------
		
		MakeNpc( iRoundNum )
		{
			new iType;
			
			if( iRoundNum < 4 )
				iType = NPC_NORMALZOMBIE;
			else if( iRoundNum >= 4 && iRoundNum < 6 )
				iType = random_num(1, 2) == 1 ? NPC_NORMALZOMBIE : NPC_BOOMER;
			else if( iRoundNum  >= 6 && iRoundNum < 9)
			{
				new iRan = random_num(1, 3);
				if( iRan == 1)
					iType = NPC_NORMALZOMBIE;
				else if( iRan == 2)
					iType = NPC_INVISIBLE;
				else if( iRan ==3 )
					iType = NPC_BOOMER;
			}
			else if( iRoundNum >= 9 && iRoundNum < 12)
			{
				iType = random_num( 1, 2) == 1? NPC_BOOMER : NPC_INVISIBLE;
			}
			else if( iRoundNum >= 12)
			{
				iType = random_num( NPC_NORMALZOMBIE,  NPC_BOOMER );
			}
			
			new iExponent = iRoundNum * iRoundNum / 2;
			new iHealth = random_num( 20 + iExponent, 30 + iExponent);
			new iDamage = random_num( 1  + iExponent, 5 + iExponent );
			new iMaxSpeed = random_num( 180 + iExponent, 200 + iExponent );
			
			new Float:vecMins[3] , Float:vecMaxs[3];
			new Float:flSkillTime = 0;
			new iModelIndex = 0;
			new szModelName[128];
			new iJumpHeight;
			
			switch( iType )
			{
			case NPC_NORMALZOMBIE:
				{
					vecMins = { -16.0, -16.0 , -32.0 };
					vecMaxs = { 16.0, 16.0 , 32.0 };
					iJumpHeight = 300;
					iModelIndex = g_sModelIndexNormalZombie;
					format( szModelName, charsmax(szModelName), "models/survivor/normal.mdl" );
					//break;
				}
			case NPC_INVISIBLE:
				{
					vecMins = { -16.0, -16.0 , -32.0 };
					vecMaxs = { 16.0, 16.0 , 32.0 };
					iJumpHeight = 500;
					flSkillTime = 1.0 + iExponent / 15.0;
					iModelIndex = g_sModelIndexInvisibleZb;
					format( szModelName, charsmax(szModelName), "models/survivor/invisible_zombi.mdl" );
					//break;
				}
			case NPC_BOOMER:
				{
					vecMins = { -32.0, -32.0 , -32.0 };
					vecMaxs = { 32.0, 32.0 , 32.0 };
					iJumpHeight = 300;
					iModelIndex = g_sModelIndexBoomerZb;
					format( szModelName, charsmax(szModelName), "models/survivor/boomer_zombie.mdl"  );
					//break;
				}
			}
			
			Suv_CreateNpc( iType, iHealth, iDamage, iMaxSpeed, iJumpHeight, iModelIndex, szModelName, vecMins, vecMaxs, flSkillTime );
		}
		
		
		
		
		ResetData()
		{
			for( new id = 1; id<33; id++ )
			{
				if( !pev_valid(id) || !is_user_connected(id))
					continue;
				g_iFrags[id] = 0;
			}
			
			g_iKillsNum = 0;
		}
		
	
	/* Notepad ++ */
	/* Built date : 2014.9.6 */
