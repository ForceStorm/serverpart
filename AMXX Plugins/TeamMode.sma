
	/*
	*	AMX Mod X Scipt
	*		| Author  : Niko
	*		| Plugin  : Team Mode
	*		| Version : 0.1
	*		| Modify  : Niko
	*
	*
	*/
	
	
	#include <amxmodx>
	#include <fakemeta>
	#include <hamsandwich>
	#include "util.sma"
	

	#define PLUGIN_NAME			"Team Mode"
	#define PLUGIN_VERSION		"0.2"
	#define PLUGIN_AUTHOR		"Niko"
	
	#define TASKID_RESPAWN		4000
	#define SPAWN_WAIT_TIME		4.0
	
	
	forward Fs_TimerEnd();
	
	

	
		public plugin_init()
		{
			register_plugin ( PLUGIN_NAME,  PLUGIN_VERSION,  PLUGIN_AUTHOR );
			
			register_event("DeathMsg", "event_DeathMsg", "a")
		}
		
		
		
		public event_DeathMsg()
		{
			new id = read_data(2);
			set_task( SPAWN_WAIT_TIME, "Task_Respawn",  id + TASKID_RESPAWN );
			
		}
		
		
		
		
		public Task_Respawn( iTaskid )
		{
			new id = iTaskid - TASKID_RESPAWN;
			if( is_user_connected(id) )
				ExecuteHamB(Ham_CS_RoundRespawn, id);
		}
		
		
		public client_putinserver( id )
		{
			//Cl_SendGameMode( id, GAME_MODE_GHOST );
			
			static iFirstTime = TRUE;
			
			if( iFirstTime )
			{
				server_cmd( "sv_restart 3" );
				iFirstTime = FALSE;
			}
		}
		
		public Fs_TimerEnd()
		{
			for( new id = 1; id <= get_playersnum(); id++)
			{
				if( !is_user_connected(id) )
					continue;
					
				delete_task( id + TASKID_RESPAWN );
			}
		}
		
		
		
		
		
		
		
		