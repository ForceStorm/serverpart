//***************************************************************
//*	Battle Mages v. 1.0.1 for AMXX by Martin J. Van der Cal		*
//*												*
//*	Mod adding magic to the game.							*
//*												*
//*	Copyright (C) 2004, Martin J. Van der Cal					*
//*	This plugin is under the GNU General Public License, read		*
//*	"Battle Mages Readme.doc" for further information.			*
//*												*
//*	bmconst.inc										*
//*												*
//***************************************************************

#if !defined BATTLE_MAGES_CONSTANTS
#define BATTLE_MAGES_CONSTANTS

#include "effectconst"

// Debug mode. with it uncommented you get alot of debug messages in server console
//#define DEBUG_SERVER_PRINT

// General Defines
#define MAX_PLAYERS				32	// Max amount of players that this mod is made for
#define INITIAL_SPELL			0
#define HUD_PLAYERINFO			1
#define DURATION_BAR_EFFECT		108

// Defines to make code more understandable
#define DONT_CHANGE_SCHOOL		0
#define DONT_REMOVE_FRAG		1	// for user_kill function
#define NOT_USED				0
#define WASNT_CREATED			0	// for use with entities
#define GET_AIM_ENTITY_HIT		3	// For use with get_user_origin, mode 3
#define ABNORMAL_HEALTH			1024
#define GET_ALL_TERRORISTS		"e", "TERRORIST"
#define GET_ALL_CT				"e", "CT"
#define ENTITY_NO_OWNER			0
#define USE_PREDATOR_MODEL		1

// Max sizes of strings
#define MAX_MENU_SIZE			512
#define MAX_HUD_SIZE			512
#define MAX_MODEL_PATH_SIZE		64
#define MAX_SPELL_SIZE			32
#define MAX_TITLE_SIZE			64
#define MAX_TEAM_SIZE			16
#define MAX_HELP_SIZE			2048
#define MAX_TEAMNAME_SIZE		16
#define MAX_PLAYERNAME_SIZE		32
#define MAX_AUTHID_SIZE			32
#define MAX_CHARS_SCHOOL_NAME	32

// Linear and space defines
#define X_AXIS					0
#define Y_AXIS					1
#define Z_AXIS					2

#define POINT					2
#define VECTOR					3


#define PLAYER_RADIUS			80
#define AMOUNT_EXTEND_AIM		9999.0
#define PLAYER_CENTER_Z_OFFSET	60

// Error codes and ok code
#define BM_OK					0
#define BM_FAILED				1
#define BM_UNSTABLE				2
#define BM_ALREADY_ACTIVE		3


// Menu defines
#define BM_MAINMENU_IDSTRING	"battlemagesmainmenu"
#define BM_MAINMENU_KEYS		MENU_KEY_1 | MENU_KEY_2 | MENU_KEY_3 | MENU_KEY_4 | MENU_KEY_5 | MENU_KEY_0

#define BM_SCHOOLMENU_IDSTRING	"battlemagesschoolmenu"
#define BM_SCHOOLMENU_KEYS		MENU_KEY_1 | MENU_KEY_2 | MENU_KEY_3 | MENU_KEY_4 | MENU_KEY_0

#define BM_UPGRADEMENU_IDSTRING	"battlemagesupgrademenu"
#define BM_UPGRADEMENU_KEYS		MENU_KEY_1 | MENU_KEY_2 | MENU_KEY_3 | MENU_KEY_4 | MENU_KEY_5 | MENU_KEY_0

// CVAR names
#define CVAR_MAGIC_COOLDOWN			"bm_magicdelay"
#define CVAR_START_MAGIC_POINTS		"bm_startmp"
#define CVAR_PLAYER_INFO_POSITION	"bm_playerinfopos"
#define CVAR_USE_PREDATOR_MODEL		"bm_usepredator"
#define CVAR_USE_SAVEXP				"bm_savexp"

// Player info positions
#define POSITION_DOWN_LEFT		0
#define POSITION_DOWN_RIGHT		1
#define POSITION_DOWN_CENTER	2

// Task IDs (1 reserved only)
#define FREEZE_TIME_TASK_ID				600

// Task IDs (35 reserved for each)
#define SPELL_COOLDOWN_TASK_ID			700
#define INVISIBILITY_DURATION_TASK_ID	735
#define FIREBALL_FLY_TASK_ID			770
#define EXPLOSION_TASK_ID				805
#define ICE_IMPLOSION_DOT_TASK_ID		840
#define LIGHTNING_SPEED_TASK_ID			875
#define SHAKE_WORLD_TASK_ID				910
#define MIND_BLAST_TASK_ID				945
#define REGENERATION_TASK_ID			980
#define ENCHANT_WEAPON_BURN_TASK_ID		1015
#define STONESHIELD_TASK_ID				1050
#define CHAIN_LIGHTNING_TASK_ID			1085
#define AURA_OF_LEECH_TASK_ID			1120
#define FORCE_FIELD_TASK_ID				1155
#define MANA_REGENERATION_TASK_ID		1190
#define AVATAR_TASK_ID					1225
#define ROUND_NEW_DELAYED_TASK_ID		1260

// Magics general defines:
#define NUMBER_OF_SCHOOLS			4

#define WARRIOR						0
#define ELEMENTAL_MAGE				1
#define ENERGY_MAGE					2
#define ILLUSION_MAGE				3
#define LIFE_MAGE					4

#define BM_MAX_SPELL_LEVEL			10	// This is the upper limit of the spell level
#define BM_SPELLPOINT_PER_LEVEL		1	// The amount of spell points a player gains each level
#define MAX_NUMBER_OF_SPELLS		5	// The number of spells each school has available to use

// Defines to make code easier to read
#define SPELL_ONE					0
#define SPELL_TWO					1
#define SPELL_THREE					2
#define SPELL_FOUR					3
#define SPELL_FIVE					4


// Magics sorted by school:
// Elemental Magics
#define NUMBER_OF_MAGICS_ELEMENTAL	5

// Name of spell								Type of spell

// 1. Fireball								AE Nuke
#define FIREBALL					0	// ID of spell, dont change
#define FIREBALL_POWER				20	// Base damage. Note direct hit result in damage * 3, thus 60 in base damage
#define FIREBALL_COST				7	// Cost to cast the spell
#define FIREBALL_INCREASE			1	// Damage increase each spell level. Is also multiplied by 3, thus 1*3 increase in damage each level
#define FIREBALL_SPEED				80	// The velocity of the fireball
#define FIREBALL_SIZE				20	// Size of the fireball
#define NUMBER_LOOPS				3	// Multiplier to damage, change with caution
#define EXPLOSION_RANGE				300	// The range of the blast when the fireball explodes
#define BLAST_RADIUS				250	// The range of the effects when the fireball explodes
#define FIREBALL_BONUS				10	// Bonus damage when the player reaches max spell lvl (mastery),

// 2. Ice Implosion							DoT + Slow
#define ICE_IMPLOSION				1	// ID of spell, dont change
#define ICE_IMPLOSION_POWER			0	// Base damage, note this damage is done each interval
#define ICE_IMPLOSION_COST			7	// Cost to cast the spell
#define ICE_IMPLOSION_INCREASE		1	// Damage increase each level, note this damage is done each interval
#define ICE_IMPLOSION_TURNS			5	// How many turns we will do damage
#define ICE_IMPLOSION_INTERVAL		2.0	// The delay between the turns
#define ICE_IMPLOSION_SLOW_SPEED	125.0	// The speed of the player who is under the ice implosion effect
#define ICE_IMPLOSION_SPEEDBONUS	2.0	// The speed bonus when gained mastery, max speed is then ICE_IMPLOSION_SLOW_SPEED / ICE_IMPLOSION_SPEEDBONUS

// 3. Magma Shield							Round Buff
#define MAGMA_SHIELD				2	// ID of spell, dont change
#define MAGMA_SHIELD_POWER			0.10	// Base power, 0.10 means the player mitigates 10% of the damage done to him
#define MAGMA_SHIELD_COST			10	// Cost to cast the spell
#define MAGMA_SHIELD_MIRROR_DMG		0.10	// Mirror damage, 0.10 means 10% of the damage is returned to the attacker
#define MAGMA_SHIELD_BONUS			0.05	// When gained mastery, this bonus will be applied
#define MAGMA_SHIELD_INCREASE		2	// Percentage increase to damage mitigation, E.g. with lvl 5 on this spell, it will be 2*5 + base power(10) = 20% mitigation

// 4. Enchant Weapon							Special: Bullets do more damage
#define ENCHANT_WEAPON				3	// ID of spell, dont change
#define ENCHANT_WEAPON_POWER		1	// Base damage, +1 damage enchantement to weapon at spell lvl 0.
#define ENCHANT_WEAPON_COST			10	// Cost to cast the spell
#define ENCHANT_WEAPON_INCREASE		1	// Damage increase for each lvl. E.g lvl 5 means base damage(1) + 5 * 1 = +6 damage

// 5. Stone Shield							Special: Summon a wall
#define STONESHIELD					4	// ID of spell, dont change
#define STONESHIELD_POWER			500.0	// Base hp of the wall
#define STONESHIELD_COST			7	// Cost to cast the spell
#define STONESHIELD_DURATION		30.0	// The duration of the wall, it will disappear after this amount of seconds
#define STONESHIELD_HEALTH_INCREASE	200.0	// Health of wall increased per spell lvl
#define STONESHIELD_DURATION_INCREASE	3.0	// Duration of wall increased per spell lvl

// Experimental variables
#define STONESHIELD_TAKEDAMAGE		100.0
#define STONESHIELD_DAMAGE_TAKE		100.0
#define STONESHIELD_ZAXIS_MODIFIER	20.0
#define STONESHIELD_RADIUS			90.0
#define STONESHIELD_HEIGHT			150.0	// This is really half the height
#define STONESHIELD_WIDTH			5.0		// This is really half the width

// 6.  Fireball (NO UPGRADE)								Area Damage
#define FIREBALL_NOUPGRADE			5	// There is no upgrade spell

// 6. Rain of Fire Possible future upgrade
// #define RAIN_OF_FIRE				5
// #define RAIN_OF_FIRE_POWER			20
// #define RAIN_OF_FIRE_COST			15

// 7. Ice Implosion (NO UPGRADE)
#define ICE_IMPLOSION_NOUPGRADE		6	// There is no upgrade spell

// 8. Magma Shield (NO UPGRADE)
#define MAGMA_SHIELD_NOUPGRADE		7

// 9. Enchant Weapon (Fire)	- Mastery spell
#define ENCHANT_WEAPON_FIRE			8	// ID of spell, dont change
#define ENCHANT_WEAPON_FIRE_POWER	10	// +10 damage on weapon with chance to cause a DoT on the enemy
#define ENCHANT_WEAPON_FIRE_COST	15	// Cost to cast the spell
#define ENCHANT_WEAPON_FIRE_DOT		3	// DoT damage
#define ENCHANT_WEAPON_FIRE_INTERVAL	5.0	// Delay between the damage intervals
#define ENCHANT_WEAPON_FIRE_BURNCHANCE	0.10	// 0.10 means 10% chance for your bullet to cause the DoT
#define ENCHANT_WEAPON_FIRE_GLOW	150	// Glow amount, used for effects

// 10. Stone Shield (NO UPGRADE)
#define STONESHIELD_NOUPGRADE		9	// There is no upgrade spell

// Energy Magics
#define NUMBER_OF_MAGICS_ENERGY		5

// 1. Lightning Strike							Nuke
#define LIGHTNING_STRIKE			0	// ID of spell, dont change
#define LIGHTNING_STRIKE_POWER		40	// Base damage of spell
#define LIGHTNING_STRIKE_COST		5	// Cost to cast the spell
#define LIGHTNING_STRIKE_POWER_INCREASE	3	// Damage increase for each spell level

// 2. Lightning Speed							Duration Buff
#define LIGHTNING_SPEED				1	// ID of spell, dont change
#define LIGHTNING_SPEED_POWER		300.0	// Run speed at spell lvl 0
#define LIGHTNING_SPEED_COST		10	// Cost to cast the spell
#define LIGHTNING_SPEED_INCREASE	10.0	// Increase in run speed for each spell lvl
#define LIGHTNING_SPEED_DURATION	30.0	// Duration of the spell in seconds
#define LIGHTNING_SPEED_BONUS_DURATION	30.0	// Bonus duration of the spell when the player becomes master, in seconds.

// 3. Planar Shield							Round Buff
#define PLANAR_ARMOR				2	// ID of spell, dont change
#define PLANAR_ARMOR_POWER			150	// Base power
#define PLANAR_ARMOR_COST			5	// Cost to cast the spell
#define PLANAR_ARMOR_INCREASE		10	// Increase for each level

// 4. Leech									Self Heal + DoT
#define LEECH						3	// ID of spell, dont change
#define LEECH_POWER					20	// Base damage/heal
#define LEECH_COST					10	// Cost to cast the spell
#define LEECH_INCREASE				3	// Damage/heal increase for each spell lvl
#define LEECH_RADIUS				2000	// Leeches from the closest enemy within this radius
#define LEECH_GLOW					150	// Glow amount, used for effects

// 5. Force Field
#define FORCE_FIELD					4	// ID of spell, dont change
#define FORCE_FIELD_POWER			3	// Base damage of spell, Note does this damage each interval
#define FORCE_FIELD_POWER_INCREASE	2	// Divided by this, so Spell lvl 10 means +5 damage thus 10 damage in total
#define FORCE_FIELD_COST			10	// Cost to cast the spell
#define FORCE_FIELD_NUM_INTERVALS	30	// The number of intervals
#define FORCE_FIELD_INTERVAL		1.0	// Delay between the intervals
#define FORCE_FIELD_DURATION_BONUS	30	// Bonus when gained mastery, also it wont hurt you as caster
#define FORCE_FIELD_RADIUS			200	// Radius of the force field
#define FORCE_FIELD_RADIUS_INCREASE	10	// Increase of the radius for each level of the caster
#define FORCE_FIELD_EFFECT_HEIGHT	1000	// Height of the effect
#define FORCE_FIELD_EFFECT_BRIGHTNESS	100	// Brightness of the effect
#define FORCE_FIELD_EFFECT_SPEED	100	//Speed of the effect
#define FORCE_FIELD_GLOW			150	// Glow amount, used for effects. Player will glow when entering a force field

// 6. Chain Lightning							Chain nuke
#define CHAIN_LIGHTNING				5	// ID of spell, dont change
#define CHAIN_LIGHTNING_POWER		2	// After the first strike, the damage will be divided with this amount (Lightning strike damage is used)
#define CHAIN_LIGHTNING_COST		10	// Cost to cast this spell
#define CHAIN_LIGHTNING_RADIUS		500	// Only jumps to players within this radius
#define CHAIN_LIGHTNING_JUMPDELAY	0.2	// Delay between the jumps

// 7. Lightning Speed (NO UPGRADE)
#define LIGHTNING_SPEED_NOUPGRADE	6	// There is no upgrade spell

// 8. Ghost Armor
#define GHOST_ARMOR					7	// ID of spell, dont change
#define GHOST_ARMOR_COST			10	// Cost to cast the spell
#define GHOST_ARMOR_EFFECT			175	// Transparent effect amount
#define GHOST_ARMOR_APPARATION		0.10	// Bullets have a chance to go through the player without hitting them, 0.10 = 10% chance

// 9. Aura of Leech							Aura Proximity nuke
#define AURA_OF_LEECH				8	// ID of spell, dont change
#define AURA_OF_LEECH_POWER			5	// Damage/heal each interval
#define AURA_OF_LEECH_COST			0	// Cost to cast the spell
#define AURA_OF_LEECH_INTERVALS		2.5	// Delay between the intervals
#define AURA_OF_LEECH_INTERVAL_COST	1	// Cost each interval
#define AURA_OF_LEECH_GLOW			100	// Glow amount, used for effects.

// 10. Force Field (NO UPGRADE)
#define FORCE_FIELD_NOUPGRADE		9	// There is no upgrade spell

// Illusion Magics
#define NUMBER_OF_MAGICS_ILLUSION	5

// 1. Invisibility								Duration Buff
#define INVISIBILITY				0	// ID of spell, dont change
#define INVISIBILITY_POWER			100	// Base invisibility amount
#define INVISIBILITY_COST			10	// Cost to cast the spell
#define INVISIBILITY_DURATION		30	// Duration of the spell
#define INVISIBILITY_INCREASE		5	// Transparent increase for each spell level

// 2. Mana Shield								Round buff
#define MANA_SHIELD					1	// ID of spell, dont change
#define MANA_SHIELD_POWER			0.20	// Base amount of damage absorbed by the mana shield
#define MANA_SHIELD_COST			0	// Cost to cast the spell
#define MANA_SHIELD_INCREASE		0.05	// Amount of damage absorbed increase per spell lvl
#define MANA_SHIELD_BONUS			0.15	// Bonus amount of damage absorbed when reaching mastery

// 3. Mind Blast								AoE DoT + Confusion
#define MIND_BLAST					2	// ID of spell, dont change
#define MIND_BLAST_POWER			3	// Multiplied with MIND_BLAST_INTERVALS and area damage
#define MIND_BLAST_COST				10	// Cost to cast the spell
#define MIND_BLAST_INCREASE			1	// Damage increase for each spell lvl, multiplied by MIND_BLAST_INTERVALS
#define MIND_BLAST_DURATION			5	// Duration between the intervals
#define MIND_BLAST_INTERVALS		3	// Number of intervals
#define MIND_BLAST_RANGE			300	// Radius of the blast
#define MIND_BLAST_FREQ				14	// Used for shake effect

// 4. Self Illusion
#define SELF_ILLUSION				3	// ID of spell, dont change
#define SELF_ILLUSION_POWER 		0.10	// Base chance to do crititcal strike with knife, 0.10 = 10%
#define SELF_ILLUSION_COST			5	// Cost to cast the spell
#define SELF_ILLUSION_INCREASE		0.07	// Chance increased for each spell level
#define SELF_ILLUSION_BONUS			0.21	// Bonus increase when gained mastery
#define SELF_ILLUSION_MODELPATH		"models/player/predator/predator.mdl"	// Predator model path
#define SELF_ILLUSION_MODELNAME		"predator"	// identification name of model, E.g. with predator.mdl as filename, remove .mdl and you get this
#define SELF_ILLUSION_MODELROBOT	"robo"	// ID name for old halflife robo model
#define SELF_ILLUSION_MODELROBOTPATH	"models/player/robo/robo.mdl"	// model path to old halflife robo model

// 5. Replenish Mana
#define REPLENISH_MANA				4	// ID of spell, dont change
#define REPLENISH_MANA_POWER		5	// Base amount of points to replenish mana
#define REPLENISH_MANA_INCREASE		4	// Mana points to be replenished per spell level
#define REPLENISH_MANA_COST			0	// Cost to cast the spell

// 6. Supreme Invisibility
#define SUPREME_INVISIBILITY		5	// ID of spell, dont change
#define SUPREME_INVISIBILITY_POWER	1	// Transparent effect, 1 = you might see their boot marks when they run or walk, 0 = total invisibility
#define SUPREME_INVISIBILITY_COST	15	// Cost to cast the spell
#define SUPREME_INVISIBILITY_MOVEMENT_SPEED	150.0	// Movement speed decreased to this while using this spell

// 7. Mana Shield (NO UPGRADE)
#define MANA_SHIELD_NOUPGRADE		6	// There is no upgrade spell

// 8. Mind Flare
#define MIND_FLARE					7	// ID of spell, dont change
#define MIND_FLARE_POWER			8	// Damage each interval
#define MIND_FLARE_COST				10	// Cost to cast the spell
#define MIND_FLARE_DURATION			3	// Delay between each interval in seconds
#define MIND_FLARE_INTERVALS		6	// Number of intervals to damage and shake them
#define MIND_FLARE_FREQ				14	// Used for shake effect

// 9. Self Illusion	(NO UPGRADE)
#define SELF_ILLUSION_NOUPGRADE		8	// There is no upgrade spell

// 10. Mana Regeneration
#define MANA_REGENERATION			9	// ID of spell, dont change
#define MANA_REGENERATION_POWER		5	// Amount to replenish each interval
#define MANA_REGENERATION_COST		0	// Cost to cast the spell
#define MANA_REGENERATION_INTERVAL	2.5	// Delay between the intervals

// Life Magics
#define NUMBER_OF_MAGICS_LIFE		5

// 1. Heal Self								Healing
#define HEAL_SELF					0	// ID of spell, dont change
#define HEAL_SELF_POWER				30	// Base amount to heal
#define HEAL_SELF_INCREASE			7	// Heal amount increase per spell level
#define HEAL_SELF_COST				10	// Cost to cast the spell

// 2. Heal Other								Healing
#define HEAL_OTHER					1	// ID of spell, dont change
#define HEAL_OTHER_POWER			30	// Base amount to heal
#define HEAL_OTHER_INCREASE			7	// Heal amount increase per spell level
#define HEAL_OTHER_COST				5	// Cost to cast the spell

// 3. Shield of Y'lien							Round Buff
#define SHIELD_YLIEN				2	// ID of spell, dont change
#define SHIELD_YLIEN_POWER			10	// Base amount to increase hit points
#define SHIELD_YLIEN_INCREASE		5	// Hit points increase for each spell level
#define SHIELD_YLIEN_COST			10	// Cost to cast the spell
#define SHIELD_YLIEN_GLOW			100	// Glow amount, used for effects

// 4. Regeneration							Healing
#define REGENERATION				3	// ID of spell, dont change
#define REGENERATION_POWER			2	// Base hitpoints increase for each interval
#define REGENERATION_BONUS			3	// Bonus amount of hit points healed when reached mastery
#define REGENERATION_INCREASE		1	// Amount of hit points healed increase for each spell level
#define REGENERATION_COST			10	// Cost to cast the spell
#define REGENERATION_DURATION		5.0	// Duration between intervals

// 5. Avatar								Special: Extreme Damage Resistance
#define AVATAR						4	// ID of spell
#define AVATAR_POWER				0.25	// Base damage mitigation
#define AVATAR_COST					15	// Cost to cast the spell
#define AVATAR_DURATION				5	// Duration of the spell in seconds
#define AVATAR_INCREASE				0.04	// Damate mitigation increase for each spell level
#define AVATAR_BONUS				0.15	// Bonus mitigation when reaching mastery

// 6. Full Heal Self
#define FULL_HEAL_SELF				5	// ID of spell, dont change
#define FULL_HEAL_SELF_COST			10	// Cost to cast the spell

// 7. Full Heal Other
#define FULL_HEAL_OTHER				6	// ID of spell, dont change
#define FULL_HEAL_OTHER_COST		5	// Cost to cast the spell

// 8. Shield of Y'lienelle
#define SHIELD_YLIENELLE			7	// ID of spell, dont change
#define SHIELD_YLIENELLE_POWER		0.10	// Damage returned, 0,10 = 10% damage returned
#define SHIELD_YLIENELLE_COST		10	// Cost to cast the spell

// 9. Regeneration (NO UPGRADE)
#define REGENERATION_NOUPGRADE		8	// There is no upgrade spell

// 10. Avatar (NO UPGRADE)
#define AVATAR_NOUPGRADE			9	// There is no upgrade spell


// Damage Causes, for use with the DoDamage function
#define DMG_CAUSE_FIREBALL				0
#define DMG_CAUSE_LIGHTNING_STRIKE		1
#define DMG_CAUSE_ICE_IMPLOSION			2
#define DMG_CAUSE_MAGMA_SHIELD			3
#define DMG_CAUSE_MIND_BLAST			4
#define DMG_CAUSE_ENCHANT_WEAPON		5
#define DMG_CAUSE_LEECH					6
#define DMG_CAUSE_ENCHANTED_WEAPON		7
#define DMG_CAUSE_ENCHANTED_WEAPON_BURN	8
#define DMG_CAUSE_CHAIN_LIGHTNING		9
#define DMG_CAUSE_AURA_OF_LEECH			10
#define DMG_CAUSE_FORCE_FIELD			11
#define DMG_CAUSE_CRITICAL_KNIFE		12
#define DMG_CAUSE_MIND_FLARE			13
#define DMG_CAUSE_SHIELD_YLIENELLE		14

// Database CVAR names
#define CVAR_DB_HOST				"bm_dbaddress"
#define CVAR_DB_USERNAME			"bm_dbusername"
#define CVAR_DB_PASSWORD			"bm_dbpassword"
#define CVAR_DB_NAME				"bm_dbname"

// Database constants
#define TABLE_NAME			"bm_players"
#define MAX_QUERY_LENGTH	512
#define MAX_ERROR_LENGTH	256

// XP types, base xp shown
#define NUMBER_OF_XP_TYPES		3	// Amount of xp types

#define BM_XP_KILL				0	// Index of the xp type
#define KILL_XP_BASEAMOUNT		20	// Base amount when player killed another, calulated in AddXP function in bmxphandler.inc, same goes for all xp types

#define BM_XP_WINROUND			1
#define WINROUND_XP_BASEAMOUNT	20

#define BM_XP_HEAL_OTHER		2
#define HEAL_OTHER_BASEAMOUNT	20

// NOTE! All XP base values must be more than BM_MAX_LEVEL / 2
// E.g. BM_XP_KILL = 20 and BM_MAX_LEVEL = 30
// 20 > 30 / 2 = true

// E.g. BM_XP_KILL = 20 and BM_MAX_LEVEL = 50
// 20 > 50 / 2 = false, IT WONT WORK

// Used with XP_REGULATOR Only
#define XP_GAIN_VERY_SLOW		16
#define XP_GAIN_SLOW			8
#define XP_GAIN_NORMAL			4
#define XP_GAIN_FAST			2
#define XP_GAIN_VERY_FAST		1

// Set to one of the 5 defines above to change the rate of which the players gain xp
// E.g. for very fast xp
// #define XP_REGULATOR	XP_GAIN_VERY_FAST
#define XP_REGULATOR			XP_GAIN_NORMAL

#define BM_MAX_LEVEL			30	//  MAX_LEVEL / 2 must be lesser than the xp amounts ! ! !

// Level ranges
#define LEVEL_1		50
#define LEVEL_2		190
#define LEVEL_3		490
#define LEVEL_4		1050
#define LEVEL_5		1950
#define LEVEL_6		3270
#define LEVEL_7		5160
#define LEVEL_8		7800
#define LEVEL_9		11310
#define LEVEL_10	15810
#define LEVEL_11	26210
#define LEVEL_12	44210
#define LEVEL_13	71410
#define LEVEL_14	109410
#define LEVEL_15	160410
#define LEVEL_16	226910
#define LEVEL_17	310910
#define LEVEL_18	414410
#define LEVEL_19	540410
#define LEVEL_20	692210
#define LEVEL_21	872210
#define LEVEL_22	1082810
#define LEVEL_23	1327810
#define LEVEL_24	1611310
#define LEVEL_25	1936110
#define LEVEL_26	2305010
#define LEVEL_27	2722610
#define LEVEL_28	3193810
#define LEVEL_29	3721810
#define LEVEL_30	4309810
#define LEVEL_MAX	2000000000

/*
Data used when I calculated the levels with a program I created myself.

Upto lvl 10 the xp that you need is increasing at a lower rate than after lvl 10 to give beginners a quick start
The calculations is based on that the player killed is equal to the level of the player. And it doesnt count bonus xp from winning rounds and healing others
The xp regulator is set to normal since its the defualt setting.

Reading data...
Number of levels (30)
Kill increase upto 10 (10)
Kill increase after 10 (100)
XP Regulator (4)
End of reading data

-------------------------

Performing calulations...
Player at level (0) get (5) xp/kill - and need (10) kills until levelup
Player at level (1) get (7) xp/kill - and need (20) kills until levelup
Player at level (2) get (10) xp/kill - and need (30) kills until levelup
Player at level (3) get (14) xp/kill - and need (40) kills until levelup
Player at level (4) get (18) xp/kill - and need (50) kills until levelup
Player at level (5) get (22) xp/kill - and need (60) kills until levelup
Player at level (6) get (27) xp/kill - and need (70) kills until levelup
Player at level (7) get (33) xp/kill - and need (80) kills until levelup
Player at level (8) get (39) xp/kill - and need (90) kills until levelup
Player at level (9) get (45) xp/kill - and need (100) kills until levelup
Player at level (10) get (52) xp/kill - and need (200) kills until levelup
Player at level (11) get (60) xp/kill - and need (300) kills until levelup
Player at level (12) get (68) xp/kill - and need (400) kills until levelup
Player at level (13) get (76) xp/kill - and need (500) kills until levelup
Player at level (14) get (85) xp/kill - and need (600) kills until levelup
Player at level (15) get (95) xp/kill - and need (700) kills until levelup
Player at level (16) get (105) xp/kill - and need (800) kills until levelup
Player at level (17) get (115) xp/kill - and need (900) kills until levelup
Player at level (18) get (126) xp/kill - and need (1000) kills until levelup
Player at level (19) get (138) xp/kill - and need (1100) kills until levelup
Player at level (20) get (150) xp/kill - and need (1200) kills until levelup
Player at level (21) get (162) xp/kill - and need (1300) kills until levelup
Player at level (22) get (175) xp/kill - and need (1400) kills until levelup
Player at level (23) get (189) xp/kill - and need (1500) kills until levelup
Player at level (24) get (203) xp/kill - and need (1600) kills until levelup
Player at level (25) get (217) xp/kill - and need (1700) kills until levelup
Player at level (26) get (232) xp/kill - and need (1800) kills until levelup
Player at level (27) get (248) xp/kill - and need (1900) kills until levelup
Player at level (28) get (264) xp/kill - and need (2000) kills until levelup
Player at level (29) get (280) xp/kill - and need (2100) kills until levelup

-------------------------

Player at level (0) need (50) XP to levelup
Player at level (1) need (140) XP to levelup
Player at level (2) need (300) XP to levelup
Player at level (3) need (560) XP to levelup
Player at level (4) need (900) XP to levelup
Player at level (5) need (1320) XP to levelup
Player at level (6) need (1890) XP to levelup
Player at level (7) need (2640) XP to levelup
Player at level (8) need (3510) XP to levelup
Player at level (9) need (4500) XP to levelup
Player at level (10) need (10400) XP to levelup
Player at level (11) need (18000) XP to levelup
Player at level (12) need (27200) XP to levelup
Player at level (13) need (38000) XP to levelup
Player at level (14) need (51000) XP to levelup
Player at level (15) need (66500) XP to levelup
Player at level (16) need (84000) XP to levelup
Player at level (17) need (103500) XP to levelup
Player at level (18) need (126000) XP to levelup
Player at level (19) need (151800) XP to levelup
Player at level (20) need (180000) XP to levelup
Player at level (21) need (210600) XP to levelup
Player at level (22) need (245000) XP to levelup
Player at level (23) need (283500) XP to levelup
Player at level (24) need (324800) XP to levelup
Player at level (25) need (368900) XP to levelup
Player at level (26) need (417600) XP to levelup
Player at level (27) need (471200) XP to levelup
Player at level (28) need (528000) XP to levelup
Player at level (29) need (588000) XP to levelup

-------------------------

Player are level (1) when they reach the xp (50)
Player are level (2) when they reach the xp (190)
Player are level (3) when they reach the xp (490)
Player are level (4) when they reach the xp (1050)
Player are level (5) when they reach the xp (1950)
Player are level (6) when they reach the xp (3270)
Player are level (7) when they reach the xp (5160)
Player are level (8) when they reach the xp (7800)
Player are level (9) when they reach the xp (11310)
Player are level (10) when they reach the xp (15810)
Player are level (11) when they reach the xp (26210)
Player are level (12) when they reach the xp (44210)
Player are level (13) when they reach the xp (71410)
Player are level (14) when they reach the xp (109410)
Player are level (15) when they reach the xp (160410)
Player are level (16) when they reach the xp (226910)
Player are level (17) when they reach the xp (310910)
Player are level (18) when they reach the xp (414410)
Player are level (19) when they reach the xp (540410)
Player are level (20) when they reach the xp (692210)
Player are level (21) when they reach the xp (872210)
Player are level (22) when they reach the xp (1082810)
Player are level (23) when they reach the xp (1327810)
Player are level (24) when they reach the xp (1611310)
Player are level (25) when they reach the xp (1936110)
Player are level (26) when they reach the xp (2305010)
Player are level (27) when they reach the xp (2722610)
Player are level (28) when they reach the xp (3193810)
Player are level (29) when they reach the xp (3721810)
Player are level (30) when they reach the xp (4309810)
*/


#endif
