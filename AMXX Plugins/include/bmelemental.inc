//***************************************************************
//*	Battle Mages v. 1.0.1 for AMXX by Martin J. Van der Cal		*
//*												*
//*	Mod adding magic to the game.							*
//*												*
//*	Copyright (C) 2004, Martin J. Van der Cal					*
//*	This plugin is under the GNU General Public License, read		*
//*	"Battle Mages Readme.doc" for further information.			*
//*												*
//*	bmelemental.inc									*
//*												*
//***************************************************************

#if !defined BATTLE_MAGES_ELEMENTAL_MAGIC
#define BATTLE_MAGES_ELEMENTAL_MAGIC

#include "bmconst"
#include "bmgvars"
#include "bmgeneralfuncs"


//***************************************************************
//*	Function CastFireBall(iCasterID)						*
//*	iCasterID:	The id of the player who triggered cast fireball		*
//*												*
//*	Description:									*
//*	Casts a fireball towards the place where the caster aimed		*
//***************************************************************
CastFireBall(iCasterID)
{
	new vecTarget[VECTOR];
	new vecStep[VECTOR];
	new vecOrigin[VECTOR];
	new args[11];
	new iDistance;
	new nSteps;

	get_user_origin(iCasterID, vecOrigin);
	get_user_origin(iCasterID, vecTarget, GET_AIM_ENTITY_HIT);

	new iEntityID, iBodyPart;
	get_user_aiming(iCasterID, iEntityID, iBodyPart);
	
	if ( iEntityID > 0 )
	{
		new Float:fVecOrigin[VECTOR];
		new Float:fVecHit[VECTOR];
		new Float:fVecTarget[VECTOR];

		new iCount = 1; // Failsafe
		fVecHit[0] = float(vecTarget[0]);
		fVecHit[1] = float(vecTarget[1]);
		fVecHit[2] = float(vecTarget[2]);
		
		// Get the amount we will extend the line with
		new Float:fExtendAmount = AMOUNT_EXTEND_AIM - float(get_distance(vecTarget, vecOrigin));
		
		// If the amount is below 0, then the target is beyond our limit (AMOUNT_EXTEND_AIM)
		if ( fExtendAmount > 0.0 )
		{
			// Here we take the line from the caster and the target and extend that line with the calculated amount.
			fVecTarget[0] = ( float(vecTarget[0] - vecOrigin[0]) * fExtendAmount ) + float(vecOrigin[0]);
			fVecTarget[1] = ( float(vecTarget[1] - vecOrigin[1]) * fExtendAmount ) + float(vecOrigin[1]);
			fVecTarget[2] = ( float(vecTarget[2] - vecOrigin[2]) * fExtendAmount ) + float(vecOrigin[2]);

			#if defined DEBUG_SERVER_PRINT
				server_print("[DEBUG][BM] EntityID = %d", iEntityID);
			#endif
			while ( iEntityID <= 32 && iEntityID > 0 && iCount < 33) // iCount is a failsafe, so an infinite loop wont occur
			{
				fVecOrigin[0] = fVecHit[0];
				fVecOrigin[1] = fVecHit[1];
				fVecOrigin[2] = fVecHit[2];
	
				iEntityID = trace_line(iEntityID, fVecOrigin, fVecTarget, fVecHit);

				#if defined DEBUG_SERVER_PRINT
					server_print("[DEBUG][BM] EntityID = %d", iEntityID);
				#endif
				
				iCount++;
			}
			
			vecTarget[0] = floatround(fVecHit[0]);
			vecTarget[1] = floatround(fVecHit[1]);
			vecTarget[2] = floatround(fVecHit[2]);
		}
	}
	iDistance = get_distance(vecOrigin, vecTarget);

	nSteps = iDistance / FIREBALL_SPEED;
	
	
	vecStep[0] = (vecTarget[0] - vecOrigin[0]) / nSteps;
	vecStep[1] = (vecTarget[1] - vecOrigin[1]) / nSteps;
	vecStep[2] = (vecTarget[2] - vecOrigin[2]) / nSteps;
	
	new iBonus = 0;
	if (g_iArrPlSpellLvls[iCasterID][FIREBALL] == BM_MAX_SPELL_LEVEL)
		iBonus = FIREBALL_BONUS;
	
	args[0]		= vecTarget[0];
	args[1]		= vecTarget[1];
	args[2]		= vecTarget[2];
	args[3]		= iCasterID;
	args[4]		= vecStep[0];
	args[5]		= vecStep[1];
	args[6]		= vecStep[2];
	args[7]		= vecOrigin[0];
	args[8]		= vecOrigin[1];
	args[9]		= vecOrigin[2];
	args[10]	= iBonus + FIREBALL_POWER + g_iArrPlSpellLvls[iCasterID][FIREBALL] * FIREBALL_INCREASE;

	#if defined DEBUG_SERVER_PRINT
		server_print("[DEBUG][BM] Fireball Power = %d", args[10]);
	#endif
	
	if ( file_exists("sound/battlemages/fireball.wav") )
		emit_sound(iCasterID, CHAN_ITEM, "battlemages/fireball.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
	
	set_task(0.1, "FireBallFly", FIREBALL_FLY_TASK_ID + iCasterID, args, 11);

	return PLUGIN_HANDLED;
}

//***************************************************************
//*	Function FireBallFly(args[])							*
//*	args[]:	Array with coordinates and id of the caster			*
//*												*
//*	Description:									*
//*	Runs everytime the fireball moves in space, does hit test and calls 	*
//*	the graphic.										*
//***************************************************************
public FireBallFly(args[])
{
	new vecTarget[3];
	vecTarget[0] = args[0];
	vecTarget[1] = args[1];
	vecTarget[2] = args[2];

	new iCasterID = args[3];

	new vecStep[3];
	vecStep[0] = args[4];
	vecStep[1] = args[5];
	vecStep[2] = args[6];

	new vecPos[3];
	vecPos[0] = args[7];
	vecPos[1] = args[8];
	vecPos[2] = args[9];

	vecPos[0] += vecStep[0];
	vecPos[1] += vecStep[1];
	vecPos[2] += vecStep[2];

	if ( get_distance(vecTarget, vecPos) < PLAYER_RADIUS )
	{
		Explosion(vecTarget, iCasterID, args[10]);
		return PLUGIN_CONTINUE;
	}

	new iArrPlayersID[32];
	new nPlayers;

	get_players(iArrPlayersID, nPlayers, "a");
	new vecPlayerPos[3];

	for (new iPlayer=0; iPlayer < nPlayers; iPlayer++)
	{
		if ( iArrPlayersID[iPlayer] != iCasterID )
		{
			get_user_origin(iArrPlayersID[iPlayer], vecPlayerPos);
			if ( get_distance(vecPlayerPos, vecPos) < PLAYER_RADIUS )
			{
				Explosion(vecPlayerPos, iCasterID, args[10]);
				return PLUGIN_CONTINUE;
			}
		}
	}

	message_begin( MSG_BROADCAST, SVC_TEMPENTITY );

	write_byte( TE_SPRITE );
	write_coord(vecPos[0] - ( vecStep[0] / 3 * 2 ));
	write_coord(vecPos[1] - ( vecStep[1] / 3 * 2 ));
	write_coord(vecPos[2] - ( vecStep[2] / 3 * 2 ));
	write_short( g_spriteBurning );
	write_byte( FIREBALL_SIZE );
	write_byte( 200 );
	message_end();

	message_begin( MSG_BROADCAST, SVC_TEMPENTITY );
	write_byte( TE_SPRITE );
	write_coord(vecPos[0] - ( vecStep[0] / 3 ));
	write_coord(vecPos[1] - ( vecStep[1] / 3 ));
	write_coord(vecPos[2] - ( vecStep[2] / 3 ));
	write_short( g_spriteBurning );
	write_byte( FIREBALL_SIZE );
	write_byte( 200 );
	message_end();

	message_begin( MSG_BROADCAST, SVC_TEMPENTITY );
	write_byte( TE_SPRITE );
	write_coord(vecPos[0]);
	write_coord(vecPos[1]);
	write_coord(vecPos[2]);
	write_short( g_spriteBurning );
	write_byte( FIREBALL_SIZE );
	write_byte( 200 );
	message_end();

	args[7] = vecPos[X_AXIS];
	args[8] = vecPos[Y_AXIS];
	args[9] = vecPos[Z_AXIS];

	set_task(0.1, "FireBallFly", FIREBALL_FLY_TASK_ID + iCasterID, args, 11);

	return PLUGIN_CONTINUE;
}


//***************************************************************
//*	Function Explosion(vecOrigin[], iCasterID, iDamage)			*
//*	vecOrigin[]:	Origin of the blast to be					*
//*	iCasterID:		The id of the player who triggered cast fireball	*
//*	iDamage:		The base damage we are to do on the players	*
//*												*
//*	Description:									*
//*	Sets up the graphic and damage tasks for the explosion			*
//***************************************************************
Explosion(vecOrigin[], iCasterID, iDamage)
{
	new args[6];
	
	args[0] = vecOrigin[X_AXIS];
	args[1] = vecOrigin[Y_AXIS];
	args[2] = vecOrigin[Z_AXIS];
	args[3] = iCasterID;
	args[4] = NUMBER_LOOPS;
	args[5] = iDamage;
	
	set_task(0.1, "Implosion", 1, vecOrigin, 3);
	set_task(0.2, "Explode", EXPLOSION_TASK_ID + iCasterID, args, 6);
	set_task(0.3, "BlastCircles", 2, vecOrigin, 3);
	
	return PLUGIN_CONTINUE;
}


//***************************************************************
//*	Function Explode(args[])							*
//*	args[]:	Array with caster id, blast position and base damage	*
//*												*
//*	Description:									*
//*	Shows some graphics and does damage to players near			*
//***************************************************************
public Explode(args[])
{
	new iCasterID = args[3];
	new vecBlastPos[3];
	vecBlastPos[X_AXIS] = args[0];
	vecBlastPos[Y_AXIS] = args[1];
	vecBlastPos[Z_AXIS] = args[2];
	
	message_begin( MSG_PVS, SVC_TEMPENTITY, vecBlastPos );
	write_byte( TE_EXPLOSION);
	write_coord( vecBlastPos[X_AXIS] + random_num( -100, 100 ));
	write_coord( vecBlastPos[Y_AXIS] + random_num( -100, 100 ));
	write_coord( vecBlastPos[Z_AXIS] + random_num( -50, 50 ));
	write_short( g_spriteExplosion );
	write_byte( random_num(0, 20) + 20  );
	write_byte( 12  );
	write_byte( TE_EXPLFLAG_NONE );
	message_end();

	message_begin( MSG_PVS, SVC_TEMPENTITY, vecBlastPos );
	write_byte( TE_SMOKE );
	write_coord( vecBlastPos[0] + random_num( -100, 100 ));
	write_coord( vecBlastPos[1] + random_num( -100, 100 ));
	write_coord( vecBlastPos[2] + random_num( -50, 50 ));
	write_short( g_spriteSmoke );
	write_byte( 60 );
	write_byte( 10  );
	message_end();

	new iArrPlayersID[MAX_PLAYERS];
	new nPlayers;

	get_players(iArrPlayersID, nPlayers, "a");

	new iTargetID;
	new iDistanceBetween;
	new vecTargetOrigin[3];
	new iDamage;
	new iMultiplier;

	// bmgeneralfunc.inc IsFriendlyFireOn function
	new bool:bFF = IsFriendlyFireOn();

	for (new iPlayer = 0; iPlayer < nPlayers; iPlayer++)
	{
		iTargetID = iArrPlayersID[iPlayer];
		get_user_origin(iTargetID, vecTargetOrigin);
		iDistanceBetween = get_distance(vecBlastPos, vecTargetOrigin);
		
		if ( get_user_team(iCasterID) != get_user_team(iTargetID) || iCasterID == iTargetID || bFF)
		{
			if ( iDistanceBetween < EXPLOSION_RANGE )
			{
				iMultiplier = args[5] * args[5] / EXPLOSION_RANGE;
				// bmgeneralfunc.inc SquareRoot function
				iDamage = SquareRoot((EXPLOSION_RANGE - iDistanceBetween) * iMultiplier);

				//	RadiusDamage(fVecOrigin, 1, 1);

				// bmgeneralfunc.inc DoDamage function
				DoDamage(iTargetID, iCasterID, iDamage, DMG_CAUSE_FIREBALL);
			}
		}
		if (iDistanceBetween < EXPLOSION_RANGE)
		{
			message_begin(MSG_ONE, g_iGameMsgScrShake, {0,0,0}, iTargetID);
			write_short( 1<<14 );	// Amplitude
			write_short( 1<<13 );	// Duration
			write_short( 1<<14 );	// Frequency
			message_end();
		}
	}

	if (--args[4] > 0)
		set_task(0.1, "Explode", EXPLOSION_TASK_ID + iCasterID, args, 6);
	
	return PLUGIN_CONTINUE;
}


//***************************************************************
//*	Function BlastCircles(vecBlastPos[VECTOR])				*
//*	vecBlastPos[VECTOR]:	Vector containing the blast position		*
//*												*
//*	Description:									*
//*	Shows the blast circles								*
//***************************************************************
public BlastCircles(vecBlastPos[VECTOR])
{
	// Blast Circles
	message_begin( MSG_PAS, SVC_TEMPENTITY, vecBlastPos );
	write_byte( TE_BEAMCYLINDER );
	write_coord( vecBlastPos[0]);
	write_coord( vecBlastPos[1]);
	write_coord( vecBlastPos[2] - 16);
	write_coord( vecBlastPos[0]);
	write_coord( vecBlastPos[1]);
	write_coord( vecBlastPos[2] - 16 + BLAST_RADIUS);
	write_short( g_spriteShockwave );
	write_byte( 0 );	// Startframe
	write_byte( 0 );	// Framerate
	write_byte( 6 );	// Life
	write_byte( 16 );	// Width
	write_byte( 0 );	// Noise
	write_byte( 188 );
	write_byte( 220 );
	write_byte( 255 );
	write_byte( 255 );	// Brightness
	write_byte( 0 );	// Speed
	message_end();

	message_begin( MSG_PAS, SVC_TEMPENTITY, vecBlastPos );
	write_byte( TE_BEAMCYLINDER );
	write_coord( vecBlastPos[0]);
	write_coord( vecBlastPos[1]);
	write_coord( vecBlastPos[2] - 16);
	write_coord( vecBlastPos[0]);
	write_coord( vecBlastPos[1]);
	write_coord( vecBlastPos[2] - 16 + BLAST_RADIUS / 2);
	write_short( g_spriteShockwave );
	write_byte( 0 );	// Startframe
	write_byte( 0 );	// Framerate
	write_byte( 6 );	// Life
	write_byte( 16 );	// Width
	write_byte( 0 );	// Noise
	write_byte( 188 );
	write_byte( 220 );
	write_byte( 255 );
	write_byte( 255 );	// Brightness
	write_byte( 0 );	// Speed
	message_end();

	return PLUGIN_CONTINUE;
}


//***************************************************************
//*	Function Implosion(vecImplosionPos[VECTOR])				*
//*	vecImplosionPos[VECTOR]:	Vector containing effect position	*
//*												*
//*	Description:									*
//*	Shows implosion graphics							*
//***************************************************************
public Implosion(vecImplosionPos[VECTOR])
{
	message_begin( MSG_BROADCAST, SVC_TEMPENTITY );
	write_byte( TE_IMPLOSION );
	write_coord(vecImplosionPos[0]);
	write_coord(vecImplosionPos[1]);
	write_coord(vecImplosionPos[2]);
	write_byte(100);
	write_byte(20);
	write_byte(5);
	message_end();

	return PLUGIN_CONTINUE;
}


//***************************************************************
//*	Function CastIceImplosion(iCasterID)					*
//*	iCasterID:	ID of the player who triggered the Ice implosion spell	*
//*												*
//*	Description:									*
//*	Casts the Ice Implosion spell							*
//***************************************************************
CastIceImplosion(iCasterID)
{
	new iTargetID, iBodyPartID;
	get_user_aiming(iCasterID, iTargetID, iBodyPartID);
	
	// bmgeneralfunc.inc IsFriendlyFireOn function
	new bool:bFF = IsFriendlyFireOn();
	if (!bFF)
		if ( get_user_team(iCasterID) == get_user_team(iTargetID) )
			return false;

	if ( iTargetID > 0 && iTargetID <= 32 )
	{
		new Float:fBonus = 1.0;
		// bmgeneralfuncs.inc IsMageMaster function
		if ( IsMageMaster(iCasterID, ICE_IMPLOSION) )
			fBonus = ICE_IMPLOSION_SPEEDBONUS;

		new args[4];
		args[0] = iCasterID;
		args[1] = iTargetID;
		args[2] = ICE_IMPLOSION_TURNS;
		args[3] = ICE_IMPLOSION_POWER + g_iArrPlSpellLvls[iCasterID][ICE_IMPLOSION] * ICE_IMPLOSION_INCREASE;
		
		if ( file_exists("sound/battlemages/coldwind.wav") )
		{
			emit_sound(iCasterID, CHAN_ITEM, "battlemages/coldwind.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
			emit_sound(iTargetID, CHAN_ITEM, "battlemages/coldwind.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
		}

		set_user_maxspeed(iTargetID, ICE_IMPLOSION_SLOW_SPEED / fBonus);
		g_bArrPlayersIsSlowed[iTargetID] = true;

		IceImplosionEffect(iTargetID);
		
		// bmgeneralfunc.inc DoDamage function
		DoDamage(iTargetID, iCasterID, args[3], DMG_CAUSE_ICE_IMPLOSION);
		
		set_task(ICE_IMPLOSION_INTERVAL, "IceImplosionDOT", ICE_IMPLOSION_DOT_TASK_ID + iTargetID, args, 4);
		return true;
	}
	return false;
}


//***************************************************************
//*	Function IceImplosionDOT(args[])						*
//*	args[]:	Array with the id of the caster and the target		*
//*												*
//*	Description:									*
//*	Task that does the damage for the Ice Implosion spell			*
//***************************************************************
public IceImplosionDOT(args[])
{
	new iCasterID = args[0];
	new iTargetID = args[1];
	
	if ( is_user_alive(iTargetID) )
	{
		IceImplosionEffect(iTargetID);
		
		// bmgeneralfunc.inc DoDamage function
		DoDamage(iTargetID, iCasterID, args[3], DMG_CAUSE_ICE_IMPLOSION);
		
		if (--args[2] > 0)
			set_task(ICE_IMPLOSION_INTERVAL, "IceImplosionDOT", ICE_IMPLOSION_DOT_TASK_ID + iTargetID, args, 4);
		else
		{
			if (g_bArrPlayersIsHasted[iTargetID])
				LightningSpeedCheckSet(iCasterID);
			else
			{
				// bmgeneralfuncs.inc ResetMaxSpeed function
				ResetMaxSpeed(iTargetID);
			}
		}
	}
	else
	{
		// bmgeneralfuncs.inc ResetMaxSpeed function
		ResetMaxSpeed(iTargetID);
	}
	
	return PLUGIN_CONTINUE;
}


//***************************************************************
//*	Function IceImplosionEffect(iTargetID)					*
//*	iTargetID:	The id of the target player					*
//*												*
//*	Description:									*
//*	Shows the Ice Implosion effect around the victim			*
//***************************************************************
IceImplosionEffect(iTargetID)
{
	new vecImplosionPos[3];
	get_user_origin(iTargetID, vecImplosionPos);
	message_begin( MSG_BROADCAST, SVC_TEMPENTITY );
	write_byte( TE_IMPLOSION );
	write_coord(vecImplosionPos[0]);
	write_coord(vecImplosionPos[1]);
	write_coord(vecImplosionPos[2]);
	write_byte(100);
	write_byte(20);
	write_byte(5);
	message_end();
}


//***************************************************************
//*	Function CastMagmaShield(iCasterID)					*
//*	iCasterID:	ID of the player who triggered the Magma Shield spell	*
//*												*
//*	Description:									*
//*	Shows the Ice Implosion effect around the victim			*
//***************************************************************
CastMagmaShield(iCasterID)
{
	set_user_health(iCasterID, get_user_health(iCasterID) + ABNORMAL_HEALTH);
	g_bArrPlayersHasMagmaShield[iCasterID] = true;
	g_iArrPlayersMaxHP[iCasterID] += ABNORMAL_HEALTH;
	
	if ( file_exists("sound/battlemages/magma.wav") )
		emit_sound(iCasterID, CHAN_ITEM, "battlemages/magma.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);

	// Adds a red aura indicating that this player is using Magma shield.
	set_user_rendering(iCasterID, kRenderFxGlowShell, 255, 25, 25, kRenderTransAlpha, 255);
}


//***************************************************************
//*	Function AttackOnMagmaShield(iCasterID, iAttackerID, iDamage,	*
//*						iBodyPartID, iWeaponID)		*
//*	iCasterID:		ID of the player with the Magma Shield spell	*
//*	iAttackerID:	ID of the player who attacked				*
//*	iDamage:		The damage that was done				*
//*	iBodyPartID:	The part on the body that was hit			*
//*	iWeaponID:	ID of the weapon which the attacker was using	*
//*												*
//*	Description:									*
//*	Reducing the damage depending on the damage mitigation and		*
//*	returns a defined damage to the attacker					*
//***************************************************************
AttackOnMagmaShield(iCasterID, iAttackerID, iDamage, iBodyPartID, iWeaponID)
{
	new Float:fBonus = 0.0
	if (g_iArrPlSpellLvls[iCasterID][MAGMA_SHIELD] == BM_MAX_SPELL_LEVEL)
		fBonus = MAGMA_SHIELD_BONUS;
	
	new Float:fMagmaPower = fBonus + MAGMA_SHIELD_POWER + float(g_iArrPlSpellLvls[iCasterID][MAGMA_SHIELD] * MAGMA_SHIELD_INCREASE) / 100.0;
	new iNewHealth = get_user_health(iCasterID) + floatround( float(iDamage) * fMagmaPower ) // Reduce damage
	set_user_health(iCasterID, iNewHealth);

	if ( iCasterID != iAttackerID )
	{
		fMagmaPower = fBonus + MAGMA_SHIELD_MIRROR_DMG + float(g_iArrPlSpellLvls[iCasterID][MAGMA_SHIELD] * MAGMA_SHIELD_INCREASE) / 100.0;
		DoDamage(iAttackerID, iCasterID, floatround( float(iDamage) * fMagmaPower ), DMG_CAUSE_MAGMA_SHIELD);
	}
	if ( get_user_health(iCasterID) < ABNORMAL_HEALTH + 1 )
	{
		new iHeadShot = 0;
		if ( iBodyPartID == HIT_HEAD )
			iHeadShot = 1;
			
		DoDamage(iCasterID, iAttackerID, ABNORMAL_HEALTH + 1, iWeaponID, true, iHeadShot);
	}
}


//***************************************************************
//*	Function CastEnchantWeapon(iCasterID)					*
//*	iCasterID:	ID of the player who triggered the Enchant Weapon	*
//*			spell									*
//*												*
//*	Description:									*
//*	Casts enchant weapon on the caster						*
//***************************************************************
CastEnchantWeapon(iCasterID)
{
	if ( file_exists("sound/battlemages/enchant.wav") )
		emit_sound(iCasterID, CHAN_ITEM, "battlemages/enchant.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);

	g_bArrPlayersHasEnchantedWeapon[iCasterID] = true;
}


//***************************************************************
//*	Function EnchantWeaponDmgAndSetBurn(iCasterID, iVictimID)	*
//*	iCasterID:	ID of the player who triggered the Enchant Weapon	*
//*			spell									*
//*	iVictimID:	ID of the victim who the caster shot at with enchanted	*
//*			bullets								*
//*												*
//*	Description:									*
//*	Adds the enchanted weapon damage and burn dot to the victim		*
//***************************************************************
EnchantWeaponDmgAndSetBurn(iCasterID, iVictimID)
{
	if ( IsMageMaster(iCasterID, ENCHANT_WEAPON) )
	{
		if ( random_float(0.0, 1.0) < ENCHANT_WEAPON_FIRE_BURNCHANCE )
		{
			new args[2];
			args[0] = iCasterID;
			args[1] = iVictimID;

			#if defined DEBUG_SERVER_PRINT
				server_print("[DEBUG][BM] Burning player (%d)", iVictimID);
			#endif
			set_task(0.1, "EnchantWeaponBurnDamage", ENCHANT_WEAPON_BURN_TASK_ID + iVictimID, args, 2);
		}
		// bmgeneralfuncs.inc DoDamage Function
		DoDamage(iVictimID, iCasterID, ENCHANT_WEAPON_FIRE_POWER, DMG_CAUSE_ENCHANTED_WEAPON);
	}
	else
	{
		// bmgeneralfuncs.inc DoDamage Function
		DoDamage(iVictimID, iCasterID,
			g_iArrPlSpellLvls[iCasterID][ENCHANT_WEAPON] * ENCHANT_WEAPON_INCREASE + ENCHANT_WEAPON_POWER, DMG_CAUSE_ENCHANTED_WEAPON);
	}
}


//***************************************************************
//*	Function EnchantWeaponBurnDamage(args[])				*
//*	args[]:	Array with the id of the caster and the victim		*
//*												*
//*	Description:									*
//*	Task that does the burn DoT damage to a victim				*
//***************************************************************
public EnchantWeaponBurnDamage(args[])
{
	new iCasterID = args[0];
	new iVictimID = args[1];
	if ( is_user_alive(iVictimID) )
	{
		// bmgeneralfuncs.inc
		SendGameMessageFade(iVictimID, 255, 0, 0, ENCHANT_WEAPON_FIRE_GLOW);
  
		// bmgeneralfuncs.inc DoDamage Function
		DoDamage(iVictimID, iCasterID, ENCHANT_WEAPON_FIRE_DOT, DMG_CAUSE_ENCHANTED_WEAPON_BURN);
		set_task(ENCHANT_WEAPON_FIRE_INTERVAL, "EnchantWeaponBurnDamage", ENCHANT_WEAPON_BURN_TASK_ID + iVictimID, args, 2);
	}
}


//***************************************************************
//*	Function CastStoneShield(iCasterID)					*
//*	iCasterID:	ID of the player who triggered the Stone Shield spell	*
//*												*
//*	Description:									*
//*	Casts StoneShield on the spot where the caster is aiming at		*
//***************************************************************
CastStoneShield(iCasterID)
{
	if ( task_exists(STONESHIELD_TASK_ID + iCasterID) )	// If the caster still has a stone wall on the map, then he cant create another
		return BM_FAILED;

	new entityStoneWall = create_entity("info_target");
	if (entityStoneWall == WASNT_CREATED)
		return BM_UNSTABLE;

	if ( file_exists("sound/battlemages/stoneshield.wav") )
		emit_sound(iCasterID, CHAN_ITEM, "battlemages/stoneshield.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);

	new Float:fStoneWallHealth		= STONESHIELD_POWER + float(g_iArrPlSpellLvls[iCasterID][STONESHIELD]) * STONESHIELD_HEALTH_INCREASE;
	new Float:fStoneWallDuration	= STONESHIELD_DURATION + float(g_iArrPlSpellLvls[iCasterID][STONESHIELD]) * STONESHIELD_DURATION_INCREASE;

	new Float:fVecAngle[VECTOR];
	new Float:fVecAim[VECTOR];
	new vecAim[VECTOR];
	get_user_origin(iCasterID, vecAim, GET_AIM_ENTITY_HIT);
	
	fVecAim[X_AXIS] = float(vecAim[X_AXIS]);
	fVecAim[Y_AXIS] = float(vecAim[Y_AXIS]);
	fVecAim[Z_AXIS] = float(vecAim[Z_AXIS]) + STONESHIELD_ZAXIS_MODIFIER;
	
	entity_set_string(entityStoneWall, EV_SZ_classname, "StoneWall");
	entity_set_model(entityStoneWall, "models/battlemages/stonewall.mdl");
	entity_set_float(entityStoneWall, EV_FL_health, fStoneWallHealth); 
	entity_set_float(entityStoneWall, EV_FL_takedamage, STONESHIELD_TAKEDAMAGE);
	entity_set_float(entityStoneWall, EV_FL_dmg_take, STONESHIELD_DAMAGE_TAKE);
	//entity_set_float(entityStoneWall, EV_FL_armorvalue, fStoneWallHealth);
	entity_set_int(entityStoneWall, EV_INT_solid, SOLID_BBOX);
	entity_set_int(entityStoneWall, EV_INT_movetype, MOVETYPE_FLY);
	entity_set_origin(entityStoneWall, fVecAim);
	
	if ( g_iArrPlSpellLvls[iCasterID][STONESHIELD] == BM_MAX_SPELL_LEVEL )
		entity_set_edict(entityStoneWall, EV_ENT_owner, iCasterID);			// If master, then caster can move and shoot through wall
	else
		entity_set_edict(entityStoneWall, EV_ENT_owner, ENTITY_NO_OWNER);	// if apprentice it will only protect him.

	entity_get_vector(iCasterID, EV_VEC_v_angle, fVecAngle);
	fVecAngle[X_AXIS] = 0.0;
	// entity_set_vector(entityStoneWall, EV_VEC_angles, fVecAngle);

	// Calculate the size depending on the casters angle in Y-axis
	if (fVecAngle[Y_AXIS] < 0.0) // Because the model looks exactly the same on both sides, we can ignore the negative angle.
		fVecAngle[Y_AXIS] *= (-1.0);
	// new Float:fPoint[POINT], Float:fPointModValue[POINT];
	
	// fPoint[X_AXIS] = floatcos(fVecAngle[Y_AXIS]) * STONESHIELD_RADIUS;
	// fPoint[Y_AXIS] = floatsin(fVecAngle[Y_AXIS]) * STONESHIELD_RADIUS;
	
	// fPointModValue[X_AXIS] = floatcos(fVecAngle[Y_AXIS]) * STONESHIELD_WIDTH;
	// fPointModValue[Y_AXIS] = ( floatsin(fVecAngle[Y_AXIS]) * STONESHIELD_WIDTH ) * (-1.0);
	
	// new Float:fVecSizeCoordOne[VECTOR],	Float:fVecSizeCoordTwo[VECTOR];

	// fVecSizeCoordOne[X_AXIS] = fPoint[X_AXIS] + fPointModValue[X_AXIS];
	// fVecSizeCoordTwo[X_AXIS] = (fPoint[X_AXIS] * (-1.0)) - fPointModValue[X_AXIS];

	// fVecSizeCoordOne[Y_AXIS] = fPoint[Y_AXIS] + fPointModValue[Y_AXIS];
	// fVecSizeCoordTwo[Y_AXIS] = (fPoint[Y_AXIS] * (-1.0)) - fPointModValue[Y_AXIS];

	// fVecSizeCoordOne[Z_AXIS] = -STONESHIELD_HEIGHT;
	// fVecSizeCoordTwo[Z_AXIS] = STONESHIELD_HEIGHT;
	
	// entity_set_vector(entityStoneWall, EV_VEC_startpos, fVecSizeCoordOne);
	// entity_set_vector(entityStoneWall, EV_VEC_endpos, fVecSizeCoordTwo);
	
	// Setting the models angle in the world works fine, but setting the hit box angle???
	if ( ( (fVecAngle[Y_AXIS] >= 45.0) && (fVecAngle[Y_AXIS] <= 135.0) ) )
	{
		fVecAngle[Y_AXIS] = 90.0;
		entity_set_size(entityStoneWall,
			Float:{-STONESHIELD_RADIUS,-STONESHIELD_WIDTH,-STONESHIELD_HEIGHT},
			Float:{STONESHIELD_RADIUS,STONESHIELD_WIDTH,STONESHIELD_HEIGHT});
	}
	else
	{
		fVecAngle[Y_AXIS] = 0.0;
		entity_set_size(entityStoneWall,
			Float:{-STONESHIELD_WIDTH,-STONESHIELD_RADIUS,-STONESHIELD_HEIGHT},
			Float:{STONESHIELD_WIDTH,STONESHIELD_RADIUS,STONESHIELD_HEIGHT});
	}
	entity_set_vector(entityStoneWall, EV_VEC_angles, fVecAngle);
	g_entArrStoneWall[iCasterID] = entityStoneWall;
	new args[1];
	args[0] = iCasterID;
	
	set_task(fStoneWallDuration, "RemoveWall", STONESHIELD_TASK_ID + iCasterID, args, 1);
	return BM_OK;
}


//***************************************************************
//*	Function RemoveWall(args[])							*
//*	args[]:	Contains the id of the caster only				*
//*												*
//*	Description:									*
//*	Task that removes the Stone Shield						*
//***************************************************************
public RemoveWall(args[])
{
	new iCasterID = args[0];
	remove_entity(g_entArrStoneWall[iCasterID]);
}

#endif
