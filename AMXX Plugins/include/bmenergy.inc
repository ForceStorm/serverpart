//***************************************************************
//*	Battle Mages v. 1.0.1 for AMXX by Martin J. Van der Cal		*
//*												*
//*	Mod adding magic to the game.							*
//*												*
//*	Copyright (C) 2004, Martin J. Van der Cal					*
//*	This plugin is under the GNU General Public License, read		*
//*	"Battle Mages Readme.doc" for further information.			*
//*												*
//*	bmenergy.inc									*
//*												*
//***************************************************************

#if !defined BATTLE_MAGES_ENERGY_MAGIC
#define BATTLE_MAGES_ENERGY_MAGIC

#include "bmconst"
#include "bmgvars"
#include "bmgeneralfuncs"


//***************************************************************
//*	Function CastLightningStrike(iCasterID)					*
//*	iCasterID:	The id of the player who triggered cast Lightning		*
//*			Strike								*
//*												*
//*	Description:									*
//*	Casts a Lightning Strike towards the place where the caster aimed	*
//***************************************************************
CastLightningStrike(iCasterID)
{
	// bmgeneralfunc.inc IsFriendlyFireOn function
	new bool:bFF = IsFriendlyFireOn();

	new iEnemyID, iBodyPart;
	new iLinewidth = 50;
	new vecTarget[VECTOR];
	new vecCaster[VECTOR];
	new iDamage = LIGHTNING_STRIKE_POWER + g_iArrPlSpellLvls[iCasterID][LIGHTNING_STRIKE] * LIGHTNING_STRIKE_POWER_INCREASE;

	get_user_origin(iCasterID, vecCaster);
	get_user_aiming(iCasterID, iEnemyID, iBodyPart);

	// if aimed on a player...
	if ( iEnemyID > 0 && iEnemyID <= 32 )
	{
		if (!bFF)
			if ( get_user_team(iCasterID) == get_user_team(iEnemyID) )
				iDamage = 0;	// if he is a friend and mp_friendlyfire is set to 0. Then we wont do any damage

		get_user_origin(iEnemyID, vecTarget);

		// bmgeneralfunc.inc DoDamage function
		DoDamage(iEnemyID, iCasterID, iDamage, DMG_CAUSE_LIGHTNING_STRIKE);
	}
	else
	{
		get_user_origin(iCasterID, vecTarget, GET_AIM_ENTITY_HIT);
	}
	LightningEffect(vecCaster, vecTarget, iLinewidth, iCasterID);

	if ( g_iArrPlSpellLvls[iCasterID][LIGHTNING_STRIKE] == BM_MAX_SPELL_LEVEL )
	{
		new args[38];
		args[0] = vecTarget[X_AXIS];
		args[1] = vecTarget[Y_AXIS];
		args[2] = vecTarget[Z_AXIS];
		args[3] = iCasterID;
		args[4] = iDamage / CHAIN_LIGHTNING_POWER;
		args[5] = iLinewidth;
		args[7] = iCasterID;
		if ( iEnemyID > 0 && iEnemyID <= 32 )
		{
			args[6] = 2;
			args[8] = iEnemyID;
		}
		else
			args[6] = 1;

		set_task(CHAIN_LIGHTNING_JUMPDELAY, "ChainLightning", CHAIN_LIGHTNING_TASK_ID + iCasterID, args, 39);
	}
}

//***************************************************************
//*	Function ChainLightning(args[])						*
//*	args[]:	Contains a list of following variables				*
//*	0-2:		Vector with the target position					*
//*	3		ID of the player who cast the spell				*
//*	4		The damage the chain lightning will cause on hits		*
//*	5		Width of the lightning effect					*
//*	6		Number of players who have already been hit		*
//*	7-38		A list with players that have been hit				*
//*												*
//*	Description:									*
//*	Checks if a player is near someone who has been hit by lightning and	*
//*	casts the chain lightning on that player					*
//***************************************************************
public ChainLightning(args[])
{
	new bool:bFF = IsFriendlyFireOn();

	new vecTarget[VECTOR], vecOrigin[VECTOR];
	vecTarget[X_AXIS] = args[0];
	vecTarget[Y_AXIS] = args[1];
	vecTarget[Z_AXIS] = args[2];
	new iCasterID = args[3];
	new iDamage = args[4];
	new iLinewidth = args[5];

	new iArrPlayersID[MAX_PLAYERS];
	new nPlayers, iEnemyID = 0, iClosestDistance = CHAIN_LIGHTNING_RADIUS + 1;
	
	get_players(iArrPlayersID, nPlayers, "a");
	new iPlayerHit;
	new bool:bIsPlayerAlreadyHit = false;
	
	// Here we will loop through all the players and check who are the closest player as well as checking if they have already been hit
	for(new iPlayer = 0; iPlayer < nPlayers; iPlayer++)
	{
		get_user_origin(iArrPlayersID[iPlayer], vecOrigin);
		if ( iClosestDistance > get_distance(vecTarget, vecOrigin) )
		{
			for(iPlayerHit = 7; iPlayerHit < 7 + args[6]; iPlayerHit++)
				if (args[iPlayerHit] == iArrPlayersID[iPlayer])
					bIsPlayerAlreadyHit = true;
			
			// if we havent hit the player before, then we will set that player as the closest player
			if ( !bIsPlayerAlreadyHit )
			{
				iClosestDistance = get_distance(vecTarget, vecOrigin);
				iEnemyID = iArrPlayersID[iPlayer];
			}
			
			// Resets whether the player is already hit since now its time to check another player
			bIsPlayerAlreadyHit = false;
		}
	}
	
	if ( iEnemyID > 0 )
	{
		if (!bFF)
			if ( get_user_team(iCasterID) == get_user_team(iEnemyID) )
				iDamage = 0;	// if he is a friend and mp_friendlyfire is set to 0. Then we wont do any damage
	
		vecOrigin[X_AXIS] = vecTarget[X_AXIS];
		vecOrigin[Y_AXIS] = vecTarget[Y_AXIS];
		vecOrigin[Z_AXIS] = vecTarget[Z_AXIS];
		get_user_origin(iEnemyID, vecTarget);
		
		// Updating variables that will go forth to next check
		args[0] = vecTarget[X_AXIS];
		args[1] = vecTarget[Y_AXIS];
		args[2] = vecTarget[Z_AXIS];
		args[7 + args[6]] = iEnemyID;	// Adding a player last in the already hit list
		args[6]++;	// Added one player to the list with players already hit

		// bmgeneralfunc.inc DoDamage function
		DoDamage(iEnemyID, iCasterID, iDamage, DMG_CAUSE_CHAIN_LIGHTNING);
		LightningEffect(vecOrigin, vecTarget, iLinewidth, iEnemyID);

		// If we hit someone then we want to continue the chain lightning
		set_task(CHAIN_LIGHTNING_JUMPDELAY, "ChainLightning", CHAIN_LIGHTNING_TASK_ID + iCasterID, args, 39);
	}
}


//***************************************************************
//*	Function LightningEffect(vecOrigin[], vecTarget[], iLinewidth,	*
//*					iTargetID)						*
//*	vecOrigin:	The start position of the lightning				*
//*	vecTarget:	The end position of the lightning				*
//*	iLinewidth:	The width of the lightning					*
//*	iTargetID:	The one we will emit sound for					*
//*												*
//*	Description:									*
//*	Creates the visual and sound effect for lightning				*
//***************************************************************
public LightningEffect(vecOrigin[], vecTarget[], iLinewidth, iTargetID)
{
	message_begin( MSG_BROADCAST, SVC_TEMPENTITY );
	write_byte( TE_BEAMPOINTS );

	// Start Position
	write_coord(vecOrigin[0]);
	write_coord(vecOrigin[1]);
	write_coord(vecOrigin[2]);

	// End Position
	write_coord(vecTarget[0]);
	write_coord(vecTarget[1]);
	write_coord(vecTarget[2]);

	write_short( g_spriteLightning );
	write_byte( 0 );			// Starting frame
	write_byte( 15 );			// Frame rate
	write_byte( 5 );			// Life
	write_byte( iLinewidth );	// Line width
	write_byte( 10 );			// Noise amplitude
	write_byte( 0 );			// Red color
	write_byte( 0 );			// Green color
	write_byte( 255 );			// Blue color
	write_byte( 255 );			// Brightness
	write_byte( 0 );			// Scroll speed
	message_end();

	if ( file_exists("sound/battlemages/lightningbolt.wav") )
		emit_sound(iTargetID, CHAN_ITEM, "battlemages/lightningbolt.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);

	return PLUGIN_CONTINUE;
}


//***************************************************************
//*	Function CastLightningSpeed(iCasterID)					*
//*	iCasterID:	The id of the player who triggered cast Lightning		*
//*			Speed								*
//*												*
//*	Returns:	true if we successfully cast the spell, false otherwise	*
//*												*
//*	Description:									*
//*	Casts Lightning Speed on the caster						*
//***************************************************************
bool:CastLightningSpeed(iCasterID)
{
	// if we arent slowed or already hasted.
	if ( !g_bArrPlayersIsSlowed[iCasterID] && !g_bArrPlayersIsHasted[iCasterID] && !g_bIsFreezeTime )
	{
		new Float:fDuration = LIGHTNING_SPEED_DURATION;
		
		// bmgeneralfuncs.inc IsMageMaster function - Checks if a mage is master of a spell specified
		if ( IsMageMaster(iCasterID, LIGHTNING_SPEED) )
			fDuration += LIGHTNING_SPEED_BONUS_DURATION;
	
		g_bArrPlayersIsHasted[iCasterID] = true;
		LightningSpeedCheckSet(iCasterID);
	
		new args[1];
		args[0] = iCasterID;
	
		if ( file_exists("sound/battlemages/thunder_speed.wav") )
			emit_sound(iCasterID, CHAN_ITEM, "battlemages/thunder_speed.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);

		ShowDurationBar(iCasterID, floatround(fDuration));
	
		set_task(fDuration, "EndLightningSpeed", LIGHTNING_SPEED_TASK_ID + iCasterID, args, 1);
		return true;
	}
	return false;
}


//***************************************************************
//*	Function LightningSpeedCheckSet(iCasterID)				*
//*	iCasterID:	The id of the player who triggered cast Lightning		*
//*			Speed								*
//*												*
//*	Description:									*
//*	Checks if the player has Lightning Speed active, and sets the max	*
//*	speed if it is.									*
//***************************************************************
LightningSpeedCheckSet(iCasterID)
{
	if ( g_bArrPlayersIsHasted[iCasterID] )
		set_user_maxspeed(iCasterID, LIGHTNING_SPEED_POWER + float(g_iArrPlSpellLvls[iCasterID][LIGHTNING_SPEED]) * LIGHTNING_SPEED_INCREASE);
}


//***************************************************************
//*	Function EndLightningSpeed(args[])						*
//*	args[]:	Contains one variable, the id of the caster			*
//*												*
//*	Description:									*
//*	Ends the lightning speed								*
//***************************************************************
public EndLightningSpeed(args[])
{
	new iCasterID = args[0];

	if ( !g_bArrPlayersIsSlowed[iCasterID] )
	{
		// bmgeneralfuncs.inc ResetMaxSpeed function
		ResetMaxSpeed(iCasterID);
	}
	else
		g_bArrPlayersIsHasted[iCasterID] = false;

	return PLUGIN_CONTINUE;
}


//***************************************************************
//*	Function CastPlanarArmor(iCasterID)					*
//*	iCasterID:	The id of the player who triggered cast Planar Armor	*
//*												*
//*	Description:									*
//*	Casts Planar Armor on the caster						*
//***************************************************************
bool:CastPlanarArmor(iCasterID)
{
	if ( get_user_armor(iCasterID) <= 100 && ! g_bArrPlayersHasGhostArmor[iCasterID] )
	{
		g_bArrPlayersHasPlanarArmor[iCasterID] = true;
		GrantPlanarArmor(iCasterID);	

		if ( file_exists("sound/battlemages/planar_armor.wav") )
			emit_sound(iCasterID, CHAN_ITEM, "battlemages/planar_armor.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);

		if ( IsMageMaster(iCasterID, PLANAR_ARMOR) )
		{
			AddGhostArmor(iCasterID);
		}
		else
		{
			// Adds a blue aura indicating that this player is using Planar armor
			set_user_rendering(iCasterID, kRenderFxGlowShell, 25, 25, 255, kRenderTransAlpha, 255);
		}
		
		return true;
	}
	return false;
}


//***************************************************************
//*	Function GrantPlanarArmor(iCasterID)					*
//*	iCasterID:	The id of the player who triggered cast Planar Armor	*
//*												*
//*	Description:									*
//*	Adds assault suit and extra armor to the caster				*
//***************************************************************
GrantPlanarArmor(iCasterID)
{
	give_item(iCasterID, "item_assaultsuit");	// Full suite including helm
	set_user_armor(iCasterID, PLANAR_ARMOR_POWER + g_iArrPlSpellLvls[iCasterID][PLANAR_ARMOR] * PLANAR_ARMOR_INCREASE);
}


//***************************************************************
//*	Function AddGhostArmor(iCasterID)						*
//*	iCasterID:	The id of the player who triggered cast Ghost Armor	*
//*												*
//*	Description:									*
//*	Adds Ghost Armor on the caster						*
//***************************************************************
AddGhostArmor(iCasterID)
{
	set_user_rendering(iCasterID, kRenderFxGlowShell, 0,0,150, kRenderTransTexture, GHOST_ARMOR_EFFECT);
	
	set_user_health(iCasterID, get_user_health(iCasterID) + ABNORMAL_HEALTH);
	g_bArrPlayersHasGhostArmor[iCasterID] = true;
	g_iArrPlayersMaxHP[iCasterID] += ABNORMAL_HEALTH;
}


//***************************************************************
//*	Function AttackOnGhostArmor(iCasterID, iAttackerID, iDamage,	*
//*						iBodyPartID, iWeaponID)		*
//*	iCasterID:		The id of the player who have ghost armor		*
//*	iAttackerID:	The id of the player who attacked			*
//*	iDamage:		The amount of damage the attacker did		*
//*	iBodyPartID:	The body part the attacker hit				*
//*	iWeaponID:	The id of the weapon the attack was using		*
//*												*
//*	Description:									*
//*	Makes the ghost armor affect the gameplay.				*
//***************************************************************
AttackOnGhostArmor(iCasterID, iAttackerID, iDamage, iBodyPartID, iWeaponID)
{
	if ( random_float(0.0, 1.0) < GHOST_ARMOR_APPARATION )
	{
		new iNewHealth = get_user_health(iCasterID) + iDamage;
		set_user_health(iCasterID, iNewHealth);
	}

	if ( get_user_health(iCasterID) < ABNORMAL_HEALTH + 1 )
	{
		new iHeadShot = 0;
		if ( iBodyPartID == HIT_HEAD )
			iHeadShot = 1;
			
		DoDamage(iCasterID, iAttackerID, ABNORMAL_HEALTH + 1, iWeaponID, true, iHeadShot);
	}
}


//***************************************************************
//*	Function CastLeech(iCasterID)						*
//*	iCasterID:	The id of the player who triggered cast Leech		*
//*												*
//*	Returns:	BM codes, see bmconst.inc for more details			*
//*												*
//*	Description:									*
//*	Casts Leech on the caster							*
//***************************************************************
CastLeech(iCasterID)
{
	if ( IsMageMaster(iCasterID, LEECH) && task_exists(AURA_OF_LEECH_TASK_ID + iCasterID) )
		return BM_ALREADY_ACTIVE;
	else if ( IsMageMaster(iCasterID, LEECH) && ! task_exists(AURA_OF_LEECH_TASK_ID + iCasterID) )
		CastAuraOfLeech(iCasterID);
	else
	{
		new vecCasterOrigin[VECTOR];
		new sCastersTeam[MAX_TEAM_SIZE];
		get_user_team(iCasterID, sCastersTeam, MAX_TEAM_SIZE-1);
		get_user_origin(iCasterID, vecCasterOrigin);
		
		new iEnemyID;
		if ( sCastersTeam[0] == 'C' )
			iEnemyID = GetClosestPlayer(vecCasterOrigin, LEECH_RADIUS, true, "TERRORIST");
		else
			iEnemyID = GetClosestPlayer(vecCasterOrigin, LEECH_RADIUS, true, "CT");
			
		if ( iEnemyID > 0 )
		{
			new iDamage = LEECH_POWER + g_iArrPlSpellLvls[iCasterID][LEECH] * LEECH_INCREASE;
			DoDamage(iEnemyID, iCasterID, iDamage, DMG_CAUSE_LEECH);
			IncreaseUserHealth(iCasterID, iDamage);

			// bmgeneralfuncs.inc
			SendGameMessageFade(iCasterID, 0, 255, 0, LEECH_GLOW);

			if ( file_exists("sound/battlemages/leech.wav") )
				emit_sound(iCasterID, CHAN_ITEM, "battlemages/leech.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
		}
		else
			return BM_FAILED;
	}
	return BM_OK;
}


//***************************************************************
//*	Function CastAuraOfLeech(iCasterID)					*
//*	iCasterID:	The id of the player who triggered cast Aura of Leech	*
//*												*
//*	Description:									*
//*	Casts Aura of Leech on the caster						*
//***************************************************************
CastAuraOfLeech(iCasterID)
{
	if ( file_exists("sound/battlemages/leech.wav") )
		emit_sound(iCasterID, CHAN_ITEM, "battlemages/leech.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
	
	new args[1];
	args[0] = iCasterID;
	LeechPlayer(args);
}


//***************************************************************
//*	Function LeechPlayer(args[])							*
//*	args[]:	Only contains one variable, the id of the caster		*
//*												*
//*	Description:									*
//*	Checks if a player is near and leeches that player				*
//***************************************************************
public LeechPlayer(args[])
{
	new iCasterID = args[0];
	if ( g_iArrPlayersMP[iCasterID] >= AURA_OF_LEECH_INTERVAL_COST && is_user_alive(iCasterID) )
	{
		new vecCasterOrigin[VECTOR];
		new sCastersTeam[MAX_TEAM_SIZE];
		get_user_team(iCasterID, sCastersTeam, MAX_TEAM_SIZE-1);
		get_user_origin(iCasterID, vecCasterOrigin);
		
		new iEnemyID;
		if ( sCastersTeam[0] == 'C' )
			iEnemyID = GetClosestPlayer(vecCasterOrigin, LEECH_RADIUS, true, "TERRORIST");
		else
			iEnemyID = GetClosestPlayer(vecCasterOrigin, LEECH_RADIUS, true, "CT");

		if ( iEnemyID > 0 )
		{
			g_iArrPlayersMP[iCasterID] -= AURA_OF_LEECH_INTERVAL_COST;
			
			// bmgeneralfuncs.inc DoDamage function
			DoDamage(iEnemyID, iCasterID, AURA_OF_LEECH_POWER, DMG_CAUSE_AURA_OF_LEECH);
			
			IncreaseUserHealth(iCasterID, AURA_OF_LEECH_POWER);
			
			// bmgeneralfuncs.inc
			SendGameMessageFade(iCasterID, 0, 255, 0, AURA_OF_LEECH_GLOW);
			
			DisplayPlayerInfo(iCasterID);
		}
		set_task(AURA_OF_LEECH_INTERVALS, "LeechPlayer", AURA_OF_LEECH_TASK_ID + iCasterID, args, 1);
	}
}


//***************************************************************
//*	Function CastForceField(iCasterID)						*
//*	iCasterID:	The id of the player who triggered cast Force Field	*
//*												*
//*	Description:									*
//*	Casts Force Field on an area with the casters target point as center	*
//***************************************************************
CastForceField(iCasterID)
{
	if ( !task_exists(FORCE_FIELD_TASK_ID + iCasterID) )
	{
		if ( file_exists("sound/battlemages/force_field.wav") )
			emit_sound(iCasterID, CHAN_ITEM, "battlemages/force_field.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
		
		new vecTarget[VECTOR], iTargetID, iBody;
		get_user_aiming(iCasterID, iTargetID, iBody);
		
		if ( iTargetID > 0 )
		{
			get_user_origin(iTargetID, vecTarget);
			vecTarget[Z_AXIS] -= PLAYER_CENTER_Z_OFFSET;
		}
		else
			get_user_origin(iCasterID, vecTarget, GET_AIM_ENTITY_HIT);
		
		new args[7];
		args[0] = iCasterID;
		args[1] = FORCE_FIELD_RADIUS + g_iArrPlSpellLvls[iCasterID][FORCE_FIELD] * FORCE_FIELD_RADIUS_INCREASE;
		args[2] = FORCE_FIELD_POWER + g_iArrPlSpellLvls[iCasterID][FORCE_FIELD] / FORCE_FIELD_POWER_INCREASE;
		
		if ( IsMageMaster(iCasterID, FORCE_FIELD) )
			args[3] = FORCE_FIELD_NUM_INTERVALS	+ FORCE_FIELD_DURATION_BONUS;
		else
			args[3] = FORCE_FIELD_NUM_INTERVALS;
		
		args[4] = vecTarget[X_AXIS];
		args[5] = vecTarget[Y_AXIS];
		args[6] = vecTarget[Z_AXIS];
		
		set_task(FORCE_FIELD_INTERVAL, "TaskForceField", FORCE_FIELD_TASK_ID + iCasterID, args, 7);
		
		return BM_OK;
	}
	return BM_ALREADY_ACTIVE;
}


//***************************************************************
//*	Function TaskForceField(args[])						*
//*	args[]:	Contains 7 variables						*
//*	0:		The id of the caster who triggered the spell			*
//*	1:		The radius of the Force Field					*
//*	2:		The damage the Force Field will do to players within	*
//*	3:		The amount of intervals the Force Field will stay		*
//*	4-6		Center point of the Force Field					*
//*												*
//*	Description:									*
//*	Checks if players are withing the Force Field and will do damage to	*
//*	them. Will also generate Force Field effects.				*
//***************************************************************
public TaskForceField(args[])
{
	new iCasterID = args[0];
	if ( is_user_alive(iCasterID) )
	{
		new iRadius = args[1];
		new iDamage = args[2];
		new vecTarget[VECTOR];
		
		vecTarget[X_AXIS] = args[4];
		vecTarget[Y_AXIS] = args[5];
		vecTarget[Z_AXIS] = args[6];
	
		new vecSpot[VECTOR];
	
		new Float:fDegree, Float:fRadius;
		fDegree = random_float(0.0, 360.0);
		fRadius = random_float(0.0, float(iRadius) );
		
		vecSpot[X_AXIS] = vecTarget[X_AXIS] + floatround( floatcos(fDegree) * fRadius );
		vecSpot[Y_AXIS] = vecTarget[Y_AXIS] + floatround( floatsin(fDegree) * fRadius );
		vecSpot[Z_AXIS] = vecTarget[Z_AXIS];
		
		ForceFieldEffect(vecSpot);
		
		new iArrPlayersID[MAX_PLAYERS], nPlayers;
		get_players(iArrPlayersID, nPlayers, "a");
		new vecOrigin[VECTOR];

		for(new iPlayer = 0; iPlayer < nPlayers; iPlayer++)
		{
			get_user_origin(iArrPlayersID[iPlayer], vecOrigin);
			if ( get_distance(vecOrigin, vecTarget) < iRadius && vecTarget[Z_AXIS] - 10 < vecOrigin[Z_AXIS] < vecTarget[Z_AXIS] + PLAYER_CENTER_Z_OFFSET * 3 )
			{
				if ( !( iArrPlayersID[iPlayer] == iCasterID && IsMageMaster(iCasterID, FORCE_FIELD) ) )
				{
					// bmgeneralfuncs.inc
					DoDamage(iArrPlayersID[iPlayer], iCasterID, iDamage, DMG_CAUSE_FORCE_FIELD);

					// bmgeneralfuncs.inc
					SendGameMessageFade(iArrPlayersID[iPlayer], 0, 0, 255, FORCE_FIELD_GLOW);
				}
			}
		}
		if ( args[3] > 0 )
		{
			args[3]--;
			set_task(FORCE_FIELD_INTERVAL, "TaskForceField", FORCE_FIELD_TASK_ID + iCasterID, args, 7);
		}
	}
}


//***************************************************************
//*	Function ForceFieldEffect(vecSpot[VECTOR])				*
//*	vecSpot:	The point where we will create the effect			*
//*												*
//*	Description:									*
//*	Creates Force Field effect on a specified point				*
//***************************************************************
ForceFieldEffect(vecSpot[VECTOR])
{
	message_begin( MSG_BROADCAST, SVC_TEMPENTITY ) 
	write_byte( 0 ) 
	write_coord( vecSpot[X_AXIS] ) 
	write_coord( vecSpot[Y_AXIS] ) 
	write_coord( vecSpot[Z_AXIS] + FORCE_FIELD_EFFECT_HEIGHT ) 
	write_coord( vecSpot[X_AXIS] ) 
	write_coord( vecSpot[Y_AXIS] ) 
	write_coord( vecSpot[Z_AXIS] ) 
	write_short( g_spriteLightning ) 
	write_byte( 1	)	// Framestart
	write_byte( 5	)	// Framerate
	write_byte( 2	)	// Life
	write_byte( 500	)	// Width
	write_byte( 20	)	// Noice
	write_byte( 0	)	// Red color
	write_byte( 0	)	// Green color
	write_byte( 255	)	// Blue Color
	write_byte( FORCE_FIELD_EFFECT_BRIGHTNESS )	// Brightness
	write_byte( FORCE_FIELD_EFFECT_SPEED )		// Speed
	message_end() 
}

#endif
