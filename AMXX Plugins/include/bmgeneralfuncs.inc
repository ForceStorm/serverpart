//***************************************************************
//*	Battle Mages v. 1.0.1 for AMXX by Martin J. Van der Cal		*
//*												*
//*	Mod adding magic to the game.							*
//*												*
//*	Copyright (C) 2004, Martin J. Van der Cal					*
//*	This plugin is under the GNU General Public License, read		*
//*	"Battle Mages Readme.doc" for further information.			*
//*												*
//*	bmgeneralfuncs.inc								*
//*												*
//***************************************************************

#if !defined BATTLE_MAGES_GENERAL_FUNCTIONS
#define BATTLE_MAGES_GENERAL_FUNCTIONS

#include "bmconst"
#include "bmxphandler"


//***************************************************************
//*	Function IsFriendlyFireOn()							*
//*												*
//*	Returns:	true if friendly fire is on, false if not			*
//*												*
//*	Description:									*
//*	Checks if friendly fire is on and returns whether it is or not		*
//***************************************************************
bool:IsFriendlyFireOn()
{
	if(cvar_exists("mp_friendlyfire"))
	{
		new iFF = get_cvar_num("mp_friendlyfire");
		return (iFF == 1);
	}

	return false;
}


//***************************************************************
//*	Function IsSaveXPOn()								*
//*												*
//*	Returns:	true if save xp is on, false if not				*
//*												*
//*	Description:									*
//*	Checks if save xp is on and returns whether it is or not			*
//***************************************************************
bool:IsSaveXPOn()
{
	return (get_cvar_num(CVAR_USE_SAVEXP) == 1);
}


//***************************************************************
//*	Function LogKill(iShooterID, iTargetID, sWeaponDescription[])	*
//*	iShooterID:	The player who shot					*
//*	iTargetID:		The player who died					*
//*	sWeaponDescription[]:	Name of weapon/spell				*
//*												*
//*	Description:									*
//*	Logs a kill to the server logs							*
//***************************************************************
stock LogKill(iShooterID, iTargetID, sWeaponDescription[])
{
	new sShooterName[MAX_PLAYERNAME_SIZE], sTargetName[MAX_PLAYERNAME_SIZE],
		sShooterAuthID[MAX_AUTHID_SIZE], sTargetAuthID[MAX_AUTHID_SIZE],
		sShooterTeamName[MAX_TEAMNAME_SIZE], sTargetTeamName[MAX_TEAMNAME_SIZE]; 

	// Info on Shooter
	get_user_name(iShooterID, sShooterName, MAX_PLAYERNAME_SIZE-1);
	get_user_team(iShooterID, sShooterTeamName, MAX_TEAMNAME_SIZE-1);
	get_user_authid(iShooterID, sShooterAuthID, MAX_AUTHID_SIZE-1);

	// Info on Target 
	get_user_name(iTargetID, sTargetName, MAX_PLAYERNAME_SIZE-1);
	get_user_team(iTargetID, sTargetTeamName, MAX_TEAMNAME_SIZE-1);
	get_user_authid(iTargetID, sTargetAuthID, MAX_AUTHID_SIZE-1);
 
	// Log this Kill
	log_message("^"%s<%d><%s><%s>^" killed ^"%s<%d><%s><%s>^" with ^"%s^"",
		sShooterName, iShooterID, sShooterAuthID, sShooterTeamName,
		sTargetName, iTargetID, sTargetAuthID, sTargetTeamName, sWeaponDescription);
}


//***************************************************************
//*	Function DoDamage(iTargetID, iShooterID, iDamage,			*
//*				iDamageCause, bIsWeaponID = false,		*
//*				iHeadShot = 0)						*
//*	iTargetID:		The player who is about to take damage		*
//*	iShooterID:	The player who did the damage				*
//*	iDamage:		The damage that will be dealt				*
//*	iDamageCause:	The cause of damage, Spell or weapon id		*
//*	bIsWeaponID:	Set if the damage cause is from a weapon		*
//*	iHeadShot:		Whether it was a headshot or not			*
//*												*
//*	Description:									*
//*	Does damage to a player								*
//***************************************************************
DoDamage(iTargetID, iShooterID, iDamage, iDamageCause, bIsWeaponID = false, iHeadShot = 0)
{
	if ( is_user_alive(iTargetID) )
	{
		new sWeaponOrMagicName[64];
	
		if ( bIsWeaponID )
		{
			// get_weaponname(iDamageCause, sWeaponOrMagicName, 63); // Doesnt give the correct weapon names
			GetWeaponName(iDamageCause, sWeaponOrMagicName, 63);
	
			if ( iDamageCause == CSW_HEGRENADE )
				iHeadShot = 0;
		}
		else
		{
			switch( iDamageCause )
			{
			case DMG_CAUSE_FIREBALL:
				sWeaponOrMagicName = "Fireball";
			case DMG_CAUSE_LIGHTNING_STRIKE:
				sWeaponOrMagicName = "Lightning Strike";
			case DMG_CAUSE_ICE_IMPLOSION:
				sWeaponOrMagicName = "Ice Implosion";
			case DMG_CAUSE_MAGMA_SHIELD:
				sWeaponOrMagicName = "Magma Shield";
			case DMG_CAUSE_MIND_BLAST:
				sWeaponOrMagicName = "Mind Blast";
			case DMG_CAUSE_ENCHANTED_WEAPON:
				sWeaponOrMagicName = "Enchanted Bullets";
			case DMG_CAUSE_ENCHANTED_WEAPON_BURN:
				sWeaponOrMagicName = "Enchanted Weapon Burn";
			case DMG_CAUSE_CHAIN_LIGHTNING:
				sWeaponOrMagicName = "Chain Lightning";
			case DMG_CAUSE_LEECH:
				sWeaponOrMagicName = "Leech";
			case DMG_CAUSE_AURA_OF_LEECH:
				sWeaponOrMagicName = "Aura of Leech";
			case DMG_CAUSE_FORCE_FIELD:
				sWeaponOrMagicName = "Force Field";
			case DMG_CAUSE_CRITICAL_KNIFE:
				sWeaponOrMagicName = "Critical Knife";
			case DMG_CAUSE_MIND_FLARE:
				sWeaponOrMagicName = "Mind Flare";
			case DMG_CAUSE_SHIELD_YLIENELLE:
				sWeaponOrMagicName = "Shield of Y'lienelle";
			}
			
			if ( g_bArrPlayersHasAvatar[iTargetID] )
				SpellResistanceAvatar(iTargetID, iDamage)
		}
		
		#if defined DEBUG_SERVER_PRINT
			server_print("[DEBUG][BM] Dealing damage to (%d), Damage (%d), Weapon (%s)", iTargetID, iDamage, sWeaponOrMagicName);
		#endif
		
		new bool:bPlayerDied = false;
		new iHP = get_user_health(iTargetID);
	
		if ( ( iHP - iDamage ) <= 0 )
			bPlayerDied = true;
		else if ( g_bArrPlayersHasMagmaShield[iTargetID] && get_user_health(iTargetID) <= ABNORMAL_HEALTH )
			bPlayerDied = true;
		else if ( g_bArrPlayersHasGhostArmor[iTargetID] && get_user_health(iTargetID) <= ABNORMAL_HEALTH )
			bPlayerDied = true;
		else if ( g_bArrPlayersHasManaShield[iTargetID] && get_user_health(iTargetID) <= ABNORMAL_HEALTH )
			bPlayerDied = true;
		
		if (bPlayerDied)
		{
			// engine.inc set_msg_block function
			set_msg_block(g_iGameMsgDeath, BLOCK_ONCE);
			user_kill(iTargetID, DONT_REMOVE_FRAG);
		}
		else
			set_user_health(iTargetID, iHP - iDamage);
		
		new sShooterName[32];
		get_user_name(iShooterID, sShooterName, 31);
		
		if (bPlayerDied)
		{
			if ( iShooterID != iTargetID )
			{
				if ( get_user_team(iShooterID) != get_user_team(iTargetID) )
					set_user_frags(iShooterID, get_user_frags(iShooterID) + 1);
				else
					set_user_frags(iShooterID, get_user_frags(iShooterID) - 1);
				
				LogKill(iShooterID, iTargetID, sWeaponOrMagicName);
			}
			
			AddXP(iShooterID, BM_XP_KILL, iTargetID); // bmxphandler.inc
			
			g_bArrPlayersDeathMsgShown[iTargetID] = true;
	
			message_begin( MSG_ALL, g_iGameMsgDeath, {0,0,0}, 0);
			write_byte(iShooterID);
			write_byte(iTargetID);
			write_byte(iHeadShot);
			write_string(sWeaponOrMagicName);
			message_end();
		}
	}
}


//***************************************************************
//*	Function SquareRoot(iValue)							*
//*	iValue:	Square root of this value					*
//*												*
//*	Description:									*
//*	Calculates the square root of the given value				*
//***************************************************************
public SquareRoot(iValue)
{
	new iDiv = iValue;
	new iResult = 1;
	
	while (iDiv > iResult)
	{
		iDiv = (iDiv + iResult) / 2;
		iResult = iValue / iDiv;
	}
	
	return iDiv;
}


//***************************************************************
//*	Function ResetMaxSpeed(iPlayerID, bNewRound = false)		*
//*	iPlayerID:		The player who we will reset max speed on		*
//*	bNewRound:	Whether it is a new round or not			*
//*												*
//*	Description:									*
//*	Resets the max speed of a player						*
//***************************************************************
ResetMaxSpeed(iPlayerID, bNewRound = false)
{
	g_bArrPlayersIsHasted[iPlayerID]		= false;
	g_bArrPlayersIsSlowed[iPlayerID]		= false;
	g_bArrPlayersIsSlowedBonus[iPlayerID]	= false;
	g_bArrPlayersIsTotalInvisible[iPlayerID]= false;

	if ( !bNewRound )
	{
		new iClipAmmo	= 0;
		new iAmmo		= 0;
		new iWeaponID	= 0;
		iWeaponID = get_user_weapon(iPlayerID, iClipAmmo, iAmmo);
	
		switch ( iWeaponID )
		{
		case CSW_P228:
			set_user_maxspeed(iPlayerID, 250.0);
		case CSW_SCOUT:
			{
				if ( g_bArrPlayersIsZoomMode[iPlayerID] )
					set_user_maxspeed(iPlayerID, 220.0);
				else
					set_user_maxspeed(iPlayerID, 260.0);
			}
		case CSW_HEGRENADE:
				set_user_maxspeed(iPlayerID, 260.0);
		case CSW_XM1014:
				set_user_maxspeed(iPlayerID, 230.0);
		case CSW_C4:
			set_user_maxspeed(iPlayerID, 250.0);
		case CSW_MAC10:
				set_user_maxspeed(iPlayerID, 250.0);
		case CSW_AUG:
				set_user_maxspeed(iPlayerID, 240.0);
		case CSW_SMOKEGRENADE:
				set_user_maxspeed(iPlayerID, 250.0);
		case CSW_ELITE:
			set_user_maxspeed(iPlayerID, 250.0);
		case CSW_FIVESEVEN:
			set_user_maxspeed(iPlayerID, 250.0);
		case CSW_UMP45:
				set_user_maxspeed(iPlayerID, 250.0);
		case CSW_SG550:
			{
				if ( g_bArrPlayersIsZoomMode[iPlayerID] )
					set_user_maxspeed(iPlayerID, 150.0);
				else
					set_user_maxspeed(iPlayerID, 210.0);
			}
		case CSW_GALIL:
				set_user_maxspeed(iPlayerID, 240.0);
		case CSW_FAMAS:
				set_user_maxspeed(iPlayerID, 240.0);
		case CSW_USP:
			set_user_maxspeed(iPlayerID, 250.0);
		case CSW_GLOCK18:
			set_user_maxspeed(iPlayerID, 250.0);
		case CSW_AWP:
			{
				if ( g_bArrPlayersIsZoomMode[iPlayerID] )
					set_user_maxspeed(iPlayerID, 150.0);
				else
					set_user_maxspeed(iPlayerID, 210.0);
			}
		case CSW_MP5NAVY:
				set_user_maxspeed(iPlayerID, 250.0);
		case CSW_M249:
				set_user_maxspeed(iPlayerID, 220.0);
		case CSW_M3:
				set_user_maxspeed(iPlayerID, 240.0);
		case CSW_M4A1:
				set_user_maxspeed(iPlayerID, 230.0);
		case CSW_TMP:
				set_user_maxspeed(iPlayerID, 250.0);
		case CSW_G3SG1:
			{
				if ( g_bArrPlayersIsZoomMode[iPlayerID] )
					set_user_maxspeed(iPlayerID, 150.0);
				else
					set_user_maxspeed(iPlayerID, 210.0);
			}
		case CSW_FLASHBANG:
				set_user_maxspeed(iPlayerID, 250.0);
		case CSW_DEAGLE:
			set_user_maxspeed(iPlayerID, 250.0);
		case CSW_SG552:
				set_user_maxspeed(iPlayerID, 235.0);
		case CSW_AK47:
				set_user_maxspeed(iPlayerID, 221.0);
		case CSW_KNIFE:
			set_user_maxspeed(iPlayerID, 250.0);
		case CSW_P90:
			set_user_maxspeed(iPlayerID, 245.0);
		}
	}
}


//***************************************************************
//*	Function GetWeaponName(iWeaponID, sWeaponName[], iLength)	*
//*	iWeaponID:	Id of the weapon we are to return the name of	*
//*	sWeaponName[]:	Will be set to the weapon name and returned	*
//*	iLength:		The max length of the sWeaponName string	*
//*												*
//*	Description:									*
//*	Returns the weapon name for use with logging a kill			*
//***************************************************************
bool:GetWeaponName(iWeaponID, sWeaponName[], iLength)
{
	switch ( iWeaponID )
	{
	case CSW_P228:
		format(sWeaponName, iLength, "p228");
	case CSW_SCOUT:
		format(sWeaponName, iLength, "scout");
	case CSW_HEGRENADE:
		format(sWeaponName, iLength, "grenade");
	case CSW_XM1014:
		format(sWeaponName, iLength, "xm1014");
	case CSW_C4:
		format(sWeaponName, iLength, "c4"); // not entirely sure, cant really kill anyway
	case CSW_MAC10:
		format(sWeaponName, iLength, "mac10");
	case CSW_AUG:
		format(sWeaponName, iLength, "aug");
	case CSW_SMOKEGRENADE:
		format(sWeaponName, iLength, "smokegrenade"); // not entirely sure, cant really kill anyway
	case CSW_ELITE:
		format(sWeaponName, iLength, "elite");
	case CSW_FIVESEVEN:
		format(sWeaponName, iLength, "fiveseven");
	case CSW_UMP45:
		format(sWeaponName, iLength, "ump45");
	case CSW_SG550:
		format(sWeaponName, iLength, "sg550");
	case CSW_GALIL:
		format(sWeaponName, iLength, "galil");
	case CSW_FAMAS:
		format(sWeaponName, iLength, "famas");
	case CSW_USP:
		format(sWeaponName, iLength, "usp");
	case CSW_GLOCK18:
		format(sWeaponName, iLength, "glock18");
	case CSW_AWP:
		format(sWeaponName, iLength, "awp");
	case CSW_MP5NAVY:
		format(sWeaponName, iLength, "mp5navy");
	case CSW_M249:
		format(sWeaponName, iLength, "m249");
	case CSW_M3:
		format(sWeaponName, iLength, "m3");
	case CSW_M4A1:
		format(sWeaponName, iLength, "m4a1");
	case CSW_TMP:
		format(sWeaponName, iLength, "tmp");
	case CSW_G3SG1:
		format(sWeaponName, iLength, "g3sg1");
	case CSW_FLASHBANG:
		format(sWeaponName, iLength, "flashbang"); // not entirely sure, cant really kill anyway
	case CSW_DEAGLE:
		format(sWeaponName, iLength, "deagle");
	case CSW_SG552:
		format(sWeaponName, iLength, "sg552");
	case CSW_AK47:
		format(sWeaponName, iLength, "ak47");
	case CSW_KNIFE:
		format(sWeaponName, iLength, "knife");
	case CSW_P90:
		format(sWeaponName, iLength, "p90");
	default:
		{
			format(sWeaponName, iLength, ""); // not entirely sure, cant really kill anyway
			return false;
		}
	}
	return true;
}


//***************************************************************
//*	Function ResetPlayerWhenDied(iPlayerID)					*
//*	iPlayerID:	The player we will reset						*
//*												*
//*	Description:									*
//*	Resets variables and spells for the given player.				*
//***************************************************************
ResetPlayerWhenDied(iPlayerID)
{
	g_iArrPlayersMaxHP[iPlayerID]				= 100;
	g_bArrPlayersIsHasted[iPlayerID]			= false;
	g_bArrPlayersIsSlowed[iPlayerID]			= false;
	g_bArrPlayersIsSlowedBonus[iPlayerID]		= false;
	g_bArrPlayersIsMagicReady[iPlayerID]		= true;
	g_bArrPlayersIsZoomMode[iPlayerID]			= false;
	g_bArrPlayersDeathMsgShown[iPlayerID]		= false;
	g_bArrPlayersHasMagmaShield[iPlayerID]		= false;
	g_bArrPlayersHasGhostArmor[iPlayerID]		= false;
	g_bArrPlayersHasPlanarArmor[iPlayerID]		= false;
	g_bArrPlayersHasEnchantedWeapon[iPlayerID]	= false;
	g_bArrPlayersHasManaShield[iPlayerID]		= false;
	g_bArrPlayersHasShieldYlien[iPlayerID]		= false;
	g_bArrPlayersHasAvatar[iPlayerID]			= false;

	if ( task_exists(MANA_REGENERATION_TASK_ID + iPlayerID) )
		remove_task(MANA_REGENERATION_TASK_ID + iPlayerID);
	if ( task_exists(REGENERATION_TASK_ID + iPlayerID) )	// Removes active Regeneration Spell
		remove_task(REGENERATION_TASK_ID + iPlayerID);
	
	// bmillusion.inc
	RemoveInvisibility(iPlayerID);
	// bmillusion.inc
	ResetModel(iPlayerID);
	set_user_rendering(iPlayerID);
}


//***************************************************************
//*	Function ResetSpellUpgrades(iPlayerID)					*
//*	iPlayerID:	The player who called reset spell upgrades			*
//*												*
//*	Description:									*
//*	Resets the current spell upgrades for a player on the current school	*
//***************************************************************
ResetSpellUpgrades(iPlayerID)
{
	for (new iSpellLevel = 0; iSpellLevel < MAX_NUMBER_OF_SPELLS; iSpellLevel++)
		g_iArrPlSpellLvls[iPlayerID][iSpellLevel] = 0;
}


//***************************************************************
//*	Function IsMageMaster(iMageID, iSpellID)				*
//*	iMageID:	The id of the player we will check				*
//*	iSpellID:	The spell we will check						*
//*												*
//*	Returns:	true if master, false otherwise					*
//*												*
//*	Description:									*
//*	Checks if a player is a master in a given spell				*
//***************************************************************
bool:IsMageMaster(iMageID, iSpellID)
{
	return (g_iArrPlSpellLvls[iMageID][iSpellID] == BM_MAX_SPELL_LEVEL)
}


//***************************************************************
//*	Function GetClosestPlayer(vecCenter[VECTOR], iRadius = 2000000,	*
//*					bSpecifyTeam = false, sTeam[] = "")		*
//*	vecCenter:		The center position from where we will check	*
//*	iRadius:		The radius we will check					*
//*	bSpecifyTeam:	true if we have specified a team			*
//*	sTeam:		The team we will check					*
//*												*
//*	Returns:		The id of the closest player, 0 if none found	*
//*												*
//*	Description:									*
//*	Checks which player is closest to the given position			*
//***************************************************************
GetClosestPlayer(vecCenter[VECTOR], iRadius = 2000000, bSpecifyTeam = false, sTeam[] = "")
{
	new iArrPlayersID[MAX_PLAYERS];
	new nPlayers, iClosestID = 0, iClosestDistance = iRadius + 1, iDistance;
	new vecOriginPlayer[VECTOR];
	
	if ( bSpecifyTeam )
		get_players(iArrPlayersID, nPlayers, "ae", sTeam);
	else
		get_players(iArrPlayersID, nPlayers, "a");
	
	for(new iPlayer = 0; iPlayer < nPlayers; iPlayer++)
	{
		get_user_origin(iArrPlayersID[iPlayer], vecOriginPlayer);
		iDistance = get_distance(vecCenter, vecOriginPlayer);
		if ( iClosestDistance > iDistance )
		{
			iClosestDistance = iDistance;
			iClosestID = iArrPlayersID[iPlayer];
		}
	}
	
	return iClosestID;
}


//***************************************************************
//*	Function IncreaseUserHealth(iPlayerID, iAmount)			*
//*	iPlayerID:	The player we are increasing health on			*
//*	iAmount:	The amount of health that is to be increased		*
//*												*
//*	Description:									*
//*	Increases the health of a player with a given amount			*
//***************************************************************
IncreaseUserHealth(iPlayerID, iAmount)
{
	new iNewHealth = get_user_health(iPlayerID) + iAmount;
	if ( iNewHealth > g_iArrPlayersMaxHP[iPlayerID] )
		iNewHealth = g_iArrPlayersMaxHP[iPlayerID];
		
	set_user_health(iPlayerID, iNewHealth);
}


//***************************************************************
//*	Function SendGameMessageFade(iPlayerID, iRed, iGreen, iBlue,	*
//*						iGlow)					*
//*	iPlayerID:	Player that the message will be sent to			*
//*	iRed:		Amount of red color						*
//*	iGreen:	Amount of green color						*
//*	iBlue:		Amount of blue color						*
//*	iGlow:	Amount of glow							*
//*												*
//*	Description:									*
//*	Sends a fade message to a client, making a player glow with a given	*
//*	amount and in a given RGB color value.					*
//***************************************************************
SendGameMessageFade(iPlayerID, iRed, iGreen, iBlue, iGlow)
{
		message_begin(MSG_ONE, g_iGameMsgFade, {0,0,0}, iPlayerID)	// Fade Message
		write_short( 1<<10 )	// Duration
		write_short( 1<<10 )	// 
		write_short( 1<<12 )	// Fade Type
		write_byte( iRed )		// Red
		write_byte( iGreen )	// Green
		write_byte( iBlue )		// Blue
		write_byte( iGlow )		// Alpha
		message_end()
}


//***************************************************************
//*	Function ShowDurationBar(iPlayerID, iDurationEnd = 0,		*
//*					iDurationStart = 0)				*
//*	iPlayerID:		Id of the player which we will show the bar for	*
//*	iDurationEnd:	The amount of seconds the bar will end at		*
//*	iDurationStart:	The second the duration bar will start at		*
//*												*
//*	Description:									*
//*	Shows a duration bar for a player that will end at a given time and	*
//*	start with a given time. Note that calling the function with only the	*
//*	id of the player will cancel the duration bar that is active.		*
//***************************************************************
ShowDurationBar(iPlayerID, iDurationEnd = 0, iDurationStart = 0)
{
	message_begin( MSG_ONE, DURATION_BAR_EFFECT, {0,0,0}, iPlayerID );
	write_byte( iDurationEnd );
	write_byte( iDurationStart );
	message_end();
}


//***************************************************************
//*	Function DisplayPlayerInfo(iPlayerID)					*
//*	iPlayerID:	The player we will show the player info to			*
//*												*
//*	Description:									*
//*	Shows information to a player, such as: experience, level, mp etc.	*
//***************************************************************
DisplayPlayerInfo(iPlayerID)
{
	new sActiveSpellName[MAX_SPELL_SIZE];
	new iSelectedMagic = g_iArrPlayersSelMagic[iPlayerID];
	new iSelectedSchool = g_iArrPlayersSchool[iPlayerID]-1;
	
	if ( IsMageMaster(iPlayerID, g_iArrPlayersSelMagic[iPlayerID]) )
		iSelectedMagic += MAX_NUMBER_OF_SPELLS;

	copy(sActiveSpellName, MAX_SPELL_SIZE-1, g_sMatrixMagics[iSelectedSchool][iSelectedMagic]);

	new iXPToNextLevel = g_iArrPlNextLevel[iPlayerID] - g_iArrPlCurXP[iPlayerID];

	new iPlayerInfoPosition = get_cvar_num(CVAR_PLAYER_INFO_POSITION);
	switch ( iPlayerInfoPosition )
	{
		case POSITION_DOWN_LEFT:
			set_hudmessage(200, 200, 200, -2.0, 0.75, 0, 6.0, 2.0, 0.1, 0.5, HUD_PLAYERINFO);
		case POSITION_DOWN_RIGHT:
			set_hudmessage(200, 200, 200,  1.0, 0.75, 0, 6.0, 2.0, 0.1, 0.5, HUD_PLAYERINFO);
		case POSITION_DOWN_CENTER:
			set_hudmessage(200, 200, 200, -1.0, 0.75, 0, 6.0, 2.0, 0.1, 0.5, HUD_PLAYERINFO);
		default:
		{
			server_print("[BM] Error in cvar %s, invalid value", CVAR_PLAYER_INFO_POSITION);
			return PLUGIN_CONTINUE;
		}
	}

	show_hudmessage(iPlayerID, "%L: %L, %L %d^n%L: %d, %L: %d^n%L: %s, %L: %dMP. %dMP %L",
		iPlayerID, "DICT_SCHOOL", iPlayerID, g_sArrMagicSchools[g_iArrPlayersSchool[iPlayerID]],
		iPlayerID, "DICT_LEVEL", g_iArrPlCurLevel[iPlayerID],
		iPlayerID, "DICT_CURRENT_XP", g_iArrPlCurXP[iPlayerID],
		iPlayerID, "DICT_XPTONEXT_LEVEL", iXPToNextLevel,
		iPlayerID, "DICT_ACTIVE_SPELL", sActiveSpellName, iPlayerID, "DICT_COST",
		g_iArrPlSelMagicCost[iPlayerID], g_iArrPlayersMP[iPlayerID], iPlayerID, "DICT_LEFT");

	return PLUGIN_CONTINUE;
}


//***************************************************************
//*	Function RemoveActiveSpells(iPlayerID)					*
//*	iPlayerID:	The id of the player we are to remove active spells on	*
//*												*
//*	Description:									*
//*	Removes all active spells on a given player.					*
//***************************************************************
RemoveActiveSpells(iPlayerID)
{
	RemoveInvisibility(iPlayerID);
	RemoveBadEffects(iPlayerID);

	if ( task_exists(STONESHIELD_TASK_ID + iPlayerID) )	// Removes active stone walls
	{
		remove_task(STONESHIELD_TASK_ID + iPlayerID);
		remove_entity(g_entArrStoneWall[iPlayerID]);
	}

	if ( task_exists(SPELL_COOLDOWN_TASK_ID + iPlayerID) )	// Removes active spell cooldowns
		remove_task(SPELL_COOLDOWN_TASK_ID + iPlayerID);

	if ( task_exists(LIGHTNING_SPEED_TASK_ID + iPlayerID) )	// Removes active Lightning Speed Spells
		remove_task(LIGHTNING_SPEED_TASK_ID + iPlayerID);

	if ( g_bArrPlayersIsSlowed[iPlayerID] || g_bArrPlayersIsHasted[iPlayerID] )
	{
		// bmgeneralfuncs.inc ResetMaxSpeed function, second parameter indicates new round
		ResetMaxSpeed(iPlayerID, true);
	}
	if ( task_exists(AVATAR_TASK_ID + iPlayerID) )
	{
		remove_task(AVATAR_TASK_ID + iPlayerID);
		g_bArrPlayersHasAvatar[iPlayerID] = false;
		g_iArrPlayersMaxHP[iPlayerID] -= ABNORMAL_HEALTH;
	}
	if ( task_exists(FORCE_FIELD_TASK_ID + iPlayerID) )	// Removes active Regeneration Spells
		remove_task(FORCE_FIELD_TASK_ID + iPlayerID);
	
	if ( get_user_armor(iPlayerID) > 100 )	// Removes active planar armor spells
		set_user_armor(iPlayerID, 100);
	
	g_iArrPlayersMP[iPlayerID] = get_cvar_num(CVAR_START_MAGIC_POINTS);
	g_bArrPlayersIsMagicReady[iPlayerID]	= true;
	g_bArrPlayersIsHasted[iPlayerID]		= false;
}


//***************************************************************
//*	Function RemoveBadEffects(iPlayerID)					*
//*	iPlayerID:	id of the player we are to remove all bad effects on	*
//*												*
//*	Description:									*
//*	Removes all bad effects on a given player					*
//***************************************************************
RemoveBadEffects(iPlayerID)
{
	if ( task_exists(ICE_IMPLOSION_DOT_TASK_ID + iPlayerID) )	// Removes active Ice Implosion Spells
		remove_task(ICE_IMPLOSION_DOT_TASK_ID + iPlayerID);
		
	if ( task_exists(FIREBALL_FLY_TASK_ID + iPlayerID) )	// Removes active Fireball
		remove_task(FIREBALL_FLY_TASK_ID + iPlayerID);

	if ( task_exists(ENCHANT_WEAPON_BURN_TASK_ID + iPlayerID) )	// Removes active Enchanted Weapon Burns
		remove_task(ENCHANT_WEAPON_BURN_TASK_ID + iPlayerID);

	if ( task_exists(MIND_BLAST_TASK_ID + iPlayerID) )	// Removes active Mind Blast Spells
		remove_task(MIND_BLAST_TASK_ID + iPlayerID);
}

#endif
