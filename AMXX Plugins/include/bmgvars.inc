//***************************************************************
//*	Battle Mages v. 1.0.1 for AMXX by Martin J. Van der Cal		*
//*												*
//*	Mod adding magic to the game.							*
//*												*
//*	Copyright (C) 2004, Martin J. Van der Cal					*
//*	This plugin is under the GNU General Public License, read		*
//*	"Battle Mages Readme.doc" for further information.			*
//*												*
//*	bmgvars.inc										*
//*												*
//***************************************************************

#if !defined BATTLE_MAGES_GVARS
#define BATTLE_MAGES_GVARS

// Global Variables, Marked by "g_"

// Strings with the names of the diffrent magic schools
new g_sArrMagicSchools[NUMBER_OF_SCHOOLS+1][] = { "DICT_WARRIOR", "DICT_MAGE_ELEMENTAL", "DICT_MAGE_ENERGY", "DICT_MAGE_ILLUSION", "DICT_MAGE_LIFE" };

// Matrix with the names and the costs of the diffrent spells
new g_sMatrixMagics[NUMBER_OF_SCHOOLS][MAX_NUMBER_OF_SPELLS * 2][MAX_SPELL_SIZE];
new g_iMatrixMagicCost[NUMBER_OF_SCHOOLS][MAX_NUMBER_OF_SPELLS * 2];

// Array with the number of spells in each school
new g_nArrSpellsInSchool[] = { WARRIOR, NUMBER_OF_MAGICS_ELEMENTAL, NUMBER_OF_MAGICS_ENERGY, NUMBER_OF_MAGICS_ILLUSION, NUMBER_OF_MAGICS_LIFE };

// What school the players belongs to
new g_iArrPlayersSchool[MAX_PLAYERS+1];

// Players Current MP
new g_iArrPlayersMP[MAX_PLAYERS+1];

// What spell currently selected
new g_iArrPlayersSelMagic[MAX_PLAYERS+1];

// the power of the selected spell
//new g_iArrPlSelMagicPower[MAX_PLAYERS+1];

// the cost of the selected spell
new g_iArrPlSelMagicCost[MAX_PLAYERS+1];

// Max hp, so that you cant heal a player over their maxhp
new g_iArrPlayersMaxHP[MAX_PLAYERS+1];

// Recieves a value if a player wants to change school, 0 as value if they arent changing school
new g_iArrPlayersNextSchool[MAX_PLAYERS+1];

// The abilities of the current players, yeah i know its a matrix...
new g_iArrPlSpellLvls[MAX_PLAYERS+1][MAX_NUMBER_OF_SPELLS];

// List with the next level XP for the players
new g_iArrPlNextLevel[MAX_PLAYERS+1];

// List with the current level for all players
new g_iArrPlCurLevel[MAX_PLAYERS+1];

// List with the current xp of the players
new g_iArrPlCurXP[MAX_PLAYERS+1];

// a list with the xp that needs to level up
new g_iArrXPLevels[BM_MAX_LEVEL+1] = {LEVEL_1, LEVEL_2, LEVEL_3, LEVEL_4, LEVEL_5, LEVEL_6, LEVEL_7, LEVEL_8, LEVEL_9, LEVEL_10,
	LEVEL_11, LEVEL_12, LEVEL_13, LEVEL_14, LEVEL_15, LEVEL_16, LEVEL_17, LEVEL_18, LEVEL_19, LEVEL_20,
	LEVEL_21, LEVEL_22, LEVEL_23, LEVEL_24, LEVEL_25, LEVEL_26, LEVEL_27, LEVEL_28, LEVEL_29, LEVEL_30, LEVEL_MAX};
	
// List with base amount of xp that you get for doing a given thing
new g_iArrXPTypes[NUMBER_OF_XP_TYPES] = { KILL_XP_BASEAMOUNT, WINROUND_XP_BASEAMOUNT, HEAL_OTHER_BASEAMOUNT };

// Lists of wall entities
new g_entArrStoneWall[MAX_PLAYERS+1];

// Booleans
new bool:g_bArrPlayersIsHasted[MAX_PLAYERS+1]			= false;
new bool:g_bArrPlayersIsSlowed[MAX_PLAYERS+1]			= false;
new bool:g_bArrPlayersIsSlowedBonus[MAX_PLAYERS+1]		= false;
new bool:g_bArrPlayersIsZoomMode[MAX_PLAYERS+1]			= false;
new bool:g_bArrPlayersIsMagicReady[MAX_PLAYERS+1]		= true;
new bool:g_bArrPlayersDeathMsgShown[MAX_PLAYERS+1]		= false;
new bool:g_bArrPlayersHasMagmaShield[MAX_PLAYERS+1]		= false;
new bool:g_bArrPlayersHasEnchantedWeapon[MAX_PLAYERS+1]	= false;
new bool:g_bArrPlayersHasGhostArmor[MAX_PLAYERS+1]		= false;
new bool:g_bArrPlayersHasPlanarArmor[MAX_PLAYERS+1]		= false;
new bool:g_bArrPlayersIsTotalInvisible[MAX_PLAYERS+1]	= false;
new bool:g_bArrPlayersHasManaShield[MAX_PLAYERS+1]		= false;
new bool:g_bArrPlayersHasSelfIllusion[MAX_PLAYERS+1]	= false;
new bool:g_bArrPlayersHasShieldYlien[MAX_PLAYERS+1]		= false;
new bool:g_bArrPlayersHasAvatar[MAX_PLAYERS+1]			= false;
new bool:g_bArrPlayersResetUpgrades[MAX_PLAYERS+1]		= false;

// Some spells may not be used until after the freeze time (Lightning Speed)
new bool:g_bIsFreezeTime = false;
new bool:g_bIsFreezeTaskSet = false;

// Sprites and messages
new g_spriteBurning, g_spriteLightning, g_spriteShockwave, g_spriteExplosion, g_spriteSmoke;
new g_iGameMsgScrShake, g_iGameMsgDeath, g_iGameMsgFade;

#endif
