//***************************************************************
//*	Battle Mages v. 1.0.1 for AMXX by Martin J. Van der Cal		*
//*												*
//*	Mod adding magic to the game.							*
//*												*
//*	Copyright (C) 2004, Martin J. Van der Cal					*
//*	This plugin is under the GNU General Public License, read		*
//*	"Battle Mages Readme.doc" for further information.			*
//*												*
//*	bmhelp.inc										*
//*												*
//***************************************************************

#if !defined BATTLE_MAGES_HELP
#define BATTLE_MAGES_HELP


//***************************************************************
//*	Function ClCmdHelp(iPlayerID)						*
//*	iPlayerID:	the id of the player which we will show help to		*
//*												*
//*	Description:									*
//*	Captures client command bm_help and displays the help			*
//***************************************************************
public ClCmdHelp(iPlayerID)
{
	DisplayHelp(iPlayerID);
	return PLUGIN_HANDLED;
}


//***************************************************************
//*	Function ClCmdSchoolHelp(iPlayerID)					*
//*	iPlayerID:	the id of the player which we will show school help to	*
//*												*
//*	Description:									*
//*	Captures client command bm_schoolhelp and displays the school help	*
//***************************************************************
public ClCmdSchoolHelp(iPlayerID)
{
	DisplaySchoolHelp(iPlayerID);
	return PLUGIN_HANDLED;
}


//***************************************************************
//*	Function DisplayHelp(iPlayerID)						*
//*	iPlayerID:	the id of the player which we will show help to		*
//*												*
//*	Description:									*
//*	Displays the help for a given player						*
//***************************************************************
DisplayHelp(iPlayerID)
{
	new sBattleMagesHelpText[MAX_HELP_SIZE+1];
	new iPos = 0;
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "<body bgcolor='#000000' text='#009900'>");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "&lt;&lt;&lt; Battle Mages - Author: Martin J. Van der Cal &gt;&gt;&gt;<br>");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "<br>");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW01");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW02");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW03");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW04");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW05");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW06");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW07");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW08");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW09");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW10");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW11");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW12");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW13");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW14");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW15");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW16");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW17");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW18");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW19");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW20");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW21");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW22");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW23");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW24");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW25");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW26");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW27");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW28");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW29");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW30");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW31");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW32");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW33");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW34");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW35");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW36");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW37");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW38");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW39");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW40");
	iPos += format(sBattleMagesHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW41");
	// iPos += format(sBattleMagesHelpText[iPos], 2048-iPos, "%L<br>", iPlayerID, "DICT_HELP_MAIN_ROW01");
	
	show_motd(iPlayerID, sBattleMagesHelpText, "Battle Mages - Help");
}


//***************************************************************
//*	Function DisplaySchoolHelp(iPlayerID)					*
//*	iPlayerID:	the id of the player which we will show school help to	*
//*												*
//*	Description:									*
//*	Displays the school help for a given player					*
//***************************************************************
DisplaySchoolHelp(iPlayerID)
{
	new sBattleMagesSchoolHelpText[MAX_HELP_SIZE+1];
	new iPos = 0;

	iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "<body bgcolor='#000000' text='#009900'>");
	iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "&lt;&lt;&lt; Battle Mages - Author: Martin J. Van der Cal &gt;&gt;&gt;<br>");
	iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "<br>");

	switch ( g_iArrPlayersSchool[iPlayerID] )
	{
		case WARRIOR:
		{
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L", iPlayerID, "DICT_HELP_WARRIOR_ROW01");
		}
		case ELEMENTAL_MAGE:
		{
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW01");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW02");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW03");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW04");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW05");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW06");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW07");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW08");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW09");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW10");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW11");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW12");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW13");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW14");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW15");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW16");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW17");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW18");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW19");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW20");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW21");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW22");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW23");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW24");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW25");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW26");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW27");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW28");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW29");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW30");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ELEMENTAL_ROW31");
		}
		case ENERGY_MAGE:
		{
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW01");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW02");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW03");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW04");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW05");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW06");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW07");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW08");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW09");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW10");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW11");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW12");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW13");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW14");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW15");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW16");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW17");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW18");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW19");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW20");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW21");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW22");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW23");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW24");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW25");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW26");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW27");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW28");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW29");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW30");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW31");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW32");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW33");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW34");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW35");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW36");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW37");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW38");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW39");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ENERGY_ROW40");
		}
		case ILLUSION_MAGE:
		{
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW01");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW02");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW03");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW04");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW05");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW06");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW07");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW08");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW09");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW10");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW11");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW12");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW13");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW14");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW15");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW16");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW17");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW18");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW19");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW20");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW21");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW22");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW23");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW24");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW25");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW26");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW27");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW28");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW29");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW30");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW31");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW32");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW33");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW34");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW35");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW36");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW37");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW38");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW39");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_ILLUSION_ROW40");
		}
		case LIFE_MAGE:
		{
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW01");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW02");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW03");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW04");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW05");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW06");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW07");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW08");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW09");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW10");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW11");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW12");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW13");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW14");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW15");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW16");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW17");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW18");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW19");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW20");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW21");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW22");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW23");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW24");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW25");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW26");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW27");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW28");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW29");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW30");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW31");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW32");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW33");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW34");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW35");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW36");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW37");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW38");
			iPos += format(sBattleMagesSchoolHelpText[iPos], MAX_HELP_SIZE-iPos, "%L<br>", iPlayerID, "DICT_HELP_LIFE_ROW39");
		}
		default:
			return;
	}

	show_motd(iPlayerID, sBattleMagesSchoolHelpText, "Battle Mages - School Help");
}

#endif
