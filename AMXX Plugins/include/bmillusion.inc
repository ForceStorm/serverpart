//***************************************************************
//*	Battle Mages v. 1.0.1 for AMXX by Martin J. Van der Cal		*
//*												*
//*	Mod adding magic to the game.							*
//*												*
//*	Copyright (C) 2004, Martin J. Van der Cal					*
//*	This plugin is under the GNU General Public License, read		*
//*	"Battle Mages Readme.doc" for further information.			*
//*												*
//*	bmillusion.inc									*
//*												*
//***************************************************************

#if !defined BATTLE_MAGES_ILLUSION_MAGIC
#define BATTLE_MAGES_ILLUSION_MAGIC

#include "bmconst"
#include "bmgvars"
#include "bmgeneralfuncs"


//***************************************************************
//*	Function CastInvisibility(iCasterID)						*
//*	iCasterID:	The player who triggered cast invisibility			*
//*												*
//*	Description:									*
//*	Casts invisibility on the given player						*
//***************************************************************
CastInvisibility(iCasterID)
{
	new iPower;
	if ( IsMageMaster(iCasterID, INVISIBILITY) )
	{
		iPower = SUPREME_INVISIBILITY_POWER;
		g_bArrPlayersIsTotalInvisible[iCasterID] = true;
		set_user_maxspeed(iCasterID, SUPREME_INVISIBILITY_MOVEMENT_SPEED);
	}
	else
		iPower = INVISIBILITY_POWER - g_iArrPlSpellLvls[iCasterID][INVISIBILITY] * INVISIBILITY_INCREASE;
	
	set_user_rendering(iCasterID, kRenderFxNone, 0,0,0, kRenderTransTexture, iPower);
	
	new args[1];
	args[0] = iCasterID;
	
	ShowDurationBar(iCasterID, INVISIBILITY_DURATION);
	set_task(float(INVISIBILITY_DURATION), "EndInvisibility", INVISIBILITY_DURATION_TASK_ID + iCasterID, args, 1);

	if ( file_exists("sound/battlemages/invisibility.wav") )
		emit_sound(iCasterID, CHAN_ITEM, "battlemages/invisibility.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
}


//***************************************************************
//*	Function EndInvisibility(args[])						*
//*	args[]:	Contains 1 variable, the id of the caster			*
//*												*
//*	Description:									*
//*	Ends the invisibility on the caster						*
//***************************************************************
public EndInvisibility(args[])
{
	new iCasterID = args[0];
	set_user_rendering(iCasterID);
	
	if ( g_bArrPlayersIsTotalInvisible[iCasterID] )
	{
		g_bArrPlayersIsTotalInvisible[iCasterID] = false;
		ResetMaxSpeed(iCasterID);
	}
}


//***************************************************************
//*	Function RemoveInvisibility(iCasterID)					*
//*	iCasterID:	The id of the caster who has Supreme Invisibility		*
//*												*
//*	Description:									*
//*	Removes active invisibility							*
//***************************************************************
RemoveInvisibility(iCasterID)
{
	if ( task_exists(INVISIBILITY_DURATION_TASK_ID + iCasterID) )	// Removes active Invisibility Spells
	{
		remove_task(INVISIBILITY_DURATION_TASK_ID + iCasterID);
		set_user_rendering(iCasterID);
		
		if ( g_bArrPlayersIsTotalInvisible[iCasterID] )
		{
			g_bArrPlayersIsTotalInvisible[iCasterID] = false;
			ResetMaxSpeed(iCasterID);
		}
		
		ShowDurationBar(iCasterID);
	}
}


//***************************************************************
//*	Function CastManaShield(iCasterID)						*
//*	iCasterID:	The id of the player who triggered cast Mana Shield	*
//*												*
//*	Description:									*
//*	Casts mana shield on the caster						*
//***************************************************************
CastManaShield(iCasterID)
{
	if ( ! g_bArrPlayersHasManaShield[iCasterID] )
	{
		if ( file_exists("sound/battlemages/planar_armor.wav") )
			emit_sound(iCasterID, CHAN_ITEM, "battlemages/planar_armor.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
		
		set_user_health(iCasterID, get_user_health(iCasterID) + ABNORMAL_HEALTH);
		g_bArrPlayersHasManaShield[iCasterID] = true;
		g_iArrPlayersMaxHP[iCasterID] += ABNORMAL_HEALTH;
		return true;
	}
	return false;
}


//***************************************************************
//*	Function AttackOnManaShield(iCasterID, iAttackerID, iDamage,	*
//*						iBodyPartID, iWeaponID)		*
//*	iCasterID:		ID of the player with the Mana Shield spell		*
//*	iAttackerID:	ID of the player who attacked				*
//*	iDamage:		The damage that was done				*
//*	iBodyPartID:	The part on the body that was hit			*
//*	iWeaponID:	ID of the weapon which the attacker was using	*
//*												*
//*	Description:									*
//*	Shields the caster of some of the damage that a player inflicts	*
//***************************************************************
AttackOnManaShield(iCasterID, iAttackerID, iDamage, iBodyPartID, iWeaponID)
{
	new Float:fBonus = 0.0;
	if ( IsMageMaster(iCasterID, MANA_SHIELD) )
		fBonus = MANA_SHIELD_BONUS;

	new Float:fShieldPercentage = MANA_SHIELD_POWER + float(g_iArrPlSpellLvls[iCasterID][MANA_SHIELD]) * MANA_SHIELD_INCREASE + fBonus;
	
	new iDamageShielded = floatround( fShieldPercentage * float(iDamage) );
	if ( iDamageShielded > g_iArrPlayersMP[iCasterID] )
		iDamageShielded = g_iArrPlayersMP[iCasterID];
		
	g_iArrPlayersMP[iCasterID] -= iDamageShielded;	
	
	set_user_health(iCasterID, get_user_health(iCasterID) + iDamageShielded);

	if ( get_user_health(iCasterID) < ABNORMAL_HEALTH + 1 )
	{
		new iHeadShot = 0;
		if ( iBodyPartID == HIT_HEAD )
			iHeadShot = 1;
			
		DoDamage(iCasterID, iAttackerID, ABNORMAL_HEALTH + 1, iWeaponID, true, iHeadShot);
	}
}


//***************************************************************
//*	Function CastMindBlast(iCasterID)						*
//*	iCasterID:	The id of the player who triggered cast Mind Blast	*
//*												*
//*	Description:									*
//*	Casts Mind Blast on an area surounding the player aim point		*
//***************************************************************
CastMindBlast(iCasterID)
{
	new iVecTarget[3];
	get_user_origin(iCasterID, iVecTarget, GET_AIM_ENTITY_HIT);
	
	new nPlayers;
	new iArrPlayersID[32];

	get_players(iArrPlayersID, nPlayers, "a");

	new iTargetID;
	new iDistanceBetween;
	new iVecTargetOrigin[3];
	
	new bool:bFF = IsFriendlyFireOn();

	if ( file_exists("sound/battlemages/mind_blast.wav") )
		emit_sound(iCasterID, CHAN_ITEM, "battlemages/mind_blast.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);

	for (new iPlayer = 0; iPlayer < nPlayers; iPlayer++)
	{
		iTargetID = iArrPlayersID[iPlayer];
		get_user_origin(iTargetID, iVecTargetOrigin);
		iDistanceBetween = get_distance(iVecTarget, iVecTargetOrigin);

		if ( get_user_team(iCasterID) != get_user_team(iTargetID) || iCasterID == iTargetID || bFF)
		{
			if ( iDistanceBetween < MIND_BLAST_RANGE )
			{
				new args[6];
				args[0] = iCasterID;
				args[1] = iTargetID;
				if ( IsMageMaster(iCasterID, MIND_BLAST) )
				{
					args[2] = MIND_FLARE_INTERVALS;
					args[3] = MIND_FLARE_POWER;
					args[4] = MIND_FLARE_DURATION;
					args[5] = DMG_CAUSE_MIND_FLARE;
				}
				else
				{
					args[2] = MIND_BLAST_INTERVALS;
					args[3] = MIND_BLAST_POWER + g_iArrPlSpellLvls[iCasterID][MIND_BLAST] * MIND_BLAST_INCREASE;
					args[4] = MIND_BLAST_DURATION;
					args[5] = DMG_CAUSE_MIND_BLAST;
				}
				set_task(0.1, "ContinueMindBlast", MIND_BLAST_TASK_ID + iTargetID, args, 6);
			}
		}
	}
}


//***************************************************************
//*	Function ContinueMindBlast(args[])						*
//*	args[]:	Contains 6 variables, listed below				*
//*	0:	the id of the caster of the spell					*
//*	1:	the id of the target of the spell						*
//*	2:	the amount of intervals							*
//*	3:	the amount of damage							*
//*	4:	the duration between the intervals					*
//*	5:	the damage cause id							*
//*												*
//*	Description:									*
//*	Creates the DoT (Damage over Time) effect. Doing damage and	*
//*	shakes the screen.									*
//***************************************************************
public ContinueMindBlast(args[])
{
	new iTargetID = args[1];
	new iCasterID = args[0];

	if ( !is_user_alive(iTargetID) )
		return PLUGIN_CONTINUE;

	DoDamage(iTargetID, iCasterID, args[3], args[5]);

	if ( !is_user_alive(iTargetID) )
		return PLUGIN_CONTINUE;

	message_begin(MSG_ONE, g_iGameMsgScrShake, {0,0,0}, iTargetID);
	write_short(255<< 14); //ammount 
	write_short(10 << 14); //lasts this long 
	write_short(255<< 14); //frequency
	message_end();
	
	if (--args[2] > 0)
		set_task(float(args[4]), "ContinueMindBlast", MIND_BLAST_TASK_ID + iTargetID, args, 6);
	
	return PLUGIN_CONTINUE;
}


//***************************************************************
//*	Function CastSelfIllusion(iCasterID)					*
//*	iCasterID:	The id of the player who triggered cast Self Illusion	*
//*												*
//*	Description:									*
//*	Casts self illusion on the caster, changing the model and gives the	*
//*	caster a chance to score a critical hit with the knife.			*
//***************************************************************
CastSelfIllusion(iCasterID)
{
	if ( !g_bArrPlayersHasSelfIllusion[iCasterID] )
	{
		g_bArrPlayersHasSelfIllusion[iCasterID] = true;
		
		if (file_exists(SELF_ILLUSION_MODELPATH) && get_cvar_num(CVAR_USE_PREDATOR_MODEL) == USE_PREDATOR_MODEL)
			cs_set_user_model(iCasterID, SELF_ILLUSION_MODELNAME);	// Set it to the illusion model
		else
			cs_set_user_model(iCasterID, SELF_ILLUSION_MODELROBOT);	// if the ordinary doesnt exist, use the halflife robo model
		
		if ( file_exists("sound/battlemages/self_illusion.wav") )
			emit_sound(iCasterID, CHAN_ITEM, "battlemages/self_illusion.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
		return true;
	}
	return false;
}


//***************************************************************
//*	Function ResetModel(iCasterID)						*
//*	iCasterID:	the id of the player we are to reset model on		*
//*												*
//*	Description:									*
//*	Resets the model of the caster.	*
//***************************************************************
ResetModel(iCasterID)
{
	if ( g_bArrPlayersHasSelfIllusion[iCasterID] )
	{
		g_bArrPlayersHasSelfIllusion[iCasterID] = false;
		cs_reset_user_model(iCasterID);
	}
}


//***************************************************************
//*	Function AttackWithSelfIllusion(iAttackerID, iVictimID,		*
//*						iWeaponID, iDamage)			*
//*	iAttackerID:	id of the attack, the one who have self illusion	*
//*	iVictimID:		id of the victim						*
//*	iWeaponID:	id of the weapon, checks if its a knife		*
//*	iDamage:		the amount of damage done to the victim		*
//*												*
//*	Description:									*
//*	If a player has self illusion and dealt damage with a knife, then	*
//*	there is a chance the player will score a critical hit.			*
//***************************************************************
AttackWithSelfIllusion(iAttackerID, iVictimID, iWeaponID, iDamage)
{
	if ( iWeaponID == CSW_KNIFE )
	{
		new Float:fBonus = 0.0;
		
		// bmgeneral.inc
		if ( IsMageMaster(iAttackerID, SELF_ILLUSION) )
			fBonus = SELF_ILLUSION_BONUS;
		
		new Float:fCriticalChance = SELF_ILLUSION_POWER + float(g_iArrPlSpellLvls[iAttackerID][SELF_ILLUSION]) * SELF_ILLUSION_INCREASE + fBonus;
		if ( random_float(0.0, 1.0) < fCriticalChance )
		{
			// bmgeneralfuncs.inc
			DoDamage(iVictimID, iAttackerID, iDamage, DMG_CAUSE_CRITICAL_KNIFE);
		}
	}
}


//***************************************************************
//*	Function CastReplenishMana(iCasterID)					*
//*	iCasterID:	The id of the player who triggered cast Replenish Mana	*
//*												*
//*	Description:									*
//*	Casts replenish mana on caster, replenishes the mana pool		*
//***************************************************************
CastReplenishMana(iCasterID)
{
	if ( IsMageMaster(iCasterID, REPLENISH_MANA) )
	{
		if ( ! task_exists(MANA_REGENERATION_TASK_ID + iCasterID) )
		{
			if ( file_exists("sound/battlemages/replenish_mana.wav") )
				emit_sound(iCasterID, CHAN_ITEM, "battlemages/replenish_mana.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);

			new args[1];
			args[0] = iCasterID;
			set_task(MANA_REGENERATION_INTERVAL, "TaskManaRegeneration", MANA_REGENERATION_TASK_ID + iCasterID, args, 1);
			return true;
		}
	}
	else
	{
		if ( file_exists("sound/battlemages/replenish_mana.wav") )
			emit_sound(iCasterID, CHAN_ITEM, "battlemages/replenish_mana.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);

		new iMaxMP = get_cvar_num(CVAR_START_MAGIC_POINTS);
		g_iArrPlayersMP[iCasterID] += REPLENISH_MANA_POWER + g_iArrPlSpellLvls[iCasterID][REPLENISH_MANA] * REPLENISH_MANA_INCREASE;
		if ( g_iArrPlayersMP[iCasterID] > iMaxMP )
			g_iArrPlayersMP[iCasterID] = iMaxMP;
			
		return true;
	}
	return false;
}


//***************************************************************
//*	Function TaskManaRegeneration(args[])					*
//*	args[]:	contains 1 variable, the id of the caster			*
//*												*
//*	Description:									*
//*	Replenishes mana over time							*
//***************************************************************
public TaskManaRegeneration(args[])
{
	new iCasterID = args[0];
	new iMaxMP = get_cvar_num(CVAR_START_MAGIC_POINTS);

	g_iArrPlayersMP[iCasterID] += MANA_REGENERATION_POWER;
	if ( g_iArrPlayersMP[iCasterID] > iMaxMP )
		g_iArrPlayersMP[iCasterID] = iMaxMP;
	
	set_task(MANA_REGENERATION_INTERVAL, "TaskManaRegeneration", MANA_REGENERATION_TASK_ID + iCasterID, args, 1);
}

#endif
