//***************************************************************
//*	Battle Mages v. 1.0.1 for AMXX by Martin J. Van der Cal		*
//*												*
//*	Mod adding magic to the game.							*
//*												*
//*	Copyright (C) 2004, Martin J. Van der Cal					*
//*	This plugin is under the GNU General Public License, read		*
//*	"Battle Mages Readme.doc" for further information.			*
//*												*
//*	bmlife.inc										*
//*												*
//***************************************************************

#if !defined BATTLE_MAGES_LIFE_MAGIC
#define BATTLE_MAGES_LIFE_MAGIC

#include "bmconst"
#include "bmgvars"


//***************************************************************
//*	Function CastHealSelf(iCasterID)						*
//*	iCasterID:	The id of the player who triggered cast Heal Self		*
//*												*
//*	Description:									*
//*	Heals the caster and if the caster is a master, then it also removes	*
//*	bad effects.									*
//***************************************************************
CastHealSelf(iCasterID)
{
	IncreaseUserHealth(iCasterID, HEAL_SELF_POWER + g_iArrPlSpellLvls[iCasterID][HEAL_SELF] * HEAL_SELF_INCREASE);

	if ( IsMageMaster(iCasterID, HEAL_SELF) )
		RemoveBadEffects(iCasterID);

	if ( file_exists("sound/battlemages/cure.wav") )
		emit_sound(iCasterID, CHAN_ITEM, "battlemages/cure.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
}


//***************************************************************
//*	Function CastHealOther(iCasterID)						*
//*	iCasterID:	The id of the player who triggered cast Heal Other	*
//*												*
//*	Description:									*
//*	Heals a player that the caster has in target. If the caster is a master	*
//*	then it will also remove bad effects.						*
//***************************************************************
CastHealOther(iCasterID)
{
	new iTargetID, iBodyPartID;
	get_user_aiming(iCasterID, iTargetID, iBodyPartID);
	
	if ( 0 < iTargetID <= 32 )
	{
		if ( get_user_team(iCasterID) == get_user_team(iTargetID) )
		{
			IncreaseUserHealth(iTargetID, HEAL_OTHER_POWER + g_iArrPlSpellLvls[iCasterID][HEAL_SELF] * HEAL_OTHER_INCREASE);
			
			if ( IsMageMaster(iCasterID, HEAL_OTHER) )
				RemoveBadEffects(iTargetID);
		
			if ( file_exists("sound/battlemages/cure.wav") )
				emit_sound(iCasterID, CHAN_ITEM, "battlemages/cure.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);

			AddXP(iCasterID, BM_XP_HEAL_OTHER);
			
			return true;
		}
	}
	return false;
}


//***************************************************************
//*	Function CastShieldYlien(iCasterID)						*
//*	iCasterID:	The id of the player who triggered cast Shield of Y'lien	*
//*												*
//*	Description:									*
//*	Adds increased hp to the caster, and a damage shield if the caster	*
//*	is a master of the spell.								*
//***************************************************************
CastShieldYlien(iCasterID)
{
	if ( ! g_bArrPlayersHasShieldYlien[iCasterID] )
	{
		g_bArrPlayersHasShieldYlien[iCasterID] = true;
		if ( file_exists("sound/battlemages/shield.wav") )
			emit_sound(iCasterID, CHAN_ITEM, "battlemages/shield.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
	
		new iPower = SHIELD_YLIEN_POWER + g_iArrPlSpellLvls[iCasterID][SHIELD_YLIEN] * SHIELD_YLIEN_INCREASE;
	
		g_iArrPlayersMaxHP[iCasterID] += iPower;
		set_user_health(iCasterID, get_user_health(iCasterID) + iPower);

		// Adds a green aura indicating that this player is using Shield of Y'lien.
		set_user_rendering(iCasterID, kRenderFxGlowShell, 25, 255, 25, kRenderTransAlpha, SHIELD_YLIEN_GLOW);
		return true;
	}
	return false;
}


//***************************************************************
//*	Function AttackOnShieldYlien(iVictimID, iAttackerID, iDamage)	*
//*	iVictimID:	The player who has Shield of Y'lienelle active		*
//*	iAttacker:	The player who we will return damage at			*
//*	iDamage:	The amount of damage the attacker did to the victim	*
//*												*
//*	Description:									*
//*	Returns damage done to a caster with Shield of Y'lienelle active	*
//***************************************************************
AttackOnShieldYlien(iVictimID, iAttackerID, iDamage)
{
	if ( IsMageMaster(iVictimID, SHIELD_YLIEN) )
	{
		iDamage = floatround( float(iDamage) * SHIELD_YLIENELLE_POWER );
		DoDamage(iAttackerID, iVictimID, iDamage, DMG_CAUSE_SHIELD_YLIENELLE);
	}
}


//***************************************************************
//*	Function CastRegeneration(iCasterID)					*
//*	iCasterID:	The id of the player who triggered cast Regeneration	*
//*												*
//*	Description:									*
//*	Setups the caster with regeneration						*
//***************************************************************
bool:CastRegeneration(iCasterID)
{
	if ( !task_exists(REGENERATION_TASK_ID + iCasterID) )
	{
		if ( file_exists("sound/battlemages/regeneration.wav") )
			emit_sound(iCasterID, CHAN_ITEM, "battlemages/regeneration.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
	
		new args[2];
		args[0] = iCasterID;
		
		args[1] = 0;
		if ( IsMageMaster(iCasterID, REGENERATION) )
			args[1] += REGENERATION_BONUS;
	
		args[1] += REGENERATION_POWER + g_iArrPlSpellLvls[iCasterID][REGENERATION] * REGENERATION_INCREASE;
		
		set_task(REGENERATION_DURATION, "TaskRegeneration", REGENERATION_TASK_ID + iCasterID, args, 2);
		return true;
	}
	return false;
}


//***************************************************************
//*	Function TaskRegeneration(args[])						*
//*	args[]:	Contains the id of the caster only				*
//*												*
//*	Description:									*
//*	Regenerates the caster each interval, until the caster is dead		*
//***************************************************************
public TaskRegeneration(args[])
{
	new iCasterID = args[0];
	if ( is_user_alive(iCasterID) )
	{
		IncreaseUserHealth(iCasterID, args[1]);
		set_task(REGENERATION_DURATION, "TaskRegeneration", REGENERATION_TASK_ID + iCasterID, args, 2);
	}
}


//***************************************************************
//*	Function CastAvatar(iCasterID)						*
//*	iCasterID:	The id of the player who triggered cast Avatar		*
//*												*
//*	Description:									*
//*	Setups a player with avatar							*
//***************************************************************
CastAvatar(iCasterID)
{
	if ( ! g_bArrPlayersHasAvatar[iCasterID] )
	{
		if ( file_exists("sound/battlemages/avatar.wav") )
			emit_sound(iCasterID, CHAN_ITEM, "battlemages/avatar.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
			
		set_user_health(iCasterID, get_user_health(iCasterID) + ABNORMAL_HEALTH);
		g_bArrPlayersHasAvatar[iCasterID] = true;
		g_iArrPlayersMaxHP[iCasterID] += ABNORMAL_HEALTH;
		
		new args[1]
		args[0] = iCasterID;
		set_task(float(AVATAR_DURATION), "TaskEndAvatar", AVATAR_TASK_ID + iCasterID, args, 1);
		ShowDurationBar(iCasterID, AVATAR_DURATION);
		return true;
	}
	return false;
}


//***************************************************************
//*	Function AttackOnAvatar(iVictimID, iAttackerID, iDamage,		*
//*					iBodyPartID, iWeaponID)			*
//*	iVictimID:		The caster with avatar active				*
//*	iAttackerID:	The player who attacked the caster			*
//*	iDamage:		The damage done to the victim				*
//*	iBodyPartID:	The body part that was hit				*
//*	iWeaponID:	The weapon that was used				*
//*												*
//*	Description:									*
//*	Does the damage resistance part of the avatar spell			*
//***************************************************************
AttackOnAvatar(iVictimID, iAttackerID, iDamage, iBodyPartID, iWeaponID)
{
	new Float:fDamageResistance = 0.0;
	if ( IsMageMaster(iVictimID, AVATAR) )
		fDamageResistance += AVATAR_BONUS;
	fDamageResistance += AVATAR_POWER + g_iArrPlSpellLvls[iVictimID][AVATAR] * AVATAR_INCREASE;
	fDamageResistance *= float(iDamage);
	
	IncreaseUserHealth(iVictimID, floatround(fDamageResistance));
	
	if ( get_user_health(iVictimID) < ABNORMAL_HEALTH + 1 )
	{
		new iHeadShot = 0;
		if ( iBodyPartID == HIT_HEAD )
			iHeadShot = 1;
			
		DoDamage(iVictimID, iAttackerID, ABNORMAL_HEALTH + 1, iWeaponID, true, iHeadShot);
	}
}


//***************************************************************
//*	Function SpellResistanceAvatar(iAvatarID, &iDamage)			*
//*	iAvatarID:	The id of the player with avatar active			*
//*	&iDamage:	The damage done by a spell. New reduced value will	*
//*			be returned through this variable				*
//*												*
//*	Description:									*
//*	Adds resistance to spells. Returns the actual damage after the spell	*
//*	resistance has been calculated.						*
//***************************************************************
SpellResistanceAvatar(iAvatarID, &iDamage)
{
	new Float:fDamageResistance = 0.0;
	if ( IsMageMaster(iAvatarID, AVATAR) )
		fDamageResistance += AVATAR_BONUS;
	fDamageResistance += AVATAR_POWER + g_iArrPlSpellLvls[iAvatarID][AVATAR] * AVATAR_INCREASE;
	
	iDamage -= floatround( fDamageResistance * float(iDamage) );
}


//***************************************************************
//*	Function TaskEndAvatar(args[])						*
//*	args[]:	Only contains the id of the caster of the Avatar spell	*
//*												*
//*	Description:									*
//*	Ends the avatar spell. Player will take normal damage from here on.	*
//***************************************************************
public TaskEndAvatar(args[])
{
	new iCasterID = args[0];
	if ( is_user_alive(iCasterID) )
	{
		set_user_health(iCasterID, get_user_health(iCasterID) - ABNORMAL_HEALTH);
		g_bArrPlayersHasAvatar[iCasterID] = false;
		g_iArrPlayersMaxHP[iCasterID] -= ABNORMAL_HEALTH;
	}
}

#endif
