//***************************************************************
//*	Battle Mages v. 1.0.1 for AMXX by Martin J. Van der Cal		*
//*												*
//*	Mod adding magic to the game.							*
//*												*
//*	Copyright (C) 2004, Martin J. Van der Cal					*
//*	This plugin is under the GNU General Public License, read		*
//*	"Battle Mages Readme.doc" for further information.			*
//*												*
//*	bmxphandler.inc									*
//*												*
//***************************************************************

#if !defined BATTLE_MAGES_XP_HANDLER
#define BATTLE_MAGES_XP_HANDLER

#include <dbi>
#include "bmgvars"
#include "bmconst"

//  Connection variable
new Sql:dbConnection;


//***************************************************************
//*	Function InitializeDatabase()							*
//*												*
//*	Description:									*
//*	Initializing the database, connecting and making sure the table	*
//*	exists.										*
//***************************************************************
InitializeDatabase()
{
	// bmgeneralfuncs.inc IsSaveXPOn()
	if ( IsSaveXPOn() )
	{
		new sHost[64], sUser[32], sPassword[32], sDatabase[32], sError[MAX_ERROR_LENGTH];
		
		get_cvar_string(CVAR_DB_HOST,		sHost,		63);
		get_cvar_string(CVAR_DB_USERNAME,	sUser,		31);
		get_cvar_string(CVAR_DB_PASSWORD,	sPassword,	31);
		get_cvar_string(CVAR_DB_NAME,		sDatabase,	31);
		
		//server_print("[BM] Host: %s, Username: %s, Password: %s, Databasename: %s", sHost, sUser, s);
	
		new sQuery[MAX_QUERY_LENGTH];
		format(sQuery, MAX_QUERY_LENGTH-1, "CREATE TABLE IF NOT EXISTS %s ( SteamID VARCHAR(32) NOT NULL, SchoolID SMALLINT NOT NULL, Experience INT NOT NULL, CreationDate DATE NOT NULL, Spell1Level SMALLINT NOT NULL, Spell2Level SMALLINT NOT NULL, Spell3Level SMALLINT NOT NULL, Spell4Level SMALLINT NOT NULL, Spell5Level SMALLINT NOT NULL, PRIMARY KEY(SteamID, SchoolID) ) ", TABLE_NAME);
		
		dbConnection = dbi_connect(sHost, sUser, sPassword, sDatabase, sError, MAX_ERROR_LENGTH-1);
	
		if ( sError[0] )
			server_print("[BM] [DB ERROR] %s", sError);
		else
		{
			dbi_query(dbConnection, sQuery);
			dbi_error(dbConnection, sError, MAX_ERROR_LENGTH-1);
	
			if ( sError[0] )
				server_print("[BM] [QUERY ERROR] %s", sError);
		}
	}
}


//***************************************************************
//*	Function SavePlayerLevel(iPlayerID, iSchoolID)				*
//*	iPlayerID:	The id of the player to be saved				*
//*	iSchoolID:	The id of the school which shall be saved			*
//*												*
//*	Description:									*
//*	Saves a given player with given school id to the database		*
//***************************************************************
SavePlayerLevel(iPlayerID, iSchoolID)
{
	// bmgeneralfuncs.inc IsSaveXPOn()
	if ( IsSaveXPOn() )
	{
		if (WARRIOR < iSchoolID <= NUMBER_OF_SCHOOLS)
		{
			#if defined DEBUG_SERVER_PRINT
				server_print("[DEBUG][BM] XP at the start of SavePlayerLevel: %d", g_iArrPlCurXP[iPlayerID]);
			#endif
		
			new sSteamID[32], sError[MAX_ERROR_LENGTH];
			get_user_authid(iPlayerID, sSteamID, 31);
			
			new sQuery[MAX_QUERY_LENGTH];
			format(sQuery, MAX_QUERY_LENGTH-1, "UPDATE %s SET Experience = %d, Spell1Level = %d, Spell2Level = %d, Spell3Level = %d, Spell4Level = %d, Spell5Level = %d WHERE SteamID = '%s' AND SchoolID = %d",
				TABLE_NAME, g_iArrPlCurXP[iPlayerID], g_iArrPlSpellLvls[iPlayerID][0], g_iArrPlSpellLvls[iPlayerID][1],
				g_iArrPlSpellLvls[iPlayerID][2], g_iArrPlSpellLvls[iPlayerID][3], g_iArrPlSpellLvls[iPlayerID][4], sSteamID, iSchoolID);
		
			#if defined DEBUG_SERVER_PRINT
				server_print("[DEBUG][BM] (PlayerID: %d) Sending query to db: %s", iPlayerID, sQuery);
			#endif
		
			dbi_query(dbConnection, sQuery);
			dbi_error(dbConnection, sError, MAX_ERROR_LENGTH-1);
			
			if ( sError[0] )
				server_print("[BM] [QUERY ERROR] %s", sError);
		}
		#if defined DEBUG_SERVER_PRINT
		else
			server_print("[DEBUG][BM] Cant save player xp. Player is Warrior");
		#endif
	}
}


//***************************************************************
//*	Function LoadPlayerLevel(iPlayerID, iSchoolID)				*
//*	iPlayerID:	The id of the player to be loaded				*
//*	iSchoolID:	The id of the school which shall be loaded			*
//*												*
//*	Description:									*
//*	Loads a given player with given school id from the database		*
//***************************************************************
LoadPlayerLevel(iPlayerID, iSchoolID)
{
	// bmgeneralfuncs.inc IsSaveXPOn()
	if ( IsSaveXPOn() )
	{
		if (iSchoolID == 0)
			return;
		
		g_iArrPlCurXP[iPlayerID] = 0;
		
		new sSteamID[32], sError[MAX_ERROR_LENGTH];
		get_user_authid(iPlayerID, sSteamID, 31);
		
		new sQuery[MAX_QUERY_LENGTH];
		format(sQuery, MAX_QUERY_LENGTH-1, "SELECT * FROM %s WHERE SteamID = '%s' AND SchoolID = %d", TABLE_NAME, sSteamID, iSchoolID);
		
		#if defined DEBUG_SERVER_PRINT
			server_print("[DEBUG][BM] (PlayerID: %d) Sending query to db: %s", iPlayerID, sQuery);
		#endif
	
		new Result:rsSelectPlayer;
		rsSelectPlayer = dbi_query(dbConnection, sQuery);
		dbi_error(dbConnection, sError, MAX_ERROR_LENGTH-1);
		
		if ( sError[0] )
		{
			server_print("[BM] [QUERY ERROR] %s", sError);
		
			g_iArrPlCurXP[iPlayerID] = 0;
			g_iArrPlSpellLvls[iPlayerID][0] = 0;
			g_iArrPlSpellLvls[iPlayerID][1] = 0;
			g_iArrPlSpellLvls[iPlayerID][2] = 0;
			g_iArrPlSpellLvls[iPlayerID][3] = 0;
			g_iArrPlSpellLvls[iPlayerID][4] = 0;
			return;
		}
		
		if ( rsSelectPlayer != RESULT_NONE && dbi_nextrow(rsSelectPlayer) > 0 )
		{
			#if defined DEBUG_SERVER_PRINT
				server_print("[DEBUG][BM] Found player, loading data...");
			#endif
	
			g_iArrPlCurXP[iPlayerID]					= dbi_result(rsSelectPlayer, "Experience");
			g_iArrPlSpellLvls[iPlayerID][SPELL_ONE]		= dbi_result(rsSelectPlayer, "Spell1Level");
			g_iArrPlSpellLvls[iPlayerID][SPELL_TWO]		= dbi_result(rsSelectPlayer, "Spell2Level");
			g_iArrPlSpellLvls[iPlayerID][SPELL_THREE]	= dbi_result(rsSelectPlayer, "Spell3Level");
			g_iArrPlSpellLvls[iPlayerID][SPELL_FOUR]	= dbi_result(rsSelectPlayer, "Spell4Level");
			g_iArrPlSpellLvls[iPlayerID][SPELL_FIVE]	= dbi_result(rsSelectPlayer, "Spell5Level");
			
			#if defined DEBUG_SERVER_PRINT
				server_print("[DEBUG][BM] Data loaded: XP: %d, lvl1: %d, lvl2: %d, lvl3: %d, lvl4: %d, lvl5: %d",
					g_iArrPlCurXP[iPlayerID], g_iArrPlSpellLvls[iPlayerID][SPELL_ONE], g_iArrPlSpellLvls[iPlayerID][SPELL_TWO],
					g_iArrPlSpellLvls[iPlayerID][SPELL_THREE], g_iArrPlSpellLvls[iPlayerID][SPELL_FOUR], g_iArrPlSpellLvls[iPlayerID][SPELL_FIVE]);
			#endif
		}
		else
		{
			#if defined DEBUG_SERVER_PRINT
				server_print("[DEBUG][BM] Did not find player, loading default data...");
			#endif
		
			g_iArrPlCurXP[iPlayerID] = 0;
			g_iArrPlSpellLvls[iPlayerID][0] = 0;
			g_iArrPlSpellLvls[iPlayerID][1] = 0;
			g_iArrPlSpellLvls[iPlayerID][2] = 0;
			g_iArrPlSpellLvls[iPlayerID][3] = 0;
			g_iArrPlSpellLvls[iPlayerID][4] = 0;
	
			format(sQuery, MAX_QUERY_LENGTH-1, "INSERT INTO %s VALUES('%s', %d, 0, CURDATE(), 0, 0, 0, 0, 0)", TABLE_NAME, sSteamID, iSchoolID);
			
			#if defined DEBUG_SERVER_PRINT
				server_print("[DEBUG][BM] Sending query to db: %s", sQuery);
			#endif
			
			dbi_query(dbConnection, sQuery);
			dbi_error(dbConnection, sError, MAX_ERROR_LENGTH-1);
			
			if ( sError[0] )
				server_print("[BM] [QUERY ERROR] %s", sError);
		}
		
		dbi_free_result(rsSelectPlayer);
		
		new iLevel = 0;
		while ( g_iArrPlCurXP[iPlayerID] >= g_iArrXPLevels[iLevel] )
			iLevel++;
		
		g_iArrPlCurLevel[iPlayerID] = iLevel;
		
		g_iArrPlNextLevel[iPlayerID] = g_iArrXPLevels[g_iArrPlCurLevel[iPlayerID]];
		
		#if defined DEBUG_SERVER_PRINT
			server_print("[DEBUG][BM] XP at the end of LoadPlayerLevel: %d", g_iArrPlCurXP[iPlayerID]);
		#endif
	}
	else
	{
		g_iArrPlCurXP[iPlayerID] = 0;
		g_iArrPlSpellLvls[iPlayerID][0] = 0;
		g_iArrPlSpellLvls[iPlayerID][1] = 0;
		g_iArrPlSpellLvls[iPlayerID][2] = 0;
		g_iArrPlSpellLvls[iPlayerID][3] = 0;
		g_iArrPlSpellLvls[iPlayerID][4] = 0;
	}
}


//***************************************************************
//*	Function AddXP(iPlayerID, iXPType, iEnemyID = 0)			*
//*	iPlayerID:	The id of the player to whom we will grant xp		*
//*	iXPType:	The type of XP that we grant them				*
//*	iEnemyID:	The id of the enemey the player killed, only used with	*
//*			BM_XP_KILL							*
//*												*
//*	Description:									*
//*	Adds XP to a given player depending on what the player did to gain	*
//*	the XP.										*
//***************************************************************
AddXP(iPlayerID, iXPType, iEnemyID = 0)
{
	if ( iPlayerID != iEnemyID )
	{
		new iBaseXP = 0;
		switch( iXPType )
		{
			case BM_XP_KILL:
				iBaseXP = g_iArrXPTypes[BM_XP_KILL] + g_iArrPlCurLevel[iEnemyID] - (BM_MAX_LEVEL / 2);
			case BM_XP_WINROUND:
				iBaseXP = g_iArrXPTypes[BM_XP_WINROUND] + g_iArrPlCurLevel[iPlayerID] - (BM_MAX_LEVEL / 2);
			case BM_XP_HEAL_OTHER:
				iBaseXP = g_iArrXPTypes[BM_XP_HEAL_OTHER] + g_iArrPlCurLevel[iPlayerID] - (BM_MAX_LEVEL / 2);
		}
		
		if ( iBaseXP > 0 )
		{
			g_iArrPlCurXP[iPlayerID] += iBaseXP + ( iBaseXP * g_iArrPlCurLevel[iPlayerID] ) / XP_REGULATOR;
		
			if ( g_iArrPlCurXP[iPlayerID] >= g_iArrPlNextLevel[iPlayerID] )
			{
				g_iArrPlCurLevel[iPlayerID]++;
				g_iArrPlNextLevel[iPlayerID] = g_iArrXPLevels[g_iArrPlCurLevel[iPlayerID]];
	
				if ( file_exists("sound/battlemages/winlevel.wav") )
					emit_sound(iPlayerID, CHAN_ITEM, "battlemages/winlevel.wav", 1.0, ATTN_NORM, 0, PITCH_NORM);
			}
		}
	}
}

#endif
