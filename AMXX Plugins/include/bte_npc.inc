native KVCS_IsEntityNpc(id)
native KVCS_RegisterNpcClass(name[],model[],health,speed,sex,modelindex,money,skilltime, round_lv,damage)
native bte_npc_get_class(iEnt)
native bte_npc_set_think(iEnt, think)
native bte_npc_get_think(iEnt)

forward KVCS_NpcCorpseRemove(ent)
forward bte_npc_think(iEnt)
forward bte_npc_create(iEnt)
forward bte_npc_dead(iEnt)

enum (+=1) {		
	THINK_MOVE = 1,
	THINK_DEATH,
	THINK_SKILL_TANK,
	THINK_SKILL_SPEED
}
#define ANIM_DEAD 2
#define ANIM_ATTACK_RUN 14
#define ANIM_ATTACK_WALK 13
#define ANIM_SoldierShoot 7

enum (+=1) {		
	ANIM_IDLE = 3,
	ANIM_WALK,
	ANIM_RUN

}
enum (+=1) {		
	MALE = 0,
	FEMALE
}