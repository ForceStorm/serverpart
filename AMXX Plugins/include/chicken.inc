/* ADDITIONAL INCS */

#include <amxmodx> 
#include <amxmisc>
#include <lang_default>
#include <fun>

/* CHICKEN FLAGS */

new bool:UserFlags[33]


/* NAME DATA */

new ChickName[33][32]
new UserOldName[33][32]
new bool:ChickenName = true
//----------------------------------------------------------------------------------------------
/* CHICKEN CODE */

chicken_user(id)
{
	if(UserFlags[id] == false && is_user_alive(id))
	{
		UserFlags[id] = true
		
		if(get_cvar_num("amx_chicken_sfx")) 
		{
			new origin[3]
			get_user_origin(id, origin)
			transform(origin)
		}
		if(!is_user_bot(id))
		{
			new user[33]
			get_user_name(id, user, 32)
			copy(UserOldName[id], 32, user)
			format(ChickName[id], 32, "Chicken #%i", id)
			
			if(ChickenName)
				 set_user_info(id, "name", ChickName[id])
		}
		server_cmd("c_chicken # %i", id)
		set_hitzones(id)
		set_vision(id)
	}
	return PLUGIN_HANDLED
}
//----------------------------------------------------------------------------------------------
/* UNCHICKEN CODE */

unchicken_user(id)
{
	if(UserFlags[id] && is_user_alive(id))
	{
		UserFlags[id] = false
		
		if(get_cvar_num("amx_chicken_sfx"))
		{
			new origin[3]
			get_user_origin(id, origin)
			transform(origin)
		}
		if(ChickenName && !is_user_bot(id))
			 set_user_info(id, "name", UserOldName[id])
		
		server_cmd("c_unchicken # %i", id)
		set_hitzones(id)
		set_vision(id)
	}
	return PLUGIN_HANDLED
}
//----------------------------------------------------------------------------------------------
/* HITZONES DATA */

public set_hitzones(id)
{
	if(UserFlags[id])
		set_user_hitzones(0, id, 192)
	else
		set_user_hitzones(0, id, 255)
}
//----------------------------------------------------------------------------------------------
/* VISION DATA */

new gmsgSetFOV
new ChickenVision = 135

set_vision(id)
{
	if(UserFlags[id])
	{
		message_begin(MSG_ONE, gmsgSetFOV, {0,0,0}, id)
		write_byte(ChickenVision)
		message_end()
	}
	else
	{
		message_begin(MSG_ONE, gmsgSetFOV, {0,0,0}, id)
		write_byte(90) // default_fov = 90
		message_end()
	}
}
//----------------------------------------------------------------------------------------------
/* CHICKEN SFX */

transform(vec[3])
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
	write_byte(11) // TE_TELEPORT
	write_coord(vec[0])
	write_coord(vec[1])
	write_coord(vec[2])
	message_end()
}