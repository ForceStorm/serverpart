/*                   *       ━
			    ┃┃
       *   //---------------┃┃-\\    *
	  //   AmxxStart 汉化版   \\  
	 //  www.csamxx.host.org   \\      
    *   ||─────────────||    *
	*    AMX Mod X functions     *
	*     by the AMX Mod X       *
	*     Development Team       *
	||  ┏┳┓		    || *
	||  ┣╋┫       ┅┅┅  ┄ ||
	||  ┗┻┛    ┄┄┄┄┄┄┄||    
	||	    ┉┉┉┉┉┉┉┉||
	───────────────
*/

//
#if defined _cstrike_included
  #endinput
#endif
#define _cstrike_included

#if AMXX_VERSION_NUM >= 175
 #pragma reqlib cstrike
 #if !defined AMXMODX_NOAUTOLOAD
  #pragma loadlib cstrike
 #endif
#else
 #pragma library cstrike
#endif
//
/* 返回玩家死亡数.
 */
native cs_get_user_deaths(index);

/* 设置玩家死亡数.
 */
native cs_set_user_deaths(index, newdeaths);

/* 返回被人质追随的玩家.
 */
native cs_get_hostage_foll(index);

/* 设置追随玩家的人质.
 */
native cs_set_hostage_foll(index, followedindex = 0);

/* 返回人质的索引.
 */
native cs_get_hostage_id(index);

/* 返回玩家的后备弹药
 * 弹药类型前缀: CSW_*.(详见amxconst.inc)
 */
native cs_get_user_bpammo(index, weapon);

/* 设置玩家的后备弹药.
 */
native cs_set_user_bpammo(index, weapon, amount);

/* 返回玩家是否携带拆弹工具.
 */
native cs_get_user_defuse(index);

/* 设置玩家拆弹工具,index是玩家ID索引.
 * rgb是图标颜色,icon是拆雷工具图标,flash是图标状态.
 */
native cs_set_user_defuse(index, defusekit = 1, r = 0, g = 160, b = 0, icon[] = "defuser", flash = 0);

/* 返回玩家是否在购买区域.
 */
native cs_get_user_buyzone(index);

/* 返回玩家是否有主武器或者防爆盾.
 */
native cs_get_user_hasprim(index);

/* 返回玩家当前模型.
 */
native cs_get_user_model(index, model[], len);

/* 设置玩家当前模型.
 */
native cs_set_user_model(index, const model[]);

/* 重设玩家模型.
 */
native cs_reset_user_model(index);

/* 返回玩家金钱数.
 */
native cs_get_user_money(index);

/* 设置玩家金钱数.
 */
native cs_set_user_money(index, money, flash = 1);

/* 返回玩家是否携带夜视仪
 */
native cs_get_user_nvg(index);

/* 设置玩家携带夜视仪
 */
native cs_set_user_nvg(index, nvgoggles = 1);

/* 返回玩家是否能下包.
 */
native cs_get_user_plant(index);

/* 设置玩家能否下包.
 */
native cs_set_user_plant(index, plant = 1, showbombicon = 1);

/* 设置玩家队伍以及模型.
 */
enum CsInternalModel {
	CS_DONTCHANGE = 0,
	CS_CT_URBAN = 1,
	CS_T_TERROR = 2,
	CS_T_LEET = 3,
	CS_T_ARCTIC = 4,
	CS_CT_GSG9 = 5,
	CS_CT_GIGN = 6,
	CS_CT_SAS = 7,
	CS_T_GUERILLA = 8,
	CS_CT_VIP = 9,
	CZ_T_MILITIA = 10,
	CZ_CT_SPETSNAZ = 11
};
native cs_set_user_team(index, {CsTeams,_}:team, {CsInternalModel,_}:model = CS_DONTCHANGE);

/* 返回玩家的当前队伍.
 * 1 = terrorist
 * 2 = counter-terrorist
 * 3 = spectator
 */
enum CsTeams {
	CS_TEAM_UNASSIGNED = 0,
	CS_TEAM_T = 1,
	CS_TEAM_CT = 2,
	CS_TEAM_SPECTATOR = 3
};
native CsTeams:cs_get_user_team(index, &{CsInternalModel,_}:model = CS_DONTCHANGE);

/* 返回玩家是否为VIP.(VIP游戏模式)
 */
native cs_get_user_vip(index);

/* 设置玩家成为VIP.
 */
 native cs_set_user_vip(index, vip = 1, model = 1, scoreboard = 1);

/* 返回团队杀敌数.
 */
native cs_get_user_tked(index);

/* 设置团队杀敌数.
 */
native cs_set_user_tked(index, tk = 1, subtract = 1);

/* 返回玩家是否有推进器.(HL1中的长跳包)
 */
native cs_get_user_driving(index);

/* 返回玩家是否有防护盾.
 */
native cs_get_user_shield(index);

/* 返回静止不动的玩家.
 */
native cs_get_user_stationary(index);

/* 返回玩家的护甲类型.
 */
enum CsArmorType {
	CS_ARMOR_NONE = 0, // no armor
	CS_ARMOR_KEVLAR = 1, // armor
	CS_ARMOR_VESTHELM = 2 // armor and helmet
};
native cs_get_user_armor(index, &CsArmorType:armortype);

/* 设置玩家的护甲类型.
 */
native cs_set_user_armor(index, armorvalue, CsArmorType:armortype);

/* 返回玩家武器的模式.(比如:爆炸开火模式)
 */
native cs_get_weapon_burst(index);

/* 设置玩家武器的模式.
 */
native cs_set_weapon_burst(index, burstmode = 1);

/* 返回玩家武器是否装载消音器.
 */
native cs_get_weapon_silen(index);

/* 设置玩家武器是否装载消音器.
 */
native cs_set_weapon_silen(index, silence = 1, draw_animation = 1);

/* 返回玩家弹夹中的弹药数.
 */
native cs_get_weapon_ammo(index);

/* 设置玩家弹夹中的弹药数.
 */
native cs_set_weapon_ammo(index, newammo);

/* 返回玩家的武器类型.(详见amxconst.inc)
 */
native cs_get_weapon_id(index);

/* 返回是否存在无重力模式.
 */
native cs_get_no_knives();

/* 设置是否无重力模式.
 */
native cs_set_no_knives(noknives = 0);

/* 复活玩家
 */
native cs_user_spawn(player);

/* 返回武器类型.
 */
native cs_get_armoury_type(index);

/* 设置玩家的武器类型.
 * 武器类型: CSW_MP5NAVY, CSW_TMP, CSW_P90, CSW_MAC10, CSW_AK47, CSW_SG552, CSW_M4A1, CSW_AUG, CSW_SCOUT
 * CSW_G3SG1, CSW_AWP, CSW_M3, CSW_XM1014, CSW_M249, CSW_FLASHBANG, CSW_HEGRENADE, CSW_VEST, CSW_VESTHELM, CSW_SMOKEGRENADE
 */
native cs_set_armoury_type(index, type);

#define CS_MAPZONE_BUY 			(1<<0)
#define CS_MAPZONE_BOMBTARGET 		(1<<1)
#define CS_MAPZONE_HOSTAGE_RESCUE 	(1<<2)
#define CS_MAPZONE_ESCAPE		(1<<3)
#define CS_MAPZONE_VIP_SAFETY 		(1<<4)

/* 返回玩家可下包区域.
 */
native cs_get_user_mapzones(index);

/* Zoom type enum. Used for get/set_user_zoom() natives.
 */
enum
{
	CS_RESET_ZOOM = 0,		// 无效果
	CS_SET_NO_ZOOM,			// 无效果
	CS_SET_FIRST_ZOOM,		// 狙击镜(小)
	CS_SET_SECOND_ZOOM,		// 狙击镜(大)
	CS_SET_AUGSG552_ZOOM,		// aug/sg552
};
/* 设置武器的开镜类型.
 */
native cs_set_user_zoom(index, type, mode);

/* 返回玩家当前开镜状态.
 */
native cs_get_user_zoom(index);

/* 返回玩家模型是否有背包
 */
native cs_get_user_submodel(index);

/* 设置玩家的背包模型状态.
 */
native cs_set_user_submodel(index, value);

/* 返回/设置玩家的上一个动作.
 */
native Float:cs_get_user_lastactivity(index);

native cs_set_user_lastactivity(index, Float:value);

/* 返回/设置玩家杀死的人质编号.
 */
native cs_get_user_hostagekills(index);

native cs_set_user_hostagekills(index, value);

/* 返回/设置人质上一次追随的玩家.
 */
native Float:cs_get_hostage_lastuse(index);

native cs_set_hostage_lastuse(index, Float:value);

/* 返回/设置人质下一次追随的玩家.
 */
native Float:cs_get_hostage_nextuse(index);

native cs_set_hostage_nextuse(index, Float:value);

/* 返回/设置C4的爆炸时间.
 */
native Float:cs_get_c4_explode_time(index);

native cs_set_c4_explode_time(index, Float:value);

/* 返回/设置一个C4.
 */
native bool:cs_get_c4_defusing(c4index);

native cs_set_c4_defusing(c4index, bool:defusing);
/**
 * 执行某个指令.
 */
forward CS_InternalCommand(id, const cmd[]);
