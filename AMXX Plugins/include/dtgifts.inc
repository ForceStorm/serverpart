/*DT Gift 插件函数.
*版本 1.1F
*作者 ahcat
*欢迎访问: http://forum.dt-club.net
*/

#if defined _DT_GIFT_INCLUDED
	#endinput
#endif
#define _DT_GIFT_INCLUDED

#define DTGIFTS_VERSION "1.1F"
#define DTGIFTS_AUTHOR "ahcat"

/*添加一个新礼物, 示例插件: dtgift_money.sma
*使用方法: register_gift( 公共函数的名称, 礼物的名称, 礼物的效果, 获得礼物的几率)
*你可以在 plugin_init 里调用此函数.
*然后声明一个公共函数
*例如:
*public get_money( id){
*
*}
*参数 id 是玩家的索引, 在 1 到 32 之间.
*返回礼物的内部索引.*/
native register_gift( handler[], name[], info[], percent=30);

/*添加一个新礼物, 和 register_gift 函数不同的是
*这个函数的 name 和 info 是语言文件 dt_gift.txt 里的字段名.
*只是为了方便用 AMXX Studio 的朋友们.
*如无特殊需要可使用 register_gift 函数.*/
native register_gift_l( handler[], name[], info[], percent=30);

/*在某个坐标生成一个礼物*/
native dg_make_gift( origin[3]);

/*通过函数名称和插件名称返回礼物的内部索引.
*找不到插件返回 -2, 找不到函数返回 -1.*/
native dg_find_giftid( function[], pluginfile[]);

/*通过礼物的内部索引设置礼物的几率.*/
native dg_set_chance( giftid, percent);

/*让某个玩家获得某个礼物*/
native dg_get_gift( id, giftid);
/*-----------------函数部分结束--------------*/

/*以下是 1.71 或以上才能使用的.*/
#if AMXX_VERSION_NUM>= 171

/*当玩家碰到礼物时触发
*参数 id 是玩家的索引, 在 1 到 32 之间.
*可用 return PLUGIN_HANDLED 来中止.*/
forward dg_touchgift( id);

/*当发生刷新礼物提示时触发, 比如: 在地图的某个地方, 出现了一份礼物.
*可用 return PLUGIN_HANDLED 来中止.
*中止时连声音也一并中止.*/
forward dg_notice_showgift()

/*当发生打开礼物提示时触发, 比如: XX 打开了箱子, 得到了 XXX. 
*比如: XX 打开了箱子, 可是里面空空如也.
*可用 return PLUGIN_HANDLED 来中止.
*中止时连声音也一并中止.
*id 是玩家的索引, 在 1 到 32 之间.
*如果礼物为空则 giftid = -1, 否则返回礼物的内部索引.*/
forward dg_notice_getgift( id, giftid);

#endif
