/*					THIS IS A TEST VERSION !!!!!!!!!!!!!!!
							FINAL FANTASY 10
							      v0.85
							      
							      v.051
							   Originally
						  	  By Z4KN4F31N
					MOdified and Ported to Amxx By Timmi
					Changed Saving and Loading.
					THIS IS A TEST COPY!!!!!!!!!!!!!!!!!!!!!
*/


// Death section called on death events
new spawntimer[33] = 70
new spawnleft, spawnlimit

public deathmatchspawn(id) {
	new spawn_limit = get_playersnum() - 4
	if (spawn_limit <= 1) spawnlimit = 2
	set_hudmessage(200,200,200,-1.0,0.30,0,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
	if (spawntimer[id] >= 1) {
		spawntimer[id] = spawntimer[id] - 1
		show_hudmessage(id, "You are spawning in %d", spawntimer[id])
	}
	if (spawntimer[id] <= 0) show_hudmessage(id, "Not enough Players have died")
	if (spawnleft >= spawnlimit) {
		if (spawntimer[id] >= 750) spawnme(id)
		return PLUGIN_CONTINUE
	}

	deathmatchspawn(id)
	return PLUGIN_CONTINUE
}
	
public deathfunct() {
	if (ffxenable==false) return PLUGIN_CONTINUE
	new attacker_id
	new victim_id
	new headshot
	new randomgil = random_num(50,GILPERWINRANDOMIZER)
	//new use1
	attacker_id = read_data(1)
	victim_id = read_data(2)
	headshot = read_data(3)
	
	client_cmd(victim_id,"mp3 stop")
  	client_cmd(victim_id,";mp3 play /sound/ffx/ffx_deathmusic.mp3") 
	client_cmd(victim_id,"mp3 play /sound/fxx/ffx_deathmusic.mp3")
	if (attacker_id == 0) return PLUGIN_CONTINUE
	if (victim_id == 0) return PLUGIN_CONTINUE
	if (victim_id > get_playersnum()) return PLUGIN_CONTINUE
	if (attacker_id > get_playersnum()) return PLUGIN_CONTINUE

	if (victim_id!=attacker_id) dropstuff(victim_id)
	if (quest[attacker_id]==1) {
		kills[attacker_id]=kills[attacker_id]+1
		set_hudmessage(250,250,100,1.0,0.01,0,6.0,2.0,0.1,0.5,HMCHAN_GENERAL)
		show_hudmessage(attacker_id, "Kills %d/10", kills[attacker_id])
	}
	if (i_level[attacker_id]>=10) {
		if (i_level[victim_id]<=10) {
		XP[attacker_id]=XP[attacker_id]+100
		checklvl(attacker_id)
		}
	}
	gil[attacker_id]=gil[attacker_id]+randomgil
	if (weaponnum[attacker_id]==0) {
		generalrandom=random_num(1,WEAPONRANDOMIZER)
		if (generalrandom<=5) {
			weaponnum[attacker_id]=1
			halfmp[attacker_id]=1
		}
		if (generalrandom>5) {
			if (generalrandom<=10) {
				weaponnum[attacker_id]=2
				counterattack[attacker_id]=1
			}
		}
		if (generalrandom>10) {
			if (generalrandom<=13) {
				weaponnum[attacker_id]=3
				onemp[attacker_id]=1
			}
		}
		if (generalrandom>13) {
			if (generalrandom<=20) {
				weaponnum[attacker_id]=4
				petrifyitm[attacker_id]=1
			}
		}
		if (generalrandom>20) {
			if (generalrandom<=23) {
				weaponnum[attacker_id]=5
				deathitm[attacker_id]=1
			}
		}
		if (generalrandom>24) {
			if (generalrandom<=28) {
				weaponnum[attacker_id]=6
				deathitm[attacker_id]=1
			}
		} 
		if (generalrandom>28) {
			if (generalrandom<=38) {
				weaponnum[attacker_id]=7
				petrifyitm[attacker_id]=1
			}
		}
		if (generalrandom>38) {
			if (generalrandom<=50) {
				weaponnum[attacker_id]=8
				slow[attacker_id]=1
			}
		}
		if (generalrandom>50) {
			if (generalrandom<=52) {
				weaponnum[attacker_id]=9
				counterattack[attacker_id]=1
				magiccounter[attacker_id]=1
			}
		}
		if (generalrandom>52) {
			if (generalrandom<=53) {
				weaponnum[attacker_id]=10
				celestial[attacker_id]=1
			}
		}
		if (generalrandom>=1) {
		set_hudmessage(5,250,5,-1.0,0.30,0,6.0,5.0,0.1,0.5,HMCHAN_GENERAL)
		if (generalrandom<=53) show_hudmessage(attacker_id, "Tidus found a weapon!")
		}
		generalrandom=random_num(1,WEAPONRANDOMIZER)
		if (generalrandom<=3) {
			weaponnum[attacker_id]=11
			onemp[attacker_id]=1
		}
		if (generalrandom>3) {
			if (generalrandom<=10) {
				weaponnum[attacker_id]=12
				poisonitm[attacker_id]=1
			}
		}
		if (generalrandom>10) {
			if (generalrandom<=17) {
				weaponnum[attacker_id]=13
				poisonitm[attacker_id]=1
			}
		}
		if (generalrandom>17) {
			if (generalrandom<=19) {
				weaponnum[attacker_id]=14
				silenceitm[attacker_id]=1
				slow[attacker_id]=1
				darknessitm[attacker_id]=1
				poisonitm[attacker_id]=1
			}
		}
		if (generalrandom>19) {
			if (generalrandom<=24) {
				weaponnum[attacker_id]=15
				deathitm[attacker_id]=1
			}
		}
		if (generalrandom>24) {
			if (generalrandom<=29) {
				weaponnum[attacker_id]=16
				gillionaire[attacker_id]=1
			} 
		}
		if (generalrandom>29) {
			if (generalrandom<=38) {
				slow[attacker_id]=1
			}
		}
		if (generalrandom>38) {
			if (generalrandom<=42) {
				weaponnum[attacker_id]=18
				halfmp[attacker_id]=1
			}
		}
		if (generalrandom>42) {
			if (generalrandom<=43) {
				weaponnum[attacker_id]=19
				celestial[attacker_id]=1
			}
		}
		if (generalrandom>43) {
			if (generalrandom<=45) {
				weaponnum[attacker_id]=20
				magiccounter[attacker_id]=1
			}
		}
		if (generalrandom>45) {
			if (generalrandom<=49) {
				weaponnum[attacker_id]=21
				deathitm[attacker_id]=1
			}
		}
		if (generalrandom>49) {
			if (generalrandom<=53) {
				weaponnum[attacker_id]=22
				silenceitm[attacker_id]=1
			}
		}
		if (generalrandom>=1) {
			set_hudmessage(5,250,5,-1.0,0.30,0,6.0,5.0,0.1,0.5,HMCHAN_GENERAL)
			if (generalrandom<=53) show_hudmessage(attacker_id, "Yuna found a weapon!")
		}
		generalrandom=random_num(1,WEAPONRANDOMIZER)
		if (generalrandom<=10) {
			weaponnum[attacker_id]=23
			STRbonus[attacker_id]=STRbonus[attacker_id]+1
			STR[attacker_id]=STR[attacker_id]+STRbonus[attacker_id]
		}
		if (generalrandom>10) {
			if (generalrandom<=14) {
				weaponnum[attacker_id]=24
				darknessitm[attacker_id]=1
			}
		}
		if (generalrandom>14) {
			if (generalrandom<=17) {
				weaponnum[attacker_id]=25
				silenceitm[attacker_id]=1
				slow[attacker_id]=1
				poisonitm[attacker_id]=1
			}
		}
		if (generalrandom>17) {
			if (generalrandom<=27) {
				weaponnum[attacker_id]=26
				STRbonus[attacker_id]=STRbonus[attacker_id]+1
				STR[attacker_id]=STR[attacker_id]+STRbonus[attacker_id]
			}
		}
		if (generalrandom>27) {
			if (generalrandom<=30) {
				weaponnum[attacker_id]=27
				halfmp[attacker_id]=1
			}
		}
		if (generalrandom>30) {
			if (generalrandom<=40) {
				weaponnum[attacker_id]=28
				sensor[attacker_id]=1
			} 
		}
		if (generalrandom>40) {
			if (generalrandom<=44) {
				weaponnum[attacker_id]=29
				petrifyitm[attacker_id]=1
			}
		}
		if (generalrandom>44) {
			if (generalrandom<=47) {
				weaponnum[attacker_id]=30
				onemp[attacker_id]=1
			}
		}
		if (generalrandom>47) {
			if (generalrandom<=55) {
				weaponnum[attacker_id]=31
				STRbonus[attacker_id]=STRbonus[attacker_id]+3
				STR[attacker_id]=STR[attacker_id]+STRbonus[attacker_id]
			}
		}
		if (generalrandom>55) {
			if (generalrandom<=59) {
				weaponnum[attacker_id]=32
				silenceitm[attacker_id]=1
			}
		}
		if (generalrandom>59) {
			if (generalrandom<=64) {
				weaponnum[attacker_id]=33
				poisonitm[attacker_id]=1
			}
		}
		if (generalrandom>64) {
			if (generalrandom<=74) {
				weaponnum[attacker_id]=34
				STRbonus[attacker_id]=STRbonus[attacker_id]+5
				STR[attacker_id]=STR[attacker_id]+STRbonus[attacker_id]
			}
		}
		if (generalrandom>74) {
			if (generalrandom<=78) {
				weaponnum[attacker_id]=35
				gillionaire[attacker_id]=1
			}
		}
		if (generalrandom>78) {
			if (generalrandom<=81) {
				weaponnum[attacker_id]=36
				deathitm[attacker_id]=1
			}
		}
		if (generalrandom>81) {
			if (generalrandom<=88) {
				weaponnum[attacker_id]=37
				poisonitm[attacker_id]=1
			}
		}
		if (generalrandom>88) {
			if (generalrandom<=89) {
				weaponnum[attacker_id]=38
				celestial[attacker_id]=1
			}
		}
		if (generalrandom>=1) {
			set_hudmessage(5,250,5,-1.0,0.30,0,6.0,5.0,0.1,0.5,HMCHAN_GENERAL)
			if (generalrandom<=89) show_hudmessage(attacker_id, "Lulu found a weapon!")
		}
		generalrandom=random_num(1,WEAPONRANDOMIZER)
		if (generalrandom<=10) {
			weaponnum[attacker_id]=39
			STRbonus[attacker_id]=STRbonus[attacker_id]+1
			STR[attacker_id]=STR[attacker_id]+STRbonus[attacker_id]
		}
		if (generalrandom>10) {
			if (generalrandom<=25) {
				weaponnum[attacker_id]=40
				STRbonus[attacker_id]=STRbonus[attacker_id]+1
				STR[attacker_id]=STR[attacker_id]+STRbonus[attacker_id]
			}
		}
		if (generalrandom>25) {
			if (generalrandom<=30) {
				weaponnum[attacker_id]=41
				STRbonus[attacker_id]=STRbonus[attacker_id]+3
				STR[attacker_id]=STR[attacker_id]+STRbonus[attacker_id]
			}
		}
		if (generalrandom>30) {
			if (generalrandom<=33) {
				weaponnum[attacker_id]=42
				slow[attacker_id]=1
			}
		}
		if (generalrandom>33) {
			if (generalrandom<=37) {
				weaponnum[attacker_id]=43
				petrifyitm[attacker_id]=1
			}
		}
		if (generalrandom>37) {
			if (generalrandom<=38) {
				weaponnum[attacker_id]=44
				celestial[attacker_id]=1
			} 
		}
		if (generalrandom>38) {
			if (generalrandom<=42) {
				weaponnum[attacker_id]=45
				darknessitm[attacker_id]=1
			}
		}
		if (generalrandom>42) {
			if (generalrandom<=45) {
				weaponnum[attacker_id]=46
				deathitm[attacker_id]=1
			}
		}
		if (generalrandom>45) {
			if (generalrandom<=47) {
				weaponnum[attacker_id]=47
				onemp[attacker_id]=1
			}
		}
		if (generalrandom>47) {
			if (generalrandom<=52) {
				weaponnum[attacker_id]=48
				STRbonus[attacker_id]=STRbonus[attacker_id]+5
				STR[attacker_id]=STR[attacker_id]+STRbonus[attacker_id]
			}
		}
		if (generalrandom>52) {
			if (generalrandom<=55) {
				weaponnum[attacker_id]=49
				darknessitm[attacker_id]=1
			}
		}
		if (generalrandom>55) {
			if (generalrandom<=57) {
				weaponnum[attacker_id]=50
				silenceitm[attacker_id]=1
			}
		}
		if (generalrandom>57) {
			if (generalrandom<=60) {
				weaponnum[attacker_id]=51
				poisonitm[attacker_id]=1
			}
		}
		if (generalrandom>60) {
			if (generalrandom<=62) {
				weaponnum[attacker_id]=52
				gillionaire[attacker_id]=1
			}
		}
		if (generalrandom>62) {
			if (generalrandom<=70) {
				weaponnum[attacker_id]=53
				STRbonus[attacker_id]=STRbonus[attacker_id]+6
				STR[attacker_id]=STR[attacker_id]+STRbonus[attacker_id]
			}
		}
		if (generalrandom>70) {
			if (generalrandom<=73) {
				weaponnum[attacker_id]=54
				counterattack[attacker_id]=1
			}
		}
		if (generalrandom>=1) {
			set_hudmessage(5,250,5,-1.0,0.30,0,6.0,5.0,0.1,0.5,HMCHAN_GENERAL)
			if (generalrandom<=73) show_hudmessage(attacker_id, "Rikku found a weapon!")
		}
		generalrandom=random_num(1,WEAPONRANDOMIZER)
		if (generalrandom<=5) {
			weaponnum[attacker_id]=55
			poisonitm[attacker_id]=1
		}
		if (generalrandom>5) {
			if (generalrandom<=10) {
				weaponnum[attacker_id]=56
				slow[attacker_id]=1
			}
		}
		if (generalrandom>10) {
			if (generalrandom<=12) {
				weaponnum[attacker_id]=57
				magiccounter[attacker_id]=1
			}
		}
		if (generalrandom>12) {
			if (generalrandom<=24) {
				weaponnum[attacker_id]=58
				STRbonus[attacker_id]=STRbonus[attacker_id]+3
				STR[attacker_id]=STR[attacker_id]+STRbonus[attacker_id]
			}
		}
		if (generalrandom>24) {
			if (generalrandom<=25) {
				weaponnum[attacker_id]=59
				celestial[attacker_id]=1
			}
		}
		if (generalrandom>25) {
			if (generalrandom<=27) {
				weaponnum[attacker_id]=60
				halfmp[attacker_id]=1
			} 
		}
		if (generalrandom>27) {
			if (generalrandom<=32) {
				weaponnum[attacker_id]=61
				sensor[attacker_id]=1
			}
		}
		if (generalrandom>32) {
			if (generalrandom<=37) {
				weaponnum[attacker_id]=62
				gillionaire[attacker_id]=1
			}
		}
		if (generalrandom>37) {
			if (generalrandom<=40) {
				weaponnum[attacker_id]=63
				darknessitm[attacker_id]=1
			}
		}
		if (generalrandom>40) {
			if (generalrandom<=44) {
				weaponnum[attacker_id]=64
				slow[attacker_id]=1
			}
		}
		if (generalrandom>44) {
			if (generalrandom<=47) {
				weaponnum[attacker_id]=65
				deathitm[attacker_id]=1
			}
		}
		if (generalrandom>=1) {
			set_hudmessage(5,250,5,-1.0,0.30,0,6.0,5.0,0.1,0.5,HMCHAN_GENERAL)
			if (generalrandom<=47) show_hudmessage(attacker_id, "Auron found a weapon!")
		}
		generalrandom=random_num(1,WEAPONRANDOMIZER)
		if (generalrandom<=10) {
			weaponnum[attacker_id]=66 
			STRbonus[attacker_id]=STRbonus[attacker_id]+6
			STR[attacker_id]=STR[attacker_id]+STRbonus[attacker_id]
		}
		if (generalrandom>10) {
			if (generalrandom<=15) {
				weaponnum[attacker_id]=67
				darknessitm[attacker_id]=1
			}
		}
		if (generalrandom>15) {
			if (generalrandom<=20) {
				weaponnum[attacker_id]=68
				darknessitm[attacker_id]=1
			}
		}
		if (generalrandom>20) {
			if (generalrandom<=24) {
				weaponnum[attacker_id]=69
				slow[attacker_id]=1
			}
		}
		if (generalrandom>24) {
			if (generalrandom<=27) {
				weaponnum[attacker_id]=70
				petrifyitm[attacker_id]=1
			}
		}
		if (generalrandom>27) {
			if (generalrandom<=30) {
				weaponnum[attacker_id]=71
				halfmp[attacker_id]=1
			} 
		}
		if (generalrandom>30) {
			if (generalrandom<=40) {
				weaponnum[attacker_id]=72
				STRbonus[attacker_id]=STRbonus[attacker_id]+1
				STR[attacker_id]=STR[attacker_id]+STRbonus[attacker_id]
			}
		}
		if (generalrandom>40) {
			if (generalrandom<=43) {
				weaponnum[attacker_id]=73
				magiccounter[attacker_id]=1
			}
		}
		if (generalrandom>43) {
			if (generalrandom<=45) {
				weaponnum[attacker_id]=74
				onemp[attacker_id]=1
			}
		}
		if (generalrandom>45) {
			if (generalrandom<=49) {
				weaponnum[attacker_id]=75
				counterattack[attacker_id]=1
			}
		}
		if (generalrandom>49) {
			if (generalrandom<=53) {
				weaponnum[attacker_id]=76
				magiccounter[attacker_id]=1
			}
		}
		if (generalrandom>53) {
			if (generalrandom<=58) {
				weaponnum[attacker_id]=77
				deathitm[attacker_id]=1
			}
		}
		if (generalrandom>58) {
			if (generalrandom<=66) {
				weaponnum[attacker_id]=78
				petrifyitm[attacker_id]=1
			}
		}
		if (generalrandom>66) {
			if (generalrandom<=70) {
				weaponnum[attacker_id]=79
				poisonitm[attacker_id]=1
			}
		}
		if (generalrandom>70) {
			if (generalrandom<=71) {
				weaponnum[attacker_id]=80
				celestial[attacker_id]=1
			}
		}
		if (generalrandom>=1) {
			set_hudmessage(5,250,5,-1.0,0.30,0,6.0,5.0,0.1,0.5,HMCHAN_GENERAL)
			if (generalrandom<=71) show_hudmessage(attacker_id, "Wakka found a weapon!")
		}
	}
	if (armornum[attacker_id]==0) {
		generalrandom=random(ARMORRANDOMIZER)
		if (generalrandom<=5) {
			armornum[attacker_id]=1
			if (ribbon[attacker_id]==1) {
				ribbon[attacker_id]=1
				getribbon[attacker_id]=1
			}
		}
		if (generalrandom>5) {
			if (generalrandom<=25) {
				armornum[attacker_id]=2
				HPbonus2[attacker_id]=10
				HPbonus[attacker_id]=HPbonus[attacker_id]+HPbonus2[attacker_id]
			}
		}
		if (generalrandom>25) {
			if (generalrandom<=50) {
				armornum[attacker_id]=3
				DEF3[attacker_id]=3
				DEF[attacker_id]=DEF[attacker_id]+DEF3[attacker_id]
			}
		}
		if (generalrandom>=1) {
			set_hudmessage(200,200,200,-1.0,0.38,0,6.0,12.0,0.1,0.5,HMCHAN_ARMOR)
			if (generalrandom<=50) show_hudmessage(attacker_id, "Tidus found an armor!")
		}
		if (generalrandom<=5) {
			armornum[attacker_id]=4
			if (ribbon[attacker_id]==1) {
				ribbon[attacker_id]=1
				getribbon[attacker_id]=1
			}
		}
		if (generalrandom>5) {
			if (generalrandom<=25) {
				armornum[attacker_id]=5
				HPbonus2[attacker_id]=20
				HPbonus[attacker_id]=HPbonus[attacker_id]+HPbonus2[attacker_id]
			}
		}
		if (generalrandom>25) {
			if (generalrandom<=50) {
				armornum[attacker_id]=6
				MPmax2[attacker_id]=20
				MPmax[attacker_id]=MPmax[attacker_id]+MPmax2[attacker_id]
			}
		}
		if (generalrandom>=1) {
			set_hudmessage(200,200,200,-1.0,0.38,0,6.0,12.0,0.1,0.5,HMCHAN_ARMOR)
			if (generalrandom<=50) show_hudmessage(attacker_id, "Auron found an armor!")
		}
		generalrandom=random(ARMORRANDOMIZER)
		if (generalrandom<=5) {
			armornum[attacker_id]=7
			if (ribbon[attacker_id]==1) {
				ribbon[attacker_id]=1
				getribbon[attacker_id]=1
			}
		}
		if (generalrandom>5) {
			if (generalrandom<=25) {
				armornum[attacker_id]=8
				DEF3[attacker_id]=2
				DEF[attacker_id]=DEF[attacker_id]+DEF3[attacker_id]
			}
		}
		if (generalrandom>25) {
			if (generalrandom<=50) {
				armornum[attacker_id]=9
				HPbonus2[attacker_id]=30
				HPbonus[attacker_id]=HPbonus[attacker_id]+HPbonus2[attacker_id]
			}
		}
		if (generalrandom>=1) {
			set_hudmessage(200,200,200,-1.0,0.38,0,6.0,12.0,0.1,0.5,HMCHAN_ARMOR)
			if (generalrandom<=50) show_hudmessage(attacker_id, "Wakka found an armor!")
		}
		generalrandom=random(ARMORRANDOMIZER)
		if (generalrandom<=5) {
			armornum[attacker_id]=10
			if (ribbon[attacker_id]==1) {
				ribbon[attacker_id]=1
				getribbon[attacker_id]=1
			}
		}
		if (generalrandom>5) {
			if (generalrandom<=25) {
				armornum[attacker_id]=11
				HPbonus2[attacker_id]=40
				HPbonus[attacker_id]=HPbonus[attacker_id]+HPbonus2[attacker_id]
				MPmax2[attacker_id]=20
				MPmax[attacker_id]=MPmax[attacker_id]+MPmax2[attacker_id]
			}
		}
		if (generalrandom>25) {
			if (generalrandom<=50) {
				armornum[attacker_id]=12
				DEF3[attacker_id]=1
				DEF[attacker_id]=DEF[attacker_id]+DEF3[attacker_id]
			}
		}
		if (generalrandom>=1) {
			set_hudmessage(200,200,200,-1.0,0.38,0,6.0,12.0,0.1,0.5,HMCHAN_ARMOR)
			if (generalrandom<=50) show_hudmessage(attacker_id, "Lulu found an armor!")
		}
		generalrandom=random(ARMORRANDOMIZER)
		if (generalrandom<=5) {
			armornum[attacker_id]=13
			if (ribbon[attacker_id]==1) {
				ribbon[attacker_id]=1
				getribbon[attacker_id]=1
			}
		}
		if (generalrandom>5) {
			if (generalrandom<=25) {
				armornum[attacker_id]=14
				DEF3[attacker_id]=3
				DEF[attacker_id]=DEF[attacker_id]+DEF3[attacker_id]
				regenitm[attacker_id]=1
			}
		}
		if (generalrandom>25) {
			if (generalrandom<=50) {
				armornum[attacker_id]=15
				DEF3[attacker_id]=3
				DEF[attacker_id]=DEF[attacker_id]+DEF3[attacker_id]
			}
		}
		if (generalrandom>=1) {
			set_hudmessage(200,200,200,-1.0,0.38,0,6.0,12.0,0.1,0.5,HMCHAN_ARMOR)
			if (generalrandom<=50) show_hudmessage(attacker_id, "Yuna found an armor!")
		}
		generalrandom=random(ARMORRANDOMIZER)
		if (generalrandom<=5) {
			armornum[attacker_id]=16
			if (ribbon[attacker_id]==1) {
				ribbon[attacker_id]=1
				getribbon[attacker_id]=1
			}
		}
		if (generalrandom>5) {
			if (generalrandom<=25) {
				armornum[attacker_id]=17
				HPbonus2[attacker_id]=50
				HPbonus[attacker_id]=HPbonus[attacker_id]+HPbonus2[attacker_id]
			}
		}
		if (generalrandom>25) {
			if (generalrandom<=50) {
				armornum[attacker_id]=18
				HPbonus2[attacker_id]=10
				HPbonus[attacker_id]=HPbonus[attacker_id]+HPbonus2[attacker_id]
			}
		}
		if (generalrandom>=1) {
			set_hudmessage(200,200,200,-1.0,0.38,0,6.0,12.0,0.1,0.5,HMCHAN_ARMOR)
			if (generalrandom<=50) show_hudmessage(attacker_id, "Rikku found an armor")
		}
	}
	inventory[attacker_id][itemnum[attacker_id]] = inventory[attacker_id][itemnum[attacker_id]] + 1
	if (itemnum[attacker_id] > 0)itemnum[attacker_id] = itemnum[attacker_id] - 1
	generalrandom = random_num(1,ITEMRANDOMIZER)
	if (generalrandom<=30) itemnum[attacker_id]=1
	if (generalrandom>30) {
		if (generalrandom<=45) itemnum[attacker_id]=2
	}
	if (generalrandom>45) {
		if (generalrandom<55) itemnum[attacker_id]=3
	}
	if (generalrandom>55) {
		if (generalrandom<=75) itemnum[attacker_id]=4
	}
	if (generalrandom>75) {
		if (generalrandom<=85) itemnum[attacker_id]=5
	}
	if (generalrandom>85) {
		if (generalrandom<=90) itemnum[attacker_id]=6
	}
	if (generalrandom>90) {
		if (generalrandom<=95) itemnum[attacker_id]=7
	}
	if (generalrandom>95) {
		if (generalrandom<=97) itemnum[attacker_id]=8
	}
	if (generalrandom>97) {
		if (generalrandom<=103) itemnum[attacker_id]=9
	}
	if (generalrandom>103) {
		if (generalrandom<=105) itemnum[attacker_id]=10
	}
	if (generalrandom>105) {
		if (generalrandom<=115) itemnum[attacker_id]=11
	}
	if (generalrandom>115) {
		if (generalrandom<=120) itemnum[attacker_id]=12
	}
	if (generalrandom>120) {
		if (generalrandom<=121) itemnum[attacker_id]=13
	}
	if (generalrandom>121) {
		if (generalrandom<=122) itemnum[attacker_id]=14
	}
	set_hudmessage(200,200,200,-1.0,0.38,0,6.0,12.0,0.1,0.5,HMCHAN_ARMOR)
	if (generalrandom<=122) show_hudmessage(attacker_id, "You Found An Item!")

	if ( itemnum[attacker_id] >=1 ) {
		generalrandom = random_num(1,ITEMRANDOMIZER)
		if (generalrandom<=30) inventory[attacker_id][1] = inventory[attacker_id][1] + 1
		if (generalrandom>30) {
			if (generalrandom<=45) inventory[attacker_id][2] = inventory[attacker_id][2] + 1
		}
		if (generalrandom>45) {
			if (generalrandom<55) inventory[attacker_id][3] = inventory[attacker_id][3] + 1
		}
		if (generalrandom>55) {
			if (generalrandom<=75) inventory[attacker_id][4] = inventory[attacker_id][4] + 1
		}
		if (generalrandom>75) {
			if (generalrandom<=85) inventory[attacker_id][5] = inventory[attacker_id][5] + 1
		}
		if (generalrandom>85) {
			if (generalrandom<=90) inventory[attacker_id][6] = inventory[attacker_id][6] + 1
		}
		if (generalrandom>90) {
			if (generalrandom<=95) inventory[attacker_id][7] = inventory[attacker_id][7] + 1
		}
		if (generalrandom>95) {
			if (generalrandom<=97) inventory[attacker_id][8] = inventory[attacker_id][8] + 1
		}
		if (generalrandom>97) {
			if (generalrandom<=103) inventory[attacker_id][9] = inventory[attacker_id][9] + 1
		}
		if (generalrandom>103) {
			if (generalrandom<=105) inventory[attacker_id][10] = inventory[attacker_id][10] + 1
		}
		if (generalrandom>105) {
			if (generalrandom<=115) inventory[attacker_id][11] = inventory[attacker_id][11] + 1
		}
		if (generalrandom>115) {
			if (generalrandom<=120) inventory[attacker_id][12] = inventory[attacker_id][12] + 1
		}
		if (generalrandom>120) {
			if (generalrandom<=121) inventory[attacker_id][13] = inventory[attacker_id][13] + 1
		}
		if (generalrandom>121) {
			if (generalrandom<=122) inventory[attacker_id][14] = inventory[attacker_id][14] + 1
		}
		set_hudmessage(200,200,200,-1.0,0.38,0,6.0,12.0,0.1,0.5,HMCHAN_ARMOR)
		if (generalrandom<=122) show_hudmessage(attacker_id, "You Found An Item!")
	}
	if ( victim_id== bombdefuser && attacker_id!= bombdefuser && get_user_team(victim_id)!= get_user_team(attacker_id) ){						
		XP[attacker_id]= XP[attacker_id]+20	
	}
	if (victim_id==bombCarrier && attacker_id!=bombCarrier && get_user_team(victim_id)!=get_user_team(attacker_id)){						
		XP[attacker_id]= XP[attacker_id]+20
	}
	if( headshot && attacker_id!=victim_id && !deathmessageshown[victim_id] && get_user_team(victim_id)!=get_user_team(attacker_id)){
		XP[attacker_id]= XP[attacker_id]+20
	}
	if (victim_id==hostagesaver && get_user_team(victim_id)!=get_user_team(attacker_id)) {
		XP[attacker_id]= XP[attacker_id]+20
	}
	if (ringnum[attacker_id]==2) XP[attacker_id]= XP[attacker_id]+15 
	XP[attacker_id]= XP[attacker_id]+NORMXP
	sayexp(attacker_id)
	checklvl(attacker_id)
	if (auto_lifeused[victim_id]==1) {
		set_task(0.5, "spawnme", victim_id, "", 0, "a", 0)
	}
	if (deathmatch == 1) {
		spawnleft ++
		deathmatchspawn(victim_id)
	}
	return PLUGIN_CONTINUE
}