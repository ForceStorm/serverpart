public SPAWNNPCCT(id) {
	if (id == 0) return PLUGIN_CONTINUE
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	if (ffxenable==false)return PLUGIN_CONTINUE	
	if (!is_valid_ent(id))return PLUGIN_CONTINUE
	NPCCT = create_entity("info_target")
	entity_set_string(NPCCT, EV_SZ_classname, "npc")
	set_pev(NPCCT,pev_solid,1)
	set_pev(NPCCT,pev_sequence,61)
	set_pev(NPCCT,pev_movetype,5)
	new Float:MinBox[3]
	new Float:MaxBox[3]
	new Float:Angles[3]
	pev(id, pev_origin, npcarea)
	pev(id, pev_v_angle, Angles)
	MinBox[0] = -20.0				    //Boxes are for the area touched :)
	MinBox[1] = -20.0				    
	MinBox[2] = -36.0
	MaxBox[0] = 20.0
	MaxBox[1] = 20.0
	MaxBox[2] = 36.0
	set_pev(NPCCT,pev_mins,MinBox)
	set_pev(NPCCT,pev_maxs,MaxBox)
	set_pev(NPCCT,pev_origin,npcarea)
	entity_set_model(NPCCT,"models/bney.mdl")
	set_pev(NPCCT,pev_v_angle,Angles)
	set_pev(NPCCT,pev_angles,Angles)
	set_pev(NPCCT,pev_framerate,2.0)
	NPCCTexists=1
	rugarea1(id)
	drop_to_floor(NPCCT)
	return PLUGIN_CONTINUE
}

public SPAWNNPCTERROR(id) {
	if (id == 0) return PLUGIN_CONTINUE
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	if (ffxenable==false)return PLUGIN_CONTINUE	
	NPCTERROR = create_entity("info_target")
	entity_set_string(NPCTERROR, EV_SZ_classname, "npc1")
	set_pev(NPCTERROR,pev_solid,2)
	set_pev(NPCTERROR,pev_sequence,61)
	set_pev(NPCTERROR,pev_movetype,5)

	new Float:MinBox[3]
	new Float:MaxBox[3]
	new Float:Angles[3]
	pev(id, pev_origin, npcarea)
	pev(id, pev_v_angle, Angles)
	npcarea[2] = npcarea[0] + 76.0
	npcarea[2] = npcarea[1] + 46.0
	npcarea[2] = npcarea[2] + 66.0
	MinBox[0] = -20.0				    //Boxes are for the area touched :)
	MinBox[1] = -20.0				    
	MinBox[2] = -36.0
	MaxBox[0] = 20.0
	MaxBox[1] = 20.0
	MaxBox[2] = 36.0
	set_pev(NPCTERROR,pev_mins,MinBox)
	set_pev(NPCTERROR,pev_maxs,MaxBox)
	set_pev(NPCTERROR,pev_origin,npcarea)
	entity_set_model(NPCTERROR,"models/bney.mdl")
	set_pev(NPCTERROR,pev_v_angle,Angles)
	set_pev(NPCTERROR,pev_angles,Angles)
	set_pev(NPCTERROR,pev_framerate,2.0)
	rugarea(id)
	NPCTexists=1
	drop_to_floor(NPCTERROR)
	return PLUGIN_CONTINUE
}


public rugarea(id) {
	if (id == 0) return PLUGIN_CONTINUE
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	if (ffxenable==false)return PLUGIN_CONTINUE	
	if (!is_valid_ent(id))return PLUGIN_CONTINUE
	pev(id, pev_origin, npcarea)
	RUG = create_entity("info_target")
	entity_set_string(RUG, EV_SZ_classname, "RUG")
	new Float:MinBox[3]
	new Float:MaxBox[3]
	MinBox[0] = -680.0
	MinBox[1] = -680.0
	MinBox[2] = -1.0
	MaxBox[0] = 680.0
	MaxBox[1] = 680.0
	MaxBox[2] = 1.0
	npcarea[2] = npcarea[2] - 30.0
	set_pev(RUG, pev_mins, MinBox) 
	set_pev(RUG, pev_maxs, MaxBox) 
	set_pev(RUG, pev_sequence, 1)
	set_pev(RUG, pev_solid, 1)
	entity_set_model(RUG,"models/rug_oriental-large.mdl")
	set_pev(RUG, pev_origin, npcarea)	
	drop_to_floor(RUG)
	drop_to_floor(RUG)
	return PLUGIN_CONTINUE
}

public rugarea1(id) {
	if (id == 0) return PLUGIN_CONTINUE
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	if (ffxenable==false)return PLUGIN_CONTINUE	
	if (!is_valid_ent(id))return PLUGIN_CONTINUE
	pev(id, pev_origin, npcarea)
	RUG1 = create_entity("info_target")
	entity_set_string(RUG1, EV_SZ_classname, "RUG1")
	entposition[2] = entposition[2] - 33.0
	pev(id, pev_origin, entposition)
	new Float:MinBox[3]
	new Float:MaxBox[3]
	MinBox[0] = -680.0
	MinBox[1] = -680.0
	MinBox[2] = -1.0
	MaxBox[0] = 680.0
	MaxBox[1] = 680.0
	MaxBox[2] = 1.0
	npcarea[2] = npcarea[2] + 90.0
	npcarea[2] = npcarea[2] + 60.0
	npcarea[2] = npcarea[2] + 20.0
	set_pev(RUG1, pev_mins, MinBox) 
	set_pev(RUG1, pev_maxs, MaxBox) 
	set_pev(RUG1, pev_solid, 2)
	entity_set_model(RUG1,"models/rug_oriental-large.mdl")
	set_pev(RUG1, pev_origin, npcarea)
	drop_to_floor(RUG1)
	drop_to_floor(RUG1)
	return PLUGIN_CONTINUE
}

public dropitem(id) {        //Drops The item 
	if (id == 0) return PLUGIN_CONTINUE
	if (!is_valid_ent(id))return PLUGIN_CONTINUE
	szentity2[id] = create_entity("info_target")
	pev(id, pev_origin, entposition)
	new Float:MinBox[3]
	new Float:MaxBox[3]
	MinBox[0] = -1.0
	MinBox[1] = -1.0
	MinBox[2] = -1.0
	MaxBox[0] = 1.0
	MaxBox[1] = 1.0
	MaxBox[2] = 1.0
	set_pev(szentity2[id], pev_mins, MinBox) 
	set_pev(szentity2[id], pev_maxs, MaxBox) 
	set_pev(szentity2[id],pev_classname,engfunc(EngFunc_AllocString,"specialitem")) 
	set_pev(szentity2[id], pev_solid, SOLID_BBOX)
	entity_set_model(szentity2[id],"models/books1.mdl")
	set_pev(szentity2[id], pev_origin, ent_position)
	set_pev(szentity2[id],pev_movetype,6)
	return PLUGIN_HANDLED
}

public dropsphere(id) {        //Drops The item 
	if (!is_valid_ent(id))return PLUGIN_CONTINUE
	szentity1[id] = create_entity("info_target")
	pev(id, pev_origin, entposition)
	set_pev(szentity1[id],pev_classname,engfunc(EngFunc_AllocString,"specialitem")) 
	entity_set_model(szentity1[id],"models/light_bulb.mdl")
	new Float:MinBox[3]
	new Float:MaxBox[3]
	MinBox[0] = -1.0
	MinBox[1] = -1.0
	MinBox[2] = -1.0
	MaxBox[0] = 1.0
	MaxBox[1] = 1.0
	MaxBox[2] = 1.0
	set_pev(szentity1[id],pev_mins,MinBox)
	set_pev(szentity1[id],pev_maxs,MaxBox)
	new Float:color[3]
	color[0] = 125.0
	color[1] = 155.0
	color[2] = 155.0
	set_pev(szentity1[id],pev_rendermode,kRenderGlow)
	set_pev(szentity1[id],pev_renderamt,128.0)
	set_pev(szentity1[id],pev_rendercolor,color)
	set_pev(szentity1[id],pev_renderfx,kRenderFxGlowShell)
	set_pev(szentity1[id],pev_origin, entposition)
	set_pev(szentity1[id],pev_movetype,6)
	set_pev(szentity1[id],pev_solid,SOLID_BBOX)
	set_pev(szentity1[id],pev_effects,32)
	set_pev(szentity1[id],pev_owner,id)
	return PLUGIN_HANDLED
}

targetaiming( id, target ) { 
	new Float:origin[3], Float:target_origin[3], Float:target_angles[3], Float:aim_at_id[3], i
	new Float:angles[3] = { 0.0, 0.0, 0.0 }, Float:offset[2] = { 0.0, 0.0 }, Float:target_offset[3] = { 0.0, 0.0, 0.0 }
	entity_get_vector( id, EV_VEC_origin, origin )
	entity_get_vector( target, EV_VEC_origin, target_origin )
	entity_get_vector( target, EV_VEC_angles, target_angles )
	target_origin[0] += target_offset[0] * floatsin( target_angles[1], degrees )
	target_origin[1] += target_offset[1] * floatcos( target_angles[1], degrees )
	target_origin[2] += target_offset[2] 
	for ( i = 0; i < 3; i++ ) aim_at_id[i] = origin[i] - target_origin[i] 
	new Float:sqrtangle = (floatsqroot( ( ( aim_at_id[0] * aim_at_id[0] ) + ( aim_at_id[1] * aim_at_id[1] ) ) ) )
	angles[0] = floatatan( aim_at_id[2] / sqrtangle, 0 )
	angles[1] = floatatan( aim_at_id[1] / aim_at_id[0], 0 )
	angles[0] = angles[0] * 360.0 / ( 2 * PI )
	angles[1] = angles[1] * 360.0 / ( 2 * PI )
	angles[0] += offset[1] 
	angles[1] += offset[0] 
	( aim_at_id[0] >= 0.0 ) ? ( angles[1] += 180.0 ) : ( angles[1] += 0.0 ) 
	angles[0] = 0.0
	entity_set_vector( id, EV_VEC_angles, angles ) 
	entity_set_int( id, EV_INT_fixangle, 1 ) 
	return 1 
}

public npchandletask(id) {
	if ( id == 0) return PLUGIN_CONTINUE
	if ( id >= 33) return PLUGIN_CONTINUE
	if ( is_user_connected( id ) ) {
		new tEntity = find_ent_by_class(-1, "npc1")	
		if (is_valid_ent(tEntity) )npchandler(id, tEntity)
		new ctEntity = find_ent_by_class(-1, "npc")
		if (is_valid_ent(ctEntity) ) npchandler(id, ctEntity)
	}
	return PLUGIN_CONTINUE
}


public npchandler(id,entid) {
	if ( !is_user_connected( id ) ) return PLUGIN_CONTINUE
	if ( id > 33 ) return PLUGIN_CONTINUE
	if ( id == 0 ) return PLUGIN_CONTINUE
	if (entity_range(entid,id) < 1600.0 ) {		
		if ( pev(id, pev_button) & IN_USE ) talkFF(id)
	}
	new randomseq = random_num(4, 30)
	drop_to_floor(entid)
	if (entity_range(entid,id) < 1000.0 ) {		
		new Float:entorigin[3]
		new Float:idorigin[3]
		new Float:rugorigin[3]
		pev(RUG1, pev_origin, rugorigin)
		entity_get_vector(id, EV_VEC_origin, idorigin)	
		entity_get_vector(entid, EV_VEC_origin, entorigin)	
		if ( followid[id] == 1 ) {
			new Float:followadj[3] 
			new Float:originadjuster[3]
			if (followswitch[entid] == 0) {
				followrandomizer[entid] = followrandomizer[entid] + 1.0
				followrandomizer2[entid] = followrandomizer2[entid] + 1.0
			}
			if (followswitch[entid] == 1) {
				followrandomizer[entid] = followrandomizer[entid] - 1.0
				followrandomizer2[entid] = followrandomizer2[entid] - 1.0
			}
			if (followrandomizer2[entid] > 100.0) followswitch[entid] = 1
			if (followrandomizer2[entid] < 50.0) followswitch[entid] = 0
			
			originadjuster[0] = rugorigin[0] + 20/ random_float( followrandomizer[entid], followrandomizer[entid] + 1.0 )
			originadjuster[1] = rugorigin[1] + 50/ random_float( followrandomizer2[entid], followrandomizer2[entid] + 1.0 )
			originadjuster[2] = rugorigin[2] 
			
			for (new a = 0 ; a < 3 ; a ++) {
				if (entorigin[a] > rugorigin[a]) followadj[a] = entorigin[a] - originadjuster[a]
				if (entorigin[a] < rugorigin[a]) followadj[a] = entorigin[a] + originadjuster[a]
			}
			
			targetaiming(entid, id)	
			engfunc(EngFunc_SetOrigin,entid,followadj)
			emit_sound(entid, CHAN_VOICE, "ffx/ffx_npc_footstep.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
			if (npcframecount[id] >= npcframe[id]) {
					set_pev(entid,pev_sequence,1)
					npcframe[id] = 20 
					npcframecount[id] = 0
			}
			
			if ( pev(id, pev_button) & IN_DUCK ) {
				engfunc(EngFunc_SetOrigin,entid,followadj)
				if (npcframecount[id] >= npcframe[id]) {
					set_pev(entid,pev_sequence,22)
					npcframe[id] = 20 
					npcframecount[id] = 0
				}
			}
			npcframecount[id] ++
			return PLUGIN_CONTINUE
		}
		
		if (buying[id] == 1 ) {
			if (npcframecount[id] >= npcframe[id]) {
				set_pev(entid,pev_sequence,103)
				npcframe[id] = 36 
				npcframecount[id] = 0
			}
			targetaiming(entid, id)
			buying[id] = 0
			npcframecount[id] ++
	
			return PLUGIN_CONTINUE
		}
		
		if (sold1[id] == 1 ) {
   			if (npcframecount[id] >= npcframe[id]) {
				set_pev(entid,pev_sequence,48)
				npcframe[id] = 37 
				npcframecount[id] = 0
			}
 			targetaiming(entid, id)
			sold1[id] = 0
			npcframecount[id] ++
			return PLUGIN_CONTINUE
		}
		targetaiming(entid, id)
		if (npcframecount[id] >= npcframe[id]) {
			set_pev(entid,pev_sequence,0)
			npcframe[id] = 31 
			npcframecount[id] = 0
			
		}
	}
	npcframecount[id] ++
	return PLUGIN_CONTINUE
}
