/*					THIS IS A TEST VERSION !!!!!!!!!!!!!!!
							FINAL FANTASY 10
							      v0.85
							      
							      v.051
							   Originally
						  	  By Z4KN4F31N
					MOdified and Ported to Amxx By Timmi
					Changed Saving and Loading.
					THIS IS A TEST COPY!!!!!!!!!!!!!!!!!!!!!
*/


//****************************************************Begin In game Help*********************************
//most help can be accessed through menu's since the MOTD txt was stacking and preventing ease of use
//Also all avail in console :) thanks to original author.
public helpFF(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE
console_print(id, "===============================================")
console_print(id, "Final Fantasy X MOD HELP FILE v0.5.1 by Z4KN4F31N")
console_print(id, "COMMANDS:")
console_print(id, "===============================================")
console_print(id, "!bindFF:Binds the following buttons to functions [ :nextmagic, ] :usemagic, . :dropweapon, , :droparmor")
console_print(id, "p : useitem, ' talkto the guy at spawn")
console_print(id, "!dropweapon:Drops the current weapon")
console_print(id, "!droparmor:Drops the current armor")
console_print(id, "!useskill:Uses Selected Magic")
console_print(id, "!next_FF:Selects next Magic")
console_print(id, "helpFFcon:Command Help")
console_print(id, "helpmagicFFcon:Magic Help")
console_print(id, "helpstatsFFcon:Stats Help")
console_print(id, "helpweaponFFcon:Weapon/Armor Skills Help")
console_print(id, "helpitemFFcon:Item Effects Help")
console_print(id, "Note:ALL OF THE HELP FILES EXIST IN MOTD WINDOW MODE TOO AND CAN BE CALLED BY REMOVING 'con' SUFFIX FROM THE HELP COMMAND YOU WANT. (example:helpweaponFF(MOTD)/helpweaponFFcon(console)")
console_print(id, "")
return PLUGIN_CONTINUE
}
public helpstatsFF(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE
console_print(id, "===============================================")
console_print(id, "Final Fantasy X MOD HELP FILE v0.5.1 by Z4KN4F31N")
console_print(id, "STATS:")
console_print(id, "===============================================")
console_print(id, "Strength:Increases chances for critical and enhances some magics")
console_print(id, "Defense:Increases defenses against critical and some magics")
console_print(id, "Magic:Makes player skills strengthen")
console_print(id, "Agility:Makes player run slightly faster. At least 1 point is needed for haste/hastega")
return PLUGIN_CONTINUE
}

public helpmagicFF(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE	
if (FFplayer[id]==1) {
console_print(id, "===============================================")
console_print(id, "Final Fantasy X MOD HELP FILE v0.5.1 by Z4KN4F31N")
console_print(id, "TIDUS MAGICS:")
console_print(id, "===============================================")
console_print(id, "1.Haste:Makes Tidus run faster(need at least 1 AGILITY)")
console_print(id, "2.Hastega:Makes Tidus run even faster(need at least 1 AGILITY)")
console_print(id, "3.Flee:Gives immunity for 5 sec. If you hit an enemy in this period, you die!")
console_print(id, "4.Flee2:Gives immunity for 10 sec. If you hit an enemy in this period, you die!")
console_print(id, "5.Spiral Cut:Great damage")
console_print(id, "6.Blitz Ace:Great damage")
console_print(id, "")
}
if (FFplayer[id]==2) {
console_print(id, "===============================================")
console_print(id, "Final Fantasy X MOD HELP FILE v0.5.1 by Z4KN4F31N")
console_print(id, "AURON MAGICS:")
console_print(id, "===============================================")
console_print(id, "1.Power Break:Nullifies opponnent's STRENGTH(temporary)")
console_print(id, "2.Armor Break:Nullifies opponnent's DEFENSE(temporary)")
console_print(id, "3.Magic Break:Nullifies opponnent's MAGIC(temporary)")
console_print(id, "4.Full Break:Nullifies opponnent's STRENGTH,DEFENSE and MAGIC(temporary)")
console_print(id, "5.Shooting Star:Great damage and some stat messing up")
console_print(id, "6.Tornado:Great damage and all stats set to 0(temporary)")
console_print(id, "")
}
if (FFplayer[id]==3) {
console_print(id, "===============================================")
console_print(id, "Final Fantasy X MOD HELP FILE v0.5.1 by Z4KN4F31N")
console_print(id, "WAKKA MAGICS:")
console_print(id, "===============================================")
console_print(id, "1.Poisonstrike:Poisons opponnent")
console_print(id, "2.Silencestrike:sets opponnent's magic to 0")
console_print(id, "3.Triple Foul:sets opponnent's magic to 0 and poisons him")
console_print(id, "4.Attack Reels:instantly kills an opponnent(3/4chances) or kills user(1/4chances)")
console_print(id, "")
}
if (FFplayer[id]==4) {
console_print(id, "===============================================")
console_print(id, "Final Fantasy X MOD HELP FILE v0.5.1 by Z4KN4F31N")
console_print(id, "LULU MAGICS:")
console_print(id, "===============================================")
console_print(id, "1.Darkness:Blinds opponnent a little")
console_print(id, "2.Darkness2:Blinds opponnent up to no light")
console_print(id, "3.Death:Instantly kills an opponnent(low chances)")
console_print(id, "4.Darkness Fury:Like darkness except lasts for whole round")
console_print(id, "5.Darkness2 Fury:Like darkness2 except lasts for whole round")
console_print(id, "")
}
if (FFplayer[id]==5) {
console_print(id, "===============================================")
console_print(id, "Final Fantasy X MOD HELP FILE v0.5.1 by Z4KN4F31N")
console_print(id, "YUNA MAGICS:")
console_print(id, "===============================================")
console_print(id, "1.Cure:Heals a little HP")
console_print(id, "2.Cura:Heals a medium amount of HP")
console_print(id, "3.Curaga:Heals a lot of HP")
console_print(id, "4.Protect:Defense+10(temporary)")
console_print(id, "5.Petrify:Opponnent cannot move for this round")
console_print(id, "6.Auto Life:Cast it in a round and when you die in that round you will respawn")
console_print(id, "")
}
if (FFplayer[id]==6) {
console_print(id, "===============================================")
console_print(id, "Final Fantasy X MOD HELP FILE v0.5.1 by Z4KN4F31N")
console_print(id, "RIKKU MAGICS:")
console_print(id, "===============================================")
console_print(id, "1.Steal:Steals an item when you hit an opponnent")
console_print(id, "2.Mug(passive):You don't cast this one. When you hit you have chances of stealing")
console_print(id, "3.Pilfer Gil:Steal all of enemy's money")
console_print(id, "4.Spare Change:Uses 1000 gold to instantly kill an opponnent")
console_print(id, "5.Quick Hit(passive):You don't cast this one. When you hit you have chances of dealing much more damage at your opponnent")
console_print(id, "")
}
if (FFplayer[id]==0) {
console_print(id, "===============================================")
console_print(id, "Final Fantasy X MOD HELP FILE v0.5.1 by Z4KN4F31N")
console_print(id, "Please select a player first...")
console_print(id, "===============================================")
}
return PLUGIN_CONTINUE
}
public helpweaponFF(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE
console_print(id, "===============================================")
console_print(id, "Final Fantasy X MOD HELP FILE v0.5.1 by Z4KN4F31N")
console_print(id, "WEAPON UNIQUE SKILLS:")
console_print(id, "===============================================")
console_print(id, "Half Mp: Reduces the cost of MP for each skill to half (You must have the normal MP to use the magic though)")
console_print(id, "Counter-Attack: Makes the attacking player have a chance to lose HP when hitting you")
console_print(id, "One Mp: Reduces the cost of MP for each skill to one (You must have the normal MP to use the magic though)")
console_print(id, "Stonestrike: Gives attacker a possibility of petrifying victim")
console_print(id, "Deathstrike: Gives attacker a possibility of instantly killing victim")
console_print(id, "Slowstrike: Gives attacker a possibility of slowing victim")
console_print(id, "Magic-Counter: Makes the attacking player have a chance to lose HP when hitting you with magic")
console_print(id, "Celestial: Causes attacker to cause GREAT damage")
console_print(id, "Poisonstrike: Gives attacker a possibility of poisoning victim")
console_print(id, "Darkness Strike: Gives attacker a possibility of preventing victim's sight for a little")
console_print(id, "Silencestrike: Gives attacker a possibility of reducing victim's MAGIC to 0")
console_print(id, "Gillionaire: Gives attacker a possibility of stealing all of victim's money")
console_print(id, "Sensor: Gives attacker a full report of the victim's characteristics at each hit")
console_print(id, "===============================================")
console_print(id, "ARMOR UNIQUE SKILLS:")
console_print(id, "===============================================")
console_print(id, "Ribbon: Protects against all status ailments")
console_print(id, "Auto-Protect: DEFENSE + 3")
console_print(id, "Auto-Regen: Regenerates Health slowly")
console_print(id, "")
return PLUGIN_CONTINUE
}
public helpitemFF(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE
console_print(id, "===============================================")
console_print(id, "Final Fantasy X MOD HELP FILE v0.5.5 by Z4KN4F31N modified by timmi the savage")
console_print(id, "ITEM EFFECTS:")
console_print(id, "===============================================")
console_print(id, "Potion: Cures 20 HP")
console_print(id, "Hi-Potion: Cures 50 HP")
console_print(id, "X-Potion: Cures 100 HP")
console_print(id, "Ether: Cures 10 MP")
console_print(id, "Turbo-Ether: Cures 30 MP")
console_print(id, "Power Sphere: STRENGTH + 1")
console_print(id, "Agility Sphere: AGILITY + 1")
console_print(id, "Magic Sphere: MAGIC + 1")
console_print(id, "Dark Matter: Casts Darkness on Enemy(100% success)")
console_print(id, "Chocobo Feather: Casts Haste on User(Needs at least 1 point to AGILITY")
console_print(id, "Elixir: Cures HP and MP completely")
console_print(id, "Antidote: Cures Poison")
console_print(id, "Remedy: Cures all status ailments")
console_print(id, "Mana Tablet: Doubles Mana")
console_print(id, "Invis Potion: Makes you invisible for 15 seconds")
console_print(id, "Weight Loss: Low grav for 10 seconds")
console_print(id, "Diguise Potion: Changes your model to the opposite team for 20 seconds")
console_print(id, "")
return PLUGIN_CONTINUE
}

public helpstatsFFm(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE	
new txt[999]
add(txt,998, "Strength:Increases chances for critical and enhances some magics.....^nDefense:Increases defenses against critical and some magics........^nMagic:Makes player gain skills and strengthen them.................^nAgility:Makes player run slightly faster. At leas 1 point is needed for haste/hastega")
show_motd(id,txt,"Final Fantasy X MOD v0.5.6 Stats")
return PLUGIN_CONTINUE
}
public helpmagicFFm(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE	
new txt[999]
new keys
format(txt,998,"\rSkills Help:")
if (FFplayer[id]==1) {
add(txt,998,"^n\y1.Haste:Makes Tidus run faster(need at least 1 AGILITY)")
add(txt,998,"^n\y2.Hastega:Makes Tidus run even faster(need at least 1 AGILITY)")
add(txt,998,"^n\y3.Flee:Gives immunity for 5 sec. If you hit an enemy   , you die!")
add(txt,998,"^n\y4.Flee2:Gives immunity for 10 sec. If you hit an enemy , you die!")
add(txt,998,"^n\y5.Spiral Cut:Great damage")
add(txt,998,"^n\y6.Blitz Ace:Great damage")
add(txt,998, "^n^n\d0. Exit" )
keys = (1<<0|1<<1|1<<2|1<<3|1<<4|1<<5|1<<6|1<<9)
show_menu( id, keys, txt, -1 )
}
if (FFplayer[id]==2) {
add(txt,998,"^n\y1.Power Break:Nullifies opponnent's STRENGTH(temporary)")
add(txt,998,"^n\y2.Armor Break:Nullifies opponnent's DEFENSE(temporary)'")
add(txt,998,"^n\y3.Magic Break:Nullifies opponnent's MAGIC(temporary)")
add(txt,998,"^n\y4.Full Break:Nullifies opponnent's STATS(temporary)")
add(txt,998,"^n\y5.Shooting Star:Great damage and some stat messing up")
add(txt,998,"^n6.Tornado:Great damage and all stats set to 0(temporary)")
add(txt,998, "^n^n\d0. Exit" )
keys = (1<<0|1<<1|1<<2|1<<3|1<<4|1<<5|1<<6|1<<9)
show_menu( id, keys, txt, -1 )
}
if (FFplayer[id]==3) {
add(txt,998,"^n\y1.Poisonstrike:Poisons opponnent")
add(txt,998,"^n\y2.Silencestrike:sets opponnent's magic to 0")
add(txt,998,"^n\y3.Triple Foul:sets opponnent's magic to 0 and poisons him")
add(txt,998,"^n\y4.Attack Reels:kills an opponnent(3/4chances) or kills user(1/4)")
add(txt,998, "^n^n\d0. Exit" )
keys = (1<<0|1<<1|1<<2|1<<3|1<<4|1<<5|1<<6|1<<9)
show_menu( id, keys, txt, -1 )
}
if (FFplayer[id]==4) {
add(txt,998,"^n\y1.Darkness:Blinds opponnent a little")
add(txt,998,"^n\y2.Darkness2:Blinds opponnent up to no light")
add(txt,998,"^n\y3.Death:Instantly kills an opponnent(low chances)")
add(txt,998,"^n\y4.Darkness Fury:Like darkness except lasts for whole round")
add(txt,998,"^n\y5.Darkness2 Fury:Like darkness2 except lasts for whole round")
add(txt,998, "^n^n\d0. Exit" )
keys = (1<<0|1<<1|1<<2|1<<3|1<<4|1<<9)
show_menu( id, keys, txt, -1 )
}
if (FFplayer[id]==5) {
add(txt,998,"^n\y1.Cure:Heals a little HP")
add(txt,998,"^n\y2.Cura:Heals a medium amount of HP")
add(txt,998,"^n\y3.Curaga:Heals a lot of HP")
add(txt,998,"^n\y4.Protect:Defense+10(temporary)")
add(txt,998,"^n\y5.Petrify:Opponnent cannot move for this round")
add(txt,998,"^n\y6.Auto Life:Cast it in a round and when you die in that round you will respawn")
add(txt,998, "^n^n\d0. Exit" )
keys = (1<<0|1<<1|1<<2|1<<3|1<<4|1<<5|1<<9)
show_menu( id, keys, txt, -1 )
}
if (FFplayer[id]==6) {
add(txt,998,"^n\y1.Steal:Steals an item when you hit an opponnent")
add(txt,998,"^n\y2.Mug(passive):You don't cast this one. When you hit you have chances of stealing")
add(txt,998,"^n\y3.Pilfer Gil:Steal all of enemy's money")
add(txt,998,"^n\y4.Spare Change:Uses 1000 gold to instantly kill an opponnent")
add(txt,998,"^n\y5.Quick Hit(passive):You don't cast this one. When you hit you have chances of dealing much more damage at your opponnent")
add(txt,998, "^n^n\d0. Exit" )
keys = (1<<0|1<<1|1<<2|1<<3|1<<4|1<<9)
show_menu( id, keys, txt, -1 )
}
return PLUGIN_CONTINUE
}
public helpmenu2(id, key) {
switch(key)
{
case 0:return PLUGIN_CONTINUE
case 1:return PLUGIN_CONTINUE
case 2:return PLUGIN_CONTINUE
case 3:return PLUGIN_CONTINUE
case 4:return PLUGIN_CONTINUE
case 5:return PLUGIN_CONTINUE
case 6:return PLUGIN_CONTINUE
case 9: return PLUGIN_CONTINUE
}
return PLUGIN_CONTINUE
}

public helpmenusk(id, key) {
switch(key)
{
case 0:return PLUGIN_CONTINUE
case 1:return PLUGIN_CONTINUE
case 2:return PLUGIN_CONTINUE
case 3:return PLUGIN_CONTINUE
case 4:return PLUGIN_CONTINUE
case 5:return PLUGIN_CONTINUE
case 9: return PLUGIN_CONTINUE
}
return PLUGIN_CONTINUE
}
public helpweaponFFm(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE	
new txt[999]
new keys
format(txt,998,"\rWEAPON UNIQUE SKILLS:")
add(txt,998,"^n\y1.Half Mp: Reduces cost of MP for each skill to half")
add(txt,998,"^n\y2.Counter-Attack: You have a chance at striking back")
add(txt,998,"^n\y3.One Mp: Reduces the cost of MP for each skill to one")
add(txt,998,"^n\y4.Stonestrike: Gives a chance of petrifying victim")
add(txt,998,"^n\y5.Deathstrike: Gives a chance of instantly killing victim")
add(txt,998,"^n\y6.Slowstrike: Gives a chance of slowing victim")
add(txt,998,"^n\y7.Magic-Counter:Opponent chances loses HP when hitting you with magic")
add(txt,998,"^n\y8.Celestial: Causes to cause GREAT damage")
add(txt,998,"^n\y9.Poisonstrike: Gives a chance of poisoning victim")
add(txt,998, "^n^n\d0. More Options" )
keys = (1<<0|1<<1|1<<2|1<<3|1<<4|1<<5|1<<6|1<<7|1<<8|1<<9)
show_menu( id, keys, txt, -1 )
return PLUGIN_CONTINUE
}
public helpmenuwps(id, key) {
switch(key)
{
case 0:helpweaponFFm1(id)
case 1:helpweaponFFm1(id)
case 2:helpweaponFFm1(id)
case 3:helpweaponFFm1(id) 
case 4:helpweaponFFm1(id)
case 5:helpweaponFFm1(id)
case 9:helpweaponFFm1(id)
}
return PLUGIN_CONTINUE
}
public helpweaponFFm1(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE	
new txt[999]
new keys
format(txt,998,"\rWEAPON UNIQUE SKILLS PART 2:")
add(txt,998,"^n\y1.Darkness Strike: Gives a chance of blinding victim a little")
add(txt,998,"^n\y2.Silencestrike: Gives a chance of reducing victim's MAGIC to 0")
add(txt,998,"^n\y3.Gillionaire: Gives a chance of stealing all of victim's money")
add(txt,998,"^n\y4.Sensor: Gives a full report of the victim's stats for each hit")
add(txt,998, "^n^n\d0. More Options" )
keys = (1<<0|1<<1|1<<2|1<<3|1<<9)
show_menu( id, keys, txt, -1 )
return PLUGIN_CONTINUE
}
public helpmenuwps1(id, key) {
switch(key)
{
case 0:helpweaponFFm2(id)
case 1:helpweaponFFm2(id)
case 2:helpweaponFFm2(id)
case 3:helpweaponFFm2(id) 
case 9:helpweaponFFm2(id)
}
return PLUGIN_CONTINUE
}
public helpmenuwps2(id, key) {
switch(key)
{
case 0:return PLUGIN_CONTINUE
case 1:return PLUGIN_CONTINUE
case 2:return PLUGIN_CONTINUE
case 9:return PLUGIN_CONTINUE
}
return PLUGIN_CONTINUE
}
public helpweaponFFm2(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE	
new txt[999]
new keys
format(txt,998,"\rARMOR UNIQUE SKILLS:")
add(txt,998,"^n\y1.Ribbon: Protects against all status ailments")
add(txt,998,"^n\y2.Auto-Protect: DEFENSE + 3")
add(txt,998,"^n\y3.Auto-Regen: Regenerates Health slowly")
add(txt,998, "^n^n\d0. Exit" )
keys = (1<<0|1<<1|1<<2|1<<9)
show_menu( id, keys, txt, -1 )
return PLUGIN_CONTINUE
}
public helpitemFFm(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE
new txt[999]
new keys
format(txt,998,"^nITEM EFFECTS:")
add(txt,998,"^n\y1.Potion: Cures 20 HP")
add(txt,998,"^n\y2.Hi-Potion: Cures 50 HP")
add(txt,998,"^n\y3.X-Potion: Cures 100 HP")
add(txt,998,"^n\y4.Ether: Cures 10 MP")
add(txt,998,"^n\y5.Turbo-Ether: Cures 30 MP")
add(txt,998,"^n\y6.Power Sphere: STRENGTH + 1")
add(txt,998,"^n\y7.Agility Sphere: AGILITY + 1")
add(txt,998,"^n\y8.Magic Sphere: MAGIC + 1")
add(txt,998,"^n\y9.Dark Matter: Casts Darkness on Enemy(100% success)")
add(txt,998, "^n^n\d0. More Options" )
keys = (1<<0|1<<1|1<<2|1<<3|1<<4|1<<5|1<<6|1<<7|1<<8|1<<9)
show_menu( id, keys, txt, -1 )
return PLUGIN_CONTINUE
}
public helpmenuitm(id, key) {
switch(key)
{
case 0:helpitemFFm1(id)
case 1:helpitemFFm1(id)
case 2:helpitemFFm1(id)
case 3:helpitemFFm1(id)
case 4:helpitemFFm1(id)
case 5:helpitemFFm1(id)
case 6:helpitemFFm1(id)
case 7:helpitemFFm1(id)
case 8:helpitemFFm1(id)
case 9:helpitemFFm1(id)
}
return PLUGIN_CONTINUE
}
public helpitemFFm1(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE
new txt[999]
new keys
format(txt,998,"^nITEM EFFECTS PART 2:")
add(txt,998,"^n\y1.Chocobo Feather: Casts Haste on User")
add(txt,998,"^n\y2.Elixir: Cures HP and MP completely")
add(txt,998,"^n\y3.Antidote: Cures Poison")
add(txt,998,"^n\y4.Remedy: Cures all status ailments")
add(txt,998,"^n\y5.Mana Tablet: Doubles Mana")
add(txt,998,"^n\y6.Invis Potion: Invisibility for 12 seconds")
add(txt,998,"^n\y7.WeightLoss: Low grave for 15 seconds")
add(txt,998,"^n\y8.Player Change: Changes player")
add(txt,998,"^n\y9.Teleporter, Hostage : Teleports you to hostage.")
add(txt,998,"^n\y9.Teleporter, Home : Teleports you to your Home.")
add(txt,998,"^n\y9.Teleporter, Enemy : Teleports you to your Enemy.")
add(txt,998, "^n^n\d0. Exit" )
keys = (1<<0|1<<1|1<<2|1<<3|1<<4|1<<5|1<<6|1<<7|1<<8|1<<9)
show_menu( id, keys, txt, -1 )
return PLUGIN_CONTINUE
}
public helpmenuitm1(id, key) {
switch(key)
{
case 0:return PLUGIN_CONTINUE
case 1:return PLUGIN_CONTINUE
case 2:return PLUGIN_CONTINUE
case 3:return PLUGIN_CONTINUE
case 4:return PLUGIN_CONTINUE
case 5:return PLUGIN_CONTINUE
case 6:return PLUGIN_CONTINUE
case 7:return PLUGIN_CONTINUE
case 8:return PLUGIN_CONTINUE
case 9:return PLUGIN_CONTINUE
}
return PLUGIN_CONTINUE
}
public aboutFFm(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE	
new txt[999]
add(txt,998,"Current Version:v0.82")
add(txt,998,"^nAuthor:Z4KN4F31N modified and maintained in its form by Timmi")
show_motd(id,txt,"Final Fantasy X MOD change log")
return PLUGIN_CONTINUE
}
public helpnpcFFm(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE	
new txt[999]
add(txt,998,"NPC COMMANDS:")
add(txt,998,"^nBless Me: This command protects the user against all status ailments for this round")
add(txt,998,"^nProtect Me: Adds 3 defense to the user for this round(protects against critical hits/lessens effects of skills. Can be used as many times as you want)")
add(txt,998,"^nHeal Me: Fully heals user's health")
add(txt,998,"^nAssign Me a Quest: Gives user a quest he must accomplish. On accomplishment, return to the NPC for a reward. Rewards are XP, gil and cs money")
add(txt,998,"^nBuy Expreience: Sells you experience for gil")
add(txt,998,"^nRemove Quest: removes quest (quests broken till next release")
add(txt,998,"^nShop: Sells Items and Armors (WOOT)")
add(txt,998,"^nBank: New banking feture adds A bank with INTREST AND EXCHANGE RATES FOR GIL AND GOLD RATES RANDOM 1 to 3 times as much :)")
show_motd(id,txt,"Final Fantasy X MOD v0.5.6 NPC")
return PLUGIN_CONTINUE
}
public helpnpcFF(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE	
console_print(id, "===============================================")
console_print(id, "Final Fantasy X MOD HELP FILE v0.5.6 by Z4KN4F31N modified and maintained by Timmi the savage")
console_print(id, "NPC COMMANDS:")
console_print(id, "===============================================")
console_print(id, "Bless Me: This command protects the user against all status ailments for this round")
console_print(id, "Protect Me: Adds 3 defense to the user for this round(protects against critical hits/lessens effects of skills. Can be used as many times as you want)")
console_print(id, "Heal Me: Fully heals user's health")
console_print(id, "Assign Me a Quest: Gives user a quest he must accomplish. On accomplishment, return to the NPC for a reward. Rewards are XP, gil and cs money")
return PLUGIN_CONTINUE
}
//Menu's for help system
public helpme(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE	
new szMenuBody1[290]
new keys
format(szMenuBody1, 289, "\rHelp Menu:")
add( szMenuBody1, 289, "^n^n\y1. Keys For !bindFF or say /bind" )
add( szMenuBody1, 289, "^n\y2. Stats help" )
add( szMenuBody1, 289, "^n\y3. Magic Help" )
add( szMenuBody1, 289, "^n\y4. Weapon Help" )
add( szMenuBody1, 289, "^n\y5. Item Help" )
add( szMenuBody1, 289, "^n\y6. NPC Help" )
add( szMenuBody1, 289, "^n\y7. FFX 0.5.1 Changelog" )
add( szMenuBody1, 289, "^n\y8. Main Menu" )
add( szMenuBody1, 289, "^n^n\d0. Exit" )
keys = (1<<0|1<<1|1<<2|1<<3|1<<4|1<<5|1<<6|1<<7|1<<9)
show_menu( id, keys, szMenuBody1, -1 )
return PLUGIN_CONTINUE
}

public HelpMenu(id, key) {
switch(key)
{
case 0:helpmeFF(id)
case 1:helpstatsFFm(id)
case 2:helpmagicFFm(id)
case 3:helpweaponFFm(id)
case 4:helpitemFFm(id)
case 5:helpnpcFFm(id)
case 6:aboutFFm(id)
case 7:BESTMENU(id)

case 9:return PLUGIN_CONTINUE
}
return PLUGIN_CONTINUE
}

public helpme1(id) {
if (ffxenable==false)
return PLUGIN_CONTINUE	
new szMenuBody13[290]
new keys
format(szMenuBody13, 289, "\rHelp Menu1:")
add( szMenuBody13, 289, "^n^n\y1. Help Menu" )
add( szMenuBody13, 289, "^n^n\d0. Exit" )
keys = (1<<0|1<<9)
show_menu( id, keys, szMenuBody13, -1 )
return PLUGIN_CONTINUE
}

public HelpMenu1(id, key) {
switch(key)
{
case 0:helpme(id)

case 9:return PLUGIN_CONTINUE
}
return PLUGIN_CONTINUE
}

