/*					THIS IS A TEST VERSION !!!!!!!!!!!!!!!
							FINAL FANTASY 10
							      v0.85
							      
							      v.051
							   Originally
						  	  By Z4KN4F31N
					MOdified and Ported to Amxx By Timmi
					Changed Saving and Loading.
					THIS IS A TEST COPY!!!!!!!!!!!!!!!!!!!!!
*/
//ffx_poshud
//#include <ffx_poshud>
//****************************************************Begin Hud adjustment//****************************************************
//Sets the ingame hud display with all your stats and items
//Default is posupcenter 
//Will be adding a sprite background if possible

public infofunct(id) {
if (ffxenable==false)return PLUGIN_CONTINUE
if (id == 0) return PLUGIN_CONTINUE
if (id > get_playersnum()) return PLUGIN_CONTINUE
if (regenitm[id]==1) {
set_user_health(id,get_user_health(id)+random(5))
if (get_user_health(id)>HPbonus[id]+100) set_user_health(id, HPbonus[id]+100)
}


if (options[id]==21) {
new modif = i_level[id] * XPMODIFIER
set_hudmessage(75,200,200,-1.0,0.86,0,6.0,2.0,0.1,0.5,HMCHAN_PLAYERINFO)
show_hudmessage(id, "Current Level:%d. /Next:%d Current MP:%d/%d Gil:%d", i_level[id], modif , MP[id], MPmax[id], gil[id])
}
if (options[id]==22) {
new modif = i_level[id] * XPMODIFIER
set_hudmessage(75,200,200,-1.0,0.86,0,6.0,2.0,0.1,0.5,HMCHAN_PLAYERINFO)
show_hudmessage(id, "Current Level:%d. /Next:%d Current MP:%d/%d Gil:%d^n STR:%d. DEF:%d. MAG:%d. AGI:%d. ", i_level[id], modif , MP[id], MPmax[id], gil[id], STR[id], DEF[id], MAG[id], AGI[id])
}

if (options[id]==23) {
set_hudmessage(75,200,200,-1.0,0.86,0,6.0,2.0,0.1,0.5,HMCHAN_PLAYERINFO)
show_hudmessage(id, "Tidus. Current Level:%d. Current MP:%d/%d Gil:%d^nWeapon:%s. Armor:%s.%s^nItem:%s. STR:%d. DEF:%d. MAG:%d. AGI:%d. ", i_level[id], MP[id], MPmax[id], gil[id],weaponstring[weaponnum[id]],armorstring[armornum[id]],ringstring[ringnum[id]],itemstring[itemnum[id]], STR[id], DEF[id], MAG[id], AGI[id])
}

if (options[id]==11) {
new modif = i_level[id] * XPMODIFIER
set_hudmessage(75,200,200,-1.0,0.01,0,6.0,2.0,0.1,0.5,HMCHAN_PLAYERINFO)
show_hudmessage(id, "|..Current Level:%d. /Next:%d Current ^n|..MP:%d/%d Gil:%d", i_level[id], modif , MP[id], MPmax[id], gil[id])
}
if (options[id]==12) {
new modif = i_level[id] * XPMODIFIER
set_hudmessage(75,200,200,-1.0,0.01,0,6.0,2.0,0.1,0.5,HMCHAN_PLAYERINFO)
show_hudmessage(id, "|..Current Level:%d. /Next:%d Current ^n|..MP:%d/%d Gil:%d ^n|..STR:%d. | DEF:%d. | MAG:%d. | AGI:%d. |", i_level[id], modif , MP[id], MPmax[id], gil[id], STR[id], DEF[id], MAG[id], AGI[id])
}
if (options[id]==13) {
new modif = i_level[id] * XPMODIFIER
set_hudmessage(75,200,200,-1.0,0.01,0,6.0,2.0,0.1,0.5,HMCHAN_PLAYERINFO)
show_hudmessage(id, "|..Current Level:%d. /Next:%d Current MP:%d/%d Gil:%d^n |..Weapon: %s. Armor: %s. ^n|.. Upgrade:%s Item:%s. ^n|..STR:%d. | DEF:%d. | MAG:%d. | AGI:%d. | ", i_level[id], modif, MP[id], MPmax[id], gil[id], weaponstring[weaponnum[id]], armorstring[armornum[id]], ringstring[ringnum[id]], itemstring[itemnum[id]], STR[id], DEF[id], MAG[id], AGI[id])
}
return PLUGIN_CONTINUE
}
