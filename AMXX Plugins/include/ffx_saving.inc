/*					THIS IS A TEST VERSION !!!!!!!!!!!!!!!
							FINAL FANTASY 10
							      v0.85
							      
							      v.051
							   Originally
						  	  By Z4KN4F31N
					MOdified and Ported to Amxx By Timmi
					Changed Saving and Loading.
					THIS IS A TEST COPY!!!!!!!!!!!!!!!!!!!!!
*/

// SAVING 

public saveinv(id) {
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35], key[51], vaultdata2[512], allowfilepath[251],  directory[201]
	get_user_authid(id,sid,34)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx/inven_%s.sav",directory,sid)
	format(key,50, "%s_save", sid)
	savenum[id] = savenum[id] + 1
	format (vaultdata2, 511, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
	inventory[id][0], inventory[id][1],inventory[id][2],inventory[id][3],inventory[id][4],inventory[id][5],
	inventory[id][6],inventory[id][7],inventory[id][8],inventory[id][9],inventory[id][10],
	inventory[id][11],inventory[id][12],inventory[id][13],inventory[id][14],inventory[id][15],
	inventory[id][16],inventory[id][17],inventory[id][18],inventory[id][19],inventory[id][20],
	inventory[id][21],inventory[id][22],inventory[id][23],inventory[id][24])
	if (file_exists(allowfilepath)) {
		delete_file(allowfilepath)
		write_file(allowfilepath, vaultdata2, -1)
		return PLUGIN_CONTINUE
	}
	delete_file(allowfilepath)
	write_file(allowfilepath, vaultdata2, -1)
	return PLUGIN_CONTINUE
}


public loadinv(id) {
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35], allowfilepath[251], directory[201], key[51], vaultdata[512], reach, line
	get_user_authid(id,sid,34)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx/inven_%s.sav",directory,sid)
	format(key,50, "%s_save", sid) 
	if(file_exists(allowfilepath)) {
		read_file(allowfilepath,line,vaultdata,511,reach)
		new invg[9], invg1[9], invg2[9], invg3[9], invg4[9], invg5[9], invg6[9], invg7[9], invg8[9], invg9[9], invg10[9]
		new invg11[9], invg12[9], invg13[9], invg14[9], invg15[9], invg16[9], invg17[9], invg18[9], invg19[9], invg20[9], invg21[9], invg22[9], invg23[9], invg24[9]
		parse(vaultdata,invg,11,invg1,11,invg2,11,invg3,11,invg4,11,invg5,11,invg6,11,invg7,11,invg8,11,invg9,11,invg10,11,invg11,11,invg12,11,invg13,11,invg14,11,invg15,11,invg16,11,invg17,11,invg18,11,invg19,11,invg20,11,invg21,11,invg22,11,invg23,11,invg24,11)
		inventory[id][0] = str_to_num(invg)
		inventory[id][1] = str_to_num(invg1)
		inventory[id][2] = str_to_num(invg2)
		inventory[id][3] = str_to_num(invg3)
		inventory[id][4] = str_to_num(invg4)
		inventory[id][5] = str_to_num(invg5)
		inventory[id][6] = str_to_num(invg6)
		inventory[id][7] = str_to_num(invg7)
		inventory[id][8] = str_to_num(invg8)
		inventory[id][9] = str_to_num(invg9)
		inventory[id][10] = str_to_num(invg10)
		inventory[id][11] = str_to_num(invg11)
		inventory[id][12] = str_to_num(invg12)
		inventory[id][13] = str_to_num(invg13)
		inventory[id][14] = str_to_num(invg14)
		inventory[id][15] = str_to_num(invg15)
		inventory[id][16] = str_to_num(invg16)
		inventory[id][17] = str_to_num(invg17)
		inventory[id][18] = str_to_num(invg18)
		inventory[id][19] = str_to_num(invg19)
		inventory[id][20] = str_to_num(invg20)
		inventory[id][21] = str_to_num(invg21)
		inventory[id][22] = str_to_num(invg22)
		inventory[id][23] = str_to_num(invg23)
		inventory[id][24] = str_to_num(invg24)
	}
	return PLUGIN_HANDLED
}


public savearm(id) {
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35], key[51], vaultdata2[512], allowfilepath[251],  directory[201]
	get_user_authid(id,sid,34)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx/armor_%s.sav",directory,sid)
	format(key,50, "%s_save", sid)
	savenum[id] = savenum[id] + 1
	format (vaultdata2, 511, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
	arminventory[id][0], arminventory[id][1],arminventory[id][2],arminventory[id][3],arminventory[id][4],arminventory[id][5],
	arminventory[id][6],arminventory[id][7],arminventory[id][8],arminventory[id][9],arminventory[id][10],
	arminventory[id][11],arminventory[id][12],arminventory[id][13],arminventory[id][14],arminventory[id][15],
	arminventory[id][16],arminventory[id][17],arminventory[id][18],arminventory[id][19],arminventory[id][20],
	arminventory[id][21],arminventory[id][22],arminventory[id][23],arminventory[id][24])
	if (file_exists(allowfilepath)) {
		delete_file(allowfilepath)
		write_file(allowfilepath, vaultdata2, -1)
		return PLUGIN_CONTINUE
	}
	delete_file(allowfilepath)
	write_file(allowfilepath, vaultdata2, -1)
	return PLUGIN_CONTINUE
}


public loadarm(id) {
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35], allowfilepath[251], directory[201], key[51], vaultdata[512], reach, line
	get_user_authid(id,sid,34)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx/armor_%s.sav",directory,sid)
	format(key,50, "%s_save", sid) 
	if(file_exists(allowfilepath)) {
		read_file(allowfilepath,line,vaultdata,511,reach)
		new arinvg[9], arinvg1[9], arinvg2[9], arinvg3[9], arinvg4[9], arinvg5[9], arinvg6[9], arinvg7[9], arinvg8[9], arinvg9[9], arinvg10[9]
		new arinvg11[9], arinvg12[9], arinvg13[9], arinvg14[9], arinvg15[9], arinvg16[9], arinvg17[9], arinvg18[9], arinvg19[9], arinvg20[9], arinvg21[9], arinvg22[9], arinvg23[9], arinvg24[9]
		parse(vaultdata,arinvg,11,arinvg1,11,arinvg2,11,arinvg3,11,arinvg4,11,arinvg5,11,arinvg6,11,arinvg7,11,arinvg8,11,arinvg9,11,arinvg10,11,arinvg11,11,arinvg12,11,arinvg13,11,arinvg14,11,arinvg15,11,arinvg16,11,arinvg17,11,arinvg18,11,arinvg19,11,arinvg20,11,arinvg21,11,arinvg22,11,arinvg23,11,arinvg24,11)
		arminventory[id][0] = str_to_num(arinvg)
		arminventory[id][1] = str_to_num(arinvg1)
		arminventory[id][2] = str_to_num(arinvg2)
		arminventory[id][3] = str_to_num(arinvg3)
		arminventory[id][4] = str_to_num(arinvg4)
		arminventory[id][5] = str_to_num(arinvg5)
		arminventory[id][6] = str_to_num(arinvg6)
		arminventory[id][7] = str_to_num(arinvg7)
		arminventory[id][8] = str_to_num(arinvg8)
		arminventory[id][9] = str_to_num(arinvg9)
		arminventory[id][10] = str_to_num(arinvg10)
		arminventory[id][11] = str_to_num(arinvg11)
		arminventory[id][12] = str_to_num(arinvg12)
		arminventory[id][13] = str_to_num(arinvg13)
		arminventory[id][14] = str_to_num(arinvg14)
		arminventory[id][15] = str_to_num(arinvg15)
		arminventory[id][16] = str_to_num(arinvg16)
		arminventory[id][17] = str_to_num(arinvg17)
		arminventory[id][18] = str_to_num(arinvg18)
		arminventory[id][19] = str_to_num(arinvg19)
		arminventory[id][20] = str_to_num(arinvg20)
		arminventory[id][21] = str_to_num(arinvg21)
		arminventory[id][0] = str_to_num(arinvg22)
		arminventory[id][0] = str_to_num(arinvg23)
		arminventory[id][0] = str_to_num(arinvg24)
	}
	return PLUGIN_HANDLED
}

public saveskill(id) {
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35], key[51], vaultdata2[512], allowfilepath[251],  directory[201]
	get_user_authid(id,sid,34)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx/skill%s.sav",directory,sid)
	format(key,50, "%s_save", sid)
	savenum[id] = savenum[id] + 1
	format (vaultdata2, 511, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
	skillnum[id][1],skillnum[id][2],skillnum[id][3],skillnum[id][4],skillnum[id][5],
	skillnum[id][6],skillnum[id][7],skillnum[id][8],skillnum[id][9],skillnum[id][10],
	skillnum[id][11],skillnum[id][12],skillnum[id][13],skillnum[id][14],skillnum[id][15],
	skillnum[id][16],skillnum[id][17],skillnum[id][18],skillnum[id][19],skillnum[id][20],
	skillnum[id][21],skillnum[id][22],skillnum[id][23],skillnum[id][24],skillnum[id][25],
	skillnum[id][26],skillnum[id][27],skillnum[id][28],skillnum[id][29],skillnum[id][30],
	skillnum[id][31],skillnum[id][32],skillnum[id][33],skillnum[id][34],skillnum[id][35])
	if (file_exists(allowfilepath)) {
		delete_file(allowfilepath)
		write_file(allowfilepath, vaultdata2, -1)
		return PLUGIN_CONTINUE
	}
	delete_file(allowfilepath)
	write_file(allowfilepath, vaultdata2, -1)
	return PLUGIN_CONTINUE
}


public loadskill(id) {
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35], allowfilepath[251], directory[201], key[51], vaultdata[512], reach, line
	get_user_authid(id,sid,34)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx/skill%s.sav",directory,sid)
	format(key,50, "%s_save", sid) 
	if(file_exists(allowfilepath)) {
		read_file(allowfilepath,line,vaultdata,511,reach)
		new sklnum[3], sklnum1[3], sklnum2[3], sklnum3[3], sklnum4[3], sklnum5[3], sklnum6[3], sklnum7[3], sklnum8[3], sklnum9[3], sklnum10[3]
		new sklnum11[3], sklnum12[3], sklnum13[3], sklnum14[3], sklnum15[3], sklnum16[3], sklnum17[3], sklnum18[3], sklnum19[3]
		new sklnum20[3]
		 
		parse(vaultdata,sklnum,6,sklnum1,6,sklnum2,6,sklnum3,6,sklnum4,6,sklnum5,6,sklnum6,6,sklnum7,6,sklnum8,6,sklnum9,6,sklnum10,6,sklnum11,6,sklnum12,6,sklnum13,6,sklnum14,6,sklnum15,6,sklnum16,6,sklnum17,6,sklnum18,6,sklnum19,6,sklnum20,6)
		skillnum[id][1] = str_to_num(sklnum)
		skillnum[id][2] = str_to_num(sklnum1)
		skillnum[id][3] = str_to_num(sklnum2)
		skillnum[id][4] = str_to_num(sklnum3)
		skillnum[id][5] = str_to_num(sklnum4)
		skillnum[id][6] = str_to_num(sklnum5)
		skillnum[id][7] = str_to_num(sklnum6)
		skillnum[id][8] = str_to_num(sklnum7)
		skillnum[id][9] = str_to_num(sklnum8)
		skillnum[id][10] = str_to_num(sklnum9)
		skillnum[id][11] = str_to_num(sklnum10)
		skillnum[id][12] = str_to_num(sklnum11)
		skillnum[id][13] = str_to_num(sklnum12)
		skillnum[id][14] = str_to_num(sklnum13)
		skillnum[id][15] = str_to_num(sklnum14)
		skillnum[id][16] = str_to_num(sklnum15)
		skillnum[id][17] = str_to_num(sklnum16)
		skillnum[id][18] = str_to_num(sklnum17)
		skillnum[id][19] = str_to_num(sklnum18)
		skillnum[id][20] = str_to_num(sklnum19)
		skillnum[id][21] = str_to_num(sklnum20)
	}
	return PLUGIN_HANDLED
}

public saveskill1(id) {
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35], key[51], vaultdata2[512], allowfilepath[251],  directory[201]
	get_user_authid(id,sid,34)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx/skill1%s.sav",directory,sid)
	format(key,50, "%s_save", sid)
	savenum[id] = savenum[id] + 1
	format (vaultdata2, 511, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
	skillnum[id][21],skillnum[id][22],skillnum[id][23],skillnum[id][24],skillnum[id][25],
	skillnum[id][26],skillnum[id][27],skillnum[id][28],skillnum[id][29],skillnum[id][30],
	skillnum[id][31],skillnum[id][32],skillnum[id][33],skillnum[id][34],skillnum[id][35])
	if (file_exists(allowfilepath)) {
		delete_file(allowfilepath)
		write_file(allowfilepath, vaultdata2, -1)
		return PLUGIN_CONTINUE
	}
	delete_file(allowfilepath)
	write_file(allowfilepath, vaultdata2, -1)
	return PLUGIN_CONTINUE
}


public loadskill1(id) {
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35], allowfilepath[251], directory[201], key[51], vaultdata[512], reach, line
	get_user_authid(id,sid,34)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx/skill1%s.sav",directory,sid)
	format(key,50, "%s_save", sid) 
	if(file_exists(allowfilepath)) {
		read_file(allowfilepath,line,vaultdata,511,reach)
		new sklnum21[3], sklnum22[3], sklnum23[3], sklnum24[3], sklnum25[3], sklnum26[3], sklnum27[3], sklnum28[3], sklnum29[9], sklnum30[9], sklnum31[9], sklnum32[9], sklnum33[9]
		parse(vaultdata,sklnum21,6,sklnum22,6,sklnum23,6,sklnum24,6,sklnum25,6,sklnum26,6,sklnum27,6,sklnum28,6,sklnum29,6,sklnum30,6,sklnum31,6,sklnum32,6,sklnum33,6)
		skillnum[id][21] = str_to_num(sklnum21)
		skillnum[id][22] = str_to_num(sklnum22)
		skillnum[id][23] = str_to_num(sklnum23)
		skillnum[id][24] = str_to_num(sklnum24)
		skillnum[id][25] = str_to_num(sklnum25)
		skillnum[id][26] = str_to_num(sklnum26)
		skillnum[id][27] = str_to_num(sklnum27)
		skillnum[id][28] = str_to_num(sklnum28)
		skillnum[id][29] = str_to_num(sklnum29)
		skillnum[id][30] = str_to_num(sklnum30)
		skillnum[id][31] = str_to_num(sklnum31)
		skillnum[id][32] = str_to_num(sklnum32)
		skillnum[id][33] = str_to_num(sklnum33)

	}
	return PLUGIN_HANDLED
}


public savespells(id) {
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35], key[51], vaultdata2[512], allowfilepath[251],  directory[201]
	get_user_authid(id,sid,34)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx/spells%s.sav",directory,sid)
	format(key,50, "%s_save", sid)
	savenum[id] = savenum[id] + 1
	format (vaultdata2, 511, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
	skillnum[id][35],skillnum[id][36],skillnum[id][37],skillnum[id][38],skillnum[id][39],
	skillnum[id][40],skillnum[id][41],skillnum[id][42],skillnum[id][43],skillnum[id][44],
	skillnum[id][45],skillnum[id][46],skillnum[id][47],skillnum[id][48],skillnum[id][49],
	skillnum[id][50],skillnum[id][51],skillnum[id][52],skillnum[id][53],skillnum[id][54])
	if (file_exists(allowfilepath)) {
		delete_file(allowfilepath)
		write_file(allowfilepath, vaultdata2, -1)
		return PLUGIN_CONTINUE
	}
	delete_file(allowfilepath)
	write_file(allowfilepath, vaultdata2, -1)
	return PLUGIN_CONTINUE
}


public loadspells(id) {
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35], allowfilepath[251], directory[201], key[51], vaultdata[512], reach, line
	get_user_authid(id,sid,34)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx/spells%s.sav",directory,sid)
	format(key,50, "%s_save", sid) 
	if(file_exists(allowfilepath)) {
		read_file(allowfilepath,line,vaultdata,511,reach)
		new skilnum[9], skilnum1[9], skilnum2[9], skilnum3[9], skilnum4[9], skilnum5[9], skilnum6[9], skilnum7[9], skilnum8[9], skilnum9[9], skilnum10[9]
		new skilnum11[9], skilnum12[9], skilnum13[9], skilnum14[9], skilnum15[9], skilnum16[9], skilnum17[9], skilnum18[9], skilnum19[9]
		parse(vaultdata,skilnum,11,skilnum1,11,skilnum2,11,skilnum3,11,skilnum4,11,skilnum5,11,skilnum6,11,skilnum7,11,skilnum8,11,skilnum9,11,skilnum10,11,skilnum11,11,skilnum12,11,skilnum13,11,skilnum14,11,skilnum15,11,skilnum16,11,skilnum17,11,skilnum18,11,skilnum19,11)
		skillnum[id][35] = str_to_num(skilnum)
		skillnum[id][36] = str_to_num(skilnum1)
		skillnum[id][37] = str_to_num(skilnum2)
		skillnum[id][38] = str_to_num(skilnum3)
		skillnum[id][39] = str_to_num(skilnum4)
		skillnum[id][40] = str_to_num(skilnum5)
		skillnum[id][41] = str_to_num(skilnum6)
		skillnum[id][42] = str_to_num(skilnum7)
		skillnum[id][43] = str_to_num(skilnum8)
		skillnum[id][44] = str_to_num(skilnum9)
		skillnum[id][45] = str_to_num(skilnum10)
		skillnum[id][46] = str_to_num(skilnum11)
		skillnum[id][47] = str_to_num(skilnum12)
		skillnum[id][48] = str_to_num(skilnum13)
		skillnum[id][49] = str_to_num(skilnum14)
		skillnum[id][50] = str_to_num(skilnum15)
		skillnum[id][51] = str_to_num(skilnum16)
		skillnum[id][52] = str_to_num(skilnum17)
		skillnum[id][53] = str_to_num(skilnum18)
		skillnum[id][54] = str_to_num(skilnum19)
	}
	return PLUGIN_HANDLED
}

public saveskills(id) {
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35], key[51], vaultdata2[512], allowfilepath[251],  directory[201]
	get_user_authid(id,sid,34)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx/skills%s.sav",directory,sid)
	format(key,50, "%s_save", sid)
	savenum[id] = savenum[id] + 1
	format (vaultdata2, 511, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
	wpnupg[id][0][0],wpnupg[id][0][1],wpnupg[id][0][2],wpnupg[id][0][3],wpnupg[id][1][0],
	wpnupg[id][1][1],wpnupg[id][1][2],wpnupg[id][1][3],wpnupg[id][2][0],wpnupg[id][2][1],
	wpnupg[id][2][2],wpnupg[id][2][3],wpnupg[id][3][0],wpnupg[id][3][1],wpnupg[id][3][2],
	wpnupg[id][3][3],wpnupg[id][4][0],wpnupg[id][4][1],wpnupg[id][4][2],wpnupg[id][4][3])
	if (file_exists(allowfilepath)) {
		delete_file(allowfilepath)
		write_file(allowfilepath, vaultdata2, -1)
		return PLUGIN_CONTINUE
	}
	delete_file(allowfilepath)
	write_file(allowfilepath, vaultdata2, -1)
	return PLUGIN_CONTINUE
}


public loadskills(id) {
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35], allowfilepath[251], directory[201], key[51], vaultdata[512], reach, line
	get_user_authid(id,sid,34)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx/skills%s.sav",directory,sid)
	format(key,50, "%s_save", sid) 
	if(file_exists(allowfilepath)) {
		read_file(allowfilepath,line,vaultdata,511,reach)
		new wpg[9], wpg1[9], wpg2[9], wpg3[9], wpg4[9], wpg5[9], wpg6[9], wpg7[9], wpg8[9], wpg9[9], wpg10[9]
		new wpg11[9], wpg12[9], wpg13[9], wpg14[9], wpg15[9], wpg16[9], wpg17[9], wpg18[9], wpg19[9]
		parse(vaultdata,wpg,11,wpg1,11,wpg2,11,wpg3,11,wpg4,11,wpg5,11,wpg6,11,wpg7,11,wpg8,11,wpg9,11,wpg10,11,wpg11,11,wpg12,11,wpg13,11,wpg14,11,wpg15,11,wpg16,11,wpg17,11,wpg18,11,wpg19,11)
		wpnupg[id][0][0] = str_to_num(wpg)
		wpnupg[id][0][1] = str_to_num(wpg1)
		wpnupg[id][0][2] = str_to_num(wpg2)
		wpnupg[id][0][3] = str_to_num(wpg3)
		wpnupg[id][0][0] = str_to_num(wpg4)
		wpnupg[id][0][1] = str_to_num(wpg5)
		wpnupg[id][0][2] = str_to_num(wpg6)
		wpnupg[id][0][3] = str_to_num(wpg7)
		wpnupg[id][0][0] = str_to_num(wpg8)
		wpnupg[id][0][1] = str_to_num(wpg9)
		wpnupg[id][0][2] = str_to_num(wpg10)
		wpnupg[id][0][3] = str_to_num(wpg11)
		wpnupg[id][0][0] = str_to_num(wpg12)
		wpnupg[id][0][1] = str_to_num(wpg13)
		wpnupg[id][0][2] = str_to_num(wpg14)
		wpnupg[id][0][3] = str_to_num(wpg15)
		wpnupg[id][0][0] = str_to_num(wpg16)
		wpnupg[id][0][1] = str_to_num(wpg17)
		wpnupg[id][0][2] = str_to_num(wpg18)
		wpnupg[id][0][3] = str_to_num(wpg19)
		client_print(id,print_chat,"[FFX] Your experience has been loaded :).")
	}
	return PLUGIN_HANDLED
}

public savexpffx(id) {	
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35],key[51], vaultdata2[512], allowfilepath[251], directory[201]
	get_user_authid(id,sid,34)
	get_configsdir(directory,200)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	format(allowfilepath,250,"%s/ffx/ffx%s.sav",directory,sid)
	format(key,50, "%s_save", sid)
	savenum[id] = savenum[id] + 1
	format (vaultdata2, 511, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",savenum[id],XP[id],i_level[id],MPmax[id],HPbonus[id],
	STR[id],DEF[id],MAG[id],AGI[id],weaponnum[id],armornum[id],magiccounter[id],skillpoints[id],BANKAC[id],ringnum[id],ringmag[id],options[id],soundoption[id],iconoptions[id])
	if (file_exists(allowfilepath)) {
		delete_file(allowfilepath)
		write_file(allowfilepath, vaultdata2, -1)
		client_print(id,print_chat,"[FFX] Your experience has been saved :).")
		return PLUGIN_CONTINUE
	}
	delete_file(allowfilepath)
	write_file(allowfilepath, vaultdata2, -1)
	client_print(id,print_chat,"[FFX] Thank Your for Playing FFX your Xp has been saved :).")
	return PLUGIN_CONTINUE
}

public load_xp(id) {
	if (!is_user_connected(id))	return PLUGIN_CONTINUE
	new sid[35], directory[201], allowfilepath[251], key[51], vaultdata[512], reach, line
	get_user_authid(id,sid,34)
	if (is_user_bot(id)) get_user_name(id,sid,34)
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx/ffx%s.sav",directory,sid)
	format(key,50, "%s_save", sid) 
	if(file_exists(allowfilepath)) {
		read_file(allowfilepath,line,vaultdata,511,reach)
		new savenumget[9], XPget[9], i_levelget[9], MPmaxget[9], HPbonusget[9], STRget[9], DEFget[9], MAGget[9], AGIget[9], weaponnumget[9], armornumget[9]
		new magiccounterget[9], skillpointsget[9], BANKACget[9], ringnumget[9], ringmagget[9], optionsget[9], soundsget[9], iconoptionsg[9]
		parse(vaultdata,savenumget,11,XPget,11,i_levelget,5,MPmaxget,6,HPbonusget,6,STRget,5,DEFget,5,MAGget,5,AGIget,5,weaponnumget,2,armornumget,2,
		magiccounterget,2,skillpointsget,1,BANKACget,11,ringnumget,2,ringmagget,2,optionsget,2,soundsget,2, iconoptionsg,2)		
		XP[id]=str_to_num(XPget)
		i_level[id]=str_to_num(i_levelget)
		MPmax[id]=str_to_num(MPmaxget)
		HPbonus[id]=str_to_num(HPbonusget)
		STR[id]=str_to_num(STRget)
		DEF[id]=str_to_num(DEFget)
		MAG[id]=str_to_num(MAGget)
		AGI[id]=str_to_num(AGIget)
		BANKAC[id]=str_to_num(BANKACget)
		weaponnum[id]=str_to_num(weaponnumget)
		armornum[id]=str_to_num(armornumget)
		magiccounter[id]=str_to_num(magiccounterget)
		skillpoints[id]=str_to_num(skillpointsget)
		ringnum[id]=str_to_num(ringnumget)
		ringmag[id]=str_to_num(ringmagget)
		savenum[id] = str_to_num(savenumget)
		options[id] = str_to_num(optionsget)
		soundoption[id] = str_to_num(soundsget)
		iconoptions[id] = str_to_num(iconoptionsg)
		client_print(id,print_chat,"[FFX] Your experience and options have been loaded :).")
	}
	if (options[id] == 0) options[id] = 12
	return PLUGIN_HANDLED
}


public saveini() {
	new allowfilepath[251], directory[201], vaultdata2[512]
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx.ini",directory)
	format (vaultdata2, 511, "%d %d %d %d %d %d %d %d %d",XPMODIFIER,WEAPONRANDOMIZER,ARMORRANDOMIZER,ITEMRANDOMIZER,
	GILPERWINRANDOMIZER,HPBONUSMODIFIER,SKBONUSMODIFIER,NPCMODIFIER,MPBONUSMODIFIER)
	if (file_exists(allowfilepath)) {
		delete_file(allowfilepath)
		write_file(allowfilepath, vaultdata2, -1)
		return PLUGIN_CONTINUE
	}
	delete_file(allowfilepath)
	write_file(allowfilepath, vaultdata2, -1)
	return PLUGIN_CONTINUE
}
/*

public loadini() {
	new allowfilepath[251]
	new directory[201]
	get_configsdir(directory,200)
	format(allowfilepath,250,"%s/ffx.ini",directory)
	new vaultdata[512]
	new line
	new reach
	if(file_exists(allowfilepath)) {
		read_file(allowfilepath,line,vaultdata,511,reach)
		new XPMODIFIER1[9],WEAPONRANDOMIZER1[9],ARMORRANDOMIZER1[9],ITEMRANDOMIZER1[9]
		new GILPERWINRANDOMIZER1[9],HPBONUSMODIFIER1[9],SKBONUSMODIFIER1[9],NPCMODIFIER1[9],MPBONUSMODIFIER1[9]
		
		parse(vaultdata,XPMODIFIER1,11,WEAPONRANDOMIZER1,11,ARMORRANDOMIZER1,11,ITEMRANDOMIZER1,GILPERWINRANDOMIZER1,11,
		HPBONUSMODIFIER1,11,SKBONUSMODIFIER1,11,NPCMODIFIER1,11,MPBONUSMODIFIER1,11)
		
		XPMODIFIER = str_to_num( XPMODIFIER1 )   				 //ammount needed to next level * modifier
 		WEAPONRANDOMIZER = str_to_num( WEAPONRANDOMIZER1 )    	 //Weapon Randomizer
 		ARMORRANDOMIZER = str_to_num( ARMORRANDOMIZER1 ) 		 //ARMOR Randomizer
 		ITEMRANDOMIZER = str_to_num( ITEMRANDOMIZER1 ) 			 //Item Randomizer
 		GILPERWINRANDOMIZER = str_to_num( GILPERWINRANDOMIZER1 ) //GIL / MONEY Randomizer
 		HPBONUSMODIFIER = str_to_num( HPBONUSMODIFIER1 )		 //HP BONUS Randomizer
 		SKBONUSMODIFIER = str_to_num( SKBONUSMODIFIER1 )		 //SKILL BONUS Randomizer
		MPBONUSMODIFIER = str_to_num( MPBONUSMODIFIER1 )   		 //MPBONUS Randomizer
		new floattemp = str_to_num( NPCMODIFIER1 )
		NPCMODIFIER = Float:floattemp				 
	}
	return PLUGIN_HANDLED
}
*/

public savexpfile() {
	new i
	for ( i = 0 ; i <= get_playersnum() ; i ++ ) {
		new id = i
		if (!is_user_connected(id))	return PLUGIN_CONTINUE
		new parsedata[512], allowfilepath[251], directory[201], vaultdata2[512], sid[35],key[64]
		get_user_authid(id,sid,34)
		new reach
		if (is_user_bot(id)) get_user_name(id,sid,34)
		get_configsdir(directory,200)
		format(allowfilepath,250,"%s/ffx/ffx.sav",directory)
		format(key,63, "%s_sav", sid)
		savenum[id] = savenum[id] + 1
		format (vaultdata2, 511, "%s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",key,savenum[id],XP[id],i_level[id],MPmax[id],HPbonus[id],
		STR[id],DEF[id],MAG[id],AGI[id],weaponnum[id],armornum[id],magiccounter[id],skillpoints[id],BANKAC[id],ringnum[id],ringmag[id],options[id],soundoption[id],iconoptions[id])
		new a
		for ( a = 2 ; a < 512 ; a ++ ) {
			read_file(allowfilepath,a,parsedata,511,reach)
			new keyg[64], savenumget[9], XPget[9], i_levelget[9], MPmaxget[9], HPbonusget[9], STRget[9], DEFget[9], MAGget[9], AGIget[9], weaponnumget[9], armornumget[9]
			new magiccounterget[9], skillpointsget[9], BANKACget[9], ringnumget[9], ringmagget[9], optionsget[9], soundsget[9], iconoptionsg[9]
			parse(parsedata,keyg,63,savenumget,11,XPget,11,i_levelget,5,MPmaxget,6,HPbonusget,6,STRget,5,DEFget,5,MAGget,5,AGIget,5,weaponnumget,2,armornumget,2,
			magiccounterget,2,skillpointsget,1,BANKACget,11,ringnumget,2,ringmagget,2,optionsget,2,soundsget,2, iconoptionsg,2)	
			if (equali( keyg, key ) ) {
				write_file(allowfilepath, vaultdata2, a)
				client_print(id,print_chat,"[FFX] Your experience has been saved :).")
				a = 510
			}
			if (a <= 509) {
				if (containi( keyg, key ) ) {
					write_file(allowfilepath, vaultdata2, a)
					client_print(id,print_chat,"[FFX] Your experience has been saved :).")
					a = 510
				}
			}
				
			if ( a == 509 ) {
				write_file(allowfilepath, vaultdata2, -1)
				return PLUGIN_CONTINUE
			}
		}
	}
	return PLUGIN_CONTINUE
}

public loadxpfile() {
	new i
	for ( i = 0 ; i <= get_playersnum() ; i ++ ) {
		new id = i
		if (!is_user_connected(id))	return PLUGIN_CONTINUE
		new sid[35], key[64], vaultdata2[512], parsedata[512], allowfilepath[251], directory[201], a , reach
		get_user_authid(id,sid,34)
		get_configsdir(directory,200)
		if (is_user_bot(id)) get_user_name(id,sid,34)
		format(allowfilepath,250,"%s/ffx/ffx.sav",directory)
		format(key, 63, "%s_sav", sid)
		savenum[id] = savenum[id] + 1
		for ( a = 2 ; a < 512 ; a ++ ) {
			read_file(allowfilepath,a,parsedata,511,reach)
			new keyg[64], savenumget[9], XPget[9], i_levelget[9], MPmaxget[9], HPbonusget[9], STRget[9], DEFget[9], MAGget[9], AGIget[9], weaponnumget[9], armornumget[9]
			new magiccounterget[9], skillpointsget[9], BANKACget[9], ringnumget[9], ringmagget[9], optionsget[9], soundsget[9], iconoptionsg[9]
			parse(parsedata,keyg,63,savenumget,11,XPget,11,i_levelget,5,MPmaxget,6,HPbonusget,6,STRget,5,DEFget,5,MAGget,5,AGIget,5,weaponnumget,2,armornumget,2,
			magiccounterget,2,skillpointsget,1,BANKACget,11,ringnumget,2,ringmagget,2,optionsget,2,soundsget,2, iconoptionsg,2)	
			
			if (containi( keyg, key ) ) {
				XP[id]=str_to_num(XPget)
				i_level[id]=str_to_num(i_levelget)
				MPmax[id]=str_to_num(MPmaxget)
				HPbonus[id]=str_to_num(HPbonusget)
				STR[id]=str_to_num(STRget)
				DEF[id]=str_to_num(DEFget)
				MAG[id]=str_to_num(MAGget)
				AGI[id]=str_to_num(AGIget)
				BANKAC[id]=str_to_num(BANKACget)
				weaponnum[id]=str_to_num(weaponnumget)
				armornum[id]=str_to_num(armornumget)
				magiccounter[id]=str_to_num(magiccounterget)
				skillpoints[id]=str_to_num(skillpointsget)
				ringnum[id]=str_to_num(ringnumget)
				ringmag[id]=str_to_num(ringmagget)
				savenum[id] = str_to_num(savenumget)
				options[id] = str_to_num(optionsget)
				soundoption[id] = str_to_num(soundsget)
				iconoptions[id] = str_to_num(iconoptionsg)
				client_print(id,print_chat,"[FFX] Your experience and options have been loaded :).")
				a = 510
			}
			
			if (a <= 509) {
				if (containi( keyg, key ) ) {
					XP[id]=str_to_num(XPget)
					i_level[id]=str_to_num(i_levelget)
					MPmax[id]=str_to_num(MPmaxget)
					HPbonus[id]=str_to_num(HPbonusget)
					STR[id]=str_to_num(STRget)
					DEF[id]=str_to_num(DEFget)
					MAG[id]=str_to_num(MAGget)
					AGI[id]=str_to_num(AGIget)
					BANKAC[id]=str_to_num(BANKACget)
					weaponnum[id]=str_to_num(weaponnumget)
					armornum[id]=str_to_num(armornumget)
					magiccounter[id]=str_to_num(magiccounterget)
					skillpoints[id]=str_to_num(skillpointsget)
					ringnum[id]=str_to_num(ringnumget)
					ringmag[id]=str_to_num(ringmagget)
					savenum[id] = str_to_num(savenumget)
					options[id] = str_to_num(optionsget)
					soundoption[id] = str_to_num(soundsget)
					iconoptions[id] = str_to_num(iconoptionsg)
					client_print(id,print_chat,"[FFX] Your experience and options have been loaded :).")
					a = 510
				}
			}
			if ( a == 509 ) {
				write_file(allowfilepath, vaultdata2, -1)
				return PLUGIN_CONTINUE
			}
		}
	}
	return PLUGIN_CONTINUE
}

