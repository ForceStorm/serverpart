/*					THIS IS A TEST VERSION !!!!!!!!!!!!!!!
							FINAL FANTASY 10
							      v0.85
							      
							      v.051
							   Originally
						  	  By Z4KN4F31N
					MOdified and Ported to Amxx By Timmi
					Changed Saving and Loading.
					THIS IS A TEST COPY!!!!!!!!!!!!!!!!!!!!!
*/
//
// All of the player skills and skill selector (to use) information is kept here.
// We will be adding in 10 more skills and removing the player boundairy.
// Meaning sooner or later we will have a giant skill inventory o.O and perhaps player models......
//*************************************************Begin Checking skills***************************
public showskills(id, num) {
	set_hudmessage(75,200,200,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
	show_hudmessage(id, "Selected Skill : %s" ,skillnames[num] )
	return PLUGIN_CONTINUE
}
//********************************************************NEXT SKILL FUNCTION**************************************************************
public next_FFfunct(id) {
	skillnum[id][0] = 1
	currentskill[id]=currentskill[id]+1
	if (currentskill[id] >= 55) currentskill[id] = 0
	if (skillnum[id][currentskill[id]]==0) next_FFfunct(id)
	showskills(id, currentskill[id])
	return PLUGIN_CONTINUE
}	

public useskillfunct(id) {
	if (is_user_alive(id)==1) {
		if (currentskill[id]==1) {
			if (MP[id]>=5) {
				if (hasteused[id]==0) {
					if (hastegaused[id]==0) {
						if (FreezeEnded==1) set_user_maxspeed(id, 250.0 + (AGI[id]*25.0)*MAG[id])
						hasteused[id]=1
						hastefx(id)
						if (halfmp[id]==1) MP[id]=MP[id]-3
						if (onemp[id]==1) MP[id]=MP[id]-1
						if (onemp[id]==0) {
							if (halfmp[id]==0) MP[id]=MP[id]-5
						}
						client_cmd(id,"mp3 stop")
  						client_cmd(id,";mp3 play /sound/ffx/ffx_haste.mp3") 
						set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
						show_hudmessage(id, "HaStE!")
						}else{
						set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
						show_hudmessage(id, "Haste/Hastega already used this round!")
						}
					}
				}else{
				set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
				show_hudmessage(id, "Not enough MP. 5 MP needed")
			}
		}
		///////////////////////////////////////////////////////////////HASTEGA/////////////////////////////////////////////////////////////////////
		if (currentskill[id]==2) {
			if (MP[id]>=8) {
				if (hasteused[id]==0) {
					if (hastegaused[id]==0) {
						if (FreezeEnded==1) set_user_maxspeed(id, 250.0 + (AGI[id]*50)*MAG[id])
						client_cmd(id,"mp3 stop")
  						client_cmd(id,";mp3 play /sound/ffx/ffx_haste.mp3") 	
  						set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
						show_hudmessage(id, "HaStEgA!")
						if (halfmp[id]==1) MP[id]=MP[id]-4
						if (onemp[id]==1) MP[id]=MP[id]-1
						if (onemp[id]==0) {
							if (halfmp[id]==0) MP[id]=MP[id]-8
						}
						hastegaused[id]=1
						hastefx(id)
					}else{
					set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
					show_hudmessage(id, "Haste/Hastega already used this round!")
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 8 MP needed")
			}
		}
		/////////////////////////////////////////////////////////////////FLEE//////////////////////////////////////////////////////////////////////
		if (currentskill[id]==3) {
			if (MP[id]>=14) {
				if (get_user_godmode(id)==0) {
					set_user_godmode(id, 1)
					usingskill[id]=1
					if (halfmp[id]==1) MP[id]=MP[id]-7
					if (onemp[id]==1) MP[id]=MP[id]-1
					if (onemp[id]==0) {
						if (halfmp[id]==0) MP[id]=MP[id]-14
					}
					client_cmd(id,"mp3 stop")
  					client_cmd(id,";mp3 play /sound/ffx/ffx_haste.mp3") 
					fleefx(id)
					set_hudmessage(250,250,250,-1.0,0.35,1,6.0,5.0,0.1,0.5,HMCHAN_NEWROUND)
					show_hudmessage(id, "FlEe!(5 sec)")
					set_task(5.0, "fleef", id + 66, "", 0, "a", 0)
					}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 14 MP needed")
			}
		}
		/////////////////////////////////////////////////////////////////FLEE2/////////////////////////////////////////////////////////////////////
		if (currentskill[id]==4) {
			if (MP[id]>=20) {
				if (usingskill[id]==0) {
					if (get_user_godmode(id)==0) {
						set_user_godmode(id, 1)
						usingskill[id]=1
						set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
						show_hudmessage(id, "FlEe2!(10 sec)")
						if (halfmp[id]==1) MP[id]=MP[id]-10
						if (onemp[id]==1) MP[id]=MP[id]-1
						if (onemp[id]==0) {
							if (halfmp[id]==0) MP[id]=MP[id]-20
						}
						fleefx2(id)
						client_cmd(id,"mp3 stop")
  						client_cmd(id,";mp3 play /sound/ffx/ffx_haste.mp3") 						
						set_task(10.0, "fleef", id + 66, "", 0, "a", 0)
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 20 MP needed")
			}
		}
		///////////////////////////////////////////////////////////////SPIRAL CUT//////////////////////////////////////////////////////////////////
		if (currentskill[id]==5) {
			if (MP[id]>=27) {
				if (usingskill[id]==0) {
					if (blitz_aceused[id]==0) {
						if (spiral_cutused[id]==0) {
							set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
							show_hudmessage(id, "NeXt BulLeT wIlL bE a SpIrAl CuT!")
							if (halfmp[id]==1) MP[id]=MP[id]-14
							if (onemp[id]==1) MP[id]=MP[id]-1
							if (onemp[id]==0) {
								if (halfmp[id]==0) MP[id]=MP[id]-27
							}
							if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
							spiral_cutused[id]=1
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 27 MP needed")
			}
		}
		////////////////////////////////////////////////////////////////BLITZ ACE//////////////////////////////////////////////////////////////////
		if (currentskill[id]==6) {
			if (MP[id]>=80) {
				if (blitz_aceused[id]==0) {
					if (spiral_cutused[id]==0) {
						if (usingskill[id]==0) {
							set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
							show_hudmessage(id, "NeXt BulLeT wIlL bE a BliTz AcE!")
							if (halfmp[id]==1) MP[id]=MP[id]-40
							if (onemp[id]==1) MP[id]=MP[id]-1
							if (onemp[id]==0) {
								if (halfmp[id]==0) MP[id]=MP[id]-80
							}
							if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
							blitz_aceused[id]=1
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 80 MP needed")
			}
		}
		
		///////////////////////////////////////////////////////////////POWER BREAK/////////////////////////////////////////////////////////////////
		if (currentskill[id]==7) {
			if (MP[id]>=8) {
				if (power_breakused[id]==0) {
					if (armor_breakused[id]==0) {
						if (magic_breakused[id]==0) {
							if (full_breakused[id]==0) {
								if (shooting_starused[id]==0) {
									if (tornadoused[id]==0) {
										set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
										show_hudmessage(id, "NeXt BulLeT wIlL bE a PoWeR bReAk!")
										if (halfmp[id]==1) MP[id]=MP[id]-4
										if (onemp[id]==1) MP[id]=MP[id]-1
										if (onemp[id]==0) {
											if (halfmp[id]==0) MP[id]=MP[id]-8
										}
										if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
										power_breakused[id]=1
									}
								}
							}
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 8 MP needed")
			}
		}
		///////////////////////////////////////////////////////////////ARMOR BREAK/////////////////////////////////////////////////////////////////
		if (currentskill[id]==8) {
			if (MP[id]>=15) {
				if (power_breakused[id]==0) {
					if (armor_breakused[id]==0) {
						if (magic_breakused[id]==0) {
							if (full_breakused[id]==0) {
								if (shooting_starused[id]==0) {
									if (tornadoused[id]==0) {
										set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
										show_hudmessage(id, "NeXt BulLeT wIlL bE aN ArMoR bReAk!")
										if (halfmp[id]==1) MP[id]=MP[id]-8
										if (onemp[id]==1) MP[id]=MP[id]-1
										if (onemp[id]==0) {
											if (halfmp[id]==0) MP[id]=MP[id]-15
										}
										if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
										armor_breakused[id]=1
									}
								}
							}
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 15 MP needed")
			}
		}
		///////////////////////////////////////////////////////////////MAGIC BREAK/////////////////////////////////////////////////////////////////
		if (currentskill[id]==9) {
			if (MP[id]>=23) {
				if (power_breakused[id]==0) {
					if (armor_breakused[id]==0) {
						if (magic_breakused[id]==0) {
							if (full_breakused[id]==0) {
								if (shooting_starused[id]==0) {
									if (tornadoused[id]==0) {
										set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
										show_hudmessage(id, "NeXt BulLeT wIlL bE a MaGiC bReAk!")
										if (halfmp[id]==1) MP[id]=MP[id]-12
										if (onemp[id]==1) MP[id]=MP[id]-1
										if (onemp[id]==0) {
											if (halfmp[id]==0) MP[id]=MP[id]-23
										}
										if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
										magic_breakused[id]=1
									}
								}
							}
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 23 MP needed")
			}
		}
		////////////////////////////////////////////////////////////////FULL BREAK/////////////////////////////////////////////////////////////////
		if (currentskill[id]==10) {
			if (MP[id]>=50) {
				if (power_breakused[id]==0) {
					if (armor_breakused[id]==0) {
						if (magic_breakused[id]==0) {
							if (full_breakused[id]==0) {
								if (shooting_starused[id]==0) {
									if (tornadoused[id]==0) {
										set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
										show_hudmessage(id, "NeXt BulLeT wIlL bE a FuLl BrEaK!")
										if (halfmp[id]==1) MP[id]=MP[id]-25
										if (onemp[id]==1) MP[id]=MP[id]-1
										if (onemp[id]==0) {
											if (halfmp[id]==0) MP[id]=MP[id]-50
										}
										if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
										full_breakused[id]=1
									}
								}
							}
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 50 MP needed")
			}
		}
		///////////////////////////////////////////////////////////////SHOOTING STAR///////////////////////////////////////////////////////////////
		if (currentskill[id]==11) {
			if (MP[id]>=70) {
				if (power_breakused[id]==0) {
					if (armor_breakused[id]==0) {
						if (magic_breakused[id]==0) {
							if (full_breakused[id]==0) {
								if (shooting_starused[id]==0) {
									if (tornadoused[id]==0) {
										set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
										show_hudmessage(id, "NeXt BulLeT wIlL bE a ShOoTiNg StAr!")
										if (halfmp[id]==1) MP[id]=MP[id]-35
										if (onemp[id]==1) MP[id]=MP[id]-1
										if (onemp[id]==0) {
											if (halfmp[id]==0) MP[id]=MP[id]-70
										}
										if (file_exists("sound/FFX/ffx_slow.wav")) emit_sound(id, CHAN_STATIC, "FFX/ffx_slow.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
										tornadoused[id]=1
									}
								}
							}
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 70 MP needed")
			}
		}
		
		/////////////////////////////////////////////////////////////////TORNADO///////////////////////////////////////////////////////////////////
		if (currentskill[id]==12) {
			if (MP[id]>=90) {
				if (power_breakused[id]==0) {
					if (armor_breakused[id]==0) {
						if (magic_breakused[id]==0) {
							if (full_breakused[id]==0) {
								if (shooting_starused[id]==0) {
									if (tornadoused[id]==0) {
										set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
										show_hudmessage(id, "NeXt BulLeT wIlL bE a ToRnAdO!")
										if (halfmp[id]==1) MP[id]=MP[id]-45
										if (onemp[id]==1) MP[id]=MP[id]-1
										if (onemp[id]==0) {
											if (halfmp[id]==0) MP[id]=MP[id]-90
										}
										if (file_exists("sound/FFX/ffx_slow.wav")) emit_sound(id, CHAN_STATIC, "FFX/ffx_slow.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
										shooting_starused[id]=1
									}
								}
							}
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 90 MP needed")
			}
		}
		
		/////////////////////////////////////////////////////////////POISON STRIKE/////////////////////////////////////////////////////////////////
		if (currentskill[id]==13) {
			if (MP[id]>=10) {
				if (poisonstrikeused[id]==0) {
					if (silencestrikeused[id]==0) {
						if (attack_reelsused[id]==0) {
							set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
							show_hudmessage(id, "NeXt BulLeT wIlL bE a PoIsOnStRiKe!")
							if (halfmp[id]==1) MP[id]=MP[id]-5
							if (onemp[id]==1) MP[id]=MP[id]-1
							if (onemp[id]==0) {
								if (halfmp[id]==0) MP[id]=MP[id]-10
							}
							if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
							poisonstrikeused[id]=1
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 10 MP needed")
			}
		}
		
		////////////////////////////////////////////////////////////SILENCE STRIKE/////////////////////////////////////////////////////////////////
		if (currentskill[id]==14) {
			if (MP[id]>=11) {
				if (poisonstrikeused[id]==0) {
					if (silencestrikeused[id]==0) {
						if (attack_reelsused[id]==0) {
							set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
							show_hudmessage(id, "NeXt BulLeT wIlL bE a SiLeNcEsTrIkE!")
							if (halfmp[id]==1) MP[id]=MP[id]-6
							if (onemp[id]==1) MP[id]=MP[id]-1
							if (onemp[id]==0) {
								if (halfmp[id]==0) MP[id]=MP[id]-11
							}
							if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
							silencestrikeused[id]=1
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 11 MP needed")
			}
		}
		
		//////////////////////////////////////////////////////////////TRIPLE FOUL//////////////////////////////////////////////////////////////////
		if (currentskill[id]==15) {
			if (MP[id]>=50) {
				if (poisonstrikeused[id]==0) {
					if (silencestrikeused[id]==0) {
						if (attack_reelsused[id]==0) {
							set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
							show_hudmessage(id, "NeXt BulLeT wIlL bE a TrIpLe FoUl!")
							if (halfmp[id]==1) MP[id]=MP[id]-25
							if (onemp[id]==1) MP[id]=MP[id]-1
							if (onemp[id]==0) {
								if (halfmp[id]==0) MP[id]=MP[id]-50
							}
							if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
							poisonstrikeused[id]=1
							silencestrikeused[id]=1
							darknessused[id]=1
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 50 MP needed")
			}
		}
		/////////////////////////////////////////////////////////////ATTACK REELS//////////////////////////////////////////////////////////////////
		if (currentskill[id]==16) {
			if (MP[id]>=70) {
				if (poisonstrikeused[id]==0) {
					if (silencestrikeused[id]==0) {
						if (attack_reelsused[id]==0) {
							set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
							show_hudmessage(id, "NeXt BulLeT wIlL bE aN aTtAcK rEeLs!")
							if (halfmp[id]==1) MP[id]=MP[id]-35
							if (onemp[id]==1) MP[id]=MP[id]-1
							if (onemp[id]==0) {
								if (halfmp[id]==0) MP[id]=MP[id]-70
							}
							if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
							attack_reelsused[id]=1
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 70 MP needed")
			}
		}
		//////////////////////////////////////////////////////////////DARKNESS/////////////////////////////////////////////////////////////////////
		if (currentskill[id]==17) {
			if (MP[id]>=15) {
				if (darknessused[id]==0) {
					if (darkness2used[id]==0) {
						if (deathused[id]==0) {
							set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
							show_hudmessage(id, "NeXt BulLeT wIlL bE a DaRkNeSs!")
							if (file_exists("sound/FFX/haste.WAV")) emit_sound(id, CHAN_STATIC, "FFX/haste.WAV", 1.0, ATTN_NORM, 0, PITCH_NORM)
							if (halfmp[id]==1) MP[id]=MP[id]-8
							if (onemp[id]==1) MP[id]=MP[id]-1
							if (onemp[id]==0) {
								if (halfmp[id]==0) MP[id]=MP[id]-15
							}
							darknessused[id]=1
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 15 MP needed")
			}
		}
		/////////////////////////////////////////////////////////////DARKNESS2/////////////////////////////////////////////////////////////////////
		if (currentskill[id]==18) {
			if (MP[id]>=24) {
				if (darknessused[id]==0) {
					if (darkness2used[id]==0) {
						if (deathused[id]==0) {
							set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
							show_hudmessage(id, "NeXt BulLeT wIlL bE a DaRkNeSs2!")
							if (file_exists("sound/FFX/haste.WAV")) emit_sound(id, CHAN_STATIC, "FFX/haste.WAV", 1.0, ATTN_NORM, 0, PITCH_NORM)
							if (halfmp[id]==1) MP[id]=MP[id]-12
							if (onemp[id]==1) MP[id]=MP[id]-1
							if (onemp[id]==0) {
								if (halfmp[id]==0) MP[id]=MP[id]-24
							}
							darkness2used[id]=1
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 24 MP needed")
			}
		}
		///////////////////////////////////////////////////////////////DEATH///////////////////////////////////////////////////////////////////////
		if (currentskill[id]==19) {
			if (MP[id]>=40) {
				if (darknessused[id]==0) {
					if (darkness2used[id]==0) {
						if (deathused[id]==0) {
							set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
							show_hudmessage(id, "NeXt BulLeT wIlL bE a DeAtH!")
							if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
							if (halfmp[id]==1) MP[id]=MP[id]-20
							if (onemp[id]==1) MP[id]=MP[id]-1
							if (onemp[id]==0) {
								if (halfmp[id]==0) MP[id]=MP[id]-40
							}
							deathused[id]=1
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 40 MP needed")
			}
		}
		///////////////////////////////////////////////////////////DARKNESS FURY///////////////////////////////////////////////////////////////////
		if (currentskill[id]==20) {
			if (MP[id]>=70) {
				if (darknessused[id]==0) {
					if (darkness2used[id]==0) {
						if (deathused[id]==0) {
							if (darkness_furyused[id]==0) {
								if (darkness2_furyused[id]==0) {
									set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
									show_hudmessage(id, "YoU aRe In A dArKnEsS fUrY!")
									if (file_exists("sound/FFX/ffx_slow.wav")) emit_sound(id, CHAN_STATIC, "FFX/ffx_slow.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
									if (halfmp[id]==1) MP[id]=MP[id]-35
									if (onemp[id]==1) MP[id]=MP[id]-1
									if (onemp[id]==0) {
										if (halfmp[id]==0) MP[id]=MP[id]-70
									}
									darkness_furyused[id]=1
								}
							}
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 70 MP needed")
			}
		}
		///////////////////////////////////////////////////////////DARKNESS2 FURY//////////////////////////////////////////////////////////////////
		if (currentskill[id]==21) {
			if (MP[id]>=100) {
				if (darknessused[id]==0) {
					if (darkness2used[id]==0) {
						if (deathused[id]==0) {
							if (darkness_furyused[id]==0) {
								if (darkness2_furyused[id]==0) {
									set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
									show_hudmessage(id, "YoU aRe In A dArKnEsS2 fUrY!")
									if (file_exists("sound/FFX/ffx_slow.wav")) emit_sound(id, CHAN_STATIC, "FFX/ffx_slow.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
									if (halfmp[id]==1) MP[id]=MP[id]-50
									if (onemp[id]==1) MP[id]=MP[id]-1
									if (onemp[id]==0) {
										if (halfmp[id]==0) MP[id]=MP[id]-100
									}
									darkness2_furyused[id]=1
								}
							}
						}
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 100 MP needed")
			}
		}
		/////////////////////////////////////////////////////////////////CURE//////////////////////////////////////////////////////////////////////
		if (currentskill[id]==22) {
			if (MP[id]>=7) {
				new HPcure
				if (halfmp[id]==1) MP[id]=MP[id]-4
				if (onemp[id]==1) MP[id]=MP[id]-1
				if (onemp[id]==0) {
					if (halfmp[id]==0) MP[id]=MP[id]-7
				}
				healing1(id)
				healingglow(id)
				HPcure=random_num(1*MAG[id],20*MAG[id])
				set_user_health( id,get_user_health( id) + HPcure)
			
				//set_user_health(id, get_user_health(id)+HPcure)
				if (get_user_health( id) >100.0 + HPbonus[id]) set_user_health( id, 100 + HPbonus[id] )
				set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
				show_hudmessage(id, "YoU cUrEd %d HiTpOiNtS!", HPcure)
				if (file_exists("sound/FFX/rd_shipshorn.wav")) emit_sound(id, CHAN_STATIC, "FFX/rd_shipshorn.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
				}else{
				set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
				show_hudmessage(id, "Not enough MP. 7 MP needed")
			}
		}
		/////////////////////////////////////////////////////////////////CURA//////////////////////////////////////////////////////////////////////
		if (currentskill[id]==23) {
			if (MP[id]>=10) {
				new HPcure
				if (halfmp[id]==1) MP[id]=MP[id]-5
				if (onemp[id]==1) MP[id]=MP[id]-1
				if (onemp[id]==0) {
					if (halfmp[id]==0) MP[id]=MP[id]-10
				}
				healing1(id)
				healingglow(id)
				HPcure=random_num(2*MAG[id],40*MAG[id])
				set_user_health( id,get_user_health( id) + HPcure)
				if (get_user_health( id) > 100.0 + HPbonus[id] ) set_user_health( id, 100 + HPbonus[id])
				set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
				show_hudmessage(id, "YoU cUrEd %d HiTpOiNtS!",  HPcure)
				if (file_exists("sound/FFX/rd_shipshorn.wav")) emit_sound(id, CHAN_STATIC, "FFX/rd_shipshorn.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 10 MP needed")
			}
		}
		////////////////////////////////////////////////////////////////CURAGA/////////////////////////////////////////////////////////////////////
		if (currentskill[id]==24) {
			if (MP[id]>=16) {
				new HPcure
				if (halfmp[id]==1) MP[id]=MP[id]-8
				if (onemp[id]==1) MP[id]=MP[id]-1
				if (onemp[id]==0) {
					if (halfmp[id]==0) MP[id]=MP[id]-16
				}
				healing1(id)
				healingglow(id)
				healstorm(id)
				HPcure=random_num(4*MAG[id],80*MAG[id])
				set_user_health( id,get_user_health( id) + HPcure)
				if (get_user_health( id) > 100.0 + HPbonus[id] ) set_user_health( id, 100 + HPbonus[id])
				set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
				show_hudmessage(id, "YoU cUrEd %d HiTpOiNtS!",  HPcure)
				if (file_exists("sound/FFX/rd_shipshorn.wav")) emit_sound(id, CHAN_STATIC, "FFX/rd_shipshorn.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 24 MP needed")
			}
		}
		////////////////////////////////////////////////////////////////PROTECT////////////////////////////////////////////////////////////////////
		if (currentskill[id]==25) {
			if (MP[id]>=20) {
				if (protectused[id]==0) {
					if (halfmp[id]==1) MP[id]=MP[id]-10
					if (onemp[id]==1) MP[id]=MP[id]-1
					if (onemp[id]==0) {
					if (halfmp[id]==0) MP[id]=MP[id]-20
					}
					DEF[id]=DEF[id]+10
					healing1(id)
					set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
					show_hudmessage(id, "YoU cAsT pRoTeCt On YoU!")
					if (file_exists("sound/FFX/ffx_slow.wav")) emit_sound(id, CHAN_STATIC, "FFX/ffx_slow.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
					protectused[id]=1
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 20 MP needed")
			}
		}
		////////////////////////////////////////////////////////////////PETRIFY////////////////////////////////////////////////////////////////////
		if (currentskill[id]==26) {
			if (MP[id]>=40) {
				if (petrifyused[id]==0) {
					if (halfmp[id]==1) MP[id]=MP[id]-20
					if (onemp[id]==1) MP[id]=MP[id]-1
					if (onemp[id]==0) {
					if (halfmp[id]==0) MP[id]=MP[id]-40
					}
					set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
					show_hudmessage(id, "NeXt BulLeT wIlL bE a PeTrIfY!")
					if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
					petrifyused[id]=1
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 40 MP needed")
			}
		}
		////////////////////////////////////////////////////////////////AUTO LIFE//////////////////////////////////////////////////////////////////
		if (currentskill[id]==27) {
			if (MP[id]>=99) {
				if (auto_lifeused[id]==0) {
					if (halfmp[id]==1) MP[id]=MP[id]-50
					if (onemp[id]==1) MP[id]=MP[id]-1
					if (onemp[id]==0) {
					if (halfmp[id]==0) MP[id]=MP[id]-99
					}
					set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
					show_hudmessage(id, "YoU cAsT aUtO lIfE oN yOu!")
					if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
					auto_lifeused[id]=1
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 99 MP needed")
			}
		}
		
		///////////////////////////////////////////////////////////////////STEAL///////////////////////////////////////////////////////////////////
		if (currentskill[id]==28) {
			if (cs_get_user_money(id)>=1000) {
				if (stealused[id]==0) {
					cs_set_user_money(id, cs_get_user_money(id))
					
					set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
					show_hudmessage(id, "NeXt BulLeT wIlL bE a StEaL!")
					if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
					stealused[id]=1
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough money. 1000 money needed")
			}
		}
		/////////////////////////////////////////////////////////////////PILFER GIL////////////////////////////////////////////////////////////////
		if (currentskill[id]==30) {
			if (MP[id]>=10) {
				if (pilfer_gilused[id]==0) {
						pilfer_gilused[id]=1
						set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
						show_hudmessage(id, "NeXt BulLeT wIlL bE a PiLfEr GiL!")
						if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
						if (halfmp[id]==1) MP[id]=MP[id]-5
						if (onemp[id]==1) MP[id]=MP[id]-1
						if (onemp[id]==0) {
						if (halfmp[id]==0) MP[id]=MP[id]-10
					}
				}
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 10 MP needed")
			}
		}
		////////////////////////////////////////////////////////////////SPARE CHANGE///////////////////////////////////////////////////////////////
		if (currentskill[id]==31) {
			if (cs_get_user_money(id)>=1000) {
				cs_set_user_money(id, cs_get_user_money(id)-1000)
				if (file_exists("sound/FFX/cast.wav")) emit_sound(id, CHAN_STATIC, "FFX/cast.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
				set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
				show_hudmessage(id, "NeXt BulLeT wIlL bE a SpArE cHaNgE!")
				deathused[id]=1
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough money. 1000 gil needed")
			}
		}
		if (currentskill[id]==33) {
			if (MP[id]>16) {
				if (file_exists("sound/FFX/speed.wav")) emit_sound(id, CHAN_STATIC, "FFX/speed.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
				smokestorm(id)
				MP[id]=MP[id] - 16
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 16 MP needed")
			}
		}
		if (currentskill[id]==34) {
			if (MP[id]>22) {
				if (file_exists("sound/FFX/speed.wav")) emit_sound(id, CHAN_STATIC, "FFX/speed.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
				smokestorm1(id)
				MP[id]=MP[id] - 22
			}else{
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,12.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "Not enough MP. 22 MP needed")
			}
		}
		///////////////////////////////////////////////////////////////////NO SKILL////////////////////////////////////////////////////////////////
		if (currentskill[id] == 35) spell_fire(id)
		if (currentskill[id] == 36) spell_fire(id)
		if (currentskill[id] == 37) spell_fire(id)
		if (currentskill[id] == 38) spell_firesh(id)
		if (currentskill[id] == 39) spell_ice(id)
		if (currentskill[id] == 40) spell_ice(id)
		if (currentskill[id] == 41) spell_ice(id)
		if (currentskill[id] == 42) spell_icesh(id)
		if (currentskill[id] == 43) spell_thunder(id)
		if (currentskill[id] == 44) spell_thunder(id)
		if (currentskill[id] == 45) spell_thunder(id)
		if (currentskill[id] == 46) spell_thundersh(id)
		if (currentskill[id] == 47)  return PLUGIN_CONTINUE //  spell_bio(id)
		if (currentskill[id] == 48)  return PLUGIN_CONTINUE //  spell_virus(id)
		if (currentskill[id] == 49)  return PLUGIN_CONTINUE //  spell_sewage(id)
		if (currentskill[id] == 50)  return PLUGIN_CONTINUE //  spell_epidemic(id)
		if (currentskill[id] == 51) spell_demi(id)
		if (currentskill[id] == 52) spell_ultima(id)
		if (currentskill[id] == 53) spell_nuke(id)
		if (currentskill[id] == 54) spell_meteo(id)
		if (currentskill[id]==0) {
			set_hudmessage(250,250,250,-1.0,0.35,1,6.0,10.0,0.1,0.5,HMCHAN_NEWROUND)
			show_hudmessage(id, "NO SKILL SELECTED")
			return PLUGIN_CONTINUE
		}
		usedid[id] = 1
		castings(id)
		//smokie3(id)
		firestorm(id)
		blueishdisk(id)
		//smokestorm(id)
	}
	return PLUGIN_CONTINUE
}

public spell_firesh(id) {
	if ( MP[id] > 30 ) {
		MP[id] = MP[id] - 30
		fireshield_used[id] = 1
		if (file_exists("sound/ffx/ffx_deathsk_fx.wav")) emit_sound(id, CHAN_STATIC, "ffx/ffx_deathsk_fx.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
		redglowfx(id)
		attachtoplayer(id, redrng, -30)
		set_user_armor(id, get_user_armor(id) + 100)
	}
}

public spell_icesh(id) {
	if ( MP[id] > 30 ) {
		MP[id] = MP[id] - 30
		iceshield_used[id] = 1
		if (file_exists("sound/ffx/ffx_deathsk_fx.wav")) emit_sound(id, CHAN_STATIC, "ffx/ffx_deathsk_fx.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
		bluglowfx(id)
		attachtoplayer(id, healsprite, -30)
		set_user_armor(id, get_user_armor(id) + 100)
	}
}

public spell_thundersh(id) {
	if ( MP[id] > 30 ) {
		thshield_used[id] = 1
		MP[id] = MP[id] - 30
		if (file_exists("sound/ffx/ffx_deathsk_fx.wav")) emit_sound(id, CHAN_STATIC, "ffx/ffx_deathsk_fx.wav", 0.8, ATTN_NORM, 0, PITCH_NORM)
		prpglowfx(id)
		attachtoplayer(id, blurng, -30)
		set_user_armor(id, get_user_armor(id) + 100)
	}
}

public spell_demi(id) {
	if ( MP[id] > 30 ) {
		MP[id] = MP[id] - 30
		demi_used[id] = 1
	}
}
	
public spell_ultima(id) {
	if ( MP[id] > 30 ) {
		MP[id] = MP[id] - 30
		ultima_used[id] = 1
	}
}


		
public spell_fire(id) {
	if (currentskill[id] == 35) {
		if ( MP[id] > 30 ) {
			MP[id] = MP[id] - 30
			fireused[id][0] = 1
			return PLUGIN_CONTINUE
		}		
	}			
	
	if (currentskill[id] == 36) {
		if ( MP[id] > 60 ) {
			fireused[id][1] = 1
			MP[id] = MP[id] - 60
			return PLUGIN_CONTINUE
		}
	}
	
	if (currentskill[id] == 37) {
		if ( MP[id] > 90 ) {
			MP[id] = MP[id] - 90
			fireused[id][2] = 1
			return PLUGIN_CONTINUE
		}
	}
	return PLUGIN_CONTINUE
	
}

public spell_ice(id) {
	if (currentskill[id] == 39) {
		if ( MP[id] > 30 ) {
			MP[id] = MP[id] - 30
			iceused[id][0] = 1
			return PLUGIN_CONTINUE
		}		
	}			
	
	if (currentskill[id] == 40) {
		if ( MP[id] > 60 ) {
			MP[id] = MP[id] - 60
			iceused[id][1] = 1
			return PLUGIN_CONTINUE
		}
	}
	
	if (currentskill[id] == 41) {
		if ( MP[id] > 90 ) {
			MP[id] = MP[id] - 90
			iceused[id][2] = 1
			return PLUGIN_CONTINUE
		}
	}
	return PLUGIN_CONTINUE
}

public spell_thunder(id) {
	if (currentskill[id] == 43) {
		if ( MP[id] > 30 ) {
			MP[id] = MP[id] - 30
			thunderused[id][0] = 1
			return PLUGIN_CONTINUE
		}		
	}			
	
	if (currentskill[id] == 44) {
		if ( MP[id] > 60 ) {
			MP[id] = MP[id] - 60
			thunderused[id][1] = 1
			return PLUGIN_CONTINUE
		}
	}
	
	if (currentskill[id] == 45) {
		if ( MP[id] > 90 ) {
			MP[id] = MP[id] - 90
			thunderused[id][2] = 1
			return PLUGIN_CONTINUE
		}
	}
	return PLUGIN_CONTINUE
}
public spell_meteo(id) {
		if ( MP[id] > 60 ) {
		MP[id] = MP[id] - 60
		meteoused[id] = 1
		return PLUGIN_CONTINUE
	}
	return PLUGIN_CONTINUE
}
public spell_nuke(id) {
	if ( MP[id] > 60 ) {
		MP[id] = MP[id] - 60
		nukeused[id] = 1
		return PLUGIN_CONTINUE
	}
	return PLUGIN_CONTINUE
}
public zoom_skill(id, number) {
	if (number == 90) zoomed[id]=0
	if (number == 60) zoomed[id]=1
	if (number == 40) zoomed[id]=2
	if (number == 20) zoomed[id]=3
	if (number == 1) zoomed[id]=4
	message_begin(MSG_ONE, gmsgSetFOV, {0,0,0}, id)
	write_byte(number)
	message_end()
	return PLUGIN_CONTINUE
} 

public zoomkeyhandle(id) {
	new clip,temp
	new wpn_id = get_user_weapon(id,clip,temp) 
	if (scopeid[id][1] == 1) {
		if ( wpn_id==CSW_P228 || wpn_id==CSW_ELITE || wpn_id==CSW_USP || wpn_id==CSW_FIVESEVEN || wpn_id==CSW_GLOCK18 || wpn_id==CSW_DEAGLE ) {
			if (zoomed[id]==0) {
				zoom_skill(id, 60)
				zoomed[id]=1
			}
			if (zoomed[id]==1) {
				 zoom_skill(id, 40)
				 zoomed[id]=2
			}
			if (zoomed[id]==2) {
				zoom_skill(id, 90)
				zoomed[id]=0
			}
		}
	}
	if (scopeid[id][2] == 1) {
		if ( wpn_id==CSW_FAMAS || wpn_id==CSW_GALIL || wpn_id==CSW_GALI || wpn_id==CSW_AK47 || wpn_id==CSW_M4A1 || wpn_id==CSW_M249 ) {
			if (zoomed[id]==0) {
				zoom_skill(id, 60)
				zoomed[id]=1
			}
			if (zoomed[id]==1) {
				 zoom_skill(id, 40)
				 zoomed[id]=2
			}
			if (zoomed[id]==2) {
				zoom_skill(id, 90)
				zoomed[id]=0
			}
		}
	}
		
	if (scopeid[id][3] == 1) {
		if ( wpn_id==CSW_MAC10 || wpn_id==CSW_P90 || wpn_id==CSW_TMP || wpn_id==CSW_MP5NAVY || wpn_id==CSW_UMP45  ) {
			if (zoomed[id]==0) {
				zoom_skill(id, 60)
				zoomed[id]=1
			}
			if (zoomed[id]==1) {
				 zoom_skill(id, 40)
				 zoomed[id]=2
			}
			if (zoomed[id]==2) {
				zoom_skill(id, 90)
				zoomed[id]=0
			}
		}
	}
		
	if (scopeid[id][4] == 1) {
		if ( wpn_id==CSW_AWP || wpn_id==CSW_AUG || wpn_id==CSW_SCOUT || wpn_id==CSW_G3SG1 || wpn_id==CSW_SG552 || CSW_SG550 ) {
			if (zoomed[id]==0) {
				zoom_skill(id, 60)
				zoomed[id]=1
			}
			if (zoomed[id]==1) {
				 zoom_skill(id, 40)
				 zoomed[id]=2
			}
			if (zoomed[id]==3) {
				zoom_skill(id, 20)
				zoomed[id]=1
			}
			if (zoomed[id]==3) {
				zoom_skill(id, 90)
				zoomed[id]=0
			}
		}
	}
	return PLUGIN_HANDLED
}
