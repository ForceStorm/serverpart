/*					THIS IS A TEST VERSION !!!!!!!!!!!!!!!
							FINAL FANTASY 10
							      v0.85
							      
							      v.051
							   Originally
						  	  By Z4KN4F31N
					MOdified and Ported to Amxx By Timmi
					Changed Saving and Loading.
					THIS IS A TEST COPY!!!!!!!!!!!!!!!!!!!!!
*/

//***************************************************************Defines*****************************************************************
#define HMCHAN_NEWROUND 503
#define HMCHAN_PLAYERINFO 1089
#define HMCHAN_SERVERANNOUNCE 1090
#define HMCHAN_NOTICE 504
#define HMCHAN_GENERIC 505
#define HMCHAN_GENERAL 506
#define HMCHAN_ARMOR 507
#define CELESTIALXP 8
#define COUNTERXP 10
#define DEATHSTRIKEXP 10
#define QUICKHITXP 10
#define DEATHXP 10
#define ATTACKREELSXP 10
#define SHOOTINGSTARXP 10
#define TEAM_T 1
#define TEAM_CT 2
#define TORNADOXP 10
#define BLITZACEXP 10
#define SPIRALCUTXP 10
#define CRITHITXP 15
#define NORMXP 20
#define MAX_NAME_LENGTH 31
#define MAX_VAR_LENGTH 64 
#define STATS_KILLS 0
#define STATS_DEATHS 1
#define STATS_HS 2
#define TE_DLIGHT 27		// dynamic light, effect world, minor entity effect
#define TE_BLOOD 103		// blood spray "crappy"
#define TE_GLOWSPRITE 23    // like flares
#define TE_LARGEFUNNEL 100  // makes a primitive particle tunnel
#define TE_BEAMENTPOINT 1   // beam
#define TE_KILLBEAM 99      // kills the beam :?
#define BEAMLIFE 100	    // life
#define TE_EXPLOSION 3      // Explode o.O

new enttempchange[512] = 0
new followid[33] = 0
new buying[33]
new sold1[33]
new Float:e_health[512]
new savenum[33] = 0
new Float:anglescomplex[33]
new ctentid
new tentid


//*************************************************************Sprites Names*************************************************************
new firespr2, firespr3, firesprite, healsprite, blurng, prprng, redrng, deathsprite, smoke2, smoke, smoke3, smoke4, poisonsprite, shockwave, criticalsprite

//*************************************************************WEAPON SKILLS***************************************************************
new sensor[33], celestial[33], gillionaire[33], darknessitm[33], silenceitm[33], poisonitm[33], halfmp[33]
new magiccounter[33], slow[33], deathitm[33], petrifyitm[33], onemp[33], counterattack[33]

//*****************************************************************SKILLS******************************************************************
new smokeskill[33], smokeskill2[33]
new spiral_cut[33], blitz_ace[33], flee2[33], flee[33], hastega[33], haste[33], ribbon[33], regenitm[33]
new darkness2[33], darkness[33], petrify[33], protect[33], curaga[33], cura[33], cure[33], auto_life[33]
new magic_break[33], armor_break[33], power_break[33], darkness2_fury[33], darkness_fury[33], death[33]
new attack_reels[33], darkmatterused[33], triple_foul[33], silencestrike[33], poisonstrike[33], mug[33]
new quick_hit[33], spare_change[33], pilfer_gil[33], steal[33], shooting_star[33], tornado[33], full_break[33]

//*************************************************************SKILL FUNCTIONS*************************************************************
new spiral_cutused[33], poisonstrikeused[33], silencestrikeused[33], attack_reelsused[33], protectused[33], pilfer_gilused[33]
new armor_breakused[33], power_breakused[33], usingskill[33], hastegaused[33], hasteused[33], blitz_aceused[33]
new darkness2_furyused[33], stealused[33], darkness_furyused[33], deathused[33], darkness2used[33], darknessused[33]
new petrifyused[33], auto_lifeused[33], tornadoused[33], shooting_starused[33], full_breakused[33], magic_breakused[33]

//*************************************************************LEVELS/STATS****************************************************************
new STR[33], DEF[33], MAG[33], AGI[33], STR2[33], DEF2[33], DEF3[33], MAG2[33], AGI2[33], MP[33], MPmax[33], MPmax2[33]
new BANKAC[33], i_level[33], FFplayer[33], XP[33], currentskill[33], skillpoints[33], HPbonus2[33], HPbonus[33],STRbonus[33]

//*********************************************Portion of code is from warcraft3***********************************************************
new hostagesaver, bombCarrier, bombdefuser
new vip_escape[33], hostage_saver[33], diffuser_1id[33], planter_id[33], defuser_id[33], deathmessageshown[33]
new g_vipID = 0
new bool:hasdefuse[33]

//*************************************************************ENTITY/NPC******************************************************************
new NPCTERROR, RUG, NPCCT, RUG1
new NPCTexists=0
new NPCCTexists=0
new Float:ent_position[3]
new Float:hostageorg[3]
new Float:ent2_position[3]
new blessed[33], gil[33], getribbon[33], szentity2[33], szentity1[33], szentity[33]
new kills[33]
new Float:f_distance[33]
new modelnumct[4][]={"sas","gsg9","urban","gign"}
new modelnumt[4][]={"arctic","leet","guerilla","terror"}
new questitem1[33], questitem2[33], questcounter[33], iquestused[33], questused[33], iquest[33], quest[33]

//****************************************************************MISC*********************************************************************
new bool:game_commence=false
new petrified[33], poisoner[33], slowed[33], unbless[33], hostammount[20], spawnonce[33]
new id_attacker, generalrandom, darkness_event, life_event, FreezeEnded, critical, hostint
new posupright[33], posupcenter[33], posupleft[33], posdownright[33], posdowncenter[33],posdownleft[33], tornadolastround[33]
new g_menuuse[33] = 0
new g_allowplchange = 1
new loaded[33] = 0

//************************************************Weapon Related**********************************
new weaponstring[81][] = {
"None", "Arc Sword(Half MP Cost)", "Avenger(Counter-Attack)", "Astral Sword(One MP Cost)", "Basilisk Steel(Stonestrike)", "Dance Macabre(Deathstrike)",
"Deathbringer(Deathstrike)", "Gravestone(Stonestrike)", "Largamente(Slowstrike)", "Vendetta(Counter-Attack, Magic-Counter)", "Caladbolg(Celestial Weapon)", 
"Astral Rod(One MP Cost)", "Bizarre Staff(Poisonstrike)", "Bleadonna Wand(Poisontouch)", "Chaos Rod(Silencestrike, Slowstrike, Darkstrike, Poisonstrike)",
"Death Wand(Deathtouch)", "El Dorado(Gillionaire)", "Impasse(Slowstrike)", "Magistral Rod(Half MP Cost)", "Nirvana(Celestial Weapon)", "Prism Rod(Magic Counter)",
"Punisher(Deathstrike)", "Reticent Staff(Silencestrike)", "Attack Mog(Str+1)", "Blinding Cait Sith(Darktouch)", "Abaddon Cait Sith(Silencestrike, Slowstrike, Poisonstrike)",
"Buster Mog(Str+1)", "Cactuar Wizard(Half MP Cost)", "Cactuar Scope(Sensor)", "Fossil Cait Sith(Stonetouch)", "Magical Cactuar(One MP Cost)", "Moomba Warrior(Str+3)", 
"Mute Cait Sith(Silencestrike)", "Noxious Cait Sith(Poisontouch)", "Power Mog(Str+5)", "Space Bandit(Gillionaire)", "Wicked Cait Sith(Deathstrike)",
"Toxic Cait Sith(Poisonstrike)", "Onion Knight(Celestial Weapon)", "Buster Claw(Str+1)", "Buster Glove(Str+1)", "Buster Knuckles(Str+3)", "Clock Hand(Slowtouch)",
"Colossus(Stonestrike)", "Godhand(Celestial Weapon)", "Eye Poker(Darktouch)", "Executioner(Deathstrike)", "Infinity(One MP Cost)", "Iron Claw(Str+5)", 
"Jammer(Darkstrike)", "Mage Husher(Silencestrike)", "Manticore Claw(Poisonstrike)", "Stickyfingers(Gillionaire)", "Spartan(Str+6)", "Tit-for-Tat(Counter-Attack)",
"Venomous Blade(Poisonstrike)", "Stunner(Slowstrike)", "Prism Blade(Counter-Magic)", "Nodachi(Str+3)", "Masamune(Celestial Weapon)", "Inducer(Half MP Cost)", 
"Hunter's Blade(Sensor)", "Gilmonger(Gillionaire)", "Dark Blade(Darkness)", "Blockade(Slowstrike)", "Assassin Blade(Deathstrike)",
"Ace Striker(Str+6)", "Blackout(Darkstrike)", "Blind Pass(Darktouch)", "Delay of Game(Slowtouch)", "Dream Team(Sleeptouch)", "Halftime(Half MP Cost)",
"Hyper Ball(Str+1)", "Mirage Ball(Magic Counter)", "Overtime(One MP Cost)", "Rematch(Counter-Attack)", "Prism Ball(Magic Counter)", "Sudden Death(Deathstrike)",
"Stone Cold(Stonestrike)", "Violation(Poisonstrike)", "World Champion(Celestial Weapon)"
}

//************************************************Armor related************************************************
new armorstring[21][] = {
"None", "Sanctuary(Ribbon)", "Seeker's Shield(HP+10)", "Protect Shield(Auto-Protect)", "Solidity(Ribbon)", "Warrior's Bracer(HP+20)", "Sorcery Bracer(MP+20)",
"Shutout(Ribbon)", "Silver Armguard(Def+2)", "Soldier's Armguard(HP+20)", "Eternity(Ribbon)", "Adept's Bangle(HP+40, MP+20)", "Metal Bangle(Def+1)",
"Holy Ring(Ribbon)", "Assault Ring(Auto-Protect, Auto-Regen)", "Protect Ring(Auto-Protect)", "Impervious(Ribbon)", "Knight's Targe(HP+50)", "Seeker's Targe(HP+10)", "Crystal Armor", "Transparent"
}
//************************************************Item Related************************************************
new itemstring[24][] = {
"None", "Potion", "Hi-Potion", "X-Potion", "Ether", "Turbo-Ether", "Power Sphere", "Agility Sphere", "Dark Matter", "Chocobo Feather", "Elixir", "Antidote", "Remedy", 
"Mana Tablet", "Magic Sphere", "Invis Potion", "Weight LOSS", "Disguise", "Defense Sphere", "Ability Sphere", "Hostage Teleporter", "X-invis Potion", "Home Teleporter", "Enemy Teleporter"
}

new ringstring[8][] = {
"None", "HP+50", "XP*2", "MP+35", "STR+3", "STR+6", "AGI+3", "DEF+3"
}
new ringmag[33], ringnum[33], itemnum[33], weaponnum[33], armornum[33]
new defchecked[33]
new options[33] = 13
new Float:origincomplex[33]
new origincomplexrev[33] = 0