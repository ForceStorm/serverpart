bool:fm_db_connect(&Sql:sql) {
	const LEN = 128
	new host[LEN], user[LEN], pass[LEN], db[LEN], error_msg[LEN]

	get_cvar_string(CVAR_HOST, host, LEN - 1)
	get_cvar_string(CVAR_USER, user, LEN - 1)
	get_cvar_string(CVAR_PASS, pass, LEN - 1)
	get_cvar_string(CVAR_DB, db, LEN - 1)

	sql = dbi_connect(host, user, pass, db, error_msg, LEN - 1)
	//server_print("Passed this to dbi_connect(%s, %s, %s, %s, %d) sql handle result: %d", host, user, pass, db, LEN - 1, sql)
	//if (dbi_check_error(sql)) {
	if (sql < SQL_OK) {
		dbi_error(sql, error_msg, LEN)
		log_amx("[%s] ERROR - Can't connect to SQL db: %s", PLUGINNAME, error_msg)

		return false
	}
	//log_amx("[%s] Successfully connected to SQL db.", PLUGINNAME)
	//server_print("[%s] Successfully connected to SQL db.", PLUGINNAME)

	return true
}
