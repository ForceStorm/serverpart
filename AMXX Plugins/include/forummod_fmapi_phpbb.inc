// phpbb
/*
For phpBB forums, the easiest way would be to first install EasyMod, a third party mod for phpBB which lets you install "EMC" compliant forum mods very easily.
Find EasyMod here: http://area51.phpbb.com/phpBB22/viewtopic.php?sid=&f=17&t=15391
After that you need the installation to install the mod used in conjunction with this plugin. It is to be found at this plugin's topic URL, mentioned at the top of this file.
Note that the mod I wrote probably really isn't EMC compliant, because at the moment it's still unclear what a EMC compliant mod is. However I've made sure this installs fine
when developing it, but as it's possible that other mods you have installed might interfere with this you could run into troubles. Ask in this plugin's topic if you run into troubles
at this point and maybe I can help you out.
*/

// Change these defines (tables, fields) if they are different.
#define g_TABLE_USERS				"phpbb_users"
#define g_FIELD_USERS_NAME			"username"
#define g_FIELD_USERS_USERID		"user_id"
#define g_FIELD_USERS_STEAMID		"user_sig" // user_sig user_steamid
#define g_TABLE_GROUPS				"phpbb_groups"
#define g_FIELD_GROUPS_ID			"group_id"
#define g_FIELD_GROUPS_TITLE		"group_name"
#define MAXGROUPNAMESIZE			40 // This is the maximum number of chars allowed in the name of a forum group.

public bool:FMAPI_GetReggedInformation(const STEAMID[], const bool:LOOKFORGROUPS, &forum_account_id, forumname[32], groups[], const MAX_GROUPS) {
	// Each FtPplugin implements this part
	// Paramaters: byref, set the data to be returned back to the FM plugin...
	// $return: bool, regged or not

	new Sql:sql
	if (!fm_db_connect(sql)) {
		return false
	}

	const SIZE = 512
	new query[SIZE + 1]

	format(query, SIZE, "SELECT `%s`, `%s` FROM `%s` WHERE `%s` LIKE ^"%%%s%%^" LIMIT 1", g_FIELD_USERS_NAME, g_FIELD_USERS_USERID, g_TABLE_USERS, g_FIELD_USERS_STEAMID, STEAMID)
	server_print("query: %s", query)

	//server_print("[%s] Query: ^"%s^"", PLUGINNAME, query)
	new Result:result = dbi_query(sql, query)

	if (result < RESULT_NONE) {
		new sqlerror[256]
		dbi_error(sql, sqlerror, 255)
		log_amx("Error: %s", sqlerror)
		dbi_close(sql)
		return false
	}
	else if (result == RESULT_NONE) {
		dbi_close(sql)
		return false
	}
	// else...

	if (!dbi_nextrow(result)) {
		//log_amx("%s just doesn't exist in forum database (dbi_nextrow() failed).", authid)
		//server_print("[%s] %s just doesn't exist in phpBB database (dbi_nextrow() failed) Number of rows: %d", PLUGINNAME, authid, dbi_num_rows(result))
		dbi_free_result(result)
		dbi_close(sql)
		return false // should have been a result, but nextrow somehow failed (because the set succeeded, but is empty?)
	}

	// Get name
	dbi_field(result, 1, forumname, 31)

	// Get userid
	new celldata[64]
	dbi_field(result, 2, celldata, 63)
	forum_account_id = str_to_num(celldata)

	dbi_free_result(result)

	// Get groups
	groups[0] = 0

	// Say goodbye if we shouldn't look for groups...
	if (!LOOKFORGROUPS) {
		dbi_close(sql)
		return true
	}

	// PHPBB has its own reference table of the sort many<->many (groups<->users)
	format(query, SIZE, "SELECT `group_id` FROM `phpbb_user_group` WHERE `user_id` = %d && `user_pending` = 0", forum_account_id)

	result = dbi_query(sql, query)
	if (result < RESULT_NONE) {
		new error[256]
		dbi_error(sql, error, 255)
		log_amx("[%s] ERROR - while quering SQL server for %s's group memberships: %s", PLUGINNAME, forumname, error)
		dbi_close(sql)
		return true
	}
	else if (result == RESULT_NONE) {
		// No memberships at all... which should be odd?
		log_amx("[%s] ERROR - while quering SQL server for %s's group memberships! Membership count was ZERO while at least a few was expected...", PLUGINNAME, forumname)
		dbi_close(sql)
		return true
	}
	// else...

	new i = 0
	while (i < MAXGROUPS && dbi_nextrow(result))
		groups[i++] = dbi_field(result, 1)

	dbi_free_result(result)

	dbi_close(sql)

	return true
}

#if defined ALLOWREADINGMAIL
// Never change these mail specific defines/variables:
#define FORMAT_OF_SUBJECT_LINE	"\y%d\w) %s^t%s^t\dfrom\w %s\R%s^n" // message number + 1, mailtype, subject, fromusername, datestring (you can remove mailtype (the first %s^t sequence) if it isn't available)
#define SUBJECTS_PER_PAGE		8
#define SUBJECTSIZE				15 // maximum number of characters allowed for subject, make string one size bigger to contain right number of chars.
#define SENDERSIZE				15 // maximum number of characters allowed for sender, make string one size bigger to contain right number of chars.
#define DATESIZE				63 // maximum number of characters allowed for date, make string one size bigger to contain right number of chars.

#define PRIVMSGS_READ_MAIL		0
#define PRIVMSGS_NEW_MAIL		1
#define PRIVMSGS_SENT_MAIL		2
#define PRIVMSGS_SAVED_IN_MAIL	3
#define PRIVMSGS_SAVED_OUT_MAIL	4
#define PRIVMSGS_UNREAD_MAIL	5

new g_mailpage[33]
new g_msgids[33][SUBJECTS_PER_PAGE]
// Never change above

// The defines below you should explicitly correct if they are wrong...
// Correct these table/field names if they are different/not used etc... you can add new defines here as well as needed. However you should probably not remove any of these lines because
// they are also used internally by Forum Mod.
#define g_TABLE_MSGS_SUBJ			"phpbb_privmsgs"
#define g_FIELD_MSGS_SUBJECT_ID		"privmsgs_id"
#define g_FIELD_MSGS_SUBJECT_SUBJ	"privmsgs_subject"
#define g_FIELD_MSGS_SUBJECT_DATE	"privmsgs_date"
#define g_FIELD_MSGS_SUBJECT_TO		"privmsgs_to_userid"
#define g_FIELD_MSGS_SUBJECT_FROM	"privmsgs_from_userid"
#define g_TABLE_MSGS_TEXT			"phpbb_privmsgs_text"
#define g_FIELD_MSGS_TEXT_ID		"privmsgs_text_id"
#define g_FIELD_MSGS_TEXT_TEXT		"privmsgs_text"

// This should be the order of the fields in the select query, starting with 1 for the leftmost.
#define FIELD_SUBJECT		1
#define FIELD_FROMUSERID	2
#define FIELD_DATE			3
#define FIELD_MSGID			4
#define FIELD_MSGTYPE		5

#define NROFMAILTYPES 6

new g_MAILTYPES[NROFMAILTYPES][20] = {
	"READ",
	"\yNEW\w",
	"SENT",
	"SAVEDINMAIL",
	"SAVEDOUTMAIL",
	"\yUNREAD\w"
}
// These will be copied into g_MAILTYPES if not running cstrike:
new const g_MAILTYPES_NOCOLOURS[NROFMAILTYPES][] = {
	"READ",
	"NEW",
	"SENT",
	"SAVEDINMAIL",
	"SAVEDOUTMAIL",
	"UNREAD"
}

// This function should create a "menubody" and also return the proper "menuflags", as well as set the number of "rows" returned by the query.
// The menubody really is the list of subjects you get when you run the cmd "amx_readmail". Because of the likely different database schemes used by different forums
// this will very likely need to be rewritten for each forum. That's why this is here. :-)
// The general working of this function should be:
// 1) Connect to db.
// 2) Get all subjects for the specified forum user account id.
// 3) For each subject, fill in button to press, subject text, sender name and that stuff. The exact format for this is specified in the define FORMAT_OF_SUBJECT_LINE above.
// 4) Don't forget to free each result :-P
// 5) Disconnect
bool:FMAPI_GetMailSubjects(const ID, const FORUM_ACCOUNT_ID, menubody[], const MENUBODYSIZE, &menuflags, messageids[SUBJECTS_PER_PAGE], &rows) {
	// Each FtPplugin implements this part
	// Paramaters: byref, set the data to be returned back to the FM plugin...
	// Don't try to set the const parameters, these are info for your use. Be careful to not override limits like sizes.
	// $return: complete success, or not

	new Sql:sql
	if (!fm_db_connect(sql)) {
		return false
	}

	const THEQUERYSIZE = 1023
	new query[THEQUERYSIZE + 1]

	format(query, THEQUERYSIZE, "\
	SELECT `%s`, `%s`, `%s`, `%s`, `privmsgs_type` \
	FROM %s, %s \
	WHERE `%s` = ^"%d^" && %s.%s = %s.%s && `privmsgs_type` != %d \
	ORDER BY `%s` DESC \
	LIMIT %d, %d",
	g_FIELD_MSGS_SUBJECT_SUBJ, g_FIELD_USERS_NAME, g_FIELD_MSGS_SUBJECT_DATE, g_FIELD_MSGS_SUBJECT_ID,
	g_TABLE_MSGS_SUBJ, g_TABLE_USERS,
	g_FIELD_MSGS_SUBJECT_TO, FORUM_ACCOUNT_ID, g_TABLE_MSGS_SUBJ, g_FIELD_MSGS_SUBJECT_FROM, g_TABLE_USERS, g_FIELD_USERS_USERID, PRIVMSGS_SENT_MAIL,
	g_FIELD_MSGS_SUBJECT_DATE,
	(g_mailpage[ID] - 1) * SUBJECTS_PER_PAGE, SUBJECTS_PER_PAGE + 1)
	// n = (n-1)*SUBJECTS_PER_PAGE, SUBJECTS_PER_PAGE + 1 (get one more item than we can display so we know if should insert a selection to go to next page...

	new Result:result = dbi_query(sql, query)

	if (result < RESULT_NONE) {
		new sqlerror[256]
		dbi_error(sql, sqlerror, 255)
		log_amx("Error: %s", sqlerror)
		dbi_close(sql)
		return false
	}
	else if (result == RESULT_NONE) {
		dbi_close(sql)
		server_print("FMAPI_GetMailSubjects() fails because result == RESULT_NONE")
		return false
	}
	// else...

	// These are made with set sizes. Don't change the sizes! You should create them + 1 though.
	new subject[SUBJECTSIZE + 1], datestring[DATESIZE + 1], fromusername[SENDERSIZE + 1]

	const CELLDATASIZE = 127
	new menubodylen = 0, celldata[CELLDATASIZE + 1]
	rows = dbi_num_rows(result)
	new mailtype

	for (new msg = 0; msg < SUBJECTS_PER_PAGE && dbi_nextrow(result); ++msg) {
		// Get msg type
		dbi_field(result, FIELD_MSGTYPE, celldata, CELLDATASIZE)
		mailtype = str_to_num(celldata)

		// Get subject
		dbi_field(result, FIELD_SUBJECT, subject, SUBJECTSIZE)

		// Retrieve sender
		dbi_field(result, FIELD_FROMUSERID, fromusername, SENDERSIZE)

		// Get date and format it
		dbi_field(result, FIELD_DATE, celldata, CELLDATASIZE)
		format_time(datestring, DATESIZE, "%m%d - %H:%M", str_to_num(celldata))

		// Get msg id
		dbi_field(result, FIELD_MSGID, celldata, CELLDATASIZE)
		messageids[msg] = str_to_num(celldata)

		// Add to menu
		menubodylen += format(menubody[menubodylen], MENUBODYSIZE - menubodylen, FORMAT_OF_SUBJECT_LINE, msg + 1, g_MAILTYPES[mailtype], subject, fromusername, datestring)

		menuflags |= (1<<(msg)) // (1<<0) would be 1.
	}

	dbi_free_result(result)
	dbi_close(sql)

	return true
}
#endif // defined ALLOWREADINGMAIL


