#define LC_UNKNOWNID -1
#define LC_NONEID 0
#define LC_MAX_LEVEL 20
#define TID_CHECK_LOGIN 10000
#define TID_MINUTE 20000
#define TID_CHECK_NOTICE 20001
#define TID_CORNER 10100
#define TID_AUTO_LOGIN 10200
#define TID_CHANGEMAP 10300
#define TID_BINDKEY_A 10400
#define TID_BINDKEY_B 10500

//这个是显示文字的函数,其中, mode = 1 表示在左方显示绿色文字,2表示在聊天区域显示彩色文字.
//3表示在屏幕中间显示文字. index 是玩家的id. message 是要显示的文字.
native lc_NoticeLeft(index, message[],mode = 1)

//返回玩家的 注册ID,返回 0 表示非注册,返回 大于1 表示注册的 ID.
native lc_IDGet(index)

//返回玩家的等级.
native lc_LevelGet(index)

//返回玩家的经验值
native lc_ExpGet(index)

//设置玩家的经验值
native lc_ExpSet(index, exp)

//返回玩家的点数
native lc_PointGet(index)

//设置玩家的点数
native lc_PointSet(index, point)

//计算玩家的等级,一般不用到
native lc_LevelCount(index)
