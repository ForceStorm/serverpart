new lang[33], check_lang_set[3]

stock lang_register(plugin[]) server_cmd("amx_multilang ^"%s^"", plugin)

stock lang_set(id)
{
	get_user_info(id, "_language", check_lang_set, 2)
	lang[id] = str_to_num(check_lang_set)
}

stock lang_geoip(id)
{
	new ip[32], code[3]
	get_user_info(id, "_language", check_lang_set, 2)
	lang[id] = strtonum(check_lang_set)

	get_user_ip(id, ip, 31)
	if(equal(check_lang_set, "") && geoip_code2(ip, code))
	{
		if(equal(code, "US") || equal(code, "GB")) client_cmd(id, "setinfo _language 0")
		else if(equal(code, "FR")) client_cmd(id, "setinfo _language 1")
		else if(equal(code, "DE")) client_cmd(id, "setinfo _language 2")
	}
}