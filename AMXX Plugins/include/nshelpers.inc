/********************************************************************************************************************************************\
** AssKicR's NShelpers - Created by AssKicR (Based on info from ns2amx by Mahnsawce)                                                        **
**                                                                                                                                          **
** Revision v1.0:                                                                                                                           **
** ---------------------------------------------------------------------------------------------------------------------------------------- **
**   Initial Release                                                                                                                        **
**                                                                                                                                          **
** Added commands:                                                                                                                          **
** ---------------------------------------------------------------------------------------------------------------------------------------- **
**    ns_getammo(id,Weapon)                                          Get the ammo of a weapon (Marines Only)                                **
**    ns_setammo(id,Weapon,Value)                                    Set the ammo of a weapon (Marines Only)                                **
**    ns_getres(id)                                                  Get resources of a player (Alien Only)                                 **
**    ns_setres(id,value)                                            Set resources of a player (Alien Only)                                 **
**    ns_getjpfuel(id)                                               Get the jetpack fuel of a player  (Marines Only)                       **
**    ns_setjpfuel(id,value)                                         Set the jetpack fuel of a player  (Marines Only)                       **
**    ns_getenergy                                                   Get energy of a player (Alien Only)                                    **
**    ns_setenergy                                                   Set energy of a player (Alien Only)                                    **
**    ns_getteam(id)                                                 Get the team of a player                                               **
**    ns_getclass(id)                                                Get the Class of a player                                              **
**    ns_getmask(id,mask)                                            Get special mask.                                                      **
**    ns_setmask(id,mask,onoff)                                      Set special mask. (1=ON / 0=OFF)                                       **
**    ns_getbuild(svClassname[],iBuiltOnly,iNumber)                  Get number of structures.. (set iBuiltOnly greater than 0 to find just **
**                                                                   completed and set iNumber to find specific one)                        **
**    ns_popup(id,svMessage[])                                       Show a message to a person (Max 189 letters)                           **
**    ns_hasweapon(id,weapon,setweapon=-1)                           Check if a person has a specified weapon..                             **
**    ns_getxp(id)                                                   Get a players experience                                               **
**    ns_setxp(id,xp)                                                Set a players experience                                               **
**    ns_getlevel                                                    Returns a players level in combat 1-10                                 **
**    ns_get_levels_spent(id)                                        Returns the number of levels spent in Combat 1-10                      **
**    ns_set_levels_spent(id)                                        Sets the number of levels spent in Combat 1-10                         **
**    is_combat()                                                    Returns 1 if the map is combat, 0 if it's not.                         **
**                                                                                                                                          **
** Commands for less typing :P (Basicly combos of above commands)                                                                           **
**    ns_gethives()                                                  ns_getbuild("team_hive",1,0) for the lazy guy :P                       **
**    ns_getcommander()                                              Returns the ID of the commander. If there is no commander it returns 0 **
**    is_digesting(id)                                               Returns 1 if player is digesting. 0 if not                             **
\********************************************************************************************************************************************/ 

#define NS_VERSION 3 //CHANGE TO "#define NS_VERSION 2" IF YOU ARE RUNNING 2.x

#if NS_VERSION == 2
/* 2.0 offset values */
	#define PRIVATE_ALIEN_RES	397
	#define PRIVATE_AMMO_LMG	229
	#define PRIVATE_AMMO_PISTOL	230
	#define PRIVATE_AMMO_SHOTGUN	231
	#define PRIVATE_AMMO_HMG	232
	#define PRIVATE_AMMO_GL		233
#endif
#if NS_VERSION == 3
/* 3.0 offset values */
	#define PRIVATE_ALIEN_RES	400
	#define PRIVATE_AMMO_LMG	230
	#define PRIVATE_AMMO_PISTOL	231
	#define PRIVATE_AMMO_SHOTGUN	232
	#define PRIVATE_AMMO_HMG	233
	#define PRIVATE_AMMO_GL		234
	#define PLAYER_POINTS_OFFSET	1581 //"numbers are 0-9, these are points SPENT not points available, so if I have a total of 9 points and 5 points left, this number would be 4."
	#define PLAYER_EXPERIENCE	1579 //FLOAT
#endif


/* Weapons */
enum
{
	NSWPN_NONE = 0,
	NSWPN_CLAWS,
	NSWPN_SPIT,
	NSWPN_SPORES,
	NSWPN_SPIKE,
	NSWPN_BITE,
	NSWPN_BITE2,
	NSWPN_SWIPE,
	NSWPN_WEBSPINNER,
	NSWPN_METABOLIZE,
	NSWPN_PARASITE,
	NSWPN_BLINK,
	NSWPN_DIVINEWIND,
	NSWPN_KNIFE,
	NSWPN_PISTOL,
	NSWPN_MG,
	NSWPN_SHOTGUN,
	NSWPN_HMG,
	NSWPN_WELDER,
	NSWPN_MINE,
	NSWPN_GRENADE_GUN,
	NSWPN_LEAP,
	NSWPN_CHARGE,
	NSWPN_UMBRA,
	NSWPN_PRIMALSCREAM,
	NSWPN_BILEBOMB,
	NSWPN_ACIDROCKET,
	NSWPN_HEALINGSPRAY,
	NSWPN_BABBLER,
	NSWPN_STOMP,
	NSWPN_DEVOUR,
	NSWPN_MAX
}

/* Teams */
#define MARINES	1
#define ALIENS	2

/* Classes */
#define CLASS_UNKNOWN	0
#define CLASS_SKULK	1
#define CLASS_GORGE	2
#define CLASS_LERK	3
#define CLASS_FADE	4
#define CLASS_ONOS	5
#define CLASS_MARINE	6
#define CLASS_JETPACK	7
#define CLASS_HEAVY	8
#define CLASS_COMMANDER	9
#define CLASS_GESTATE	10
#define CLASS_DEAD	11
#define CLASS_NOTEAM	12

// A few of the AvHSpecials.h iuser4 flags, modified for ease-of-use...
enum
{
	MASK_NONE = 0,
	MASK_SIGHTED = 1,
	MASK_DETECTED = 2,
	MASK_BUILDABLE = 4,
	MASK_BASEBUILD0 = 8,		// Base build slot #0
	MASK_WEAPONS1 = 8,		// Marine weapons 1
	MASK_CARAPACE = 8,		// Alien carapace
	MASK_WEAPONS2 = 16,		// Marines weapons 2
	MASK_REGENERATION = 16,		// Alien regeneration
	MASK_BASEBUILD1 = 16,		// Base build slot #1
	MASK_WEAPONS3 = 32,		// Marine weapons 3
	MASK_REDEMPTION = 32,		// Alien redemption
	MASK_BASEBUILD2 = 32,		// Base build slot #2
	MASK_ARMOR1 = 64,		// Marine armor 1
	MASK_CELERITY = 64,		// Alien celerity
	MASK_BASEBUILD3 = 64,		// Base build slot #3
	MASK_ARMOR2 = 128,		// Marine armor 2
	MASK_ADRENALINE = 128,		// Alien adrenaline
	MASK_BASEBUILD4 = 128,		// Base build slot #4
	MASK_ARMOR3 = 256,		// Marine armor 3
	MASK_SILENCE = 256,		// Alien silence
	MASK_BASEBUILD5 = 256,		// Base build slot #5
	MASK_JETPACK = 512,		// Marine jetpacks
	MASK_CLOAKING = 512,		// Alien cloaking
	MASK_BASEBUILD6 = 512,		// Base build slot #6
	MASK_FOCUS = 1024,		// Alien focus
	MASK_MOTION = 1024,		// Marine motion tracking
	MASK_BASEBUILD7 = 1024,		// Base build slot #7
	MASK_SCENTOFFEAR = 2048,	// Alien scent of fear
	MASK_DEFENSE2 = 4096,		// Defense level 2
	MASK_DEFENSE3 = 8192,		// Defense level 3
	MASK_ELECTRICITY = 8192,	// Electricy
	MASK_MOVEMENT2 = 16384,		// Movement level 2,
	MASK_MOVEMENT3 = 32768,		// Movement level 3
	MASK_HEAVYARMOR = 32768,	// Marine heavy armor
	MASK_SENSORY2 = 65536,		// Sensory level 2
	MASK_SENSORY3 = 131072,		// Sensory level 3
	MASK_ALIEN_MOVEMENT = 262144,	// Onos is charging
	MASK_WALLSTICKING = 524288,	// Flag for wall-sticking
	MASK_PRIMALSCREAM = 1048576,	// Alien is in range of active primal scream
	MASK_UMBRA = 2097152,		// In umbra
	MASK_DIGESTING = 4194304,	// When set on a visible player, player is digesting.  When set on invisible player, player is being digested
	MASK_RECYCLING = 8388608,	// Building is recycling
	MASK_TOPDOWN = 16777216,	// Commander view
	MASK_PLAYER_STUNNED = 33554432,	// Player has been stunned by stomp
	MASK_ENSNARED = 67108864,	// Webbed
	MASK_ALIEN_EMBRYO = 268435456,	// Gestating
	MASK_SELECTABLE = 536870912,	// ???
	MASK_PARASITED = 1073741824,	// Parasite flag
	MASK_SENSORY_NEARBY = 2147483648// Sensory chamber in range
}

stock ns_getammo(id,Weapon) //GET AMMO ON MARINES WEAPONS
{
	if (ns_getteam(id) == MARINES) // this only works for marines
	{
		if (Weapon == NSWPN_MG)		return get_offset(id,PRIVATE_AMMO_LMG)
		if (Weapon == NSWPN_HMG)		return get_offset(id,PRIVATE_AMMO_HMG)
		if (Weapon == NSWPN_GRENADE_GUN)	return get_offset(id,PRIVATE_AMMO_GL)
		if (Weapon == NSWPN_PISTOL)		return get_offset(id,PRIVATE_AMMO_PISTOL)
		if (Weapon == NSWPN_SHOTGUN)		return get_offset(id,PRIVATE_AMMO_SHOTGUN)
	}
	return 0
}

stock ns_setammo(id,Weapon,Value) //SET AMMO ON MARINES WEAPONS
{
	if (ns_getteam(id) == MARINES) // this only works for marines
	{
		if (Weapon == NSWPN_MG)		set_offset(id,PRIVATE_AMMO_LMG,Value)
		if (Weapon == NSWPN_HMG)		set_offset(id,PRIVATE_AMMO_HMG,Value)
		if (Weapon == NSWPN_GRENADE_GUN)	set_offset(id,PRIVATE_AMMO_GL,Value)
		if (Weapon == NSWPN_PISTOL)		set_offset(id,PRIVATE_AMMO_PISTOL,Value)
		if (Weapon == NSWPN_SHOTGUN)		set_offset(id,PRIVATE_AMMO_SHOTGUN,Value)
	}
	return 0
}

stock ns_setres(id,value)
{
	if (ns_getteam(id) == ALIENS) // this only works for aliens
	{
		set_offset_float(id,PRIVATE_ALIEN_RES,float(value))
		return 1
	}
	return 0
}

stock ns_getres(id)
{
	if (ns_getteam(id) == ALIENS) // this only works for aliens
	{
		return floatround(get_offset_float(id,PRIVATE_ALIEN_RES))
	}
	return 0
}

stock ns_getjpfuel(id)
{
	if (ns_getteam(id) == MARINES) // this only works for marines
	{
		return (entity_get_float(id,EV_FL_fuser3) / 10.0)
	}
	return 0
}

stock ns_setjpfuel(id,fuel)
{
	if (ns_getteam(id) == MARINES) // this only works for marines
	{
		entity_set_float(id,EV_FL_fuser3, fuel * 10)
		return 1
	}
	return 0
}

stock ns_getenergy(id)
{
	if (ns_getteam(id) == ALIENS) // this only works for aliens
	{
		return floatround(entity_get_float(id,EV_FL_fuser1) / 10.0)
	}
	return 0
}

stock ns_setenergy(id,Float:value)
{
	if (ns_getteam(id) == ALIENS) // this only works for aliens
	{
		entity_set_float(id,EV_FL_fuser1,value*10.0)
		return 1
	}
	return 0
}

stock ns_getexp(id)
{
	if (is_combat()) {
		new Vector[3]
		entity_get_vector(id,EV_VEC_vuser4,Vector)
		return floatround(Vector[2])
	}
	return 0
}

stock ns_setexp(id,exp)
{
	if (is_combat()) {
		new Vector[3]
		Vector[2]=float(exp)
		entity_set_vector(id,EV_VEC_vuser4,Vector)
		return 1
	}
	return 0
}

stock ns_getteam(id)
{
	return entity_get_int(id, EV_INT_team)
}

stock ns_getclass(id) 
{
	if ( ns_getteam(id)!=MARINES && ns_getteam(id)!=ALIENS ) return CLASS_NOTEAM
	if (entity_get_int(id,EV_INT_deadflag)) return CLASS_DEAD
	new iuser3 = entity_get_int(id,EV_INT_iuser3)
	
	if (iuser3 == 1) {
		if (ns_getmask(id,MASK_HEAVYARMOR)) return CLASS_HEAVY
		if (ns_getmask(id,MASK_JETPACK)) return CLASS_JETPACK
		return CLASS_MARINE
	}
	if (iuser3 == 2) return CLASS_COMMANDER
	if (iuser3 == 3) return CLASS_SKULK
	if (iuser3 == 4) return CLASS_GORGE
	if (iuser3 == 5) return CLASS_LERK
	if (iuser3 == 6) return CLASS_FADE
	if (iuser3 == 7) return CLASS_ONOS
	if (iuser3 == 8) return CLASS_GESTATE
	return 0
}

stock ns_getmask(id,mask)
{
	if (entity_get_int(id,EV_INT_iuser4)&mask > 0) 
	{
		return 1
	}
	return 0
}

stock ns_setmask(id,mask,onoff)
{
	if ((entity_get_int(id,EV_INT_iuser4)&mask) > 0)
	{
		if (onoff == 1)
		{		
			return 2
		}
		else
		{
			entity_set_int(id,EV_INT_iuser4,entity_get_int(id,EV_INT_iuser4)-mask)
			return 1
		}
	}
	else
	{
		if (onoff == 0)
		{
			return 2
		}
		else
		{
			entity_set_int(id,EV_INT_iuser4,entity_get_int(id,EV_INT_iuser4)+mask)
			return 1
		}
	}
	return 0
}

stock ns_getbuild(svClassname[],iBuiltOnly,iNumber)
{
	new iCount = 0
	new pBuild = find_ent_by_class(-1, svClassname)
	while (pBuild > 0)
	{
		if (iBuiltOnly > 0)
		{
			if (equal(svClassname,"team_hive")) {
				if (entity_get_float(pBuild,EV_FL_fuser1)>=1000)
				{
					iCount++
				}
			
			}
			else
			{
				if (entity_get_float(pBuild,EV_FL_fuser2)>0)
				{
					iCount++
				}
			}

		}else{
			iCount++
		}
		if (iNumber > 0 && iCount == iNumber) 
		{
			return pBuild
		}
		pBuild = find_ent_by_class(pBuild, svClassname)
	}
	return iCount
}

stock ns_hasweapon(id,weapon,setweapon=-1)
{
	if (setweapon == -1)
	{
		if ((entity_get_int(id, EV_INT_weapons) & (1<<weapon)) > 0)
		{
			return 1
		}
	}
	else
	{
		if ((entity_get_int(id, EV_INT_weapons) & (1<<weapon)) > 0)
		{
			if (setweapon == 0)
			{
				entity_set_int(id, EV_INT_weapons,entity_get_int(id, EV_INT_weapons)-(1<<weapon)))
				return 1
			}
			return 0;
		}
		else
		{
			if (setweapon == 1)
			{
				entity_set_int(id, EV_INT_weapons,entity_get_int(id, EV_INT_weapons)+(1<<weapon)))
				return 1
			}
		}
		return 0
	}
	return 0
}


stock ns_popup(id,svMessage[190])
{
	message_begin(MSG_ONE,get_user_msgid("HudText2"),{0,0,0},id)
	write_string(svMessage)
	write_byte(0)
	message_end()
}

stock ns_gethives()
{
	return ns_getbuild("team_hive",1,0)
}

stock ns_getcommander() 
{
	for(new id = 1; id <= get_playersnum(); id++)
	{  
		if(ns_getclass(id)==CLASS_COMMANDER)
		{
			return id
		}
	}
	return 0
}

stock is_digesting(id)
 {
	if (ns_getmask(id,MASK_DIGESTING) && ns_getteam(id)=ALIENS) 
	{
		return 1
	}
	return 0
}

stock is_combat() {
	new MapName[4]
	get_mapname(MapName,3)
	if (equal(MapName,"co_")) 
	{
		return 1
	}
	return 0
}

/* Returns a players experience */
stock ns_getxp(id) {
	if (!is_combat())
		return 0
// This method doesn't rely on an offset, but doesn't work when a player is in first person spectate mode.
// If you don't care about compatability then just use the offset.
	if (entity_get_int(id, EV_INT_iuser1) != 4) {	
		new Float:tempv[3]
		entity_get_vector(id, EV_VEC_vuser4, tempv)
		return floatround(tempv[2])
	} else
		return get_offset(index, PLAYER_EXPERIENCE)
	return 0
}

/* Set a players experience */
stock ns_setxp(index, newxp) {
	if (!is_combat())
		return 0
	set_offset(index, PLAYER_EXPERIENCE, newxp)
	return 1
}

/* Returns a players level in combat 1-10 */
stock ns_getlevel(index) {
	if (!is_combat())
		return 0

	new userxp = get_xp(index)

	if (userxp > 270000)	return 10
	if (userxp > 220000)	return 9
	if (userxp > 175000)	return 8
	if (userxp > 135000)	return 7
	if (userxp > 100000)	return 6
	if (userxp > 70000)	return 5
	if (userxp > 45000)	return 4
	if (userxp > 25000)	return 3
	if (userxp > 10000)	return 2
	if (userxp >= 0)	return 1
	return 0
}

/* Returns the number of levels spent in Combat 1-10 */
stock ns_get_levels_spent(id) {
	if (!is_combat())
		return 0
	return get_offset(id, PLAYER_POINTS_OFFSET)
}

/* sets the number of levels spent in Combat 1-10 */
stock ns_set_levels_spent(id, newlevels) {
	if (!is_combat())
		return 0
	set_offset(id, PLAYER_POINTS_OFFSET, newlevels)
	return 1
}