
#if defined _war3ft_included
  #endinput
#endif
#define _war3ft_included

#pragma library war3ft

#define HELM_RESET	0
#define HELM_SET	1

/* Sets Headshot Immunity for a player */
native Item_Set_Helm(index, status);

/* Determines if a player is immune to headshots */
native Item_Get_Helm(index);

/* Kills a user (Day Of Defeat use only) */
native war3ft_kill(index);

/* This native will block a death message with a certain index */
native dod_block_deathmsg(index);