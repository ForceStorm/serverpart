#include <xs>
#include "util/MessageSender.inc"
#include "util/cdll_dll.h"
#include "util/MetaHook_Msg.h"
#include "util/offset.inc"
#include "util/mp_data.inc"


#define TRUE 1
#define FALSE 0

#define REMOVE_ENTITY(%1)			engfunc(EngFunc_RemoveEntity, %1)



		stock delete_task( iTaskid)
		{
			if( task_exists( iTaskid ) )
				remove_task( iTaskid );
		}

		
		
		
		
		
#define SHORT_BYTES	2 
#define INT_BYTES	4 
#define BYTE_BITS	8 

		stock bool:get_pdata_bool(ent, charbased_offset, intbase_linuxdiff = 5) 
		{ 
			return !!( get_pdata_int(ent, charbased_offset / INT_BYTES, intbase_linuxdiff) & (0xFF<<((charbased_offset % INT_BYTES) * BYTE_BITS)) ) 
		}


		
		
		
		

		stock Float:GetVelocity2D(id)
		{
			new Float:vecVelocity[3];
			pev(id, pev_velocity, vecVelocity);
			vecVelocity[2] = 0.0;
			
			return xs_vec_len(vecVelocity);
		}
		
		
				
		stock StripSlot(id, slot)
		{
			new item = get_pdata_cbase(id, m_rgpPlayerItems[slot]);
			
			while (item > 0)
			{
				set_pev(id, pev_weapons, pev(id, pev_weapons) &~ (1<<get_pdata_int(item, m_iId)))
				
				ExecuteHamB(Ham_Weapon_RetireWeapon, item);
				new new_item = get_pdata_cbase(item, m_pNext);
				
				if (ExecuteHamB(Ham_RemovePlayerItem, id, item))
					ExecuteHamB(Ham_Item_Kill, item);
				
				item = new_item;
			}
			
			set_pdata_cbase(id, m_rgpPlayerItems[slot], -1);
		}