


#define FX_FADE 0
#define FX_FLASH 1
#define FX_FADE_INTERVAL 2

#define DRAW_SPR 1
#define DRAW_TGA 2
#define DRAW_ICON 3
#define PLAY_BINK 4
#define DRAW_BAR 5
#define DRAW_TEXT 6

#define DRAW_SPR_NORMAL 1
#define DRAW_SPR_ADDITIVE 2
#define DRAW_SPR_HOLES 3

#define PLAYBINK_NORMAL_RATE -1.0


enum GameMode_e
{
	GAME_MODE_SURVIVOR = 1,
	GAME_MODE_GHOST,
};


// szTga : you need to put the tga file name to the parameter such as "NewView".And the complete file name also need to be writen in "TgaFilesList.txt"
// iEffect : the value should be FX_FADE / FX_FLASH / FX_FADE_INTERVAL
// flFlashTime : if you set a negative value to this, the flash effect will be there  all along.
// flFadeIntervalTime : if you set a negative value to this, the interval fade effect will be there  all along.
// flHoldTime : if you set a negative value to this, the image will be displayed all along
// iChannel : from 0 to 498

native Cl_DrawTga(id, szTga[], iFullScreen, iCenter, red, green, blue, Float:x, Float:y, iEffect, iChannel, Float:flFadeinTime, Float:flFadeoutTime, Float:flFlashTime, Float:flFadeIntervalTime, Float:flHoldTime)


// iType : use DRAW_TGA / DRAW_SPR
// szImage : if iType is DRAW_TGA, you need to put the tga file name to the parameter such as "BombTarget".If iType is DRAW_SPR, you need to put spr file name to the parameter such as "sprites/icon_bomb.spr"
// iSprDrawType : if iType is DRAW_SPR, iSprDrawType should be DRAW_SPR_NORMAL / DRAW_SPR_ADDITIVE / DRAW_SPR_HOLES (in DRAW_SPR_ADDITIVE mode only the spr image's second frame would be shown)
// iEntityIndex : specifies the index of the entity on which is drawn a followed icon. If the parameter isn't null, you needn't put value to the param x,y,z
// iDistance : specifies the value 1(to enable it to show the distance text) or 0(to disable it to show the distance text)
// iChannel : from 0 to 148
native Cl_DrawFollowIcon(id, iType, szImage[], iSprDrawType, iEntityIndex, x, y, z, iDistance, iChannel)

// szSpr : if iLoadType is HUD_LOAD_SPR, you should put the spr name which is also writen in hud.txt to this parameter.if iLoadType is ENG_LOAD_SPR, you need to put spr file address to this parameter,for instance:"sprites/test.spr"
// iSprDrawType : specifies the value of DRAW_SPR_NORMAL / DRAW_SPR_ADDITIVE / DRAW_SPR_HOLES (in DRAW_SPR_ADDITIVE mode only the spr image's second frame would be shown)
// iEffect : the parameter should be FX_FADE / FX_FLASH / FX_FADE_INTERVAL
// flFlashTime : if you set a negative value to this, the flash effect will be there  all along.
// flFadeIntervalTime : if you set a negative value to this, the interval fade effect will be there  all along.
// flHoldTime : if you set a negative value to this, the image will be displayed all along
// iChannel : from 0 to 198
native Cl_KVCS_DrawSpr(id, szSpr[], red, green, blue, iSprDrawType, Float:x, Float:y, iCenter, iEffect, Float:flFadeinTime, Float:flFadeoutTime, Float:flFlashTime, Float:flFadeIntervalTime, Float:flHoldTime, iChannel)


// iType : the value should be DRAW_SPR / DRAW_TGA / DRAW_ICON / PLAY_BINK / DRAW_BAR
native Cl_RemoveImage(id, iChannel, iType)


// GameMode : the value should be MOD_NORMAL / MOD_TEAM / MOD_INFECTION / MOD_SURVIVE
native Cl_SendGameMode( id, GameMode )


// szBink : put the bink file name to this parameter such as "test".Then it will load "metahook\bik\test.bik"
// flPlayRate : Put the value to this parameter how many seconds it needs when it plays per frame.Normal type : PLAYBINK_NORMAL_RATE
// flStayTime : when it finished playing bik, how long could the image stay on the screen
// flFadeoutTime : when it would stay on screen no longer, how long would the image take to fade out on screen
// iChannel : from 0 to 9
native Cl_PlayBink(id, szBink[], Float:x, Float:y, Float:flPlayRate = PLAYBINK_NORMAL_RATE, Float:flStayTime, Float:flFadeoutTime, iCenter, iLoop, iFullScreen, red, green, blue, iChannel)


// szName_BG : put the background tga file name to this parameter. But the complete file name need to be writen in "TgaFilesList.txt"
// szName_Bar : put the bar tga file name to this parameter. But the complete file name also need to be writen in "TgaFilesList.txt"
// x_BG : the background tga image 's X pos
// y_BG : the background tga image 's Y pos
// x_Bar : the bar tga image 's X pos
// y_Bar : the bar tga image 's Y pos
// iLength : the bar 's complete length
// iChannel : from 0 to 4
native Cl_DrawBar(id, szName_BG[], szName_Bar[], Float:x_BG, Float:y_BG, Float:x_Bar, Float:y_Bar, iCenter, iLength, Float:flBarTime, iChannel)


// iBody : The submodel sequence
native Cl_SetViewEntityBody(id, iBody)


// iBody : The submodel sequence
native Cl_SetViewEntityRender(id, iRenderMode, iRenderFX, iRenderAmout, r, g, b)

// szSpr : if iLoadType is HUD_LOAD_SPR, you should put the spr name which is also writen in hud.txt to this parameter.if iLoadType is ENG_LOAD_SPR, you need to put spr file address to this parameter,for instance:"sprites/test.spr"
// iSprDrawType : specifies the value of DRAW_SPR_NORMAL / DRAW_SPR_ADDITIVE / DRAW_SPR_HOLES (in DRAW_SPR_ADDITIVE mode only the spr image's second frame would be shown)
// iEffect : the parameter should be FX_FADE / FX_FLASH / FX_FADE_INTERVAL
// flFlashTime : if you set a negative value to this, the flash effect will be there  all along.
// flFadeIntervalTime : if you set a negative value to this, the interval fade effect will be there  all along.
// flHoldTime : if you set a negative value to this, the image will be displayed all along
// iChannel : from 0 to 198
native Cl_DrawSpr(id, szSpr[], red, green, blue, iSprDrawType, Float:x, Float:y, iCenter, iEffect, Float:flFadeinTime, Float:flFadeoutTime, Float:flFlashTime, Float:flFadeIntervalTime, Float:flHoldTime, iChannel)

// szWord : must be in UTF-8
// flDisplayTime : if the value is smaller than zero, the text will be displayed always
// iChannel : from 1 to 100
native Cl_DrawFontText( id, szWord[], iCenter, Float:x, Float:y, r, g, b, iFontSize, Float:flDisplayTime, Float:flFadeTime, iChannel )

// This is a private func
// not for share
native Cl_UpdateClientData( id, iMsgType, iValue );


// Very important one:
// szWpnName : The weapon 's name
// iDataType : see the "mp_data.inc"
// szValue : you just need to put the value in the string type
// example : if you wanna set the "ak47" 's clip num to 999, you can code this : MP_ModifyItemData( "ak47", DATA_TYPE_CLIP, "999" );
native MP_SetItemData( szWpnName[], iDataType, szValue[] );

// szWpnName : The weapon 's name
// iDataType : see the "mp_data.inc"
// return : a int type value
native MP_GetItemDataInt( szWpnName[], iDataType );


// szWpnName : The weapon 's name
// iDataType : see the "mp_data.inc"
// return : a float type value
native MP_GetItemDataFloat( szWpnName[], iDataType );


// szWpnName : The weapon 's name
// iDataType : see the "mp_data.inc"
// szData : for restoring the returned data
// iLen : data 's length
native MP_GetItemDataString( szWpnName[], iDataType , szData[], iLen);


// func : Get the current wpn
// id : player index
// szWpnName : restore the weapon 's name
// iLen : data 's length
native MP_GetActiveItem( id , szWpnName[], iLen);


// id : player index
// szWpnName  the weapon 's name
native MP_GiveWpnItem( id , szWpnName[]);


// id : player index
// szWpnName  the weapon 's name
native MP_GiveWpnAmmo( id , szWpnName[], iAmmo);