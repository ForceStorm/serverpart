#include "amxxmodule.h"
#include "main.h"
#include "MetaHook_Msg.h"

int gmsgMetaHook = 0;

fnModifyItemData pfnModifyItemData;
fnGetItemDataInt pfnGetItemDataInt;
fnGetItemDataFloat pfnGetItemDataFloat;
fnGetItemDataString pfnGetItemDataString;
fnGetActiveItem pfnGetActiveItem;
fnGiveWpnAmmo pfnGiveWpnAmmo;
fnGiveWpnItem pfnGiveWpnItem;


int FN_RegUserMsg_Post(const char *pszName, int iSize)
{
	if( !strcmp( pszName, "MetaHook") )
		gmsgMetaHook = META_RESULT_ORIG_RET(int);

	RETURN_META_VALUE(MRES_IGNORED, 0);
}

static cell AMX_NATIVE_CALL DrawTga(AMX *amx, cell *params)
{
	int iIndex = params[1];

	int iLen;
	char* szTga = MF_GetAmxString(amx, params[2], 0, &iLen);

	MESSAGE_BEGIN(iIndex?MSG_ONE_UNRELIABLE:MSG_BROADCAST, gmsgMetaHook, NULL, MF_GetPlayerEdict(iIndex));
	WRITE_BYTE( MH_MSG_DRAW_TGA );
	WRITE_STRING( szTga );
	WRITE_BYTE(params[3]);
	WRITE_BYTE(params[4]);
	WRITE_BYTE(static_cast<byte>(params[5]));
	WRITE_BYTE(static_cast<byte>(params[6]));
	WRITE_BYTE(static_cast<byte>(params[7]));
	WRITE_COORD(amx_ctof(params[8]) * 100);
	WRITE_COORD(amx_ctof(params[9]) * 100);
	WRITE_BYTE(params[10]);
	WRITE_SHORT(params[11]);
	WRITE_COORD(amx_ctof(params[12]) * 10);
	WRITE_COORD(amx_ctof(params[13]) * 10);
	WRITE_COORD(amx_ctof(params[14]) * 10);
	WRITE_COORD(amx_ctof(params[15]) * 10);
	WRITE_COORD(amx_ctof(params[16]) * 10);
	MESSAGE_END();

	return 1;
}
static cell AMX_NATIVE_CALL RemoveImage(AMX *amx, cell *params)
{
	int iIndex = params[1];

	MESSAGE_BEGIN(iIndex?MSG_ONE_UNRELIABLE:MSG_BROADCAST, gmsgMetaHook, NULL, MF_GetPlayerEdict(iIndex));
	WRITE_BYTE( MH_MSG_REMOVE_IMAGE );
	WRITE_SHORT(params[2]);
	WRITE_BYTE(params[3]);
	MESSAGE_END();
	return 1; 
}
static cell AMX_NATIVE_CALL SendGameMode(AMX *amx, cell *params)
{

	int iIndex = params[1];

	MESSAGE_BEGIN( MSG_BROADCAST, gmsgMetaHook);
	//MESSAGE_BEGIN(iIndex?MSG_ONE_UNRELIABLE:MSG_BROADCAST, gmsgMetaHook, NULL, MF_GetPlayerEdict(iIndex));
	WRITE_BYTE( MH_MSG_SET_GAMEMODE );
	WRITE_BYTE(params[2]);
	MESSAGE_END();
	return 1; 
}
static cell AMX_NATIVE_CALL PlayBink(AMX *amx, cell *params)
{
	int iIndex = params[1];

	char* szBink = MF_GetAmxString(amx, params[2], 0, NULL);

	MESSAGE_BEGIN(iIndex?MSG_ONE_UNRELIABLE:MSG_BROADCAST, gmsgMetaHook, NULL, MF_GetPlayerEdict(iIndex));
	WRITE_BYTE( MH_MSG_PLAY_BINK );
	WRITE_STRING(szBink);
	WRITE_COORD(amx_ctof(params[3]) * 100);
	WRITE_COORD(amx_ctof(params[4]) * 100);
	WRITE_COORD(amx_ctof(params[5]) * 10);
	WRITE_COORD(amx_ctof(params[6]) * 10);
	WRITE_COORD(amx_ctof(params[7]) * 10);
	WRITE_BYTE(params[8]);
	WRITE_BYTE(params[9]);
	WRITE_BYTE(params[10]);
	WRITE_SHORT(params[11]);
	WRITE_SHORT(params[12]);
	WRITE_SHORT(params[13]);
	WRITE_SHORT(params[14]);
	MESSAGE_END();
	return 1;
}
static cell AMX_NATIVE_CALL DrawBar(AMX *amx, cell *params)
{
	int iIndex = params[1];

	int iLen1, iLen2;
	
	MESSAGE_BEGIN(iIndex?MSG_ONE_UNRELIABLE:MSG_BROADCAST, gmsgMetaHook, NULL, MF_GetPlayerEdict(iIndex));
	WRITE_BYTE( MH_MSG_DRAW_BAR );
	char* szName_BG = MF_GetAmxString(amx, params[2], 0, &iLen1);
	WRITE_STRING(szName_BG);
	char* szName_Bar = MF_GetAmxString(amx, params[3], 0, &iLen2);
	WRITE_STRING(szName_Bar);
	WRITE_COORD(amx_ctof(params[4]) * 100);
	WRITE_COORD(amx_ctof(params[5]) * 100);
	WRITE_COORD(amx_ctof(params[6]) * 100);
	WRITE_COORD(amx_ctof(params[7]) * 100);
	WRITE_BYTE(params[8]);
	WRITE_SHORT(params[9]);
	WRITE_COORD(amx_ctof(params[10]) * 10);
	WRITE_SHORT(params[11]);
	MESSAGE_END();
	return 1;
}

static cell AMX_NATIVE_CALL DrawFollowIcon(AMX *amx, cell *params)
{
	int iIndex = params[1];

	int iLen;
	char* szImage = MF_GetAmxString(amx, params[3], 0, &iLen);

	MESSAGE_BEGIN(iIndex?MSG_ONE_UNRELIABLE:MSG_BROADCAST, gmsgMetaHook, NULL, MF_GetPlayerEdict(iIndex));
	WRITE_BYTE( MH_MSG_DRAW_FOLLOWICON );
	WRITE_BYTE(params[2]);
	WRITE_STRING(szImage);
	WRITE_BYTE(params[4]);
	WRITE_SHORT(params[5]);
	WRITE_SHORT(params[6]);
	WRITE_SHORT(params[7]);
	WRITE_SHORT(params[8]);
	WRITE_BYTE(params[9]);
	WRITE_SHORT(params[10]);
	MESSAGE_END();
	return 1;
}


static cell AMX_NATIVE_CALL DrawFontText(AMX *amx, cell *params)
{
	int iIndex = params[1];

	int iLen;
	char* szText = MF_GetAmxString(amx, params[2], 0, &iLen);

	MESSAGE_BEGIN(iIndex?MSG_ONE_UNRELIABLE:MSG_BROADCAST, gmsgMetaHook, NULL, MF_GetPlayerEdict(iIndex));
	WRITE_BYTE( MH_MSG_DRAW_TEXT );
	WRITE_STRING(szText);
	WRITE_BYTE(params[3]);
	WRITE_COORD(amx_ctof(params[4]) * 100);
	WRITE_COORD(amx_ctof(params[5]) * 100);
	WRITE_BYTE(static_cast<byte>(params[6]));
	WRITE_BYTE(static_cast<byte>(params[7]));
	WRITE_BYTE(static_cast<byte>(params[8]));
	WRITE_BYTE( params[9] );
	WRITE_COORD(amx_ctof(params[10]) * 10);
	WRITE_COORD(amx_ctof(params[11]) * 10);
	WRITE_BYTE(params[12]);
	MESSAGE_END();
	return 1;
}

static cell AMX_NATIVE_CALL SetViewEntityRender(AMX *amx, cell *params)
{
	int iIndex = params[1];

	MESSAGE_BEGIN(iIndex?MSG_ONE_UNRELIABLE:MSG_BROADCAST, gmsgMetaHook, NULL, MF_GetPlayerEdict(iIndex));
	WRITE_BYTE( MH_MSG_VIEWMDL_RENDER );
	WRITE_BYTE(params[2]);
	WRITE_BYTE(params[3]);
	WRITE_BYTE(params[4]);
	WRITE_BYTE(params[5]);
	WRITE_BYTE(params[6]);
	WRITE_BYTE(params[7]);
	MESSAGE_END();
	return 1;
}

static cell AMX_NATIVE_CALL SetViewEntityBody(AMX *amx, cell *params)
{
	int iIndex = params[1];

	MESSAGE_BEGIN(iIndex?MSG_ONE_UNRELIABLE:MSG_BROADCAST, gmsgMetaHook, NULL, MF_GetPlayerEdict(iIndex));
	WRITE_BYTE( MH_MSG_VIEWENT );
	WRITE_BYTE(params[2]);
	MESSAGE_END();
	return 1;
}

static cell AMX_NATIVE_CALL DrawSpr(AMX *amx, cell *params)
{
	int iIndex = params[1];

	int iLen;
	char* szSprites = MF_GetAmxString(amx, params[3], 0, &iLen);

	MESSAGE_BEGIN(iIndex?MSG_ONE_UNRELIABLE:MSG_BROADCAST, gmsgMetaHook, NULL, MF_GetPlayerEdict(iIndex));
	WRITE_BYTE( MH_MSG_DRAW_SPR );
	WRITE_STRING(szSprites);
	WRITE_SHORT(params[3]);
	WRITE_SHORT(params[4]);
	WRITE_SHORT(params[5]);
	WRITE_BYTE(params[6]);
	WRITE_COORD(amx_ctof(params[7]) * 100);
	WRITE_COORD(amx_ctof(params[8]) * 100);
	WRITE_BYTE(params[9]);
	WRITE_BYTE(params[10]);
	WRITE_COORD(amx_ctof(params[11]) * 10);
	WRITE_COORD(amx_ctof(params[12]) * 10);
	WRITE_COORD(amx_ctof(params[13]) * 10);
	WRITE_COORD(amx_ctof(params[14]) * 10);
	WRITE_COORD(amx_ctof(params[15]) * 10);
	WRITE_SHORT(params[16]);
	MESSAGE_END();
	return 1;
}

bool IsValidPlayer (edict_t *pent)
{
   if (FNullEnt (pent))
      return false;

   if(  ( pent->v.flags & FL_CLIENT)  ||  ( pent->v.flags & FL_FAKECLIENT) )
	   return true;

   return false;
}
static cell AMX_NATIVE_CALL UpdateClientData(AMX *amx, cell *params)
{
	int iIndex = params[1];
	int iMsgType = params[2];

	MESSAGE_BEGIN(iIndex?MSG_ONE_UNRELIABLE:MSG_BROADCAST, gmsgMetaHook, NULL, MF_GetPlayerEdict(iIndex));
	WRITE_BYTE( iMsgType );
	if( iMsgType == MH_MSG_UPDATE_SUV_KILL )
		WRITE_LONG( params[3] );
	else if( iMsgType == MH_MSG_UPDATE_SUV_ROUND )
		WRITE_BYTE( params[3] );
	else if( iMsgType == MH_MSG_UPDATE_TIME)
		WRITE_LONG( params[3] );
	else if( iMsgType == MH_MSG_SET_COUNTDOWN)
		WRITE_BYTE( params[3] );
	else if( iMsgType == MH_MSG_SET_SUV_ROUNDEND)
		WRITE_BYTE( params[3] );
	MESSAGE_END();
	return 1;
}

static cell AMX_NATIVE_CALL SetItemData(AMX *amx, cell *params)
{
	int iLen;
	char *szWpnName = MF_GetAmxString(amx, params[1], 0, &iLen);
	char *szValue = MF_GetAmxString(amx, params[3], 0, &iLen);

	pfnModifyItemData(  szWpnName, params[2],  szValue );

	return 1;
}

static cell AMX_NATIVE_CALL GetItemDataInt(AMX *amx, cell *params)
{
	int iLen;
	char *szWpnName = MF_GetAmxString(amx, params[1], 0, &iLen);

	return pfnGetItemDataInt(  szWpnName, params[2] );
}
static cell AMX_NATIVE_CALL GetItemDataFloat(AMX *amx, cell *params)
{
	int iLen;
	char *szWpnName = MF_GetAmxString(amx, params[1], 0, &iLen);

	return amx_ftoc(  pfnGetItemDataFloat(  szWpnName, params[2] )  );
}

static cell AMX_NATIVE_CALL GetItemDataString(AMX *amx, cell *params)
{
	int iLen;
	char *szWpnName = MF_GetAmxString(amx, params[1], 0, &iLen);

	char *szData;
	szData = pfnGetItemDataString( szWpnName, params[2] );

	MF_SetAmxString(amx, params[3], szData, params[4]);

	return 1;
}


static cell AMX_NATIVE_CALL GetActiveItem(AMX *amx, cell *params)
{
	edict_t *pent = INDEXENT( params[1] );

	char * szData = pfnGetActiveItem( pent );
	MF_SetAmxString(amx, params[2], szData, params[3]);

	return 1;
}


static cell AMX_NATIVE_CALL GiveWpnItem(AMX *amx, cell *params)
{
	edict_t *pent = INDEXENT( params[1] );

	int iLen;
	char *szWpnName = MF_GetAmxString(amx, params[2], 0, &iLen);
	pfnGiveWpnItem( pent, szWpnName );

	return 1;
}

static cell AMX_NATIVE_CALL GiveWpnAmmo(AMX *amx, cell *params)
{
	edict_t *pent = INDEXENT( params[1] );

	int iLen;
	char *szWpnName = MF_GetAmxString(amx, params[2], 0, &iLen);
	pfnGiveWpnAmmo( pent, szWpnName , params[3]);

	return 1;
}


AMX_NATIVE_INFO RegisterNative[] =
{
	{ "Cl_DrawTga", DrawTga },								// done
	{ "Cl_DrawBar", DrawBar },								// done
	{ "Cl_RemoveImage" , RemoveImage },						// done
	{ "Cl_SendGameMode", SendGameMode },					// done
	{ "Cl_PlayBink", PlayBink },							// done
	{ "Cl_DrawFollowIcon", DrawFollowIcon },				// done
	{ "Cl_DrawFontText", DrawFontText },					// done
	{ "Cl_SetViewEntityRender", SetViewEntityRender },		// done
	{ "Cl_SetViewEntityBody", SetViewEntityBody },			// done
	{ "Cl_DrawSpr", DrawSpr},								// done
	{ "Cl_UpdateClientData", UpdateClientData },			// done
	{ "MP_SetItemData", SetItemData },						// done
	{ "MP_GetItemDataInt", GetItemDataInt },				// done
	{ "MP_GetItemDataFloat", GetItemDataFloat },			// done
	{ "MP_GetItemDataString", GetItemDataString },			// done
	{ "MP_GiveWpnItem", GiveWpnItem },						// done
	{ "MP_GiveWpnAmmo", GiveWpnAmmo },						// done
	{ "MP_GetActiveItem", GetActiveItem },					// done
	{ NULL, NULL }
};
void OnAmxxAttach()
{
	MF_AddNatives(RegisterNative);
}

void OnPluginsLoaded()
{
	HMODULE hMp = GetModuleHandle( "mp.dll" );
	if( ! hMp )
	{
		SERVER_PRINT( "[ERROR] The mp.dll handle can't not be got by mudule." );
		return;
	}
	pfnModifyItemData = ( fnModifyItemData ) GetProcAddress( hMp,  "ModifyItemData" );
	if( !pfnModifyItemData )
		SERVER_PRINT( "[ERROR] Function \"ModifyItemData\" can't not be Hook" );

	pfnGetItemDataInt = (fnGetItemDataInt) GetProcAddress( hMp,  "GetItemDataInit" );
	if( !pfnGetItemDataInt )
		SERVER_PRINT( "[ERROR] Function \"ModifyItemData\" can't not be Hook" );

	pfnGetItemDataFloat = (fnGetItemDataFloat) GetProcAddress( hMp,  "GetItemDataFloat" );
	if( !pfnGetItemDataFloat )
		SERVER_PRINT( "[ERROR] Function \"ModifyItemData\" can't not be Hook" );

	pfnGetItemDataString = (fnGetItemDataString) GetProcAddress( hMp,  "GetItemDataString" );
	if( !pfnGetItemDataString )
		SERVER_PRINT( "[ERROR] Function \"ModifyItemData\" can't not be Hook" );

	pfnGiveWpnItem = (fnGiveWpnItem) GetProcAddress( hMp,  "GiveWpnItem" );
	if( !pfnGiveWpnItem )
		SERVER_PRINT( "[ERROR] Function \"GiveWpnItem\" can't not be Hook" );

	pfnGiveWpnAmmo = (fnGiveWpnAmmo) GetProcAddress( hMp,  "GiveWpnAmmo" );
	if( !pfnGiveWpnAmmo )
		SERVER_PRINT( "[ERROR] Function \"GiveWpnAmmo\" can't not be Hook" );

	pfnGetActiveItem = (fnGetActiveItem) GetProcAddress( hMp,  "GetActiveItem" );
	if( !pfnGetActiveItem )
		SERVER_PRINT( "[ERROR] Function \"GetActiveItem\" can't not be Hook" );


}