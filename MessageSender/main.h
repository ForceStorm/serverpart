#ifndef _MAIN_H
#define _MAIN_H

enum ClientDataType_e
{
	CLIENT_DATA_SUV_ROUND = 1,
	CLIENT_DATA_SUV_KILLS,
};

typedef void ( *fnModifyItemData)(const char *pszWpnName , int iDataType,  const char *szTemp );
typedef void ( *fnGiveWpnItem)( edict_t *pent, const char *pszWpnName );
typedef void ( *fnGiveWpnAmmo)( edict_t *pent, const char *pszWpnName , int iAmmo );
typedef char* ( *fnGetActiveItem)( edict_t *pent );

typedef int ( *fnGetItemDataInt)(const char *pszWpnName , int iDataType );
typedef float ( *fnGetItemDataFloat)(const char *pszWpnName , int iDataType );
typedef char* ( *fnGetItemDataString)(const char *pszWpnName , int iDataType );


#endif