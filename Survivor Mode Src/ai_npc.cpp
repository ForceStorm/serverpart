#include "amxxmodule.h"
#include "ai_npc.h"
#include "util.h"
#include "main.h"
#include <cbase.h>

CNpcManager g_NpcManager;


// ---------------------------------------------------------------------- Npc Base
CNpcBase::CNpcBase()
{
	m_iState = 0;
	m_pentCurEnemy = NULL;
	m_pentNpc = NULL;
	m_pNext = NULL;
	m_iNpcType = 0;
	flNextFindEnemyTime = 0.0;
}
CNpcBase::~CNpcBase()
{
	Kill();
}
void CNpcBase::Kill(void)
{
	REMOVE_ENTITY( m_pentNpc );
}

void CNpcBase::Spawn(  Vector vecOrigin)
{
	vecOrigin.z += 2.0;
	SET_ORIGIN( m_pentNpc, vecOrigin);

	m_pentNpc->v.maxs  = Vector( 16.0, 16.0, 36.0 );
	m_pentNpc->v.mins = Vector( -16.0, -16.0, -36.0 );
	m_pentNpc->v.takedamage = DAMAGE_AIM;
	
	m_iState = STATE_IDLE;

	DROP_TO_FLOOR( m_pentNpc  );
}
void CNpcBase::SetAnim( int iAnim )
{
	if( m_pentNpc->v.sequence == iAnim )
		return;

	m_pentNpc->v.sequence = iAnim;
	m_pentNpc->v.animtime = gpGlobals->time ;
}
edict_t * CNpcBase::FindNearestEnemy( void )
{
	Vector vecOrigin = m_pentNpc->v.origin ;

	double iNearest = 99999;

	edict_t *pentNearestOne = NULL;

	for(int i = 1; i <= gpGlobals->maxClients ; i++)
	{
		edict_t *pentPlayer = NULL;
		pentPlayer = INDEXENT(i);
		
		if( !IsValidPlayer( pentPlayer ) )
			continue;

		if( !CanSeeEnemy( pentPlayer) )
			continue;

		if( pentPlayer->v.health <= 0 )
			continue;

		if( pentPlayer->v.takedamage == DAMAGE_NO)
			continue;

		Vector vecDestOrigin = pentPlayer->v.origin ;

		int iLength;
		iLength = CalcDistance( vecOrigin, vecDestOrigin );

		if( iLength < iNearest)
		{
			iNearest = iLength;
			pentNearestOne = pentPlayer;
		}
	}

	return pentNearestOne;

}
void CNpcBase::SetTurnToEnemy( Vector & vecAngles )
{
	if( FNullEnt( m_pentCurEnemy ) || FNullEnt( m_pentNpc ) )
		return;

	Vector vecVec;
	vecVec = m_pentCurEnemy->v.origin  -   m_pentNpc->v.origin ;

	VEC_TO_ANGLES( vecVec , vecAngles );
	vecAngles.x *= -1.0;
	vecAngles.z = 0.0;

	m_pentNpc->v.angles = vecAngles;
	m_pentNpc->v.angles.x = 0.0;
}
bool CNpcBase::CanSeeEnemy( edict_t *pentTarget )
{
	TraceResult trResult;
	Vector vecEyeOrigin;

	vecEyeOrigin = m_pentNpc->v.origin ;
	vecEyeOrigin.z += m_pentNpc->v.maxs.z ;

	TRACE_LINE( vecEyeOrigin , pentTarget->v.origin ,  1,  NULL,  &trResult);

	Vector vecHitOrigin = trResult.vecEndPos ;

	int iLength = CalcDistance( vecHitOrigin , pentTarget->v.origin );

	if( !iLength )
		return true;
	else
		return false;

}

// ---------------------------------------------------------------------- Normal Zombie Npc
CNpcNormalZombie::CNpcNormalZombie( )
{
}
CNpcNormalZombie::~CNpcNormalZombie()
{
}
void CNpcNormalZombie::SetDead( void )
{
	SetAnim( ANIM_NZ_DEAD );
	m_pentNpc->v.nextthink = gpGlobals->time + ANIM_NZ_DEAD_TIME;
	m_pentNpc->v.solid = SOLID_NOT;
	m_pentNpc->v.v_angle =  Vector( 0.0,  0.0,   0.0 );
	m_pentNpc->v.movetype = MOVETYPE_NONE;

	m_iState = STATE_DYING;
}
int CNpcNormalZombie::Dead( int & iNextState )
{
	g_NpcManager.KillNpc( ENTINDEX( m_pentNpc ) );

	return THINK_STOP;
}
int CNpcNormalZombie::Dying(  int & iNextState  )
{
	m_pentNpc->v.velocity = Vector( 0.0,  0.0,   0.0 );
	m_pentNpc->v.v_angle =  Vector( 0.0,  0.0,   0.0 );

	iNextState = STATE_DEAD;
	m_pentNpc->v.nextthink = gpGlobals->time + NPC_CORPSE_STAY_TIME;

	return THINK_STOP;
}
int CNpcNormalZombie::Idle( int & iNextState  )
{
	m_pentNpc->v.velocity = Vector( 0.0,  0.0,   0.0 );

	iNextState = STATE_RETHINK;

	SetAnim( ANIM_NZ_IDLE );
	m_pentNpc->v.nextthink = gpGlobals->time + ANIM_NZ_IDLE_TIME;

	return THINK_STOP;
}
int CNpcNormalZombie::ReThink( int & iNextState)
{
	if( m_pentNpc->v.health <= 0 ||  m_pentNpc->v.solid == SOLID_NOT )		// fix bug that the  dead npc  still can walk..
	{
		SetDead();
		return THINK_CONTINUE;
	}

	if( gpGlobals->time >  flNextFindEnemyTime   ||  !m_pentCurEnemy )
	{
		m_pentCurEnemy = FindNearestEnemy();
		if( !m_pentCurEnemy )
		{
			m_pentNpc->v.nextthink = gpGlobals->time + NPC_RELAX_TIME;
			iNextState = STATE_IDLE;
			return THINK_CONTINUE;
		}
		flNextFindEnemyTime = gpGlobals->time + NPC_FIND_ENEMY_TIME;
	}

	int iDistance = CalcDistance( m_pentCurEnemy->v.origin , m_pentNpc->v.origin  );
	if( iDistance > NPC_STOP_DISTANCE)
	{
		if( CanSeeEnemy( m_pentCurEnemy ) )
		{
			iNextState = STATE_IDLE;
			return THINK_CONTINUE;
		}
		else
		{
			g_NpcManager.KillNpc( ENTINDEX( m_pentNpc ) );
			return THINK_STOP;
		}
	}

	Vector vecAngles;
	SetTurnToEnemy(vecAngles);

	if( iDistance > ATTACK_RANGE )
	{
		iNextState = STATE_MOVE;
		return THINK_CONTINUE;
	}
	else
	{
		iNextState = STATE_ATTACK;
		return THINK_CONTINUE;
	}

}
int CNpcNormalZombie::Jump( int & iNextState )
{
	// jump
	Vector vecAngles;
	SetTurnToEnemy( vecAngles );
	SetAnim( ANIM_BM_JUMP );

	Vector vecVelocity = m_pentNpc->v.velocity ;

	vecVelocity.x *= 0.5;
	vecVelocity.y *= 0.5;
	vecVelocity.z = m_pentNpc->v.iJumpHeight;

	m_pentNpc->v.velocity = vecVelocity;
	m_pentNpc->v.nextthink = gpGlobals->time + 0.5;

	iNextState = STATE_RETHINK;
	return THINK_STOP;

}
int CNpcNormalZombie::Attack( int & iNextState  )
{
	Vector vecAngles;
	SetTurnToEnemy(vecAngles);

	int iDistance = CalcDistance( m_pentNpc->v.origin , m_pentCurEnemy->v.origin );

	if( iDistance < 0 )
	{
		iNextState = STATE_DEAD;
		return THINK_CONTINUE;
	}

	if( iDistance > ATTACK_RANGE)
	{
		m_iState = STATE_RETHINK;
		return THINK_CONTINUE;
	}
	else
	{
		if( m_pentCurEnemy->v.health - m_pentNpc->v.dmg  <= 0 )
		{
			gpGamedllFuncs->dllapi_table->pfnClientKill( m_pentCurEnemy );
		}
		else
		{
			PlayerTakeDamage( m_pentCurEnemy, m_pentNpc, m_pentNpc->v.dmg );
		}
		m_pentNpc->v.nextthink = gpGlobals->time + ANIM_NZ_ATTACK_TIME;

		SetAnim( ANIM_NZ_ATTACK );

		iNextState = STATE_ATTACK;
		return THINK_STOP;
	}
}
int CNpcNormalZombie::Move( int & iNextState )
{
	Vector vecAngles;
	SetTurnToEnemy( vecAngles );

	int iBlockerType, iWalkStatus;
	iWalkStatus = WALK_MOVE( m_pentNpc, vecAngles.y, 0.5, WALKMOVE_NORMAL );

	if( !iWalkStatus )
		iBlockerType = AssessBlocker( m_pentCurEnemy, m_pentNpc );
	else
		iBlockerType = BLOCKER_NONE;
	
	if( iBlockerType == BLOCKER_NONE || iBlockerType == BLOCKER_UNKNOWN )
	{
		MAKE_VECTORS( vecAngles );
		vecAngles = gpGlobals->v_forward ;
		Vector vecVelocity = vecAngles * m_pentNpc->v.maxspeed ;

		m_pentNpc->v.velocity = vecVelocity;
		m_pentNpc->v.nextthink = gpGlobals->time + ANIM_NZ_WALK_TIME;

		SetAnim( ANIM_NZ_WALK );

		iNextState = STATE_RETHINK;

		return THINK_STOP;
	}
	else if( iBlockerType == BLOCKER_CAN_JUMP)
	{
		iNextState = STATE_JUMP;
		return THINK_CONTINUE;
	}
	else if( iBlockerType == BLOCKER_BIG )
	{
		iNextState = STATE_IDLE;
		return THINK_CONTINUE;
	}
}
void CNpcNormalZombie::Think(void)
{
	int iThinkState = 1;

	while( iThinkState )
	{
		switch( m_iState )
		{
		case STATE_DEAD:
			iThinkState = Dead( m_iState );
			break;
		case STATE_DYING:
			iThinkState = Dying( m_iState );
			break;
		case STATE_JUMP:
			iThinkState = Jump( m_iState );
			break;
		case STATE_ATTACK:
			iThinkState = Attack( m_iState );
			break;
		case STATE_MOVE:
			iThinkState = Move( m_iState );
			break;
		case STATE_IDLE:
			iThinkState = Idle( m_iState );
			break;
		case STATE_RETHINK:
			iThinkState = ReThink( m_iState );
		}

		if( iThinkState == THINK_STOP)
			break;
	}
}
CNpcNormalZombie *CNpcNormalZombie::Create( int iMaxHealth, int iDamage , int iModexIndex , int iMaxSpeed , int iJumpHeight , char *szModel, Vector vecMins, Vector vecMaxs)
{
	CNpcNormalZombie *pNpcZombie = new CNpcNormalZombie;
	pNpcZombie->m_iNpcType = NPC_NORMALZOMBIE;

	pNpcZombie->m_pentNpc = CREATE_NAMED_ENTITY( MAKE_STRING("info_target") );

	if( FNullEnt( pNpcZombie->m_pentNpc ) )
	{
		return pNpcZombie;
	}

	pNpcZombie->m_pentNpc->v.solid = SOLID_BBOX;
	pNpcZombie->m_pentNpc->v.movetype = MOVETYPE_STEP;
	//pNpcZombie->m_pentNpc->v.takedamage = DAMAGE_AIM;
	pNpcZombie->m_pentNpc->v.classname = MAKE_STRING( NPC_CLASSNAME );
	pNpcZombie->m_pentNpc->v.animtime = gpGlobals->time ;
	pNpcZombie->m_pentNpc->v.max_health = iMaxHealth;
	pNpcZombie->m_pentNpc->v.health = iMaxHealth;
	pNpcZombie->m_pentNpc->v.gravity = 1.0;
	pNpcZombie->m_pentNpc->v.dmg = iDamage;
	pNpcZombie->m_pentNpc->v.modelindex = iModexIndex;
	pNpcZombie->m_pentNpc->v.nextthink =  gpGlobals->time + 1.0;
	pNpcZombie->m_pentNpc->v.framerate = 1.0;
	pNpcZombie->m_pentNpc->v.frame = 0;
	pNpcZombie->m_pentNpc->v.maxspeed = iMaxSpeed;
	pNpcZombie->m_pentNpc->v.iJumpHeight = iJumpHeight;

	SET_MODEL( pNpcZombie->m_pentNpc, szModel );
	SET_SIZE( pNpcZombie->m_pentNpc , vecMins, vecMaxs );

	pNpcZombie->m_pentNpc->v.mins = vecMins ;
	pNpcZombie->m_pentNpc->v.maxs = vecMaxs;

	return pNpcZombie;
}
// ----------------------------------------------------------------------- Invisible Zombie
CNpcInvisibleZb::CNpcInvisibleZb( )
{
	m_flSkillEndTime = 0;
}
CNpcInvisibleZb::~CNpcInvisibleZb()
{
}
void CNpcInvisibleZb::SetDead( void )
{
	SetAnim( ANIM_IZ_DEAD );
	m_pentNpc->v.nextthink = gpGlobals->time + ANIM_IZ_DEAD_TIME;
	m_pentNpc->v.solid = SOLID_NOT;
	m_pentNpc->v.v_angle = Vector( 0.0,  0.0,   0.0 );
	m_pentNpc->v.movetype = MOVETYPE_NONE;

	m_iState = STATE_DYING;
}
int CNpcInvisibleZb::Dead( int & iNextState )
{
	m_pentNpc->v.velocity = Vector( 0.0,  0.0,   0.0 );

	g_NpcManager.KillNpc( ENTINDEX( m_pentNpc ) );

	return THINK_STOP;
}
int CNpcInvisibleZb::Dying(  int & iNextState  )
{
	m_pentNpc->v.velocity = Vector( 0.0,  0.0,   0.0 );
	m_pentNpc->v.v_angle = Vector( 0.0,  0.0,   0.0 );
	m_pentNpc->v.nextthink = gpGlobals->time + NPC_CORPSE_STAY_TIME;

	iNextState = STATE_DEAD;
	return THINK_STOP;
}
int CNpcInvisibleZb::Idle( int & iNextState  )
{
	m_pentNpc->v.velocity = Vector( 0.0,  0.0,   0.0 );

	iNextState = STATE_RETHINK;

	SetAnim( ANIM_IZ_IDLE );
	m_pentNpc->v.nextthink = gpGlobals->time + ANIM_IZ_IDLE_TIME;

	return THINK_STOP;
}
int CNpcInvisibleZb::ReThink( int & iNextState)
{
	if( m_pentNpc->v.health <= 0 ||  m_pentNpc->v.solid == SOLID_NOT )		// fix bug that the  dead npc  still can walk..
	{
		SetDead();
		return THINK_CONTINUE;
	}

	int iSkillStatus = 0;
	if( !m_flSkillEndTime )
	{
		iSkillStatus = SKILL_STATUS_NONE;
	}
	else if( gpGlobals->time <= m_flSkillEndTime )
	{
		iSkillStatus = SKILL_STATUS_INSKILL;
		m_pentNpc->v.effects |= EF_NODRAW;
	}
	else if(  gpGlobals->time > m_flSkillEndTime && gpGlobals->time < m_flSkillEndTime +  SKILL_COOLDOWN_TIME )
	{
		m_pentNpc->v.effects &= ~EF_NODRAW;
		iSkillStatus = SKILL_STATUS_COOLDOWN;
	}
	else
		iSkillStatus = SKILL_STATUS_NONE;


	if( gpGlobals->time >  flNextFindEnemyTime   ||  !m_pentCurEnemy )
	{
		m_pentCurEnemy = FindNearestEnemy();
		if( !m_pentCurEnemy )
		{
			m_pentNpc->v.nextthink = gpGlobals->time + NPC_RELAX_TIME;
			iNextState = STATE_IDLE;
			return THINK_CONTINUE;
		}
		flNextFindEnemyTime = gpGlobals->time + NPC_FIND_ENEMY_TIME;
	}

	int iDistance = CalcDistance( m_pentCurEnemy->v.origin , m_pentNpc->v.origin  );
	if( iDistance > NPC_STOP_DISTANCE)
	{
		if( CanSeeEnemy( m_pentCurEnemy ) )
		{
			iNextState = STATE_IDLE;
			return THINK_CONTINUE;
		}
		else
		{
			g_NpcManager.KillNpc( ENTINDEX( m_pentNpc ) );
			return THINK_STOP;
		}
	}
	if( iSkillStatus == SKILL_STATUS_NONE )
	{
		m_flSkillEndTime = gpGlobals->time + m_pentNpc->v.flSkillTime;
		m_pentNpc->v.effects |= EF_NODRAW;
	}

	Vector vecAngles;
	SetTurnToEnemy(vecAngles);

	if( iDistance > IZ_ATTACK_RANGE )
	{
		iNextState = STATE_MOVE;
		return THINK_CONTINUE;
	}
	else
	{
		iNextState = STATE_ATTACK;
		return THINK_CONTINUE;
	}

}
int CNpcInvisibleZb::Jump( int & iNextState )
{
	// jump
	Vector vecAngles;
	SetTurnToEnemy( vecAngles );
	SetAnim( ANIM_IZ_JUMP );

	Vector vecVelocity = m_pentNpc->v.velocity ;

	vecVelocity.x *= 0.5;
	vecVelocity.y *= 0.5;
	vecVelocity.z =  m_pentNpc->v.iJumpHeight;

	m_pentNpc->v.velocity = vecVelocity;
	m_pentNpc->v.nextthink = gpGlobals->time + 0.5;

	iNextState = STATE_RETHINK;
	return THINK_STOP;
}
int CNpcInvisibleZb::Attack( int & iNextState  )
{
	Vector vecAngles;
	SetTurnToEnemy(vecAngles);

	int iDistance = CalcDistance( m_pentNpc->v.origin , m_pentCurEnemy->v.origin );
	if( iDistance < 0 )
	{
		iNextState = STATE_DEAD;
		return THINK_CONTINUE;
	}

	if( iDistance > IZ_ATTACK_RANGE)
	{
		m_iState = STATE_RETHINK;
		return THINK_CONTINUE;
	}
	else
	{
		if( m_pentCurEnemy->v.health - m_pentNpc->v.dmg  <= 0 )
		{
			gpGamedllFuncs->dllapi_table->pfnClientKill( m_pentCurEnemy );
		}
		else
		{
			PlayerTakeDamage( m_pentCurEnemy, m_pentNpc, m_pentNpc->v.dmg );
		}

		m_pentNpc->v.nextthink = gpGlobals->time + ANIM_IZ_ATTACK_TIME;

		SetAnim( ANIM_IZ_ATTACK );

		iNextState = STATE_ATTACK;
		return THINK_STOP;
	}
}
int CNpcInvisibleZb::Move( int & iNextState )
{
	Vector vecAngles;
	SetTurnToEnemy( vecAngles );

	int iBlockerType, iWalkStatus;
	iWalkStatus = WALK_MOVE( m_pentNpc, vecAngles.y, 0.5, WALKMOVE_NORMAL );

	if( !iWalkStatus )
		iBlockerType = AssessBlocker( m_pentCurEnemy, m_pentNpc );
	else
		iBlockerType = BLOCKER_NONE;
	
	if( iBlockerType == BLOCKER_NONE || iBlockerType == BLOCKER_UNKNOWN )
	{
		MAKE_VECTORS( vecAngles );
		vecAngles = gpGlobals->v_forward ;
		Vector vecVelocity = vecAngles * m_pentNpc->v.maxspeed ;

		m_pentNpc->v.velocity = vecVelocity;
		m_pentNpc->v.nextthink = gpGlobals->time + ANIM_IZ_WALK_TIME;
		
		SetAnim( ANIM_IZ_WALK );

		iNextState = STATE_RETHINK;

		return THINK_STOP;
	}
	else if( iBlockerType == BLOCKER_CAN_JUMP )
	{
		iNextState = STATE_JUMP;
		return THINK_CONTINUE;
	}
	else if( iBlockerType == BLOCKER_BIG )
	{
		iNextState = STATE_IDLE;
		return THINK_CONTINUE;
	}
}
void CNpcInvisibleZb::Think(void)
{
	int iThinkState = 1;

	while( iThinkState )
	{
		switch( m_iState )
		{
		case STATE_DEAD:
			iThinkState = Dead( m_iState );
			break;
		case STATE_DYING:
			iThinkState = Dying( m_iState );
			break;
		case STATE_JUMP:
			iThinkState = Jump( m_iState );
			break;
		case STATE_ATTACK:
			iThinkState = Attack( m_iState );
			break;
		case STATE_MOVE:
			iThinkState = Move( m_iState );
			break;
		case STATE_IDLE:
			iThinkState = Idle( m_iState );
			break;
		case STATE_RETHINK:
			iThinkState = ReThink( m_iState );
		}

		if( iThinkState == THINK_STOP)
			break;
	}
}
CNpcInvisibleZb *CNpcInvisibleZb::Create( int iMaxHealth, int iDamage , int iModexIndex , int iMaxSpeed, float flSkillTime, int iJumpHeight , char *szModel, Vector vecMins, Vector vecMaxs)
{
	CNpcInvisibleZb *pNpcZombie = new CNpcInvisibleZb;
	pNpcZombie->m_iNpcType = NPC_INVISIBLE;

	pNpcZombie->m_pentNpc = CREATE_NAMED_ENTITY( MAKE_STRING("info_target") );

	if( FNullEnt( pNpcZombie->m_pentNpc ) )
	{
		return pNpcZombie;
	}

	pNpcZombie->m_pentNpc->v.solid = SOLID_BBOX;
	pNpcZombie->m_pentNpc->v.movetype = MOVETYPE_STEP;
	//pNpcZombie->m_pentNpc->v.takedamage = DAMAGE_AIM;
	pNpcZombie->m_pentNpc->v.classname = MAKE_STRING( NPC_CLASSNAME );
	pNpcZombie->m_pentNpc->v.animtime = gpGlobals->time ;
	pNpcZombie->m_pentNpc->v.max_health = iMaxHealth;
	pNpcZombie->m_pentNpc->v.health = iMaxHealth;
	pNpcZombie->m_pentNpc->v.gravity = 0.8;
	pNpcZombie->m_pentNpc->v.dmg = iDamage;
	pNpcZombie->m_pentNpc->v.modelindex = iModexIndex;
	pNpcZombie->m_pentNpc->v.nextthink =  gpGlobals->time + 1.0;
	pNpcZombie->m_pentNpc->v.framerate = 1.0;
	pNpcZombie->m_pentNpc->v.frame = 0;
	pNpcZombie->m_pentNpc->v.maxspeed = iMaxSpeed;
	pNpcZombie->m_pentNpc->v.flSkillTime = flSkillTime;
	pNpcZombie->m_pentNpc->v.iJumpHeight = iJumpHeight;

	SET_MODEL( pNpcZombie->m_pentNpc, szModel );
	SET_SIZE( pNpcZombie->m_pentNpc , vecMins, vecMaxs );

	pNpcZombie->m_pentNpc->v.mins = vecMins ;
	pNpcZombie->m_pentNpc->v.maxs = vecMaxs;

	return pNpcZombie;
}

// ---------------------------------------------------------------------- Boomer Zombie Npc
CNpcBoomerZb::CNpcBoomerZb( )
{
}
CNpcBoomerZb::~CNpcBoomerZb()
{
}
void CNpcBoomerZb::SetDead( void )
{
	Vector vecOrigin = m_pentNpc->v.origin ;

	MESSAGE_BEGIN( MSG_BROADCAST, SVC_TEMPENTITY );
	WRITE_BYTE( TE_EXPLOSION );
	WRITE_COORD( vecOrigin.x  );
	WRITE_COORD( vecOrigin.y  );
	WRITE_COORD( vecOrigin.z  );
	WRITE_SHORT( g_sModelIndexBoomerExplodeSpr );
	WRITE_BYTE( 40 );
	WRITE_BYTE( 30 );
	WRITE_BYTE( TE_EXPLFLAG_NONE );
	MESSAGE_END();

	RadiusDamage( vecOrigin, m_pentNpc, m_pentNpc, 80.0, 150.0, 0 );
	SetAnim( ANIM_BM_DEAD );

	m_pentNpc->v.solid = SOLID_NOT;
	m_pentNpc->v.velocity = Vector( 0.0,  0.0,   0.0 );
	m_pentNpc->v.nextthink = gpGlobals->time + ANIM_BM_DEAD_TIME;
	m_pentNpc->v.movetype = MOVETYPE_NONE;
	
	m_iState = STATE_DYING;
}
int CNpcBoomerZb::Dead( int & iNextState )
{
	g_NpcManager.KillNpc( ENTINDEX( m_pentNpc ) );

	return THINK_STOP;
}
int CNpcBoomerZb::BoomerDmg( int & iNextState )
{
	Vector vecOrigin = m_pentNpc->v.origin ;
	
	float flRadius = 50.0;
	PLAYBACK_EVENT_FULL( 0, NULL, g_usEventBoomerExplode, 0, vecOrigin, vecOrigin, flRadius, 0, 0, 0, FALSE, FALSE);

	RadiusDamage( vecOrigin, m_pentNpc, m_pentNpc, 60.0, flRadius, 0 );

	float flPoisonEndTime = m_pentNpc->v.fuser2 ;

	if( gpGlobals->time > flPoisonEndTime )
	{
		iNextState = STATE_DEAD;
		return THINK_CONTINUE;
	}
	else
	{
		m_pentNpc->v.nextthink = gpGlobals->time + BM_POISON_CYCLE;
		iNextState = STATE_BOOMER_POISON;
		return THINK_STOP;
	}
}
int CNpcBoomerZb::Dying(  int & iNextState  )
{
	m_pentNpc->v.effects |= EF_NODRAW;					//danger
	m_pentNpc->v.fuser2 = gpGlobals->time + BM_POISON_TIME;

	iNextState = STATE_BOOMER_POISON;

	return THINK_CONTINUE;
}
int CNpcBoomerZb::Idle( int & iNextState  )
{
	m_pentNpc->v.velocity = Vector( 0.0,  0.0,   0.0 );
	SetAnim( ANIM_BM_IDLE );

	iNextState = STATE_RETHINK;
	m_pentNpc->v.nextthink = gpGlobals->time + ANIM_BM_IDLE_TIME;
	
	return THINK_STOP;
}
int CNpcBoomerZb::ReThink( int & iNextState)
{
	if( m_pentNpc->v.health <= 0 ||  m_pentNpc->v.solid == SOLID_NOT )		// fix bug that the  dead npc  still can walk..
	{
		SetDead();
		return THINK_CONTINUE;
	}

	if( gpGlobals->time >  flNextFindEnemyTime   ||  !m_pentCurEnemy )
	{
		m_pentCurEnemy = FindNearestEnemy();
		if( !m_pentCurEnemy )
		{
			m_pentNpc->v.nextthink = gpGlobals->time + NPC_RELAX_TIME;
			iNextState = STATE_IDLE;
			
			return THINK_CONTINUE;
		}
		flNextFindEnemyTime = gpGlobals->time + NPC_FIND_ENEMY_TIME;
	}

	int iDistance = CalcDistance( m_pentCurEnemy->v.origin , m_pentNpc->v.origin  );
	if( iDistance > NPC_STOP_DISTANCE)
	{
		if( CanSeeEnemy( m_pentCurEnemy ) )
		{
			iNextState = STATE_IDLE;
			return THINK_CONTINUE;
		}
		else
		{
			g_NpcManager.KillNpc( ENTINDEX( m_pentNpc ) );
			return THINK_STOP;
		}
	}

	Vector vecAngles;
	SetTurnToEnemy(vecAngles);

	if( iDistance > ATTACK_RANGE )
	{
		iNextState = STATE_MOVE;
		return THINK_CONTINUE;
	}
	else
	{
		iNextState = STATE_ATTACK;
		return THINK_CONTINUE;
	}

}
int CNpcBoomerZb::Jump( int & iNextState )
{
	// jump
	Vector vecAngles;
	SetTurnToEnemy( vecAngles );
	SetAnim( ANIM_BM_JUMP );

	Vector vecVelocity = m_pentNpc->v.velocity ;

	vecVelocity.x *= 0.5;
	vecVelocity.y *= 0.5;
	vecVelocity.z = m_pentNpc->v.iJumpHeight;

	m_pentNpc->v.velocity = vecVelocity;
	m_pentNpc->v.nextthink = gpGlobals->time + 0.5;

	iNextState = STATE_RETHINK;
	return THINK_STOP;
}
int CNpcBoomerZb::Attack( int & iNextState  )
{
	Vector vecAngles;
	SetTurnToEnemy(vecAngles);

	int iDistance = CalcDistance( m_pentNpc->v.origin , m_pentCurEnemy->v.origin );

	if( iDistance < 0 )
	{
		iNextState = STATE_DEAD;
		return THINK_CONTINUE;
	}

	if( iDistance > ATTACK_RANGE)
	{
		m_iState = STATE_RETHINK;
		return THINK_CONTINUE;
	}
	else
	{
		if( m_pentCurEnemy->v.health - m_pentNpc->v.dmg  <= 0 )
		{
			gpGamedllFuncs->dllapi_table->pfnClientKill( m_pentCurEnemy );
		}
		else
		{
			PlayerTakeDamage( m_pentCurEnemy, m_pentNpc, m_pentNpc->v.dmg );
		}

		m_pentNpc->v.nextthink = gpGlobals->time + ANIM_BM_ATTACK_TIME;
		SetAnim( ANIM_BM_ATTACK );

		iNextState = STATE_ATTACK;
		return THINK_STOP;
	}
}
int CNpcBoomerZb::Move( int & iNextState )
{
	Vector vecAngles;
	SetTurnToEnemy( vecAngles );

	int iBlockerType, iWalkStatus;
	iWalkStatus = WALK_MOVE( m_pentNpc, vecAngles.y, 0.5, WALKMOVE_NORMAL );

	if( !iWalkStatus )
		iBlockerType = AssessBlocker( m_pentCurEnemy, m_pentNpc );
	else
		iBlockerType = BLOCKER_NONE;
	
	if( iBlockerType == BLOCKER_NONE || iBlockerType == BLOCKER_UNKNOWN )
	{
		MAKE_VECTORS( vecAngles );
		vecAngles = gpGlobals->v_forward ;
		Vector vecVelocity = vecAngles * m_pentNpc->v.maxspeed ;

		m_pentNpc->v.velocity = vecVelocity;
		m_pentNpc->v.nextthink = gpGlobals->time + ANIM_BM_WALK_TIME;

		SetAnim( ANIM_BM_WALK );

		iNextState = STATE_RETHINK;

		return THINK_STOP;
	}
	else if( iBlockerType == BLOCKER_CAN_JUMP)
	{
		iNextState = STATE_JUMP;
		return THINK_CONTINUE;
	}
	else if( iBlockerType == BLOCKER_BIG )
	{
		iNextState = STATE_IDLE;
		return THINK_CONTINUE;
	}
}
void CNpcBoomerZb::Think(void)
{
	int iThinkState = 1;

	while( iThinkState )
	{
		switch( m_iState )
		{
		case STATE_DEAD:
			iThinkState = Dead( m_iState );
			break;
		case STATE_DYING:
			iThinkState = Dying( m_iState );
			break;
		case STATE_JUMP:
			iThinkState = Jump( m_iState );
			break;
		case STATE_ATTACK:
			iThinkState = Attack( m_iState );
			break;
		case STATE_MOVE:
			iThinkState = Move( m_iState );
			break;
		case STATE_IDLE:
			iThinkState = Idle( m_iState );
			break;
		case STATE_RETHINK:
			iThinkState = ReThink( m_iState );
			break;
		case STATE_BOOMER_POISON:
			iThinkState = BoomerDmg( m_iState );
			break;
		}

		if( iThinkState == THINK_STOP)
			break;
	}
}
CNpcBoomerZb *CNpcBoomerZb::Create( int iMaxHealth, int iDamage , int iModexIndex , int iMaxSpeed , int iJumpHeight, char *szModel, Vector vecMins, Vector vecMaxs)
{
	CNpcBoomerZb *pNpcZombie = new CNpcBoomerZb;
	pNpcZombie->m_iNpcType = NPC_BOOMER;

	pNpcZombie->m_pentNpc = CREATE_NAMED_ENTITY( MAKE_STRING("info_target") );

	if( FNullEnt( pNpcZombie->m_pentNpc ) )
	{
		return pNpcZombie;
	}

	pNpcZombie->m_pentNpc->v.solid = SOLID_BBOX;
	pNpcZombie->m_pentNpc->v.movetype = MOVETYPE_STEP;
	//pNpcZombie->m_pentNpc->v.takedamage = DAMAGE_AIM;
	pNpcZombie->m_pentNpc->v.classname = MAKE_STRING( NPC_CLASSNAME );
	pNpcZombie->m_pentNpc->v.animtime = gpGlobals->time ;
	pNpcZombie->m_pentNpc->v.max_health = iMaxHealth;
	pNpcZombie->m_pentNpc->v.health = iMaxHealth;
	pNpcZombie->m_pentNpc->v.gravity = 1.0;
	pNpcZombie->m_pentNpc->v.dmg = iDamage;
	pNpcZombie->m_pentNpc->v.modelindex = iModexIndex;
	pNpcZombie->m_pentNpc->v.nextthink =  gpGlobals->time + 1.0;
	pNpcZombie->m_pentNpc->v.framerate = 1.0;
	pNpcZombie->m_pentNpc->v.frame = 0;
	pNpcZombie->m_pentNpc->v.maxspeed = iMaxSpeed;
	pNpcZombie->m_pentNpc->v.iJumpHeight = iJumpHeight;

	SET_MODEL( pNpcZombie->m_pentNpc, szModel );
	SET_SIZE( pNpcZombie->m_pentNpc , vecMins, vecMaxs );

	pNpcZombie->m_pentNpc->v.mins = vecMins ;
	pNpcZombie->m_pentNpc->v.maxs = vecMaxs;

	return pNpcZombie;
}

// ----------------------------------------------------------------------Npc Manager
CNpcManager::CNpcManager()
{
	m_pNpc = NULL;
	m_bCanStartGame = false;
	m_iSpanwPointNum = 0;
	m_iCurrentNpcNum = 0;
}
void CNpcManager::Think( edict_t *pent )
{
	CNpcBase *pointer;
	pointer = m_pNpc;

	while( pointer )
	{
		if( pointer->m_pentNpc == pent)
		{
			pointer->Think();
			break;
		}
		
		pointer = pointer->m_pNext ;
	}
}
bool CNpcManager:: AddNpc( CNpcBase *pNpc )
{
	if( IsNpcOverflow() )
		return false;
	

	if( !m_pNpc )
	{
		m_pNpc = pNpc;
	}
	else
	{
		CNpcBase *pointer = m_pNpc;

		while( pointer->m_pNext )
			pointer = pointer->m_pNext;

		pointer->m_pNext = pNpc;
	}

	int iNearest = 99999;
	int iPoint;

	for( int i =0; i< m_iSpanwPointNum ; i ++)
	{
		if( !IsHullVacant( m_vecSpawnPoint[i] ) )
			continue;

		int iLength = GetNearestPlayerDistance( m_vecSpawnPoint[i] );
		if( iLength < iNearest)
		{
			iNearest = iLength;
			iPoint = i;
		}
	}

	pNpc->Spawn( m_vecSpawnPoint[ iPoint ] );

	m_iCurrentNpcNum ++;

	return true;

}
int CNpcManager::GetNearestPlayerDistance( Vector vecOrigin )
{
	double iNearest = 99999;

	for(int i = 1; i <= gpGlobals->maxClients ; i++)
	{
		edict_t *pentPlayer = NULL;
		pentPlayer = INDEXENT(i);

		if( !IsValidPlayer( pentPlayer ) )
			continue;

		Vector vecDestOrigin = pentPlayer->v.origin ;

		int iLength;
		iLength = CalcDistance( vecOrigin, vecDestOrigin );

		if( iLength < iNearest)
			iNearest = iLength;
	}

	return (int)sqrt( iNearest );
}

bool CNpcManager::IsHullVacant( Vector vecOrigin )
{
	edict_t *pentEntity = NULL;
	pentEntity = FIND_ENTITY_IN_SPHERE( pentEntity , vecOrigin, 100.0 );

	if( !pentEntity )
		return true;

	if( FNullEnt( pentEntity ) )
		return true;
	

	return false;
}

bool CNpcManager::KillAllNpc( bool bEndRound  )
{
	m_iCurrentNpcNum = 0;

	if( !m_pNpc )
		return false;

	CNpcBase *pointer1 = m_pNpc;
	CNpcBase *pointer2 = NULL;

	while( pointer1 )
	{
		pointer2 = pointer1->m_pNext ;
		if( bEndRound )
			//delete pointer1;
			pointer1->SetDead();				// fix bugs
		else
			delete pointer1;

		pointer1 = pointer2;
	}

	return true;
}
void CNpcManager::NpcSetDead( int index )
{
	if( !m_pNpc )
		return;

	edict_t *pentNpc = INDEXENT( index );

	CNpcBase *pointer;
	pointer = m_pNpc;

	if( !pointer)
		return;

	while( pointer && pointer->m_pentNpc != pentNpc )
		pointer = pointer->m_pNext ;

	if( !pointer )
		return;
	else if( !(pointer->m_iState & STATE_DEAD) && !(pointer->m_iState & STATE_DYING)) // fix bug
	{
		pointer->SetDead();
		UpdateClientData();
	}
}
void CNpcManager::UpdateClientData(void)
{
}
void CNpcManager::KillNpc( int index )
{
	if( !m_pNpc )
		return;

	m_iCurrentNpcNum --;
	if( m_iCurrentNpcNum < 0)
		m_iCurrentNpcNum = 0;

	edict_t *pentNpc = INDEXENT( index );

	CNpcBase *pointer1;
	CNpcBase *pointer2;
	
	if( m_pNpc->m_pentNpc == pentNpc )
	{
		pointer1 = m_pNpc->m_pNext ;
		delete m_pNpc;
		m_pNpc = pointer1;
	}
	else
	{
		pointer1 = m_pNpc;

		while( pointer1->m_pNext && pointer1->m_pNext->m_pentNpc != pentNpc )
			pointer1 = pointer1->m_pNext ;

		if( !pointer1->m_pNext )
			return;
		else
		{
			pointer2 = pointer1->m_pNext->m_pNext ;
			delete pointer1->m_pNext ;
			pointer1->m_pNext = pointer2;

			return;
		}
	}
}
bool CNpcManager::IsNpcOverflow(void)
{
	if( m_iCurrentNpcNum >= MAX_NPC - 3  )				// to fix bugs
		return true;
	else
		return false;
}
void CNpcManager::CreateNpc( int iType, int iMaxHealth, int iDamage, int iMaxSpeed, int iJumpHeight, int iModelIndex, char *pszModelName, Vector vecMins, Vector vecMaxs, float flSkillTime )
{
	if( !m_bCanStartGame )
		return;

	if( IsNpcOverflow() )
		return;
	
	switch( iType )
	{
	case NPC_NORMALZOMBIE:
		{
			CNpcNormalZombie *pNpc = CNpcNormalZombie::Create( iMaxHealth, iDamage, iModelIndex, iMaxSpeed, iJumpHeight, pszModelName, vecMins, vecMaxs);
			if( FNullEnt( pNpc->m_pentNpc)  || !AddNpc( pNpc ) )
			{
				delete pNpc;
			}
			break;
		}
	case NPC_INVISIBLE:
		{
			CNpcInvisibleZb *pNpc = CNpcInvisibleZb::Create( iMaxHealth, iDamage, iModelIndex, iMaxSpeed, flSkillTime , iJumpHeight, pszModelName, vecMins, vecMaxs);
			if( FNullEnt( pNpc->m_pentNpc)  || !AddNpc( pNpc ) )
			{
				delete pNpc;
			}
			break;
		}
	case NPC_BOOMER:
		{
			CNpcBoomerZb *pNpc = CNpcBoomerZb::Create( iMaxHealth, iDamage, iModelIndex, iMaxSpeed, iJumpHeight , pszModelName, vecMins, vecMaxs);
			if( FNullEnt( pNpc->m_pentNpc)  || !AddNpc( pNpc ) )
			{
				delete pNpc;
			}
			break;
		}
	}
	
}
void CNpcManager::LoadPointData(void)
{
	char szAddr[128];
	sprintf( szAddr , "cstrike\\addons\\amxmodx\\configs\\survivor\\spawn_points\\%s.cfg", STRING( gpGlobals->mapname));

	FILE *pFile = fopen( szAddr, "rt" );

	if( !pFile )
	{
		m_bCanStartGame = false;
		return;
	}
	else
	{
		m_bCanStartGame = true;
	}

	char szLineData[128];

	while( !feof( pFile ) )
	{
		fgets( szLineData, sizeof( szLineData ) -1, pFile );

		if( !szLineData[0] || szLineData[0] == ';' || szLineData[0] == '\n')
			continue;

		if( szLineData[ strlen( szLineData) - 1] == '\n' )
			szLineData[ strlen( szLineData) - 1] = '\0';
		

		char szTemp[32];
		int id = 0;
		int iType = 0;
		int iData;

		for( int i=0; i< strlen( szLineData) + 2; i++ )
		{

			if( szLineData[i] == ' ' || szLineData[ strlen( szLineData) - 1] == '\0')
			{
				szTemp[ id ] = '\0';

				iData = atoi( szTemp );
				iType ++;

				switch( iType )
				{
				case 1:
					m_vecSpawnPoint[ m_iSpanwPointNum ].x = iData;
					break;
				case 2:
					iType = 0;
					m_vecSpawnPoint[ m_iSpanwPointNum ].y = iData;
					break;
				}

				id = 0;
				memset( szTemp , 0, sizeof( szTemp ));
			}

			szTemp[id] = szLineData[i];
			id ++;
		}

		iData = atoi( szTemp );
		m_vecSpawnPoint[ m_iSpanwPointNum ].z = iData;

		m_iSpanwPointNum ++;
	}

	fclose( pFile );
}