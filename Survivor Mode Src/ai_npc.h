#ifndef _AI_NPC_H
#define _AI_NPC_H

#define MAX_NPC							80
#define NPC_CLASSNAME					"AiNpc"

enum NpcType_e
{
	NPC_NORMALZOMBIE = 1,
	NPC_INVISIBLE,
	NPC_BOOMER,
};


#define NPC_CORPSE_STAY_TIME			2.0
#define NPC_FIND_ENEMY_TIME				2.0
#define NPC_STOP_DISTANCE				1000
#define NPC_RELAX_TIME					3.0

#define flSkillTime						fuser1
#define iJumpHeight						iuser1

enum ThinkState_e
{
	THINK_STOP = 1,
	THINK_CONTINUE,
};

enum NpcState_e
{
	STATE_DEAD = 1,
	STATE_DYING,
	STATE_JUMP,
	STATE_ATTACK,
	STATE_MOVE,
	STATE_IDLE,
	STATE_RETHINK,
	STATE_BOOMER_POISON,
};

enum SkillStatus_e
{
	SKILL_STATUS_COOLDOWN = 1,
	SKILL_STATUS_INSKILL,
	SKILL_STATUS_NONE,
	
};

inline bool CheckDead( int iState ) { return (iState == STATE_DEAD) || (iState == STATE_DYING);  }

//-------------------------------------------------  Npc Base
class CNpcBase
{
public:
	CNpcBase();
	virtual ~CNpcBase();

public:
	virtual void Think( void ){}
	virtual void Spawn(  Vector vecOrigin );
	virtual void Kill( void );
	virtual void SetDead( void ){}
	
public:
	virtual int Move( int & iNextState ){ return 0; }
	virtual int Jump( int & iNextState ){ return 0; }
	virtual int Attack( int & iNextState ){ return 0; }
	virtual int Dead( int & iNextState ){ return 0; }
	virtual int Dying( int & iNextState ){ return 0; }
	virtual int Idle( int & iNextState ){ return 0; }
	virtual int ReThink( int & iNextState){ return 0; }

public:
	virtual void SpanwBlood(void){}
	virtual void SetAnim( int iAnim );
	virtual void SetTurnToEnemy( Vector & vecAngles );
	virtual edict_t *FindNearestEnemy();
	virtual bool CanSeeEnemy( edict_t *pentTarget );
	
public:
	int m_iState;
	int m_iNpcType;
	float flNextFindEnemyTime;
	edict_t *m_pentCurEnemy;
	edict_t *m_pentNpc;
	CNpcBase *m_pNext;
};

//------------------------------------------------- Normal zombie Npc 

#define ATTACK_RANGE			50

#define ANIM_NZ_IDLE_TIME		2.0
#define ANIM_NZ_ATTACK_TIME		1.35
#define ANIM_NZ_WALK_TIME		0.5
#define ANIM_NZ_DEAD_TIME		1.1

enum NormalZombieAnim_e
{
	ANIM_NZ_ATTACK = 15 ,
	ANIM_NZ_WALK = 4,
	ANIM_NZ_JUMP = 6,
	ANIM_NZ_IDLE = 7,
	ANIM_NZ_DEAD = 2,
};

class CNpcNormalZombie : public CNpcBase
{
public:
	CNpcNormalZombie();
	virtual ~CNpcNormalZombie();
	
private:
	virtual int Move( int & iNextState );
	virtual int Jump( int & iNextState );
	virtual int Attack( int & iNextState );
	virtual int Dead( int & iNextState );
	virtual int Dying( int & iNextState );
	virtual int Idle( int & iNextState );
	virtual int ReThink( int & iNextState);

public:
	virtual void Think(void);
	virtual void SetDead( void );

public:
	static CNpcNormalZombie * Create(  int iMaxHealth, int iDamage , int iModexIndex  , int iMaxSpeed , int iJumpHeight, char *szModel, Vector vecMins, Vector vecMaxs);
};

//------------------------------------------------- Invisible zombie Npc 

#define IZ_ATTACK_RANGE			50

#define SKILL_COOLDOWN_TIME		5.0

#define ANIM_IZ_IDLE_TIME		2.0
#define ANIM_IZ_ATTACK_TIME		1.35
#define ANIM_IZ_WALK_TIME		0.3
#define ANIM_IZ_DEAD_TIME		1.1

enum InvisibleZombieAnim_e
{
	ANIM_IZ_ATTACK = 15 ,
	ANIM_IZ_WALK = 4,
	ANIM_IZ_JUMP = 6,
	ANIM_IZ_IDLE = 7,
	ANIM_IZ_DEAD = 2,
};

class CNpcInvisibleZb : public CNpcBase
{
public:
	CNpcInvisibleZb();
	virtual ~CNpcInvisibleZb();
	
private:
	virtual int Move( int & iNextState );
	virtual int Jump( int & iNextState );
	virtual int Attack( int & iNextState );
	virtual int Dead( int & iNextState );
	virtual int Dying( int & iNextState );
	virtual int Idle( int & iNextState );
	virtual int ReThink( int & iNextState);

public:
	virtual void Think(void);
	virtual void SetDead( void );
private:
	float m_flSkillEndTime;
public:
	static CNpcInvisibleZb * Create(  int iMaxHealth, int iDamage , int iModexIndex  , int iMaxSpeed , float flSkillTime, int iJumpHeight, char *szModel, Vector vecMins, Vector vecMaxs);
};
//------------------------------------------------- Boomer Zombie Npc 

#define BM_ATTACK_RANGE			50

#define BM_POISON_TIME			2.0

#define ANIM_BM_IDLE_TIME		2.0
#define ANIM_BM_ATTACK_TIME		1.35
#define ANIM_BM_WALK_TIME		0.3
#define ANIM_BM_DEAD_TIME		0.7

#define BM_POISON_CYCLE			0.8

enum BoomerZombieAnim_e
{
	ANIM_BM_ATTACK = 76,
	ANIM_BM_WALK = 3,
	ANIM_BM_JUMP = 7,
	ANIM_BM_IDLE = 1,
	ANIM_BM_DEAD = 114,
};

class CNpcBoomerZb : public CNpcBase
{
public:
	CNpcBoomerZb();
	virtual ~CNpcBoomerZb();
	
private:
	virtual int Move( int & iNextState );
	virtual int Jump( int & iNextState );
	virtual int Attack( int & iNextState );
	virtual int Dead( int & iNextState );
	virtual int Dying( int & iNextState );
	virtual int Idle( int & iNextState );
	virtual int ReThink( int & iNextState);
	virtual int BoomerDmg( int & iNextState );

public:
	virtual void Think(void);
	virtual void SetDead( void );

public:
	static CNpcBoomerZb * Create(  int iMaxHealth, int iDamage , int iModexIndex  , int iMaxSpeed , int iJumpHeight, char *szModel, Vector vecMins, Vector vecMaxs);
};

//-------------------------------------------------  Npc Manager

#define MAX_SPAWN				200

class CNpcManager
{
public:
	CNpcManager();
public:
	void Think(  edict_t *pent  );
	bool AddNpc( CNpcBase *pNpc );
	void KillNpc( int index );
	bool KillAllNpc( bool bEndRound  );
	void NpcSetDead( int index );

public:
	void CreateNpc( int iType, int iMaxHealth, int iDamage, int iMaxSpeed, int iJumpHeight, int iModelIndex, char *pszModelName, Vector vecMins, Vector vecMaxs, float flSkillTime );
	void LoadPointData(void);

private:
	bool IsNpcOverflow(void);
	bool IsHullVacant( Vector vecOrigin );
	int GetNearestPlayerDistance( Vector vecOrigin );
	void UpdateClientData(void);

private:
	CNpcBase *m_pNpc;
	int m_iCurrentNpcNum;
	bool m_bCanStartGame;
	Vector m_vecSpawnPoint[ MAX_SPAWN ];
	int m_iSpanwPointNum;

};

extern CNpcManager g_NpcManager;


#endif