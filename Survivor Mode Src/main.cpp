#include "amxxmodule.h"
#include "main.h"
#include "ai_npc.h"
#include "util.h"

int gmsgMetaHook = 0;

extern DLL_FUNCTIONS *g_pFunctionTable;

short g_sModelIndexLaser = 0;
short g_sModelIndexBoomerSpr = 0;
short g_sModelIndexBoomerExplodeSpr = 0;

unsigned short g_usEventBoomerExplode = 0;

void Think(edict_t *pent);

int FN_RegUserMsg_Post(const char *pszName, int iSize)
{
	if( !strcmp( pszName, "MetaHook") )
		gmsgMetaHook = META_RESULT_ORIG_RET(int);

	RETURN_META_VALUE(MRES_IGNORED, 0);
}

static cell AMX_NATIVE_CALL Suv_CreateNpc(AMX *amx, cell *params)
{
	int iType = params[1];
	int iMaxHealth = params[2];
	int iDamage = params[3];
	int iMaxSpeed = params[4];
	int iJumpHeight = params[5];
	int iModelIndex = params[6];

	int iLen;
	char* szModelName = MF_GetAmxString(amx, params[7], 0, &iLen);

	cell *cAddr;
	Vector vecMins, vecMaxs;

	cAddr = MF_GetAmxAddr(amx,params[8]);
	vecMins.x =amx_ctof(cAddr[0]);
	vecMins.y =amx_ctof(cAddr[1]);
	vecMins.z =amx_ctof(cAddr[2]);

	cAddr = MF_GetAmxAddr(amx,params[9]);
	vecMaxs.x =amx_ctof(cAddr[0]);
	vecMaxs.y =amx_ctof(cAddr[1]);
	vecMaxs.z =amx_ctof(cAddr[2]);

	REAL fSkillTime = amx_ctof(params[10]);

	g_NpcManager.CreateNpc( iType, iMaxHealth, iDamage, iMaxSpeed, iJumpHeight, iModelIndex, szModelName, vecMins, vecMaxs,  fSkillTime );

	return 1;
}
static cell AMX_NATIVE_CALL Suv_SetRoundEnd(AMX *amx, cell *params)
{
	g_NpcManager.KillAllNpc( true );

	return 1;
}
static cell AMX_NATIVE_CALL Suv_SetNpcDead(AMX *amx, cell *params)
{
	int iEnt = params[1];
	g_NpcManager.NpcSetDead( iEnt );

	return 1;
}
AMX_NATIVE_INFO RegisterNative[] =
{
	{ "Suv_CreateNpc", Suv_CreateNpc },
	{ "Suv_SetRoundEnd" , Suv_SetRoundEnd },
	{ "Suv_SetNpcDead" , Suv_SetNpcDead },
	{ NULL, NULL }
};
void OnAmxxAttach()
{
	FILE *fp;
	fp = fopen(LOG_FILE_ADR, "rb");
	if(fp)
	{
		fclose(fp);
		char filepath[256];
		::GetCurrentDirectory(255,filepath);
		sprintf(filepath,"%s/%s",filepath, LOG_FILE_ADR);
		DeleteFile(filepath);
		LogToFile("<The old content has been cleared>");
	}

	LogToFile("Attaching....");

		MF_AddNatives(RegisterNative);

	LogToFile( "Attached Successfully");
}

void OnPluginsLoaded()
{
	g_NpcManager.LoadPointData();
	g_pFunctionTable->pfnThink = Think;

	g_sModelIndexLaser = PRECACHE_MODEL ("sprites/laserbeam.spr");
	g_sModelIndexBoomerSpr = PRECACHE_MODEL( "sprites/Survivor/spr_boomer.spr" );
	g_sModelIndexBoomerExplodeSpr = PRECACHE_MODEL( "sprites/Survivor/zombiebomb_exp.spr" );

	g_usEventBoomerExplode = PRECACHE_EVENT(1, "events/boomer_explode.sc");
}


// -------------------------

void Think( edict_t *pent)
{
	if( strcmp( STRING( pent->v.classname ) ,  NPC_CLASSNAME ) )
		return;

	g_NpcManager.Think( pent );
}