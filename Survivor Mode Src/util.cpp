#include "amxxmodule.h"
#include "util.h"

extern short g_sModelIndexLaser;

int CalcDistance( Vector vecSrc, Vector vecDest )
{
	double iLength = (vecSrc.x - vecDest.x )*(vecSrc.x - vecDest.x ) + (vecSrc.y - vecDest.y)*(vecSrc.y - vecDest.y) + (vecSrc.z - vecDest.z ) * (vecSrc.z - vecDest.z );

	return  (int)sqrt( iLength );;
}

bool IsValidPlayer (edict_t *pent)
{
   if (FNullEnt (pent))
      return false;

   if(  ( pent->v.flags & FL_CLIENT)  ||  ( pent->v.flags & FL_FAKECLIENT) )
	   return true;

   return false;
}

void DrawLine (edict_t *pentEdict, Vector start, Vector end, int width, int noise, int red, int green, int blue, int brightness, int speed, int life)
{
   // this function draws a line visible from the client side of the player whose player entity
   // is pointed to by pentEdict, from the vector location start to the vector location end,
   // which is supposed to last life tenths seconds, and having the color defined by RGB.

  if (!IsValidPlayer (pentEdict))
      return; // reliability check

   MESSAGE_BEGIN ( MSG_ONE, SVC_TEMPENTITY, NULL, pentEdict);
      WRITE_BYTE (TE_BEAMPOINTS);
      WRITE_COORD (start.x);
      WRITE_COORD (start.y);
      WRITE_COORD (start.z);
      WRITE_COORD (end.x);
      WRITE_COORD (end.y);
      WRITE_COORD (end.z);
      WRITE_SHORT (g_sModelIndexLaser);
      WRITE_BYTE (0); // framestart
      WRITE_BYTE (10); // framerate
      WRITE_BYTE (life); // life in 0.1's
      WRITE_BYTE (width); // width
      WRITE_BYTE (noise);  // noise

      WRITE_BYTE (red);   // r, g, b
      WRITE_BYTE (green);   // r, g, b
      WRITE_BYTE (blue);   // r, g, b

      WRITE_BYTE (brightness);   // brightness
      WRITE_BYTE (speed);    // speed
   MESSAGE_END ();
}


int AssessBlocker( edict_t *pentTarget, edict_t *pentEnt )
{
	int iLength1, iLength2;
	Vector vecPoint , vecVector, vecOrigin;

	pentEnt->v.v_angle = pentEnt->v.angles ;
	velocity_by_aim( pentEnt, 50, vecVector );

	vecOrigin = pentEnt->v.origin ;
	vecOrigin.z -= pentEnt->v.size.z / 2;
	vecPoint = vecOrigin + vecVector;

	Vector vecEntEye;
	vecEntEye = pentEnt->v.origin;
	vecEntEye.z += pentEnt->v.size.z / 2;

	//DrawLine ( pentTarget , vecEntEye, vecPoint, 15, 0, 255, 160, 160, 250, 0, 10);

	TraceResult trResult;
	TRACE_LINE( vecEntEye, vecPoint, dont_ignore_monsters, pentEnt, &trResult );
	iLength1= CalcDistance( trResult.vecEndPos , vecEntEye );
	iLength2 = CalcDistance( vecEntEye, vecPoint );

	if( iLength1 >= iLength2 )
	{
		return BLOCKER_NONE;
	}
	else if( iLength1 > iLength2 * 0.95 )
	{
		return BLOCKER_CAN_JUMP;
	}
	else if( iLength1 <= iLength2 * 0.95 )
	{
		
		vecPoint.z += pentEnt->v.size.z + 100.0;
		TRACE_LINE( vecEntEye , vecPoint, dont_ignore_monsters, pentEnt, &trResult );

		iLength1 = CalcDistance( trResult.vecEndPos , pentEnt->v.origin );
		iLength2 = CalcDistance( vecPoint , pentEnt->v.origin );

		if( iLength1 != iLength2 )
			return BLOCKER_BIG;
		else
			return BLOCKER_CAN_JUMP;
	}

	return BLOCKER_UNKNOWN;
}

int LogToFile(char *szLogText, ...)
{
	FILE *fp;

	if (!(fp = fopen( LOG_FILE_ADR , "a")))
		return 0;
	
	va_list vArgptr;
	char szText[1024];

	va_start(vArgptr, szLogText);
	vsprintf(szText, szLogText, vArgptr);
	va_end(vArgptr);

	SYSTEMTIME systime;
	::GetLocalTime(&systime);


	fprintf(fp, "[%02d:%02d:%02d.%03d] %s\n", systime.wHour,systime.wMinute,systime.wSecond,systime.wMilliseconds,szText);
	fclose(fp);
	return 1;
}

void velocity_by_aim( edict_t *pEnt, int iVelLength, Vector &vecRet )
{
	MAKE_VECTORS(pEnt->v.v_angle);
	vecRet = gpGlobals->v_forward * iVelLength;
}
void SetRendering( edict_t *pent, int fx, Vector vecColor, int render , int amount)
{
	pent->v.renderfx = fx;
	pent->v.rendercolor = vecColor;
	pent->v.rendermode = render;
	pent->v.renderamt = amount;
}
void RadiusDamage(Vector vecSrc, edict_t *pentInflictor, edict_t *pentAttacker, float flDamage, float flRadius, int iClassIgnore )
{
	int falloff = flDamage / flRadius;
	edict_t *pent = NULL;

	while( (pent = FIND_ENTITY_IN_SPHERE( pent, vecSrc, flRadius)) != NULL )
	{
		if( !FNullEnt( pent ) )
			break;

		if( pent->v.takedamage == DAMAGE_NO )
			continue;

		if( !IsValidPlayer( pent ) )						// Only damage player
			continue;

		float flAdjustedDamage;
		flAdjustedDamage = (vecSrc - pent->v.origin).Length() * falloff;
		flAdjustedDamage = flDamage - flAdjustedDamage;

		if (flAdjustedDamage < 0)
			flAdjustedDamage = 0;
		
		if( pent->v.health - flAdjustedDamage  <= 0 )
		{
			gpGamedllFuncs->dllapi_table->pfnClientKill( pent );
		}
		else
		{
			PlayerTakeDamage( pent, pentInflictor, flAdjustedDamage );
		}
	}

}

void PlayerTakeDamage( edict_t *pent , edict_t *pentInflictor, float flDamage )
{
	if( FNullEnt( pent)  ||  pent->v.takedamage == DAMAGE_NO )
		return;
	
	pent->v.health = pent->v.health - flDamage;
	pent->v.dmg_inflictor = pentInflictor;
	pent->v.dmg_take = flDamage;
}