#ifndef _UTIL_H_
#define _UTIL_H_

#define HULL_HUMAN                      1
#define LOG_FILE_ADR					"Survivor.log"

enum BlockerType_e
{
	BLOCKER_NONE = 1,
	BLOCKER_CAN_JUMP, 
	BLOCKER_BIG,
	BLOCKER_UNKNOWN,
};


int CalcDistance( Vector vecSrc, Vector vecDest );
int LogToFile(char *szLogText, ...);
void velocity_by_aim( edict_t *pEnt, int iVelLength, Vector &vecRet );
int AssessBlocker( edict_t *pentTarget, edict_t *pentEnt );
bool IsValidPlayer (edict_t *pent);
void SetRendering( edict_t *pent, int iRenderfx = kRenderFxNone, Vector vecColor = Vector( 255, 255, 255 ), int iRenderMode = kRenderNormal, int iAmount = 16);
void RadiusDamage(Vector vecSrc, edict_t *pentInflictor, edict_t *pentAttacker, float flDamage, float flRadius, int iClassIgnore );
void PlayerTakeDamage( edict_t *pent , edict_t *pentInflictor, float flDamage );

#endif