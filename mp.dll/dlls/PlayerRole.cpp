#include "extdll.h"
#include "PlayerRole.h"

PlayerRoleInfo_t g_PlayerRoleInfo[ MAX_PLAYER_ROLE ];
int g_iPlayerRoleNum;

void ReadPlayerRoleInfo(void)
{
	memset( g_PlayerRoleInfo, 0, sizeof( g_PlayerRoleInfo ));

	FILE* pFile;
	pFile = fopen("cstrike\\character_data\\PlayerInUse.lst", "rt");

	if(!pFile)
		return;

	char szBuff[32];
	char szAddr[256];
	FILE *pFileItem = NULL;

	int i = 0;
	while(!feof(pFile))
	{
		fgets(szBuff, sizeof(szBuff)-1, pFile);
		if(szBuff[0]==';' || !szBuff[0]  || szBuff[ 0] == '\n')
			continue;
		if(szBuff[ strlen(szBuff)-1 ] == '\n')
			szBuff[ strlen(szBuff)-1 ] = '\0';

		sprintf(szAddr, "cstrike\\character_data\\%s.data", szBuff);
		
		pFileItem = fopen(szAddr, "rt");
		if(!pFileItem)
			continue;

		// read weapon item
		g_PlayerRoleInfo[i].szName  = (char *)malloc( sizeof(char) * 32);
		sprintf(g_PlayerRoleInfo[i].szName ,szBuff);

		g_PlayerRoleInfo[i].szModel = (char *)malloc( sizeof(char) * 128 );
		sprintf(g_PlayerRoleInfo[i].szModel , "models/player/%s/%s.mdl", szBuff, szBuff);

		char szBuff2[256], szTemp[256];
		while(!feof(pFileItem))
		{
			fgets(szBuff2, sizeof(szBuff2)-1, pFileItem);
			if(szBuff2[0]==';' || !szBuff2[0] || szBuff2[ 0] == '\n')
				continue;
			if(szBuff2[ strlen(szBuff2)-1 ] == '\n')
				szBuff2[ strlen(szBuff2)-1 ] = '\0';

			int iii = 0, iDataType = 0;
			for(int ii=0;ii<strlen(szBuff2);ii++)
			{
				if(szBuff2[ii] == '=')
				{
					szTemp[iii] = '\0';

					if(!strcmp(szTemp, "TEAM"))
						iDataType = DATA_TEAM;
					else if( !strcmp( szTemp, "SPEED") )
						iDataType = DATA_SPEED;
					else if( !strcmp( szTemp, "WPN") )
						iDataType = DATA_WPN;
					else if( !strcmp( szTemp, "HEALTH") )
						iDataType = DATA_HEALTH;
					else if( !strcmp( szTemp, "ARMOR"))
						iDataType = DATA_ARMOR;

					iii = 0;
					memset(szTemp,0,sizeof(szTemp));
					continue;
				}

				szTemp[iii] = szBuff2[ii];
				iii ++;
			}
			switch(iDataType)
			{
			case DATA_TEAM:
				{
					g_PlayerRoleInfo[i].iTeam = atoi(szTemp);
					break;
				}
			case DATA_SPEED:
				{
					g_PlayerRoleInfo[i].iSpeed = atoi(szTemp);
					break;
				}
			case DATA_WPN:
				{
					int aa = 0, aaa = 0;
					int iItems[8];
					char szBuff[16];

					for(int a=0; a < strlen(szTemp); a++)
					{
						if(szTemp[a] == ',')
						{
							szBuff[aa] = '\0';
							g_PlayerRoleInfo[i].szWpn[aaa] = (char *)malloc( sizeof(char)  * 64);
							sprintf( g_PlayerRoleInfo[i].szWpn[aaa], szBuff );
							
							aaa++;
							aa=0;
							continue;
						}
						szBuff[aa] = szTemp[a];
						aa ++;
					}
					break;
				}
			case DATA_HEALTH:
				{
					g_PlayerRoleInfo[i].iHealth = atoi( szTemp);
					break;
				}
			case DATA_ARMOR:
				{
					g_PlayerRoleInfo[i].iArmor = atoi( szTemp);
					break;
				}
			
			}
			memset(szTemp, 0, sizeof(szTemp));
			memset(szBuff2, 0, sizeof(szBuff2));

		}
		fclose(pFileItem);

		i++;
	}
	fclose(pFile);

	g_iPlayerRoleNum = i;
}