#ifndef _PLAYERROLE_H
#define _PLAYERROLE_H

#define MAX_PLAYER_ROLE 50

enum 
{
	DATA_TEAM = 1,
	DATA_SPEED,
	DATA_WPN,
	DATA_HEALTH,
	DATA_ARMOR,
};

struct PlayerRoleInfo_s
{
	char *szName;
	char *szModel;
	short iTeam;
	int iSpeed;
	char *szWpn[8];
	int iHealth;
	int iArmor;
};

typedef PlayerRoleInfo_s PlayerRoleInfo_t;


extern PlayerRoleInfo_t g_PlayerRoleInfo[ MAX_PLAYER_ROLE ];
extern int g_iPlayerRoleNum;

void ReadPlayerRoleInfo(void);

#endif