
#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "monsters.h"
#include "weapons.h"
#include "nodes.h"
#include "soundent.h"
#include "decals.h"
#include "player.h"
#include "gamerules.h"
#include "hltv.h"
#include "rocket.h"

LINK_ENTITY_TO_CLASS(rocket, CRocket);


void fm_get_aim_origin(entvars_t *pev, Vector &vecReturn)
{
	Vector vecStart;
	vecStart = pev->origin + pev->view_ofs;

	Vector vecDest;
	vecDest = pev->v_angle;

	UTIL_MakeVectors(vecDest);
	vecDest = gpGlobals->v_forward ;

	vecDest = vecDest * 9999;
	vecDest = vecDest + vecStart;
	
	TraceResult tr;
	UTIL_TraceLine(vecStart, vecDest, dont_ignore_monsters, ENT(pev), &tr);

	vecReturn = tr.vecEndPos ;
	
}

void CRocket::GrenadeThink(void)
{
	int iWpnId = pev->iuser4 ;
	float flExplodeTime = pev->fuser2 ;

	if( g_WpnItemInfo[ iWpnId ].flAttackDelay1 && gpGlobals->time >= flExplodeTime )
	{
		Explode();
		return;
	}

	pev->nextthink = gpGlobals->time + 0.01;
	SetThink(&CRocket::GrenadeThink);

	Vector vecOrigin, vecAngle;

	vecOrigin = pev->origin ;
	vecAngle = pev->v_angle ;

	Vector vecAimOrigin, vecVelocity;

#define FLY_OFFSET		0.5

	vecAngle.x += RANDOM_FLOAT( - FLY_OFFSET, FLY_OFFSET );
	vecAngle.y += RANDOM_FLOAT(- FLY_OFFSET, FLY_OFFSET);
	vecAngle.z += RANDOM_FLOAT(- FLY_OFFSET, FLY_OFFSET);

	fm_get_aim_origin(pev, vecAimOrigin);

	get_speed_vector(vecOrigin, vecAimOrigin, 1000, vecVelocity);
	pev->velocity = vecVelocity;
}
void CRocket::GrenadeTouch( CBaseEntity *pOther )
{
	if( pOther->edict() == pev->owner )
		return;

	int iWpnId = pev->iuser4;

	if (UTIL_PointContents(pev->origin) == CONTENTS_SKY)
	{
		UTIL_Remove(this);
		return;
	}

	if( g_WpnItemInfo[ iWpnId ].flAttackDelay1 )
	{
		pev->movetype = MOVETYPE_NONE;
		pev->fuser2 = gpGlobals->time + g_WpnItemInfo[ iWpnId ].flAttackDelay1;
		pev->velocity = g_vecZero;

		SetTouch(NULL);

		pev->nextthink = gpGlobals->time + g_WpnItemInfo[ iWpnId ].flAttackDelay1 + 0.1;
		SetThink(&CRocket::GrenadeThink);
	}
	else
		Explode();
	
}

void CRocket::Explode(void)
{
	pev->model = 0;
	//pev->solid = SOLID_NOT;
	pev->takedamage = DAMAGE_NO;

	int iWpnId = pev->iuser4 ;

	PLAYBACK_EVENT_FULL( 0, ENT(pev), m_usEvent, 0, pev->origin, (float *)&g_vecZero, 0, 0, 0, 0, FALSE, FALSE);

	CSoundEnt::InsertSound(bits_SOUND_COMBAT, pev->origin, NORMAL_EXPLOSION_VOLUME, 3);
	entvars_t *pevOwner;

	if (pev->owner)
		pevOwner = VARS(pev->owner);
	else
		pevOwner = NULL;

	//pev->owner = NULL;

	::RadiusDamage( pev->origin , pev, pevOwner, g_WpnItemInfo[iWpnId].iBulletDamage , g_WpnItemInfo[iWpnId].iRadius , CLASS_NONE, DMG_GENERIC);

	float flRndSound = RANDOM_FLOAT(0, 1);

	switch (RANDOM_LONG(0, 2))
	{
		case 0: EMIT_SOUND(ENT(pev), CHAN_VOICE, "weapons/debris1.wav", 0.55, ATTN_NORM); break;
		case 1: EMIT_SOUND(ENT(pev), CHAN_VOICE, "weapons/debris2.wav", 0.55, ATTN_NORM); break;
		case 2: EMIT_SOUND(ENT(pev), CHAN_VOICE, "weapons/debris3.wav", 0.55, ATTN_NORM); break;
	}

	pev->effects |= EF_NODRAW;
	pev->velocity = g_vecZero;

	UTIL_Remove(this);
}

void CRocket::Spawn(void)
{
	if (pev->classname)
		RemoveEntityHashValue(pev, STRING(pev->classname), CLASSNAME);

	pev->classname = MAKE_STRING("rocket");
	AddEntityHashValue(pev, STRING(pev->classname), CLASSNAME);

	pev->solid = SOLID_BBOX;

	SET_MODEL(ENT(pev), "models/grenade.mdl");
	UTIL_SetSize( pev, Vector(-3, -6, 0), Vector(3, 6, 8));
}

CRocket *CRocket::Create(int iWpnId, entvars_t *pevOwner, bool bBounce, unsigned short usEvent)
{
	CRocket *pRocket = GetClassPtr((CRocket *)NULL);
	pRocket->Spawn();

	SET_MODEL( ENT(pRocket->pev), g_WpnItemInfo[iWpnId].szGrenadeModel   );
	
	Vector vecOrigin , vecAngle;
	GetAimOrigin(32.0, 4.0, -2.0, pevOwner, vecOrigin);
	vecAngle = pevOwner->angles;
	vecAngle.x = vecAngle.x * 3;

	int iMoveType = 0;

	if( bBounce )
		iMoveType = MOVETYPE_BOUNCE;
	else 
		iMoveType = MOVETYPE_FLY;

	pRocket->m_usEvent = usEvent;
	pRocket->pev->iuser4 = iWpnId;
	pRocket->pev->origin = vecOrigin;
	pRocket->pev->movetype = iMoveType;
	pRocket->pev->angles = vecAngle ;
	pRocket->pev->v_angle  = pevOwner->v_angle ;
	pRocket->pev->owner = ENT( pevOwner );
	pRocket->pev->fuser1 = gpGlobals->time + 0.5; // the time of jet sound
	pRocket->pev->nextthink = gpGlobals->time ;
	pRocket->SetTouch(&CRocket::GrenadeTouch);

	MESSAGE_BEGIN(MSG_BROADCAST, SVC_TEMPENTITY);
	WRITE_BYTE( TE_BEAMFOLLOW);
	WRITE_SHORT( ENTINDEX( ENT (pRocket->pev) ) );
	WRITE_SHORT( g_sModelIndexTdmSmoke );
	WRITE_BYTE( 10 );
	WRITE_BYTE(3);
	WRITE_BYTE(255);
	WRITE_BYTE(255);
	WRITE_BYTE(255);
	WRITE_BYTE(255);
	MESSAGE_END();

	if( bBounce )
	{
		Vector vecAimOrigin,  vecVelocity;
		fm_get_aim_origin( pevOwner, vecAimOrigin);
		get_speed_vector( pevOwner->origin , vecAimOrigin, 1000, vecVelocity);
		pRocket->pev->velocity = vecVelocity;
		return pRocket;
	}
	else 
	{
		pRocket->SetThink(&CRocket::GrenadeThink);
		return pRocket;
	}
}
