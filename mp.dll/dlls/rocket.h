#ifndef _ROCKET_H
#define _ROCKET_H


class CRocket : public CBaseMonster
{
private:
	void Spawn(void);
	void GrenadeThink(void);
	void GrenadeTouch( CBaseEntity *pOther );
	void CRocket::Explode(void);

	unsigned short m_usEvent;

public:
	int BloodColor(void) { return DONT_BLEED; }
	void Killed(entvars_t *pevAttacker, int iGib) { UTIL_Remove(this); }

public:
	static CRocket *Create(int iWpnId, entvars_t *pevOwner, bool bBounce, unsigned short usEvent);
};







#endif