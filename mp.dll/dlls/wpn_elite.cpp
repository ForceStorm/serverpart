/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "player.h"
#include "weapons.h"

enum elite_e
{
	ELITE_IDLE,
	ELITE_IDLE_LEFTEMPTY,
	ELITE_SHOOTLEFT1,
	ELITE_SHOOTLEFT2,
	ELITE_SHOOTLEFT3,
	ELITE_SHOOTLEFT4,
	ELITE_SHOOTLEFT5,
	ELITE_SHOOTLEFTLAST,
	ELITE_SHOOTRIGHT1,
	ELITE_SHOOTRIGHT2,
	ELITE_SHOOTRIGHT3,
	ELITE_SHOOTRIGHT4,
	ELITE_SHOOTRIGHT5,
	ELITE_SHOOTRIGHTLAST,
	ELITE_RELOAD,
	ELITE_DRAW
};

LINK_ENTITY_TO_CLASS(weapon_elite, CELITE);

void CELITE::Spawn(void)
{
	pev->classname = MAKE_STRING("weapon_elite");

	Precache();
	m_iId = WEAPON_ELITE;
	SET_MODEL(ENT(pev), g_WpnItemInfo[m_iWpnId].szWModel );
	pev->body = g_WpnItemInfo[m_iWpnId].iWModelBody  ;
	pev->sequence = g_WpnItemInfo[m_iWpnId].iWModelSeq ;

	m_iDefaultAmmo = g_WpnItemInfo[m_iWpnId].iClip;
	m_flAccuracy = 0.88;

	FallInit();
}

void CELITE::Precache(void)
{
	PRECACHE_SOUND("weapons/elite_fire.wav");
	PRECACHE_SOUND("weapons/elite_reloadstart.wav");
	PRECACHE_SOUND("weapons/elite_leftclipin.wav");
	PRECACHE_SOUND("weapons/elite_clipout.wav");
	PRECACHE_SOUND("weapons/elite_sliderelease.wav");
	PRECACHE_SOUND("weapons/elite_rightclipin.wav");
	PRECACHE_SOUND("weapons/elite_deploy.wav");

	m_usFireELITE_LEFT = PRECACHE_EVENT(1, g_WpnItemInfo[m_iWpnId].szEvent);
	m_usFireELITE_RIGHT = PRECACHE_EVENT(1, g_WpnItemInfo[m_iWpnId].szEvent2);
}

int CELITE::GetItemInfo(ItemInfo *p)
{
	p->pszName = STRING(pev->classname);
	p->pszAmmo1 = "9mm";
	p->iMaxAmmo1 = _9MM_MAX_CARRY;
	p->pszAmmo2 = NULL;
	p->iMaxAmmo2 = -1;
	p->iMaxClip =  g_WpnItemInfo[m_iWpnId].iClip;
	p->iSlot = 1;
	p->iPosition = 5;
	p->iId = m_iId = WEAPON_ELITE;
	p->iFlags = 0;
	p->iWeight = ELITE_WEIGHT;

	return 1;
}

BOOL CELITE::Deploy(void)
{
	m_pPlayer->m_iWpnState = 0;
	m_flAccuracy = 0.88;

	if (!(m_iClip & 1))
		m_iWeaponState |= WPNSTATE_ELITE_LEFT;

	return DefaultDeploy(g_WpnItemInfo[m_iWpnId].szVModel , g_WpnItemInfo[m_iWpnId].szPModel ,  ELITE_DRAW, g_WpnItemInfo[m_iWpnId].szAnimExtention, UseDecrement() != FALSE);
}

void CELITE::FovChange(void)
{
	if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT_DOWN)
	{
		// sight down
		if(m_pPlayer->m_iFOV >= 90)
		{
			m_pPlayer->SendFOV( 90);
			return;
		}
		m_pPlayer->SendFOV( m_pPlayer->m_iFOV + 1);
		pev->nextthink = gpGlobals->time + g_WpnItemInfo[m_iWpnId].flScopeDownTime / (90 - g_WpnItemInfo[m_iWpnId].iMinFov);
	}
	else if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT_UP)
	{
		// sight up
		if(m_pPlayer->m_iFOV <= g_WpnItemInfo[m_iWpnId].iMinFov)
		{
			m_pPlayer->SendFOV( g_WpnItemInfo[m_iWpnId].iMinFov);
			return;
		}
		m_pPlayer->SendFOV( m_pPlayer->m_iFOV - 1);
		pev->nextthink = gpGlobals->time + g_WpnItemInfo[m_iWpnId].flScopeUpTime / (90- g_WpnItemInfo[m_iWpnId].iMinFov);
	}
	
}

void CELITE::SecondaryAttack(void)
{

	if(m_pPlayer->m_iWpnState & WPNSTATE_FASTRUN)
		return;

	if(g_WpnItemInfo[m_iWpnId].iRightBtnFunc == FUNC_SIGHT)
	{
		if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT)
		{
			// Sight down

			m_pPlayer->m_iWpnState &= ~WPNSTATE_SIGHT;
			m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_DOWN;

			m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeDownTime ;

			m_pPlayer->pev->viewmodel = MAKE_STRING( g_WpnItemInfo[m_iWpnId].szVModel );
			SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimmScopeDown , UseDecrement() != FALSE);

			SetThink( &CELITE::FovChange);
			pev->nextthink = gpGlobals->time;
		}
		else
		{
			// sight up
			m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_UP;
			SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimScopeUp  , UseDecrement() != FALSE);

			m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeUpTime;
			
			SetThink( &CELITE::FovChange);
			pev->nextthink = gpGlobals->time;
		}
	}
}

void CELITE::PrimaryAttack(void)
{

	float flSpread;
	float flCycleTime;

	flSpread = g_WpnItemInfo[m_iWpnId].flFireSpread ;
	flCycleTime = g_WpnItemInfo[m_iWpnId].flFireCycleTime;

	if (!FBitSet(m_pPlayer->pev->flags, FL_ONGROUND))
		ELITEFire(
			flSpread * 1.131275 * (1 - m_flAccuracy), 
			flCycleTime, 
			FALSE);

	else if (m_pPlayer->pev->velocity.Length2D() > 20)
		ELITEFire(
			flSpread * 1.011452 * (1 - m_flAccuracy), 
			flCycleTime, 
			FALSE);

	else if (FBitSet(m_pPlayer->pev->flags, FL_DUCKING))
		ELITEFire(
			flSpread * 0.99851 * (1 - m_flAccuracy), 
			flCycleTime, 
			FALSE);

	else
		ELITEFire(
			flSpread * (1 - m_flAccuracy), 
			flCycleTime, 
			FALSE);
}

void CELITE::ELITEFire(float flSpread, float flCycleTime, BOOL fUseAutoAim)
{
	flCycleTime -= 0.125;
	m_iShotsFired++;

	if (m_iShotsFired > 1)
		return;

	if (m_flLastFire)
	{
		m_flAccuracy -= (g_WpnItemInfo[m_iWpnId].flFireAccuracyBase - (gpGlobals->time - m_flLastFire)) * 0.275;

		if (m_flAccuracy > 0.88)
			m_flAccuracy = 0.88;
		else if (m_flAccuracy < 0.55)
			m_flAccuracy = 0.55;
	}

	m_flLastFire = gpGlobals->time;

	if (m_iClip <= 0)
	{
		if (m_fFireOnEmpty)
		{
			PlayEmptySound();
			m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + 0.2;
		}

		return;
	}

	m_iClip--;
	m_flNextPrimaryAttack = m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + flCycleTime;
	m_pPlayer->m_iWeaponVolume = BIG_EXPLOSION_VOLUME;
	m_pPlayer->m_iWeaponFlash = DIM_GUN_FLASH;

	UTIL_MakeVectors(m_pPlayer->pev->v_angle + m_pPlayer->pev->punchangle);
	m_pPlayer->pev->effects |= EF_MUZZLEFLASH;

	Vector vecDir;

	if (m_iWeaponState & WPNSTATE_ELITE_LEFT)
	{
		m_pPlayer->SetAnimation(PLAYER_ATTACK1);
		m_iWeaponState &= ~WPNSTATE_ELITE_LEFT;

		vecDir = FireBullets3(
			m_pPlayer->GetGunPosition() - gpGlobals->v_right * 5, 
			gpGlobals->v_forward, 
			flSpread, 
			g_WpnItemInfo[m_iWpnId].iBulletDistance, 
			g_WpnItemInfo[m_iWpnId].iBulletPenetration, 
			g_WpnItemInfo[m_iWpnId].iBulletType, 
			g_WpnItemInfo[m_iWpnId].iBulletDamage, 
			g_WpnItemInfo[m_iWpnId].flRangeModifier, 
			m_pPlayer->pev, 
			TRUE, 
			m_pPlayer->random_seed);
		
		PLAYBACK_EVENT_FULL(0, ENT(m_pPlayer->pev), m_usFireELITE_LEFT, 0, (float *)&g_vecZero, (float *)&g_vecZero, vecDir.x, vecDir.y, (int)(m_pPlayer->pev->punchangle.y * 100), m_iClip, FALSE, FALSE);
	}
	else
	{
		m_pPlayer->SetAnimation(PLAYER_ATTACK2);
		m_iWeaponState |= WPNSTATE_ELITE_LEFT;

		vecDir = FireBullets3(
			m_pPlayer->GetGunPosition() - gpGlobals->v_right * 5, 
			gpGlobals->v_forward, 
			flSpread, 
			g_WpnItemInfo[m_iWpnId].iBulletDistance2, 
			g_WpnItemInfo[m_iWpnId].iBulletPenetration2, 
			g_WpnItemInfo[m_iWpnId].iBulletType2, 
			g_WpnItemInfo[m_iWpnId].iBulletDamage2, 
			g_WpnItemInfo[m_iWpnId].flRangeModifier2, 
			m_pPlayer->pev, 
			TRUE, 
			m_pPlayer->random_seed);

		PLAYBACK_EVENT_FULL(0, ENT(m_pPlayer->pev), m_usFireELITE_RIGHT, 0, (float *)&g_vecZero, (float *)&g_vecZero, vecDir.x, vecDir.y, (int)(m_pPlayer->pev->punchangle.y * 100), m_iClip, FALSE, FALSE);
	}

	if (!m_iClip && m_pPlayer->m_rgAmmo[m_iWpnId] <= 0)
		m_pPlayer->SetSuitUpdate("!HEV_AMO0", FALSE, 0);

	m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 2;
	m_pPlayer->pev->punchangle.x -= 2;

}

void CELITE::Reload(void)
{
	if (DefaultReload(g_WpnItemInfo[m_iWpnId].iClip, ELITE_RELOAD, g_WpnItemInfo[m_iWpnId].flReloadTime))
	{
		m_pPlayer->SetAnimation(PLAYER_RELOAD);
		m_flAccuracy = 0.88;
	}
}

void CELITE::WeaponIdle(void)
{
	ResetEmptySound();
	m_pPlayer->GetAutoaimVector(AUTOAIM_10DEGREES);

	if (m_flTimeWeaponIdle > UTIL_WeaponTimeBase())
		return;

	if (m_iClip)
	{
		m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 60;

		if (m_iClip == 1)
			SendWeaponAnim(ELITE_IDLE_LEFTEMPTY, UseDecrement() != FALSE);
		else
			SendWeaponAnim(ELITE_IDLE, UseDecrement() != FALSE);
	}
}