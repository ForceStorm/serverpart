/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "player.h"
#include "weapons.h"

LINK_ENTITY_TO_CLASS(weapon_glock18, CGLOCK18);

void CGLOCK18::Spawn(void)
{
	pev->classname = MAKE_STRING("weapon_glock18");

	Precache();
	m_iId = WEAPON_GLOCK18;
	SET_MODEL(ENT(pev), g_WpnItemInfo[m_iWpnId].szWModel );
	pev->body = g_WpnItemInfo[m_iWpnId].iWModelBody  ;
	pev->sequence = g_WpnItemInfo[m_iWpnId].iWModelSeq ;


	m_iDefaultAmmo = g_WpnItemInfo[m_iWpnId].iClip;

	m_bBurstFire = 0;
	m_iGlock18ShotsFired = 0;
	m_flGlock18Shoot = 0;
	m_flAccuracy = 0.9;
	m_iWeaponState &= ~WPNSTATE_SHIELD_DRAWN;

	FallInit();
}

void CGLOCK18::Precache(void)
{
	PRECACHE_SOUND("weapons/glock18-1.wav");
	PRECACHE_SOUND("weapons/glock18-2.wav");
	PRECACHE_SOUND("weapons/clipout1.wav");
	PRECACHE_SOUND("weapons/clipin1.wav");
	PRECACHE_SOUND("weapons/sliderelease1.wav");
	PRECACHE_SOUND("weapons/slideback1.wav");
	PRECACHE_SOUND("weapons/357_cock1.wav");
	PRECACHE_SOUND("weapons/de_clipin.wav");
	PRECACHE_SOUND("weapons/de_clipout.wav");

	m_usFireGlock18 = PRECACHE_EVENT(1, g_WpnItemInfo[m_iWpnId].szEvent);
}

int CGLOCK18::GetItemInfo(ItemInfo *p)
{
	p->pszName = STRING(pev->classname);
	p->pszAmmo1 = "9mm";
	p->iMaxAmmo1 = _9MM_MAX_CARRY;
	p->pszAmmo2 = NULL;
	p->iMaxAmmo2 = -1;
	p->iMaxClip =  g_WpnItemInfo[m_iWpnId].iClip;
	p->iSlot = 1;
	p->iPosition = 2;
	p->iId = m_iId = WEAPON_GLOCK18;
	p->iFlags = 0;
	p->iWeight = GLOCK18_WEIGHT;

	return 1;
}

BOOL CGLOCK18::Deploy(void)
{
	m_bBurstFire = 0;
	m_iGlock18ShotsFired = 0;
	m_flGlock18Shoot = 0;
	m_flAccuracy = 0.9;
	m_fMaxSpeed = 250;
	m_iWeaponState &= ~WPNSTATE_SHIELD_DRAWN;
	m_pPlayer->m_bShieldDrawn = false;
	m_pPlayer->m_iWpnState = 0;

	return DefaultDeploy(g_WpnItemInfo[m_iWpnId].szVModel , g_WpnItemInfo[m_iWpnId].szPModel , g_WpnItemInfo[m_iWpnId].iAnimDraw , g_WpnItemInfo[m_iWpnId].szAnimExtention, UseDecrement() != FALSE);
}

void CGLOCK18::FovChange(void)
{
	if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT_DOWN)
	{
		// sight down
		if(m_pPlayer->m_iFOV >= 90)
		{
			m_pPlayer->SendFOV( 90);
			return;
		}
		m_pPlayer->SendFOV( m_pPlayer->m_iFOV + 1);
		pev->nextthink = gpGlobals->time + g_WpnItemInfo[m_iWpnId].flScopeDownTime / (90 - g_WpnItemInfo[m_iWpnId].iMinFov);
	}
	else if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT_UP)
	{
		// sight up
		if(m_pPlayer->m_iFOV <= g_WpnItemInfo[m_iWpnId].iMinFov)
		{
			m_pPlayer->SendFOV( g_WpnItemInfo[m_iWpnId].iMinFov);
			return;
		}
		m_pPlayer->SendFOV( m_pPlayer->m_iFOV - 1);
		pev->nextthink = gpGlobals->time + g_WpnItemInfo[m_iWpnId].flScopeUpTime / (90- g_WpnItemInfo[m_iWpnId].iMinFov);
	}
	
}
void CGLOCK18::SecondaryAttack(void)
{
	if(m_pPlayer->m_iWpnState & WPNSTATE_FASTRUN)
		return;

	if(g_WpnItemInfo[m_iWpnId].iRightBtnFunc == FUNC_SIGHT)
	{
		if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT)
		{
			// Sight down

			m_pPlayer->m_iWpnState &= ~WPNSTATE_SIGHT;
			m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_DOWN;

			m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeDownTime ;

			m_pPlayer->pev->viewmodel = MAKE_STRING( g_WpnItemInfo[m_iWpnId].szVModel );
			SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimmScopeDown , UseDecrement() != FALSE);

			SetThink(&CGLOCK18::FovChange);
			pev->nextthink = gpGlobals->time;
		}
		else
		{
			// sight up
			m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_UP;
			SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimScopeUp  , UseDecrement() != FALSE);

			m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeUpTime;
			
			SetThink(&CGLOCK18::FovChange);
			pev->nextthink = gpGlobals->time;
		}
	}
	else if(g_WpnItemInfo[m_iWpnId].iRightBtnFunc == FUNC_BURST_SHOOT)
	{
		if (m_iWeaponState & WPNSTATE_GLOCK18_BURST_MODE)
		{
			ClientPrint(m_pPlayer->pev, HUD_PRINTCENTER, "#Switch_To_SemiAuto");
			m_iWeaponState &= ~WPNSTATE_GLOCK18_BURST_MODE;
		}
		else
		{
			ClientPrint(m_pPlayer->pev, HUD_PRINTCENTER, "#Switch_To_BurstFire");
			m_iWeaponState |= WPNSTATE_GLOCK18_BURST_MODE;
		}
		m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + 0.3;
	}	
}

void CGLOCK18::PrimaryAttack(void)
{
	float flSpread;
	float flCycleTime;

	if (m_iWeaponState & WPNSTATE_GLOCK18_BURST_MODE)
	{
		flSpread = g_WpnItemInfo[m_iWpnId].flFireSpread ;
		flCycleTime = g_WpnItemInfo[m_iWpnId].flFireCycleTime;
	}
	else
	{
		flSpread = g_WpnItemInfo[m_iWpnId].flFireSpread2 ;
		flCycleTime = g_WpnItemInfo[m_iWpnId].flFireCycleTime2;
	}


	if (!FBitSet(m_pPlayer->pev->flags, FL_ONGROUND))
		GLOCK18Fire(
			flSpread * 1.131275 * (1 - m_flAccuracy), 
			flCycleTime, 
			FALSE);

	else if (m_pPlayer->pev->velocity.Length2D() > 20)
		GLOCK18Fire(
			flSpread * 1.011452 * (1 - m_flAccuracy), 
			flCycleTime, 
			FALSE);

	else if (FBitSet(m_pPlayer->pev->flags, FL_DUCKING))
		GLOCK18Fire(
			flSpread * 0.99851 * (1 - m_flAccuracy), 
			flCycleTime, 
			FALSE);

	else
		GLOCK18Fire(
			flSpread * (1 - m_flAccuracy), 
			flCycleTime, 
			FALSE);
}

void CGLOCK18::GLOCK18Fire(float flSpread, float flCycleTime, BOOL fUseBurstMode)
{
	if (fUseBurstMode == TRUE)
	{
		m_iGlock18ShotsFired = 0;
	}
	else
	{
		m_iShotsFired++;
		flCycleTime -= 0.05;

		if (m_iShotsFired > 1)
			return;
	}

	if (m_flLastFire)
	{
		m_flAccuracy -= (g_WpnItemInfo[m_iWpnId].flFireAccuracyBase - (gpGlobals->time - m_flLastFire)) * 0.275;

		if (m_flAccuracy > 0.9)
			m_flAccuracy = 0.9;
		else if (m_flAccuracy < 0.6)
			m_flAccuracy = 0.6;
	}

	m_flLastFire = gpGlobals->time;

	if (m_iClip <= 0)
	{
		if (m_fFireOnEmpty)
		{
			PlayEmptySound();
			m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + 0.2;
		}

		return;
	}

	m_iClip--;
	m_pPlayer->pev->effects |= EF_MUZZLEFLASH;
	SetPlayerShieldAnim();
	m_pPlayer->SetAnimation(PLAYER_ATTACK1);

	UTIL_MakeVectors(m_pPlayer->pev->v_angle + m_pPlayer->pev->punchangle);

	m_pPlayer->m_iWeaponVolume = NORMAL_GUN_VOLUME;
	m_pPlayer->m_iWeaponFlash = BRIGHT_GUN_FLASH;

	Vector vecSrc = m_pPlayer->GetGunPosition();
	Vector vecDir = m_pPlayer->FireBullets3(
		vecSrc, 
		gpGlobals->v_forward, 
		flSpread, 
		g_WpnItemInfo[m_iWpnId].iBulletDistance , 
		g_WpnItemInfo[m_iWpnId].iBulletPenetration  , 
		g_WpnItemInfo[m_iWpnId].iBulletType , 
		fUseBurstMode ? g_WpnItemInfo[m_iWpnId].iBulletDamage2 : g_WpnItemInfo[m_iWpnId].iBulletDamage, 
		g_WpnItemInfo[m_iWpnId].flRangeModifier  , 
		m_pPlayer->pev, 
		TRUE, 
		m_pPlayer->random_seed);

	PLAYBACK_EVENT_FULL(0, m_pPlayer->edict(), m_usFireGlock18, 0, (float *)&g_vecZero, (float *)&g_vecZero, vecDir.x, vecDir.y, (int)(m_pPlayer->pev->punchangle.x * 100), (int)(m_pPlayer->pev->punchangle.y * 100), m_iClip != 0, FALSE);
	m_flNextPrimaryAttack = m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + flCycleTime;

	if (!m_iClip && m_pPlayer->m_rgAmmo[m_iWpnId] <= 0)
		m_pPlayer->SetSuitUpdate("!HEV_AMO0", FALSE, 0);

	m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 2.5;

	if (fUseBurstMode == TRUE)
	{
		m_iGlock18ShotsFired++;
		m_flGlock18Shoot = gpGlobals->time + 0.1;
	}

	ResetPlayerShieldAnim();

}

void CGLOCK18::Reload(void)
{
	if (DefaultReload(g_WpnItemInfo[m_iWpnId].iClip, g_WpnItemInfo[m_iWpnId].iAnimReload, g_WpnItemInfo[m_iWpnId].flReloadTime))
	{
		m_pPlayer->SetAnimation(PLAYER_RELOAD);
		m_flAccuracy = 0.9;
	}
}

void CGLOCK18::WeaponIdle(void)
{
	ResetEmptySound();
	m_pPlayer->GetAutoaimVector(AUTOAIM_10DEGREES);

	if (m_flTimeWeaponIdle > UTIL_WeaponTimeBase())
		return;


	if (m_iClip)
	{
		m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 3.0625;
		SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimIdle, UseDecrement() != FALSE);
	}
}