/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "player.h"
#include "weapons.h"
#include "rocket.h"

enum m3_e
{
	M3_IDLE,
	M3_FIRE1,
	M3_FIRE2,
	M3_RELOAD,
	M3_PUMP,
	M3_START_RELOAD,
	M3_DRAW,
	M3_HOLSTER
};

LINK_ENTITY_TO_CLASS(weapon_launcher, CLauncher);

void CLauncher::Spawn(void)
{
	pev->classname = MAKE_STRING("weapon_launcher");

	Precache();
	m_iId = WEAPON_LAUNCHER;
	SET_MODEL(ENT(pev), g_WpnItemInfo[m_iWpnId].szWModel );
	pev->body = g_WpnItemInfo[m_iWpnId].iWModelBody  ;
	pev->sequence = g_WpnItemInfo[m_iWpnId].iWModelSeq ;

	m_iDefaultAmmo = g_WpnItemInfo[m_iWpnId].iClip;

	FallInit();
}

void CLauncher::Precache(void)
{
	PRECACHE_SOUND("weapons/m3-1.wav");
	PRECACHE_SOUND("weapons/m3_insertshell.wav");
	PRECACHE_SOUND("weapons/m3_pump.wav");
	PRECACHE_SOUND("weapons/reload1.wav");
	PRECACHE_SOUND("weapons/reload3.wav");

	m_usFireLauncher = PRECACHE_EVENT(1, g_WpnItemInfo[m_iWpnId].szEvent );
	m_usExplosion = PRECACHE_EVENT(1, g_WpnItemInfo[m_iWpnId].szEvent2 );
}

int CLauncher::GetItemInfo(ItemInfo *p)
{
	p->iId = m_iId = WEAPON_LAUNCHER;

	p->pszName = STRING(pev->classname);
	p->pszAmmo1 = "buckshot";
	p->iMaxAmmo1 = BUCKSHOT_MAX_CARRY;
	p->pszAmmo2 = NULL;
	p->iMaxAmmo2 = -1;
	p->iMaxClip =  g_WpnItemInfo[m_iWpnId].iClip;
	p->iSlot = 0;
	p->iPosition = 5;
	p->iFlags = 0;
	p->iWeight = M3_WEIGHT;

	return 1;
}

BOOL CLauncher::Deploy(void)
{
	m_pPlayer->m_iWpnState = 0;

	return DefaultDeploy(g_WpnItemInfo[m_iWpnId].szVModel , g_WpnItemInfo[m_iWpnId].szPModel , M3_DRAW, g_WpnItemInfo[m_iWpnId].szAnimExtention, UseDecrement() != FALSE);
}

void CLauncher::RocketFire(void)
{
	if (m_pPlayer->pev->waterlevel == 3)
	{
		PlayEmptySound();
		m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + 0.15;
		return;
	}

	if (m_iClip <= 0)
	{
		Reload();

		if (m_iClip == 0)
			PlayEmptySound();

		m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + 1.0;
		return;
	}

	m_iClip--;
	m_pPlayer->pev->effects |= EF_MUZZLEFLASH;
	m_pPlayer->SetAnimation(PLAYER_ATTACK1);

	UTIL_MakeVectors(m_pPlayer->pev->v_angle + m_pPlayer->pev->punchangle);

	m_pPlayer->m_iWeaponVolume = BIG_EXPLOSION_VOLUME;
	m_pPlayer->m_iWeaponFlash = BRIGHT_GUN_FLASH;

	m_flNextPrimaryAttack = m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flFireCycleTime;

	if (m_iClip)
		m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 2.5;
	else
		m_flTimeWeaponIdle = 0.875;

	m_pPlayer->pev->punchangle.x -= 8;

	CRocket::Create( m_iWpnId,  m_pPlayer->pev, TRUE, m_usExplosion);
	PLAYBACK_EVENT_FULL(
		0, 
		ENT(m_pPlayer->pev), 
		m_usFireLauncher, 
		0, 
		(float *)&g_vecZero, 
		(float *)&g_vecZero, 
		0, 
		0, 
		0, 
		0, 
		FALSE, 
		FALSE);

	KickBack(8.4, 1.0, 0.56, 0.0375, 30.0, 8.0, 8);


	m_pPlayer->m_iWeaponVolume = LOUD_GUN_VOLUME;
	m_pPlayer->m_iWeaponFlash = BRIGHT_GUN_FLASH;

	m_fInSpecialReload = 0;
}

void CLauncher::PrimaryAttack(void)
{
	if(g_WpnItemInfo[m_iWpnId].iBulletType == BULLET_M32)
	{
		RocketFire();
		return;
	}
}

void CLauncher::FovChange(void)
{
	if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT_DOWN)
	{
		// sight down
		if(m_pPlayer->m_iFOV >= 90)
		{
			m_pPlayer->SendFOV( 90);
			return;
		}
		m_pPlayer->SendFOV( m_pPlayer->m_iFOV + 1);
		pev->nextthink = gpGlobals->time + g_WpnItemInfo[m_iWpnId].flScopeDownTime / (90 - g_WpnItemInfo[m_iWpnId].iMinFov);
	}
	else if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT_UP)
	{
		// sight up
		if(m_pPlayer->m_iFOV <= g_WpnItemInfo[m_iWpnId].iMinFov)
		{
			m_pPlayer->SendFOV( g_WpnItemInfo[m_iWpnId].iMinFov);
			return;
		}
		m_pPlayer->SendFOV( m_pPlayer->m_iFOV - 1);
		pev->nextthink = gpGlobals->time + g_WpnItemInfo[m_iWpnId].flScopeUpTime / (90- g_WpnItemInfo[m_iWpnId].iMinFov);
	}
	
}
void CLauncher::SecondaryAttack(void)
{

	if(m_pPlayer->m_iWpnState & WPNSTATE_FASTRUN)
		return;

	if(g_WpnItemInfo[m_iWpnId].iRightBtnFunc == FUNC_SIGHT)
	{
		if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT)
		{
			// Sight down

			m_pPlayer->m_iWpnState &= ~WPNSTATE_SIGHT;
			m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_DOWN;

			m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeDownTime ;

			m_pPlayer->pev->viewmodel = MAKE_STRING( g_WpnItemInfo[m_iWpnId].szVModel );
			SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimmScopeDown , UseDecrement() != FALSE);

			SetThink(&CLauncher::FovChange);
			pev->nextthink = gpGlobals->time;
		}
		else
		{
			// sight up
			m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_UP;
			SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimScopeUp  , UseDecrement() != FALSE);

			m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeUpTime;
			
			SetThink(&CLauncher::FovChange);
			pev->nextthink = gpGlobals->time;
		}
	}
}

void CLauncher::Reload(void)
{
	if (m_pPlayer->m_rgAmmo[m_iWpnId] <= 0 || m_iClip == g_WpnItemInfo[m_iWpnId].iClip)
		return;

	if (m_flNextPrimaryAttack > UTIL_WeaponTimeBase())
		return;

	
	if(g_WpnItemInfo[m_iWpnId].iScopeType == SCOPE_SIGHT && m_pPlayer->pev->viewmodel == MAKE_STRING( g_WpnItemInfo[m_iWpnId].szScopeModel ))
	{
		m_pPlayer->pev->viewmodel = MAKE_STRING( g_WpnItemInfo[m_iWpnId].szVModel  );
		m_pPlayer->SendFOV(90);

		m_pPlayer->m_iWpnState &= ~WPNSTATE_SIGHT_DOWN;
		m_pPlayer->m_iWpnState &= ~WPNSTATE_SIGHT;
	}

	if (!m_fInSpecialReload)
	{
		m_pPlayer->SetAnimation(PLAYER_RELOAD);
		SendWeaponAnim(M3_START_RELOAD, UseDecrement() != FALSE);

		m_fInSpecialReload = 1;
		m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flReloadStartTime;
		m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flReloadStartTime;
		m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flReloadStartTime;
		m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flReloadStartTime;
	}
	else if (m_fInSpecialReload == 1)
	{
		if (m_flTimeWeaponIdle > UTIL_WeaponTimeBase())
			return;

		m_fInSpecialReload = 2;
		SendWeaponAnim(M3_RELOAD, UseDecrement() != FALSE);

		m_flNextReload = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flReloadTime  ;
		m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flReloadTime ;
	}
	else
	{
		m_iClip++;
		m_pPlayer->m_rgAmmo[m_iWpnId]--;
		m_fInSpecialReload = 1;
		m_pPlayer->ammo_buckshot--;
	}

}

void CLauncher::WeaponIdle(void)
{
	ResetEmptySound();
	m_pPlayer->GetAutoaimVector(AUTOAIM_5DEGREES);

	if (m_flPumpTime && m_flPumpTime < UTIL_WeaponTimeBase())
		m_flPumpTime = 0;

	if (m_flTimeWeaponIdle < UTIL_WeaponTimeBase())
	{
		if (m_iClip == 0 && m_fInSpecialReload == 0 && m_pPlayer->m_rgAmmo[m_iWpnId])
		{
			Reload();
		}
		else if (m_fInSpecialReload != 0)
		{
			if (m_iClip != g_WpnItemInfo[m_iWpnId].iClip && m_pPlayer->m_rgAmmo[m_iWpnId])
			{
				Reload();
			}
			else
			{
				SendWeaponAnim(M3_PUMP, UseDecrement() != FALSE);

				m_fInSpecialReload = 0;
				m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flReloadEndTime ;
			}
		}
		else
			SendWeaponAnim(M3_IDLE, UseDecrement() != FALSE);
	}
}