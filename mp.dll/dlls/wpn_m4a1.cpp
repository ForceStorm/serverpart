/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "player.h"
#include "weapons.h"


LINK_ENTITY_TO_CLASS(weapon_m4a1, CM4A1);

void CM4A1::Spawn(void)
{
	pev->classname = MAKE_STRING("weapon_m4a1");

	Precache();
	m_iId = WEAPON_M4A1;
	SET_MODEL(ENT(pev), g_WpnItemInfo[m_iWpnId].szWModel );
	pev->body = g_WpnItemInfo[m_iWpnId].iWModelBody  ;
	pev->sequence = g_WpnItemInfo[m_iWpnId].iWModelSeq ;

	m_iDefaultAmmo = g_WpnItemInfo[m_iWpnId].iClip;
	m_flAccuracy = 0.2;
	m_iShotsFired = 0;
	m_bDelayFire = true;

	FallInit();
}

void CM4A1::Precache(void)
{
	PRECACHE_SOUND("weapons/m4a1-1.wav");
	PRECACHE_SOUND("weapons/m4a1_unsil-1.wav");
	PRECACHE_SOUND("weapons/m4a1_unsil-2.wav");
	PRECACHE_SOUND("weapons/m4a1_clipin.wav");
	PRECACHE_SOUND("weapons/m4a1_clipout.wav");
	PRECACHE_SOUND("weapons/m4a1_boltpull.wav");
	PRECACHE_SOUND("weapons/m4a1_deploy.wav");
	PRECACHE_SOUND("weapons/m4a1_silencer_on.wav");
	PRECACHE_SOUND("weapons/m4a1_silencer_off.wav");

	m_usFire1 = PRECACHE_EVENT(1, g_WpnItemInfo[m_iWpnId].szEvent );
	m_usFire2 = PRECACHE_EVENT(1, g_WpnItemInfo[m_iWpnId].szEvent2 );
}

int CM4A1::GetItemInfo(ItemInfo *p)
{

	p->pszName = STRING(pev->classname);
	p->pszAmmo1 = "556Nato";
	p->iMaxAmmo1 = _556NATO_MAX_CARRY;
	p->pszAmmo2 = NULL;
	p->iMaxAmmo2 = -1;
	p->iMaxClip = g_WpnItemInfo[m_iWpnId].iClip;
	p->iSlot = 0;
	p->iPosition = 6;
	p->iId = m_iId = WEAPON_M4A1;
	p->iFlags = 0;
	p->iWeight = M4A1_WEIGHT;

	return 1;
}

BOOL CM4A1::Deploy(void)
{
	m_pPlayer->m_iWpnState = 0;

	m_bDelayFire = true;
	iShellOn = 1;
	m_flAccuracy = 0.2;
	m_iShotsFired = 0;

	int a = g_WpnItemInfo[m_iWpnId].iAnimEnd ;
	int b = g_WpnItemInfo[m_iWpnId].iAnimDraw  ;

	if (m_iWeaponState & WPNSTATE_M4A1_SILENCED)
		return DefaultDeploy(g_WpnItemInfo[m_iWpnId].szVModel , g_WpnItemInfo[m_iWpnId].szPModel , b, g_WpnItemInfo[m_iWpnId].szAnimExtention, UseDecrement() != FALSE);
	else
		return DefaultDeploy(g_WpnItemInfo[m_iWpnId].szVModel , g_WpnItemInfo[m_iWpnId].szPModel , a+b , g_WpnItemInfo[m_iWpnId].szAnimExtention, UseDecrement() != FALSE);
}

void CM4A1::FovChange(void)
{
	if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT_DOWN)
	{
		// sight down
		if(m_pPlayer->m_iFOV >= 90)
		{
			m_pPlayer->SendFOV( 90);
			return;
		}
		m_pPlayer->SendFOV( m_pPlayer->m_iFOV + 1);
		pev->nextthink = gpGlobals->time + g_WpnItemInfo[m_iWpnId].flScopeDownTime / (90 - g_WpnItemInfo[m_iWpnId].iMinFov);
	}
	else if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT_UP)
	{
		// sight up
		if(m_pPlayer->m_iFOV <= g_WpnItemInfo[m_iWpnId].iMinFov)
		{
			m_pPlayer->SendFOV( g_WpnItemInfo[m_iWpnId].iMinFov);
			return;
		}
		m_pPlayer->SendFOV( m_pPlayer->m_iFOV - 1);
		pev->nextthink = gpGlobals->time + g_WpnItemInfo[m_iWpnId].flScopeUpTime / (90- g_WpnItemInfo[m_iWpnId].iMinFov);
	}
	
}
void CM4A1::SecondaryAttack(void)
{
	if(m_pPlayer->m_iWpnState & WPNSTATE_FASTRUN)
		return;

	if(g_WpnItemInfo[m_iWpnId].iRightBtnFunc == FUNC_SIGHT)
	{
		if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT)
		{
			// Sight down

			m_pPlayer->m_iWpnState &= ~WPNSTATE_SIGHT;
			m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_DOWN;

			m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeDownTime ;

			m_pPlayer->pev->viewmodel = MAKE_STRING( g_WpnItemInfo[m_iWpnId].szVModel );
			SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimmScopeDown , UseDecrement() != FALSE);

			SetThink(&CM4A1::FovChange);
			pev->nextthink = gpGlobals->time;
		}
		else
		{
			// sight up
			m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_UP;
			SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimScopeUp  , UseDecrement() != FALSE);

			m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeUpTime;
			
			SetThink(&CM4A1::FovChange);
			pev->nextthink = gpGlobals->time;
		}
	}
	else if(g_WpnItemInfo[m_iWpnId].iRightBtnFunc == FUNC_SILENCER)
	{
		if (m_iWeaponState & WPNSTATE_M4A1_SILENCED)
		{
			m_iWeaponState &= ~WPNSTATE_M4A1_SILENCED;
			SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimChangeMode + g_WpnItemInfo[m_iWpnId].iAnimEnd, UseDecrement() != FALSE);
			//strcpy(m_pPlayer->m_szAnimExtention, "rifle");
		}
		else
		{
			m_iWeaponState |= WPNSTATE_M4A1_SILENCED;
			SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimChangeMode, UseDecrement() != FALSE);
			//strcpy(m_pPlayer->m_szAnimExtention, "rifle");
		}

		m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 2;
		m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + 2;
		m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + 2;
	}
}

void CM4A1::PrimaryAttack(void)
{
	float flSpread;
	float flCycleTime;

	if (m_iWeaponState & WPNSTATE_M4A1_SILENCED)
	{
		flSpread = g_WpnItemInfo[m_iWpnId].flFireSpread ;
		flCycleTime = g_WpnItemInfo[m_iWpnId].flFireCycleTime;

	}
	else
	{
		flSpread = g_WpnItemInfo[m_iWpnId].flFireSpread2 ;
		flCycleTime = g_WpnItemInfo[m_iWpnId].flFireCycleTime2;
	}

	if (!FBitSet(m_pPlayer->pev->flags, FL_ONGROUND))
		M4A1Fire(
			flSpread * 1.000583 * m_flAccuracy, 
			flCycleTime, 
			FALSE);

	else if (m_pPlayer->pev->velocity.Length2D() > 140)
		M4A1Fire(
			flSpread * 1.000357 * m_flAccuracy, 
			flCycleTime, 
			FALSE);

	else
		M4A1Fire(
			flSpread * m_flAccuracy, 
			flCycleTime, 
			FALSE);
}

void CM4A1::M4A1Fire(float flSpread, float flCycleTime, BOOL fUseAutoAim)
{
	m_bDelayFire = true;
	m_iShotsFired++;
	if (m_iWeaponState & WPNSTATE_M4A1_SILENCED)
		m_flAccuracy = ((m_iShotsFired * m_iShotsFired * m_iShotsFired) / g_WpnItemInfo[m_iWpnId].iFireAccuracyModifier ) + g_WpnItemInfo[m_iWpnId].flFireAccuracyBase ;
	else
		m_flAccuracy = ((m_iShotsFired * m_iShotsFired * m_iShotsFired) / g_WpnItemInfo[m_iWpnId].iFireAccuracyModifier2 ) + g_WpnItemInfo[m_iWpnId].flFireAccuracyBase2 ;


	if (m_flAccuracy > 1)
		m_flAccuracy = 1;

	if (m_iClip <= 0)
	{
		if (m_fFireOnEmpty)
		{
			PlayEmptySound();
			m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + 0.2;
		}

		return;
	}

	m_iClip--;
	m_pPlayer->SetAnimation(PLAYER_ATTACK1);

	UTIL_MakeVectors(m_pPlayer->pev->v_angle + m_pPlayer->pev->punchangle);

	m_pPlayer->m_iWeaponVolume = NORMAL_GUN_VOLUME;
	m_pPlayer->m_iWeaponFlash = BRIGHT_GUN_FLASH;

	Vector vecSrc = m_pPlayer->GetGunPosition();
	Vector vecDir;

	if (m_iWeaponState & WPNSTATE_M4A1_SILENCED)
	{
		
		vecDir = m_pPlayer->FireBullets3(
			vecSrc, 
			gpGlobals->v_forward, 
			flSpread, 
			g_WpnItemInfo[m_iWpnId].iBulletDistance2 , 
			g_WpnItemInfo[m_iWpnId].iBulletPenetration2  , 
			g_WpnItemInfo[m_iWpnId].iBulletType2 , 
			g_WpnItemInfo[m_iWpnId].iBulletDamage2 , 
			g_WpnItemInfo[m_iWpnId].flRangeModifier2  , 
			m_pPlayer->pev, 
			FALSE, 
			m_pPlayer->random_seed);

		PLAYBACK_EVENT_FULL(0, m_pPlayer->edict(), m_usFire1, 0, (float *)&g_vecZero, (float *)&g_vecZero, vecDir.x, vecDir.y, (int)(m_pPlayer->pev->punchangle.x * 100), (int)(m_pPlayer->pev->punchangle.y * 100), (m_iWeaponState & WPNSTATE_M4A1_SILENCED) ? true : false, FALSE);

		
	}
	else
	{
		
		vecDir = m_pPlayer->FireBullets3(
			vecSrc, 
			gpGlobals->v_forward, 
			flSpread, 
			g_WpnItemInfo[m_iWpnId].iBulletDistance , 
			g_WpnItemInfo[m_iWpnId].iBulletPenetration  , 
			g_WpnItemInfo[m_iWpnId].iBulletType , 
			g_WpnItemInfo[m_iWpnId].iBulletDamage , 
			g_WpnItemInfo[m_iWpnId].flRangeModifier  , 
			m_pPlayer->pev, 
			FALSE, 
			m_pPlayer->random_seed);

		PLAYBACK_EVENT_FULL(0, m_pPlayer->edict(), m_usFire2, 0, (float *)&g_vecZero, (float *)&g_vecZero, vecDir.x, vecDir.y, (int)(m_pPlayer->pev->punchangle.x * 100), (int)(m_pPlayer->pev->punchangle.y * 100), (m_iWeaponState & WPNSTATE_M4A1_SILENCED) ? true : false, FALSE);
		m_pPlayer->pev->effects |= EF_MUZZLEFLASH;
	}


	m_flNextPrimaryAttack = m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + flCycleTime;

	if (!m_iClip && m_pPlayer->m_rgAmmo[m_iWpnId] <= 0)
		m_pPlayer->SetSuitUpdate("!HEV_AMO0", FALSE, 0);

	m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 1.5;

	Vector velocity			= m_pPlayer->pev->velocity;
	float up_base			= g_WpnItemInfo[m_iWpnId].up_base ;
	float lateral_base		= g_WpnItemInfo[m_iWpnId].lateral_base ;
	float up_modifier		= g_WpnItemInfo[m_iWpnId].up_modifier ;
	float lateral_modifier	= g_WpnItemInfo[m_iWpnId].lateral_modifier ;
	float up_max			= g_WpnItemInfo[m_iWpnId].up_max ;
	float lateral_max		= g_WpnItemInfo[m_iWpnId].lateral_max ;
	int   direction_change	= g_WpnItemInfo[m_iWpnId].direction_change ;

	if (velocity.Length2D() > 0)
		KickBack(
			up_base +velocity.z /1713.5,  
			lateral_base +velocity.Length2D() /1750.3, 
			up_modifier + velocity.z /2971.0,
			lateral_modifier +velocity.Length2D() /2450.0, 
			up_max*1.2, 
			lateral_max*1.3, 
			(int)direction_change*0.976 );

	else if (!FBitSet(m_pPlayer->pev->flags, FL_ONGROUND))
		KickBack(
			up_base +velocity.z /813.5,
			lateral_base +velocity.Length2D() /1576.3, 
			up_modifier + velocity.z /1384.0,
			lateral_modifier +velocity.Length2D() /330.0, 
			up_max*1.6, 
			lateral_max*1.3, 
			(int)direction_change*0.925 );

	else if (FBitSet(m_pPlayer->pev->flags, FL_DUCKING))
		KickBack(
			up_base*0.67,
			lateral_base +velocity.Length2D() /2684.0, 
			up_modifier*0.67,
			lateral_modifier +velocity.Length2D() /530.0, 
			up_max*0.976, 
			lateral_max*0.895, 
			(int)direction_change*1.89 );

	else
		KickBack(
			up_base,
			lateral_base +velocity.Length2D() /830.0, 
			up_modifier,
			lateral_modifier, 
			up_max, 
			lateral_max, 
			(int)direction_change );

}

void CM4A1::Reload(void)
{
	int iAnim;

	if (m_iWeaponState & WPNSTATE_M4A1_SILENCED)
		iAnim = g_WpnItemInfo[m_iWpnId].iAnimReload ;
	else
		iAnim = g_WpnItemInfo[m_iWpnId].iAnimEnd + g_WpnItemInfo[m_iWpnId].iAnimReload ;

	if (DefaultReload(g_WpnItemInfo[m_iWpnId].iClip, iAnim, g_WpnItemInfo[m_iWpnId].flReloadTime))
	{
		m_pPlayer->SetAnimation(PLAYER_RELOAD);
		m_flAccuracy = 0.2;
		m_iShotsFired = 0;
		m_bDelayFire = false;
	}
}

void CM4A1::WeaponIdle(void)
{
	ResetEmptySound();
	m_pPlayer->GetAutoaimVector(AUTOAIM_10DEGREES);

	if (m_flTimeWeaponIdle > UTIL_WeaponTimeBase())
		return;

	m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 20;

	
	int a = g_WpnItemInfo[m_iWpnId].iAnimEnd ;
	int b = g_WpnItemInfo[m_iWpnId].iAnimIdle   ;

	if (m_iWeaponState & WPNSTATE_M4A1_SILENCED)
		SendWeaponAnim(b, UseDecrement() != FALSE);
	else
		SendWeaponAnim(a+b, UseDecrement() != FALSE);
}