/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "player.h"
#include "weapons.h"
#include "rocket.h"


enum awp_e
{
	AWP_IDLE,
	AWP_SHOOT1,
	AWP_SHOOT2,
	AWP_SHOOT3,
	AWP_RELOAD,
	AWP_DRAW
};

LINK_ENTITY_TO_CLASS(weapon_rpg, CRPG);

void CRPG::Spawn(void)
{
	pev->classname = MAKE_STRING("weapon_rpg");

	Precache();
	m_iId = WEAPON_RPG;
	SET_MODEL(ENT(pev), g_WpnItemInfo[m_iWpnId].szWModel );
	pev->body = g_WpnItemInfo[m_iWpnId].iWModelBody  ;
	pev->sequence = g_WpnItemInfo[m_iWpnId].iWModelSeq ;

	m_iDefaultAmmo = g_WpnItemInfo[m_iWpnId].iClip;
	FallInit();
}

void CRPG::Precache(void)
{
	PRECACHE_SOUND("weapons/zoom.wav");

	m_usFireExp = PRECACHE_EVENT(1, "events/rocketexplode.sc");
	m_usFireRocket = PRECACHE_EVENT(1, "events/rocketfire.sc");
}

int CRPG::GetItemInfo(ItemInfo *p)
{
	p->iId = m_iId = WEAPON_RPG;

	p->pszName = STRING(pev->classname);
	p->pszAmmo1 = "762Nato";
	p->iMaxAmmo1 = _762NATO_MAX_CARRY;
	p->pszAmmo2 = NULL;
	p->iMaxAmmo2 = -1;
	p->iMaxClip =  g_WpnItemInfo[m_iWpnId].iClip;
	p->iSlot = 0;
	p->iPosition = 3;
	p->iFlags = 0;
	p->iWeight = 30;

	return 1;
}

BOOL CRPG::Deploy(void)
{

	m_pPlayer->m_iWpnState = 0;

	if (DefaultDeploy(g_WpnItemInfo[m_iWpnId].szVModel , g_WpnItemInfo[m_iWpnId].szPModel , AWP_DRAW, g_WpnItemInfo[m_iWpnId].szAnimExtention , UseDecrement() != FALSE))
	{
		m_flNextPrimaryAttack = m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + 1.4;
		m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + 1;
		return TRUE;
	}

	return FALSE;
}

void CRPG::SecondaryAttack(void)
{
	if(m_pPlayer->m_iWpnState & WPNSTATE_FASTRUN)
		return;

	if(g_WpnItemInfo[m_iWpnId].iRightBtnFunc == FUNC_SIGHT)
	{
		if(g_WpnItemInfo[m_iWpnId].iScopeType == SCOPE_SNIPER)
		{
			if(m_pPlayer->m_iFOV == 90)
			{
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 40;

				MESSAGE_BEGIN(MSG_ONE_UNRELIABLE, gmsgMetaHook, NULL, m_pPlayer->pev);
				WRITE_BYTE(MH_MSG_SCOPE);
				WRITE_BYTE(m_iWpnId);
				MESSAGE_END();
				
				m_pPlayer->m_iWpnState |= WPNSTATE_SCOPE;
			}
			else if(m_pPlayer->m_iFOV <= 40 && m_pPlayer->m_iFOV >10)
			{
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 10;
				m_pPlayer->m_iWpnState |= WPNSTATE_SCOPE;
			}
			else
			{
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 90;

				MESSAGE_BEGIN(MSG_ONE_UNRELIABLE, gmsgMetaHook, NULL, m_pPlayer->pev);
				WRITE_BYTE(MH_MSG_SCOPE);
				WRITE_BYTE(0);
				MESSAGE_END();

				m_pPlayer->m_iWpnState &= ~WPNSTATE_SCOPE;
			}

			m_pPlayer->ResetMaxSpeed();
			EMIT_SOUND(ENT(m_pPlayer->pev), CHAN_ITEM, "weapons/zoom.wav", 0.2, 2.4);
			m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + 0.3;

		}
		else if(g_WpnItemInfo[m_iWpnId].iScopeType == SCOPE_ACTION)
		{
			if( !m_iClip )
			{
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 90;
				m_pPlayer->m_iWpnState = 0;

				MESSAGE_BEGIN(MSG_ONE_UNRELIABLE, gmsgMetaHook, NULL, m_pPlayer->pev);
				WRITE_BYTE(MH_MSG_SCOPE);
				WRITE_BYTE(0);
				MESSAGE_END();
			}
			else if( (m_pPlayer->m_iWpnState & WPNSTATE_SCOPE_TIME2))
			{
				// Sight down
				m_pPlayer->m_iWpnState &= ~WPNSTATE_SIGHT;
				m_pPlayer->m_iWpnState &= ~WPNSTATE_SCOPE_TIME2;
				m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_DOWN;

				m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeDownTime ;

				SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimmScopeDown , UseDecrement() != FALSE);
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 90;

				MESSAGE_BEGIN(MSG_ONE_UNRELIABLE, gmsgMetaHook, NULL, m_pPlayer->pev);
				WRITE_BYTE(MH_MSG_SCOPE);
				WRITE_BYTE(0);
				MESSAGE_END();
			}
			else if( !(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT) )
			{
				// sight up
				m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_UP;
				SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimScopeUp  , UseDecrement() != FALSE);

				m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeUpTime;
			}
			else if( (m_pPlayer->m_iWpnState & WPNSTATE_SIGHT) && !(m_pPlayer->m_iWpnState & WPNSTATE_SCOPE_TIME1))
			{
				// sight up
				m_pPlayer->m_iWpnState |= WPNSTATE_SCOPE_TIME1;
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 40;

				m_flNextSecondaryAttack = UTIL_WeaponTimeBase() +  0.3;

				MESSAGE_BEGIN(MSG_ONE_UNRELIABLE, gmsgMetaHook, NULL, m_pPlayer->pev);
				WRITE_BYTE(MH_MSG_SCOPE);
				WRITE_BYTE(m_iWpnId);
				MESSAGE_END();
			}
			else if( (m_pPlayer->m_iWpnState & WPNSTATE_SCOPE_TIME1) && !(m_pPlayer->m_iWpnState & WPNSTATE_SCOPE_TIME2))
			{
				// sight up
				m_flNextSecondaryAttack = UTIL_WeaponTimeBase() +  0.3;
				m_pPlayer->m_iWpnState &= ~WPNSTATE_SCOPE_TIME1;
				m_pPlayer->m_iWpnState |= WPNSTATE_SCOPE_TIME2;
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 10;
			}
		}
	}
}

void CRPG::PrimaryAttack(void)
{
	RocketFire();
}

void CRPG::RocketFire(void)
{
	if (m_pPlayer->pev->fov != 90)
	{
		m_pPlayer->m_bResumeZoom = true;
		m_pPlayer->m_iLastZoom = m_pPlayer->m_iFOV;
		m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 90;
	}

	if (m_iClip <= 0)
	{
		if (m_fFireOnEmpty)
		{
			PlayEmptySound();
			m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flFireCycleTime;
		}

		return;
	}

	m_iClip--;
	m_pPlayer->pev->effects |= EF_MUZZLEFLASH;
	m_pPlayer->SetAnimation(PLAYER_ATTACK1);

	UTIL_MakeVectors(m_pPlayer->pev->v_angle + m_pPlayer->pev->punchangle);

	m_pPlayer->m_iWeaponVolume = BIG_EXPLOSION_VOLUME;
	m_pPlayer->m_iWeaponFlash = BRIGHT_GUN_FLASH;

	CRocket *pRocket = CRocket::Create( m_iWpnId,  m_pPlayer->pev, FALSE, m_usFireExp);

	PLAYBACK_EVENT_FULL(0, m_pPlayer->edict(), m_usFireRocket, 0, pRocket->pev ->origin , (float *)&g_vecZero, 0.0, 0.0, m_iWpnId, ENTINDEX(ENT(pRocket->pev)), FALSE, FALSE);
	m_flNextPrimaryAttack = m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flFireCycleTime;

	m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 2;
	m_pPlayer->pev->punchangle.x -= 8;
}
void CRPG::Reload(void)
{
	if (DefaultReload(g_WpnItemInfo[m_iWpnId].iClip, AWP_RELOAD, g_WpnItemInfo[m_iWpnId].flReloadTime ))
	{
		m_pPlayer->SetAnimation(PLAYER_RELOAD);

		if (m_pPlayer->pev->fov != 90)
		{
			if( g_WpnItemInfo[m_iWpnId].iScopeType == SCOPE_SNIPER )
			{
				m_pPlayer->pev->fov = m_pPlayer->m_iFOV = 10;
				SecondaryAttack();
			}
			
			else if(g_WpnItemInfo[m_iWpnId].iScopeType == SCOPE_ACTION)
			{
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 90;
				m_pPlayer->m_iWpnState = 0;

				MESSAGE_BEGIN(MSG_ONE_UNRELIABLE, gmsgMetaHook, NULL, m_pPlayer->pev);
				WRITE_BYTE(MH_MSG_SCOPE);
				WRITE_BYTE(0);
				MESSAGE_END();
			}
			
			
		}
	}
}

void CRPG::WeaponIdle(void)
{
	ResetEmptySound();
	m_pPlayer->GetAutoaimVector(AUTOAIM_10DEGREES);

	if (m_flTimeWeaponIdle > UTIL_WeaponTimeBase())
		return;

	if (m_iClip)
	{
		m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 60;
		SendWeaponAnim(AWP_IDLE, UseDecrement() != FALSE);
	}
}

float CRPG::GetMaxSpeed(void)
{
	if (m_pPlayer->m_iFOV == 90)
		return (255 - g_WpnItemInfo[m_iWpnId].iWeight);

	return (160 - g_WpnItemInfo[m_iWpnId].iWeight);
}