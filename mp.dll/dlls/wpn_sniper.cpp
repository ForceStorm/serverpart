/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "player.h"
#include "weapons.h"

LINK_ENTITY_TO_CLASS(weapon_sniper, CSniper);

void CSniper::Spawn(void)
{
	pev->classname = MAKE_STRING("weapon_sniper");

	Precache();
	m_iId = WEAPON_SNIPER;
	SET_MODEL(ENT(pev), g_WpnItemInfo[m_iWpnId].szWModel );
	pev->body = g_WpnItemInfo[m_iWpnId].iWModelBody  ;
	pev->sequence = g_WpnItemInfo[m_iWpnId].iWModelSeq ;

	m_iDefaultAmmo = g_WpnItemInfo[m_iWpnId].iClip;
	FallInit();
}

void CSniper::Precache(void)
{
	PRECACHE_SOUND("weapons/scout_fire-1.wav");
	PRECACHE_SOUND("weapons/scout_bolt.wav");
	PRECACHE_SOUND("weapons/scout_clipin.wav");
	PRECACHE_SOUND("weapons/scout_clipout.wav");
	PRECACHE_SOUND("weapons/zoom.wav");
	
	m_iShellId = m_iShell = PRECACHE_MODEL("models/rshell_big.mdl");
	m_usFireSniper = PRECACHE_EVENT(1, g_WpnItemInfo[m_iWpnId].szEvent );
}

int CSniper::GetItemInfo(ItemInfo *p)
{
	p->pszName = STRING(pev->classname);
	p->pszAmmo1 = "762Nato";
	p->iMaxAmmo1 = _762NATO_MAX_CARRY;
	p->pszAmmo2 = NULL;
	p->iMaxAmmo2 = -1;
	p->iMaxClip =  g_WpnItemInfo[m_iWpnId].iClip;
	p->iSlot = 0;
	p->iPosition = 9;
	p->iId = m_iId = WEAPON_SNIPER;
	p->iFlags = 0;
	p->iWeight = SCOUT_WEIGHT;

	return 1;
}

BOOL CSniper::Deploy(void)
{
	m_pPlayer->m_iWpnState = 0;

	if (DefaultDeploy(g_WpnItemInfo[m_iWpnId].szVModel , g_WpnItemInfo[m_iWpnId].szPModel , g_WpnItemInfo[m_iWpnId].iAnimDraw , g_WpnItemInfo[m_iWpnId].szAnimExtention, UseDecrement() != FALSE))
	{
		m_flNextPrimaryAttack = m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + 1.25;
		m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + 1;
		return TRUE;
	}

	return FALSE;
}

void CSniper::SecondaryAttack(void)
{
	if(m_pPlayer->m_iWpnState & WPNSTATE_FASTRUN)
		return;

	if(g_WpnItemInfo[m_iWpnId].iRightBtnFunc == FUNC_SIGHT)
	{
		if(g_WpnItemInfo[m_iWpnId].iScopeType == SCOPE_SNIPER)
		{
			if(m_pPlayer->m_iFOV == 90)
			{
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 40;

				MESSAGE_BEGIN(MSG_ONE_UNRELIABLE, gmsgMetaHook, NULL, m_pPlayer->pev);
				WRITE_BYTE(MH_MSG_SCOPE);
				WRITE_BYTE(m_iWpnId);
				MESSAGE_END();

				if( !(m_pPlayer->m_iWpnState & WPNSTATE_SCOPE) )
					m_pPlayer->m_iWpnState |= WPNSTATE_SCOPE;
			}
			else if(m_pPlayer->m_iFOV <= 40 && m_pPlayer->m_iFOV >10)
			{
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 10;

				if( !(m_pPlayer->m_iWpnState & WPNSTATE_SCOPE) )
					m_pPlayer->m_iWpnState |= WPNSTATE_SCOPE;
			}
			else
			{
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 90;

				MESSAGE_BEGIN(MSG_ONE_UNRELIABLE, gmsgMetaHook, NULL, m_pPlayer->pev);
				WRITE_BYTE(MH_MSG_SCOPE);
				WRITE_BYTE(0);
				MESSAGE_END();

				if( m_pPlayer->m_iWpnState & WPNSTATE_SCOPE )
					m_pPlayer->m_iWpnState &= ~WPNSTATE_SCOPE;
			}

			m_pPlayer->ResetMaxSpeed();
			EMIT_SOUND(ENT(m_pPlayer->pev), CHAN_ITEM, "weapons/zoom.wav", 0.2, 2.4);
			m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + 0.3;

		}
		else if(g_WpnItemInfo[m_iWpnId].iScopeType == SCOPE_ACTION)
		{
			if( !m_iClip )
			{
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 90;
				m_pPlayer->m_iWpnState = 0;

				MESSAGE_BEGIN(MSG_ONE_UNRELIABLE, gmsgMetaHook, NULL, m_pPlayer->pev);
				WRITE_BYTE(MH_MSG_SCOPE);
				WRITE_BYTE(0);
				MESSAGE_END();
			}
			else if( (m_pPlayer->m_iWpnState & WPNSTATE_SCOPE_TIME2))
			{
				// Sight down
				m_pPlayer->m_iWpnState &= ~WPNSTATE_SIGHT;
				m_pPlayer->m_iWpnState &= ~WPNSTATE_SCOPE_TIME2;
				m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_DOWN;

				m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeDownTime ;

				SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimmScopeDown , UseDecrement() != FALSE);
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 90;

				MESSAGE_BEGIN(MSG_ONE_UNRELIABLE, gmsgMetaHook, NULL, m_pPlayer->pev);
				WRITE_BYTE(MH_MSG_SCOPE);
				WRITE_BYTE(0);
				MESSAGE_END();
			}
			else if( !(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT) )
			{
				// sight up
				m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_UP;
				SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimScopeUp  , UseDecrement() != FALSE);

				m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeUpTime;
			}
			else if( (m_pPlayer->m_iWpnState & WPNSTATE_SIGHT) && !(m_pPlayer->m_iWpnState & WPNSTATE_SCOPE_TIME1))
			{
				// sight up
				m_pPlayer->m_iWpnState |= WPNSTATE_SCOPE_TIME1;
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 40;

				m_flNextSecondaryAttack = UTIL_WeaponTimeBase() +  0.3;

				MESSAGE_BEGIN(MSG_ONE_UNRELIABLE, gmsgMetaHook, NULL, m_pPlayer->pev);
				WRITE_BYTE(MH_MSG_SCOPE);
				WRITE_BYTE(m_iWpnId);
				MESSAGE_END();
			}
			else if( (m_pPlayer->m_iWpnState & WPNSTATE_SCOPE_TIME1) && !(m_pPlayer->m_iWpnState & WPNSTATE_SCOPE_TIME2))
			{
				// sight up
				m_flNextSecondaryAttack = UTIL_WeaponTimeBase() +  0.3;
				m_pPlayer->m_iWpnState &= ~WPNSTATE_SCOPE_TIME1;
				m_pPlayer->m_iWpnState |= WPNSTATE_SCOPE_TIME2;
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 10;
			}
		}
	}
}
void CSniper::PrimaryAttack(void)
{
	float flSpread;
	float flCycleTime;

	flSpread = g_WpnItemInfo[m_iWpnId].flFireSpread ;
	flCycleTime = g_WpnItemInfo[m_iWpnId].flFireCycleTime;

	if (m_pPlayer->pev->velocity.Length2D() > 170)
		SniperFire(
			flSpread * 1.015887, 
			flCycleTime, 
			FALSE);

	else if (FBitSet(m_pPlayer->pev->flags, FL_DUCKING))
		SniperFire(
			flSpread * 0.000000112, 
			flCycleTime, 
			FALSE);

	else
		SniperFire(
			flSpread, 
			flCycleTime, 
			FALSE);

	MESSAGE_BEGIN(MSG_ONE_UNRELIABLE, gmsgMetaHook, NULL, m_pPlayer->pev);
	WRITE_BYTE(MH_MSG_SCOPE);
	WRITE_BYTE(0);
	MESSAGE_END();
}

void CSniper::SniperFire(float flSpread, float flCycleTime, BOOL fUseAutoAim)
{
	if (m_pPlayer->pev->fov != 90)
	{
		m_pPlayer->m_bResumeZoom = true;
		m_pPlayer->m_iLastZoom = m_pPlayer->m_iFOV;
		m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 90;
	}
	else
		flSpread += 0.025;

	if (m_iClip <= 0)
	{
		if (m_fFireOnEmpty)
		{
			PlayEmptySound();
			m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + 0.2;
		}

		return;
	}

	m_iClip--;
	m_pPlayer->pev->effects |= EF_MUZZLEFLASH;
	m_pPlayer->SetAnimation(PLAYER_ATTACK1);

	UTIL_MakeVectors(m_pPlayer->pev->v_angle + m_pPlayer->pev->punchangle);

	m_pPlayer->m_flEjectBrass = gpGlobals->time + 0.56;
	m_pPlayer->m_iWeaponVolume = BIG_EXPLOSION_VOLUME;
	m_pPlayer->m_iWeaponFlash = NORMAL_GUN_FLASH;

	Vector vecDir = FireBullets3(
		m_pPlayer->GetGunPosition(), 
		gpGlobals->v_forward, 
		flSpread, 
		g_WpnItemInfo[m_iWpnId].iBulletDistance , 
		g_WpnItemInfo[m_iWpnId].iBulletPenetration , 
		g_WpnItemInfo[m_iWpnId].iBulletType, 
		g_WpnItemInfo[m_iWpnId].iBulletDamage , 
		g_WpnItemInfo[m_iWpnId].flRangeModifier , 
		m_pPlayer->pev, 
		TRUE, 
		m_pPlayer->random_seed);

	PLAYBACK_EVENT_FULL(
		0, 
		ENT(m_pPlayer->pev), 
		m_usFireSniper, 
		0, 
		(float *)&g_vecZero, 
		(float *)&g_vecZero, 
		vecDir.x * 1000, 
		vecDir.y * 1000, 
		(int)(m_pPlayer->pev->punchangle.x * 100), 
		(int)(m_pPlayer->pev->punchangle.x * 100), 
		FALSE, 
		FALSE);

	m_flNextPrimaryAttack = m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + flCycleTime;

	if (!m_iClip && m_pPlayer->m_rgAmmo[m_iWpnId] <= 0)
		m_pPlayer->SetSuitUpdate("!HEV_AMO0", FALSE, 0);

	m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 1.8;
	m_pPlayer->pev->punchangle.x -= 2;

}

void CSniper::Reload(void)
{
	if (DefaultReload(g_WpnItemInfo[m_iWpnId].iClip, g_WpnItemInfo[m_iWpnId].iAnimReload, g_WpnItemInfo[m_iWpnId].flReloadTime))
	{
		m_pPlayer->SetAnimation(PLAYER_RELOAD);

		
		if (m_pPlayer->pev->fov != 90)
		{
			if( g_WpnItemInfo[m_iWpnId].iScopeType == SCOPE_SNIPER )
			{
				m_pPlayer->pev->fov = m_pPlayer->m_iFOV = 10;
				SecondaryAttack();
			}
			
			else if(g_WpnItemInfo[m_iWpnId].iScopeType == SCOPE_ACTION)
			{
				m_pPlayer->m_iFOV = m_pPlayer->pev->fov = 90;
				m_pPlayer->m_iWpnState = 0;

				MESSAGE_BEGIN(MSG_ONE_UNRELIABLE, gmsgMetaHook, NULL, m_pPlayer->pev);
				WRITE_BYTE(MH_MSG_SCOPE);
				WRITE_BYTE(0);
				MESSAGE_END();
			}
			
			
		}
	}
}

void CSniper::WeaponIdle(void)
{
	ResetEmptySound();
	m_pPlayer->GetAutoaimVector(AUTOAIM_10DEGREES);

	if (m_flTimeWeaponIdle > UTIL_WeaponTimeBase())
		return;

	if (m_iClip)
	{
		m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 60;
		SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimIdle, UseDecrement() != FALSE);
	}
}

float CSniper::GetMaxSpeed(void)
{
	if (m_pPlayer->m_iFOV == 90)
		return (255 - g_WpnItemInfo[m_iWpnId].iWeight);

	return (160 - g_WpnItemInfo[m_iWpnId].iWeight);
}