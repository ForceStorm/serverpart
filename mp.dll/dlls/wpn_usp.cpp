/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "player.h"
#include "weapons.h"

enum usp_e
{
	USP_IDLE,
	USP_SHOOT1,
	USP_SHOOT2,
	USP_SHOOT3,
	USP_SHOOT_EMPTY,
	USP_RELOAD,
	USP_DRAW,
	USP_ATTACH_SILENCER,
	USP_UNSIL_IDLE,
	USP_UNSIL_SHOOT1,
	USP_UNSIL_SHOOT2,
	USP_UNSIL_SHOOT3,
	USP_UNSIL_SHOOT_EMPTY,
	USP_UNSIL_RELOAD,
	USP_UNSIL_DRAW,
	USP_DETACH_SILENCER
};

LINK_ENTITY_TO_CLASS(weapon_usp, CUSP);

void CUSP::Spawn(void)
{
	pev->classname = MAKE_STRING("weapon_usp");

	Precache();
	m_iId = WEAPON_USP;
	SET_MODEL(ENT(pev), g_WpnItemInfo[m_iWpnId].szWModel );
	pev->body = g_WpnItemInfo[m_iWpnId].iWModelBody  ;
	pev->sequence = g_WpnItemInfo[m_iWpnId].iWModelSeq ;

	m_iDefaultAmmo = g_WpnItemInfo[m_iWpnId].iClip;
	m_flAccuracy = 0.92;
	m_iWeaponState &= ~WPNSTATE_SHIELD_DRAWN;

	FallInit();
}

void CUSP::Precache(void)
{
	PRECACHE_SOUND("weapons/usp1.wav");
	PRECACHE_SOUND("weapons/usp2.wav");
	PRECACHE_SOUND("weapons/usp_unsil-1.wav");
	PRECACHE_SOUND("weapons/usp_clipout.wav");
	PRECACHE_SOUND("weapons/usp_clipin.wav");
	PRECACHE_SOUND("weapons/usp_silencer_on.wav");
	PRECACHE_SOUND("weapons/usp_silencer_off.wav");
	PRECACHE_SOUND("weapons/usp_sliderelease.wav");
	PRECACHE_SOUND("weapons/usp_slideback.wav");

	m_usFire1 = PRECACHE_EVENT(1, g_WpnItemInfo[m_iWpnId].szEvent );
	m_usFire2 = PRECACHE_EVENT(1, g_WpnItemInfo[m_iWpnId].szEvent2 );
}

int CUSP::GetItemInfo(ItemInfo *p)
{
	p->pszName = STRING(pev->classname);
	p->pszAmmo1 = "45acp";
	p->iMaxAmmo1 = _45ACP_MAX_CARRY;
	p->pszAmmo2 = NULL;
	p->iMaxAmmo2 = -1;
	p->iMaxClip =  g_WpnItemInfo[m_iWpnId].iClip;
	p->iSlot = 1;
	p->iPosition = 4;
	p->iId = m_iId = WEAPON_USP;
	p->iFlags = 0;
	p->iWeight = USP_WEIGHT;

	return 1;
}

BOOL CUSP::Deploy(void)
{
	m_pPlayer->m_iWpnState = 0;
	m_flAccuracy = 0.92;
	m_fMaxSpeed = 250;
	m_iWeaponState &= ~WPNSTATE_SHIELD_DRAWN;
	m_pPlayer->m_bShieldDrawn = false;

	return DefaultDeploy(g_WpnItemInfo[m_iWpnId].szVModel , g_WpnItemInfo[m_iWpnId].szPModel ,  (m_iWeaponState & WPNSTATE_USP_SILENCED) ? USP_DRAW : USP_UNSIL_DRAW, g_WpnItemInfo[m_iWpnId].szAnimExtention, UseDecrement() != FALSE);
}

void CUSP::FovChange(void)
{
	if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT_DOWN)
	{
		// sight down
		if(m_pPlayer->m_iFOV >= 90)
		{
			m_pPlayer->SendFOV( 90);
			return;
		}
		m_pPlayer->SendFOV( m_pPlayer->m_iFOV + 1);
		pev->nextthink = gpGlobals->time + g_WpnItemInfo[m_iWpnId].flScopeDownTime / (90 - g_WpnItemInfo[m_iWpnId].iMinFov);
	}
	else if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT_UP)
	{
		// sight up
		if(m_pPlayer->m_iFOV <= g_WpnItemInfo[m_iWpnId].iMinFov)
		{
			m_pPlayer->SendFOV( g_WpnItemInfo[m_iWpnId].iMinFov);
			return;
		}
		m_pPlayer->SendFOV( m_pPlayer->m_iFOV - 1);
		pev->nextthink = gpGlobals->time + g_WpnItemInfo[m_iWpnId].flScopeUpTime / (90- g_WpnItemInfo[m_iWpnId].iMinFov);
	}
	
}
void CUSP::SecondaryAttack(void)
{
	if(m_pPlayer->m_iWpnState & WPNSTATE_FASTRUN)
		return;

	if(g_WpnItemInfo[m_iWpnId].iRightBtnFunc == FUNC_SIGHT)
	{
		if(m_pPlayer->m_iWpnState & WPNSTATE_SIGHT)
		{
			// Sight down

			m_pPlayer->m_iWpnState &= ~WPNSTATE_SIGHT;
			m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_DOWN;

			m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeDownTime ;

			m_pPlayer->pev->viewmodel = MAKE_STRING( g_WpnItemInfo[m_iWpnId].szVModel );
			SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimmScopeDown , UseDecrement() != FALSE);

			SetThink(&CUSP::FovChange);
			pev->nextthink = gpGlobals->time;
		}
		else
		{
			// sight up
			m_pPlayer->m_iWpnState |= WPNSTATE_SIGHT_UP;
			SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimScopeUp  , UseDecrement() != FALSE);

			m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flScopeUpTime;
			
			SetThink(&CUSP::FovChange);
			pev->nextthink = gpGlobals->time;
		}
	}
	else if(g_WpnItemInfo[m_iWpnId].iRightBtnFunc == FUNC_SILENCER)
	{
		if (m_iWeaponState & WPNSTATE_USP_SILENCED)
		{
			m_iWeaponState &= ~WPNSTATE_USP_SILENCED;
			SendWeaponAnim(USP_DETACH_SILENCER, UseDecrement() != FALSE);
		}
		else
		{
			m_iWeaponState |= WPNSTATE_USP_SILENCED;
			SendWeaponAnim(USP_ATTACH_SILENCER, UseDecrement() != FALSE);
		}

		m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 3;
		m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + 3;
		m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + 3;
	}
}

void CUSP::PrimaryAttack(void)
{
	float flSpread;
	float flCycleTime;

	if (m_iWeaponState & WPNSTATE_GLOCK18_BURST_MODE)
	{
		flSpread = g_WpnItemInfo[m_iWpnId].flFireSpread ;
		flCycleTime = g_WpnItemInfo[m_iWpnId].flFireCycleTime;
	}
	else
	{
		flSpread = g_WpnItemInfo[m_iWpnId].flFireSpread2 ;
		flCycleTime = g_WpnItemInfo[m_iWpnId].flFireCycleTime2;
	}


	if (!FBitSet(m_pPlayer->pev->flags, FL_ONGROUND))
		USPFire(
			flSpread * 1.131275 * (1 - m_flAccuracy), 
			flCycleTime, 
			FALSE);

	else if (m_pPlayer->pev->velocity.Length2D() > 20)
		USPFire(
			flSpread * 1.011452 * (1 - m_flAccuracy), 
			flCycleTime, 
			FALSE);

	else if (FBitSet(m_pPlayer->pev->flags, FL_DUCKING))
		USPFire(
			flSpread * 0.99851 * (1 - m_flAccuracy), 
			flCycleTime, 
			FALSE);

	else
		USPFire(
			flSpread * (1 - m_flAccuracy), 
			flCycleTime, 
			FALSE);

}

void CUSP::USPFire(float flSpread, float flCycleTime, BOOL fUseAutoAim)
{
	flCycleTime -= 0.075;
	m_iShotsFired++;

	if (m_iShotsFired > 1)
		return;

	if (m_flLastFire)
	{
		m_flAccuracy -= (g_WpnItemInfo[m_iWpnId].flFireAccuracyBase - (gpGlobals->time - m_flLastFire)) * 0.275;

		if (m_flAccuracy > 0.92)
			m_flAccuracy = 0.92;
		else if (m_flAccuracy < 0.6)
			m_flAccuracy = 0.6;
	}

	m_flLastFire = gpGlobals->time;

	if (m_iClip <= 0)
	{
		if (m_fFireOnEmpty)
		{
			PlayEmptySound();
			m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + 0.2;
		}

		return;
	}

	m_iClip--;
	m_pPlayer->SetAnimation(PLAYER_ATTACK1);

	m_pPlayer->m_iWeaponVolume = BIG_EXPLOSION_VOLUME;
	m_pPlayer->m_iWeaponFlash = DIM_GUN_FLASH;

	UTIL_MakeVectors(m_pPlayer->pev->v_angle + m_pPlayer->pev->punchangle);

	if (!(m_iWeaponState & WPNSTATE_USP_SILENCED))
		m_pPlayer->pev->effects |= EF_MUZZLEFLASH;

	int iDamage = (m_iWeaponState & WPNSTATE_USP_SILENCED) ? 30 : 34;

	if(m_iWeaponState & WPNSTATE_USP_SILENCED)
	{
		
		Vector vecDir = FireBullets3(
			m_pPlayer->GetGunPosition(), 
			gpGlobals->v_forward, 
			flSpread, 
			g_WpnItemInfo[m_iWpnId].iBulletDistance2 , 
			g_WpnItemInfo[m_iWpnId].iBulletPenetration2  , 
			g_WpnItemInfo[m_iWpnId].iBulletType2 , 
			g_WpnItemInfo[m_iWpnId].iBulletDamage2 , 
			g_WpnItemInfo[m_iWpnId].flRangeModifier2  , 
			m_pPlayer->pev, 
			TRUE, 
			m_pPlayer->random_seed);

		PLAYBACK_EVENT_FULL(0, ENT(m_pPlayer->pev), m_usFire1, 0, (float *)&g_vecZero, (float *)&g_vecZero, vecDir.x, vecDir.y, (int)(m_pPlayer->pev->punchangle.x * 100), 0, m_iClip != 0, m_iWeaponState & WPNSTATE_USP_SILENCED);

	}
	else
	{
		Vector vecDir = FireBullets3(
			m_pPlayer->GetGunPosition(), 
			gpGlobals->v_forward, 
			flSpread, 
			g_WpnItemInfo[m_iWpnId].iBulletDistance , 
			g_WpnItemInfo[m_iWpnId].iBulletPenetration  , 
			g_WpnItemInfo[m_iWpnId].iBulletType , 
			g_WpnItemInfo[m_iWpnId].iBulletDamage , 
			g_WpnItemInfo[m_iWpnId].flRangeModifier  , 
			m_pPlayer->pev, 
			TRUE, 
			m_pPlayer->random_seed);

		PLAYBACK_EVENT_FULL(0, ENT(m_pPlayer->pev), m_usFire2, 0, (float *)&g_vecZero, (float *)&g_vecZero, vecDir.x, vecDir.y, (int)(m_pPlayer->pev->punchangle.x * 100), 0, m_iClip != 0, m_iWeaponState & WPNSTATE_USP_SILENCED);

	}
	

	m_flNextPrimaryAttack = m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + flCycleTime;

	if (!m_iClip && m_pPlayer->m_rgAmmo[m_iWpnId] <= 0)
		m_pPlayer->SetSuitUpdate("!HEV_AMO0", FALSE, 0);

	m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 2;
	m_pPlayer->pev->punchangle.x -= 2;
	ResetPlayerShieldAnim();

}

void CUSP::Reload(void)
{
	int iAnim;

	if (m_iWeaponState & WPNSTATE_USP_SILENCED)
		iAnim = USP_RELOAD;
	else
		iAnim = USP_UNSIL_RELOAD;

	if (DefaultReload(g_WpnItemInfo[m_iWpnId].iClip, iAnim, g_WpnItemInfo[m_iWpnId].flReloadTime))
	{
		m_pPlayer->SetAnimation(PLAYER_RELOAD);
		m_flAccuracy = 0.92;
	}
}

void CUSP::WeaponIdle(void)
{
	ResetEmptySound();
	m_pPlayer->GetAutoaimVector(AUTOAIM_10DEGREES);

	if (m_flTimeWeaponIdle > UTIL_WeaponTimeBase())
		return;

	if (m_pPlayer->HasShield())
	{
		m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 20;

		if (m_iWeaponState & WPNSTATE_SHIELD_DRAWN)
			SendWeaponAnim(USP_DRAW, UseDecrement() != FALSE);

		return;
	}

	if (m_iClip)
	{
		m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 60;

		if (m_iWeaponState & WPNSTATE_USP_SILENCED)
			SendWeaponAnim(USP_IDLE, UseDecrement() != FALSE);
		else
			SendWeaponAnim(USP_UNSIL_IDLE, UseDecrement() != FALSE);
	}
}