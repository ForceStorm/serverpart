/***
*
*	Copyright (c) 1996-2002, Valve LLC. All rights reserved.
*
*	This product contains software technology licensed from Id
*	Software, Inc. ("Id Technology").  Id Technology (c) 1996 Id Software, Inc.
*	All Rights Reserved.
*
*   Use, distribution, and modification of this source code and/or resulting
*   object code is restricted to non-commercial enhancements to products from
*   Valve LLC.  All other use, distribution, or modification is prohibited
*   without written permission from Valve LLC.
*
****/

#include "extdll.h"
#include "util.h"
#include "cbase.h"
#include "player.h"
#include "weapons.h"
#include "hltv.h"
#include "gamerules.h"

LINK_ENTITY_TO_CLASS(weapon_wpnitem, CWpnitem);

void CWpnitem::Spawn(void)
{
	pev->classname = MAKE_STRING("weapon_wpnitem");

	m_iId = WEAPON_WPNITEM;
	
	m_iDefaultAmmo = HEGRENADE_DEFAULT_GIVE;

	FallInit();
}

void CWpnitem::Precache(void)
{
}

int CWpnitem::GetItemInfo(ItemInfo *p)
{
	p->iId = m_iId = WEAPON_WPNITEM;
	return 1;
}

BOOL CWpnitem::Deploy(void)
{
	m_pPlayer->m_iWpnState = 0;
	pev->body = 0;

	return DefaultDeploy(g_WpnItemInfo[m_iWpnId].szVModel , g_WpnItemInfo[m_iWpnId].szPModel , g_WpnItemInfo[m_iWpnId].iAnimDraw, g_WpnItemInfo[m_iWpnId].szAnimExtention, UseDecrement() != FALSE);
}

void CWpnitem::Holster(int skiplocal)
{
	m_pPlayer->m_flNextAttack = UTIL_WeaponTimeBase() + 0.5;

	if( m_flItemRetireTime && gpGlobals->time > m_flItemRetireTime )
	{
		DestroyItem();
	}
}

void CWpnitem::PrimaryAttack(void)
{
	if( !strcmp( g_WpnItemInfo[m_iWpnId].szWpnName , "painkiller" ) )
	{
		PainkillerUse();
		return;
	}
	else if( !strcmp( g_WpnItemInfo[m_iWpnId].szWpnName , "adrenalin") )
	{
		AdrenalinUse();
		return;
	}
}

void CWpnitem::PainkillerUse(void)
{
	m_pPlayer->SetAnimation(PLAYER_ATTACK1);
	SendWeaponAnim( g_WpnItemInfo[m_iWpnId].iAnimUse ,0 );

	m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flAttackAnimTime1 + 0.2;
	m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flAttackAnimTime1;
	m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flAttackAnimTime1;

	m_flItemRetireTime = gpGlobals->time + g_WpnItemInfo[m_iWpnId].flAttackAnimTime1;
}

void CWpnitem::AdrenalinUse(void)
{
	m_pPlayer->SetAnimation(PLAYER_ATTACK1);
	SendWeaponAnim( g_WpnItemInfo[m_iWpnId].iAnimUse ,0 );

	m_flNextPrimaryAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flAttackAnimTime1 + 0.2;
	m_flNextSecondaryAttack = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flAttackAnimTime1;
	m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + g_WpnItemInfo[m_iWpnId].flAttackAnimTime1;

	m_flItemRetireTime = gpGlobals->time + g_WpnItemInfo[m_iWpnId].flAttackAnimTime1;
}

void CWpnitem::WeaponIdle(void)
{
	if( m_flItemRetireTime && gpGlobals->time > m_flItemRetireTime)
	{
		if( !strcmp( g_WpnItemInfo[m_iWpnId].szWpnName , "painkiller" ) )
		{
			int iNewHealth = min( g_WpnItemInfo[m_iWpnId].iHealthAdd + m_pPlayer->pev->health,    m_pPlayer->pev->max_health  );
			m_pPlayer->pev->health = iNewHealth;

			RetireWeapon();
		}
		else if( !strcmp( g_WpnItemInfo[m_iWpnId].szWpnName , "adrenalin") )
		{
			int iNewHealth = min( g_WpnItemInfo[m_iWpnId].iHealthAdd + m_pPlayer->pev->health,    m_pPlayer->pev->max_health  );
			m_pPlayer->pev->health = iNewHealth;
			m_pPlayer->SetAdrenalinState (7.0, g_WpnItemInfo[m_iWpnId].iSpeedAdd);

			RetireWeapon();
		}
		return;
	}
	else
	{
		if (m_flTimeWeaponIdle > UTIL_WeaponTimeBase())
			return;

		m_flTimeWeaponIdle = UTIL_WeaponTimeBase() + 20;
		SendWeaponAnim(g_WpnItemInfo[m_iWpnId].iAnimIdle, UseDecrement() != FALSE);
		
	}

}
