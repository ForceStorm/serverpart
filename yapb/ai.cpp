//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#include "yapb.h"

int CBot::GetMessageQueue (void)
{
   // this function get the current message from the bots message queue

   int iMessage = m_aMessageQueue[m_iActMessageIndex++];
   m_iActMessageIndex &= 0x1f; // wraparound

   return iMessage;
}

void CBot::PushMessageQueue (int iMessage)
{
   // this function put a message into the bot message queue

   if (iMessage == MESSAGE_SAY)
   {
      // notify other bots of the spoken text otherwise, bots won't respond to other bots (network messages aren't sent from bots)
      int iEntIndex = GetIndex ();

      for (int iBotIndex = 0; iBotIndex < MaxClients (); iBotIndex++)
      {
         CBot *pOtherBot = g_pBotManager->GetBot (iBotIndex);

         if ((pOtherBot != NULL) && (pOtherBot->pev != pev))
         {
            if (IsAlive (GetEntity ()) == IsAlive (pOtherBot->GetEntity ()))
            {
               pOtherBot->m_sSaytextBuffer.iEntityIndex = iEntIndex;
               strcpy (pOtherBot->m_sSaytextBuffer.szSayText, m_szMiscStrings);
            }
            pOtherBot->m_sSaytextBuffer.fTimeNextChat = WorldTime () + pOtherBot->m_sSaytextBuffer.fChatDelay;
         }
      }
   }
   m_aMessageQueue[m_iPushMessageIndex++] = iMessage;
   m_iPushMessageIndex &= 0x1f; // wraparound
}

int CBot::InFieldOfView (Vector vDest)
{
   float fEntAngle = AngleMod (VecToYaw (vDest)); // find yaw angle from source to destination...
   float fViewAngle = AngleMod (pev->v_angle.y); // get bot's current view angle...

   // return the absolute value of angle to destination entity
   // zero degrees means straight ahead, 45 degrees to the left or
   // 45 degrees to the right is the limit of the normal view angle
   int iAngle = abs (static_cast <int> (fViewAngle) - static_cast <int> (fEntAngle));
  
   if (iAngle > 180)
      iAngle = 360 - iAngle;
   
   return iAngle;
}

bool CBot::IsInViewCone (Vector vOrigin)
{
   // this function returns true if the spatial vector location vOrigin is located inside
   // the field of view cone of the bot entity, false otherwise. It is assumed that entities
   // have a human-like field of view, that is, about 90 degrees.

   return ::IsInViewCone (vOrigin, GetEntity ());
}

bool CBot::CheckVisibility (entvars_t *pevTarget, Vector *pvOrigin, byte *pucBodyId)
{
   // this function checks visibility of a bot target.

   Vector vBotHead = EyePosition () + Vector (0, 0, 3);
   TraceResult tr;

   *pucBodyId = NULL;

   // check for the head
   TraceLine (vBotHead, pevTarget->origin + pevTarget->view_ofs, true, true, GetEntity (), &tr);

   if (tr.flFraction >= 1.0)
   {
      *pucBodyId |= VISIBLE_HEAD;
      *pvOrigin = tr.vecEndPos;
   }

   // check for the body
   TraceLine (vBotHead, pevTarget->origin, true, true, GetEntity (), &tr);

   if (tr.flFraction >= 1.0)
   {
      *pucBodyId |= VISIBLE_BODY;
      *pvOrigin = tr.vecEndPos + Vector (0, 0, 3);
   }
   
   if (*pucBodyId != NULL)
      return true;

   // worst case, choose random position in enemy body
   for (int i = 0; i < 5; i++)
   {
      Vector vTarget = pevTarget->origin; // get the player origin

      // find the vector beetwen mins and maxs of the player body
      vTarget.x += RandomFloat (pevTarget->mins.x, pevTarget->maxs.x);
      vTarget.y += RandomFloat (pevTarget->mins.y, pevTarget->maxs.y);
      vTarget.z += RandomFloat (pevTarget->mins.z, pevTarget->maxs.z);

      // check direct line to random part of the player body
      TraceLine (vBotHead, vTarget, true, true, GetEntity (), &tr);

      // check if we hit something
      if (tr.flFraction >= 1.0)
      {
         *pvOrigin = tr.vecEndPos;
         *pucBodyId |= VISIBLE_OTHER;

         return true;
      }
   }
   return false;
}
  
bool CBot::IsEnemyViewable (edict_t *pentPlayer)
{
   if (FNullEnt (pentPlayer))
      return false;

   bool bForceTrueIfVisible = false;

   if (IsValidPlayer (pev->dmg_inflictor) && (GetTeam (pev->dmg_inflictor) != GetTeam (GetEntity ())) && ::IsInViewCone (EyePosition (), pev->dmg_inflictor))
      bForceTrueIfVisible = true;

   if (CheckVisibility (VARS (pentPlayer), &m_vEnemy, &m_ucVisibility) && (IsInViewCone (pentPlayer->v.origin + Vector (0, 0, 14)) || bForceTrueIfVisible))
   {
      m_fSeeEnemyTime = WorldTime ();
      m_pentLastEnemy = pentPlayer;
      m_vLastEnemyOrigin = pentPlayer->v.origin;

      return true;
   }

   return false;
}

bool CBot::ItemIsVisible (Vector vDest, char *szItemName)
{
   TraceResult tr;

   // trace a line from bot's eyes to destination..
   TraceLine (EyePosition (), vDest, true, GetEntity (), &tr);

   // check if line of sight to object is not blocked (i.e. visible)
   if (tr.flFraction != 1.0)
      return strcmp (STRING (tr.pHit->v.classname), szItemName) == 0;

   return true;
}

bool CBot::EntityIsVisible (Vector vDest, bool bFromBody)
{
   TraceResult tr;

   // trace a line from bot's eyes to destination...
   TraceLine (bFromBody ? pev->origin : EyePosition (), vDest, true, true, GetEntity (), &tr);

   // check if line of sight to object is not blocked (i.e. visible)
   return tr.flFraction >= 1.0;
}

void CBot::AvoidGrenades (void)
{
   // checks if bot 'sees' a grenade, and avoid it

   edict_t *pent = m_pentAvoidGrenade;
   float fDistance;

  // check if old pointers to grenade is invalid
   if (FNullEnt (pent))
   {
      m_pentAvoidGrenade = NULL;
      m_cAvoidGrenade = 0;
   }
   else if ((pent->v.flags & FL_ONGROUND) || (pent->v.effects & EF_NODRAW))
   {
      m_pentAvoidGrenade = NULL;
      m_cAvoidGrenade = 0;
   }
   pent = NULL;

   // find all grenades on the map
   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "grenade")))
   {
      // if grenade is invisible don't care for it
      if (pent->v.effects & EF_NODRAW)
         continue;

      // check if visible to the bot
      if (!EntityIsVisible (pent->v.origin) && (InFieldOfView (pent->v.origin - EyePosition ()) > pev->fov / 3))
         continue;

      if ((m_iSkill >= 50) && (FStrEq (STRING (pent->v.model) + 9, "flashbang.mdl")))
      {
         Vector vPosition = VecToAngles (GetEntityOrigin (pent) - EyePosition ());

         // don't look at flashbang, only if we're highskilled
         if (pent->v.owner != GetEntity ())
            pev->v_angle.y = AngleNormalize (vPosition.y  + 180.0);
      }
      else if (FStrEq (STRING (pent->v.model) + 9, "hegrenade.mdl"))
      {
         if (FNullEnt (m_pentAvoidGrenade))
         {
           if ((GetTeam (pent->v.owner) == GetTeam (GetEntity ())) && (pent->v.owner != GetEntity ()))
               return;

            if ((pent->v.flags & FL_ONGROUND) == 0)
            {
               fDistance = (pent->v.origin - pev->origin).Length ();
               float fDistanceMoved = ((pent->v.origin + pent->v.velocity * g_fTimeFrameInterval) - pev->origin).Length ();

               if ((fDistanceMoved < fDistance) && (fDistance < 500))
               {
                  Vector2D v2DirToPoint, v2RightSide;
                  MakeVectors (pev->v_angle);

                  v2DirToPoint = (pev->origin - pent->v.origin).Make2D ().Normalize ();
                  v2RightSide = (g_pGlobals->v_right.Make2D ().Normalize ());

                  if (DotProduct (v2DirToPoint, v2RightSide) > 0)
                     m_cAvoidGrenade = -1;
                  else
                     m_cAvoidGrenade = 1;

                  m_pentAvoidGrenade = pent;
               }
            }
         }
      }
      if (FStrEq (STRING (pent->v.model) + 9, "smokegrenade.mdl") && (pent->v.flags & (FL_ONGROUND | FL_PARTIALGROUND)))
      {
         fDistance = (pent->v.origin - pev->origin).Length ();

         // shrink bot's viewing distance to smoke grenade's distance
         if (m_fViewDistance > fDistance)
            m_fViewDistance = fDistance;
      }
   }
}

int CBot::GetBestWeaponCarried (void)
{
   // this function returns the best weapon of this bot (based on personality prefs)

   int *ptrWeaponTab = g_ptrWeaponPrefs[m_ucPersonality];
   int iWeaponIndex = 0;
   int iWeapons = pev->weapons;

   tWeaponSelect *pWeaponTab = &g_rgkWeaponSelect[0];

   // take the shield in account
   if (HasShield ())
      iWeapons |= (1 << WEAPON_SHIELDGUN);

   for (int i = 0; i < NUM_WEAPONS; i++)
   {
      if (iWeapons & (1 << pWeaponTab[*ptrWeaponTab].iId))
         iWeaponIndex = i;

      ptrWeaponTab++;
   }
   return iWeaponIndex;
}

int CBot::GetBestSecondaryWeaponCarried (void)
{
   // this function returns the best secondary weapon of this bot (based on personality prefs)

   int *ptrWeaponTab = g_ptrWeaponPrefs[m_ucPersonality];
   int iWeaponIndex = 0;
   int iWeapons = pev->weapons;

   // take the shield in account
   if (HasShield ())
      iWeapons |= (1 << WEAPON_SHIELDGUN);

   tWeaponSelect *pWeaponTab = &g_rgkWeaponSelect[0];

   for (int i = 0; i < NUM_WEAPONS; i++)
   {
      int iId = pWeaponTab[*ptrWeaponTab].iId;

      if ((iWeapons & (1 << pWeaponTab[*ptrWeaponTab].iId)) && ((iId == WEAPON_USP) || (iId == WEAPON_GLOCK18) || (iId == WEAPON_DEAGLE) || (iId == WEAPON_P228) || (iId == WEAPON_ELITE) || (iId == WEAPON_FIVESEVEN)))
         iWeaponIndex = i;

      ptrWeaponTab++;
   }
   return iWeaponIndex; 
}

bool CBot::RateGroundWeapon (edict_t *pent)
{
   // this function compares weapons on the ground to the one the bot is using

   int iHasWeapon = GetBestWeaponCarried ();

   int *ptrWeaponTab = g_ptrWeaponPrefs[m_ucPersonality];
   tWeaponSelect *pWeaponTab = g_rgkWeaponSelect;

   int iGroundIndex = 0;
   char szModelName[40];

   strcpy (szModelName, STRING (pent->v.model) + 9);

   for (int i = 0; i < NUM_WEAPONS; i++)
   {
      if (FStrEq (pWeaponTab[*ptrWeaponTab].szModelName, szModelName))
         iGroundIndex = i;

      ptrWeaponTab++;
   }

   if (iGroundIndex < 7)
   {
      ptrWeaponTab = g_ptrWeaponPrefs[m_ucPersonality];
      iHasWeapon = 0;

      for (int i = 0; i < 7; i++)
      {
         if (pev->weapons & (1 << pWeaponTab[*ptrWeaponTab].iId))
            iHasWeapon = i;

         ptrWeaponTab++;
      }
   }
   return iGroundIndex > iHasWeapon;
}

edict_t *CBot::FindBreakable (void)
{
   // this function checks if bot is blocked by a shootable breakable in his moving direction

   TraceResult tr;
   TraceLine (pev->origin, pev->origin + (m_vDestOrigin - pev->origin).Normalize () * 64, false, false, GetEntity (), &tr);
  
   if (tr.flFraction != 1.0)
   {
      edict_t *pent = tr.pHit;

      // check if this isn't a triggered (bomb) breakable and if it takes damage. if true, shoot the crap!
      if (IsShootableBreakable (pent))
      {
         m_vBreakable = tr.vecEndPos;
         return pent;
      }
   }
   TraceLine (EyePosition (), EyePosition () + (m_vDestOrigin - EyePosition ()).Normalize () * 64, false, false, GetEntity (), &tr);

   if (tr.flFraction != 1.0)
   {
      edict_t *pent = tr.pHit;

      if (IsShootableBreakable (pent))
      {
         m_vBreakable = tr.vecEndPos;
         return pent;
      }
   }
   m_pentShootBreakable = NULL;
   m_vBreakable = NULLVEC;

   return NULL;
}

void CBot::FindItem (void)
{
   // this function finds Items to collect or use in the near of a bot

   edict_t *pent = NULL;
   edict_t *pentPickupItem = NULL;
   CBot *pBot = NULL;

   bool bAllowPickup;
   float fDistance, fMinDistance = 501.0;

   // don't try to pickup anything while on ladder or trying to escape from bomb...
   if (IsOnLadder () || (CurrentTask ()->iTask == TASK_ESCAPEFROMBOMB))
   {
      m_pentPickupItem = NULL;
      m_iPickupType = PICKUP_NONE;

      return;
   }

   if (!FNullEnt (m_pentPickupItem))
   {
      bool bItemExists = false;
      pentPickupItem = m_pentPickupItem;
 
      while (!FNullEnt (pent = FIND_ENTITY_IN_SPHERE (pent, pev->origin, 400.0)))
      {
         if (pent->v.effects & EF_NODRAW)
            continue; // someone owns this weapon or it hasn't respawned yet

         if (pent == pentPickupItem)
         {
            if (ItemIsVisible (GetEntityOrigin (pent), const_cast <char *> (STRING (pent->v.classname))))
               bItemExists = true;
            break;
         }
      }
      if (bItemExists)
         return;
      else
      {
         m_pentPickupItem = NULL;
         m_iPickupType = PICKUP_NONE;
      }
   }

   pent = NULL;
   pentPickupItem = NULL;
   ePickup iPickupType = PICKUP_NONE;

   Vector vPickupOrigin = NULLVEC;
   Vector vEntityOrigin = NULLVEC;

   m_pentPickupItem = NULL;
   m_iPickupType = PICKUP_NONE;

   while (!FNullEnt (pent = FIND_ENTITY_IN_SPHERE (pent, pev->origin, 340.0)))
   {
      bAllowPickup = false;  // assume can't use it until known otherwise

      if ((pent->v.effects & EF_NODRAW) || (pent == m_pentItemIgnore))
         continue; // someone owns this weapon or it hasn't respawned yet

      vEntityOrigin = GetEntityOrigin (pent);

      // check if line of sight to object is not blocked (i.e. visible)
      if (ItemIsVisible (vEntityOrigin, const_cast <char *> (STRING (pent->v.classname))))
      {
         if (strncmp ("hostage_entity", STRING (pent->v.classname), 14) == 0)
         {
            bAllowPickup = true;
            iPickupType = PICKUP_HOSTAGE;
         }
         else if ((strncmp ("weaponbox", STRING (pent->v.classname), 9) == 0) && (strcmp (STRING (pent->v.model) + 9, "backpack.mdl") == 0))
         {
            bAllowPickup = true;
            iPickupType = PICKUP_DROPPED_C4;
         }
         else if (((strncmp ("weaponbox", STRING (pent->v.classname), 9) == 0) || (strncmp ("armoury_entity", STRING (pent->v.classname), 14) == 0)) && !m_bUsingGrenade)
         {
            bAllowPickup = true;
            iPickupType = PICKUP_WEAPON;
         }
         else if ((strncmp ("weapon_shield", STRING (pent->v.classname), 13) == 0) && !m_bUsingGrenade)
         {
            bAllowPickup = true;
            iPickupType = PICKUP_SHIELD;
         }
         else if ((strncmp ("item_thighpack", STRING (pent->v.classname), 14) == 0) && (GetTeam (GetEntity ()) == TEAM_CT) && !m_bHasDefuser)
         {
            bAllowPickup = true;
            iPickupType = PICKUP_DEFUSEKIT;
         }
         else if ((strncmp ("grenade", STRING (pent->v.classname), 7) == 0) && FStrEq (STRING (pent->v.model) + 9, "c4.mdl"))
         {
            bAllowPickup = true;
            iPickupType = PICKUP_PLANTED_C4;
         }
      }

      if (bAllowPickup) // if the bot found something it can pickup...
      {
         fDistance = (vEntityOrigin - pev->origin).Length ();

         // see if it's the closest item so far...
         if (fDistance < fMinDistance)
         {
            if (iPickupType == PICKUP_WEAPON) // found weapon on ground?
            {
               if (m_bIsVIP || !RateGroundWeapon (pent))
                  bAllowPickup = false;
               else if (FStrEq (STRING (pent->v.model) + 9, "kevlar.mdl") && (pev->armorvalue >= 100)) // armor vest
                  bAllowPickup = false;
               else if (FStrEq (STRING (pent->v.model) + 9, "flashbang.mdl") && (pev->weapons & (1 << WEAPON_FLASHBANG))) // concussion grenade
                  bAllowPickup = false;
               else if (FStrEq (STRING (pent->v.model) + 9, "hegrenade.mdl") && (pev->weapons & (1 << WEAPON_HEGRENADE))) // explosive grenade
                  bAllowPickup = false;
               else if (FStrEq (STRING (pent->v.model) + 9, "smokegrenade.mdl") && (pev->weapons & (1 << WEAPON_SMOKEGRENADE))) // smoke grenade
                  bAllowPickup = false;
            }
            else if (iPickupType == PICKUP_SHIELD) // found a shield on ground?
            {
               if ((pev->weapons & (1 << WEAPON_ELITE)) || HasShield () || m_bIsVIP || (HasPrimaryWeapon () && !RateGroundWeapon (pent)))
                  bAllowPickup = false;
            }
            else if (GetTeam (GetEntity ()) == TEAM_TERRORIST) // terrorist team specific
            {
               if (iPickupType == PICKUP_DROPPED_C4)
               {
                  bAllowPickup = true;
                  m_vDestOrigin = vEntityOrigin; // ensure we reached droped bomb

                  ChatterMessage (VOICE_FOUND_C4); // play info about that
               }
               else if (iPickupType == PICKUP_PLANTED_C4)
               {
                  bAllowPickup = false;

                  if (!m_bDefendedBomb)
                  {
                     m_bDefendedBomb = true;

                     int iIndex = FindDefendWaypoint (vEntityOrigin);
                     float fTimeMidBlowup = g_fTimeBombPlanted + ((g_rgpcvBotCVar[CVAR_C4TIMER]->GetFloat () / 2) + g_rgpcvBotCVar[CVAR_C4TIMER]->GetFloat () / 4) - g_pWaypoint->GetTravelTime (pev->maxspeed, pev->origin, g_pWaypoint->GetPath (iIndex)->vOrigin);
                     
                     if (fTimeMidBlowup > WorldTime ())
                     {
                        RemoveCertainTask (TASK_MOVETOPOSITION); // remove any move tasks

                        StartTask (TASK_CAMP, TASKPRI_CAMP, -1, fTimeMidBlowup, true); // push camp task on to stack
                        StartTask (TASK_MOVETOPOSITION, TASKPRI_MOVETOPOSITION, iIndex, fTimeMidBlowup, true); // push move command

                        if (g_pWaypoint->GetPath (iIndex)->kVis.uCrouch <= g_pWaypoint->GetPath (iIndex)->kVis.uStand)
                           m_iCampButtons |= IN_DUCK;
                        else
                           m_iCampButtons &= ~IN_DUCK;

                        DebugMsg ("i'm defending bombsite (terrorist)");

                        if (RandomLong (0, 100) < 90)
                           ChatterMessage (VOICE_DEFENDING_BOMBSITE);
                     }
                     else
                        RadioMessage (RADIO_SHESGONNABLOW); // issue an additional radio message
                  }
               }
               else if (iPickupType == PICKUP_HOSTAGE)
               {
                  m_pentItemIgnore = pent;
                  bAllowPickup = false;

                  if ((m_iSkill > 80) && (RandomLong (0, 100) < 50) && (CurrentTask ()->iTask != TASK_MOVETOPOSITION) && (CurrentTask ()->iTask != TASK_CAMP))
                  {
                     int iIndex = FindDefendWaypoint (vEntityOrigin);
                     
                     StartTask (TASK_CAMP, TASKPRI_CAMP, -1, WorldTime () + RandomFloat (60.0, 120.0), true); // push camp task on to stack
                     StartTask (TASK_MOVETOPOSITION, TASKPRI_MOVETOPOSITION, iIndex, WorldTime () + RandomFloat (3.0, 6.0), true); // push move command

                     if (g_pWaypoint->GetPath (iIndex)->kVis.uCrouch <= g_pWaypoint->GetPath (iIndex)->kVis.uStand)
                        m_iCampButtons |= IN_DUCK;
                     else
                        m_iCampButtons &= ~IN_DUCK;

                     ChatterMessage (VOICE_GOING_TO_GUARD_HOSTAGES); // play info about that
                     DebugMsg ("i'm guarding hostages i've found");

                     return;
                  }
               }
            }
            else if (GetTeam (GetEntity ()) == TEAM_CT)
            {
               if (iPickupType == PICKUP_HOSTAGE)
               {
                  if (FNullEnt (pent))
                     bAllowPickup = false; // never pickup dead hostage
                  else for (int i = 0; i < MaxClients (); i++)
                  {
                     if (((pBot = g_pBotManager->GetBot (i)) != NULL) && IsAlive (pBot->GetEntity ()))
                     {
                        for (int j = 0; j < MAX_HOSTAGES; j++)
                        {
                           if (pBot->m_rgpHostages[j] == pent)
                           {
                              bAllowPickup = false;
                              break;
                           }
                        }
                     }
                  }
               }

               else if ((iPickupType == PICKUP_PLANTED_C4) && !OutOfBombTimer ())
               {
                  bAllowPickup = true;
                  iPickupType = PICKUP_PLANTED_C4;

                  if (IsAlive (m_pentEnemy) || (IsAlive (m_pentLastEnemy) && ((m_pentLastEnemy->v.origin - pev->origin).Length () < 600)))
                  {
                     bAllowPickup = false;
                     m_fItemCheckTime = WorldTime () + 3.0;
                     
                     if (FNullEnt (m_pentEnemy))
                        m_pentEnemy = m_pentLastEnemy; // interupt TASK_DEFUSE

                     return;
                  }
   
                  if (RandomLong (0, 100) < 90)
                     ChatterMessage (VOICE_FOUND_BOMB_PLACE);

                  // search the world for players...
                  for (int i = 0; i < MaxClients (); i++)
                  {
                     if (!(g_rgkClients[i].iFlags & CFLAG_USED) || !(g_rgkClients[i].iFlags & CFLAG_ALIVE) || (g_rgkClients[i].iTeam != GetTeam (GetEntity ())) || (g_rgkClients[i].pentEdict == GetEntity ()))
                        continue;

                     if (((pBot = g_pBotManager->GetBot (i)) != NULL) && (pBot->CurrentTask ()->iTask == TASK_ESCAPEFROMBOMB))
                        continue;

                     if ((g_rgkClients[i].pentEdict->v.origin - vEntityOrigin).Length () < 60)
                     {
                        bAllowPickup = false;

                        if (!m_bDefendedBomb)
                        {
                           m_bDefendedBomb = true;

                           int iIndex = FindDefendWaypoint (vEntityOrigin);   
                           float fTimeBlowup = g_fTimeBombPlanted + g_rgpcvBotCVar[CVAR_C4TIMER]->GetFloat () - g_pWaypoint->GetTravelTime (pev->maxspeed, pev->origin, g_pWaypoint->GetPath (iIndex)->vOrigin);
                              
                           RemoveCertainTask (TASK_MOVETOPOSITION); // remove any move tasks

                           StartTask (TASK_CAMP, TASKPRI_CAMP, -1, fTimeBlowup, true); // push camp task on to stack
                           StartTask (TASK_MOVETOPOSITION, TASKPRI_MOVETOPOSITION, iIndex, fTimeBlowup, true); // push move command

                           if (g_pWaypoint->GetPath (iIndex)->kVis.uCrouch <= g_pWaypoint->GetPath (iIndex)->kVis.uStand)
                              m_iCampButtons |= IN_DUCK;
                           else
                              m_iCampButtons &= ~IN_DUCK;

                           if (RandomLong (0, 100) < 90)
                              ChatterMessage (VOICE_DEFENDING_BOMBSITE);

                           DebugMsg ("i'm defending bombsite (counter-terrorist)");
                           return;
                        }
                     }
                  }
               }
               else if (iPickupType == PICKUP_DROPPED_C4)
               {
                  m_pentItemIgnore = pent;
                  bAllowPickup = false;

                  if ((m_iSkill > 80) && (RandomLong (0, 100) < 90))
                  {
                     int iIndex = FindDefendWaypoint (vEntityOrigin);
                     
                     StartTask (TASK_CAMP, TASKPRI_CAMP, -1, WorldTime () + RandomFloat (60.0, 120.0), true); // push camp task on to stack
                     StartTask (TASK_MOVETOPOSITION, TASKPRI_MOVETOPOSITION, iIndex, WorldTime () + RandomFloat (10.0, 30.0), true); // push move command

                     if (g_pWaypoint->GetPath (iIndex)->kVis.uCrouch <= g_pWaypoint->GetPath (iIndex)->kVis.uStand)
                        m_iCampButtons |= IN_DUCK;
                     else
                        m_iCampButtons &= ~IN_DUCK;

                     ChatterMessage (VOICE_GOING_TO_GUARD_LOOSED_C4); // play info about that
                     DebugMsg ("i'm guarding droped bomb");

                     return;
                  }
               }
            }
            if (bAllowPickup)
            {
               fMinDistance = fDistance; // update the minimum distance
               vPickupOrigin = vEntityOrigin; // remember location of entity
               pentPickupItem = pent; // remember this entity

               m_iPickupType = iPickupType;
            }
            else
               iPickupType = PICKUP_NONE;
         }
      }
   } // end of the while loop

   if (!FNullEnt (pentPickupItem))
   {
      for (int i = 0; i < MaxClients (); i++)
      {
         if (((pBot = g_pBotManager->GetBot (i)) != NULL) && IsAlive (pBot->GetEntity ()) && (pBot->m_pentPickupItem == pentPickupItem))
         {
            m_pentPickupItem = NULL;
            m_iPickupType = PICKUP_NONE;

            return;
         }
      }

      if ((vPickupOrigin.z > EyePosition ().z + 3.0) || IsDeadlyDrop (vPickupOrigin)) // check if Item is too high to reach, check if getting the item would hurt bot
      {
         m_pentPickupItem = NULL;
         m_iPickupType = PICKUP_NONE;

         return;
      }
      m_pentPickupItem = pentPickupItem; // save pointer of picking up entity
   }
}

void CBot::GetCampDirection (Vector *vDest)
{
   // this function check if view on last enemy position is blocked - replace with better vector then
   // mostly used for getting a good camping direction vector if not camping on a camp waypoint 

   TraceResult tr;
   Vector vSource = EyePosition ();

   TraceLine (vSource, *vDest, true, GetEntity (), &tr);

   // check if the trace hit something...
   if (tr.flFraction < 1.0)
   {
      float fLength = LengthSquared (tr.vecEndPos - vSource);

      if (fLength > 10000)
         return;

      float fMinDistance1 = FLT_MAX;
      float fMinDistance2 = FLT_MAX;

      int iBotIndex = -1, iEnemyIndex = -1, i;

      // find nearest waypoint to bot and position
      for (i = 0; i < g_iNumWaypoints; i++)
      {
         float fDistance = LengthSquared (g_pWaypoint->GetPath (i)->vOrigin - pev->origin);

         if (fDistance < fMinDistance1)
         {
            fMinDistance1 = fDistance;
            iBotIndex = i;
         }
         fDistance = LengthSquared (g_pWaypoint->GetPath (i)->vOrigin - *vDest);

         if (fDistance < fMinDistance2)
         {
            fMinDistance2 = fDistance;
            iEnemyIndex = i;
         }
      }

      if ((iBotIndex == -1) || (iEnemyIndex == -1))
         return;

      fMinDistance1 = FLT_MAX;

      tPath *pPath = g_pWaypoint->GetPath (iBotIndex);
      int iLookAtWaypoint = -1;

      for (i = 0; i < MAX_PATH_INDEX; i++)
      {
         if (pPath->iIndex[i] == -1)
            continue;

         float fDistance = g_pWaypoint->GetPathDistance (pPath->iIndex[i], iEnemyIndex);

         if (fDistance < fMinDistance1)
         {
            fMinDistance1 = fDistance;
            iLookAtWaypoint = i;
         }
      }
      if ((iLookAtWaypoint != -1) && (iLookAtWaypoint < g_iNumWaypoints))
         *vDest = g_pWaypoint->GetPath (iLookAtWaypoint)->vOrigin;
   }
}

void CBot::SwitchChatterIcon (bool bShow)
{
   // this function depending on bShow boolen, shows/remove chatter, icon, on the head of bot.

   if (g_iCSVersion == VERSION_WON)
      return;

   for (int i = 0; i < MaxClients (); i++)
   {
      edict_t *pent = INDEXENT (i);

      if (!IsValidPlayer (pent) || IsValidBot (pent) || (GetTeam (pent) != GetTeam (GetEntity ())))
         continue;

      MESSAGE_BEGIN (MSG_ONE, GetUserMsgId ("BotVoice"), NULL, pent); // begin message
         WRITE_BYTE (bShow); // switch on/off
         WRITE_BYTE (GetIndex ());
      MESSAGE_END ();
      
   }
}

void CBot::InstantChatterMessage (int iType)
{
   // this function sends instant chatter messages.

   if ((g_rgpcvBotCVar[CVAR_COMMUNICATION]->GetInt () != 2) || g_arVoiceEngine[iType].IsFree () || (g_iCSVersion == VERSION_WON) || (g_bSendAudioFinished == false))
      return;

   if (IsAlive (GetEntity ()))
      SwitchChatterIcon (true);

   for (int i = 0; i < MaxClients (); i++)
   {
      edict_t *pent = INDEXENT (i);

      if (!IsValidPlayer (pent) || IsValidBot (pent) || (GetTeam (pent) != GetTeam (GetEntity ())))
         continue;

      g_bSendAudioFinished = false;

      MESSAGE_BEGIN (MSG_ONE, GetUserMsgId ("SendAudio"), NULL, pent); // begin message
         WRITE_BYTE (GetIndex ());

         if (pev->deadflag & DEAD_DYING)
            WRITE_STRING (VarArgs ("%s/%s.wav", g_rgpcvBotCVar[CVAR_VOICEPATH]->GetString (), g_arVoiceEngine[VOICE_PAIN_ON_DIE].Random ().sName));
         else if (!(pev->deadflag & DEAD_DEAD))
            WRITE_STRING (VarArgs ("%s/%s.wav", g_rgpcvBotCVar[CVAR_VOICEPATH]->GetString (), g_arVoiceEngine[iType].Random ().sName));
         
         WRITE_SHORT (m_iVoicePitch);
      MESSAGE_END ();

      g_bSendAudioFinished = true;
   }
}

void CBot::RadioMessage (int iMessage)
{
   // this function inserts the radio message into the message queue

   if ((g_rgpcvBotCVar[CVAR_COMMUNICATION]->GetInt () == 0) || (GetNearbyFriendsNearPosition (pev->origin, 9999.0) == 0))
      return;

   if (g_arVoiceEngine[iMessage].IsFree () || (g_iCSVersion == VERSION_WON) || (g_rgpcvBotCVar[CVAR_COMMUNICATION]->GetInt () != 2))
      g_bRadioInsteadVoice = true; // use radio instead voice

   static float fReportTime = WorldTime ();

   // delay only reportteam
   if (iMessage == RADIO_REPORTTEAM)
   {
      if (fReportTime >= WorldTime ())
         return;

      fReportTime  = WorldTime () + RandomFloat (30.0, 80.0);
   }

   m_iRadioSelect = iMessage;
   PushMessageQueue (MESSAGE_RADIO);
}

void CBot::ChatterMessage (int iMessage)
{
   // this function inserts the voice message into the message queue (mostly same as above)
   
   if ((g_rgpcvBotCVar[CVAR_COMMUNICATION]->GetInt () != 2) || g_arVoiceEngine[iMessage].IsFree () || (GetNearbyFriendsNearPosition (pev->origin, 9999.0) == 0))
      return;

   bool bExecute = false;

   if ((m_fVoiceTime[iMessage] < WorldTime ()) || (m_fVoiceTime[iMessage] == FLT_MAX))
   {
      if (m_fVoiceTime[iMessage] != FLT_MAX)
         m_fVoiceTime[iMessage] = WorldTime () + g_arVoiceEngine[iMessage][0].fRepeatTime;

      bExecute = true;
   }

   if (!bExecute)
      return;

   m_iRadioSelect = iMessage;
   PushMessageQueue (MESSAGE_RADIO);
}

void CBot::CheckMessageQueue (void)
{
   // this function checks and executes pending messages

   // no new message?
   if (m_iActMessageIndex == m_iPushMessageIndex)
      return;

   // get message from stack
   int iCurrentMSG = GetMessageQueue ();

   // nothing to do?
   if (iCurrentMSG == MESSAGE_IDLE)
      return;

   int team = GetTeam (GetEntity ());

   switch (iCurrentMSG)
   {
   case MESSAGE_BUY: // general buy message
      // buy weapon
      if (m_fNextBuyTime > WorldTime ())
      {
         // keep sending message
         PushMessageQueue (MESSAGE_BUY);
         return;
      }

      if (!m_bInBuyZone)
      {
         m_bBuyPending = true;
         m_bBuyingFinished = true;
         break;
      }

      m_bBuyPending = false;
      m_fNextBuyTime = WorldTime () + RandomFloat (0.3, 0.8);

      // if bot buying is off then no need to buy
      if (!g_rgpcvBotCVar[CVAR_BOTBUY]->GetBool ())
         m_iBuyCount = 6;

      // if fun-mode no need to buy
      if (g_rgpcvBotCVar[CVAR_KNIFEMODE]->GetBool ())
      {
         m_iBuyCount = 6;
         SelectWeaponByName ("weapon_knife");
      }

      // prevent vip from buying
      if (g_iMapType & MAP_AS)
      {
         if (*(INFOKEY_VALUE (GET_INFOKEYBUFFER (GetEntity ()), "model")) == 'v')
         {
            m_bIsVIP = true;
            m_iBuyCount = 6;
            m_byPathType = 1;
         }
      }

      // prevent terrorists from buying on es maps
      if ((g_iMapType & MAP_ES) && (GetTeam (GetEntity ()) == TEAM_TERRORIST))
         m_iBuyCount = 6;

      // prevent teams from buying on fun maps
      if (g_iMapType & (MAP_KA | MAP_FY))
      {
         m_iBuyCount = 6;

         if (g_iMapType & MAP_KA)
            CVarSetFloat (g_rgpcvBotCVar[CVAR_KNIFEMODE]->GetName (), 1);
      }

      if (m_iBuyCount > 5)
      {
         m_bBuyingFinished = true;
         return;
      }

      PushMessageQueue (MESSAGE_IDLE);
      DoWeaponBuying ();

      break;

   case MESSAGE_RADIO: // general radio message issued
     // if last bot radio command (global) happened just a second ago, delay response
      if (g_rgfLastRadioTime[team] + 1.0 < WorldTime ())
      {
         // if same message like previous just do a yes/no 
         if ((m_iRadioSelect != RADIO_AFFIRMATIVE) && (m_iRadioSelect != RADIO_NEGATIVE))
         {
            if ((m_iRadioSelect == g_rgiLastRadio[team]) && (g_rgfLastRadioTime[team] + 1.5 > WorldTime ()))
               m_iRadioSelect = -1;
            else
            {
               if (m_iRadioSelect != RADIO_REPORTINGIN)
                  g_rgiLastRadio[team] = m_iRadioSelect;
               else
                  g_rgiLastRadio[team] = -1;

               for (int i = 0; i < MaxClients (); i++)
               {
                  if (g_pBotManager->GetBot (i))
                  {
                     if (pev != g_pBotManager->GetBot (i)->pev && GetTeam (g_pBotManager->GetBot (i)->GetEntity ()) == team)
                     {
                        g_pBotManager->GetBot (i)->m_iRadioOrder = m_iRadioSelect;
                        g_pBotManager->GetBot (i)->m_pentRadioEntity = GetEntity ();
                     }
                  }
               } 
            }
         }

         if (m_iRadioSelect == RADIO_REPORTINGIN)
         {
            switch (CurrentTask ()->iTask)
            {
            case TASK_NORMAL:
               if (CurrentTask ()->iData != -1)
               {
                  if (g_pWaypoint->GetPath (CurrentTask ()->iData)->iFlags & W_FL_GOAL)
                  {
                     if ((g_iMapType & MAP_DE) && (GetTeam (GetEntity ()) == TEAM_TERRORIST) && (pev->weapons & (1 << WEAPON_C4)))
                        InstantChatterMessage (VOICE_GOING_TO_PLANTBOMB);
                     else
                        InstantChatterMessage (VOICE_NOTHING);
                  }
                  else if (g_pWaypoint->GetPath (CurrentTask ()->iData)->iFlags & W_FL_RESCUE)
                     InstantChatterMessage (VOICE_RESCUING_HOSTAGES);
                  else if (g_pWaypoint->GetPath (CurrentTask ()->iData)->iFlags & W_FL_CAMP)
                     InstantChatterMessage (VOICE_GOING_TO_CAMP);
                  else
                     InstantChatterMessage (VOICE_HEAR_SOMETHING);
               }
               else
                  InstantChatterMessage (VOICE_REPORTING_IN);
               break;

            case TASK_MOVETOPOSITION:
               InstantChatterMessage (VOICE_GOING_TO_CAMP);
               break;

            case TASK_CAMP:
               if (g_bBombPlanted && (GetTeam (GetEntity ()) == TEAM_TERRORIST))
                  InstantChatterMessage (VOICE_GUARDING_C4);
               else if (m_bInVIPZone && (GetTeam (GetEntity ()) == TEAM_TERRORIST))
                  InstantChatterMessage (VOICE_GUARDING_VIPSAFETY);
               else
                  InstantChatterMessage (VOICE_CAMPING);
               break;

            case TASK_PLANTBOMB:
               InstantChatterMessage (VOICE_PLANTING_C4);
               break;

            case TASK_DEFUSEBOMB:
               InstantChatterMessage (VOICE_DEFUSING_C4);
               break;

            case TASK_ATTACK:
               InstantChatterMessage (VOICE_IN_COMBAT);
               break;

            case TASK_HIDE:
            case TASK_SEEKCOVER:
               InstantChatterMessage (VOICE_SEEKING_ENEMY);
               break;

            default:
               InstantChatterMessage (VOICE_NOTHING);
               break;
            }
         }

         if ((m_iRadioSelect != RADIO_REPORTINGIN) && g_bRadioInsteadVoice || (g_rgpcvBotCVar[CVAR_COMMUNICATION]->GetInt () != 2) || g_arVoiceEngine[m_iRadioSelect].IsFree () || (g_iCSVersion == VERSION_WON))
         {
            if (m_iRadioSelect < RADIO_GOGOGO)
               FakeClientCommand (GetEntity (), "radio1");
            else if (m_iRadioSelect < RADIO_AFFIRMATIVE)
            {
               m_iRadioSelect -= RADIO_GOGOGO - 1;
               FakeClientCommand (GetEntity (), "radio2");
            }
            else
            {
               m_iRadioSelect -= RADIO_AFFIRMATIVE - 1;
               FakeClientCommand (GetEntity (), "radio3");
            }

            // select correct menu item for this radio message
            FakeClientCommand (GetEntity (), "menuselect %d", m_iRadioSelect);
         }
         else if ((m_iRadioSelect != -1) && (m_iRadioSelect != RADIO_REPORTINGIN))
            InstantChatterMessage (m_iRadioSelect);

         g_bRadioInsteadVoice = false; // reset radio to voice
         g_rgfLastRadioTime[team] = WorldTime (); // store last radio usage
      }
      else
         PushMessageQueue (MESSAGE_RADIO);
      break;

   // team independant saytext
   case MESSAGE_SAY:
      SayText (m_szMiscStrings);
      break;

   // team dependant saytext
   case MESSAGE_TEAMSAY:
      TeamSayText (m_szMiscStrings);
      break;

   case MESSAGE_IDLE:
   default:
      return;
   }
}

bool CBot::IsRestricted (int iWeaponId)
{
   // this function checks for weapon restrictions. base idea & code from KWo (PodbotMM)

   if (IsNullString (g_rgpcvBotCVar[CVAR_RESTWEAPONS]->GetString ()))
      return false; // no banned weapons

   vector <string> kBannedWeapons = string (g_rgpcvBotCVar[CVAR_RESTWEAPONS]->GetString ()).Split (';');

   ITERATE_ARRAY (kBannedWeapons, i)
   {
      const char *cszBanned = STRING (GetWeaponReturn (true, NULL, iWeaponId));

      // check is this weapon is banned
      if (strncmp (kBannedWeapons[i].GetString (), cszBanned, kBannedWeapons[i].Length ()) == 0)
      {
         DebugMsg ("Weapon %s, banned!", cszBanned);
         return true;
      }
   }
   return false;
}

bool CBot::IsMorePowerfulWeaponCanBeBought (void)
{
   // this function determines currently owned primary weapon, and checks if bot has
   // enough money to buy more powerful weapon.

   // if bot is not rich enough or non-standard weaponmode enabled return false
   if ((g_rgkWeaponSelect[25].iTeamStandard != 1) || (m_iAccount < 4000))
      return false;

   // also check if bot has really bad weapon, maybe it's time to change it
   if (UsesBadPrimary ())
      return true;

   vector <string> kBanned = string (g_rgpcvBotCVar[CVAR_RESTWEAPONS]->GetString ()).Split (';');

   // check if its banned
   ITERATE_ARRAY (kBanned, i)
   {
      if (m_iCurrentWeapon == GetWeaponReturn (false, kBanned[i]))
         return true;
   }

   if ((m_iCurrentWeapon == WEAPON_SCOUT) && (m_iAccount > 5000))
      return true;
   else if ((m_iCurrentWeapon == WEAPON_MP5NAVY) && (m_iAccount > 3000))
      return true;

   return false;
}

void CBot::DoWeaponBuying (void)
{
   // this function does all the work in selecting correct buy menus for most weapons/items

   tWeaponSelect *pSelectedWeapon = NULL;
   m_fNextBuyTime = WorldTime ();

   int iCount = 0, iFoundWeapons = 0, iEconomyPrice = 0;
   int iBuyChoices[NUM_WEAPONS];

   // select the priority tab for this personality
   int *ptrWeaponTab = g_ptrWeaponPrefs[m_ucPersonality] + NUM_WEAPONS;
   int iTeam = GetTeam (GetEntity ());

   switch (m_iBuyCount)
   {
   case 0: // if no primary weapon and bot has some money, buy a primary weapon
       if ((!HasShield () && !HasPrimaryWeapon () && m_bTeamEconomicsGood) || IsMorePowerfulWeaponCanBeBought ())
      {
         do
         {
            bool bIgnoreWeapon = false;

            ptrWeaponTab--;

            InternalAssert (*ptrWeaponTab > -1);
            InternalAssert (*ptrWeaponTab < NUM_WEAPONS);

            pSelectedWeapon = &g_rgkWeaponSelect[*ptrWeaponTab];
            iCount++;

            if (pSelectedWeapon->iBuyGroup == 1)
               continue;

            // weapon available for every team?
            if (g_iMapType & MAP_AS)
            {
               if ((pSelectedWeapon->iTeamAS != 2) && (pSelectedWeapon->iTeamAS != iTeam))
                  continue;
            }

            // ignore weapon if this weapon not supported by currently running cs version...
            if ((g_iCSVersion == VERSION_WON) && (pSelectedWeapon->iBuySelect == -1))
               continue;

            // ignore weapon if this weapon is not targeted to out team....
            if ((pSelectedWeapon->iTeamStandard != 2) && (pSelectedWeapon->iTeamStandard != iTeam))
               continue;

            // ignore weapon if this weapon is restricted
            if (IsRestricted (pSelectedWeapon->iId))
               continue;

            // filter out weapons with bot economics (thanks for idea to nicebot project)
            switch (m_ucPersonality)
            {
            case PERSONALITY_AGRESSIVE:
               iEconomyPrice = g_rgiBotBuyEconomyTable[8];
               break;

            case PERSONALITY_DEFENSIVE:
               iEconomyPrice = g_rgiBotBuyEconomyTable[7];
               break;

            case PERSONALITY_NORMAL:
               iEconomyPrice = g_rgiBotBuyEconomyTable[9];
               break;
            }

            if (iTeam == TEAM_CT)
            {
               switch (pSelectedWeapon->iId)
               {
               case WEAPON_TMP:
               case WEAPON_UMP45:
               case WEAPON_P90:
               case WEAPON_MP5NAVY:
                  if (m_iAccount > g_rgiBotBuyEconomyTable[1] + iEconomyPrice)
                     bIgnoreWeapon = true;
                  break;
               }

               if ((pSelectedWeapon->iId == WEAPON_SHIELDGUN) && (m_iAccount > g_rgiBotBuyEconomyTable[10]))
                  bIgnoreWeapon = true;
            }
            else if (iTeam == TEAM_TERRORIST)
            {
               switch (pSelectedWeapon->iId)
               {
               case WEAPON_UMP45:
               case WEAPON_MAC10:
               case WEAPON_P90:
               case WEAPON_MP5NAVY:
               case WEAPON_SCOUT:
                  if (m_iAccount > g_rgiBotBuyEconomyTable[2] + iEconomyPrice)
                     bIgnoreWeapon = true;
                  break;
               }
            }

            switch (pSelectedWeapon->iId)
            {
            case WEAPON_XM1014:
            case WEAPON_M3:
               if ((m_iAccount < g_rgiBotBuyEconomyTable[3]) || (m_iAccount > g_rgiBotBuyEconomyTable[4]))
                  bIgnoreWeapon = true;
               break;
            }

            switch (pSelectedWeapon->iId)
            {
            case WEAPON_SG550:
            case WEAPON_G3SG1:
            case WEAPON_AWP:
            case WEAPON_M249:
               if ((m_iAccount < g_rgiBotBuyEconomyTable[5]) || (m_iAccount > g_rgiBotBuyEconomyTable[6]))
                  bIgnoreWeapon = true;
               break;
            }

            if (bIgnoreWeapon && (g_rgkWeaponSelect[25].iTeamStandard == 1))
            {
               DebugMsg ("filtering weapon with economics (%s)", pSelectedWeapon->szWeaponName);
               continue;
            }

            if (pSelectedWeapon->iPrice <= (m_iAccount - RandomLong (80, 120)))
               iBuyChoices[iFoundWeapons++] = *ptrWeaponTab;

         } while ((iCount < NUM_WEAPONS) && (iFoundWeapons < 4));

         // found a desired weapon?
         if (iFoundWeapons > 0)
         {
            int iChosenWeapon;

            // Choose randomly from the best ones...
            if (iFoundWeapons > 1)
               iChosenWeapon = iBuyChoices[RandomLong (0, iFoundWeapons - 1)];
            else
               iChosenWeapon = iBuyChoices[iFoundWeapons - 1];

            pSelectedWeapon = &g_rgkWeaponSelect[iChosenWeapon];
         }
         else
            pSelectedWeapon = NULL;

         if (pSelectedWeapon != NULL)
         {
            FakeClientCommand (GetEntity (), "buy;menuselect %d", pSelectedWeapon->iBuyGroup);
            
            if (g_iCSVersion == VERSION_WON)
               FakeClientCommand (GetEntity (), "menuselect %d", pSelectedWeapon->iBuySelect);
            else // SteamCS buy menu is different from the old one
            {
               if (GetTeam (GetEntity ()) == TEAM_TERRORIST)
                  FakeClientCommand (GetEntity (), "menuselect %d", pSelectedWeapon->iNewBuySelectT);
               else
                  FakeClientCommand (GetEntity (), "menuselect %d", pSelectedWeapon->iNewBuySelectCT);
            }
         }
      } 
      else if (HasPrimaryWeapon () || !HasShield ())
         m_iReloadState = RELOAD_PRIMARY;

      break;

   case 1: // if armor is damaged and bot has some money, buy some armor
      if ((pev->armorvalue < RandomLong (50, 80)) && (m_iAccount > 1000) && m_bTeamEconomicsGood)
      {
         // if bot is rich, buy kevlar + helmet, else buy a single kevlar
         if ((m_iAccount > 1500) && !IsRestricted (WEAPON_ARMORHELM))
            FakeClientCommand (GetEntity (), "buyequip;menuselect 2");
         else if (!IsRestricted (WEAPON_ARMOR))
            FakeClientCommand (GetEntity (), "buyequip;menuselect 1");
      }
      break;

   case 2: // if bot has still some money, buy a better secondary weapon
      if (((m_iAccount >= 1000) || (!HasPrimaryWeapon () && (m_iAccount > 1100))) && (pev->weapons & ((1 << WEAPON_USP) | (1 << WEAPON_GLOCK18))) && m_bTeamEconomicsGood)
      {
         do
         {
            ptrWeaponTab--;

            InternalAssert (*ptrWeaponTab > -1);
            InternalAssert (*ptrWeaponTab < NUM_WEAPONS);

            pSelectedWeapon = &g_rgkWeaponSelect[*ptrWeaponTab];
            iCount++;

            if (pSelectedWeapon->iBuyGroup != 1)
               continue;

            // ignore weapon if this weapon is restricted
            if (IsRestricted (pSelectedWeapon->iId))
               continue;

            // weapon available for every team?
            if (g_iMapType & MAP_AS)
            {
               if ((pSelectedWeapon->iTeamAS != 2) && (pSelectedWeapon->iTeamAS != iTeam))
                  continue;
            }

            if ((g_iCSVersion == VERSION_WON) && (pSelectedWeapon->iBuySelect == -1))
               continue;

            if ((pSelectedWeapon->iTeamStandard != 2) && (pSelectedWeapon->iTeamStandard != iTeam))
               continue;
 
            if (pSelectedWeapon->iPrice <= (m_iAccount - RandomLong (100, 200)))
               iBuyChoices[iFoundWeapons++] = *ptrWeaponTab;

         } while ((iCount < NUM_WEAPONS) && (iFoundWeapons < 4));

         // found a desired weapon?
         if (iFoundWeapons > 0)
         {
            int iChosenWeapon;

            // choose randomly from the best ones...
            if (iFoundWeapons > 1)
               iChosenWeapon = iBuyChoices[RandomLong (0, iFoundWeapons - 1)];
            else
               iChosenWeapon = iBuyChoices[iFoundWeapons - 1];
            pSelectedWeapon = &g_rgkWeaponSelect[iChosenWeapon];
         }
         else
            pSelectedWeapon = NULL;

         if (pSelectedWeapon)
         {
            FakeClientCommand (GetEntity (), "buy;menuselect %d", pSelectedWeapon->iBuyGroup);
            
            if (g_iCSVersion == VERSION_WON)
               FakeClientCommand (GetEntity (), "menuselect %d", pSelectedWeapon->iBuySelect);
            else // SteamCS buy menu is different from old one
            {
               if (GetTeam (GetEntity ()) == TEAM_TERRORIST)
                  FakeClientCommand (GetEntity (), "menuselect %d", pSelectedWeapon->iNewBuySelectT);
               else
                  FakeClientCommand (GetEntity (), "menuselect %d", pSelectedWeapon->iNewBuySelectCT);
            }         
         }
      }
      break;

   case 3: // if bot has still some money, choose if bot should buy a grenade or not
      if ((RandomLong (1, 100) < g_rgiGrenadeBuyPrecent[0]) && (m_iAccount >= 300) && !IsRestricted (WEAPON_HEGRENADE))
      {
         // buy a he grenade
         FakeClientCommand (GetEntity (), "buyequip");
         FakeClientCommand (GetEntity (), "menuselect 4");
      }

      if ((RandomLong (1, 100) < g_rgiGrenadeBuyPrecent[1]) && (m_iAccount >= 200) && m_bTeamEconomicsGood && !IsRestricted (WEAPON_FLASHBANG))
      {
         // buy a concussion grenade, i.e., 'flashbang'
         FakeClientCommand (GetEntity (), "buyequip");
         FakeClientCommand (GetEntity (), "menuselect 3");
      }

      if ((RandomLong (1, 100) < g_rgiGrenadeBuyPrecent[2]) && (m_iAccount >= 300) && m_bTeamEconomicsGood && !IsRestricted (WEAPON_SMOKEGRENADE))
      {
         // buy a smoke grenade
         FakeClientCommand (GetEntity (), "buyequip");
         FakeClientCommand (GetEntity (), "menuselect 5");
      }
      break;

   case 4: // if bot is CT and we're on a bomb map, randomly buy the defuse kit
      if ((g_iMapType & MAP_DE) && (GetTeam (GetEntity ()) == TEAM_CT) && (RandomLong (1, 100) < 80) && (m_iAccount > 200) && !IsRestricted (WEAPON_DEFUSER))
      {
         if (g_iCSVersion == VERSION_WON)
            FakeClientCommand (GetEntity (), "buyequip;menuselect 6");
         else
            FakeClientCommand (GetEntity (), "defuser"); // use alias in SteamCS
      }
      break;

   case 5: // buy enough primary & secondary ammo (do not check for money here)
      for (int i = 0; i <= 5; i++)
         FakeClientCommand (GetEntity (), "buyammo%d", RandomLong (1, 2)); // simulate human

      // buy enough secondary ammo
      if (HasPrimaryWeapon ())
         FakeClientCommand (GetEntity (), "buy;menuselect 7");

      // buy enough primary ammo
      FakeClientCommand (GetEntity (), "buy;menuselect 6");

      // try to reload secondary weapon
      if (m_iReloadState != RELOAD_PRIMARY)
         m_iReloadState = RELOAD_SECONDARY;

      break;
   }

   m_iBuyCount++;
   PushMessageQueue (MESSAGE_BUY);
}

//====================================\\
//      Task Filter Functions         \\
//                                    \\
// Taken from Robert Zubek's paper    \\
// entitled "Game Agent Control Using \\
// Parallel Behaviors".               \\
//                                    \\
//     rob@cs.northwestern.edu        \\
// http://www.cs.northwestern.edu/~rob\\
//====================================\\

tTask *ClampDesire (tTask *pFirst, float fMin, float fMax)
{
   // this function iven some values min and max, clamp the inputs to be inside the [min, max] range.

   if (pFirst->fDesire < fMin)
      pFirst->fDesire = fMin;
   else if (pFirst->fDesire > fMax)
      pFirst->fDesire = fMax;

   return pFirst;
}

tTask *MaxDesire (tTask *pFirst, tTask *pSecond)
{
   // this function returns the behavior having the higher activation level.

   if (pFirst->fDesire > pSecond->fDesire)
      return pFirst;
   else
      return pSecond;
}

tTask *SubsumeDesire (tTask *pFirst, tTask *pSecond)
{
   // this function returns the first behavior if its activation level is anything higher than zero.

   if (pFirst->fDesire > 0)
      return pFirst;
   else
      return pSecond;
}

tTask *ThresholdDesire (tTask *pFirst, float fThreshold, float fDesire)
{
   // this function returns the input behavior if it's activation level exceeds the threshold, or some default 
   // behavior otherwise.

   if (pFirst->fDesire < fThreshold)
      pFirst->fDesire = fDesire;

   return pFirst;
}

float HysteresisDesire (float fX, float fMin, float fMax, float fOld)
{
   // this function clamp the inputs to be the last known value outside the [min, max] range.
  
   if ((fX <= fMin) || (fX >= fMax))
      fOld = fX;

   return fOld;
}

void CBot::SetConditions (void)
{
   // this function carried out each frame. does all of the sensing, calculates emotions and finally sets the desired 
   // action after applying all of the Filters

   int iTeam = GetTeam (GetEntity ());
   m_iAimFlags = 0;

   // slowly increase/decrease dynamic emotions back to their base level
   if (m_fNextEmotionUpdate < WorldTime ())
   {
      if (g_rgpcvBotCVar[CVAR_HARCOREMODE]->GetBool ())
      {
         m_fAgressionLevel *= 2;
         m_fFearLevel /= 2;
      }

      if (m_fAgressionLevel > m_fBaseAgressionLevel)
         m_fAgressionLevel -= 0.05;
      else
         m_fAgressionLevel += 0.05;

      if (m_fFearLevel > m_fBaseFearLevel)
         m_fFearLevel -= 0.05;
      else
         m_fFearLevel += 0.05;

      if (m_fAgressionLevel < 0.0)
         m_fAgressionLevel = 0.0;

      if (m_fFearLevel < 0.0)
         m_fFearLevel = 0.0;

      m_fNextEmotionUpdate = WorldTime () + 0.5;
   }

   // does bot see an enemy?
   if (!g_rgpcvBotCVar[CVAR_DONTSHOOT]->GetBool () && LookupEnemy ())
      m_iStates |= STATE_SEEINGENEMY;
   else
   {
      m_iStates &= ~STATE_SEEINGENEMY;
      m_pentEnemy = NULL;
   }

   // did bot just kill an enemy?
   if (!FNullEnt (m_pentLastVictim))
   {
      if (GetTeam (m_pentLastVictim) != iTeam)
      {
         // add some aggression because we just killed somebody
         m_fAgressionLevel += 0.1;

         if (m_fAgressionLevel > 1.0)
            m_fAgressionLevel = 1.0;

         if (RandomLong (1, 100) > 50)
            ChatMessage (CHAT_KILLING);

         if (RandomLong (1, 100) < 10)
            RadioMessage (RADIO_ENEMYDOWN);
         else
         {
            if ((m_pentLastVictim->v.weapons & (1 << WEAPON_AWP)) || (m_pentLastVictim->v.weapons & (1 << WEAPON_SCOUT)) ||  (m_pentLastVictim->v.weapons & (1 << WEAPON_G3SG1)) || (m_pentLastVictim->v.weapons & (1 << WEAPON_SG550)))
               ChatterMessage (VOICE_SNIPER_KILLED);
            else
            {
               switch (GetNearbyEnemiesNearPosition (pev->origin, 9999.0))
               {
               case 0:
                  if (RandomLong (0, 100) < 50)
                     ChatterMessage (VOICE_NO_ONE_LEFT);
                  else
                     ChatterMessage (VOICE_ENEMYDOWN);
                  break;

               case 1:
                  ChatterMessage (VOICE_ONE_ENEMY_LEFT);
                  break;

               case 2:
                  ChatterMessage (VOICE_TWO_ENEMIES_LEFT);
                  break;

               case 3:
                  ChatterMessage (VOICE_THREE_ENEMIES_LEFT);
                  break;

               default:
                  ChatterMessage (VOICE_ENEMYDOWN);
               }
            }
         }
         // if no more enemies found AND bomb planted, switch to knife to get to bombplace faster
         if ((GetTeam (GetEntity ()) == TEAM_CT) && (m_iCurrentWeapon != WEAPON_KNIFE) && (GetNearbyEnemiesNearPosition (pev->origin, 9999.0) == 0) && g_bBombPlanted)
         {
            SelectWeaponByName ("weapon_knife");

            // order team to regroup
            RadioMessage (RADIO_REGROUPTEAM);
         }
      }
      else
      {
         ChatMessage (CHAT_TEAMKILL, true);
         ChatterMessage (VOICE_TEAM_KILLING);
      }
      m_pentLastVictim = NULL;
   }

   // check if our current enemy is still valid
   if (!FNullEnt (m_pentLastEnemy)) 
   {
      if (!IsAlive (m_pentLastEnemy) && (m_fShootAtDeadTime < WorldTime ()))
      {
         m_vLastEnemyOrigin = NULLVEC;
         m_pentLastEnemy = NULL;
      }
   }
   else
   {
      m_vLastEnemyOrigin = NULLVEC;
      m_pentLastEnemy = NULL;
   }

   // don't listen if seeing enemy, just checked for sounds or being blinded (because its inhuman)
   if (!g_rgpcvBotCVar[CVAR_DONTSHOOT]->GetBool () && (m_fSoundUpdateTime <= WorldTime ()) && (m_fSeeEnemyTime + 1.5 < WorldTime ()) && (m_fBlindTime < WorldTime ()))
   {
      ReactOnSound ();
      m_fSoundUpdateTime = WorldTime () + g_rgpcvBotCVar[CVAR_TIMERSOUND]->GetFloat ();
   }
   else
      m_iStates &= ~STATE_HEARINGENEMY;

   if ((FNullEnt (m_pentEnemy) && (m_fSeeEnemyTime + 1.5 < WorldTime ())) && !FNullEnt (m_pentLastEnemy))
   {
      TraceResult tr;
      TraceLine (EyePosition (), m_vLastEnemyOrigin, true, GetEntity (), &tr);

      if (((pev->origin - m_vLastEnemyOrigin).Length () < 1600.0) && ((tr.flFraction >= 0.2) || (tr.pHit != g_pentWorldEdict)))
      {
         m_iAimFlags |= AIM_PREDICTPATH;

         if (EntityIsVisible (m_vLastEnemyOrigin))
            m_iAimFlags |= AIM_LASTENEMY;
      }
   }

   // check if throwing a grenade is a good thing to do...
   if ((m_fGrenadeCheckTime < WorldTime ()) && !m_bUsingGrenade && (CurrentTask ()->iTask != TASK_PLANTBOMB) && (CurrentTask ()->iTask != TASK_DEFUSEBOMB) && !m_bIsReloading)
   {
      // check again in some seconds
      m_fGrenadeCheckTime = WorldTime () + g_rgpcvBotCVar[CVAR_TIMERGRENADE]->GetFloat ();

      // check if we have grenades to throw
      int iGrenadeToThrow = CheckGrenades ();

      // if we don't have grenades no need to check it this round again
      if (iGrenadeToThrow == -1)
      {
         m_fGrenadeCheckTime = WorldTime () + 30.0; // changed since, conzero can drop grens from dead players
         m_iStates &= ~(STATE_THROWHEGREN | STATE_THROWFLASHBANG | STATE_THROWSMOKEGREN);
      }
      else
      {
         // care about different types of grenades
         if (((iGrenadeToThrow == WEAPON_HEGRENADE) || (iGrenadeToThrow == WEAPON_SMOKEGRENADE)) && IsAlive (m_pentLastEnemy) && (RandomLong (0, 100) < 45) && !(m_iStates & STATE_SEEINGENEMY))
         {
            float fDistance = (m_pentLastEnemy->v.origin - pev->origin).Length ();

            // is enemy to high to throw
            if ((m_pentLastEnemy->v.origin.z > (pev->origin.z + 500.0)) || !(m_pentLastEnemy->v.flags & FL_ONGROUND))
               fDistance = FLT_MAX; // just some crazy value

            // enemy is within a good throwing distance ?
            if ((fDistance > (iGrenadeToThrow == WEAPON_SMOKEGRENADE ? 400 : 600)) && (fDistance < 1200))
            {
               // care about different types of grenades
               if (iGrenadeToThrow == WEAPON_HEGRENADE)
               {
                  bool bAllowThrowing = true;

                  // check for teammates
                  if (GetNearbyFriendsNearPosition (m_pentLastEnemy->v.origin, 256.0) > 0)
                     bAllowThrowing = false;
                  
                  if (bAllowThrowing)
                  {
                     Vector vPredictEnemy = ((m_pentLastEnemy->v.velocity * 0.5).IgnoreZComponent () + m_pentLastEnemy->v.origin);
                     int rgiWptTab[4], iCount = 4;

                     float fSearchRadius = m_pentLastEnemy->v.velocity.Length2D ();

                     // check the search radius
                     if (fSearchRadius < 128.0)
                        fSearchRadius = 128.0;

                     // search waypoints
                     g_pWaypoint->FindInRadius (vPredictEnemy, fSearchRadius, rgiWptTab, &iCount);

                     while (iCount > 0)
                     {
                        bAllowThrowing = true;

                        // check the throwing
                        m_vThrow = g_pWaypoint->GetPath (rgiWptTab[iCount--])->vOrigin;
                        Vector vSource = CheckThrow (EyePosition (), m_vThrow, 400.0);

                        if (LengthSquared (vSource) < 100.0)
                           vSource = CheckToss (EyePosition (), m_vThrow);

                        if (vSource == NULLVEC)
                           bAllowThrowing = false;
                        else
                           break;
                     }
                  }

                  // start explosive grenade throwing?
                  if (bAllowThrowing)
                     m_iStates |= STATE_THROWHEGREN;
                  else
                     m_iStates &= ~STATE_THROWHEGREN;
               }
               else if (iGrenadeToThrow == WEAPON_SMOKEGRENADE)
               {
                  // start smoke grenade throwing?
                  if (!FNullEnt (m_pentEnemy) && (GetShootingConeDeviation (m_pentEnemy, &pev->origin) >= 0.90))
                     m_iStates &= ~STATE_THROWSMOKEGREN;
                  else 
                     m_iStates |= STATE_THROWSMOKEGREN;
               }
            }
         }
         else if ((iGrenadeToThrow == WEAPON_FLASHBANG) && CheckCorridorAround ())
         {
            bool bAllowThrowing = true;

            if ((m_rgiPrevWptIndex[0] < 0) || (m_rgiPrevWptIndex[0] >= g_iNumWaypoints))
               bAllowThrowing = false;
            
            if (bAllowThrowing)
            {
               Vector vMoveDirection = (m_vWptOrigin - g_pWaypoint->GetPath (m_rgiPrevWptIndex[0])->vOrigin).Normalize () * 500.0;
               Vector vDestination = vMoveDirection + m_vWptOrigin;

               if (GetNearbyFriendsNearPosition (vDestination, 256.0) > 0)
                  bAllowThrowing = false;
               else
               {
                  m_vThrow = vDestination;

#if _DEBUG
                  Vector vSource = CheckThrow (EyePosition (), m_vThrow, 1.0);

                  if (LengthSquared (vSource) < 100.0)
                     vSource = CheckToss (EyePosition (), m_vThrow);

                  if (vSource == NULLVEC)
                     bAllowThrowing = false;
#endif
               }
            }
            // start concussion grenade throwing?
            if (bAllowThrowing)
               m_iStates |= STATE_THROWFLASHBANG;
            else
               m_iStates &= ~STATE_THROWFLASHBANG;
         }

         if (m_iStates & STATE_THROWHEGREN)
            StartTask (TASK_THROWHEGRENADE, TASKPRI_THROWGRENADE, -1, WorldTime () + 0.5, false);
         else if (m_iStates & STATE_THROWFLASHBANG)
            StartTask (TASK_THROWFLASHBANG, TASKPRI_THROWGRENADE, -1, WorldTime () + 0.5, false);
         else if (m_iStates & STATE_THROWSMOKEGREN)
            StartTask (TASK_THROWSMOKEGRENADE, TASKPRI_THROWGRENADE, -1, WorldTime () + 0.5, false);
      }
   }
   else
      m_iStates &= ~(STATE_THROWHEGREN | STATE_THROWFLASHBANG | STATE_THROWSMOKEGREN);

   // check if there are items needing to be used/collected
   if ((m_fItemCheckTime < WorldTime ()) || !FNullEnt (m_pentPickupItem))
   {
      m_fItemCheckTime = WorldTime () + g_rgpcvBotCVar[CVAR_TIMERPICKUP]->GetFloat ();
      FindItem ();
   }

   float fTempFear = m_fFearLevel;
   float fTempAgression = m_fAgressionLevel;

   // decrease fear if teammates near 
   int iFriendlyNum = 0;

   if (m_vLastEnemyOrigin != NULLVEC)
      iFriendlyNum = GetNearbyFriendsNearPosition (pev->origin, 300) - GetNearbyEnemiesNearPosition (m_vLastEnemyOrigin, 500);

   if (iFriendlyNum > 0)
      fTempFear = fTempFear * 0.5;

   // increase/decrease fear/aggression if bot uses a sniping weapon to be more careful
   if (UsesSniper ())
   {
      fTempFear = fTempFear * 1.5;
      fTempAgression = fTempAgression * 0.5;
   }

   // initialize & calculate the desire for all actions based on distances, emotions and other stuff
   CurrentTask ();

   // bot found some item to use?
   if (!FNullEnt (m_pentPickupItem))
   {
      m_iStates |= STATE_PICKUPITEM;

      if (m_iPickupType == PICKUP_BUTTON)
         g_rgkTaskFilters[TASK_PICKUPITEM].fDesire = 50; // always pickup button
      else
      {
         float fDistance = (500.0 - (GetEntityOrigin (m_pentPickupItem) - pev->origin).Length ()) * 0.2;

         if (fDistance > 50)
            fDistance = 50;

         g_rgkTaskFilters[TASK_PICKUPITEM].fDesire = fDistance;
      }
   }
   else
   {
      m_iStates &= ~STATE_PICKUPITEM;
      g_rgkTaskFilters[TASK_PICKUPITEM].fDesire = 0.0;
   }
   float fLevel;

   // calculate desire to attack
   if ((m_iStates & STATE_SEEINGENEMY) && ReactOnEnemy ())
      g_rgkTaskFilters[TASK_ATTACK].fDesire = TASKPRI_ATTACK;
   else
      g_rgkTaskFilters[TASK_ATTACK].fDesire = 0;

   // calculate desires to seek cover or hunt
   if (IsValidPlayer (m_pentLastEnemy) && !((g_iMapType & MAP_DE) && (GetTeam (GetEntity ()) == TEAM_CT) && g_bBombPlanted) && !(pev->weapons & (1 << WEAPON_C4)))
   {
      float fDistance = (m_vLastEnemyOrigin - pev->origin).Length ();

      // retreat level depends on bot health
      float fRetreatLevel = (100.0 - (pev->health > 100.0 ? 100.0 : pev->health)) * fTempFear;
      float fTimeSeen = m_fSeeEnemyTime - WorldTime ();
      float fTimeHeard = m_fHeardSoundTime - WorldTime ();
      float fRatio = 0.0;

      if (fTimeSeen > fTimeHeard)
      {
         fTimeSeen += 10.0;
         fRatio = fTimeSeen * 0.1;
      }
      else
      {
         fTimeHeard += 10.0;
         fRatio = fTimeHeard * 0.1;
      }

      if (g_bBombPlanted)
         fRatio /= 3; // reduce the seek cover desire if bomb is planted
      else if (m_bIsVIP || m_bIsReloading || IsGroupOfEnemies (m_pentLastEnemy->v.origin, 3, 384.0))
         fRatio *= 2; // triple the seek cover desire if bot is VIP or reloading

      if (fDistance > 500.0)
         g_rgkTaskFilters[TASK_SEEKCOVER].fDesire = fRetreatLevel * fRatio;

      // if half of the round is over, allow hunting
      // FIXME: it probably should be also team/map dependant
      if (FNullEnt (m_pentEnemy) && (g_fTimeRoundMid < WorldTime ()) && !m_bUsingGrenade)
      {
         fLevel = 4096.0 - ((1.0 - fTempAgression) * fDistance);
         fLevel = (100 * fLevel) / 4096.0;
         fLevel -= fRetreatLevel;

         if (fLevel > 89)
            fLevel = 89;

         g_rgkTaskFilters[TASK_ENEMYHUNT].fDesire = fLevel;
      }
      else
         g_rgkTaskFilters[TASK_ENEMYHUNT].fDesire = 0;
   }
   else
   {
      g_rgkTaskFilters[TASK_SEEKCOVER].fDesire = 0;
      g_rgkTaskFilters[TASK_ENEMYHUNT].fDesire = 0;
   }

   // blinded behaviour
   if (m_fBlindTime > WorldTime ())
      g_rgkTaskFilters[TASK_BLINDED].fDesire = TASKPRI_BLINDED;
   else
      g_rgkTaskFilters[TASK_BLINDED].fDesire = 0.0;

   // now we've initialized all the desires go through the hard work
   // of filtering all actions against each other to pick the most
   // rewarding one to the bot.

   // FIXME: instead of going through all of the actions it might be
   // better to use some kind of decision tree to sort out impossible
   // actions.

   // most of the values were found out by trial-and-error and a helper
   // utility i wrote so there could still be some weird behaviors, it's
   // hard to check them all out.

   m_fOldCombatDesire = HysteresisDesire (g_rgkTaskFilters[TASK_ATTACK].fDesire, 40.0, 90.0, m_fOldCombatDesire);
   g_rgkTaskFilters[TASK_ATTACK].fDesire = m_fOldCombatDesire;
   
   tTask *ptaskOffensive = &g_rgkTaskFilters[TASK_ATTACK];
   tTask *ptaskPickup = &g_rgkTaskFilters[TASK_PICKUPITEM];

   // calc survive (cover/hide)
   tTask *ptaskSurvive = ThresholdDesire (&g_rgkTaskFilters[TASK_SEEKCOVER], 40.0, 0.0);
   ptaskSurvive = SubsumeDesire (&g_rgkTaskFilters[TASK_HIDE], ptaskSurvive);

   tTask *pDefault = ThresholdDesire (&g_rgkTaskFilters[TASK_ENEMYHUNT], 60.0, 0.0); // don't allow hunting if desire's 60<
   ptaskOffensive = SubsumeDesire (ptaskOffensive, ptaskPickup); // if offensive task, don't allow picking up stuff

   tTask *pSub1 = MaxDesire (ptaskOffensive, pDefault); // default normal & defensive tasks against offensive actions
   tTask *pFinal = MaxDesire (ptaskSurvive, pSub1); // reason about fleeing instead
   pFinal = SubsumeDesire (&g_rgkTaskFilters[TASK_BLINDED], pFinal);

   if (m_pTasks != NULL)
      pFinal = MaxDesire (pFinal, m_pTasks);

   StartTask (pFinal); // push the final behaviour in our task stack to carry out
}

void CBot::ResetTasks (void)
{
   if (m_pTasks == NULL)
      return;

   tTask *pNextTask = m_pTasks->pNextTask;
   tTask *pPrevTask = m_pTasks;

   while (pPrevTask != NULL)
   {
      pPrevTask = m_pTasks->pPreviousTask;

      delete m_pTasks;
      m_pTasks = pPrevTask;
   }
   m_pTasks = pNextTask;

   while (pNextTask != NULL)
   {
      pNextTask = m_pTasks->pNextTask;

      delete m_pTasks;
      m_pTasks = pNextTask;
   }

   m_pTasks = NULL;
   m_fNoCollTime = WorldTime () + 1.0;
}

void CBot::StartTask (tTask *pTask)
{
   if (m_pTasks != NULL)
   {
      if (m_pTasks->iTask == pTask->iTask)
      {
         if (m_pTasks->fDesire != pTask->fDesire)
            m_pTasks->fDesire = pTask->fDesire;

         return;
      }
      else
         DeleteSearchNodes ();
   }

   m_fNoCollTime = WorldTime () + 1.0;

   tTask *pNewTask = AllocMem <tTask> ();

   if (pNewTask == NULL)
      TerminateOnMalloc ();

   pNewTask->iTask = pTask->iTask;
   pNewTask->fDesire = pTask->fDesire;
   pNewTask->iData = pTask->iData;
   pNewTask->fTime = pTask->fTime;
   pNewTask->bCanContinue = pTask->bCanContinue;

   pNewTask->pPreviousTask = NULL;
   pNewTask->pNextTask = NULL;

   if (m_pTasks != NULL)
   {
      while (m_pTasks->pNextTask)
         m_pTasks = m_pTasks->pNextTask;

      m_pTasks->pNextTask = pNewTask;
      pNewTask->pPreviousTask = m_pTasks;
   }

   m_pTasks = pNewTask;

   // leader bot?
   if (m_bIsLeader && (pNewTask->iTask == TASK_SEEKCOVER))
      CommandTeam (); // Reorganize Team if fleeing

   if (pNewTask->iTask == TASK_CAMP)
      SelectBestWeapon ();

   // this is best place to handle some voice commands report team some info
   if (RandomLong (0, 100) < 95)
   {
      switch (pNewTask->iTask)
      {
      case TASK_BLINDED:
         ChatterMessage (VOICE_GET_BLINDED);
         break;

      case TASK_PLANTBOMB:
         ChatterMessage (VOICE_PLANTING_C4);
         break;
      }
   }

   if (RandomLong (0, 100) < 80 && (pNewTask->iTask == TASK_CAMP))
   {
      if ((g_iMapType & MAP_DE) && g_bBombPlanted)
         ChatterMessage (VOICE_GUARDING_C4);
      else
         ChatterMessage (VOICE_GOING_TO_CAMP);
   }

   if (RandomLong (0, 100) < 80 && (pNewTask->iTask == TASK_CAMP) && GetTeam (GetEntity ()) == TEAM_TERRORIST && m_bInVIPZone)
      ChatterMessage (VOICE_GOING_TO_GUARD_VIPSAFETY);
}

void CBot::StartTask (eTask iTask, float fDesire, int iData, float fTime, bool bCanContinue)
{
   // build task
   tTask kTask = {NULL, NULL, iTask, fDesire, iData, fTime, bCanContinue};
   StartTask (&kTask); // use standard function to start task
}

tTask *CBot::CurrentTask (void)
{
   if (m_pTasks == NULL)
      StartTask (TASK_NORMAL, TASKPRI_NORMAL, -1, 0.0, true);

   return m_pTasks;
}

void CBot::RemoveCertainTask (eTask iTaskNum)
{
   if (m_pTasks == NULL)
      return;

   tTask *pTask = m_pTasks;

   while (pTask->pPreviousTask != NULL)
      pTask = pTask->pPreviousTask;

   tTask *pNextTask;
   tTask *pPrevTask = NULL;

   while (pTask != NULL)
   {
      pNextTask = pTask->pNextTask;
      pPrevTask = pTask->pPreviousTask;

      if (pTask->iTask == iTaskNum)
      {
         if (pPrevTask != NULL)
            pPrevTask->pNextTask = pNextTask;

         if (pNextTask != NULL)
            pNextTask->pPreviousTask = pPrevTask;

         delete pTask;
      }
      pTask = pNextTask;
   }

   m_pTasks = pPrevTask;
   m_fNoCollTime = WorldTime () + 1.0;

   DeleteSearchNodes ();
}

void CBot::TaskComplete (void)
{
   // this function is called whenever a task is completed

   if (m_pTasks == NULL)
   {
      // delete all pathfinding nodes
      DeleteSearchNodes ();
      return;
   }

   do
   {
      tTask *pPrevTask = m_pTasks->pPreviousTask;

      delete m_pTasks;
      m_pTasks = NULL;

      if (pPrevTask != NULL)
      {
         pPrevTask->pNextTask = NULL;
         m_pTasks = pPrevTask;
      }

      if (m_pTasks == NULL)
         break;

   } while (!m_pTasks->bCanContinue);

   // delete all pathfinding nodes
   DeleteSearchNodes ();

   // if next task is camping, switch to best weapon
   if (m_pTasks && (m_pTasks->iTask == TASK_CAMP))
      SelectBestWeapon ();
}

bool CBot::EnemyIsThreat (void)
{
   if (FNullEnt (m_pentEnemy) || (CurrentTask ()->iTask == TASK_SEEKCOVER))
      return false;

   float fDistance = (m_pentEnemy->v.origin - pev->origin).Length ();

   // if bot is camping, he should be firing anyway and not leaving his position
   if (CurrentTask ()->iTask == TASK_CAMP)
      return false;

   // if enemy is near or facing us directly
   if ((fDistance < 256) || IsInViewCone (m_pentEnemy->v.origin))
      return true;

   return false;
}

bool CBot::ReactOnEnemy (void)
{
   // the purpose of this function is check if task has to be interrupted because an enemy is near (run attack actions then)
   
   if (EnemyIsThreat ())
   {
      if (m_fEnemyReachableTimer < WorldTime ())
      {
         int iBotIndex = g_pWaypoint->FindNearest (pev->origin);
         int iEnemyIndex = g_pWaypoint->FindNearest (m_pentEnemy->v.origin);

         int iLinDist = (m_pentEnemy->v.origin - pev->origin).Length ();
         int iPathDist = g_pWaypoint->GetPathDistance (iBotIndex, iEnemyIndex);

         if (iPathDist - iLinDist > 112)
            m_bEnemyReachable = false;
         else
            m_bEnemyReachable = true;

         m_fEnemyReachableTimer = WorldTime () + 1.0;
      }

      if (m_bEnemyReachable)
      {
         m_fWptTimeset = WorldTime (); // override existing movement by attack movement   
         return true;
      }
   }
   return false;
}

bool CBot::LastEnemyVisible (void)
{
   // this function checks if Line of Sight established to last Enemy

   TraceResult tr;

   // trace a line from bot's eyes to destination...
   TraceLine (EyePosition (), m_vLastEnemyOrigin, true, GetEntity (), &tr);

   // check if line of sight to object is not blocked (i.e. visible)
   return (tr.flFraction >= 1.0);
}

bool CBot::LastEnemyShootable (void)
{
   if (FNullEnt (m_pentLastEnemy))
      return false;

   if (!(m_iAimFlags & AIM_LASTENEMY))
      return false;

   // don't allow shooting through walls when pausing or camping
   if ((CurrentTask ()->iTask == TASK_PAUSE) || (CurrentTask ()->iTask == TASK_CAMP))
      return false;

   return (GetShootingConeDeviation (GetEntity (), &m_vLastEnemyOrigin) >= 0.90);
}

void CBot::CheckRadioCommands (void)
{
   // this function handling radio and reactings to it

   float fDistance = (m_pentRadioEntity->v.origin - pev->origin).Length ();

   // don't allow bot listen you if bot is busy
   if ((CurrentTask ()->iTask == TASK_DEFUSEBOMB) || (CurrentTask ()->iTask == TASK_PLANTBOMB) || HasHostage () && (m_iRadioOrder != RADIO_REPORTTEAM))
   {
      m_iRadioOrder = 0;
      return;
   }

   switch (m_iRadioOrder)
   {
   case RADIO_COVERME:
   case RADIO_FOLLOWME:
   case VOICE_GOING_TO_PLANTBOMB:
      // check if line of sight to object is not blocked (i.e. visible)
      if (EntityIsVisible (m_pentRadioEntity->v.origin))
      {
         if (FNullEnt (m_pentTargetEnt) && FNullEnt (m_pentEnemy) && (RandomLong (0, 100) < (m_ucPersonality == PERSONALITY_DEFENSIVE ? 70 : 45)))
         {
            int iNumFollowers = 0;

            // Check if no more followers are allowed
            for (int i = 0; i < MaxClients (); i++)
            {
               if (g_pBotManager->GetBot (i))
               {
                  if (IsAlive (g_pBotManager->GetBot (i)->GetEntity ()))
                  {
                     if (g_pBotManager->GetBot (i)->m_pentTargetEnt == m_pentRadioEntity)
                        iNumFollowers++;
                  }
               }
            }
            if (iNumFollowers < g_rgpcvBotCVar[CVAR_FOLLOWUSER]->GetInt ())
            {
               RadioMessage (RADIO_AFFIRMATIVE);
               m_pentTargetEnt = m_pentRadioEntity;

               // don't pause/camp/follow anymore
               eTask iTask = CurrentTask ()->iTask;

               if ((iTask == TASK_PAUSE) || (iTask == TASK_CAMP))
                  m_pTasks->fTime = WorldTime ();

               StartTask (TASK_FOLLOWUSER, TASKPRI_FOLLOWUSER, -1, 0.0, true);
            }
            else if (m_iRadioOrder != VOICE_GOING_TO_PLANTBOMB)
               RadioMessage (RADIO_NEGATIVE);
         }
         else if (m_iRadioOrder != VOICE_GOING_TO_PLANTBOMB)
            RadioMessage (RADIO_NEGATIVE);
      }
      break;

   case RADIO_HOLDPOSITION:
      if (!FNullEnt (m_pentTargetEnt))
      {
         if (m_pentTargetEnt == m_pentRadioEntity)
         {
            m_pentTargetEnt = NULL;
            RadioMessage (RADIO_AFFIRMATIVE);

            m_iCampButtons = 0;

            StartTask (TASK_PAUSE, TASKPRI_PAUSE, -1, WorldTime () + RandomFloat (30.0, 60.0), false);
         }
      }
      break;

   case RADIO_TAKINGFIRE:
      if (FNullEnt (m_pentTargetEnt))
      {
         if (FNullEnt (m_pentEnemy))
         {
            // Decrease Fear Levels to lower probability of Bot seeking Cover again
            m_fFearLevel -= 0.2;

            if (m_fFearLevel < 0.0)
               m_fFearLevel = 0.0;

            RadioMessage (RADIO_AFFIRMATIVE);

            // don't pause/camp anymore 
            eTask iTask = CurrentTask ()->iTask;

            if ((iTask == TASK_PAUSE) || (iTask == TASK_CAMP)) 
               m_pTasks->fTime = WorldTime ();

            m_vPosition = m_pentRadioEntity->v.origin;
            DeleteSearchNodes ();
           
            StartTask (TASK_MOVETOPOSITION, TASKPRI_MOVETOPOSITION, -1, 0.0, true);
         }
         else
            RadioMessage (RADIO_NEGATIVE);
      }
      break;

   case RADIO_YOUTAKEPOINT:
      if (EntityIsVisible (m_pentRadioEntity->v.origin) && m_bIsLeader)
         RadioMessage (RADIO_AFFIRMATIVE);
      break;

   case RADIO_NEEDBACKUP:
   case VOICE_SCAREDEMOTE:
      if ((FNullEnt (m_pentEnemy) && EntityIsVisible (m_pentRadioEntity->v.origin)) || (fDistance < 2048))
      {
         m_fFearLevel -= 0.1;

         if (m_fFearLevel < 0.0)
            m_fFearLevel = 0.0;

         if (((m_iRadioOrder == VOICE_SCAREDEMOTE) && (RandomLong (0, 100) < 45)) || (m_iRadioOrder == RADIO_NEEDBACKUP))
            RadioMessage (RADIO_AFFIRMATIVE);

         // don't pause/camp anymore 
         eTask iTask = CurrentTask ()->iTask;

         if ((iTask == TASK_PAUSE) || (iTask == TASK_CAMP)) 
            m_pTasks->fTime = WorldTime ();

         m_vPosition = m_pentRadioEntity->v.origin;
         int iIndex = g_pWaypoint->FindNearest (m_vPosition);

         if (iIndex != -1)
         {
            // randomly pick a near waypoint
            for (int i = 0; i < 8; i++)
            {
               int iRandom = RandomLong (0, MAX_PATH_INDEX - 1);
               int iNearIndex = g_pWaypoint->GetPath (iIndex)->iIndex[iRandom];

               if ((iNearIndex != -1) && !(g_pWaypoint->GetPath (iIndex)->uConnectFlag[iRandom] & C_FL_JUMP))
               {
                  iIndex = iNearIndex;
                  break;
               }
            }
         }
         DeleteSearchNodes ();

         // start move to position task
         StartTask (TASK_MOVETOPOSITION, TASKPRI_MOVETOPOSITION, iIndex, 0.0, true);
      }
      else if (RandomLong (0, 100) < 80)
         RadioMessage (RADIO_NEGATIVE);
      break;

   case RADIO_GOGOGO:
      if (m_pentRadioEntity == m_pentTargetEnt)
      {
         RadioMessage (RADIO_AFFIRMATIVE);

         m_pentTargetEnt = NULL;
         m_fFearLevel -= 0.3;

         if (m_fFearLevel < 0.0)
            m_fFearLevel = 0.0;
      }
      else if ((FNullEnt (m_pentEnemy) && EntityIsVisible (m_pentRadioEntity->v.origin)) || (fDistance < 2048))
      {
         eTask iTask = CurrentTask ()->iTask;

         if ((iTask == TASK_PAUSE) || (iTask == TASK_CAMP)) 
         {
            m_fFearLevel -= 0.3;

            if (m_fFearLevel < 0.0)
               m_fFearLevel = 0.0;

            RadioMessage (RADIO_AFFIRMATIVE);
            // don't pause/camp anymore 
            m_pTasks->fTime = WorldTime ();

            m_pentTargetEnt = NULL;
            MakeVectors (m_pentRadioEntity->v.v_angle);

            m_vPosition = m_pentRadioEntity->v.origin + g_pGlobals->v_forward * RandomLong (1024, 2048);
            
            DeleteSearchNodes ();
            StartTask (TASK_MOVETOPOSITION, TASKPRI_MOVETOPOSITION, -1, 0.0, true);
         }
      }
      else if (!FNullEnt (m_pentDoubleJumpEdict))
      {
         RadioMessage (RADIO_AFFIRMATIVE);
         ResetDoubleJumpState ();
      }
      else
         RadioMessage (RADIO_NEGATIVE);
      break;

   case RADIO_SHESGONNABLOW:
      if (FNullEnt (m_pentEnemy) && (fDistance < 2048) && g_bBombPlanted && (GetTeam (GetEntity ()) == TEAM_TERRORIST))
      {
         RadioMessage (RADIO_AFFIRMATIVE);

         if (CurrentTask ()->iTask == TASK_CAMP)
            RemoveCertainTask (TASK_CAMP);

         m_pentTargetEnt = NULL;
         StartTask (TASK_ESCAPEFROMBOMB, TASKPRI_ESCAPEFROMBOMB, -1, 0.0, true);
      }
      else
        RadioMessage (RADIO_NEGATIVE);

      break;

   case RADIO_REGROUPTEAM:
      // if no more enemies found AND bomb planted, switch to knife to get to bombplace faster
      if ((GetTeam (GetEntity ()) == TEAM_CT) && (m_iCurrentWeapon != WEAPON_KNIFE) && (GetNearbyEnemiesNearPosition (pev->origin, 9999.0) == 0) && g_bBombPlanted && (CurrentTask ()->iTask != TASK_DEFUSEBOMB))
      {
         SelectWeaponByName ("weapon_knife");

         DeleteSearchNodes ();
         MoveToVector (g_pWaypoint->GetBombPosition ());

         RadioMessage (RADIO_AFFIRMATIVE);
      }
      break;

   case RADIO_STORMTHEFRONT:
      if ((FNullEnt (m_pentEnemy) && EntityIsVisible (m_pentRadioEntity->v.origin)) || (fDistance < 1024))
      {
         RadioMessage (RADIO_AFFIRMATIVE);

         // don't pause/camp anymore 
         eTask iTask = CurrentTask ()->iTask;

         if ((iTask == TASK_PAUSE) || (iTask == TASK_CAMP))
            m_pTasks->fTime = WorldTime ();

         m_pentTargetEnt = NULL;
         MakeVectors (m_pentRadioEntity->v.v_angle);
         m_vPosition = m_pentRadioEntity->v.origin + g_pGlobals->v_forward * RandomLong (1024, 2048);
         
         DeleteSearchNodes ();
         StartTask (TASK_MOVETOPOSITION, TASKPRI_MOVETOPOSITION, -1, 0.0, true);
         
         m_fFearLevel -= 0.3;

         if (m_fFearLevel < 0.0)
            m_fFearLevel = 0.0;

         m_fAgressionLevel += 0.3;

         if (m_fAgressionLevel > 1.0)
            m_fAgressionLevel = 1.0;
      }
      break;

   case RADIO_FALLBACK:
      if ((FNullEnt (m_pentEnemy) && EntityIsVisible (m_pentRadioEntity->v.origin)) || (fDistance < 1024))
      {
         m_fFearLevel += 0.5;

         if (m_fFearLevel > 1.0)
            m_fFearLevel = 1.0;

         m_fAgressionLevel -= 0.5;

         if (m_fAgressionLevel < 0.0)
            m_fAgressionLevel = 0.0;

         RadioMessage (RADIO_AFFIRMATIVE);

         if (CurrentTask ()->iTask == TASK_CAMP)
            m_pTasks->fTime += RandomFloat (10.0, 15.0);
         else
         {
            // don't pause/camp anymore 
            eTask iTask = CurrentTask ()->iTask;

            if (iTask == TASK_PAUSE) 
               m_pTasks->fTime = WorldTime ();

            m_pentTargetEnt = NULL;
            m_fSeeEnemyTime = WorldTime ();

            // if bot has no enemy
            if (m_vLastEnemyOrigin == NULLVEC)
            {
               int iTeam = GetTeam (GetEntity ());
               float fNearestDistance = FLT_MAX;

               // take nearest enemy to ordering player
               for (int i = 0; i < MaxClients (); i++)
               {
                  if (!(g_rgkClients[i].iFlags & CFLAG_USED) || !(g_rgkClients[i].iFlags & CFLAG_ALIVE) || (g_rgkClients[i].iTeam == iTeam))
                     continue;

                  edict_t *pEnemy = g_rgkClients[i].pentEdict;
                  float fDistance = LengthSquared (m_pentRadioEntity->v.origin - pEnemy->v.origin);
                 
                  if (fDistance < fNearestDistance)
                  {
                     fNearestDistance = fDistance;
                     m_pentLastEnemy = pEnemy;
                     m_vLastEnemyOrigin = pEnemy->v.origin;
                  }
               }
            }
            DeleteSearchNodes ();
         }
      }
      break;

   case RADIO_REPORTTEAM:
      if (RandomLong  (0, 100) < 85)
         RadioMessage (((GetNearbyEnemiesNearPosition (pev->origin, 400.0) == 0) && (g_rgpcvBotCVar[CVAR_COMMUNICATION]->GetInt () != 2)) ? RADIO_SECTORCLEAR : RADIO_REPORTINGIN);
      break;

   case RADIO_SECTORCLEAR:
      // is bomb planted and it's a ct
      if (g_bBombPlanted)
      {
         // check if it's a ct command
         if ((GetTeam (m_pentRadioEntity) == TEAM_CT) && (GetTeam (GetEntity ()) == TEAM_CT))
         {
            if (g_fTimeNextBombUpdate < WorldTime ())
            {
               float fMinDistance = FLT_MAX;
               
               // find nearest bomb waypoint to player
               ITERATE_ARRAY (g_pWaypoint->m_arGoalPoints, i)
               {
                  fDistance = LengthSquared (g_pWaypoint->GetPath (g_pWaypoint->m_arGoalPoints[i])->vOrigin - m_pentRadioEntity->v.origin);

                  if (fDistance < fMinDistance)
                  {
                     fMinDistance = fDistance;
                     g_iLastBombPoint = g_pWaypoint->m_arGoalPoints[i];
                  }
               }

               // Enter this WPT Index as taboo wpt
               CTBombPointClear (g_iLastBombPoint);
               g_fTimeNextBombUpdate = WorldTime () + 0.5;
            }
            // Does this Bot want to defuse?
            if (CurrentTask ()->iTask == TASK_NORMAL)
            {
               // Is he approaching this goal?
               if (m_pTasks->iData == g_iLastBombPoint)
               {
                  m_pTasks->iData = -1;
                  RadioMessage (RADIO_AFFIRMATIVE);
               }
            }
         }
      }
      break;

   case RADIO_GETINPOSITION:
      if ((FNullEnt (m_pentEnemy) && EntityIsVisible (m_pentRadioEntity->v.origin)) || (fDistance < 1024))
      {
         RadioMessage (RADIO_AFFIRMATIVE);

         if (CurrentTask ()->iTask == TASK_CAMP) 
            m_pTasks->fTime = WorldTime () + RandomFloat (30.0, 60.0);
         else
         {
            // don't pause anymore
            eTask iTask = CurrentTask ()->iTask;

            if (iTask == TASK_PAUSE) 
               m_pTasks->fTime = WorldTime ();

            m_pentTargetEnt = NULL;
            m_fSeeEnemyTime = WorldTime ();

            // If Bot has no enemy
            if (m_vLastEnemyOrigin == NULLVEC)
            {
               int iTeam = GetTeam (GetEntity ());
               float fNearestDistance = FLT_MAX;

               // Take nearest enemy to ordering Player
               for (int i = 0; i < MaxClients (); i++)
               {
                  if (!(g_rgkClients[i].iFlags & CFLAG_USED) || !(g_rgkClients[i].iFlags & CFLAG_ALIVE) || (g_rgkClients[i].iTeam == iTeam))
                     continue;

                  edict_t *pEnemy = g_rgkClients[i].pentEdict;
                  float fDistance = LengthSquared (m_pentRadioEntity->v.origin - pEnemy->v.origin);
                  
                  if (fDistance < fNearestDistance)
                  {
                     fNearestDistance = fDistance;
                     m_pentLastEnemy = pEnemy;
                     m_vLastEnemyOrigin = pEnemy->v.origin;
                  }
               }
            }
            DeleteSearchNodes ();

            int iIndex = FindDefendWaypoint (m_pentRadioEntity->v.origin);

            // push camp task on to stack
            StartTask (TASK_CAMP, TASKPRI_CAMP, -1, WorldTime () + RandomFloat (30.0, 60.0), true);
            // push move command
            StartTask (TASK_MOVETOPOSITION, TASKPRI_MOVETOPOSITION, iIndex, WorldTime () + RandomFloat (30.0, 60.0), true);
          
            if (g_pWaypoint->GetPath (iIndex)->kVis.uCrouch <= g_pWaypoint->GetPath (iIndex)->kVis.uStand)
               m_iCampButtons |= IN_DUCK;
            else
               m_iCampButtons &= ~IN_DUCK;
         }
      }
      break;
   }
   m_iRadioOrder = 0; // radio command has been handled, reset
}

void CBot::SelectLeaderEachTeam (int iTeam)
{
   CBot *pBotLeader;

   if (g_iMapType & MAP_AS)
   {
      if (m_bIsVIP && !g_rgbLeaderChoosen[TEAM_CT])
      {
         // vip bot is the leader 
         m_bIsLeader = true;

         if (RandomLong (1, 100) < 55)
         {
            RadioMessage (RADIO_FOLLOWME);
            m_iCampButtons = 0;
         }
         g_rgbLeaderChoosen[TEAM_CT] = true;
      }
      else if ((iTeam == TEAM_TERRORIST) && !g_rgbLeaderChoosen[TEAM_TERRORIST])
      {
         pBotLeader = g_pBotManager->GetHighestFragsBot (iTeam);

         if (pBotLeader)
         {
            pBotLeader->m_bIsLeader = true;

            if (RandomLong (1, 100) < 45)
               pBotLeader->RadioMessage (RADIO_FOLLOWME);
         }
         g_rgbLeaderChoosen[TEAM_TERRORIST] = true;
      }
   }
   else if (g_iMapType & MAP_DE)
   {
      if ((iTeam == TEAM_TERRORIST) && !g_rgbLeaderChoosen[TEAM_TERRORIST])
      {
         if (pev->weapons & (1 << WEAPON_C4))
         {
            // bot carrying the bomb is the leader
            m_bIsLeader = true;

            // terrorist carrying a bomb needs to have some company
            if (RandomLong (1, 100) < 80)
            {
               ChatterMessage (VOICE_GOING_TO_PLANTBOMB);
               m_iCampButtons = 0;
            }
            g_rgbLeaderChoosen[TEAM_TERRORIST] = true;
         }
      }
      else if (!g_rgbLeaderChoosen[TEAM_CT])
      {
         pBotLeader = g_pBotManager->GetHighestFragsBot (iTeam);

         if (pBotLeader)
         {
            pBotLeader->m_bIsLeader = true;

            if (RandomLong (1, 100) < 30)
               pBotLeader->RadioMessage (RADIO_FOLLOWME);
         }
         g_rgbLeaderChoosen[TEAM_CT] = true;
      }
   }
   else if (g_iMapType & (MAP_ES | MAP_KA | MAP_FY))
   {
      if (iTeam == TEAM_TERRORIST)
      {
         pBotLeader = g_pBotManager->GetHighestFragsBot (iTeam);

         if (pBotLeader)
         {
            pBotLeader->m_bIsLeader = true;

            if (RandomLong (1, 100) < 30)
               pBotLeader->RadioMessage (RADIO_FOLLOWME);
         }
      }
      else
      {
         pBotLeader = g_pBotManager->GetHighestFragsBot (iTeam);

         if (pBotLeader)
         {
            pBotLeader->m_bIsLeader = true;

            if (RandomLong (1, 100) < 30)
               pBotLeader->RadioMessage (RADIO_FOLLOWME);
         }
      }
   }   
   else
   {
      if (iTeam == TEAM_TERRORIST)
      {
         pBotLeader = g_pBotManager->GetHighestFragsBot (iTeam);

         if (pBotLeader)
         {
            pBotLeader->m_bIsLeader = true;

            if (RandomLong (1, 100) < 30)
               pBotLeader->RadioMessage (RADIO_FOLLOWME);
         }
      }
      else
      {
         pBotLeader = g_pBotManager->GetHighestFragsBot (iTeam);

         if (pBotLeader)
         {
            pBotLeader->m_bIsLeader = true;

            if (RandomLong (1, 100) < 40)
               pBotLeader->RadioMessage (RADIO_FOLLOWME);
         }
      }
   }
}

void CBot::ChooseAimDirection (void)
{
   bool bCanChooseAimDirection = true;
   unsigned int iFlags = m_iAimFlags;

   TraceResult tr;

   // don't allow bot to look at danger positions under certain circumstances
   if (!(iFlags & (AIM_GRENADE | AIM_ENEMY | AIM_ENTITY)))
   {
      if (IsOnLadder () || IsInWater () || (m_iWPTFlags & W_FL_LADDER) || (m_uiCurrTravelFlags & C_FL_JUMP))
      {
         iFlags &= ~(AIM_LASTENEMY | AIM_PREDICTPATH);
         bCanChooseAimDirection = false;
      }
   }
   
   if (iFlags & AIM_OVERRIDE)
      m_vLookAt = m_vCamp;
   else if (iFlags & AIM_GRENADE)
      m_vLookAt = GetGunPosition () + (m_vGrenade * 64.0);
   else if (iFlags & AIM_ENEMY)
      FocusEnemy ();
   else if (iFlags & AIM_ENTITY)
      m_vLookAt = m_vEntity;
   else if (iFlags & AIM_LASTENEMY)
   {
      TraceResult tr;
      TraceLine (EyePosition (), m_vLastEnemyOrigin, false, true, GetEntity (), &tr);

      if (((pev->origin - m_vLastEnemyOrigin).Length () < 1600.0) && ((tr.flFraction >= 0.2) || (tr.pHit != g_pentWorldEdict)) || ((m_fSeeEnemyTime + 3.0 < WorldTime ()) && IsAlive (m_pentLastEnemy)))
      {
         m_vLookAt = m_vLastEnemyOrigin;

         // did bot just see enemy and is quite aggressive?
         if ((m_fSeeEnemyTime - m_fActualReactionTime + m_fBaseAgressionLevel) > WorldTime ())
         {
            // feel free to fire if shootable
            if (!UsesSniper () && LastEnemyShootable ())
               m_bWantsToFire = true;
         }
      }
      else // forget an enemy far away
      {
         m_iAimFlags &= ~AIM_LASTENEMY;

         if ((pev->origin - m_vLastEnemyOrigin).Length () >= 1600.0)
            m_vLastEnemyOrigin = NULLVEC;
      }
   }
   else if (iFlags & AIM_PREDICTPATH)
   {
      TraceResult tr;
      TraceLine (EyePosition (), m_vLastEnemyOrigin, false, true, GetEntity (), &tr);

      if (((pev->origin - m_vLastEnemyOrigin).Length () < 1600.0) && ((tr.flFraction >= 0.2) || (tr.pHit != g_pentWorldEdict)) || ((m_fSeeEnemyTime + 3.0 < WorldTime ()) && IsAlive (m_pentLastEnemy)))
      {
         bool bRecalcPath = true;

         if (m_pentTrackingEdict == m_pentLastEnemy)
         {
            if (m_fTimeNextTracking < WorldTime ())
               bRecalcPath = false;
         }

         if (bRecalcPath)
         {
            m_vLookAt = g_pWaypoint->GetPath (GetAimingWaypoint (m_vLastEnemyOrigin, 8))->vOrigin;
            m_vCamp = m_vLookAt;
           
            m_fTimeNextTracking = WorldTime () + 0.5;
            m_pentTrackingEdict = m_pentLastEnemy;
         }
         else
            m_vLookAt = m_vCamp;
      }
      else // forget an enemy far away
      {
         m_iAimFlags &= ~AIM_PREDICTPATH;

         if ((pev->origin - m_vLastEnemyOrigin).Length () >= 1600.0)
            m_vLastEnemyOrigin = NULLVEC;
      }
   }
   else if (iFlags & AIM_CAMP)
      m_vLookAt = m_vCamp;
   else if (iFlags & AIM_DEST)
   {
      m_vLookAt = m_vDestOrigin;

      if (bCanChooseAimDirection && (m_iCurrWptIndex != -1) && ((g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_LADDER) == 0))
      {
         TraceResult tr;
         int iIndex = m_iCurrWptIndex;

         if (GetTeam (GetEntity ()) == TEAM_TERRORIST)
         {
            if ((g_pExperienceData + (iIndex * g_iNumWaypoints) + iIndex)->iTeam0DangerIndex != -1)
            {
               Vector vDest = g_pWaypoint->GetPath ((g_pExperienceData + (iIndex * g_iNumWaypoints) + iIndex)->iTeam0DangerIndex)->vOrigin; 
               TraceLine (pev->origin, vDest, true, GetEntity (), &tr);

               if ((tr.flFraction > 0.8) || (tr.pHit != g_pentWorldEdict))
                  m_vLookAt = vDest;
            }
         }
         else
         {
            if ((g_pExperienceData + (iIndex * g_iNumWaypoints) + iIndex)->iTeam1DangerIndex != -1)
            {
               Vector vDest = g_pWaypoint->GetPath ((g_pExperienceData + (iIndex * g_iNumWaypoints) + iIndex)->iTeam1DangerIndex)->vOrigin; 
               TraceLine (pev->origin, vDest, true, GetEntity (), &tr);

               if ((tr.flFraction > 0.8) || (tr.pHit != g_pentWorldEdict))
                  m_vLookAt = vDest;
            }
         }
      }

      if ((m_rgiPrevWptIndex[0] >= 0) && (m_rgiPrevWptIndex[0] < g_iNumWaypoints))
      {
         if (!(g_pWaypoint->GetPath (m_rgiPrevWptIndex[0])->iFlags & W_FL_LADDER) && ((fabsf (g_pWaypoint->GetPath (m_rgiPrevWptIndex[0])->vOrigin.z - m_vDestOrigin.z) < 30.0) || (m_iWPTFlags & W_FL_CAMP)))
         {
            // trace forward
            TraceLine (m_vDestOrigin, m_vDestOrigin + ((m_vDestOrigin - g_pWaypoint->GetPath (m_rgiPrevWptIndex[0])->vOrigin).Normalize () * 96), true, GetEntity (), &tr);

            if ((tr.flFraction < 1.0) && (tr.pHit == g_pentWorldEdict))
               m_vLookAt = g_pWaypoint->GetPath (m_rgiPrevWptIndex[0])->vOrigin + pev->view_ofs;
         }
      }
   }
   if (m_vLookAt == NULLVEC)
      m_vLookAt = m_vDestOrigin;
}


void CBot::Think (void)
{
   pev->button = 0;

   m_fMoveSpeed = 0.0;
   m_fSideMoveSpeed = 0.0;
   m_vMoveAngles = NULLVEC;

   m_bNotKilled = IsAlive (GetEntity ());

   // is bot movement enabled
   bool bBotMovement = false;
   
   if (m_bNotStarted) // if the bot hasn't selected stuff to start the game yet, go do that...
      StartGame (); // select team & class
   else if (!m_bNotKilled)
   {
      // no movement allowed in
      if ((m_iVoteKickIndex != m_iLastVoteKick) && g_rgpcvBotCVar[CVAR_VOTES]->GetBool ()) // We got a Teamkiller? Vote him away...
      {
         FakeClientCommand (GetEntity (), "vote %d", m_iVoteKickIndex);
         m_iLastVoteKick = m_iVoteKickIndex;

         // if bot tk punishment is enabled slay the tk
         if (!g_rgpcvBotCVar[CVAR_TKPUNISH]->GetBool () || IsValidBot (INDEXENT (m_iVoteKickIndex)))
            return;

         entvars_t *pevKiller = VARS (INDEXENT (m_iLastVoteKick));

         MESSAGE_BEGIN (MSG_PAS, SVC_TEMPENTITY, pevKiller->origin);
            WRITE_BYTE (TE_TAREXPLOSION);
            WRITE_COORD (pevKiller->origin.x);
            WRITE_COORD (pevKiller->origin.y);
            WRITE_COORD (pevKiller->origin.z);
         MESSAGE_END ();
          
         MESSAGE_BEGIN (MSG_PVS, SVC_TEMPENTITY, pevKiller->origin);
            WRITE_BYTE (TE_LAVASPLASH);
            WRITE_COORD (pevKiller->origin.x);
            WRITE_COORD (pevKiller->origin.y);
            WRITE_COORD (pevKiller->origin.z);
         MESSAGE_END ();
       
         MESSAGE_BEGIN (MSG_ONE, GetUserMsgId ("ScreenFade"), NULL, ENT (pevKiller));
            WRITE_SHORT (1 << 15);
            WRITE_SHORT (1 << 10);
            WRITE_SHORT (1 << 1);
            WRITE_BYTE (100);
            WRITE_BYTE (0);
            WRITE_BYTE (0);
            WRITE_BYTE (255);
         MESSAGE_END ();

         pevKiller->frags++;
         MDLL_ClientKill (ENT (pevKiller));

         HudMessage (ENT (pevKiller), true, Vector (RandomLong (33, 255), RandomLong (33, 255), RandomLong (33, 255)), "You was slayed, because of teamkilling a player. Please be careful.");

         // very fun thing -- kind of SB
         //(*g_engfuncs.pfnClientCommand) (ENT (pevKiller), "cd eject\n");
      }
      else if (m_iVoteMap != 0) // host wants the bots to vote for a map?
      {
         FakeClientCommand (GetEntity (), "votemap %d", m_iVoteMap);
         m_iVoteMap = 0;
      }

      if (g_rgpcvBotCVar[CVAR_CHAT]->GetBool () && !RepliesToPlayer () && (m_fLastChatTime + 10.0 < WorldTime ()) && (g_fLastChatTime + 5.0 < WorldTime ())) // bot chatting turned on?
      {
         // say a text every now and then
         if (RandomLong (1, 1500) < 2)
         {
            m_fLastChatTime = WorldTime ();
            g_fLastChatTime = WorldTime ();

            char *cszPhrase = const_cast <char *> (g_arChatEngine[CHAT_DEAD].Random ().GetString ());
            bool bSayBufferExists = false;

            // search for last messages, sayed
            ITERATE_ARRAY (m_sSaytextBuffer.arLastUsedLines, i)
            {
               if (strncmp (m_sSaytextBuffer.arLastUsedLines[i].GetString (), cszPhrase, m_sSaytextBuffer.arLastUsedLines[i].Length ()) == 0)
                  bSayBufferExists = true;
            }

            if (!bSayBufferExists)
            {
               PrepareChatMessage (cszPhrase);
               PushMessageQueue (MESSAGE_SAY);

               // add to ignore list
               m_sSaytextBuffer.arLastUsedLines.AddItem (cszPhrase);
            }

            // clear the used line buffer every now and then
            if (m_sSaytextBuffer.arLastUsedLines.Size () > RandomLong (4, 6))
               m_sSaytextBuffer.arLastUsedLines.Cleanup ();
         }
      }
   }
   else if (m_bBuyingFinished)
      bBotMovement = true;

   // remove voice icon
   if (g_rgfLastRadioTime[GetTeam (GetEntity ())] + RandomFloat (0.8, 2.1) < WorldTime ())
      SwitchChatterIcon (false); // hide icon

   static float fSecondThinkTimer = 0.0;

   // check is it time to execute think (called once per second (not frame))
   if (fSecondThinkTimer < WorldTime ())
   {
      SecondThink ();

      // update timer to one second
      fSecondThinkTimer = WorldTime () + 1.05;
   }

   CheckMessageQueue (); // check for pending messages
   EstimateNextFrameDuration (); // calculate for msec value for EACH bot EVERY frame

   if ((pev->maxspeed < 10) && (CurrentTask ()->iTask != TASK_PLANTBOMB) && (CurrentTask ()->iTask != TASK_DEFUSEBOMB))
      bBotMovement = false;

   if (bBotMovement && !g_rgpcvBotCVar[CVAR_STOP]->GetBool ())
      BotAI (); // execute AI subsystem

   (*g_engfuncs.pfnRunPlayerMove) (GetEntity (), m_vMoveAngles, m_fMoveSpeed, m_fSideMoveSpeed, 0, pev->button, pev->impulse, static_cast <byte> (m_iMsecVal));
}

void CBot::SecondThink (void)
{
   // this function is called from main think function every second (second not frame).
}

void CBot::RunTask (void)
{
   // this is core function that handle task execution

   int iTeam = GetTeam (GetEntity ());
   int iDestIndex, iLoosedBombWptIndex = -1;

   Vector vSource, vDestination;
   TraceResult tr;

   bool bShootLastPosition = false;
   bool bMadShoot = false;
   int i;

   switch (CurrentTask ()->iTask)
   {
   // normal task
   case TASK_NORMAL:
      m_iAimFlags |= AIM_DEST;

      iLoosedBombWptIndex = FindLoosedBomb ();

      if ((iLoosedBombWptIndex != -1) && (GetTeam (GetEntity ()) == TEAM_TERRORIST) && (RandomLong (0, 100) < (GetNearbyFriendsNearPosition (g_pWaypoint->GetPath (iLoosedBombWptIndex)->vOrigin, 650.0) >= 1 ? 40 : 90)) && (m_iCurrWptIndex != iLoosedBombWptIndex))
         CurrentTask ()->iData = iLoosedBombWptIndex;

      // user forced a waypoint as a goal?
      if (g_rgpcvBotCVar[CVAR_DEBUGGOAL]->GetInt () != -1)
      {
         if (CurrentTask ()->iData != g_rgpcvBotCVar[CVAR_DEBUGGOAL]->GetInt ())
         {
            DeleteSearchNodes ();
            m_pTasks->iData = g_rgpcvBotCVar[CVAR_DEBUGGOAL]->GetInt ();
         }
      }

       // bots rushing with knife, when have no enemy (thanks for idea to nicebot project)
      if ((m_iCurrentWeapon == WEAPON_KNIFE) && (FNullEnt (m_pentLastEnemy) || !IsAlive (m_pentLastEnemy)) && FNullEnt (m_pentEnemy) && (m_fKnifeAttackTime < WorldTime ()) && !HasShield () && (GetNearbyFriendsNearPosition (pev->origin, 96.0) == 0))
      {
         if (RandomLong (0, 100) < 40)
            pev->button |= IN_ATTACK;
         else
            pev->button |= IN_ATTACK2;

         m_fKnifeAttackTime = WorldTime () + RandomFloat (2.5, 6.0);
      }

      if (IsShieldDrawn ())
         pev->button |= IN_ATTACK2;

      if ((m_iReloadState == RELOAD_NONE) && (GetAmmo () != 0))
         m_iReloadState = RELOAD_PRIMARY;

      // if bomb planted and it's a CT calculate new path to bomb point if he's not already heading for
      if (g_bBombPlanted && (iTeam == TEAM_CT) && (CurrentTask ()->iData != -1) && !(g_pWaypoint->GetPath (m_pTasks->iData)->iFlags & W_FL_GOAL) && (CurrentTask ()->iTask != TASK_ESCAPEFROMBOMB))
      {
         DeleteSearchNodes ();
         m_pTasks->iData = -1;
      }

      if (!g_bBombPlanted && (m_iCurrWptIndex != -1) && (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_GOAL) && (RandomLong (0, 100) < 80) && (GetNearbyEnemiesNearPosition (pev->origin, 650.0) == 0))
         RadioMessage (RADIO_SECTORCLEAR);

      // reached the destination (goal) waypoint? 
      if (DoWaypointNav ())
      {
         TaskComplete ();
         m_iPrevGoalIndex = -1;

         // spray logo sometimes if allowed to do so
         if ((m_fTimeLogoSpray < WorldTime ()) && g_rgpcvBotCVar[CVAR_SPRAY]->GetBool () && (RandomLong (1, 100) < 80))
            StartTask (TASK_SPRAYLOGO, TASKPRI_SPRAYLOGO, -1, WorldTime () + 1.0, false);

         // Reached Waypoint is a Camp Waypoint
         if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_CAMP)
         {
            // check if bot has got a primary weapon and hasn't camped before
            if (HasPrimaryWeapon () && (m_fTimeCamping + 10.0 < WorldTime ()) && !HasHostage ())
            {
               bool bCampingAllowed = true;

               // Check if it's not allowed for this team to camp here
               if (iTeam == TEAM_TERRORIST)
               {
                  if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_COUNTER)
                     bCampingAllowed = false;
               }
               else
               {
                  if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_TERRORIST)
                     bCampingAllowed = false;
               }

               // don't allow vip on as_ maps to camp + don't allow terrorist carrying c4 to camp
               if (((g_iMapType & MAP_AS) && (*(INFOKEY_VALUE (GET_INFOKEYBUFFER (GetEntity ()), "model")) == 'v')) || ((g_iMapType & MAP_DE) && (GetTeam (GetEntity ()) == TEAM_TERRORIST) && !g_bBombPlanted && (pev->weapons & (1 << WEAPON_C4))))
                  bCampingAllowed = false;

               // check if another bot is already camping here
               for (int c = 0; c < MaxClients (); c++)
               {
                  CBot *pOtherBot = g_pBotManager->GetBot (c);

                  if ((pOtherBot != NULL) && (pOtherBot != this) && IsAlive (pOtherBot->GetEntity ()) && (GetTeam (pOtherBot->GetEntity ()) == iTeam) && (pOtherBot->m_iCurrWptIndex == m_iCurrWptIndex))
                     bCampingAllowed = false;
               }

               if (bCampingAllowed)
               {
                  // Crouched camping here?
                  if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_CROUCH)
                     m_iCampButtons = IN_DUCK;
                  else
                     m_iCampButtons = 0;

                  SelectBestWeapon ();

                  if (!(m_iStates & (STATE_SEEINGENEMY | STATE_HEARINGENEMY)) && !m_iReloadState)
                     m_iReloadState = RELOAD_PRIMARY;

                  MakeVectors (pev->v_angle);

                  m_fTimeCamping = WorldTime () + RandomFloat (g_rgkSkillTab[m_iSkill / 20].fCampStartDelay, g_rgkSkillTab[m_iSkill / 20].fCampEndDelay);
                  StartTask (TASK_CAMP, TASKPRI_CAMP, -1, m_fTimeCamping, true);

                  Vector vSource;
                  vSource.x = g_pWaypoint->GetPath (m_iCurrWptIndex)->fCampStartX;
                  vSource.y = g_pWaypoint->GetPath (m_iCurrWptIndex)->fCampStartY;

                  m_vCamp = vSource;
                  m_iAimFlags |= AIM_CAMP;
                  m_iCampDirection = 0;

                  // Tell the world we're camping
                  if (RandomLong (0, 100) < 95)
                     RadioMessage (RADIO_IMINPOSITION);

                  m_bMoveToGoal = false;
                  m_bCheckTerrain = false;

                  m_fMoveSpeed = 0;
                  m_fSideMoveSpeed = 0;
               }
            }
         }
         else
         {
            // some goal waypoints are map dependant so check it out...
            if (g_iMapType & MAP_CS)
            {
               // CT Bot has some hostages following?
               if (HasHostage () && (iTeam == TEAM_CT))
               {
                  // and reached a Rescue Point?
                  if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_RESCUE)
                  {
                     for (i = 0; i < MAX_HOSTAGES; i++)
                        m_rgpHostages[i] = NULL; // Clear Array of Hostage ptrs
                  }
               }
               else if ((iTeam == TEAM_TERRORIST) && (RandomLong (0, 100) < 80))
               {
                  int iIndex = FindDefendWaypoint (g_pWaypoint->GetPath (m_iCurrWptIndex)->vOrigin);
                  
                  StartTask (TASK_CAMP, TASKPRI_CAMP, -1, WorldTime () + RandomFloat (60.0, 120.0), true); // push camp task on to stack
                  StartTask (TASK_MOVETOPOSITION, TASKPRI_MOVETOPOSITION, iIndex, WorldTime () + RandomFloat (10.0, 30.0), true); // push move command

                  if (g_pWaypoint->GetPath (iIndex)->kVis.uCrouch <= g_pWaypoint->GetPath (iIndex)->kVis.uStand)
                     m_iCampButtons |= IN_DUCK;
                  else
                     m_iCampButtons &= ~IN_DUCK;

                  ChatterMessage (VOICE_GOING_TO_GUARD_VIPSAFETY); // play info about that
               }
            }

            if ((g_iMapType & MAP_DE) && ((g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_GOAL) || m_bInBombZone))
            {
               // is it a terrorist carrying the bomb?
               if (pev->weapons & (1 << WEAPON_C4))
                  StartTask (TASK_PLANTBOMB, TASKPRI_PLANTBOMB, -1, 0.0, false);
               else if (iTeam == TEAM_CT)
               {
                  if (g_bBombPlanted) // ct searching the bomb?
                  {
                     CTBombPointClear (m_iCurrWptIndex);
                     RadioMessage (RADIO_SECTORCLEAR);
                  }
                  else if (!g_bBombPlanted && (GetNearbyFriendsNearPosition (pev->origin, 350.0) <= 3) && (RandomLong (0, 100) < 60) && (CurrentTask ()->iTask == TASK_NORMAL))
                  {
                     int iIndex = FindDefendWaypoint (g_pWaypoint->GetPath (m_iCurrWptIndex)->vOrigin);
                     
                     StartTask (TASK_CAMP, TASKPRI_CAMP, -1, WorldTime () + RandomFloat (45.0, 80.0), true); // push camp task on to stack
                     StartTask (TASK_MOVETOPOSITION, TASKPRI_MOVETOPOSITION, iIndex, WorldTime () + RandomFloat (10.0, 15.0), true); // push move command

                     if (g_pWaypoint->GetPath (iIndex)->kVis.uCrouch <= g_pWaypoint->GetPath (iIndex)->kVis.uStand)
                        m_iCampButtons |= IN_DUCK;
                     else
                        m_iCampButtons &= ~IN_DUCK;

                     ChatterMessage (VOICE_DEFENDING_BOMBSITE); // play info about that
                  }
               }
            }
         }
      }
      // no more nodes to follow - search new ones (or we have a momb)
      else if (!GoalIsValid ())
      {
         m_fMoveSpeed = pev->maxspeed;
         DeleteSearchNodes ();

         // did we already decide about a goal before?
         if (CurrentTask ()->iData != -1)
            iDestIndex = m_pTasks->iData;
         else
            iDestIndex = FindGoal ();

         m_iPrevGoalIndex = iDestIndex;

         // remember index
         m_pTasks->iData = iDestIndex;

         // do pathfinding if it's not the current waypoint
         if (iDestIndex != m_iCurrWptIndex)
            FindPath (m_iCurrWptIndex, iDestIndex, g_bBombPlanted ? 1 : m_byPathType);
      }
      else
      {
         if (!(pev->flags & FL_DUCKING) && (m_fMinSpeed != pev->maxspeed))
            m_fMoveSpeed = m_fMinSpeed;
      }

      if (!(m_iAimFlags & AIM_ENEMY) && ((m_fHeardSoundTime + 13.0 >= WorldTime ()) || (m_iStates & (STATE_HEARINGENEMY | STATE_SUSPECTENEMY))) && (GetNearbyEnemiesNearPosition (pev->origin, 1024) >= 1) && !(m_uiCurrTravelFlags & C_FL_JUMP) && !(pev->button & IN_DUCK) && !(pev->flags & FL_DUCKING) && !g_rgpcvBotCVar[CVAR_KNIFEMODE]->GetBool () && !g_bBombPlanted)
         m_fMoveSpeed = GetWalkSpeed ();

      // bot hasn't seen anything in a long time and is asking his teammates to report in
      if ((m_fSeeEnemyTime != 0.0) && (m_fSeeEnemyTime + RandomFloat (30.0, 80.0) < WorldTime ()) && (RandomLong (0, 100) < 70) && (g_fTimeRoundStart + 20.0 < WorldTime ()))
         RadioMessage (RADIO_REPORTTEAM);

      break;

   // bot sprays messy logos all over the place...
   case TASK_SPRAYLOGO:
      m_iAimFlags |= AIM_ENTITY;

      // bot didn't spray this round?
      if ((m_fTimeLogoSpray <= WorldTime ()) && (m_pTasks->fTime > WorldTime ()))
      {
         MakeVectors (pev->v_angle);
         Vector vSprayPos = EyePosition () + (g_pGlobals->v_forward * 128);

         TraceLine (EyePosition (), vSprayPos, true, GetEntity (), &tr);

         // no wall in front?
         if (tr.flFraction >= 1.0)
            vSprayPos.z -= 128.0;

         m_vEntity = vSprayPos;

         if (m_pTasks->fTime - 0.5 < WorldTime ())
         {
            // emit spraycan sound
            EMIT_SOUND_DYN2 (GetEntity (), CHAN_VOICE, "player/sprayer.wav", 1.0, ATTN_NORM, 0, 100);
            TraceLine (EyePosition (), EyePosition () + g_pGlobals->v_forward * 128, true, GetEntity (), &tr);
            
            // paint the actual logo decal
            DecalTrace (pev, &tr, const_cast <char *> (g_arSprayNames[m_iSprayLogo].GetString ()));
            m_fTimeLogoSpray = WorldTime () + RandomFloat (30.0, 45.0);
         }
      }
      else
         TaskComplete ();

      m_bMoveToGoal = false;
      m_bCheckTerrain = false;

      m_fWptTimeset = WorldTime ();      
      m_fMoveSpeed = 0;
      m_fSideMoveSpeed = 0.0;

      break;

   // hunt down enemy
   case TASK_ENEMYHUNT:
      m_iAimFlags |= AIM_DEST;

      if ((m_iReloadState == RELOAD_NONE) && (GetAmmo () != 0))
         m_iReloadState = RELOAD_PRIMARY;

      // if we've got new enemy...
      if (!FNullEnt (m_pentEnemy) || FNullEnt (m_pentLastEnemy))
      {
         // forget about it...
         TaskComplete ();
         m_iPrevGoalIndex = -1;
      }
      else if (DoWaypointNav ()) // reached last enemy pos?
      {
         // forget about it...
         TaskComplete ();

         m_iPrevGoalIndex = -1;
         m_vLastEnemyOrigin = NULLVEC;
      }
      else if (m_pWaypointNodes == NULL) // do we need to calculate a new path?
      {
         DeleteSearchNodes ();

         // is there a remembered index?
         if ((CurrentTask ()->iData != -1) && (CurrentTask ()->iData < g_iNumWaypoints))
            iDestIndex = m_pTasks->iData;
         else // no. we need to find a new one
            iDestIndex = g_pWaypoint->FindNearest (m_vLastEnemyOrigin);

         // remember index
         m_iPrevGoalIndex = iDestIndex;
         m_pTasks->iData = iDestIndex;

         if (iDestIndex != m_iCurrWptIndex)
            FindPath (m_iCurrWptIndex, iDestIndex);
      }

      // bots skill higher than 60?
      if (m_iSkill > 60)
      {
         // then make him move slow if near enemy 
         if (!(m_uiCurrTravelFlags & C_FL_JUMP))
         {
            if (m_iCurrWptIndex != -1)
            {
               if ((g_pWaypoint->GetPath (m_iCurrWptIndex)->fRadius < 32) && !IsOnLadder () && !IsInWater () && (m_fSeeEnemyTime + 4.0 > WorldTime ()) && (m_iSkill < 80))
                  pev->button |= IN_DUCK;
            }

            if (((m_vLastEnemyOrigin - pev->origin).Length () < 512.0) && !(pev->flags & FL_DUCKING))
               m_fMoveSpeed = GetWalkSpeed ();
         }
      }
      break;

   // bot seeks cover from enemy
   case TASK_SEEKCOVER:
      m_iAimFlags |= AIM_DEST;

      if (FNullEnt (m_pentLastEnemy) || !IsAlive (m_pentLastEnemy))
      {
         TaskComplete ();
         m_iPrevGoalIndex = -1;
      }
      else if (DoWaypointNav ()) // reached final cover waypoint?
      {
         // yep. activate hide behaviour
         TaskComplete ();

         m_iPrevGoalIndex = -1;
         m_byPathType = 1;

         // start hide task
         StartTask (TASK_HIDE, TASKPRI_HIDE, -1, WorldTime () + RandomFloat (5.0, 15.0), false);
         vDestination = m_vLastEnemyOrigin;

         // get a valid look direction
         GetCampDirection (&vDestination);

         m_iAimFlags |= AIM_CAMP;
         m_vCamp = vDestination;
         m_iCampDirection = 0;

         // chosen waypoint is a camp waypoint? 
         if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_CAMP)
         {
            // use the existing camp wpt prefs
            if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_CROUCH)
               m_iCampButtons = IN_DUCK;
            else
               m_iCampButtons = 0;
         }
         else
         {
            // choose a crouch or stand pos
            if (g_pWaypoint->GetPath (m_iCurrWptIndex)->kVis.uCrouch <= g_pWaypoint->GetPath (m_iCurrWptIndex)->kVis.uStand)
               m_iCampButtons = IN_DUCK;
            else
               m_iCampButtons = 0;

            // enter look direction from previously calculated positions
            g_pWaypoint->GetPath (m_iCurrWptIndex)->fCampStartX = vDestination.x;
            g_pWaypoint->GetPath (m_iCurrWptIndex)->fCampStartY = vDestination.y;

            g_pWaypoint->GetPath (m_iCurrWptIndex)->fCampStartX = vDestination.x;
            g_pWaypoint->GetPath (m_iCurrWptIndex)->fCampEndY = vDestination.y;
         }

         if ((m_iReloadState == RELOAD_NONE) && (GetAmmoInClip () < 8) && (GetAmmo () != 0))
            m_iReloadState = RELOAD_PRIMARY;

         m_fMoveSpeed = 0;
         m_fSideMoveSpeed = 0;

         m_bMoveToGoal = false;
         m_bCheckTerrain = false;
      }
      else if (!GoalIsValid ()) // we didn't choose a cover waypoint yet or lost it due to an attack?
      {
         DeleteSearchNodes ();

         if (CurrentTask ()->iData != -1)
            iDestIndex = m_pTasks->iData;
         else
         {
            iDestIndex = FindCoverWaypoint (1024);

            if (iDestIndex == -1)
               iDestIndex = g_pWaypoint->FindFarest (pev->origin, 500);
         
         }

         m_iCampDirection = 0;
         m_iPrevGoalIndex = iDestIndex;
         m_pTasks->iData = iDestIndex;

         if (iDestIndex != m_iCurrWptIndex)
            FindPath (m_iCurrWptIndex, iDestIndex, 1);
      }
      break;

   // plain attacking
   case TASK_ATTACK:
      m_bMoveToGoal = false;
      m_bCheckTerrain = false;

      if (!FNullEnt (m_pentEnemy))
         CombatFight ();
      else
      {
         TaskComplete ();
         m_vDestOrigin = m_vLastEnemyOrigin;
      }
      m_fWptTimeset = WorldTime ();     
      break;

   // Bot is pausing
   case TASK_PAUSE:
      m_bMoveToGoal = false;
      m_bCheckTerrain = false;

      m_fWptTimeset = WorldTime ();      
      m_fMoveSpeed = 0.0;
      m_fSideMoveSpeed = 0.0;

      m_iAimFlags |= AIM_DEST;

      // is bot blinded and above average skill?
      if ((m_fViewDistance < 500.0) && (m_iSkill > 60))
      {
         // go mad!
         m_fMoveSpeed = -fabsf ((m_fViewDistance - 500.0) / 2.0);

         if (m_fMoveSpeed < -pev->maxspeed)
            m_fMoveSpeed = -pev->maxspeed;

         Vector vDirection;
         MakeVectors (pev->v_angle);
         m_vCamp = GetGunPosition () + (g_pGlobals->v_forward * 500);

         m_iAimFlags |= AIM_OVERRIDE;
         m_bWantsToFire = true;
      }
      else
         pev->button |= m_iCampButtons;

      // stop camping if time over or gets hurt by something else than bullets
      if ((m_pTasks->fTime < WorldTime ()) || (m_iLastDamageType > 0))
         TaskComplete ();
      break;

   // blinded (flashbanged) behaviour
   case TASK_BLINDED:
      m_bMoveToGoal = false;
      m_bCheckTerrain = false;
      m_fWptTimeset = WorldTime ();

      switch (m_ucPersonality)
      {
      case PERSONALITY_NORMAL:
         if ((m_iSkill > 60) && (m_vLastEnemyOrigin != NULLVEC))
            bShootLastPosition = true;
         break;

      case PERSONALITY_AGRESSIVE:
         if (m_vLastEnemyOrigin != NULLVEC)
            bShootLastPosition = true;
         else
            bMadShoot = true;
         break;
      }

      if (bShootLastPosition) // If Bot remembers last Enemy Position 
      {
         // Face it and shoot
         m_vLookAt = m_vLastEnemyOrigin;
         m_bWantsToFire = true;
      }
      else if (bMadShoot) // If Bot is mad
      {
         // Just shoot in forward direction
         MakeVectors (pev->v_angle);

         m_vLookAt = GetGunPosition () + (g_pGlobals->v_forward * 500);
         m_bWantsToFire = true;
      }

      m_fMoveSpeed = m_fBlindMoveSpeed;
      m_fSideMoveSpeed = m_fBlindSidemoveSpeed;

      if (m_fBlindTime < WorldTime ())
         TaskComplete ();
      break;

   // Camping Behaviour
   case TASK_CAMP:
      m_iAimFlags |= AIM_CAMP;
      m_bCheckTerrain = false;
      m_bMoveToGoal = false;

      // half the reaction time if camping because you're more aware of enemies if camping
      m_fIdealReactionTime = (RandomFloat (g_rgkSkillTab[m_iSkill / 20].fMinSurpriseTime, g_rgkSkillTab[m_iSkill / 20].fMaxSurpriseTime)) / 2;
      m_fWptTimeset = WorldTime ();      
      
      m_fMoveSpeed = 0;
      m_fSideMoveSpeed = 0.0;

      GetValidWaypoint ();

      if (m_fNextCampDirTime < WorldTime ()) 
      {
         m_fNextCampDirTime = WorldTime () + RandomFloat (2.0, 5.0);
         
         if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_CAMP)
         {
            vDestination.z = 0;

            // Switch from 1 direction to the other
            if (m_iCampDirection < 1)
            {
               vDestination.x = g_pWaypoint->GetPath (m_iCurrWptIndex)->fCampStartX;
               vDestination.y = g_pWaypoint->GetPath (m_iCurrWptIndex)->fCampStartY;
               m_iCampDirection ^= 1;
            }
            else
            {
               vDestination.x = g_pWaypoint->GetPath (m_iCurrWptIndex)->fCampEndX;
               vDestination.y = g_pWaypoint->GetPath (m_iCurrWptIndex)->fCampEndY;
               m_iCampDirection ^= 1;
            }

            // find a visible waypoint to this direction...
            // i know this is ugly hack, but i just don't want to break compatiability :)
            int iNumFoundWP = 0;
            int rgiFoundWP[3];
            int rgiDistance[3];

            Vector2D a = (vDestination - pev->origin).Make2D ().Normalize ();

            for (i = 0; i < g_iNumWaypoints; i++)
            {
               // skip invisible waypoints or current waypoint
               if (!g_pWaypoint->IsVisible (m_iCurrWptIndex, i) || (i == m_iCurrWptIndex))
                  continue;

               Vector2D b = (g_pWaypoint->GetPath (i)->vOrigin - pev->origin).Make2D ().Normalize ();

               if (DotProduct (a, b) > 0.9)
               {
                  int iDist = (pev->origin - g_pWaypoint->GetPath (i)->vOrigin).Length ();

                  if (iNumFoundWP >= 3)
                  {
                     for (int j = 0; j < 3; j++)
                     {
                        if (iDist > rgiDistance[j])
                        {
                           rgiDistance[j] = iDist;
                           rgiFoundWP[j] = i;

                           break;
                        }
                     }
                  }
                  else
                  {
                     rgiFoundWP[iNumFoundWP] = i;
                     rgiDistance[iNumFoundWP] = iDist;

                     iNumFoundWP++;
                  }
               }
            }

            if (--iNumFoundWP >= 0)
               m_vCamp = g_pWaypoint->GetPath (rgiFoundWP[RandomLong (0, iNumFoundWP)])->vOrigin;
            else
               m_vCamp = g_pWaypoint->GetPath (GetAimingWaypoint ())->vOrigin;
         }
         else
            m_vCamp = g_pWaypoint->GetPath (GetAimingWaypoint ())->vOrigin;
      }
      // Press remembered crouch Button
      pev->button |= m_iCampButtons;

      // Stop camping if time over or gets hurt by something else than bullets
      if ((m_pTasks->fTime < WorldTime ()) || (m_iLastDamageType > 0))
         TaskComplete ();
      break;

   // hiding behaviour
   case TASK_HIDE:
      m_iAimFlags |= AIM_CAMP;
      m_bCheckTerrain = false;
      m_bMoveToGoal = false;

      // half the reaction time if camping
      m_fIdealReactionTime = (RandomFloat (g_rgkSkillTab[m_iSkill / 20].fMinSurpriseTime, g_rgkSkillTab[m_iSkill / 20].fMaxSurpriseTime)) / 2;

      m_fWptTimeset = WorldTime ();      
      m_fMoveSpeed = 0;
      m_fSideMoveSpeed = 0.0;

      GetValidWaypoint ();

      if (HasShield () && !m_bIsReloading)
      {
         if (!IsShieldDrawn ())
            pev->button |= IN_ATTACK2; // draw the shield!
         else
            pev->button |= IN_DUCK; // duck under if the shield is already drawn
      }

      // if we see an enemy and aren't at a good camping point leave the spot
      if ((m_iStates & STATE_SEEINGENEMY) || m_bInBombZone)
      {
         if (!(g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_CAMP))
         {
            TaskComplete ();

            m_iCampButtons = 0;
            m_iPrevGoalIndex = -1;

            if (!FNullEnt (m_pentEnemy))
               CombatFight ();

            break;
         }
      }
      else if (m_vLastEnemyOrigin == NULLVEC) // If we don't have an enemy we're also free to leave
      {
         TaskComplete ();

         m_iCampButtons = 0;
         m_iPrevGoalIndex = -1;

         if (CurrentTask ()->iTask == TASK_HIDE)
            TaskComplete ();
         break;
      }

      pev->button |= m_iCampButtons;
      m_fWptTimeset = WorldTime ();

      // stop camping if time over or gets hurt by something else than bullets
      if ((CurrentTask ()->fTime < WorldTime ()) || (m_iLastDamageType > 0))
         TaskComplete ();

      break;

   // moves to a position specified in m_vPosition has a higher priority than task_normal
   case TASK_MOVETOPOSITION:
      m_iAimFlags |= AIM_DEST;

      if (IsShieldDrawn ())
         pev->button |= IN_ATTACK2;

      if ((m_iReloadState == RELOAD_NONE) && (GetAmmo () != 0))
         m_iReloadState = RELOAD_PRIMARY;

      if (DoWaypointNav ()) // reached destination?
      {
         TaskComplete (); // we're done

         m_iPrevGoalIndex = -1;
         m_vPosition = NULLVEC;
      }
      else if (!GoalIsValid ()) // didn't choose goal waypoint yet?
      {
         DeleteSearchNodes ();

         if ((CurrentTask ()->iData != -1) && (CurrentTask ()->iData < g_iNumWaypoints))
            iDestIndex = m_pTasks->iData;
         else
            iDestIndex = g_pWaypoint->FindNearest (m_vPosition);

         if ((iDestIndex >= 0) && (iDestIndex < g_iNumWaypoints))
         {
            m_iPrevGoalIndex = iDestIndex;
            m_pTasks->iData = iDestIndex;

            FindPath (m_iCurrWptIndex, iDestIndex);
         }
         else
            TaskComplete ();
      }
      break;

   // planting the bomb right now
   case TASK_PLANTBOMB:
      m_iAimFlags |= AIM_DEST;

      if (pev->weapons & (1 << WEAPON_C4)) // we're still got the C4?
      {
         SelectWeaponByName ("weapon_c4");

         if (IsAlive (m_pentEnemy) || !m_bInBombZone)
            TaskComplete ();
         else
         {
            m_bMoveToGoal = false;
            m_bCheckTerrain = false;
            m_fWptTimeset = WorldTime ();

            if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_CROUCH)
               pev->button |= (IN_ATTACK | IN_DUCK);
            else
               pev->button |= IN_ATTACK;

            m_fMoveSpeed = 0;
            m_fSideMoveSpeed = 0;
         }
      }
      else // done with planting
      {
         TaskComplete ();

         // tell teammates to move over here...
         if (GetNearbyFriendsNearPosition (pev->origin, 1200.0) != 0)
            RadioMessage (RADIO_NEEDBACKUP);

         DeleteSearchNodes ();
         int iIndex = FindDefendWaypoint (pev->origin);

         // push camp task on to stack
         StartTask (TASK_CAMP, TASKPRI_CAMP, -1, WorldTime () + ((g_rgpcvBotCVar[CVAR_C4TIMER]->GetFloat () / 2) + (g_rgpcvBotCVar[CVAR_C4TIMER]->GetFloat () / 4)), true);
         // push move command
         StartTask (TASK_MOVETOPOSITION, TASKPRI_MOVETOPOSITION, iIndex, WorldTime () + ((g_rgpcvBotCVar[CVAR_C4TIMER]->GetFloat () / 2) + (g_rgpcvBotCVar[CVAR_C4TIMER]->GetFloat () / 4)), true);
         
         if (g_pWaypoint->GetPath (iIndex)->kVis.uCrouch <= g_pWaypoint->GetPath (iIndex)->kVis.uStand)
            m_iCampButtons |= IN_DUCK;
         else
            m_iCampButtons &= ~IN_DUCK;
      }
      break;

   // Bomb defusing Behaviour
   case TASK_DEFUSEBOMB:
      m_iAimFlags |= AIM_ENTITY;
      m_bMoveToGoal = false;
      m_bCheckTerrain = false;

      m_fWptTimeset = WorldTime ();

      if (!FNullEnt (m_pentEnemy))
         TaskComplete ();

      if (!FNullEnt (m_pentPickupItem)) // bomb still there?
      {
         // get face position
         m_vEntity = m_pentPickupItem->v.origin;

         if (m_iSkill >= 80)
         {
            pev->button |= IN_USE;

            // if we have enemies around duck
            if (GetNearbyEnemiesNearPosition (pev->origin, 9999.0) != 0)
               pev->button |= IN_DUCK;
         }
         else
            pev->button |= (IN_USE | IN_DUCK);
      }
      else
      {
         if (RandomLong (0, 100) < 80)
         {
            if ((g_fTimeBombPlanted + g_rgpcvBotCVar[CVAR_C4TIMER]->GetFloat ()) - WorldTime () < 3.0)
               ChatterMessage (VOICE_BARELY_DEFUSED);
            else
               RadioMessage (RADIO_SECTORCLEAR);
         }
         TaskComplete ();
      }
      m_fMoveSpeed = 0.0;
      m_fSideMoveSpeed = 0.0;

      break;

   // follow user behaviour
   case TASK_FOLLOWUSER:
      if (FNullEnt (m_pentTargetEnt) || !IsAlive (m_pentTargetEnt))
      {
         m_pentTargetEnt = NULL;
         TaskComplete ();
         break;
      }

      if ((m_iReloadState == RELOAD_NONE) && (GetAmmo () != 0))
         m_iReloadState = RELOAD_PRIMARY;

      if ((m_pentTargetEnt->v.origin - pev->origin).Length () > 150)
         m_fFollowWaitTime = 0.0;
      else
      {
         m_fMoveSpeed = 0.0; // don't move

         if (m_fFollowWaitTime == 0.0)
            m_fFollowWaitTime = WorldTime ();
         else
         {
            if (m_fFollowWaitTime + 3.0 < WorldTime ())
            {
               // stop following if we have been waiting too long
               m_pentTargetEnt = NULL;
               RadioMessage (RADIO_YOUTAKEPOINT);
               TaskComplete ();

               break;
            }
         }
      }
      m_iAimFlags |= AIM_DEST;

      if (IsShieldDrawn ())
         pev->button |= IN_ATTACK2;

      if (DoWaypointNav ()) // Reached destination?
         CurrentTask ()->iData = -1;

      if (!GoalIsValid ()) // Didn't choose Goal Waypoint yet?
      {
         DeleteSearchNodes ();

         iDestIndex = g_pWaypoint->FindNearest (m_pentTargetEnt->v.origin);

         if ((iDestIndex >= 0) && (iDestIndex < g_iNumWaypoints))
         {
            m_iPrevGoalIndex = iDestIndex;
            m_pTasks->iData = iDestIndex;

            // always take the shortest path
            FindShortestPath (m_iCurrWptIndex, iDestIndex);
         }
         else
         {
            m_pentTargetEnt = NULL;
            TaskComplete ();
         }
      }
      break;

   // HE grenade throw behaviour
   case TASK_THROWHEGRENADE:
      m_iAimFlags |= AIM_GRENADE;
      vDestination = m_vThrow;

      if (!(m_iStates & STATE_SEEINGENEMY))
      {
         m_fMoveSpeed = 0.0;
         m_fSideMoveSpeed = 0.0;

         m_bMoveToGoal = false;
      }
      else if (!(m_iStates & STATE_SUSPECTENEMY) && !FNullEnt (m_pentEnemy))
      {
         // find feet
         vDestination = m_pentEnemy->v.origin;

         vSource = m_pentEnemy->v.velocity;
         vSource.z = 0.0;

         vDestination = vDestination + (vSource * 0.3);
      }

      m_bUsingGrenade = true;
      m_bCheckTerrain = false;

      if (LengthSquared (pev->origin - vDestination) < (400 * 400))
      {
         // heck, I don't wanna blow up myself
         m_fGrenadeCheckTime = WorldTime () + GRENADE_TIMER;

         SelectBestWeapon ();
         TaskComplete ();

         break;
      }

      m_vGrenade = CheckThrow (GetGunPosition (), vDestination, 400);

      if (LengthSquared (m_vGrenade) < 100)
         m_vGrenade = CheckToss (GetGunPosition (), vDestination);

      if (LengthSquared (m_vGrenade) <= 100)
      {
         m_fGrenadeCheckTime = WorldTime () + GRENADE_TIMER;
         m_vGrenade = m_vLookAt;

         SelectBestWeapon ();
         TaskComplete ();
      }
      else
      {
         edict_t *pent = NULL;

         while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "grenade")))
         {
            if ((pent->v.owner == GetEntity ()) && FStrEq (STRING (pent->v.model) + 9, "hegrenade.mdl"))
            {
              // set the correct velocity for the grenade
               if (LengthSquared (m_vGrenade) > 100)
                  pent->v.velocity = m_vGrenade;

               m_fGrenadeCheckTime = WorldTime () + GRENADE_TIMER;

               SelectBestWeapon ();
               TaskComplete ();
               break;
            }
         }

         if (FNullEnt (pent))
         {
            if (m_iCurrentWeapon != WEAPON_HEGRENADE)
            {
               if (pev->weapons & (1 << WEAPON_HEGRENADE))
                  SelectWeaponByName ("weapon_hegrenade");
            }
            else if (!(pev->oldbuttons & IN_ATTACK))
               pev->button |= IN_ATTACK;
         }
      }
      pev->button |= m_iCampButtons;
      break;

   // flashbang throw behavior (basically the same code like for HE's)
   case TASK_THROWFLASHBANG:
      m_iAimFlags |= AIM_GRENADE;
      vDestination = m_vThrow;

      m_bUsingGrenade = true;
      m_bCheckTerrain = false;

      if (LengthSquared (pev->origin - vDestination) < (400 * 400))
      {
         // heck, I don't wanna blow up myself
         m_fGrenadeCheckTime = WorldTime () + GRENADE_TIMER;

         SelectBestWeapon ();
         TaskComplete ();

         break;
      }

      m_vGrenade = CheckThrow (GetGunPosition (), vDestination, 400);

      if (LengthSquared (m_vGrenade) < 100)
         m_vGrenade = CheckToss (pev->origin, vDestination);

      if (LengthSquared (m_vGrenade) <= 100)
      {
         m_fGrenadeCheckTime = WorldTime () + GRENADE_TIMER;
         m_vGrenade = m_vLookAt;

         SelectBestWeapon ();
         TaskComplete ();
      }
      else
      {
         edict_t *pent = NULL;
         while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "grenade")))
         {
            if ((pent->v.owner == GetEntity ()) && FStrEq (STRING (pent->v.model) + 9, "flashbang.mdl"))
            {
               // set the correct velocity for the grenade
               if (LengthSquared (m_vGrenade) > 100)
                  pent->v.velocity = m_vGrenade;

               m_fGrenadeCheckTime = WorldTime () + GRENADE_TIMER;

               SelectBestWeapon ();
               TaskComplete ();
               break;
            }
         }

         if (FNullEnt (pent))
         {
            if (m_iCurrentWeapon != WEAPON_FLASHBANG)
            {
               if (pev->weapons & (1 << WEAPON_FLASHBANG))
                  SelectWeaponByName ("weapon_flashbang");
            }
            else if (!(pev->oldbuttons & IN_ATTACK))
               pev->button |= IN_ATTACK;
         }
      }
      pev->button |= m_iCampButtons;
      break;

   // smoke grenade throw behavior
   // a bit different to the others because it mostly tries to throw the sg on the ground
   case TASK_THROWSMOKEGRENADE:
      m_iAimFlags |= AIM_GRENADE;

      if (!(m_iStates & STATE_SEEINGENEMY))
      {
         m_fMoveSpeed = 0.0;
         m_fSideMoveSpeed = 0.0;
         m_bMoveToGoal = false;
      }

      m_bCheckTerrain = false;
      m_bUsingGrenade = true;

      vSource = m_vLastEnemyOrigin;
      vSource = vSource - pev->velocity;

      // Predict where the enemy is in 0.5 secs 
      if (!FNullEnt (m_pentEnemy))
         vSource = vSource + m_pentEnemy->v.velocity * 0.5;

      m_vGrenade = (vSource - GetGunPosition ()).Normalize ();

      if ((m_pTasks->fTime < WorldTime ()) || !(pev->weapons & (1 << WEAPON_SMOKEGRENADE)))
      {
         m_iAimFlags &= ~AIM_GRENADE;
         m_iStates &= ~STATE_THROWSMOKEGREN;

         TaskComplete ();
         break;
      }

      if (m_iCurrentWeapon != WEAPON_SMOKEGRENADE)
      {
         if (pev->weapons & (1 << WEAPON_SMOKEGRENADE))
         {
            SelectWeaponByName ("weapon_smokegrenade");
            m_pTasks->fTime = WorldTime () + GRENADE_TIMER;
         }
         else
            m_pTasks->fTime = WorldTime () + 0.1;
      }
      else if (!(pev->oldbuttons & IN_ATTACK))
         pev->button |= IN_ATTACK; 
      break;

   // bot helps human player (or other bot) to get somewhere
   case TASK_DOUBLEJUMP:
      if (FNullEnt (m_pentDoubleJumpEdict) || !IsAlive (m_pentDoubleJumpEdict) || (m_iAimFlags & AIM_ENEMY) || ((m_iTravelStartIndex != -1) && (m_pTasks->fTime + (g_pWaypoint->GetTravelTime (pev->maxspeed, g_pWaypoint->GetPath (m_iTravelStartIndex)->vOrigin, m_vDoubleJumpOrigin) + 11.0) < WorldTime ())))
      {
         ResetDoubleJumpState ();
         break;
      }
      m_iAimFlags |= AIM_DEST;

      if (m_bJumpReady)
      {
         m_bMoveToGoal = false;
         m_bCheckTerrain = false;

         m_fWptTimeset = WorldTime ();
         m_fMoveSpeed = 0.0;
         m_fSideMoveSpeed = 0.0;

         if (m_fDuckForJump < WorldTime ())
            pev->button |= IN_DUCK;

         TraceResult tr;
         MakeVectors (NULLVEC);

         Vector vDest = EyePosition () + (g_pGlobals->v_forward + 999);
         vDest.z = 180.0;

         TraceLine (EyePosition (), vDest, false, true, GetEntity (), &tr); 

         if ((tr.flFraction < 1.0) && (tr.pHit == m_pentDoubleJumpEdict))
         {
            if (m_pentDoubleJumpEdict->v.button & IN_JUMP)
            {
               m_fDuckForJump = WorldTime () + RandomFloat (3.0, 5.0);
               pev->button |= IN_JUMP;

               m_pTasks->fTime = WorldTime ();
            }
         }
         break;
      }

      if (m_iCurrWptIndex == m_iPrevGoalIndex)
      {
         m_vWptOrigin = m_vDoubleJumpOrigin;
         m_vDestOrigin = m_vDoubleJumpOrigin;
      }

      if (DoWaypointNav ()) // reached destination?
         CurrentTask ()->iData = -1;

      if (!GoalIsValid ()) // didn't choose goal waypoint yet?
      {
         DeleteSearchNodes ();

         iDestIndex = g_pWaypoint->FindNearest (m_vDoubleJumpOrigin);

         if ((iDestIndex >= 0) && (iDestIndex < g_iNumWaypoints))
         {
            m_iPrevGoalIndex = iDestIndex;
            m_pTasks->iData = iDestIndex;
            m_iTravelStartIndex = m_iCurrWptIndex;

            // Always take the shortest path
            FindShortestPath (m_iCurrWptIndex, iDestIndex);

            if (m_iCurrWptIndex == iDestIndex)
               m_bJumpReady = true;
         }
         else
            ResetDoubleJumpState ();
      }
      break;

   // escape from bomb behaviour
   case TASK_ESCAPEFROMBOMB:
      m_iAimFlags |= AIM_DEST;

      if (!g_bBombPlanted)
         TaskComplete ();

      if (IsShieldDrawn ())
         pev->button |= IN_ATTACK2;

      if ((m_iCurrentWeapon != WEAPON_KNIFE) && (GetNearbyEnemiesNearPosition (pev->origin, 2048) == 0) && !m_bIsReloading)
         SelectWeaponByName ("weapon_knife");

      if (DoWaypointNav ()) // reached destination?
      {
         TaskComplete (); // we're done

         // press duck button if we still have some enemies
         if (GetNearbyEnemiesNearPosition (pev->origin, 2048.0))
            m_iCampButtons = IN_DUCK;

         // we're reached destination point so just sit down and camp
         StartTask (TASK_CAMP, TASKPRI_CAMP, -1, WorldTime () + 10.0, true);
      }
      else if (!GoalIsValid ()) // didn't choose goal waypoint yet?
      {
         DeleteSearchNodes ();
         iDestIndex = g_pWaypoint->FindFarest (g_pWaypoint->GetBombPosition (), RandomFloat (1024.0, 2048.0));

         if ((iDestIndex >= 0) && (iDestIndex < g_iNumWaypoints))
         {
            m_iPrevGoalIndex = iDestIndex;
            m_pTasks->iData = iDestIndex;

            FindShortestPath (m_iCurrWptIndex, iDestIndex); // use shortes path, when escaping from bomb
         }
         else
            TaskComplete ();
      }
      break;

   // shooting breakables in the way action
   case TASK_SHOOTBREAKABLE:
      m_iAimFlags |= AIM_OVERRIDE;

      // Breakable destroyed?
      if (FNullEnt (FindBreakable ()))
      {
         TaskComplete ();
         break;
      }
      pev->button |= m_iCampButtons;

      m_bCheckTerrain = false;
      m_bMoveToGoal = false;
      m_fWptTimeset = WorldTime ();

      vSource = m_vBreakable;
      m_vCamp = vSource;

      // is bot facing the breakable?
      if (GetShootingConeDeviation (GetEntity (), &vSource) >= 0.90)
      {
         m_fMoveSpeed = 0.0;
         m_fSideMoveSpeed = 0.0;

         if (m_iCurrentWeapon == WEAPON_KNIFE)
            SelectBestWeapon ();

         m_bWantsToFire = true;
      }
      else
      {
         m_bCheckTerrain = true;
         m_bMoveToGoal = true;
      }
      break;

   // picking up items and stuff behaviour
   case TASK_PICKUPITEM:
      if (FNullEnt (m_pentPickupItem))
      {
         m_pentPickupItem = NULL;
         TaskComplete ();

         break;
      }

      vDestination = GetEntityOrigin (m_pentPickupItem);
      m_vDestOrigin = vDestination;
      m_vEntity = vDestination;

      // find the distance to the item
      float fItemDistance = (vDestination - pev->origin).Length ();

      switch (m_iPickupType)
      {
      case PICKUP_WEAPON:
         m_iAimFlags |= AIM_DEST;

         // near to weapon?
         if (fItemDistance < 50)
         {
            for (i = 0; i < 7; i++)
            {
               if (strcmp (g_rgkWeaponSelect[i].szModelName, STRING (m_pentPickupItem->v.model) + 9) == 0)
                  break;
            }

            if (i < 7)
            {
               // secondary weapon. i.e., pistol
               int iWeaponNum = 0;

               for (i = 0; i < 7; i++)
               {
                  if (pev->weapons & (1 << g_rgkWeaponSelect[i].iId))
                     iWeaponNum = i;
               }

               if (iWeaponNum > 0)
               {
                  SelectWeaponbyNumber (iWeaponNum);
                  FakeClientCommand (GetEntity (), "drop");

                  if (HasShield ()) // If we have the shield...
                     FakeClientCommand (GetEntity (), "drop"); // discard both shield and pistol
               }

               // if bot is in buy zone, try to buy ammo for this weapon...
               if (m_bInBuyZone && ((g_fTimeRoundStart + RandomFloat (10, 20) + g_rgpcvBotCVar[CVAR_BUYTIME]->GetFloat ()) < WorldTime ()) && !g_bBombPlanted)
               {
                  m_bBuyingFinished = false;
                  m_iBuyCount = 0;

                  PushMessageQueue (MESSAGE_BUY);
                  m_fNextBuyTime = WorldTime ();
               }
            }
            else
            {
               // primary weapon
               int iWeaponNum = GetHighestWeapon ();

               if ((iWeaponNum > 6) || HasShield ())
               {
                  SelectWeaponbyNumber (iWeaponNum);
                  FakeClientCommand (GetEntity (), "drop");
               }

               // if bot is in buy zone, try to buy ammo for this weapon...
               if (m_bInBuyZone && ((g_fTimeRoundStart + RandomFloat (10, 20) + g_rgpcvBotCVar[CVAR_BUYTIME]->GetFloat ()) < WorldTime ()) && !g_bBombPlanted)
               {
                  m_bBuyingFinished = false;
                  m_iBuyCount = 0;

                  PushMessageQueue (MESSAGE_BUY);
                  m_fNextBuyTime = WorldTime ();
               }
            }
            CheckSilencer (); // check the silencer
         }
         break;

      case PICKUP_SHIELD:
         m_iAimFlags |= AIM_DEST;

         if (HasShield ())
         {
            m_pentPickupItem = NULL;
            break;
         }
         else if (fItemDistance < 50) // near to shield?
         {
            // get current best weapon to check if it's a primary in need to be dropped
            int iWeaponNum = GetHighestWeapon ();

            if (iWeaponNum > 6)
            {
               SelectWeaponbyNumber (iWeaponNum);
               FakeClientCommand (GetEntity (), "drop");
            }
         }
         break;

      case PICKUP_HOSTAGE:
         m_iAimFlags |= AIM_ENTITY;
         vSource = EyePosition ();

         if (!IsAlive (m_pentPickupItem))
         {
            // don't pickup dead hostages
            m_pentPickupItem = NULL;
            TaskComplete ();

            break;
         }

         if (fItemDistance < 50)
         {
            float fAngleToEntity = InFieldOfView (vDestination - vSource);

            if (fAngleToEntity <= 10) // bot faces hostage?
            {
               // Use game DLL function to make sure the hostage is correctly 'used'
               MDLL_Use (m_pentPickupItem, GetEntity ());

               if (RandomLong (0, 100) < 80)
                  ChatterMessage (VOICE_USINGHOSTAGE);

               for (i = 0; i < MAX_HOSTAGES; i++)
               {
                  if (FNullEnt (m_rgpHostages[i])) // store pointer to hostage so other bots don't steal from this one or bot tries to reuse it
                  {
                     m_rgpHostages[i] = m_pentPickupItem;
                     m_pentPickupItem = NULL;

                     break;
                  }
               }
            }
            m_fMoveSpeed = 0.0; // don't move while using a hostage
            m_fNoCollTime = WorldTime () + 0.1; // also don't consider being stuck
         }
         break;

      case PICKUP_PLANTED_C4:
         m_iAimFlags |= AIM_ENTITY;

         if ((iTeam == TEAM_CT) && (fItemDistance < 55))
         {
            ChatterMessage (VOICE_DEFUSING_C4);

            // notify team of defusing
            if (GetNearbyFriendsNearPosition (pev->origin, 650) < 2)
               RadioMessage (RADIO_NEEDBACKUP);

            m_bMoveToGoal = false;
            m_bCheckTerrain = false;

            m_fMoveSpeed = 0;
            m_fSideMoveSpeed = 0;

            StartTask (TASK_DEFUSEBOMB, TASKPRI_DEFUSEBOMB, -1, 0.0, false);
         }
         break;

      case PICKUP_DEFUSEKIT:
         m_iAimFlags |= AIM_DEST;

         if (m_bHasDefuser)
         {
            m_pentPickupItem = NULL;
            m_iPickupType = PICKUP_NONE;
         }
         break;

      case PICKUP_BUTTON:
         m_iAimFlags |= AIM_ENTITY;

         if (FNullEnt (m_pentPickupItem)) // it's safer...
         {
            TaskComplete ();
            m_iPickupType = PICKUP_NONE;

            break;
         }

         // find angles from bot origin to entity...
         vSource = EyePosition ();
         float fAngleToEntity = InFieldOfView (vDestination - vSource);

         if (fItemDistance < 50) // Near to the Button?
         {
            m_fMoveSpeed = 0.0;
            m_fSideMoveSpeed = 0.0;
            m_bMoveToGoal = false;
            m_bCheckTerrain = false;

            if (fAngleToEntity <= 10) // Facing it directly?
            {
               MDLL_Use (m_pentPickupItem, GetEntity ());

               m_pentPickupItem = NULL;
               m_iPickupType = PICKUP_NONE;

               TaskComplete ();
            }
         }
         break;
      }
      break;
   }
}

void CBot::BotAI (void)
{
   // this function gets called each frame and is the core of all bot ai. from here all other subroutines are called

   float fMovedDistance; // length of different vector (distance bot moved)
   TraceResult tr;

   int iTeam = GetTeam (GetEntity ());

   // switch to knife if time to do this
   if (m_bCheckKnifeSwitch && m_bBuyingFinished && (m_fSpawnTime + RandomFloat (4.0, 6.5) < WorldTime ()))
   {
      if ((RandomLong (1, 100) < 40) && g_rgpcvBotCVar[CVAR_SPRAY]->GetBool ())
         StartTask (TASK_SPRAYLOGO, TASKPRI_SPRAYLOGO, -1, WorldTime () + 1.0, false);

      if ((m_iSkill > 75) && (RandomLong (0, 100) < (m_ucPersonality == PERSONALITY_AGRESSIVE ? 99 : 80)) && !m_bIsReloading && (g_iMapType & (MAP_CS | MAP_DE | MAP_ES | MAP_AS)))
        SelectWeaponByName ("weapon_knife");

      m_bCheckKnifeSwitch = false;
   }

   // check if we already switched weapon mode
   if (m_bCheckWeaponSwitch && m_bBuyingFinished && (m_fSpawnTime + RandomFloat (2.0, 3.5) < WorldTime ()))
   {
      if (HasShield () && IsShieldDrawn ())
        pev->button |= IN_ATTACK2;
      else
      {
         switch (m_iCurrentWeapon)
         {
         case WEAPON_M4A1:
         case WEAPON_USP:
            CheckSilencer ();
            break;

         case WEAPON_FAMAS:
         case WEAPON_GLOCK18:
            if (RandomLong (1, 100) < 50)
               ChangeBurstMode ();
            break;
         }
      }
  
      // select a leader bot for this team
      SelectLeaderEachTeam (iTeam);
      m_bCheckWeaponSwitch = false;
   }

   // warning: the following timers aren't frame independant so it varies on slower/faster computers

   // increase reaction time
   m_fActualReactionTime += 0.2;

   if (m_fActualReactionTime > m_fIdealReactionTime)
      m_fActualReactionTime = m_fIdealReactionTime;

   // bot could be blinded by flashbang or smoke, recover from it
   m_fViewDistance += 3.0;

   if (m_fViewDistance > m_fMaxViewDistance)
      m_fViewDistance = m_fMaxViewDistance;

   m_fMoveSpeed = pev->maxspeed;

   if (m_fPrevTime <= WorldTime ())
   {
      // see how far bot has moved since the previous position...
      Vector vDifferent = m_vPrevOrigin - pev->origin; // vector from previous to current location
      fMovedDistance = vDifferent.Length ();

      // save current position as previous
      m_vPrevOrigin = pev->origin;
      m_fPrevTime = WorldTime () + 0.2;
   }
   else
      fMovedDistance = 2.0;

   // if there's some radio message to respond, check it
   if (m_iRadioOrder != 0)
      CheckRadioCommands ();

   // do all sensing, calculate/filter all actions here 
   SetConditions ();

   // some stuff required by by chatter engine
   if ((m_iStates & STATE_SEEINGENEMY) && !FNullEnt (m_pentEnemy))
   {
      if ((RandomLong (0, 100) < 45) && (GetNearbyFriendsNearPosition (pev->origin, 512) == 0) && (m_pentEnemy->v.weapons & (1 << WEAPON_C4)))
         ChatterMessage (VOICE_SPOT_THE_BOMBER);

      if ((RandomLong (0, 100) < 45) && (GetTeam (GetEntity ()) == TEAM_TERRORIST) && (GetNearbyFriendsNearPosition (pev->origin, 512) == 0) && (*g_engfuncs.pfnInfoKeyValue (g_engfuncs.pfnGetInfoKeyBuffer (m_pentEnemy), "model") == 'v'))
         ChatterMessage (VOICE_VIP_SPOTTED);

      if ((RandomLong (0, 100) < 50) && (GetNearbyFriendsNearPosition (pev->origin, 450) == 0) && (GetTeam (m_pentEnemy) != GetTeam (GetEntity ())) && IsGroupOfEnemies (m_pentEnemy->v.origin, 2, 384))
         ChatterMessage (VOICE_SCAREDEMOTE);

      if ((RandomLong (0, 100)) < 40 && (GetNearbyFriendsNearPosition (pev->origin, 1024) == 0) && ((m_pentEnemy->v.weapons & (1 << WEAPON_AWP)) || (m_pentEnemy->v.weapons & (1 << WEAPON_SCOUT)) ||  (m_pentEnemy->v.weapons & (1 << WEAPON_G3SG1)) || (m_pentEnemy->v.weapons & (1 << WEAPON_SG550))))
         ChatterMessage (VOICE_SNIPER_WARNING);
   }
   Vector vSource, vDestination;

   m_bCheckTerrain = true;
   m_bMoveToGoal = true;
   m_bWantsToFire = false;

   AvoidGrenades (); // avoid flyings grenades
   m_bUsingGrenade = false;

   RunTask (); // execute current task
   ChooseAimDirection (); // choose aim direction
   FacePosition (); // and turn to choosen aim direction

   // the bots wants to fire at something?
   if (m_bWantsToFire && !m_bUsingGrenade && (m_fShootTime <= WorldTime ()))
   {
      // if bot didn't fire a bullet try again next frame
      if (!FireWeapon (m_vLookAt - EyePosition ()))
         m_fShootTime = WorldTime ();
   }

   // check for reloading
   if (m_fReloadCheckTime <= WorldTime ())
      CheckReload ();

   // set the reaction time (surprise momentum) different each frame according to skill
   m_fIdealReactionTime = RandomFloat (g_rgkSkillTab[m_iSkill / 20].fMinSurpriseTime, g_rgkSkillTab[m_iSkill / 20].fMaxSurpriseTime);

   // calculate 2 direction vectors, 1 without the up/down component
   Vector vDirectionOld = m_vDestOrigin - (pev->origin + pev->velocity * g_fTimeFrameInterval);
   
   Vector vDirectionNormal = vDirectionOld.Normalize ();
   Vector vDirection = vDirectionNormal;

   vDirectionNormal.z = 0.0;
   m_vMoveAngles = VecToAngles (vDirectionOld);

   ClampAngles (m_vMoveAngles);
   m_vMoveAngles.x *= -1.0; // invert for engine

   if (((m_iAimFlags & AIM_ENEMY) || (m_iStates & (STATE_SEEINGENEMY | STATE_SUSPECTENEMY)) || ((CurrentTask ()->iTask == TASK_SEEKCOVER) && (m_bIsReloading || m_bIsVIP))) && !g_rgpcvBotCVar[CVAR_KNIFEMODE]->GetBool () && (CurrentTask ()->iTask != TASK_CAMP) && !IsOnLadder ())
   {
      m_bMoveToGoal = false; // don't move to goal
      m_bCheckTerrain = false; // and dont' check for stuck

      m_fNoCollTime = WorldTime () + 1.0;
      m_fWptTimeset = WorldTime ();

      if (IsValidPlayer (m_pentEnemy))
         CombatFight ();
   }

   // check if we need to escape from bomb
   if ((g_iMapType & MAP_DE) && g_bBombPlanted && m_bNotKilled && (CurrentTask ()->iTask != TASK_ESCAPEFROMBOMB) && (CurrentTask ()->iTask != TASK_CAMP) && OutOfBombTimer ())
   {
      TaskComplete (); // complete current task

      // then start escape from bomb immideately
      StartTask (TASK_ESCAPEFROMBOMB, TASKPRI_ESCAPEFROMBOMB, -1, 0.0, true);
   }

   // allowed to move to a destination position?
   if (m_bMoveToGoal)
   {
      GetValidWaypoint ();

      // press duck button if we need to
      if ((g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_CROUCH) && !(g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_CAMP))
         pev->button |= IN_DUCK;

      m_fTimeWaypointMove = WorldTime ();

      if (IsOnLadder ()) // when climbing the ladder...
      {
         if (CurrentTask ()->iTask != TASK_SHOOTBREAKABLE)
            HandleLadder (); // handle ladder movement
         else
         {
            // don't move if we are shooting breakable on ladder
            pev->button |= (IN_FORWARD | IN_BACK | IN_MOVELEFT | IN_MOVERIGHT);

            m_fMoveSpeed = 0.0;
            m_fSideMoveSpeed = 0.0;
         }
      }
      else if (IsInWater ()) // special movement for swimming here
      {
         // check if we need to go forward or back press the correct buttons
         if (InFieldOfView (m_vDestOrigin - EyePosition ()) > 90)
            pev->button |= IN_BACK;
         else
            pev->button |= IN_FORWARD;

         if (m_vMoveAngles.x > 60.0)
            pev->button |= IN_DUCK;
         else if (m_vMoveAngles.x < -60.0)
            pev->button |= IN_JUMP;
      }
   }

   if (m_bCheckTerrain) // are we allowed to check blocking terrain (and react to it)?
   {
      edict_t *pent;
      bool bBotIsStuck = false;

      // Test if there's a shootable breakable in our way
      if (!FNullEnt (pent = FindBreakable ()))
      {
         m_pentShootBreakable = pent;
         m_iCampButtons = pev->button & IN_DUCK;

         StartTask (TASK_SHOOTBREAKABLE, TASKPRI_SHOOTBREAKABLE, -1, 0.0, false);
      }
      else
      {
         pent = NULL;
         edict_t *pentNearest = NULL;

         if (FindNearestPlayer (reinterpret_cast <void **> (&pentNearest), GetEntity (), pev->maxspeed, true, false, true, true) && IsInViewCone (pentNearest->v.origin)) // found somebody?
         {
            // bot found a visible teammate
            float fAngle = pentNearest->v.velocity.y - pev->velocity.y;
            float fViewAngle = pentNearest->v.v_angle.y - pev->v_angle.y;
            float fNearestDistance = (pentNearest->v.origin - pev->origin).Length2D ();

            AngleNormalize (fAngle);
            AngleNormalize (fViewAngle);

            // is that teammate near us OR coming in front of us and within a certain distance ?
            if ((fNearestDistance < 100) || ((fNearestDistance < 300) && (fabs (fAngle) > 160) && (fabs (fViewAngle) > 165)))
            {
               TraceHull (pev->origin, pev->origin + vDirection * 32.0, false, head_hull, GetEntity (), &tr);

               // if we're moving fullspeed + there's room forward or teammate is very close...
               if ((fNearestDistance < 70) || ((tr.flFraction == 1.0) && (pev->velocity.Length2D () > 50)))
               {
                  // is the player coming on the left?
                  if (fAngle > 0)
                     m_fSideMoveSpeed = pev->maxspeed;

                  // or is the player coming on the right?
                  else if (fAngle < 0)
                     m_fSideMoveSpeed = -pev->maxspeed;

                  if (fNearestDistance <= 56.0)
                     m_fMoveSpeed = -pev->maxspeed;

                  ResetCollideState ();

                  if (fMovedDistance < 2.0)
                     pev->button |= IN_DUCK;
               }
            }

            MakeVectors (m_vMoveAngles); // use our movement angles

            // try to predict where we should be next frame
            Vector vMoved = pev->origin + g_pGlobals->v_forward * m_fMoveSpeed * g_fTimeFrameInterval;
            vMoved = vMoved + g_pGlobals->v_right * m_fSideMoveSpeed * g_fTimeFrameInterval;
            vMoved = vMoved + pev->velocity * g_fTimeFrameInterval;

            float fMovedDistance = (pentNearest->v.origin - vMoved).Length2D ();
            float fNextFrameDistance = ((pentNearest->v.origin + pentNearest->v.velocity * g_fTimeFrameInterval) - pev->origin).Length2D ();

            // is player that near now or in future that we need to steer away?
            if ((fMovedDistance <= 48.0) || ((fNearestDistance <= 56.0) && (fNextFrameDistance < fNearestDistance)))
            {
               // to start strafing, we have to first figure out if the target is on the left side or right side
               Vector2D v2DirToPoint = (pev->origin - pentNearest->v.origin).Make2D ();

               if (DotProduct (v2DirToPoint, g_pGlobals->v_right.Make2D ()) > 0.0)
                  SetStrafeSpeed (vDirectionNormal, pev->maxspeed);
               else
                  SetStrafeSpeed (vDirectionNormal, -pev->maxspeed);

               if ((fNearestDistance < 56.0) && (DotProduct (v2DirToPoint, g_pGlobals->v_forward.Make2D ()) < 0.0))
                  m_fMoveSpeed = -pev->maxspeed;
            }
         }
               
         // Standing still, no need to check?
         // FIXME: doesn't care for ladder movement (handled separately) should be included in some way
         if (((m_fMoveSpeed >= 10) || (m_fSideMoveSpeed >= 10)) && (m_fNoCollTime < WorldTime ()) && (CurrentTask ()->iTask != TASK_ATTACK) && !(m_iAimFlags & AIM_ENEMY))
         {
            if ((fMovedDistance < 2.0) && (m_fPrevSpeed >= 1.0)) // didn't we move enough previously?
            {
               // Then consider being stuck
               m_fPrevTime = WorldTime ();
               bBotIsStuck = true;

               if (m_fFirstCollideTime == 0.0)
                  m_fFirstCollideTime = WorldTime () + 0.2;

               DebugMsg ("STUCK");
            }
            else // not stuck yet
            {
               // test if there's something ahead blocking the way
               if (CantMoveForward (vDirectionNormal, &tr) && !IsOnLadder ())
               {
                  if (m_fFirstCollideTime == 0.0)
                     m_fFirstCollideTime = WorldTime () + 0.2;

                  else if (m_fFirstCollideTime <= WorldTime ())
                     bBotIsStuck = true;
               }
               else
                  m_fFirstCollideTime = 0.0;
            }

            if (!bBotIsStuck) // not stuck?
            {
               if (m_fProbeTime + 0.5 < WorldTime ())
                  ResetCollideState (); // reset collision memory if not being stuck for 0.5 secs
               else
               {
                  // remember to keep pressing duck if it was necessary ago
                  if ((m_rgcCollideMoves[m_cCollStateIndex] == COLLISION_DUCK) && IsOnFloor () || IsInWater ())
                     pev->button |= IN_DUCK;
               }
            }
            else // bot is stuck!
            {
               DebugMsg ("UN-STUCK");

               // not yet decided what to do?
               if (m_cCollisionState == COLLISION_NOTDECIDED)
               {
                  char cBits;

                  if (IsOnLadder ())
                     cBits = PROBE_STRAFE;
                  else if (IsInWater ())
                     cBits = (PROBE_JUMP | PROBE_STRAFE);
                  else
                     cBits = ((RandomFloat (0, 10.0) > 9.0 ? PROBE_JUMP : 0) | PROBE_STRAFE | PROBE_DUCK);

                  // collision check allowed if not flying through the air
                  if (IsOnFloor () || IsOnLadder () || IsInWater ())
                  {
                     char cState[8];
                     int i = 0;

                     // first 4 entries hold the possible collision states
                     cState[i++] = COLLISION_JUMP;
                     cState[i++] = COLLISION_DUCK;
                     cState[i++] = COLLISION_STRAFELEFT;
                     cState[i++] = COLLISION_STRAFERIGHT;

                     // now weight all possible states
                     if (cBits & PROBE_JUMP)
                     {
                        cState[i] = 0;

                        if (CanJumpUp (vDirectionNormal))
                           cState[i] += 10;

                        if (m_vDestOrigin.z >= pev->origin.z + 18.0)
                           cState[i] += 5;

                        if (EntityIsVisible (m_vDestOrigin))
                        {
                           MakeVectors (m_vMoveAngles);

                           vSource = EyePosition ();
                           vSource = vSource + (g_pGlobals->v_right * 15);

                           TraceLine (vSource, m_vDestOrigin, true, true, GetEntity (), &tr);
                           
                           if (tr.flFraction >= 1.0)
                           {
                              vSource = EyePosition ();
                              vSource = vSource - (g_pGlobals->v_right * 15);

                              TraceLine (vSource, m_vDestOrigin, true, true, GetEntity (), &tr);
                              
                              if (tr.flFraction >= 1.0)
                                 cState[i] += 5;
                           }
                        }
                        if (pev->flags & FL_DUCKING)
                           vSource = pev->origin;
                        else
                           vSource = pev->origin + Vector (0, 0, -17);

                        vDestination = vSource + vDirectionNormal * 30;
                        TraceLine (vSource, vDestination, true, true, GetEntity (), &tr);
                        
                        if (tr.flFraction != 1.0)
                           cState[i] += 10;
                     }
                     else
                        cState[i] = 0;
                     i++;

                     if (cBits & PROBE_DUCK)
                     {
                        cState[i] = 0;

                        if (CanDuckUnder (vDirectionNormal))
                           cState[i] += 10;

                        if ((m_vDestOrigin.z + 36.0 <= pev->origin.z) && EntityIsVisible (m_vDestOrigin))
                           cState[i] += 5;
                     }
                     else
                        cState[i] = 0;
                     i++;

                     if (cBits & PROBE_STRAFE)
                     {
                        cState[i] = 0;
                        cState[i + 1] = 0;

                        Vector2D v2DirToPoint;
                        Vector2D v2RightSide;

                        // to start strafing, we have to first figure out if the target is on the left side or right side
                        MakeVectors (m_vMoveAngles);

                        v2DirToPoint = (pev->origin - m_vDestOrigin).Make2D ().Normalize ();
                        v2RightSide = g_pGlobals->v_right.Make2D ().Normalize ();
                        
                        bool bDirRight = false;
                        bool bDirLeft = false;
                        bool bBlockedLeft = false;
                        bool bBlockedRight = false;

                        if (DotProduct (v2DirToPoint, v2RightSide) > 0)
                           bDirRight = true;
                        else
                           bDirLeft = true;

                        if (m_fMoveSpeed > 0)
                           vDirection = g_pGlobals->v_forward;
                        else
                           vDirection = -g_pGlobals->v_forward;

                        // Now check which side is blocked
                        vSource = pev->origin + (g_pGlobals->v_right * 32);
                        vDestination = vSource + (vDirection * 32);

                        TraceHull (vSource, vDestination, true, head_hull, GetEntity (), &tr);
                        
                        if (tr.flFraction != 1.0)
                           bBlockedRight = true;

                        vSource = pev->origin - (g_pGlobals->v_right * 32);
                        vDestination = vSource + (vDirection * 32);

                        TraceHull (vSource, vDestination, true, head_hull, GetEntity (), &tr);
                        
                        if (tr.flFraction != 1.0)
                           bBlockedLeft = true;

                        if (bDirLeft)
                           cState[i] += 5;
                        else
                           cState[i] -= 5;

                        if (bBlockedLeft)
                           cState[i] -= 5;

                        i++;

                        if (bDirRight)
                           cState[i] += 5;
                        else
                           cState[i] -= 5;

                        if (bBlockedRight)
                           cState[i] -= 5;
                     }
                     else
                     {
                        cState[i] = 0;
                        i++;
                        cState[i] = 0;
                     }

                     // weighted all possible moves, now sort them to start with most probable
                     char cTemp;
                     bool bSorting;

                     do
                     {
                        bSorting = false;
                        for (i = 0; i < 3; i++)
                        {
                           if (cState[i + 4] < cState[i + 5])
                           {
                              cTemp = cState[i];

                              cState[i] = cState[i + 1];
                              cState[i + 1] = cTemp;

                              cTemp = cState[i + 4];

                              cState[i + 4] = cState[i + 5];
                              cState[i + 5] = cTemp;

                              bSorting = true;
                           }
                        }
                     } while (bSorting);

                     for (i = 0; i < 4; i++)
                        m_rgcCollideMoves[i] = cState[i];

                     m_fCollideTime = WorldTime ();
                     m_fProbeTime = WorldTime () + 0.5;
                     m_cCollisionProbeBits = cBits;
                     m_cCollisionState = COLLISION_PROBING;
                     m_cCollStateIndex = 0;
                  }
               }

               if (m_cCollisionState == COLLISION_PROBING)
               {
                  if (m_fProbeTime < WorldTime ())
                  {
                     m_cCollStateIndex++;
                     m_fProbeTime = WorldTime () + 0.5;

                     if (m_cCollStateIndex > 4)
                     {
                        m_fWptTimeset = WorldTime () - 5.0;
                        ResetCollideState ();
                     }
                  }

                  if (m_cCollStateIndex <= 4)
                  {
                     switch (m_rgcCollideMoves[m_cCollStateIndex])
                     {
                     case COLLISION_JUMP:
                        if (IsOnFloor () || IsInWater ())
                           pev->button |= IN_JUMP;
                        break;

                     case COLLISION_DUCK:
                        if (IsOnFloor () || IsInWater ())
                           pev->button |= IN_DUCK;
                        break;

                     case COLLISION_STRAFELEFT:
                        SetStrafeSpeed (vDirectionNormal, -pev->maxspeed);
                        break;

                     case COLLISION_STRAFERIGHT:
                        SetStrafeSpeed (vDirectionNormal, pev->maxspeed);
                        break;
                     }
                  }
               }
            }
         }
      }
   }

   // Must avoid a Grenade?
   if (m_cAvoidGrenade != 0)
   {
      // Don't duck to get away faster
      pev->button &= ~IN_DUCK;

      m_fMoveSpeed = -pev->maxspeed;
      m_fSideMoveSpeed = pev->maxspeed * m_cAvoidGrenade;
   }
   float fEstimatedTime = 5.0; // time to reach next waypoint

   // calculate 'real' time that we need to get from one waypoint to another
   if ((m_pWaypointNodes != NULL) && (m_pWaypointNodes->pNext != NULL))
   {
      // calculate time from current point, to our next point using diving real path distance on our current movespeed
      float fEstimatedTime = g_pWaypoint->GetPathDistance (m_pWaypointNodes->iIndex, m_pWaypointNodes->pNext->iIndex) / m_fMoveSpeed * 2;

      // check if we got valid time
      if (fEstimatedTime <= 0.5)
         fEstimatedTime = 5.0; // used old method
   }

   // time to reach waypoint
   if ((m_fWptTimeset + fEstimatedTime < WorldTime ()) && FNullEnt (m_pentEnemy))
   {
      GetValidWaypoint ();
      DebugMsg ("unable to reach destination point, in %.1f seconds", fEstimatedTime);

      // clear these pointers, bot mingh be stuck getting to them
      if (!FNullEnt (m_pentPickupItem) && !m_bHasProgressBar)
         m_pentItemIgnore = m_pentPickupItem;

      m_pentPickupItem = NULL;
      m_pentShootBreakable = NULL;
      m_fItemCheckTime = WorldTime () + 5.0;
      m_iPickupType = PICKUP_NONE;
   }

   if (m_fDuckTime > WorldTime ())
      pev->button |= IN_DUCK;

   if (pev->button & IN_JUMP)
      m_fJumpTime = WorldTime ();

   if (m_fJumpTime + 0.85 > WorldTime ())
   {
      if (!IsOnFloor () && !IsInWater ())
         pev->button |= IN_DUCK;
   }

   if (!(pev->button & (IN_FORWARD | IN_BACK)))
   {
      if (m_fMoveSpeed > 0)
         pev->button |= IN_FORWARD;
      else if (m_fMoveSpeed < 0)
         pev->button |= IN_BACK;
   }

   if (!(pev->button & (IN_MOVELEFT | IN_MOVERIGHT)))
   {
      if (m_fSideMoveSpeed > 0)
         pev->button |= IN_MOVERIGHT;
      else if (m_fSideMoveSpeed < 0)
         pev->button |= IN_MOVELEFT;
   }

   static float fTimeDebugUpdate = 0.0;

   if (!FNullEnt (g_pentHostEdict) && (g_rgpcvBotCVar[CVAR_DEBUG]->GetInt () >= 1))
   {
      int iSpecIndex = g_pentHostEdict->v.iuser2;
      if (iSpecIndex == ENTINDEX (GetEntity ()))
      {
         static int iIndex, iGoal, iTask;

         if (m_pTasks != NULL)
         {
            if ((iTask != m_pTasks->iTask) || (iIndex != m_iCurrWptIndex) || (iGoal != m_pTasks->iData) || (fTimeDebugUpdate < WorldTime ()))
            {
               iTask = m_pTasks->iTask;
               iIndex = m_iCurrWptIndex;
               iGoal = m_pTasks->iData;

               char szTaskname[80];

               switch (iTask)
               {
               case TASK_NORMAL:
                  sprintf (szTaskname, "TASK_NORMAL");
                  break;

               case TASK_PAUSE:
                  sprintf (szTaskname, "TASK_PAUSE");
                  break;

               case TASK_MOVETOPOSITION:
                  sprintf (szTaskname, "TASK_MOVETOPOSITION");
                  break;

               case TASK_FOLLOWUSER:
                  sprintf (szTaskname, "TASK_FOLLOWUSER");
                  break;

               case TASK_WAITFORGO:
                  sprintf (szTaskname, "TASK_WAITFORGO");
                  break;

               case TASK_PICKUPITEM:
                  sprintf (szTaskname, "TASK_PICKUPITEM");
                  break;

               case TASK_CAMP:
                  sprintf (szTaskname, "TASK_CAMP");
                  break;

               case TASK_PLANTBOMB:
                  sprintf (szTaskname, "TASK_PLANTBOMB");
                  break;

               case TASK_DEFUSEBOMB:
                  sprintf (szTaskname, "TASK_DEFUSEBOMB");
                  break;

               case TASK_ATTACK:
                  sprintf (szTaskname, "TASK_ATTACK");
                  break;

               case TASK_ENEMYHUNT:
                  sprintf (szTaskname, "TASK_ENEMYHUNT");
                  break;

               case TASK_SEEKCOVER:
                  sprintf (szTaskname, "TASK_SEEKCOVER");
                  break;

               case TASK_THROWHEGRENADE:
                  sprintf (szTaskname, "TASK_THROWHEGRENADE");
                  break;

               case TASK_THROWFLASHBANG:
                  sprintf (szTaskname, "TASK_THROWFLASHBANG");
                  break;

               case TASK_THROWSMOKEGRENADE:
                  sprintf (szTaskname, "TASK_THROWSMOKEGRENADE");
                  break;

               case TASK_DOUBLEJUMP:
                  sprintf (szTaskname, "TASK_DOUBLEJUMP");
                  break;

               case TASK_ESCAPEFROMBOMB:
                  sprintf (szTaskname, "TASK_ESCAPEFROMBOMB");
                  break;

               case TASK_SHOOTBREAKABLE:
                  sprintf (szTaskname, "TASK_SHOOTBREAKABLE");
                  break;

               case TASK_HIDE:
                  sprintf (szTaskname, "TASK_HIDE");
                  break;

               case TASK_BLINDED:
                  sprintf (szTaskname, "TASK_BLINDED");
                  break;

               case TASK_SPRAYLOGO:
                  sprintf (szTaskname, "TASK_SPRAYLOGO");
                  break;
               }

               char szEnemyName[80], szWeaponName[80], szAimFlags[32];

               if (!FNullEnt (m_pentEnemy))
                  strcpy (szEnemyName, STRING (m_pentEnemy->v.netname));
               else if (!FNullEnt (m_pentLastEnemy))
               {
                  strcpy (szEnemyName, " (L)");
                  strcat (szEnemyName, STRING (m_pentLastEnemy->v.netname));
               }
               else
                  strcpy (szEnemyName, " (null)");

               char szPickupName[80];

               if (!FNullEnt (m_pentPickupItem))
                  strcpy (szPickupName, STRING (m_pentPickupItem->v.classname));
               else
                  strcpy (szPickupName, " (null)");

               tWeaponSelect *pSelect = &g_rgkWeaponSelect[0];
               char cWeaponCount = 0;

               while ((m_iCurrentWeapon != pSelect->iId) && (cWeaponCount < NUM_WEAPONS))
               {
                  pSelect++;
                  cWeaponCount++;
               }

               sprintf (szAimFlags, "%s%s%s%s%s%s%s%s", 
                  m_iAimFlags & AIM_DEST ? " DEST" : "", 
                  m_iAimFlags & AIM_CAMP ? " CAMP" : "",
                  m_iAimFlags & AIM_PREDICTPATH ? " PREDICTPATH" : "",
                  m_iAimFlags & AIM_LASTENEMY ? " LASTENEMY" : "",
                  m_iAimFlags & AIM_ENTITY ? " ENTITY" : "",
                  m_iAimFlags & AIM_ENEMY ? " ENEMY" : "",
                  m_iAimFlags & AIM_GRENADE ? " GRENADE" : "",
                  m_iAimFlags & AIM_OVERRIDE ? " OVERRIDE" : "");

               if (cWeaponCount >= NUM_WEAPONS)
               {
                  // prevent printing unknown message from known weapons
                  switch (m_iCurrentWeapon)
                  {
                  case WEAPON_HEGRENADE:
                     sprintf (szWeaponName, "weapon_hegrenade");
                     break;

                  case WEAPON_FLASHBANG:
                     sprintf (szWeaponName, "weapon_flashbang");
                     break;

                  case WEAPON_SMOKEGRENADE:
                     sprintf (szWeaponName, "weapon_smokegrenade");
                     break;

                  case WEAPON_C4:
                     sprintf (szWeaponName, "weapon_c4");
                     break;

                  default:
                     sprintf (szWeaponName, "UNKNOWN! (%d)", m_iCurrentWeapon);
                  }
               }
               else
                  sprintf (szWeaponName, pSelect->szWeaponName);

               char szOutput[512];

               sprintf (szOutput, "\n\n\n\n%s (H:%.1f/A:%.1f)- Task: %d=%s Desire:%.02f\nItem: %s Clip: %d Ammo: %d%s Money: %d AimFlags: %s\nSP=%.02f SSP=%.02f I=%d PG=%d G=%d T: %.02f MT: %d\nEnemy=%s Pickup=%s\n", STRING (pev->netname), pev->health, pev->armorvalue, iTask, szTaskname, CurrentTask ()->fDesire, &szWeaponName[7], GetAmmoInClip (), GetAmmo (), m_bIsReloading ? " (R)" : "", m_iAccount, szAimFlags, m_fMoveSpeed, m_fSideMoveSpeed, iIndex, m_iPrevGoalIndex, iGoal, m_fWptTimeset - WorldTime (), pev->movetype, szEnemyName, szPickupName);

               MESSAGE_BEGIN (MSG_ONE_UNRELIABLE, SVC_TEMPENTITY, NULL, g_pentHostEdict);
                  WRITE_BYTE (TE_TEXTMESSAGE);
                  WRITE_BYTE (1);
                  WRITE_SHORT (FixedSigned16 (-1, 1 << 13));
                  WRITE_SHORT (FixedSigned16 (0, 1 << 13));
                  WRITE_BYTE (0);
                  WRITE_BYTE (GetTeam (GetEntity ()) == TEAM_CT ? 0 : 255);
                  WRITE_BYTE (100);
                  WRITE_BYTE (GetTeam (GetEntity ()) != TEAM_CT ? 0 : 255);
                  WRITE_BYTE (0);
                  WRITE_BYTE (255);
                  WRITE_BYTE (255);
                  WRITE_BYTE (255);
                  WRITE_BYTE (0);
                  WRITE_SHORT (FixedUnsigned16 (0, 1 << 8));
                  WRITE_SHORT (FixedUnsigned16 (0, 1 << 8));
                  WRITE_SHORT (FixedUnsigned16 (1.0, 1 << 8));
                  WRITE_STRING (const_cast <const char *> (&szOutput[0]));
               MESSAGE_END (); 

               fTimeDebugUpdate = WorldTime () + 1.0;
            }

            DrawArrow (g_pentHostEdict, EyePosition (), m_vDestOrigin, 10, 0, 0, 255, 0, 250, 5, 1);

            MakeVectors (m_vIdealAngles);
            DrawArrow (g_pentHostEdict, EyePosition (), EyePosition () + (g_pGlobals->v_forward * 300), 10, 0, 0, 0, 255, 250, 5, 1);
            
            MakeVectors (pev->v_angle);
            DrawArrow (g_pentHostEdict, EyePosition (), EyePosition () + (g_pGlobals->v_forward * 300), 10, 0, 255, 0, 0, 250, 5, 1);
            
            // now draw line from source to destination
            tPathNode *pNode = &m_pWaypointNodes[0];

            while (pNode != NULL)
            {
               Vector vSource = g_pWaypoint->GetPath (pNode->iIndex)->vOrigin;
               pNode = pNode->pNext;

               if (pNode != NULL)
               {
                  Vector vDest = g_pWaypoint->GetPath (pNode->iIndex)->vOrigin;
                  DrawArrow (g_pentHostEdict, vSource, vDest, 15, 0, 255, 100, 55, 200, 5, 1);
               }
            }
         }
      }
   }

   // save the previous speed (for checking if stuck)
   m_fPrevSpeed = fabsf (m_fMoveSpeed);
   m_iLastDamageType = -1; // Reset Damage
   
   ClampAngles (pev->angles);
   ClampAngles (pev->v_angle);
}

bool CBot::HasHostage (void)
{
   for (int i = 0; i < MAX_HOSTAGES; i++)
   {
      if (!FNullEnt (m_rgpHostages[i]))
      {
         // don't care about dead hostages
         if ((m_rgpHostages[i]->v.health <= 0) || ((pev->origin - m_rgpHostages[i]->v.origin).Length () > 600))
         {
            m_rgpHostages[i] = NULL;
            continue;
         }
         return true;
      }
   }
   return false;
}

void CBot::ResetCollideState (void)
{
   m_fCollideTime = 0.0;
   m_fProbeTime = 0.0;

   m_cCollisionProbeBits = 0;
   m_cCollisionState = COLLISION_NOTDECIDED;
   m_cCollStateIndex = 0;
}

int CBot::GetAmmo (void)
{
   if (g_rgkWeaponDefs[m_iCurrentWeapon].iAmmo1 == -1) 
      return 0; 

   return m_rgAmmo[g_rgkWeaponDefs[m_iCurrentWeapon].iAmmo1]; 
}

void CBot::TakeDamage (edict_t *pentInflictor, Vector vOrigin, int iDamage, int iArmor, int iBits)
{
   m_iLastDamageType = iBits;

   int iTeam = GetTeam (GetEntity ());
   CollectGoalExperience (iDamage, iTeam);

   if (IsValidPlayer (pentInflictor))
   {
      if (GetTeam (pentInflictor) == iTeam)
      {
         // attacked by a teammate
         if (pev->health < (m_ucPersonality == PERSONALITY_AGRESSIVE ? 98 : 90))
         {
            if (m_fSeeEnemyTime + 1.7 < WorldTime ())
            {
               // alright, die you teamkiller!!!
               m_fActualReactionTime = 0.0;
               m_fSeeEnemyTime = WorldTime ();
               m_pentEnemy = pentInflictor;
               m_pentLastEnemy = m_pentEnemy;
               m_vLastEnemyOrigin = m_pentEnemy->v.origin;
               m_vEnemy = m_pentEnemy->v.origin;

               ChatMessage (CHAT_TEAMATTACK);
            }
         }
         HandleChatterMessage ("#Bot_TeamAttack");
      }
      else
      {
         // attacked by an enemy
         if (pev->health > 70)
         {
            m_fAgressionLevel += 0.1;

            if (m_fAgressionLevel > 1.0)
               m_fAgressionLevel = 1.0;
         }
         else
         {
            m_fFearLevel += 0.05;

            if (m_fFearLevel > 1.0)
               m_fFearLevel = 1.0;
         }
         // stop bot from hiding
         RemoveCertainTask (TASK_HIDE);

         if (FNullEnt (m_pentEnemy))
         {
            m_pentLastEnemy = pentInflictor;
            m_vLastEnemyOrigin = pentInflictor->v.origin;

            // FIXME - Bot doesn't necessary sees this enemy
            m_fSeeEnemyTime = WorldTime ();
         }
         CollectExperienceData (pentInflictor, iArmor + iDamage);
      }
   }
   else // hurt by unusual damage like drowning or gas
   {
      // leave the camping/hiding position
      if (!g_pWaypoint->Reachable (pev->origin, m_vDestOrigin, GetEntity ()))
      {
         DeleteSearchNodes ();
         FindWaypoint ();
      }
   }
}

void CBot::CollectGoalExperience (int iDamage, int iTeam)
{
   // gets called each time a bot gets damaged by some enemy. tries to achieve a statistic about most/less dangerous
   // waypoints for a destination goal used for pathfinding

   if ((g_iNumWaypoints < 1) || g_bWaypointsChanged || (m_iChosenGoalIndex < 0) || (m_iPrevGoalIndex < 0))
      return;

   // only rate goal waypoint if bot died because of the damage
   // FIXME: could be done a lot better, however this cares most about damage done by sniping or really deadly weapons
   if (pev->health - iDamage <= 0)
   {
      if (iTeam == TEAM_TERRORIST)
      {
         int iValue = (g_pExperienceData + (m_iChosenGoalIndex * g_iNumWaypoints) + m_iPrevGoalIndex)->wTeam0Value;
         iValue -= pev->health / 20;

         if (iValue < -MAX_GOAL_VAL)
            iValue = -MAX_GOAL_VAL;

         else if (iValue > MAX_GOAL_VAL)
            iValue = MAX_GOAL_VAL;

         (g_pExperienceData + (m_iChosenGoalIndex * g_iNumWaypoints) + m_iPrevGoalIndex)->wTeam0Value = static_cast <signed short> (iValue);
      }
      else
      {
         int iValue = (g_pExperienceData + (m_iChosenGoalIndex * g_iNumWaypoints) + m_iPrevGoalIndex)->wTeam1Value;
         iValue -= pev->health / 20;

         if (iValue < -MAX_GOAL_VAL)
            iValue = -MAX_GOAL_VAL;

         else if (iValue > MAX_GOAL_VAL)
            iValue = MAX_GOAL_VAL;

         (g_pExperienceData + (m_iChosenGoalIndex * g_iNumWaypoints) + m_iPrevGoalIndex)->wTeam1Value = static_cast <signed short> (iValue);
      }
   }
}

void CBot::CollectExperienceData (edict_t *pentAttacker, int iDamage)
{
   // this function gets called each time a bot gets damaged by some enemy. sotores the damage (teamspecific) done by victim.

   if (!IsValidPlayer (pentAttacker))
      return;

   int iAttackerTeam = GetTeam (pentAttacker);
   int iVictimTeam = GetTeam (GetEntity ());

   if (iAttackerTeam == iVictimTeam)
      return;

   // if these are bots also remember damage to rank destination of the bot
   m_fGoalValue -= static_cast <float> (iDamage);

   if (g_pBotManager->GetBot (pentAttacker) != NULL)
      g_pBotManager->GetBot (pentAttacker)->m_fGoalValue += static_cast <float> (iDamage);

   if (iDamage < 20)
      return; // do not collect damage less than 20

   int iAttackerIndex = g_pWaypoint->FindNearest (pentAttacker->v.origin);
   int iVictimIndex = g_pWaypoint->FindNearest (pev->origin);

   if (pev->health - iDamage < 0)
   {
      if (iVictimTeam == TEAM_TERRORIST)
         (g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + iVictimIndex)->uTeam0Damage++;
      else
         (g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + iVictimIndex)->uTeam1Damage++;

      if ((g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + iVictimIndex)->uTeam0Damage > MAX_DAMAGE_VAL)
         (g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + iVictimIndex)->uTeam0Damage = MAX_DAMAGE_VAL;

      if ((g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + iVictimIndex)->uTeam1Damage > MAX_DAMAGE_VAL)
         (g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + iVictimIndex)->uTeam1Damage = MAX_DAMAGE_VAL;
   }

   float fUpdate = IsValidBot (pentAttacker) ? 10.0 : 7.0;

   // store away the damage done
   if (iVictimTeam == TEAM_TERRORIST)
   {
      int iValue = (g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + iAttackerIndex)->uTeam0Damage;
      iValue += static_cast <int> (iDamage / fUpdate);

      if (iValue > MAX_DAMAGE_VAL)
         iValue = MAX_DAMAGE_VAL;

      (g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + iAttackerIndex)->uTeam0Damage = static_cast <unsigned short> (iValue);

      tPath *pPath = g_pWaypoint->GetPath (iVictimIndex);
      
      // affect nearby connected with victim waypoints
      for (int i = 0; i < MAX_PATH_INDEX; i++)
      {
         if ((pPath->iIndex[i] > -1) && (pPath->iIndex[i] < g_iNumWaypoints))
         {
            iValue = (g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + pPath->iIndex[i])->uTeam0Damage;
            iValue += static_cast <int> (iDamage / (fUpdate * 4));

            if (iValue > MAX_DAMAGE_VAL)
               iValue = MAX_DAMAGE_VAL;

            (g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + pPath->iIndex[i])->uTeam0Damage = static_cast <unsigned short> (iValue);
         }
      }
   }
   else
   {
      int iValue = (g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + iAttackerIndex)->uTeam1Damage;
      iValue += static_cast <int> (iDamage / fUpdate);

      if (iValue > MAX_DAMAGE_VAL)
         iValue = MAX_DAMAGE_VAL;

      (g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + iAttackerIndex)->uTeam1Damage = static_cast <unsigned short> (iValue);

      tPath *pPath = g_pWaypoint->GetPath (iVictimIndex);
      
      // affect nearby connected with victim waypoints
      for (int i = 0; i < MAX_PATH_INDEX; i++)
      {
         if ((pPath->iIndex[i] > -1) && (pPath->iIndex[i] < g_iNumWaypoints))
         {
            iValue = (g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + pPath->iIndex[i])->uTeam1Damage;
            iValue += static_cast <int> (iDamage / (fUpdate * 4));

            if (iValue > MAX_DAMAGE_VAL)
               iValue = MAX_DAMAGE_VAL;

            (g_pExperienceData + (iVictimIndex * g_iNumWaypoints) + pPath->iIndex[i])->uTeam1Damage = static_cast <unsigned short> (iValue);
         }
      }
   }
}

void CBot::HandleChatterMessage (const char *csz)
{
   // this function is added to prevent engine crashes with: 'Message XX started, before message XX ended', or something.
   
   if (FStrEq (csz, "#CTs_Win") && GetTeam (GetEntity ()) == TEAM_CT)
   {
      if (g_fTimeRoundMid > WorldTime ())
         ChatterMessage (VOICE_WON_ROUND_QUICK);
      else
         ChatterMessage (VOICE_WON_ROUND);
   }

   if (FStrEq (csz, "#Terrorists_Win") && GetTeam (GetEntity ()) == TEAM_TERRORIST)
   {
      if (g_fTimeRoundMid > WorldTime ())
         ChatterMessage (VOICE_WON_ROUND_QUICK);
      else
         ChatterMessage (VOICE_WON_ROUND);
   }

   if (FStrEq (csz, "#Bot_TeamAttack"))
      ChatterMessage (VOICE_FRIENDLY_FIRE);

   if (FStrEq (csz, "#Bot_NiceShotCommander"))
      ChatterMessage (VOICE_NICESHOT_COMMANDER);

   if (FStrEq (csz, "#Bot_NiceShotPall"))
      ChatterMessage (VOICE_NICESHOT_PALL);
}

void CBot::ChatMessage (int iType, bool bTeamSay)
{
   if (g_arChatEngine[iType].IsFree () || !g_rgpcvBotCVar[CVAR_CHAT]->GetBool ())
      return;

   const char *cszPhrase = g_arChatEngine[iType].Random ().GetString ();

   if (IsNullString (cszPhrase))
      return;

   PrepareChatMessage (const_cast <char *> (cszPhrase)); 
   PushMessageQueue (bTeamSay ? MESSAGE_TEAMSAY : MESSAGE_SAY);   
}

void CBot::DiscardWeaponForUser (edict_t *pentUser, bool bDiscardC4)
{
   // this function, asks bot to discard his current primary weapon (or c4) to the user that requsted it with /drop* 
   // command, very useful, when i'm don't have money to buy anything... )

   if (IsAlive (pentUser) && (m_iAccount >= 2000) && HasPrimaryWeapon () && ((pentUser->v.origin - pev->origin).Length () <= 240))
   {
      m_iAimFlags |= AIM_ENTITY;
      m_vLookAt = pentUser->v.origin;

      if (bDiscardC4)
      {
         SelectWeaponByName ("weapon_c4");
         FakeClientCommand (GetEntity (), "drop");

         SayText (VarArgs ("Here! %s, and now go and setup it!", STRING (pentUser->v.netname)));
      }
      else
      {
         SelectBestWeapon ();
         FakeClientCommand (GetEntity (), "drop");

         SayText (VarArgs ("Here the weapon! %s, feel free to use it ;)", STRING (pentUser->v.netname)));
      }

      m_pentPickupItem = NULL;
      m_iPickupType = PICKUP_NONE;
      m_fItemCheckTime = WorldTime () + 5.0;

      if (m_bInBuyZone)
      {
         m_bBuyingFinished = false;
         m_iBuyCount = 0;

         PushMessageQueue (MESSAGE_BUY);
         m_fNextBuyTime = WorldTime ();
      }
   }
   else
      SayText (VarArgs ("Sorry %s, i don't want discard my %s to you!", STRING (pentUser->v.netname), bDiscardC4 ? "bomb" : "weapon"));
}

void CBot::ResetDoubleJumpState (void)
{
   TaskComplete ();
   DeleteSearchNodes ();

   m_pentDoubleJumpEdict = NULL;
   m_fDuckForJump = 0.0;
   m_vDoubleJumpOrigin = NULLVEC;
   m_iTravelStartIndex = -1;
   m_bJumpReady = false;
}

void CBot::DebugMsg (const char *fmt, ...)
{
   if (g_rgpcvBotCVar[CVAR_DEBUG]->GetInt () < 2)
      return;

   va_list ap;
   char szBuffer[1024];

   va_start (ap, fmt);
   vsprintf (szBuffer, fmt, ap);
   va_end (ap);

   ServerPrintNoTag ("%s: %s", STRING (pev->netname), szBuffer);

   if (g_rgpcvBotCVar[CVAR_DEBUG]->GetInt () >= 3)
      LogToFile (false, LOG_DEFAULT, "%s: %s", STRING (pev->netname), szBuffer);
}

Vector CBot::CheckToss (const Vector &vStart, Vector vEnd)
{
   // this function returns the velocity at which an object should looped from start to land near end.
   // returns null vector if toss is not feasible.

   TraceResult tr;
   float fGravity = g_rgpcvBotCVar[CVAR_GRAVITY]->GetFloat () * 0.55;

   vEnd = vEnd - pev->velocity;
   vEnd.z -= 15.0;

   if (fabsf (vEnd.z - vStart.z) > 500.0)
      return NULLVEC;

   Vector vMidPoint = vStart + (vEnd - vStart) * 0.5;
   TraceHull (vMidPoint, vMidPoint + Vector (0, 0, 500), true, head_hull, ENT (pev), &tr);

   if (tr.flFraction < 1.0)
   {
      vMidPoint = tr.vecEndPos;
      vMidPoint.z = tr.pHit->v.absmin.z - 1.0;
   }

   if ((vMidPoint.z < vStart.z) || (vMidPoint.z < vEnd.z))
      return NULLVEC;

   float fTimeOne = sqrtf ((vMidPoint.z - vStart.z) / (0.5 * fGravity));
   float fTimeTwo = sqrtf ((vMidPoint.z - vEnd.z) / (0.5 * fGravity));

   if (fTimeOne < 0.1)
      return NULLVEC;

   Vector vNadeVelocity = (vEnd - vStart) / (fTimeOne + fTimeTwo);
   vNadeVelocity.z = fGravity * fTimeOne;

   Vector vApex = vStart + vNadeVelocity * fTimeOne;
   vApex.z = vMidPoint.z;

   TraceHull (vStart, vApex, false, head_hull, ENT (pev), &tr);

   if ((tr.flFraction < 1.0) || tr.fAllSolid)
      return NULLVEC;

   TraceHull (vEnd, vApex, true, head_hull, ENT (pev), &tr);

   if (tr.flFraction != 1.0)
   {
      float fDot = -DotProduct (tr.vecPlaneNormal, (vApex - vEnd).Normalize ());
      
      if ((fDot > 0.7) || (tr.flFraction < 0.8)) // 60 degrees
         return NULLVEC;
   }
   return (vNadeVelocity * 0.777);
}

Vector CBot::CheckThrow (const Vector &vStart, Vector vEnd, float fSpeed)
{
   // this function returns the velocity vector at which an object should be thrown from start to hit end.
   // returns null vector if throw is not feasible.

   Vector vNadeVelocity = (vEnd - vStart);
   TraceResult tr;

   float fGravity = g_rgpcvBotCVar[CVAR_GRAVITY]->GetFloat () * 0.55;
   float fTime = vNadeVelocity.Length () / 195.0;

   if (fTime < 0.01)
      return NULLVEC;
   else if (fTime > 2.0)
      fTime = 1.2;

   vNadeVelocity = vNadeVelocity * (1.0 / fTime);
   vNadeVelocity.z += fGravity * fTime * 0.5;

   Vector vApex = vStart + (vEnd - vStart) * 0.5;
   vApex.z += 0.5 * fGravity * (fTime * 0.5) * (fTime * 0.5);

   TraceHull (vStart, vApex, false, head_hull, GetEntity (), &tr);

   if ((tr.flFraction != 1.0) && !(m_iStates & STATE_THROWFLASHBANG))
      return NULLVEC;

   TraceHull (vEnd, vApex, true, head_hull, GetEntity (), &tr);

   if ((tr.flFraction != 1.0) || tr.fAllSolid)
   {
      float fDot = -DotProduct (tr.vecPlaneNormal, (vApex - vEnd).Normalize ());

      if ((fDot > 0.7) || (tr.flFraction < 0.8) && !(m_iStates & STATE_THROWFLASHBANG))
         return NULLVEC;
   }
   return vNadeVelocity * 0.7793;
}

void CBot::MoveToVector (Vector vTo)
{
   if (vTo == NULLVEC)
      return;

   FindShortestPath (m_iCurrWptIndex, g_pWaypoint->FindNearest (vTo));
}

void CBot::EstimateNextFrameDuration (void)
{
   // the purpose of this function is to compute, according to the specified computation
   // method, the msec value which will be passed as an argument of pfnRunPlayerMove. This
   // function is called every frame for every bot, since the RunPlayerMove is the function
   // that tells the engine to put the bot character model in movement. This msec value
   // tells the engine how long should the movement of the model extend inside the current
   // frame. It is very important for it to be exact, else one can experience bizarre
   // problems, such as bots getting stuck into each others. That's because the model's
   // bounding boxes, which are the boxes the engine uses to compute and detect all the
   // collisions of the model, only exist, and are only valid, while in the duration of the
   // movement. That's why if you get a pfnRunPlayerMove for one bot that lasts a little too
   // short in comparison with the frame's duration, the remaining time until the frame
   // elapses, that bot will behave like a ghost : no movement, but bullets and players can
   // pass through it. Then, when the next frame will begin, the stucking problem will arise !

   switch (g_iMsecMethod)
   {
   default:
      g_iMsecMethod = METHOD_PM; // PM's calc method

   case METHOD_PM:
      // this is Pierre-Marie 'PM' Baty's method for calculating the msec value.

      m_iMsecVal = static_cast <int> (g_fTimeFrameInterval * 1000);

      if (m_iMsecVal > 255)
         m_iMsecVal = 255;

      break;

   case METHOD_RICH:
      // this is Rich 'TheFatal' Whitehouse's method for calculating the msec value,

      if (m_fMsecDel <= WorldTime ())
      {
         if (m_fMsecNum > 0)
            m_iMsecVal = 450.0 / m_fMsecNum;

         m_fMsecDel = WorldTime () + 0.5; // next check in half a second
         m_fMsecNum = 0;
      }
      else
         m_fMsecNum++;

      if (m_iMsecVal < 1)
         m_iMsecVal = 1; // don't allow the msec delay to be null
      else if (m_iMsecVal > 100)
         m_iMsecVal = 100; // don't allow it to last longer than 100 milliseconds either

      break;

   case METHOD_TOBIAS:
      // this is Tobias 'Killaruna' Heimann's method for calculating the msec value,

      if ((m_fMsecDel + m_fMsecNum / 1000) < WorldTime () - 0.5)
      {
         m_fMsecDel = WorldTime () - 0.05; // after pause
         m_fMsecNum = 0;
      }

      if (m_fMsecDel > WorldTime ())
      {
         m_fMsecDel = WorldTime () - 0.05; // after map changes
         m_fMsecNum = 0;
      }

      m_iMsecVal = (WorldTime () - m_fMsecDel) * 1000 - m_fMsecNum; // optimal msec value since start of 1 sec period
      m_fMsecNum = (WorldTime () - m_fMsecDel) * 1000; // value ve have to add to reach optimum

      // do we have to start a new 1 sec period ?
      if (m_fMsecNum > 1000)
      {
         m_fMsecDel += m_fMsecNum / 1000;
         m_fMsecNum = 0;
      }

      if (m_iMsecVal < 5)
         m_iMsecVal = 5; // don't allow the msec delay to be too low
      else if (m_iMsecVal > 255)
         m_iMsecVal = 255; // don't allow it to last longer than 255 milliseconds either

      break;

   case METHOD_LEON:
      // this is Leon's method for computing the msec value
      m_iMsecVal = static_cast <int> ((WorldTime () - m_fMsecInterval) * 1000);

      if (m_iMsecVal > 255)
         m_iMsecVal = 255;

      m_fMsecInterval = WorldTime ();

      break;

   }
   return;
}

bool CBot::CanHearC4 (Vector *pvHolder)
{
   // this function returns if bomb is hearable and if so the exact position as a vector

   const float fDistance = MAX_BOMBHEARDISTANCE * MAX_BOMBHEARDISTANCE;

   if (LengthSquared (pev->origin - g_pWaypoint->GetBombPosition ()) < fDistance)
   {
      *pvHolder = g_pWaypoint->GetBombPosition ();
      return true;
   }
   return false;
}

void CBot::ChangeBurstMode (void)
{
   if ((m_iCurrentWeapon == WEAPON_GLOCK18) || (m_iCurrentWeapon == WEAPON_FAMAS) && !HasShield ())
   {
      pev->button |= IN_ATTACK2; // preess second mouse button to disable or enable burst mode

      if (m_iWeaponBurstMode == BMSL_DISABLED)
         m_iWeaponBurstMode = BMSL_ENABLED;
      else if (m_iWeaponBurstMode == BMSL_ENABLED)
         m_iWeaponBurstMode = BMSL_DISABLED;

      DebugMsg ("burst mode set to: %s", m_iWeaponBurstMode == BMSL_ENABLED ? "enabled" : "disabled");
   }
}

void CBot::CheckSilencer (void)
{
   if ((m_iCurrentWeapon == WEAPON_USP) || (m_iCurrentWeapon == WEAPON_M4A1) && !HasShield ())
   {
      int iRandomNum = (m_ucPersonality == PERSONALITY_AGRESSIVE ? 35 : 65);

      // aggressive bots don't like the silencer
      if (RandomLong (1, 100) <= (m_iCurrentWeapon == WEAPON_USP ? iRandomNum / 3 : iRandomNum))
      {
         if (pev->weaponanim > 6) // is the silencer not attached...
            pev->button |= IN_ATTACK2; // attach the silencer
      }
      else
      {
         if (pev->weaponanim <= 6) // is the silencer attached...
            pev->button |= IN_ATTACK2; // detach the silencer
      }
      DebugMsg ("silencer: %s", pev->weaponanim <= 6 ? "detached" : pev->weaponanim > 6 ? "attached" : "error");
   }
}

float CBot::GetBombTimeleft (void)
{
   if (!g_bBombPlanted)
      return 0.0;

   return fabsf ((g_fTimeBombPlanted + g_rgpcvBotCVar[CVAR_C4TIMER]->GetFloat ()) - WorldTime ());
}

bool CBot::OutOfBombTimer (void)
{
   if ((g_iMapType & MAP_DE) && (m_bHasProgressBar || (CurrentTask ()->iTask == TASK_ESCAPEFROMBOMB)))
      return false; // if CT bot already start defusing, or already escaping, return false

   // calculate left time
   float fTimeLeft = GetBombTimeleft ();

   // if time left greater than 13, no need to do other checks
   if (fTimeLeft > 13)
      return false;

   Vector vBombOrigin = g_pWaypoint->GetBombPosition ();

   // for terrorist, if timer is lower than eleven seconds, return true
   if ((static_cast <int> (fTimeLeft) < 12) && (GetTeam (GetEntity ()) == TEAM_TERRORIST) && ((vBombOrigin - pev->origin).Length () < 1000))
      return true;

   bool bHasTeammatesWithDefuserKit = false;

   // check if our teammates has defusal kit
   for (int i = 0; i < MaxClients (); i++)
   {
      CBot *pBot = NULL; // temporaly pointer to bot

      // search players with defuse kit
      if (((pBot = g_pBotManager->GetBot (i)) != NULL) && (GetTeam (pBot->GetEntity ()) == TEAM_CT) && pBot->m_bHasDefuser && ((vBombOrigin - pBot->pev->origin).Length () < 500))
      {
         bHasTeammatesWithDefuserKit = true;
         break;
      }
   }

   // add reach time to left time
   float fReachTime = g_pWaypoint->GetTravelTime (pev->maxspeed, g_pWaypoint->GetPath (m_iCurrWptIndex)->vOrigin, vBombOrigin);

   // for counter-terrorist check alos is we have time to reach position plus average defuse time
   if (((fTimeLeft < fReachTime + 10.0) && !m_bHasDefuser && !bHasTeammatesWithDefuserKit) || ((fTimeLeft < fReachTime + 5.0) && m_bHasDefuser))
      return true;

   return false; // return false otherwise
}

void CBot::ReactOnSound (void)
{
   // setup engines potentially audible set for this bot
   Vector vOrg = EarPosition ();

   if (pev->flags & FL_DUCKING)
      vOrg = vOrg + (VEC_HULL_MIN - VEC_DUCK_HULL_MIN);

   unsigned char *ucPAS = ENGINE_SET_PAS (reinterpret_cast <float *> (&vOrg));

   edict_t *pentPlayer = NULL;
   float fMinDistance = 9999.0, fDistance;

   // loop through all enemy clients to check for hearable stuff
   for (int i = 0; i < MaxClients (); i++)
   {
      if (!(g_rgkClients[i].iFlags & CFLAG_USED) || !(g_rgkClients[i].iFlags & CFLAG_ALIVE) || (g_rgkClients[i].iTeam == GetTeam (GetEntity ())) || (g_rgkClients[i].pentEdict == GetEntity ()) || (g_rgkClients[i].fTimeSoundLasting < WorldTime ()))
         continue;

      // despite its name it also checks for sounds...
      if (!ENGINE_CHECK_VISIBILITY (g_rgkClients[i].pentEdict, ucPAS))
         continue;

      fDistance = (g_rgkClients[i].vSoundPosition - pev->origin).Length ();

      if (fDistance > g_rgkClients[i].fHearingDistance)
         continue;

      if (fDistance > fMinDistance)
         continue;

      pentPlayer = g_rgkClients[i].pentEdict;
      fMinDistance = fDistance;
   }

   // did the bot hear someone?
   if (!FNullEnt (pentPlayer))
   {
      // change to best weapon if heard something
      if (IsOnFloor () && (m_iCurrentWeapon != WEAPON_C4) && (m_iCurrentWeapon != WEAPON_HEGRENADE) && (m_iCurrentWeapon != WEAPON_SMOKEGRENADE) && (m_iCurrentWeapon != WEAPON_FLASHBANG) && !g_rgpcvBotCVar[CVAR_KNIFEMODE]->GetBool ()) 
         SelectBestWeapon ();

      m_iStates |= STATE_HEARINGENEMY;
      m_fHeardSoundTime = WorldTime ();

      if ((RandomLong (0, 100) < 25) && FNullEnt (m_pentEnemy) && FNullEnt (m_pentLastEnemy) && (m_fSeeEnemyTime + 7.0 < WorldTime ()))
         ChatterMessage (VOICE_HEARDENEMY);

      // didn't bot already have an enemy? take this one...
      if (m_vLastEnemyOrigin == NULLVEC)
      {
         m_pentLastEnemy = pentPlayer;
         m_vLastEnemyOrigin = pentPlayer->v.origin;
      }
      else // bot had an enemy, check if it's the heard one
      {
         if (pentPlayer == m_pentLastEnemy)
         {
            // bot sees enemy? then bail out!
            if (m_iStates & STATE_SEEINGENEMY)
               return;

            m_vLastEnemyOrigin = pentPlayer->v.origin;
         }
         else
         {
            // if bot had an enemy but the heard one is nearer, take it instead
            fDistance = (m_vLastEnemyOrigin - pev->origin).Length ();

            if (fDistance > (pentPlayer->v.origin - pev->origin).Length () && (m_fSeeEnemyTime + 2.0 < WorldTime ()))
            {
               m_pentLastEnemy = pentPlayer;
               m_vLastEnemyOrigin = pentPlayer->v.origin;
            }
            else
               return;
         }
      }

      // check if heard enemy can be seen
      if (CheckVisibility (VARS (pentPlayer), &m_vLastEnemyOrigin, &m_ucVisibility))
      {
         m_pentEnemy = pentPlayer;
         m_pentLastEnemy = pentPlayer;

         m_iStates |= STATE_SEEINGENEMY;
         m_fSeeEnemyTime = WorldTime ();
      }
      else if ((m_pentLastEnemy == pentPlayer) && (m_fSeeEnemyTime + 2.0 > WorldTime ()) && g_rgpcvBotCVar[CVAR_THRUWALLS]->GetBool () && (RandomLong (1, 100) < g_rgkSkillTab[m_iSkill / 20].iHeardShootThruProb) && IsShootableThruObstacle (pentPlayer->v.origin))
      {
         m_pentEnemy = pentPlayer;
         m_pentLastEnemy = pentPlayer;
         m_vLastEnemyOrigin = pentPlayer->v.origin;

         m_iStates |= (STATE_SEEINGENEMY | STATE_SUSPECTENEMY);
         m_fSeeEnemyTime = WorldTime ();

         DebugMsg ("shooting through wall, on the hearing");
      }
   }
}

bool CBot::IsShootableBreakable (edict_t *pent)
{
   // this function is checking that pointed by pent pointer obstacle, can be destroyed.

   if (FClassnameIs (pent, "func_breakable") || (FClassnameIs (pent, "func_pushable") && (pent->v.spawnflags & SF_PUSH_BREAKABLE)))
      return ((pent->v.takedamage != DAMAGE_NO) && (pent->v.impulse <= 0) && !(pent->v.flags & FL_WORLDBRUSH) && !(pent->v.spawnflags & SF_BREAK_TRIGGER_ONLY) && (pent->v.health < 500));

   return false;
}