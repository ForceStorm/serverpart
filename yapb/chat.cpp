//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#include "yapb.h"

void StripTags (char *szBuffer)
{
   // this function strips 'clan' tags specified below in given string buffer
      
   // first three tags for Enhanced POD-Bot (e[POD], 3[POD], E[POD])
   char *szOpenTag[25] = {"e[P", "3[P", "E[P", "-=", "-[", "-]", "-}", "-{", "<[", "<]", "[-", "]-", "{-", "}-", "[[", "[", "{", "]", "}", "<", ">", "-", "|", "=", "+"};
   char *szCloseTag[25] = {"]", "]", "]", "=-", "]-", "[-", "{-", "}-", "]>", "[>", "-]", "-[", "-}", "-{", "]]", "]", "}", "[", "{", ">", "<", "-", "|", "=", "+"};
   
   int iIndex, iFieldStart, iFieldStop, i, iLength;
   iLength = strlen (szBuffer); // get iLength of string

   // foreach known tag...
   for (iIndex = 0; iIndex < 25; iIndex++) 
   {
      iFieldStart = strstr (szBuffer, szOpenTag[iIndex]) - szBuffer; // look for a tag start

      // have we found a tag start?
      if ((iFieldStart >= 0) && (iFieldStart < 32))
      {
         iFieldStop = strstr (szBuffer, szCloseTag[iIndex]) - szBuffer; // look for a tag stop

         // have we found a tag stop?
         if ((iFieldStop > iFieldStart) && (iFieldStop < 32))
         {
            for (i = iFieldStart; i < iLength - (iFieldStop + static_cast <int> (strlen (szCloseTag[iIndex])) - iFieldStart); i++)
               szBuffer[i] = szBuffer[i + (iFieldStop + strlen (szCloseTag[iIndex]) - iFieldStart)]; // overwrite the buffer with the stripped string
            szBuffer[i] = '\0'; // terminate the string
         }
      }
   }

   // have we stripped too much (all the stuff)?
   if (strlen (szBuffer) == 0) 
   {
      StrTrim (szBuffer); // if so, string is just a tag

      // strip just the tag part...
      for (iIndex = 0; iIndex < 25; iIndex++) 
      {
         iFieldStart = strstr (szBuffer, szOpenTag[iIndex]) - szBuffer; // look for a tag start

         // have we found a tag start?
         if (iFieldStart >= 0 && iFieldStart < 32) 
         {
            iFieldStop = iFieldStart + strlen (szOpenTag[iIndex]); // set the tag stop

            for (i = iFieldStart; i < iLength - static_cast <int> (strlen (szOpenTag[iIndex])); i++)
               szBuffer[i] = szBuffer[i + strlen (szOpenTag[iIndex])]; // overwrite the szBuffer with the stripped string
            szBuffer[i] = '\0'; // terminate the string

            iFieldStart = strstr (szBuffer, szCloseTag[iIndex]) - szBuffer; // look for a tag stop

            // have we found a tag stop ?
            if ((iFieldStart >= 0) && (iFieldStart < 32))
            {
               iFieldStop = iFieldStart + strlen (szCloseTag[iIndex]); // set the tag stop

               for (i = iFieldStart; i < iLength - static_cast <int> (strlen (szCloseTag[iIndex])); i++)
                  szBuffer[i] = szBuffer[i + static_cast <int> (strlen (szCloseTag[iIndex]))]; // overwrite the szBuffer with the stripped string
               szBuffer[i] = 0; // terminate the string
            }
         }
      }
   }
   StrTrim (szBuffer); // to finish, strip eventual blanks after and before the tag marks
}

char *HumanizeName (char *szName)
{
   // this function humanize player name (i.e. trim clan and switch to lower case (sometimes))
      
   char szOutName[256]; // create return name buffer
   strcpy (szOutName, szName); // copy name to new buffer

   // drop tag marks, 80 precent of time
   if (RandomLong (1, 100) < 80)
      StripTags (szOutName);
   else
      StrTrim (szOutName);

   // sometimes switch name to lower characters
   // note: since we're using russian names written in english
   // we reduse this shit to 6 precent
   if (RandomLong (1, 100) <= 6)
   {
      for (int i = 0; i < static_cast <int> (strlen (szOutName)); i++)
         szOutName[i] = tolower (szOutName[i]); // to lower case
   }
   return (&szOutName[0]); // return terminated string
}

void HumanizeChat (char *szBuffer)
{
   // this function humanize chat string to be more handwritten
      
   int iLength = strlen (szBuffer); // get length of string
   int i = 0;

   // sometimes switch text to lowercase
   // note: since we're using russian chat written in english
   // we reduse this shit to 4 precent
   if (RandomLong (1, 100) <= 4)
   {
      for (i = 0; i < iLength; i++)
         szBuffer[i] = tolower (szBuffer[i]); // switch to lowercase
   }

   if (iLength > 15)
   {
      // "length / 2" precent of time drop a character
      if (RandomLong (1, 100) < (iLength / 2))
      {
         int iPos = RandomLong ((iLength / 8), iLength - (iLength / 8)); // chose random position in string

         for (i = iPos; i < iLength - 1; i++)
            szBuffer[i] = szBuffer[i + 1]; // overwirte the buffer with stripped string
         
         szBuffer[i] = '\0'; // terminate strng;
         iLength--; // update new string length
      }

      // "length" / 4 precent of time swap character
      if (RandomLong (1, 100) < (iLength / 4))
      {
         int iPos = RandomLong ((iLength / 8), ((3 * iLength) / 8)); // coohse random position in string
         char ch = szBuffer[iPos]; // swap characters

         szBuffer[iPos] = szBuffer[iPos + 1];
         szBuffer[iPos + 1] = ch;
      }
   }
   szBuffer[iLength] = 0; // terminate string
}

void CBot::PrepareChatMessage (char *szText)
{
   // this function parses messages from the botchat, replaces keywords and converts names into a more human style

   if (IsNullString (szText))
      return;

   int iLen;
   memset (&m_szMiscStrings, 0, sizeof (m_szMiscStrings));

   char *szTextStart = szText;
   char *szPattern = szText;

   edict_t *pentTalk = NULL;

   while (szPattern)
   {
      // all replacement placeholders start with a %
      szPattern = strstr (szTextStart,"%");
      if (szPattern)
      {
         iLen = szPattern - szTextStart;

         if (iLen > 0)
            strncpy (m_szMiscStrings, szTextStart, iLen);

         szPattern++;

         // player with most frags?
         if (*szPattern == 'f')
         {
            int iHighestFrags = -9000; // just pick some start value
            int iCurrFrags;
            int iIndex = 0;

            for (int i = 0; i < MaxClients (); i++)
            {
               if (!(g_rgkClients[i].iFlags & CFLAG_USED) || (g_rgkClients[i].pentEdict == GetEntity ()))
                  continue;

               iCurrFrags = g_rgkClients[i].pentEdict->v.frags;

               if (iCurrFrags > iHighestFrags)
               {
                  iHighestFrags = iCurrFrags;
                  iIndex = i;
               }
            }

            pentTalk = g_rgkClients[iIndex].pentEdict;

            if (!FNullEnt (pentTalk))
               strcat (m_szMiscStrings, HumanizeName (const_cast <char *> (STRING (pentTalk->v.netname))));
         }
         // mapname?
         else if (*szPattern == 'm')
            strcat (m_szMiscStrings, GetMapName ());
         // roundtime?
         else if (*szPattern == 'r')
         {
            int iTime = static_cast <int> (g_fTimeRoundEnd - WorldTime ());
            strcat (m_szMiscStrings, VarArgs ("%02d:%02d", iTime / 60, iTime % 60));
         }
         // chat reply?
         else if (*szPattern == 's')
         {
            pentTalk = INDEXENT (m_sSaytextBuffer.iEntityIndex);

            if (!FNullEnt (pentTalk))
               strcat (m_szMiscStrings, HumanizeName (const_cast <char *> (STRING (pentTalk->v.netname))));
         }
         // teammate alive?
         else if (*szPattern == 't')
         {
            int iTeam = GetTeam (GetEntity ());
            int i;

            for (i = 0; i < MaxClients (); i++)
            {
               if (!(g_rgkClients[i].iFlags & CFLAG_USED) || !(g_rgkClients[i].iFlags & CFLAG_ALIVE) || (g_rgkClients[i].iTeam != iTeam) || (g_rgkClients[i].pentEdict == GetEntity ()))
                  continue;

               break;
            }

            if (i < MaxClients ())
            {
               if (!FNullEnt (pev->dmg_inflictor) && (GetTeam (GetEntity ()) == GetTeam (pev->dmg_inflictor)))
                  pentTalk = pev->dmg_inflictor;
               else
                  pentTalk = g_rgkClients[i].pentEdict;

               if (!FNullEnt (pentTalk))
                  strcat (m_szMiscStrings, HumanizeName (const_cast <char *> (STRING (pentTalk->v.netname))));
            }
            else // no teammates alive...
            {
               for (i = 0; i < MaxClients (); i++)
               {
                  if (!(g_rgkClients[i].iFlags & CFLAG_USED) || (g_rgkClients[i].iTeam != iTeam) || (g_rgkClients[i].pentEdict == GetEntity ()))
                     continue;

                  break;
               }
               if (i < MaxClients ())
               {
                  pentTalk = g_rgkClients[i].pentEdict;

                  if (!FNullEnt (pentTalk))
                     strcat (m_szMiscStrings, HumanizeName (const_cast <char *> (STRING (pentTalk->v.netname))));
               }
            }
         }
         else if (*szPattern == 'e')
         {
            int iTeam = GetTeam (GetEntity ());
            int i;

            for (i = 0; i < MaxClients (); i++)
            {
               if (!(g_rgkClients[i].iFlags & CFLAG_USED) || !(g_rgkClients[i].iFlags & CFLAG_ALIVE) || (g_rgkClients[i].iTeam == iTeam) || (g_rgkClients[i].pentEdict == GetEntity ()))
                  continue;
               break;
            }
            if (i < MaxClients ())
            {
               pentTalk = g_rgkClients[i].pentEdict;

               if (!FNullEnt (pentTalk))
                  strcat (m_szMiscStrings, HumanizeName (const_cast <char *> (STRING (pentTalk->v.netname))));
            }
            else // no teammates alive...
            {
               for (i = 0; i < MaxClients (); i++)
               {
                  if (!(g_rgkClients[i].iFlags & CFLAG_USED) || (g_rgkClients[i].iTeam == iTeam) || (g_rgkClients[i].pentEdict == GetEntity ()))
                     continue;
                  break;
               }
               if (i < MaxClients ())
               {
                  pentTalk = g_rgkClients[i].pentEdict;

                  if (!FNullEnt (pentTalk))
                     strcat (m_szMiscStrings, HumanizeName (const_cast <char *> (STRING (pentTalk->v.netname))));
               }
            }
         }
         else if (*szPattern == 'd')
         {
            if (g_iCSVersion == VERSION_CSCZ)
            {
               if (RandomLong (1, 100) < 30)
                  strcat (m_szMiscStrings, "CZ");
               else if (RandomLong (1, 100) < 80)
                  strcat (m_szMiscStrings, "KoHTpa K3");
               else 
                  strcat (m_szMiscStrings, "Condition Zero");
            }
            else if ((g_iCSVersion == VERSION_STEAM) || (g_iCSVersion == VERSION_WON))
            {
               if (RandomLong (1, 100) < 30)
                  strcat (m_szMiscStrings, "CS");
               else if (RandomLong (1, 100) < 80)
                  strcat (m_szMiscStrings, "KoHTpa");
               else 
                  strcat (m_szMiscStrings, "Counter-Strike");
            }
         }
         else if (*szPattern == 'v')
         {
            pentTalk = m_pentLastVictim;

            if (!FNullEnt (pentTalk))
               strcat (m_szMiscStrings, HumanizeName (const_cast <char *> (STRING (pentTalk->v.netname))));
         }
         szPattern++;
         szTextStart = szPattern;
      }
   }

   // let the bots make some mistakes...
   char szTempString[160];
   strncpy (szTempString, szTextStart, 159);

   HumanizeChat (szTempString);
   strcat (m_szMiscStrings, szTempString);
}

bool ChecarKeywords (char *pszMessage, char *pszReply)
{
   // this function checks is string contain keyword, and generates relpy to it

   if (IsNullString (pszMessage))
      return false;

   ITERATE_ARRAY (g_arReplyChat, i)
   {
      ITERATE_ARRAY (g_arReplyChat[i].arKeywords, j)
      {
         // check is keyword has occured in message
         if (strstr (pszMessage, g_arReplyChat[i].arKeywords[j].GetString ()) != NULL)
         {
            if (g_arReplyChat[i].kUsedReplies.Size () >= g_arReplyChat[i].arReplies.Size () / 2)
               g_arReplyChat[i].kUsedReplies.Cleanup ();

            bool bReplyUsed = false;
            const char *cszReply = g_arReplyChat[i].arReplies.Random ();

            // don't say this twice
            ITERATE_ARRAY (g_arReplyChat[i].kUsedReplies, k)
            {
               if (strstr (g_arReplyChat[i].kUsedReplies[k].GetString (), cszReply) != NULL)
                  bReplyUsed = true;
            }

            // reply not used, so use it
            if (!bReplyUsed)
            {
               strcpy (pszReply, cszReply); // update final buffer
               g_arReplyChat[i].kUsedReplies.AddItem (cszReply); //add to ignore list
               
               return true;
            }
         }
      }
   }

   // didn't find a keyword? 70% of the time use some universal reply
   if ((RandomLong (1, 100) < 70) && !g_arChatEngine[CHAT_NOKEYWORD].IsFree ())
   {
      strcpy (pszReply, g_arChatEngine[CHAT_NOKEYWORD].Random ().GetString ());
      return true;
   }
   return false;
}

bool CBot::ParseChat (char *pszReply)
{
   // this function parse chat buffer, and prepare buffer to keyword searching
      
   char szMessage[512];

   strcpy (szMessage, m_sSaytextBuffer.szSayText); // copy to safe place

   // text to uppercase for keyword parsing
   for (int i = 0; i < static_cast <int> (strlen (szMessage)); i++)
      szMessage[i] = toupper (szMessage[i]); 

   return ChecarKeywords (szMessage, pszReply);
}

bool CBot::RepliesToPlayer (void)
{
   // this function sends reply to a player
      
   if ((m_sSaytextBuffer.iEntityIndex != -1) && !IsNullString (m_sSaytextBuffer.szSayText))
   {
      char szText[256];

      // check is time to chat is good
      if (m_sSaytextBuffer.fTimeNextChat < WorldTime ())
      {
         if (RandomLong (1, 100) < (m_sSaytextBuffer.cChatProbability + RandomLong (2, 10)) && ParseChat (reinterpret_cast <char *> (&szText)))
         {
            PrepareChatMessage (szText);
            PushMessageQueue (MESSAGE_SAY);

            m_sSaytextBuffer.iEntityIndex = -1;
            m_sSaytextBuffer.szSayText[0] = 0x0;
            m_sSaytextBuffer.fTimeNextChat = WorldTime () + m_sSaytextBuffer.fChatDelay;

            return true;
         }
         m_sSaytextBuffer.iEntityIndex = -1;
         m_sSaytextBuffer.szSayText[0] = 0x0;
      }
   }
   return false;
}

void CBot::SayText (const char *cszText)
{
   // this function prints saytext message to all players
      
   if (IsNullString (cszText))
      return;

   char szBotName[80];
   char szMessage[256];
    
   strcpy (szBotName, STRING (pev->netname));

   for (int i = 0; i < MaxClients (); i++)
   {
      if (!(g_rgkClients[i].iFlags & CFLAG_USED) || (g_rgkClients[i].pentEdict == GetEntity ()))
         continue;

      if (IsAlive (GetEntity ()))
         sprintf (szMessage, "%c%s :  %s\n", 0x02, szBotName, cszText);
      else
         sprintf (szMessage, "%c*DEAD* %c%s%c :  %s\n", 0x01, 0x03, szBotName, 0x01, cszText);
         
      MESSAGE_BEGIN (MSG_ONE, GetUserMsgId ("SayText"), NULL, g_rgkClients[i].pentEdict);
          WRITE_BYTE (GetIndex ());
          WRITE_STRING (szMessage);
      MESSAGE_END ();
   }
}

void CBot::TeamSayText (const char *cszText)
{
   // this function prints saytext message only for teammates

   if (IsNullString (cszText))
      return;

   char szBotName[80];
   char szMessage[256];
   char szBotTeam[80];

   int iBotTeam = GetTeam (GetEntity ());

   if (iBotTeam == TEAM_TERRORIST)
      strcpy (szBotTeam, "(Terrorist)");
   else if (iBotTeam == TEAM_CT)
      strcpy (szBotTeam, "(Counter-Terrorist)");

   strcpy (szBotName, STRING (pev->netname));

   for (int i = 0; i < MaxClients (); i++)
   {
      if (!(g_rgkClients[i].iFlags & CFLAG_USED) || (g_rgkClients[i].iTeam != iBotTeam) || (g_rgkClients[i].pentEdict == GetEntity ()))
         continue;
      
      if (IsAlive (GetEntity ()))
         sprintf (szMessage, "%c%s %c%s%c :  %s\n", 0x01, szBotTeam, 0x03, szBotName, 0x01, cszText);
      else
         sprintf (szMessage, "%c*DEAD*%s %c%s%c :  %s\n", 0x01, szBotTeam, 0x03, szBotName, 0x01, cszText);

      MESSAGE_BEGIN (MSG_ONE, GetUserMsgId ("SayText"), NULL, g_rgkClients[i].pentEdict);
          WRITE_BYTE (GetIndex ());
          WRITE_STRING (szMessage);
      MESSAGE_END ();
   }
}