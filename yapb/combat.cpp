//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#include "yapb.h"

int CBot::GetNearbyFriendsNearPosition (Vector vPosition, int iRadius)
{
   int iCount = 0, iTeam = GetTeam (GetEntity ());

   for (int i = 0; i < MaxClients (); i++)
   {
      if (!(g_rgkClients[i].iFlags & CFLAG_USED) || !(g_rgkClients[i].iFlags & CFLAG_ALIVE) || (g_rgkClients[i].iTeam != iTeam) || (g_rgkClients[i].pentEdict == GetEntity ()))
         continue;

      if (LengthSquared (g_rgkClients[i].vOrigin - vPosition) < static_cast <float>( iRadius * iRadius))
         iCount++;
   }
   return iCount;
}

int CBot::GetNearbyEnemiesNearPosition (Vector vPosition, int iRadius)
{
   int iCount = 0, iTeam = GetTeam (GetEntity ());

   for (int i = 0; i < MaxClients (); i++)
   {
      if (!(g_rgkClients[i].iFlags & CFLAG_USED) || !(g_rgkClients[i].iFlags & CFLAG_ALIVE) || (g_rgkClients[i].iTeam == iTeam))
         continue;

      if (LengthSquared (g_rgkClients[i].vOrigin - vPosition) < static_cast <float> (iRadius * iRadius))
         iCount++;
   }
   return iCount;
}

bool CBot::LookupEnemy (void)
{
   // this function tries to find the best suitable enemy for the bot

   m_ucVisibility = 0;
   m_vEnemy = NULLVEC;

   // do not search for enemies while we're blinded or on the ladder (since most of time we can't hurt anyone from the ladder)
   if ((m_fBlindTime > WorldTime ()) || IsOnLadder ())
      return false;

   edict_t *pentPlayer, *pentNewEnemy = NULL;
   edict_t *pentMonster = NULL;

   float fNearestDistance = m_fViewDistance;
   int i, iTeam = GetTeam (GetEntity ());

   // setup potentially visible set for this bot
   Vector vOrg = EyePosition ();

   if (pev->flags & FL_DUCKING) 
      vOrg = vOrg + (VEC_HULL_MIN - VEC_DUCK_HULL_MIN);

   byte *ucPVS = ENGINE_SET_PVS (reinterpret_cast <float *> (&vOrg));

   // clear suspected flag
   m_iStates &= ~STATE_SUSPECTENEMY;

   if (!FNullEnt (m_pentEnemy) && (m_fEnemyUpdateTime > WorldTime ()))
   {
      pentPlayer = m_pentEnemy;

      // is player is alive 
      if (IsAlive (pentPlayer) && IsEnemyViewable (pentPlayer))
         pentNewEnemy = pentPlayer;
   }

   if (FNullEnt (pentNewEnemy))
   {
      // search the world for monsters...
      while (!FNullEnt (pentMonster = FIND_ENTITY_IN_SPHERE (pentMonster, pev->origin, 4096.0)))
      {
         if (!(pentMonster->v.flags & FL_MONSTER) || !IsAlive (pentMonster) || (strcmp (STRING (pentMonster->v.classname), "hostage_entity") == 0))
            continue; // discard anything that is not a monster

         Vector vEnd = pentMonster->v.origin + pentMonster->v.view_ofs;

         // see if bot can't see the player...
         if (!IsInViewCone (vEnd) || !IsVisible (vEnd, GetEntity ()))
            continue;

         float fDistance = (pentMonster->v.origin - pev->origin).Length ();

         if (fDistance < fNearestDistance)
         {
            fNearestDistance = fDistance;
            pentNewEnemy = pentMonster;

            m_pentTargetEnt = NULL;  // don't follow user when enemy found
         }
      }
      m_fEnemyUpdateTime = WorldTime () + 0.5;

      // search the world for players...
      for (i = 0; i < MaxClients (); i++)
      {
         if (!(g_rgkClients[i].iFlags & CFLAG_USED) || !(g_rgkClients[i].iFlags & CFLAG_ALIVE) || (g_rgkClients[i].iTeam == iTeam) || (g_rgkClients[i].pentEdict == GetEntity ()))
            continue;

         pentPlayer = g_rgkClients[i].pentEdict;

         // let the engine check if this player is potentially visible
         if (!ENGINE_CHECK_VISIBILITY (pentPlayer, ucPVS))
            continue;

         // see if bot can see the player...
         if (IsEnemyViewable (pentPlayer))
         {
            float fDistance = (pentPlayer->v.origin - pev->origin).Length ();

            if (fDistance < fNearestDistance)
            {
               fNearestDistance = fDistance;
               pentNewEnemy = pentPlayer;

               // aim VIP first on AS maps...
               if ((g_iMapType & MAP_AS) && *(INFOKEY_VALUE (GET_INFOKEYBUFFER (pentPlayer), "model")) == 'v')
                  break;
            }
         }
      }
   }

   if (IsValidPlayer (pentNewEnemy))
   {
      g_bBotsCanPause = true;
      m_iAimFlags |= AIM_ENEMY;

      if (pentNewEnemy == m_pentEnemy)
      {
         // if enemy is still visible and in field of view, keep it keep track of when we last saw an enemy
         m_fSeeEnemyTime = WorldTime ();
         
         // zero out reaction time
         m_fActualReactionTime = 0.0;
         m_pentLastEnemy = pentNewEnemy;
         m_vLastEnemyOrigin = pentNewEnemy->v.origin;

         return true;
      }
      else
      {
         if ((m_fSeeEnemyTime + 3.0 < WorldTime ()) && (pev->weapons & (1 << WEAPON_C4) || HasHostage () || !FNullEnt (m_pentTargetEnt)))
            RadioMessage (RADIO_ENEMYSPOTTED);

         m_pentTargetEnt = NULL; // stop following when we see an enemy...

         if (RandomLong (0, 100) < m_iSkill)
            m_fEnemySurpriseTime = WorldTime () + (m_fActualReactionTime / 3);
         else
            m_fEnemySurpriseTime = WorldTime () + m_fActualReactionTime;

         DebugMsg ("attacking enemy: %s (reaction time: %.1f (+ delay), surprise time: %.1f)", STRING (pentNewEnemy->v.netname), m_fActualReactionTime, m_fEnemySurpriseTime - WorldTime ());

         // zero out reaction time
         m_fActualReactionTime = 0.0;
         m_pentEnemy = pentNewEnemy;
         m_pentLastEnemy = pentNewEnemy;
         m_vLastEnemyOrigin = pentNewEnemy->v.origin;
         m_fEnemyReachableTimer = 0.0;

         // keep track of when we last saw an enemy
         m_fSeeEnemyTime = WorldTime ();

         // now alarm all teammates who see this bot & don't have an actual enemy of the bots enemy
         // should simulate human players seeing a teammate firing
         CBot *pFriendlyBot;

         for (int j = 0; j < MaxClients (); j++)
         {
            if (!(g_rgkClients[j].iFlags & CFLAG_USED) || !(g_rgkClients[j].iFlags & CFLAG_ALIVE) || (g_rgkClients[j].iTeam != iTeam) || (g_rgkClients[j].pentEdict == GetEntity ()))
               continue;

            pFriendlyBot = g_pBotManager->GetBot (g_rgkClients[j].pentEdict);

            if (pFriendlyBot != NULL)
            {
               if ((pFriendlyBot->m_fSeeEnemyTime + 2.0 < WorldTime ()) || FNullEnt (pFriendlyBot->m_pentLastEnemy))
               {
                  if (IsVisible (pev->origin, ENT (pFriendlyBot->pev)))
                  {
                     pFriendlyBot->m_pentLastEnemy = pentNewEnemy;
                     pFriendlyBot->m_vLastEnemyOrigin = m_vLastEnemyOrigin;
                     pFriendlyBot->m_fSeeEnemyTime = WorldTime ();
                  }
               }
            }
         }
         return true;
      }
   }
   else if (!FNullEnt (m_pentEnemy))
   {
      pentNewEnemy = m_pentEnemy;
      m_pentLastEnemy = pentNewEnemy;

      if (!IsAlive (pentNewEnemy))
      {
         m_pentEnemy = NULL;

         // shoot at dying players if no new enemy to give some more human-like illusion
         if (m_fSeeEnemyTime + 0.1 > WorldTime ())
         {
            if (!UsesSniper ())
            {
               m_fShootAtDeadTime = WorldTime () + 0.2;
               m_fActualReactionTime = 0.0;
               m_iStates |= STATE_SUSPECTENEMY;

               return true;
            }
            return false;
         }
         else if (m_fShootAtDeadTime > WorldTime ())
         {
            m_fActualReactionTime = 0.0;
            m_iStates |= STATE_SUSPECTENEMY;

            return true;
         }
         return false;
      }

      // if no enemy visible check if last one shootable thru wall 
      if (g_rgpcvBotCVar[CVAR_THRUWALLS]->GetBool () && (RandomLong (1, 100) < g_rgkSkillTab[m_iSkill / 20].iSeenShootThruProb))
      {
         if (IsShootableThruObstacle (pentNewEnemy->v.origin))
         {
            m_fSeeEnemyTime = WorldTime ();

            m_iStates |= STATE_SUSPECTENEMY;
            m_iAimFlags |= AIM_LASTENEMY;
            m_pentLastEnemy = pentNewEnemy;
            m_vLastEnemyOrigin = pentNewEnemy->v.origin;

            DebugMsg ("shooting through wall (%s)", STRING (pentNewEnemy->v.netname));
         
            return true;
         }
      }
   }

   // check if bots should reload...
   if (((m_iAimFlags <= AIM_PREDICTPATH) && (m_fSeeEnemyTime + 4.0 < WorldTime ()) &&  !(m_iStates & (STATE_SEEINGENEMY | STATE_HEARINGENEMY)) && FNullEnt (m_pentLastEnemy) && FNullEnt (m_pentEnemy) && (CurrentTask ()->iTask != TASK_SHOOTBREAKABLE) && (CurrentTask ()->iTask != TASK_PLANTBOMB) && (CurrentTask ()->iTask != TASK_DEFUSEBOMB)) || g_bRoundEnded)
   {
      if (!m_iReloadState)
         m_iReloadState = RELOAD_PRIMARY;
   }

   // is the bot using a sniper rifle or a zoomable rifle?
   if ((UsesSniper () || UsesZoomableRifle ()) && (m_fZoomCheckTime + 1.0 < WorldTime ()))
   {
      if (pev->fov < 90) // let the bot zoom out
         pev->button |= IN_ATTACK2;
      else
         m_fZoomCheckTime = 0.0;
   }
   return false;
}

Vector CBot::AimPosition (void)
{
   // the purpose of this function, is to make bot aiming not so ideal. it's mutate m_vEnemy enemy vector
   // returned from visibility check function.

   float fZComponentUpdate = 17.0;
   float fDistance = (m_pentEnemy->v.origin - pev->origin).Length ();

   // get enemy position initially
   Vector vTarget = m_pentEnemy->v.origin;

   // this is cheating, but we're in hardcore mode :)
   if (g_rgpcvBotCVar[CVAR_HARCOREMODE]->GetBool ())
      fZComponentUpdate = (UsesSniper () ? 3.32 : UsesPistol () ? 5.0 : (fDistance < 300 ? 3.28 : 5.18)) + fZComponentUpdate;

   // do not aim at head, at long distance (only if not using sniper weapon)
   if ((m_ucVisibility & VISIBLE_BODY) && !UsesSniper () && ((vTarget - pev->origin).Length () > 1800.0))
      m_ucVisibility &= ~VISIBLE_HEAD;

   // if we only suspect an enemy behind a wall take the worst skill
   if ((m_iStates & STATE_SUSPECTENEMY) && !(m_iStates & STATE_SEEINGENEMY))
      vTarget = vTarget + Vector (RandomFloat (m_pentEnemy->v.mins.x, m_pentEnemy->v.maxs.x), RandomFloat (m_pentEnemy->v.mins.y, m_pentEnemy->v.maxs.y), RandomFloat (m_pentEnemy->v.mins.z, m_pentEnemy->v.maxs.z));
   else
   {
      // now take in account different parts of enemy body
      if (m_ucVisibility & (VISIBLE_HEAD | VISIBLE_BODY)) // visible head & body
      {
         // now check is our skill match to aim at head, else aim at enemy body
         if ((RandomLong (1, 100) < g_rgkSkillTab[m_iSkill / 20].iHeadshotFrequency) || UsesPistol ())
            vTarget = vTarget + Vector (0, 0, fZComponentUpdate);
         else
            vTarget = vTarget;
      }
      else if (m_ucVisibility & VISIBLE_HEAD) // visible only head
         vTarget = vTarget + Vector (0, 0, fZComponentUpdate);
      else if (m_ucVisibility & VISIBLE_BODY) // visible only body
         vTarget = vTarget;
      else if (m_ucVisibility & VISIBLE_OTHER) // random part of body is visible
         vTarget = m_vEnemy; // ideal position of body part (maybe really small)
      else // something goes wrong, use last enemy origin
         vTarget = m_vLastEnemyOrigin;

      m_vLastEnemyOrigin = vTarget;
   }
   return m_vEnemy = vTarget;
}

bool CBot::IsFriendInLineOfFire (float fDistance)
{
   // bot can't hurt teammates, if friendlyfire is not enabled...
   if (!g_rgpcvBotCVar[CVAR_FRIENDLYFIRE]->GetBool ())
      return false;

   // search the worl for friends...
   for (int i = 0; i < MaxClients (); i++)
   {
      if (!(g_rgkClients[i].iFlags & CFLAG_USED) || !(g_rgkClients[i].iFlags & CFLAG_ALIVE) || (g_rgkClients[i].iTeam != GetTeam (GetEntity ())) || (g_rgkClients[i].pentEdict == GetEntity ()))
         continue;

      // check if we can hurt player...
      if ((GetShootingConeDeviation (GetEntity (), &g_rgkClients[i].pentEdict->v.origin) > 0.95) && (LengthSquared (g_rgkClients[i].pentEdict->v.origin) < fDistance * fDistance))
         return true;
   }
   return false;
}

bool CBot::IsShootableThruObstacle (Vector vDest)
{
   // Returns if enemy can be shoot through some obstacle

   if (!IsWeaponShootingThroughWall (m_iCurrentWeapon))
      return false;

   Vector vSrc = EyePosition ();
   Vector vDir = (vDest - vSrc).Normalize ();  // 1 unit long
   Vector vPoint = NULLVEC;

   int iThickness = 0;
   int iHits = 0;

   TraceResult tr;
   TraceLine (vSrc, vDest, true, true, GetEntity (), &tr);

   while ((tr.flFraction != 1.0) && (iHits < 3))
   {
      iHits++;
      iThickness++;
      vPoint = tr.vecEndPos + vDir;
      
      while ((POINT_CONTENTS (vPoint) == CONTENTS_SOLID) && (iThickness < 74))
      {
         vPoint = vPoint + vDir;
         iThickness++;
      }
      TraceLine (vPoint, vDest, true, true, GetEntity (), &tr);
   }

   if ((iHits < 3) && (iThickness < 71))
   {
      if (LengthSquared (vDest - vPoint) < 12544)
         return true;
   }
   return false;
}

bool CBot::DoFirePause (float fDistance)
{
   // returns true if bot needs to pause between firing to compensate for punchangle & weapon spread

   if (UsesSniper ())
      return false;

   if (m_fTimeFirePause > WorldTime ())
   {
      m_fShootTime = WorldTime ();
      return true;
   }

   float fAngle = (fabsf (pev->punchangle.y) + fabsf (pev->punchangle.x)) * PI / 360.0;

   if ((tanf (fAngle) * fDistance) > (20 + (100 - m_iSkill) / 5))
   {
      if (m_fTimeFirePause < (WorldTime () - 0.4))
         m_fTimeFirePause = WorldTime () + RandomFloat (0.4, 0.4 + 1.2 * (100 - m_iSkill) / 100.0); 
      
      DebugMsg ("compensating weapon recoil with %.1f secs.", m_fTimeFirePause - WorldTime ());

      m_fShootTime = WorldTime ();
      return true;
   }
   return false;
}

bool CBot::FireWeapon (Vector vEnemy)
{
   // this function will return true if weapon was fired, false otherwise
   float fDistance = vEnemy.Length (); // how far away is the enemy?

   if (m_bUsingGrenade || IsFriendInLineOfFire (fDistance))
      return false;

   tWeaponSelect *pSelect = &g_rgkWeaponSelect[0];
   edict_t *pentEnemy = m_pentEnemy;

   int iSelectId = WEAPON_KNIFE, iSelectIndex = 0, iChosenWeaponIndex = 0;
   int iWeapons = pev->weapons;

   // if jason mode use knife only
   if (g_rgpcvBotCVar[CVAR_KNIFEMODE]->GetBool ())
      goto WeaponSelectEnd;

   // use knife if near and good skill (l33t dude!)
   if ((m_iSkill > 80) && !FNullEnt (pentEnemy) && (fDistance < 80.0) && (pev->health > 80) && (pev->health >= pentEnemy->v.health) && !IsGroupOfEnemies (pev->origin))
      goto WeaponSelectEnd;

   // loop through all the weapons until terminator is found...
   while (pSelect[iSelectIndex].iId)
   {
      // is the bot carrying this weapon?
      if (iWeapons & (1 << pSelect[iSelectIndex].iId))
      {
         // is enough ammo available to fire AND check is better to use pistol in our current situation...
         if ((m_rgAmmoInClip[pSelect[iSelectIndex].iId] > 0) && !IsWeaponBadInDistance (iSelectIndex, fDistance))
            iChosenWeaponIndex = iSelectIndex;
      }
      iSelectIndex++;
   }
   iSelectId = pSelect[iChosenWeaponIndex].iId;

   // if no available weapon...
   if (iChosenWeaponIndex == 0)
   {
      iSelectIndex = 0;

      // loop through all the weapons until terminator is found...
      while (pSelect[iSelectIndex].iId)
      {
         int iId = pSelect[iSelectIndex].iId;

         // is the bot carrying this weapon?
         if (iWeapons & (1 << iId))
         {
            if ((g_rgkWeaponDefs[iId].iAmmo1 != -1) && (m_rgAmmo[g_rgkWeaponDefs[iId].iAmmo1] >= pSelect[iSelectIndex].iMinPrimaryAmmo))
            {
               // available ammo found, reload weapon
               if ((m_iReloadState == RELOAD_NONE) || (m_fReloadCheckTime > WorldTime ()))
               {
                  m_bIsReloading = true;
                  m_iReloadState = RELOAD_PRIMARY;
                  m_fReloadCheckTime = WorldTime ();

                  RadioMessage (RADIO_NEEDBACKUP);
               }
               return false;
            }
         }
         iSelectIndex++;  
      }
      iSelectId = WEAPON_KNIFE; // no available ammo, use knife!
   }

WeaponSelectEnd:
   // we want to fire weapon, don't reload now
   if (!m_bIsReloading)
   {
      m_iReloadState = RELOAD_NONE;
      m_fReloadCheckTime = WorldTime () + 3.0;
   }

   // select this weapon if it isn't already selected
   if (m_iCurrentWeapon != iSelectId)
   {
      SelectWeaponByName (g_rgkWeaponDefs[iSelectId].szClassname);

      DebugMsg ("changing weapon (to: %s)", g_rgkWeaponDefs[iSelectId].szClassname);

      // reset burst fire variables
      m_fTimeFirePause = 0.0;
      m_fTimeLastFired = 0.0;

      return false;
   }

   if (pSelect[iChosenWeaponIndex].iId != iSelectId)
   {
      iChosenWeaponIndex = 0;

      // loop through all the weapons until terminator is found...
      while (pSelect[iChosenWeaponIndex].iId)
      {
         if (pSelect[iChosenWeaponIndex].iId == iSelectId)
            break;

         iChosenWeaponIndex++;
      }
   }

   // if we're have a glock or famas vary burst fire mode
   if ((m_iCurrentWeapon == WEAPON_GLOCK18) || (m_iCurrentWeapon == WEAPON_FAMAS))
   {
      // if we own glock & enemy is far disable burstmode, else enable it
      if ((m_iCurrentWeapon == WEAPON_GLOCK18) && (((fDistance < 250.0) && (m_iWeaponBurstMode == BMSL_DISABLED)) || ((fDistance >= 400.0) && (m_iWeaponBurstMode == BMSL_ENABLED))))
         ChangeBurstMode ();

      // ... or if we own famas & enemy is close disable burst mode, else enable it
      if ((m_iCurrentWeapon == WEAPON_FAMAS) && (((fDistance < 300.0) && (m_iWeaponBurstMode == BMSL_ENABLED)) || ((fDistance >= 500.0) && (m_iWeaponBurstMode == BMSL_DISABLED))))
         ChangeBurstMode ();
   }

   if (HasShield () && (m_fShieldCheckTime < WorldTime ()) && (CurrentTask ()->iTask != TASK_CAMP)) // better shieldgun usage
   {
      if ((fDistance > 550) && !IsShieldDrawn ())
         pev->button |= IN_ATTACK2; // draw the shield
      else if (IsShieldDrawn () || (!FNullEnt (m_pentEnemy) && (m_pentEnemy->v.button & IN_RELOAD)))
         pev->button |= IN_ATTACK2; // draw out the shield

      m_fShieldCheckTime = WorldTime () + 1.0;
   }

   if (UsesSniper () && (m_fZoomCheckTime < WorldTime ())) // is the bot holding a sniper rifle?
   {
      if ((fDistance > 1500) && (pev->fov >= 40)) // should the bot switch to the long-range zoom?
         pev->button |= IN_ATTACK2;

      else if ((fDistance > 150) && (pev->fov >= 90)) // else should the bot switch to the close-range zoom ?
         pev->button |= IN_ATTACK2;

      else if ((fDistance <= 150) && (pev->fov < 90)) // else should the bot restore the normal view ?
         pev->button |= IN_ATTACK2;

      m_fZoomCheckTime = WorldTime ();
      
      if (!FNullEnt (m_pentEnemy) && ((pev->velocity.x != 0) || (pev->velocity.y != 0) || (pev->velocity.z != 0))&& ((pev->basevelocity.x != 0) || (pev->basevelocity.y != 0) || (pev->basevelocity.z != 0)))
      {
         m_fMoveSpeed = 0.0;
         m_fSideMoveSpeed = 0.0;
         m_fWptTimeset = WorldTime ();
      }
   }
   else if (UsesZoomableRifle () && (m_fZoomCheckTime < WorldTime ())) // else is the bot holding a zoomable rifle?
   {
      if ((fDistance > 800) && (pev->fov >= 90)) // should the bot switch to zoomed mode?
         pev->button |= IN_ATTACK2;

      else if ((fDistance <= 800) && (pev->fov < 90)) // else should the bot restore the normal view?
         pev->button |= IN_ATTACK2;

      m_fZoomCheckTime = WorldTime ();
   }
   
   const float rgfBaseDelay[NUM_WEAPONS] = {0.0, 0.09, 0.08, 0.05, 0.04, 0.06, 0.0, 0.86, 0.15, 0.10, 0.05, 0.10, 0.06, 0.15, 0.09, 0.11, 0.08, 0.11, 0.23, 1.45, 0.25, 0.25, 0.1, 0.10, 0.09, 0.0};
   const float rgfMinDelay[6] = {0.0, 0.1, 0.2, 0.3, 0.4, 0.6};
   const float rgfMaxDelay[6] = {0.1, 0.2, 0.3, 0.4, 0.5, 0.7};

   // need to care for burst fire?
   if ((fDistance < 152.0) || (m_fBlindTime > WorldTime ()))
   {
      if (iSelectId == WEAPON_KNIFE)
      {
         if (fDistance < 64.0)
         {
            if ((RandomLong (1, 100) < 30) || HasShield ())
               pev->button |= IN_ATTACK; // use primary attack
            else
               pev->button |= IN_ATTACK2; // use secondary attack
         }
      }
      else
      {
         if (pSelect[iChosenWeaponIndex].bPrimaryFireHold) // if automatic weapon, just press attack
            pev->button |= IN_ATTACK;
         else // if not, toggle buttons
         {
            if ((pev->oldbuttons & IN_ATTACK) == 0)
               pev->button |= IN_ATTACK;
         }
      }
      m_fShootTime = WorldTime ();
   }
   else
   {
      if (DoFirePause (fDistance))
         return false;

      // don't attack with knife over long distance
      if (iSelectId == WEAPON_KNIFE)
      {
         m_fShootTime = WorldTime ();
         return false;
      }

      if (pSelect[iChosenWeaponIndex].bPrimaryFireHold)
      {
         m_fShootTime = WorldTime ();
         m_fZoomCheckTime = WorldTime ();
         
         pev->button |= IN_ATTACK;  // use primary attack
      }
      else
      {
         pev->button |= IN_ATTACK;  // use primary attack

         m_fShootTime = WorldTime () + rgfBaseDelay[iChosenWeaponIndex] + RandomFloat (rgfMinDelay[abs ((m_iSkill / 20) - 5)], rgfMaxDelay[abs ((m_iSkill / 20) - 5)]);
         m_fZoomCheckTime = WorldTime ();
      }
   }
   return true; 
}

bool CBot::CheckCorridorAround (void)
{
   // this function checks is bot currently in corridor, function code is courtesy of KWo.

   if ((m_rgiPrevWptIndex[0] < 0) || (m_rgiPrevWptIndex[0] >= g_iNumWaypoints))
      return false;

   Vector vSource = m_vWptOrigin + pev->view_ofs;
   Vector vMoveDirection = (m_vWptOrigin - g_pWaypoint->GetPath (m_rgiPrevWptIndex[0])->vOrigin).Normalize () * 150.0;
   Vector vDestination = m_vWptOrigin + pev->view_ofs + vMoveDirection;

   TraceResult tr1, tr2;
   TraceLine (vSource, vDestination, true, GetEntity (), &tr1);

   // fire a traceline forward to check if we have some space in front of us
   if ((tr1.pHit != g_pentWorldEdict) || (tr1.flFraction < 1.0))
      return false;

   vSource = g_pWaypoint->GetPath (m_rgiPrevWptIndex[0])->vOrigin + pev->view_ofs;
   vDestination = vSource - vMoveDirection;
   TraceLine (vSource, vDestination, true, GetEntity (), &tr1);

   // fire a traceline forward to check if we have some space in front of us
   if ((tr1.pHit != g_pentWorldEdict) || (tr1.flFraction < 1.0))
      return false;

   Vector vMoveAngle = VecToAngles (vMoveDirection);
   MakeVectors (vMoveAngle);

   vSource = g_pWaypoint->GetPath (m_rgiPrevWptIndex[0])->vOrigin + pev->view_ofs;
   Vector vLeft = vSource + g_pGlobals->v_right * -180;
   Vector vRight = vSource + g_pGlobals->v_right * 180;

   TraceLine (vSource, vLeft, true, GetEntity (), &tr1);
   TraceLine (vSource, vRight, true, GetEntity (), &tr2);

   // if both sides of trace origin are limited by some obstacles - probably we aren't in the free area
   if ((tr1.flFraction < 1.0) && (tr2.flFraction < 1.0))
      return false;

   vSource = m_vWptOrigin + pev->view_ofs + vMoveDirection;
   vLeft = vSource + g_pGlobals->v_right * -250;
   vRight = vSource + g_pGlobals->v_right * 250;

   // now we need to fire 2 tracelines from forward position to the left and to the right to check if they are blocked both - it may mean we are in some corridor
   TraceLine (vSource, vLeft, true, GetEntity (), &tr1);
   TraceLine (vSource, vRight, true, GetEntity (), &tr2);

   if ((tr1.pHit == g_pentWorldEdict) && (tr1.flFraction < 0.5) && (tr2.pHit == g_pentWorldEdict) && (tr2.flFraction < 0.5))
      return true;

   return false;
}

bool CBot::IsWeaponBadInDistance (int iWeaponIndex, float fDistance)
{
   // this function checks, is it better to use pistol instead of current primary weapon
   // to attack our enemy, since current weapon is not very good in this situation.

   int iWeaponId = g_rgkWeaponSelect[iWeaponIndex].iId;

   // check is ammo available for secondary weapon
   if (m_rgAmmoInClip[g_rgkWeaponSelect[GetBestSecondaryWeaponCarried ()].iId] >= 1)
      return false;

   // better use pistol in short range distances, when using sniper weapons
   if (((iWeaponId == WEAPON_SCOUT) || (iWeaponId == WEAPON_AWP) || (iWeaponId == WEAPON_G3SG1) || (iWeaponId == WEAPON_SG550)) && (fDistance < 300.0))
      return true;

   // shotguns is too inaccurate at long distances, so weapon is bad
   if (((iWeaponId == WEAPON_M3) || (iWeaponId == WEAPON_XM1014)) && (fDistance > 750.0))
      return true;

   return false;
}

void CBot::FocusEnemy (void)
{
   // aim for the head and/or body
   Vector vEnemy = AimPosition ();
   m_vLookAt = vEnemy;

   if (m_fEnemySurpriseTime > WorldTime ())
      return;

   vEnemy = (vEnemy - GetGunPosition ()).IgnoreZComponent ();

   float fDistance = vEnemy.Length ();  // how far away is the enemy scum?

   if (fDistance < 128)
   {
      if (m_iCurrentWeapon == WEAPON_KNIFE)
      {
         if (fDistance < 80.0)
            m_bWantsToFire = true;
      }
      else
         m_bWantsToFire = true; 
   }
   else
   {
      if (m_iCurrentWeapon == WEAPON_KNIFE)
         m_bWantsToFire = true;
      else
      {
         float fDot = GetShootingConeDeviation (GetEntity (), &m_vEnemy);
         
         if (fDot < 0.90)
            m_bWantsToFire = false;
         else
         {
            float fEnemyDot = GetShootingConeDeviation (m_pentEnemy, &pev->origin);
            
            // enemy faces bot?
            if (fEnemyDot >= 0.90)
               m_bWantsToFire = true;
            else
            {
               if (fDot > 0.99)
                  m_bWantsToFire = true;
               else
                  m_bWantsToFire = false;
            }
         }
      }
   }
   // don't shoot to enemy if enemy with shield & facing us, we can't hurt it
   if (IsEnemyProtectedByShield ())
      m_bWantsToFire = false;
}

void CBot::CombatFight (void)
{
   // no enemy? no need to do strafing
   if (FNullEnt (m_pentEnemy))
      return;

   Vector vEnemy = m_vLookAt;
   TraceResult tr;
   
   m_vDestOrigin = m_pentEnemy->v.origin;
   vEnemy = (vEnemy - EyePosition ()).IgnoreZComponent (); // ignore z component (up & down)
   
   float fDistance = vEnemy.Length ();  // how far away is the enemy scum?

   if (UsesSniper () || (UsesZoomableRifle () && (pev->fov < 90))) // fix for snipers
   {
      m_fMoveSpeed = 0.0;
      m_fSideMoveSpeed = 0.0;

      if (fDistance > 800)
         m_fDuckTime = WorldTime () + 0.3;

      // don't allow bot to jump
      pev->button &= ~IN_JUMP;

      return;
   }

   if (m_fTimeWaypointMove + g_fTimeFrameInterval < WorldTime ())
   {
      int iApproach;

      if ((m_iStates & STATE_SUSPECTENEMY) && !(m_iStates & STATE_SEEINGENEMY)) // if suspecting enemy stand still
         iApproach = 49;
      else if (m_bIsReloading || m_bIsVIP) // if reloading or vip back off
         iApproach = 29;
      else if (m_iCurrentWeapon == WEAPON_KNIFE) // knife?
         iApproach = 100;
      else
      {
         iApproach = static_cast <int> (pev->health * m_fAgressionLevel);
         
         if (UsesSniper () && (iApproach > 49))
            iApproach = 49;
      }

      // only take cover when bomb is not planted and enemy can see the bot or the bot is VIP
      if ((iApproach < 30) && !g_bBombPlanted && (::IsInViewCone (pev->origin, m_pentEnemy) || m_bIsVIP))
      {
         m_fMoveSpeed = -pev->maxspeed;
         
         CurrentTask ()->iTask = TASK_SEEKCOVER;
         CurrentTask ()->bCanContinue = true;
         CurrentTask ()->fDesire = TASKPRI_ATTACK + 1.0;
      }
      else if (iApproach < 50)
         m_fMoveSpeed = 0.0;
      else
         m_fMoveSpeed = pev->maxspeed;

      if ((fDistance < 96) && (m_iCurrentWeapon != WEAPON_KNIFE))
         m_fMoveSpeed = -pev->maxspeed;

      if (m_iCurrentWeapon == WEAPON_KNIFE)
         return;

      if (UsesRifle () || UsesSubmachineGun ())
      {
         if (m_fLastFightStyleCheck + 3.0 < WorldTime ())
         {
            int iRand = RandomLong (1, 100);
            
            if (fDistance < 500)
               m_ucFightStyle = 0;
            else if (fDistance < 1024)
            {
               if (iRand < 50)
                  m_ucFightStyle = 0;
               else
                  m_ucFightStyle = 1;
            }
            else
            {
               if (iRand < (UsesSubmachineGun () ? 60 : 90))
                  m_ucFightStyle = 1;
               else
                  m_ucFightStyle = 0;
            }
            m_fLastFightStyleCheck = WorldTime ();

            DebugMsg ("changing fighting: %s", m_ucFightStyle  == 0 ? "strafe move" : "duck and don't move");
         }
      }
      else
      {
         if (m_fLastFightStyleCheck + 3.0 < WorldTime ())
         {
            if (!IsFriendInLineOfFire (fDistance) && (RandomLong (0, 100) < 75))
               m_ucFightStyle = 1;
            else
               m_ucFightStyle = 0;

            m_fLastFightStyleCheck = WorldTime ();
         }
      }
      
      if (((m_iSkill > 50) && (m_ucFightStyle == 0)) || m_bIsReloading || IsFriendInLineOfFire (fDistance))
      {
         if (m_fStrafeSetTime < WorldTime ())
         {
            // to start strafing, we have to first figure out if the target is on the left side or right side
            MakeVectors (m_pentEnemy->v.v_angle);

            Vector2D v2DirToPoint = (pev->origin - m_pentEnemy->v.origin).Make2D ().Normalize ();
            Vector2D v2RightSide = g_pGlobals->v_right.Make2D ().Normalize ();

            if (DotProduct (v2DirToPoint, v2RightSide) < 0)
               m_ucCombatStrafeDir = 1;
            else
               m_ucCombatStrafeDir = 0;

            if (RandomLong (1, 100) < 30)
               m_ucCombatStrafeDir ^= 1;

            m_fStrafeSetTime = WorldTime () + RandomFloat (0.5, 2.4);
         }

         if (m_ucCombatStrafeDir == 0)
         {
            if (!CheckWallOnLeft ())
               m_fSideMoveSpeed = -160.0;
            else
            {
               m_ucCombatStrafeDir ^= 1;
               m_fStrafeSetTime = WorldTime () + 0.7;
            }
         }
         else
         {
            if (!CheckWallOnRight ())
               m_fSideMoveSpeed = 160.0;
            else
            {
               m_ucCombatStrafeDir ^= 1;
               m_fStrafeSetTime = WorldTime () + 1.0;
            }
         }

         if ((m_iSkill > 80) && ((m_fJumpTime + 5.0 < WorldTime ()) && IsOnFloor () && (RandomLong (0, 1000) < 2) && (pev->velocity.Length2D () > 150.0)))
            pev->button |= IN_JUMP;

         if ((m_fMoveSpeed != 0.0) && (fDistance > 150.0))
            m_fMoveSpeed = 0.0;
      }
      else if (m_ucFightStyle == 1)
      {
         int iNearestToEnemyPoint = g_pWaypoint->FindNearest (m_pentEnemy->v.origin);

         if (EntityIsVisible (m_pentEnemy->v.origin, true) && (m_ucVisibility & VISIBLE_BODY) && !(m_ucVisibility & VISIBLE_OTHER) && g_pWaypoint->IsDuckVisible (m_iCurrWptIndex, iNearestToEnemyPoint) && g_pWaypoint->IsStandVisible (m_iCurrWptIndex, iNearestToEnemyPoint))
            m_fDuckTime = WorldTime () + (g_fTimeFrameInterval * 3.0);

         if (m_ucVisibility & VISIBLE_BODY)
            m_fMoveSpeed = 0.0;
         else
            m_fMoveSpeed = GetWalkSpeed ();

         m_fSideMoveSpeed = 0.0;
         m_fWptTimeset = WorldTime ();
      }
   }

   if (m_fDuckTime > WorldTime ())
   {
      m_fMoveSpeed = 0.0;
      m_fSideMoveSpeed = 0.0;
   }

   if (m_fMoveSpeed != 0.0)
      m_fMoveSpeed = GetWalkSpeed ();

   if (m_bIsReloading)
   {
      m_fMoveSpeed = -pev->maxspeed;
      m_fDuckTime = WorldTime () - (g_fTimeFrameInterval * 4.0);
   }

   if (!IsInWater () && !IsOnLadder () && ((m_fMoveSpeed != 0) || (m_fSideMoveSpeed != 0)))
   {
      if (IsDeadlyDrop (pev->origin + (g_pGlobals->v_forward * m_fMoveSpeed * 0.2) + (g_pGlobals->v_right * m_fSideMoveSpeed * 0.2) + (pev->velocity * g_fTimeFrameInterval)))
      {
         m_fSideMoveSpeed = -m_fSideMoveSpeed;
         m_fMoveSpeed = -m_fMoveSpeed;

         pev->button &= ~IN_JUMP;
      }
   }
}

bool CBot::HasPrimaryWeapon (void)
{
   // this function returns returns true, if bot has a primary weapon
   
   return (pev->weapons & WEAPON_PRIMARY) != 0;
}

bool CBot::HasShield (void)
{
   // this function returns true, if bot has a tactical shield
   
   return (strncmp (STRING (pev->viewmodel), "models/shield/v_shield_", 23) == 0);
}

bool CBot::IsShieldDrawn (void)
{
   // this function returns true, is the tactical shield is drawn
   
   if (!HasShield ())
      return false;

   return ((pev->weaponanim == 6) || (pev->weaponanim == 7));
}

bool CBot::IsEnemyProtectedByShield (void)
{
   // this function returns true, if enemy protected by the shield

   if (FNullEnt (m_pentEnemy) || (HasShield () && IsShieldDrawn ()))
      return false;

   // check if enemy has shield and this shield is drawn
   if ((strncmp (STRING (m_pentEnemy->v.viewmodel), "models/shield/v_shield_", 23) == 0) && ((m_pentEnemy->v.weaponanim == 6) || (m_pentEnemy->v.weaponanim == 7)))
   {
      if (::IsInViewCone (pev->origin, m_pentEnemy))
         return true;
   }
   return false;
}

bool CBot::UsesSniper (void)
{
   // this function returns true, ifreturns if bot is using a sniper rifle
   
   return ((m_iCurrentWeapon == WEAPON_AWP) || (m_iCurrentWeapon == WEAPON_G3SG1) || (m_iCurrentWeapon == WEAPON_SCOUT) || (m_iCurrentWeapon == WEAPON_SG550));
}

bool CBot::UsesRifle (void)
{
   tWeaponSelect *pSelect = &g_rgkWeaponSelect[0];
   int iCount = 0;

   while (pSelect->iId)
   {
      if (m_iCurrentWeapon == pSelect->iId)
         break;

      pSelect++;
      iCount++;
   }

   if (pSelect->iId && (iCount > 13))
      return true;

   return false;
}

bool CBot::UsesPistol (void)
{
   tWeaponSelect *pSelect = &g_rgkWeaponSelect[0];
   int iCount = 0;

   // loop through all the weapons until terminator is found
   while (pSelect->iId)
   {
      if (m_iCurrentWeapon == pSelect->iId)
         break;

      pSelect++;
      iCount++;
   }

   if (pSelect->iId && (iCount < 7))
      return true;

   return false;
}

bool CBot::UsesSubmachineGun (void)
{
   return ((m_iCurrentWeapon == WEAPON_MP5NAVY) || (m_iCurrentWeapon == WEAPON_TMP) || (m_iCurrentWeapon == WEAPON_P90) || (m_iCurrentWeapon == WEAPON_MAC10) || (m_iCurrentWeapon == WEAPON_UMP45));
}

bool CBot::UsesZoomableRifle (void)
{
   return ((m_iCurrentWeapon == WEAPON_AUG) || (m_iCurrentWeapon == WEAPON_SG552));
}

bool CBot::UsesBadPrimary (void)
{
   return ((m_iCurrentWeapon == WEAPON_XM1014) || (m_iCurrentWeapon == WEAPON_M3) || (m_iCurrentWeapon == WEAPON_UMP45) || (m_iCurrentWeapon == WEAPON_MAC10) || (m_iCurrentWeapon == WEAPON_TMP) || (m_iCurrentWeapon == WEAPON_P90));
}

int CBot::CheckGrenades (void)
{
   if (pev->weapons & (1 << WEAPON_HEGRENADE))
      return WEAPON_HEGRENADE;
   else if (pev->weapons & (1 << WEAPON_SMOKEGRENADE))
      return WEAPON_SMOKEGRENADE;
   else if (pev->weapons & (1 << WEAPON_FLASHBANG))
      return WEAPON_FLASHBANG;

   return -1;
}

void CBot::SelectBestWeapon (void)
{
   // this function chooses best weapon, from weapons that bot currently own, and change
   // current weapon to best one.

   if (g_rgpcvBotCVar[CVAR_KNIFEMODE]->GetBool ())
   {
      // if knife mode activated, force bot to use knife
      SelectWeaponByName ("weapon_knife");
      return;
   }
   tWeaponSelect *pSelect = &g_rgkWeaponSelect[0];

   int iSelectIndex = 0;
   int iChosenWeaponIndex = 0;

   // loop through all the weapons until terminator is found...
   while (pSelect[iSelectIndex].iId)
   {
      // is the bot NOT carrying this weapon?
      if (!(pev->weapons & (1 << pSelect[iSelectIndex].iId)))
      {
         iSelectIndex++;  // skip to next weapon
         continue;
      }
      int iId = pSelect[iSelectIndex].iId;

      bool bAmmoLeft = false;

      // is the bot already holding this weapon and there is still ammo in clip?
      if ((pSelect[iSelectIndex].iId == m_iCurrentWeapon) && ((GetAmmoInClip () < 0) || (GetAmmoInClip () >= pSelect[iSelectIndex].iMinPrimaryAmmo)))
         bAmmoLeft = true;

      // is no ammo required for this weapon OR enough ammo available to fire
      if ((g_rgkWeaponDefs[iId].iAmmo1 < 0) || (m_rgAmmo[g_rgkWeaponDefs[iId].iAmmo1] >= pSelect[iSelectIndex].iMinPrimaryAmmo))
         bAmmoLeft = true;

      if (bAmmoLeft)
         iChosenWeaponIndex = iSelectIndex;

      iSelectIndex++;
   }

   iChosenWeaponIndex %= NUM_WEAPONS + 1;
   iSelectIndex = iChosenWeaponIndex;

   int iId = pSelect[iSelectIndex].iId;

   // select this weapon if it isn't already selected
   if (m_iCurrentWeapon != iId)
      SelectWeaponByName (pSelect[iSelectIndex].szWeaponName);

   m_bIsReloading = false;
   m_iReloadState = RELOAD_NONE;
}


void CBot::SelectPistol (void)
{
   int iOldWeapons = pev->weapons;

   pev->weapons &= ~WEAPON_PRIMARY;
   SelectBestWeapon ();

   pev->weapons = iOldWeapons;
}

int CBot::GetHighestWeapon (void)
{
   tWeaponSelect *pSelect = &g_rgkWeaponSelect[0];

   int iWeapons = pev->weapons;
   int iNum = 0;
   int i = 0;

   // loop through all the weapons until terminator is found...
   while (pSelect->iId)
   {
      // is the bot carrying this weapon?
      if (iWeapons & (1 << pSelect->iId))
         iNum = i;
      i++;
      pSelect++;
   }
   return iNum;
}

void CBot::SelectWeaponByName (const char *cszName)
{
   FakeClientCommand (GetEntity (), cszName);
}

void CBot::SelectWeaponbyNumber (int iNum)
{
   FakeClientCommand (GetEntity (), g_rgkWeaponSelect[iNum].szWeaponName);
}

void CBot::CommandTeam (void)
{
   // Prevent spamming
   if (m_fTimeTeamOrder < WorldTime ())
   {
      bool bMemberNear = false;
      bool bMemberExists = false;

      edict_t *pTeamEdict;

      int iTeam = GetTeam (GetEntity ());

      // Search Teammates seen by this Bot
      for (int ind = 0; ind < MaxClients (); ind++)
      {
         if (!(g_rgkClients[ind].iFlags & CFLAG_USED) || !(g_rgkClients[ind].iFlags & CFLAG_ALIVE) || (g_rgkClients[ind].iTeam != iTeam )|| (g_rgkClients[ind].pentEdict == GetEntity ()))
            continue;

         bMemberExists = true;

         pTeamEdict = g_rgkClients[ind].pentEdict;
         
         if (EntityIsVisible (pTeamEdict->v.origin))
         {
            bMemberNear = true;
            break;
         }
      }

      if (bMemberNear) // has teammates ?
      {
         if (m_ucPersonality == PERSONALITY_AGRESSIVE)
            RadioMessage (RADIO_STORMTHEFRONT);
         else
            RadioMessage (RADIO_FALLBACK);
      }
      else if (bMemberExists)
         RadioMessage (RADIO_TAKINGFIRE);

      m_fTimeTeamOrder = WorldTime () + RandomFloat (5.0, 30.0);
   }
}

bool CBot::IsGroupOfEnemies (Vector vLocation, int iGroupIs, int iRadius)
{
   edict_t *pentPlayer;

   int iNumPlayers = 0;
   float fDistance;

   // search the world for enemy players...
   for (int i = 0; i < MaxClients (); i++)
   {
      if (!(g_rgkClients[i].iFlags & CFLAG_USED) || !(g_rgkClients[i].iFlags & CFLAG_ALIVE) || (g_rgkClients[i].pentEdict == GetEntity ()))
         continue;

      pentPlayer = g_rgkClients[i].pentEdict;
      fDistance = (pentPlayer->v.origin - vLocation).Length ();

      if (fDistance < iRadius)
      {
         // don't target our teammates...
         if (g_rgkClients[i].iTeam == GetTeam (GetEntity ()))
            return false;

         iNumPlayers++;

         if (iNumPlayers > iGroupIs)
            return true;
      }
   }
   return false;
}

void CBot::CheckReload (void)
{
   // check the reload state
   if ((CurrentTask ()->iTask == TASK_PLANTBOMB) || (CurrentTask ()->iTask == TASK_DEFUSEBOMB) || (CurrentTask ()->iTask == TASK_PICKUPITEM) || m_bUsingGrenade)
   {
      m_iReloadState = RELOAD_NONE;
      return;
   }

   m_bIsReloading = false;    // update reloading status
   m_fReloadCheckTime = WorldTime () + 1.0;

   if (m_iReloadState != RELOAD_NONE)
   {
      int iWeaponId = 0, iMaxClip = 0;
      int iWeapons = pev->weapons;

      if (m_iReloadState == RELOAD_PRIMARY)
         iWeapons &= WEAPON_PRIMARY;
      else if (m_iReloadState == RELOAD_SECONDARY)
         iWeapons &= WEAPON_SECONDARY;

      if (iWeapons == NULL)
      {
         m_iReloadState++;

         if (m_iReloadState > RELOAD_SECONDARY)
            m_iReloadState = RELOAD_NONE;

         return;
      }

      for (int i = 1; i < MAX_WEAPONS; i++)
      {
         if (iWeapons & (1 << i))
         {
            iWeaponId = i;
            break;
         }
      }

      InternalAssert (iWeaponId);

      switch (iWeaponId)
      {
      case WEAPON_M249:
         iMaxClip = 100;
         break;

      case WEAPON_P90:
         iMaxClip = 50;
         break;

      case WEAPON_GALIL:
         iMaxClip = 35;
         break;

      case WEAPON_ELITE:
      case WEAPON_MP5NAVY:
      case WEAPON_TMP:
      case WEAPON_MAC10:
      case WEAPON_M4A1:
      case WEAPON_AK47:
      case WEAPON_SG552:
      case WEAPON_AUG:
      case WEAPON_SG550:
         iMaxClip = 30;
         break;

      case WEAPON_UMP45:
      case WEAPON_FAMAS:
         iMaxClip = 25;
         break;

      case WEAPON_GLOCK18:
      case WEAPON_FIVESEVEN:
      case WEAPON_G3SG1:
         iMaxClip = 20;
         break;

      case WEAPON_P228:
         iMaxClip = 13;
         break;

      case WEAPON_USP:
         iMaxClip = 12;
         break;

      case WEAPON_AWP:
      case WEAPON_SCOUT:
         iMaxClip = 10;
         break;

      case WEAPON_M3:
         iMaxClip = 8;
         break;

      case WEAPON_DEAGLE:
      case WEAPON_XM1014:
         iMaxClip = 7;
         break;
      }

      if ((m_rgAmmoInClip[iWeaponId] < (iMaxClip * 0.8)) && (m_rgAmmo[g_rgkWeaponDefs[iWeaponId].iAmmo1] != 0))
      {
         if (m_iCurrentWeapon != iWeaponId)
            SelectWeaponByName (g_rgkWeaponDefs[iWeaponId].szClassname);

         if ((pev->oldbuttons & IN_RELOAD) == RELOAD_NONE)
            pev->button |= IN_RELOAD; // press reload button
         
         m_bIsReloading = true;
      }
      else
      {
         // if we have enemy don't reload next weapon
         if ((m_iStates & (STATE_SEEINGENEMY | STATE_HEARINGENEMY)) || (m_fSeeEnemyTime + 5.0 > WorldTime ()))
         {
            m_iReloadState = RELOAD_NONE;
            return;
         }
         m_iReloadState++;

         if (m_iReloadState > RELOAD_SECONDARY)
            m_iReloadState = RELOAD_NONE;

         return;
      }
   }
}