//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#ifndef COMPRESS_INCLUDED
#define COMPRESS_INCLUDED

const int N = 4096, F = 18, THRESHOLD = 2, NIL = N;

class CCompress
{
protected:
   unsigned long int m_ulTextSize;
   unsigned long int m_ulCodeSize;

   byte        m_ucTextBuffer[N + F - 1];
   int         m_iMatchPosition;
   int         m_iMatchLength;

   int         m_rgiLeft[N + 1];
   int         m_rgiRight[N + 257];
   int         m_rgiParent[N + 1];

private:
   void InitTree (void)
   {
      for (int i = N + 1; i <= N + 256; i++)
         m_rgiRight[i] = NIL;

      for (int j = 0; j < N; j++)
         m_rgiParent[j] = NIL;
   }

   void InsertNode (int iNode)
   {
      int i;

      int iCompare = 1;

      byte *pucKey = &m_ucTextBuffer[iNode];
      int iTemp = N + 1 + pucKey[0];

      m_rgiRight[iNode] = m_rgiLeft[iNode] = NIL;
      m_iMatchLength = 0;

      for (;;)
      {
         if (iCompare >= 0)
         {
            if (m_rgiRight[iTemp] != NIL)
               iTemp = m_rgiRight[iTemp];
            else
            {
               m_rgiRight[iTemp] = iNode;
               m_rgiParent[iNode] = iTemp;
               return;
            }
         }
         else
         {
            if (m_rgiLeft[iTemp] != NIL)
               iTemp = m_rgiLeft[iTemp];
            else
            {
               m_rgiLeft[iTemp] = iNode;
               m_rgiParent[iNode] = iTemp;
               return;
            }
         }

         for (i = 1; i < F; i++)
            if ((iCompare = pucKey[i] - m_ucTextBuffer[iTemp + i]) != 0)
               break;

         if (i > m_iMatchLength)
         {
            m_iMatchPosition = iTemp;
            if ((m_iMatchLength = i) >= F)
               break;
         }
      }

      m_rgiParent[iNode] = m_rgiParent[iTemp];
      m_rgiLeft[iNode] = m_rgiLeft[iTemp];
      m_rgiRight[iNode] = m_rgiRight[iTemp];
      m_rgiParent[m_rgiLeft[iTemp]] = iNode;
      m_rgiParent[m_rgiRight[iTemp]] = iNode;

      if (m_rgiRight[m_rgiParent[iTemp]] == iTemp)
         m_rgiRight[m_rgiParent[iTemp]] = iNode;
      else
         m_rgiLeft[m_rgiParent[iTemp]] = iNode;

      m_rgiParent[iTemp] = NIL;
   }


   void DeleteNode (int iNode)
   {
      int iTemp;

      if (m_rgiParent[iNode] == NIL)
         return; // not in tree

      if (m_rgiRight[iNode] == NIL)
         iTemp = m_rgiLeft[iNode];

      else if (m_rgiLeft[iNode] == NIL) 
         iTemp = m_rgiRight[iNode];

      else
      {
         iTemp = m_rgiLeft[iNode];

         if (m_rgiRight[iTemp] != NIL)
         {
            do
               iTemp = m_rgiRight[iTemp];
            while (m_rgiRight[iTemp] != NIL);

            m_rgiRight[m_rgiParent[iTemp]] = m_rgiLeft[iTemp];
            m_rgiParent[m_rgiLeft[iTemp]] = m_rgiParent[iTemp];
            m_rgiLeft[iTemp] = m_rgiLeft[iNode];
            m_rgiParent[m_rgiLeft[iNode]] = iTemp;
         }

         m_rgiRight[iTemp] = m_rgiRight[iNode];
         m_rgiParent[m_rgiRight[iNode]] = iTemp;
      }

      m_rgiParent[iTemp] = m_rgiParent[iNode];

      if (m_rgiRight[m_rgiParent[iNode]] == iNode)
         m_rgiRight[m_rgiParent[iNode]] = iTemp;
      else
         m_rgiLeft[m_rgiParent[iNode]] = iTemp;

      m_rgiParent[iNode] = NIL;
   }

public:
   CCompress (void)
   {
      m_ulTextSize = 0;
      m_ulCodeSize = 0;
   }

   ~CCompress (void)
   {
      m_ulTextSize = 0;
      m_ulCodeSize = 0;
   }

   int InternalEncode (char *szFile, byte *pucHeader, int iHeaderSize, byte *pucBuffer, int iBufferSize)
   {
      int i, iByte, iLength, iNode, iString, iLastMatchLength, iCodeBufferPointer, iBufferPointer = 0;
      byte rgucCodeBuffer[17], ucMask;

      stdfile fp (szFile, "wb");

      if (!fp.IsValid ())
         return -1;

      fp.Write (pucHeader, iHeaderSize, 1);
      InitTree ();

      rgucCodeBuffer[0] = 0;
      iCodeBufferPointer = ucMask = 1;
      iString = 0;
      iNode = N - F;

      for (i = iString; i < iNode; i++)
         m_ucTextBuffer[i] = ' ';

      for (iLength = 0; (iLength < F) && (iBufferPointer < iBufferSize); iLength++)
      {
         iByte = pucBuffer[iBufferPointer++];
         m_ucTextBuffer[iNode + iLength] = iByte;
      }

      if ((m_ulTextSize = iLength) == 0)
         return -1;

      for (i = 1; i <= F; i++)
         InsertNode (iNode - i);
      InsertNode (iNode);

      do
      {
         if (m_iMatchLength > iLength)
            m_iMatchLength = iLength;

         if (m_iMatchLength <= THRESHOLD)
         {
            m_iMatchLength = 1;
            rgucCodeBuffer[0] |= ucMask;
            rgucCodeBuffer[iCodeBufferPointer++] = m_ucTextBuffer[iNode];
         }
         else
         {
            rgucCodeBuffer[iCodeBufferPointer++] = (unsigned char) m_iMatchPosition;
            rgucCodeBuffer[iCodeBufferPointer++] = (unsigned char) (((m_iMatchPosition >> 4) & 0xf0) | (m_iMatchLength - (THRESHOLD + 1)));
         }

         if ((ucMask  <<= 1) == 0)
         {
            for (i = 0; i < iCodeBufferPointer; i++)
               fp.PutChar (rgucCodeBuffer[i]);

            m_ulCodeSize += iCodeBufferPointer;
            rgucCodeBuffer[0] = 0;
            iCodeBufferPointer = ucMask = 1;
         }

         iLastMatchLength = m_iMatchLength;

         for (i = 0; (i < iLastMatchLength) && (iBufferPointer < iBufferSize); i++)
         {
            iByte = pucBuffer[iBufferPointer++];
            DeleteNode (iString);
            m_ucTextBuffer[iString] = iByte;

            if (iString < F - 1)
               m_ucTextBuffer[iString + N] = iByte;

            iString = (iString + 1) & (N - 1);
            iNode = (iNode + 1) & (N - 1);
            InsertNode (iNode);
         }

         while (i++ < iLastMatchLength)
         {
            DeleteNode (iString);

            iString = (iString + 1) & (N - 1);
            iNode = (iNode + 1) & (N - 1);

            if (iLength--)
               InsertNode (iNode);
         }
      } while (iLength > 0);

      if (iCodeBufferPointer > 1)
      {
         for (i = 0; i < iCodeBufferPointer; i++)
            fp.PutChar (rgucCodeBuffer[i]);
         m_ulCodeSize += iCodeBufferPointer;
      }
      fp.Close ();

      return m_ulCodeSize;
   }

   int InternalDecode (char *szFile, int iHeaderSize, byte *pucBuffer, int iBufferSize)
   {
      int i, j, k, iNode, iByte;
      unsigned int ucFlags;
      int iBufferPointer = 0;

      stdfile fp (szFile, "rb");

      if (!fp.IsValid ())
         return -1;

      fp.Seek (iHeaderSize, SEEK_SET);

      iNode = N - F;
      for (i = 0; i < iNode; i++)
         m_ucTextBuffer[i] = ' ';

      ucFlags = 0;

      for (;;)
      {
         if (((ucFlags >>= 1) & 256) == 0)
         {
            if ((iByte = fp.GetChar ()) == EOF)
               break;
            ucFlags = iByte | 0xff00;
         } 

         if (ucFlags & 1)
         {
            if ((iByte = fp.GetChar ()) == EOF)
               break;
            pucBuffer[iBufferPointer++] = iByte;

            if (iBufferPointer > iBufferSize)
               return -1;

            m_ucTextBuffer[iNode++] = iByte;
            iNode &= (N - 1);
         }
         else
         {
            if ((i = fp.GetChar ()) == EOF)
               break;
            if ((j = fp.GetChar ()) == EOF)
               break;

            i |= ((j & 0xf0) << 4);
            j = (j & 0x0f) + THRESHOLD;

            for (k = 0; k <= j; k++)
            {
               iByte = m_ucTextBuffer[(i + k) & (N - 1)];
               pucBuffer[iBufferPointer++] = iByte;

               if (iBufferPointer > iBufferSize)
                  return -1;

               m_ucTextBuffer[iNode++] = iByte;
               iNode &= (N - 1);
            }
         }
      }
      fp.Close ();

      return iBufferPointer;
   }

   // external decoder
   static int Uncompress (char *szFile, int iHeaderSize, byte *pucBuffer, int iBufferSize)
   {
      CCompress kDecompress = CCompress ();
      return kDecompress.InternalDecode (szFile, iHeaderSize, pucBuffer, iBufferSize);
   }

   // external encoder
   static int Compress(char *szFile, byte *pucHeader, int iHeaderSize, byte *pucBuffer, int iBufferSize)
   {
      CCompress kCompress = CCompress ();
      return kCompress.InternalEncode (szFile, pucHeader, iHeaderSize, pucBuffer, iBufferSize);
   }
};

#endif
