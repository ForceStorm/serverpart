//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#include "yapb.h"

int BotCommandHandler (edict_t *pentEdict, const char *cszArg0, const char *cszArg1, const char *cszArg2, const char *cszArg3, const char *cszArg4, const char *cszArg5)
{
   // adding one bot with random parameters to random team
   if ((stricmp (cszArg0, "addbot") == 0) || (stricmp (cszArg0, "add") == 0))
      g_pBotManager->AddBot (cszArg4, cszArg1, cszArg2, cszArg3, cszArg5);

   // adding one bot with random parameters to terrorist team
   else if ((stricmp (cszArg0, "addbot_t") == 0) || (stricmp (cszArg0, "add_t") == 0))
      g_pBotManager->AddBot (cszArg4, cszArg1, cszArg2, "1", cszArg5);

   // adding one bot with random parameters to counter-terrorist team
   else if ((stricmp (cszArg0, "addbot_ct") == 0) || (stricmp (cszArg0, "add_ct") == 0))
      g_pBotManager->AddBot (cszArg4, cszArg1, cszArg2, "2", cszArg5);

   // kicking off one bot from the terrorist team
   else if ((stricmp (cszArg0, "kickbot_t") == 0) || (stricmp (cszArg0, "kick_t") == 0))
      g_pBotManager->RemoveFromTeam (TEAM_TERRORIST);

   // kicking off one bot from the counter-terrorist team
   else if ((stricmp (cszArg0, "kickbot_ct") == 0) || (stricmp (cszArg0, "kick_ct") == 0))
      g_pBotManager->RemoveFromTeam (TEAM_CT);

   // kills all bots on the terrorist team
   else if ((stricmp (cszArg0, "killbots_t") == 0) || (stricmp (cszArg0, "kill_t") == 0))
      g_pBotManager->KillAll (TEAM_TERRORIST);

   // kills all bots on the counter-terrorist team
   else if ((stricmp (cszArg0, "killbots_ct") == 0) || (stricmp (cszArg0, "kill_ct") == 0))
      g_pBotManager->KillAll (TEAM_CT);

   // list all bots playeing on the server
   else if ((stricmp (cszArg0, "listbots") == 0) || (stricmp (cszArg0, "list") == 0))
      g_pBotManager->List ();

   // kick off all bots from the played server
   else if ((stricmp (cszArg0, "kickbots") == 0) || (stricmp (cszArg0, "kickall") == 0))
      g_pBotManager->RemoveAll ();

   // kill all bots on the played server
   else if ((stricmp (cszArg0, "killbots") == 0) || (stricmp (cszArg0, "killall") == 0))
      g_pBotManager->KillAll ();

   // kick off one random bot from the played server
   else if ((stricmp (cszArg0, "kickone") == 0) || (stricmp (cszArg0, "kick") == 0))
      g_pBotManager->RemoveRandom ();

   // fill played server with bots
   else if ((stricmp (cszArg0, "fillserver") == 0) || (stricmp (cszArg0, "fill") == 0))
      g_pBotManager->FillServer (atoi (cszArg1), IsNullString (cszArg2) ? -1 : atoi (cszArg2));

   // swap counter-terrorist and terrorist teams
   else if ((stricmp (cszArg0, "swaptteams") == 0) || (stricmp (cszArg0, "swap") == 0))
   {
      for (int i = 0; i < MaxClients (); i++)
      {
         if (!(g_rgkClients[i].iFlags & CFLAG_USED))
            continue;

         if (IsValidBot (g_rgkClients[i].pentEdict))
            FakeClientCommand (g_rgkClients[i].pentEdict, "chooseteam; menuselect %d; menuselect 5", GetTeam (g_rgkClients[i].pentEdict) == TEAM_CT ? 1 : 2);
         else
            (*g_engfuncs.pfnClientCommand) (g_rgkClients[i].pentEdict, "chooseteam; menuselect %d", GetTeam (g_rgkClients[i].pentEdict) == TEAM_CT ? 1 : 2);
      }
   }

   // select the weapon mode for bots
   else if ((stricmp (cszArg0, "weaponmode") == 0) || (stricmp (cszArg0, "wmode") == 0))
   {
      int iSelection = atoi (cszArg1);

      // check is selected range valid
      if ((iSelection >= 1) && (iSelection <= 7))
         g_pBotManager->SetWeaponMode (iSelection);
      else
         ClientPrint (pentEdict, print_withtag, "Choose weapon from 1 to 7 range");
   }

   // force all bots to vote to specified map
   else if (stricmp (cszArg0, "votemap") == 0)
   {
      if (!IsNullString (cszArg1))
      {
         int iVoteMap = atoi (cszArg1);

         // loop through all players
         for (int i = 0; i < MaxClients (); i++)
         {
            if (g_pBotManager->GetBot (i) != NULL)
               g_pBotManager->GetBot (i)->m_iVoteMap = iVoteMap;
         }
         ClientPrint (pentEdict, print_withtag, "All dead bots will vote for map #%d", iVoteMap);
      }
   }

   // force bots to execute client command
   else if ((stricmp (cszArg0, "sendcmd") == 0) || (stricmp (cszArg0, "order") == 0))
   {
      if (IsNullString (cszArg1))
         return 1;

      edict_t *pent = INDEXENT (atoi (cszArg1)) - 1;

      if (IsValidBot (pent))
      {
         FakeClientCommand (pent, cszArg2);
         ClientPrint (pentEdict, print_withtag, "Bot %s executing command %s", STRING (pent->v.netname), cszArg2);
      }
      else
         ClientPrint (pentEdict, print_withtag, "Player is NOT Bot!");
   }

   // display current time on the server
   else if ((stricmp (cszArg0, "ctime") == 0) || (stricmp (cszArg0, "time") == 0))
   {
      time_t kTickTime = time (NULL);
      tm *pLocalTime = localtime (&kTickTime);

      char szDate[32];
      strftime (szDate, 31, "--- Current Time: %H:%M:%S ---", pLocalTime);

      ClientPrint (pentEdict, print_center, szDate);
   }

   // displays bot about information
   else if ((stricmp (cszArg0, "about_bot") == 0) || (stricmp (cszArg0, "about") == 0))
   {
      char cszAboutData[] =
         "+---------------------------------------------------------------------------------------+\n"
         " The YaPB for Counter-Strike Version " PRODUCT_SUPPORT_VERSION " and Above\n"
         " Created by " PRODUCT_AUTHOR ", Using PODBot 2.6 Source Code\n"
         " Website: " PRODUCT_URL " | E-Mail: " PRODUCT_EMAIL " \n"
         "+---------------------------------------------------------------------------------------+\n";

      //HudMessage (pentEdict, true, Vector (RandomLong (33, 255), RandomLong (33, 255), RandomLong (33, 255)), cszAboutData); 
   }

   // displays version information
   else if ((stricmp (cszArg0, "version") == 0) || (stricmp (cszArg0, "ver") == 0))
   {
      char cszVersionData[] =
         "------------------------------------------------\n"
         "Name: %s\n"
         "Version: %s (Build: %u)\n"
         "Builded by: %s\n"
         "Compiled: %s, %s +300 (GMT)\n"
         "Optimization: %s\n"
         "Meta-Interface Version: %s\n"
         "------------------------------------------------";

      ClientPrint (pentEdict, print_console, cszVersionData, PRODUCT_NAME, PRODUCT_VERSION, GenerateBuildNumber (), PRODUCT_AUTHOR, __DATE__, __TIME__, PRODUCT_OPT_TYPE, META_INTERFACE_VERSION);
   }

   // display some sort of help information
   else if ((stricmp (cszArg0, "?") == 0) || (stricmp (cszArg0, "help") == 0))
   {
      ClientPrint (pentEdict, print_console, "Bot Commands:"); 
      ClientPrint (pentEdict, print_console, "yapb version            - display version information.");
      ClientPrint (pentEdict, print_console, "yapb about              - show bot about information."); 
      ClientPrint (pentEdict, print_console, "yapb add                - create a bot in current game."); 
      ClientPrint (pentEdict, print_console, "yapb fill               - fill the server with random bots.");
      ClientPrint (pentEdict, print_console, "yapb kickall            - disconnects all bots from current game."); 
      ClientPrint (pentEdict, print_console, "yapb killbots           - kills all bots in current game.");
      ClientPrint (pentEdict, print_console, "yapb kick               - disconnect one random bot from game."); 
      ClientPrint (pentEdict, print_console, "yapb weaponmode         - select bot weapon mode."); 
      ClientPrint (pentEdict, print_console, "yapb votemap            - allows dead bots to vote for specific map.");
            
      if ((stricmp (cszArg1, "full") == 0) || (stricmp (cszArg1, "f") == 0) || (stricmp (cszArg1, "?") == 0))
      {
         ClientPrint (pentEdict, print_console, "yapb msec               - choose msec calculation method.");
         ClientPrint (pentEdict, print_console, "yapb add_t              - creates one random bot to terrorist team.");
         ClientPrint (pentEdict, print_console, "yapb add_ct             - creates one random bot to ct team."); 
         ClientPrint (pentEdict, print_console, "yapb kick_t             - disconnect one random bot from terrorist team.");
         ClientPrint (pentEdict, print_console, "yapb kick_ct            - disconnect one random bot from ct team."); 
         ClientPrint (pentEdict, print_console, "yapb kill_t             - kills all bots on terrorist team.");
         ClientPrint (pentEdict, print_console, "yapb kill_ct            - kills all bots on ct team.");
         ClientPrint (pentEdict, print_console, "yapb list               - display list of bots currently playing."); 
         ClientPrint (pentEdict, print_console, "yapb order              - execute specific command on specified bot.");
         ClientPrint (pentEdict, print_console, "yapb time               - displays current time on server.");
         ClientPrint (pentEdict, print_console, "yapb deletewp           - erase waypoint file from hard disk (permanently).");
         
          if (!IsDedicatedServer ())
          {
             ServerPrintNoTag ("yapb autowp            - toggle autowppointing."); 
             ServerPrintNoTag ("yapb wp                - toggle waypoint showing.");
             ServerPrintNoTag ("yapb wp on noclip      - enable noclip cheat"); 
             ServerPrintNoTag ("yapb wp save nocheck   - save waypoints without checking.");
             ServerPrintNoTag ("yapb wp add            - open menu for waypoint creation.");
             ServerPrintNoTag ("yapb wp menu           - open main waypoint menu.");
             ServerPrintNoTag ("yapb wp addbasic       - creates basic waypoints on map.");
             ServerPrintNoTag ("yapb wp find           - show direction to specified waypoint."); 
             ServerPrintNoTag ("yapb wp load           - wload the waypoint file from hard disk.");
             ServerPrintNoTag ("yapb wp check          - checks if all waypoints connections are valid.");
             ServerPrintNoTag ("yapb wp cache          - cache nearest waypoint.");
             ServerPrintNoTag ("yapb wp teleport       - teleport hostile to specified waypoint waypoint."); 
             ServerPrintNoTag ("yapb wp setradius      - manually sets the wayzone radius for this waypoint.");
             ServerPrintNoTag ("yapb path autodistance - opens menu for setting autopath maximum distance.");
             ServerPrintNoTag ("yapb path cache        - remember the nearest to player waypoint.");
             ServerPrintNoTag ("yapb path create       - opens menu for path creation.");
             ServerPrintNoTag ("yapb path delete       - delete path from cached to nearest waypoint.");
             ServerPrintNoTag ("yapb path create_in    - creating incoming path connection.");
             ServerPrintNoTag ("yapb path create_out   - creating outgoing path connection.");
             ServerPrintNoTag ("yapb path create_both  - creating both-ways path connection.");
             ServerPrintNoTag ("yapb exp <on/off>      - enable/disable bot experience system (less CPU usage).");
             ServerPrintNoTag ("yapb exp save          - save the experience data.");
          }
      }
   }
   
   // sets health for all available bots
   else if ((stricmp (cszArg0, "sethealth") == 0) || (stricmp (cszArg0, "health") == 0))
   {
      if (IsNullString (cszArg1))
         ClientPrint (pentEdict, print_withtag, "Please specify health");
      else
      {
         ClientPrint (pentEdict, print_withtag, "Bot health is set to %d%%", atoi (cszArg1));

         for (int i = 0; i < MaxClients (); i++)
         {
            if (g_pBotManager->GetBot (i) != NULL)
               g_pBotManager->GetBot (i)->pev->health = fabsf (atof (cszArg1));
         }
      }
   }

   // selecting the bot msec calculation method
   else if ((stricmp (cszArg0, "msec") == 0) || (stricmp (cszArg0, "setmsec") == 0))
   {
      if (!IsNullString (cszArg1))
          g_iMsecMethod = atoi (cszArg1);

      if ((g_iMsecMethod) >= 1 && (g_iMsecMethod <= 4))
         ClientPrint (pentEdict, print_withtag, "Msec calculation method is set to METHOD_%s", (g_iMsecMethod == METHOD_TOBIAS ? "TOBIAS" : (g_iMsecMethod == METHOD_RICH ? "RICH" : (g_iMsecMethod == METHOD_LEON ? "LEON" : "PM"))));
      else  
         ClientPrint (pentEdict, print_withtag, "Unknown method, choose method from 1 to 4 range");
   }

   // displays main bot menu
   else if ((stricmp (cszArg0, "botmenu") == 0) || (stricmp (cszArg0, "menu") == 0))
      ShowMenu (pentEdict, &g_rgkMenu[0]);

   // sets public server bot cvar
   else if ((stricmp (cszArg0, "setcvar") == 0) || (stricmp (cszArg0, "set") == 0))
   {
      for (int i = 0; i < CVAR_TOTAL; i++)
      {
         if (stricmp (&g_rgpcvBotCVar[i]->GetName ()[3], cszArg1) == 0)
         {
            if ((i == CVAR_PASSWORD) || (i == CVAR_PASSWORDKEY))
               return 3;

            CVarSetString (g_rgpcvBotCVar[i]->GetName (), cszArg2);
            break;
         }
      }
   }

   // waypoint manimupulation (really obsolete, can be edited through menu) (supported only on listen server)
   else if ((stricmp (cszArg0, "waypoint") == 0) || (stricmp (cszArg0, "wp") == 0) || (stricmp (cszArg0, "wpt") == 0))
   {
      if (IsDedicatedServer () || FNullEnt (g_pentHostEdict))
         return 2;
      
      // enables or disable waypoint displaying
      if (stricmp (cszArg1, "on") == 0)
      {
         g_bWaypointOn = true;
         ServerPrint ("Waypoint Editing Enabled");

         // enables noclip cheat
         if (stricmp (cszArg2, "noclip") == 0)
         {
            if (g_bEditNoclip)
            {
               g_pentHostEdict->v.movetype = MOVETYPE_WALK;
               ServerPrint ("Noclip Cheat Disabled");
            }
            else 
            {
               g_pentHostEdict->v.movetype = MOVETYPE_NOCLIP;
               ServerPrint ("Noclip Cheat Enabled");
            }
            g_bEditNoclip ^= true; // switch on/off (XOR it!)
         }
         ServerCommand ("yapb wp mdl on");
      }
      
      // switching waypoint editing off
      else if (stricmp (cszArg1, "off") == 0)
      {
         g_bWaypointOn = false;
         g_bEditNoclip = false;
         g_pentHostEdict->v.movetype = MOVETYPE_WALK;

         ServerPrint ("Waypoint Editing Disabled");
         ServerCommand ("yapb wp mdl off");
      }

      // toggles displaying player models on spawn spots
      else if ((stricmp (cszArg1, "mdl") == 0) || (stricmp (cszArg1, "models") == 0))
      {
         edict_t *pentSpawn = NULL;

         if (stricmp (cszArg2, "on") == 0)
         {
            while (!FNullEnt (pentSpawn = FIND_ENTITY_BY_CLASSNAME (pentSpawn, "info_player_start")))
               pentSpawn->v.effects &= ~EF_NODRAW;
            while (!FNullEnt (pentSpawn = FIND_ENTITY_BY_CLASSNAME (pentSpawn, "info_player_deathmatch")))
               pentSpawn->v.effects &= ~EF_NODRAW;
            while (!FNullEnt (pentSpawn = FIND_ENTITY_BY_CLASSNAME (pentSpawn, "info_vip_start")))
               pentSpawn->v.effects &= ~EF_NODRAW;

            ServerCommand ("mp_roundtime 9"); // reset round time to maximum
            ServerCommand ("mp_timelimit 0"); // disable the time limit
            ServerCommand ("mp_freezetime 0"); // disable freezetime
         }
         else if (stricmp (cszArg2, "off") == 0)
         {
            while (!FNullEnt (pentSpawn = FIND_ENTITY_BY_CLASSNAME (pentSpawn, "info_player_start")))
               pentSpawn->v.effects |= EF_NODRAW;
            while (!FNullEnt (pentSpawn = FIND_ENTITY_BY_CLASSNAME (pentSpawn, "info_player_deathmatch")))
               pentSpawn->v.effects |= EF_NODRAW;
            while (!FNullEnt (pentSpawn = FIND_ENTITY_BY_CLASSNAME (pentSpawn, "info_vip_start")))
               pentSpawn->v.effects |= EF_NODRAW;
         }
      }

      // show direction to specified waypoint
      else if (stricmp (cszArg1, "find") == 0)
      {
         g_iFindWPIndex = atoi (cszArg2);

         if (g_iFindWPIndex < g_iNumWaypoints)
            ServerPrint ("Showing Direction to Waypoint #%d", g_iFindWPIndex);
         else
            g_iFindWPIndex = -1;
      }

      // opens adding waypoint menu
      else if (stricmp (cszArg1, "add") == 0)
      {
         g_bWaypointOn = true;  // turn waypoints on
         ShowMenu (g_pentHostEdict, &g_rgkMenu[12]);
      }

      // creates basic waypoints on the map (ladder/spawn points/goals)
      else if (stricmp (cszArg1, "addbasic") == 0)
      {
         g_pWaypoint->CreateBasic ();
         CenterPrint ("Basic waypoints was Created");
      }

      // delete nearest to host edict waypoint
      else if (stricmp (cszArg1, "delete") == 0)
      {
         g_bWaypointOn = true; // turn waypoints on
         g_pWaypoint->Delete ();
      }

      // save waypoint data into file on hard disk
      else if (stricmp (cszArg1, "save") == 0)
      {
         char *szWaypointSaveMessage = g_pLocalize->T ("Waypoints Saved");

         if (FStrEq (cszArg2, "nocheck"))
         {
            g_pWaypoint->Save ();
            ServerPrint (szWaypointSaveMessage);
         }
         else if (g_pWaypoint->NodesValid ())
         {
            g_pWaypoint->Save ();
            ServerPrint (szWaypointSaveMessage);
         }
      }

      // remove waypoint and all corresponding files from hard disk
      else if (stricmp (cszArg1, "erase") == 0)
         g_pWaypoint->EraseFromHardDisk ();

      // load all waypoints again (overrides all changes, that wasn't saved)
      else if (stricmp (cszArg1, "load") == 0)
      {
         if (g_pWaypoint->Load ())
            ServerPrint ("Waypoints loaded");
      }

      // check all nodes for validation
      else if (stricmp (cszArg1, "check") == 0)
      {
         if (g_pWaypoint->NodesValid ())
            CenterPrint ("Nodes work Fine");
      }

      // opens menu for setting (removing) waypoint flags
      else if (stricmp (cszArg1, "flags") == 0)
         ShowMenu (g_pentHostEdict, &g_rgkMenu[13]);

      // setting waypoint radius
      else if (stricmp (cszArg1, "setradius") == 0)
         g_pWaypoint->SetRadius (atoi (cszArg2));

      // remembers nearest waypoint
      else if (stricmp (cszArg1, "cache") == 0)
         g_pWaypoint->CacheWaypoint ();

      // teleport player to specified waypoint
      else if (stricmp (cszArg1, "teleport") == 0)
      {
         int iTelIndex = atoi (cszArg2);

         if (iTelIndex < g_iNumWaypoints)
         {
            (*g_engfuncs.pfnSetOrigin) (g_pentHostEdict, g_pWaypoint->GetPath (iTelIndex)->vOrigin);
            g_bWaypointOn = true;

            ServerPrint ("Player '%s' teleported to waypoint #%d (x:%.1f, y:%.1f, z:%.1f)", STRING (g_pentHostEdict->v.netname), iTelIndex, g_pWaypoint->GetPath (iTelIndex)->vOrigin.x, g_pWaypoint->GetPath (iTelIndex)->vOrigin.y, g_pWaypoint->GetPath (iTelIndex)->vOrigin.z);
            g_bEditNoclip = true;
         }
      }

      // displays waypoint menu
      else if (stricmp (cszArg1, "menu") == 0)
         ShowMenu (g_pentHostEdict, &g_rgkMenu[9]);

      // otherwise display waypoint current status
      else
         ServerPrint ("Waypoints are %s", g_bWaypointOn == true ? "Enabled" : "Disabled");
   }

   // path waypoint editing system (supported only on listen server)
   else if ((stricmp (cszArg0, "pathwaypoint") == 0) || (stricmp (cszArg0, "path") == 0) || (stricmp (cszArg0, "pwp") == 0))
   {
      if (IsDedicatedServer () || FNullEnt (g_pentHostEdict))
         return 2;

      // opens path creation menu
      if (stricmp (cszArg1, "create") == 0)
         ShowMenu (g_pentHostEdict, &g_rgkMenu[19]);

      // creates incoming path from the cached waypoint
      else if (stricmp (cszArg1, "create_in") == 0)
         g_pWaypoint->CreatePath (PATH_INCOMING);

      // creates outgoing path from current waypoint
      else if (stricmp (cszArg1, "create_out") == 0)
         g_pWaypoint->CreatePath (PATH_OUTGOING);

      // creates bidirectional path from cahed to current waypoint
      else if (stricmp (cszArg1, "create_both") == 0)
         g_pWaypoint->CreatePath (PATH_BOTHWAYS); 

      // delete special path
      else if (stricmp (cszArg1, "delete") == 0)
         g_pWaypoint->DeletePath ();

      // sets autho path maximum distance
      else if (stricmp (cszArg1, "autodistance") == 0)
         ShowMenu (g_pentHostEdict, &g_rgkMenu[19]);
   }

   // automatic waypoint handling (supported only on listen server)
   else if ((stricmp (cszArg0, "autowaypoint") == 0) || (stricmp (cszArg0, "autowp") == 0))
   {
      if (IsDedicatedServer () || FNullEnt (g_pentHostEdict))
         return 2;

      // enable autowaypointing
      if (stricmp (cszArg1, "on") == 0)
      {
         g_bAutoWaypoint = true;
         g_bWaypointOn = true; // turn this on just in case
      }

      // disable autowaypointing
      else if (stricmp (cszArg1, "off") == 0)
         g_bAutoWaypoint = false;

      // display status
      ServerPrint ("Auto-Waypoint %s", g_bAutoWaypoint == true ? "Enabled" : "Disabled");
   }

   // experience system handling (supported only on listen server)
   else if ((stricmp (cszArg0, "experience") == 0) || (stricmp (cszArg0, "exp") == 0))
   {
      if (IsDedicatedServer () || FNullEnt (g_pentHostEdict))
         return 2;

      // write experience table (and visibility table) to hard disk
      if (stricmp (cszArg1, "save") == 0)
      {
         g_pWaypoint->SaveExperienceTab ();
         g_pWaypoint->SaveVisibilityTab ();

         ServerPrint ("Experience tab saved");
      }

      // enable using of experience data
      else if (stricmp (cszArg1, "on") == 0)
      {
         g_bDangerDirection = true;
         g_bWaypointOn = true;  // turn this on just in case

         ServerPrint ("Experience Enabled");
      }

      // disable using of experience data
      else if (stricmp (cszArg1, "off") == 0)
      {
         g_bDangerDirection = false;
         ServerPrint ("Experience Disabled");
      }
   }
   else
      return 0; // command is not handled by bot

   return 1; // command was handled by bot
}

void CommandHandler (void)
{
   // this function is the dedicated server command handler for the new yapb server command we
   // registered at game start. It will be called by the engine each time a server command that
   // starts with "yapb" is entered in the server console. It works exactly the same way as
   // ClientCommand() does, using the CmdArgc() and CmdArgv() facilities of the engine. Argv(0)
   // is the server command itself (here "yapb") and the next ones are its arguments. Just like
   // the stdio command-line parsing in C when you write "long main (long argc, char **argv)".

   // check status for dedicated server command
   if (BotCommandHandler (g_pentHostEdict, IsNullString (CMD_ARGV (1)) ? "help" : CMD_ARGV (1), CMD_ARGV (2), CMD_ARGV (3), CMD_ARGV (4), CMD_ARGV (5), CMD_ARGV (6)) == 0)
      ServerPrint ("Unknown command: %s", CMD_ARGV (1));
}

void ParseVoiceEvent (vector <string> kBase, int iType, float fTimeToRepeat)
{
   // this function does common work of parsing single line of voice chatter

   vector <string> kTemp = string (kBase[1]).Split (',');
   tChatter kVoiceCmd;

   ITERATE_ARRAY (kTemp, i)
   {
      kTemp[i].Trim ().DropQuotes ();

      if (GetWaveLength (kTemp[i]) == 0.0)
         continue;

      kVoiceCmd.sName = kTemp[i];
      kVoiceCmd.fRepeatTime = fTimeToRepeat;

      g_arVoiceEngine[iType].AddItem (kVoiceCmd);
    }
}

void ParsePlaceEvent (vector <string> kBase)
{
   vector <string> kTemp = string (kBase[1]).Split (',');
   tChatterPlace kPlaceCmd;
   
   kPlaceCmd.uHash = kBase[0].Trim ().Hash ();
   kPlaceCmd.sName = kBase[0].Trim ();

   ITERATE_ARRAY (kTemp, i)
   {
      kTemp[i].Trim ().DropQuotes ();

      if (GetWaveLength (kTemp[i]) == 0.0)
         continue;

      kPlaceCmd.kFiles.AddItem (kTemp[i]);

      if (i == kTemp.Size () - 1)
         g_arPlaceNames.AddItem (kPlaceCmd);
    }
}

void InitConfig (void)
{
   stdfile fp;
   char szCmd[80], szArg[80], szLine[256];
   int i, iLine = 0, iParseWeapons = 0;

   int *g_ptrWeaponPrefs = NULL;
   int iWeaponParseState = MAP_DE;
   char *szStart, *szEnd;

   tKeywordChat kReply;
   int iChatType = -1;

   // fixes for crashing if configs couldn't be accessed
   g_arChatEngine.Size (CHAT_TOTAL);
   g_arVoiceEngine.Size (VOICE_TOTAL);
   
   #define SKIP_COMMENTS() if ((szLine[0] == '/') || (szLine[0] == '\r') || (szLine[0] == '\n') || (szLine[0] == 0) || (szLine[0] == ' ') || (szLine[0] == '\t')) continue;

   // NAMING SYSTEM INITIALIZATION
   FILE *pFile;
   pFile = fopen( "cstrike\\addons\\yapb\\config\\BotNames.ini" , "rt");
   if( !pFile )
	   return;

   char szBuff[256];
   while( !feof(pFile) )
   {
	   fgets(szBuff, sizeof(szBuff)-1, pFile);
	   if( !szBuff[0] || szBuff[0] == ';')
		   continue;
	   if( szBuff[ sizeof(szBuff) - 1 ] == '\n')
		   szBuff[ sizeof(szBuff) - 1 ] = '\0';

	   tBotName kItem;
	   kItem.sName = szBuff;
       kItem.bUsed = false;

	   g_arBotNames.AddItem (kItem);
   }

   fclose(pFile);

  /* if (OpenConfig ("names.cfg", "Name configuration file not found.", &fp , true))
   {
      while (fp.GetString (szLine, 255))
      {
         SKIP_COMMENTS ();

         StrTrim (szLine);
         tBotName kItem;

         kItem.sName = szLine;
         kItem.bUsed = false;

         g_arBotNames.AddItem (kItem);
      }
      fp.Close ();
   }*/

   // CHAT SYSTEM CONFIG INITIALIZATION
   if (OpenConfig ("chat.cfg", "Chat file not found.", &fp, true))
   {
      while (fp.GetString (szLine, 255))
      { 
         SKIP_COMMENTS ();
         strcpy (szCmd, GetField (szLine, 0, 1));
         
         if (strcmp (szCmd, "[KILLED]") == 0)
         {
            iChatType = 0;
            continue;
         }
         else if (strcmp (szCmd, "[BOMBPLANT]") == 0)
         {
            iChatType = 1;
            continue;
         }       
         else if (strcmp (szCmd, "[DEADCHAT]") == 0)
         {
            iChatType = 2;
            continue;
         }       
         else if (strcmp (szCmd, "[REPLIES]") == 0)
         {
            iChatType = 3;
            continue;
         }     
         else if (strcmp (szCmd, "[UNKNOWN]") == 0)
         {
            iChatType = 4;
            continue;
         }    
         else if (strcmp (szCmd, "[TEAMATTACK]") == 0)
         {
            iChatType = 5;
            continue;
         }   
         else if (strcmp (szCmd, "[WELCOME]") == 0)
         {
            iChatType = 6;
            continue;
         }     
         else if (strcmp (szCmd, "[TEAMKILL]") == 0)
         {
            iChatType = 7;
            continue;
         }

         if (iChatType != 3)
            szLine[79] = 0;

         StrTrim (szLine);

         switch (iChatType)
         {
         case 0:
            g_arChatEngine[CHAT_KILLING].AddItem (szLine);
            break;

         case 1:
            g_arChatEngine[CHAT_BOMBPLANT].AddItem (szLine);
            break;

         case 2:
            g_arChatEngine[CHAT_DEAD].AddItem (szLine);
            break;

         case 3:
            if (strstr (szLine, "@KEY") != NULL)
            {
               if (!kReply.arKeywords.IsFree () && !kReply.arReplies.IsFree ())
               {
                  g_arReplyChat.AddItem (kReply);
                  kReply.arReplies.Cleanup ();
               }

               kReply.arKeywords.Cleanup ();
               kReply.arKeywords = string (&szLine[4]).Split (',');

               ITERATE_ARRAY (kReply.arKeywords, i)
                  kReply.arKeywords[i].Trim ().DropQuotes ();
            }
            else if (!kReply.arKeywords.IsFree ())
               kReply.arReplies.AddItem (szLine);

            break;

         case 4:
            g_arChatEngine[CHAT_NOKEYWORD].AddItem (szLine);
            break;

         case 5:
            g_arChatEngine[CHAT_TEAMATTACK].AddItem (szLine);
            break;

         case 6:
            g_arChatEngine[CHAT_WELCOMING].AddItem (szLine);
            break;

         case 7:
            g_arChatEngine[CHAT_TEAMKILL].AddItem (szLine);
            break;
         }
      }
      fp.Close ();
   }

   // SKILL DATA INITIALIZATION
   if (OpenConfig ("skill.cfg", "Skill configuration file not found. Loading defaults", &fp))
   {
      i = 0;

      while (fp.GetString (szLine, 255))
      {
         SKIP_COMMENTS ();

         strcpy (szCmd, GetField (szLine, 0));
         strcpy (szArg, GetField (szLine, 1, 1));

         if (strcmp (szCmd, "MIN_DELAY") == 0)
            g_rgkSkillTab[i].fMinSurpriseTime = atof (szArg);
         else if (strcmp (szCmd, "MAX_DELAY") == 0)
            g_rgkSkillTab[i].fMaxSurpriseTime = atof (szArg);

         else if (strcmp (szCmd, "MIN_TURNSPEED") == 0)
            g_rgkSkillTab[i].fMinTurnSpeed = atof (szArg);
         else if (strcmp (szCmd, "MAX_TURNSPEED") == 0)
            g_rgkSkillTab[i].fMaxTurnSpeed = atof (szArg);

         else if (strcmp (szCmd, "AIM_OFFS_X") == 0)
            g_rgkSkillTab[i].fAimOffs_X = atof (szArg);
         else if (strcmp (szCmd, "AIM_OFFS_Y") == 0)
            g_rgkSkillTab[i].fAimOffs_Y = atof (szArg);
         else if (strcmp (szCmd, "AIM_OFFS_Z") == 0)
            g_rgkSkillTab[i].fAimOffs_Z = atof (szArg);

         else if (strcmp (szCmd, "HEADSHOT_ALLOW") == 0)
            g_rgkSkillTab[i].iHeadshotFrequency = atoi (szArg);
         else if (strcmp (szCmd, "HEAR_SHOOTTHRU") == 0)
            g_rgkSkillTab[i].iHeardShootThruProb = atoi (szArg);         
         else if (strcmp (szCmd, "SEEN_SHOOTTHRU") == 0)
         {
            g_rgkSkillTab[i].iSeenShootThruProb = atoi (szArg);

            if (i < 5)
               i++; // prevent overflows in config files
         }
      }
      fp.Close ();
   }

   // LOGO ENUMERATION INITIALIZATION
   if (OpenConfig ("logos.cfg", "Spray configuration file not found. Loading defaults", &fp))
   { 
      while (fp.GetString (szLine, 255))
      {
         SKIP_COMMENTS ();
         g_arSprayNames.AddItem (GetField (szLine, 0, 1));
      }
      fp.Close ();

      if (g_arSprayNames.IsFree ())
      {
         // add default spraynames
         g_arSprayNames.AddItem ("{biohaz");
         g_arSprayNames.AddItem ("{graf004");
         g_arSprayNames.AddItem ("{graf005");
         g_arSprayNames.AddItem ("{lambda06");
         g_arSprayNames.AddItem ("{target");
         g_arSprayNames.AddItem ("{hand1");
      }
   }

   // WEAPON SYSTEM CONFIG INITIALIZATION
   if (OpenConfig ("weapons.cfg", "Weapon configuration file not found. Loading defaults", &fp))
   {
      while (fp.GetString (szLine, 255))
      {
         iLine++;

         SKIP_COMMENTS ();
         strcpy (szCmd, GetField (szLine, 0, 1));
            
         if (iParseWeapons < 4)
         {
            if (strcmp (szCmd, "[STANDARD]") == 0)
               iWeaponParseState = 1;
            else if (strcmp (szCmd, "[AS]") == 0)
               iWeaponParseState = 2;
            else if (strcmp (szCmd, "[BUYPRECENT]") == 0)
               iWeaponParseState = 3;
            else if (strcmp (szCmd, "[ECONOMY]") == 0)
               iWeaponParseState = 4;
            else
            {
               szStart = &szLine[0];

               switch (iWeaponParseState)
               {
               case 1:
                  for (i = 0; i < NUM_WEAPONS; i++)
                  {
                     szEnd = WPN_StrChr (szStart, ',', iLine);
                     g_rgkWeaponSelect[i].iTeamStandard = atoi (szStart);
                     szStart = szEnd + 1;
                  }
                  break;

               case 2:
                  for (i = 0; i < NUM_WEAPONS; i++)
                  {
                     szEnd = WPN_StrChr (szStart, ',', iLine);
                     g_rgkWeaponSelect[i].iTeamAS = atoi (szStart);
                     szStart = szEnd + 1;
                  }
                  break;
              
               case 3:
                  for (i = 0; i < 3; i++)
                  {
                     szEnd = WPN_StrChr (szStart, ',', iLine);
                     g_rgiGrenadeBuyPrecent[i] = atoi (szStart);
                     szStart = szEnd + 1;
                  }
                  break;

               case 4:
                  for (i = 0; i < 10; i++)
                  {
                     szEnd = WPN_StrChr (szStart, ',', iLine);
                     g_rgiBotBuyEconomyTable[i] = atoi (szStart);
                     szStart = szEnd + 1;
                  }
                  break;
               }
               iParseWeapons++;
            }
         }
         else
         {
            if (strcmp (szCmd, "[NORMAL]") == 0)
               g_ptrWeaponPrefs = &g_rgiNormalWeaponPrefs[0];
            else if (strcmp (szCmd, "[AGRESSIVE]") == 0)
               g_ptrWeaponPrefs = &g_rgiAgressiveWeaponPrefs[0];
            else if (strcmp (szCmd, "[DEFENSIVE]") == 0)
               g_ptrWeaponPrefs = &g_rgiDefensiveWeaponPrefs[0];
            else
            {
               szStart = &szLine[0];
               
               for (i = 0; i < NUM_WEAPONS; i++)
               {
                  szEnd = WPN_StrChr (szStart, ',', iLine);
                  *g_ptrWeaponPrefs++ = atoi (szStart);
                  szStart = szEnd + 1;
               }
            }
         }
      }
      fp.Close ();
   }

   // RADIO/VOICE SYSTEM INITIALIZATION
   if (OpenConfig ("voice.cfg", "Couldn't open voice system configuration", &fp) && (g_iCSVersion != VERSION_WON))
   {
      vector <string> kArray;

      while (fp.GetString (szLine, 511))
      {
         SKIP_COMMENTS ();

         if (strncmp (szLine, "RewritePath", 11) == 0)
            CVarSetString (g_rgpcvBotCVar[CVAR_VOICEPATH]->GetName (), string (&szLine[12]).Trim ());
         else if (strncmp (szLine, "VoiceEvent", 10) == 0)
         {
            kArray = string (&szLine[11]).Split ('=');

            if (kArray.Size () != 2)
               TerminateOnError ("Error in voice config file syntax... Please correct all Errors.");

            ITERATE_ARRAY (kArray, i)
               kArray[i].Trim ().Trim (); // double trim

            // just to be more unique :)
            kArray[1].TrimLeft ('(');
            kArray[1].TrimRight (';');
            kArray[1].TrimRight (')');

            #define PARSE_VOICE_EVENT(kType, fTimeToRepeatAgain) { if (kArray[0] == #kType) ParseVoiceEvent (kArray, kType, fTimeToRepeatAgain); }

            // radio system
            PARSE_VOICE_EVENT (RADIO_COVERME, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_YOUTAKEPOINT, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_HOLDPOSITION, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_REGROUPTEAM, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_FOLLOWME, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_TAKINGFIRE, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_GOGOGO, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_FALLBACK, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_STICKTOGETHER, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_GETINPOSITION, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_STORMTHEFRONT, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_REPORTTEAM, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_AFFIRMATIVE, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_ENEMYSPOTTED, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_NEEDBACKUP, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_SECTORCLEAR, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_IMINPOSITION, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_REPORTINGIN, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_SHESGONNABLOW, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_NEGATIVE, FLT_MAX);
            PARSE_VOICE_EVENT (RADIO_ENEMYDOWN, FLT_MAX);

            // voice system
            PARSE_VOICE_EVENT (VOICE_SPOT_THE_BOMBER, 4.3);
            PARSE_VOICE_EVENT (VOICE_VIP_SPOTTED, 5.3);
            PARSE_VOICE_EVENT (VOICE_FRIENDLY_FIRE, 2.1);
            PARSE_VOICE_EVENT (VOICE_PAIN_ON_DIE, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_GET_BLINDED, 5.0); 
            PARSE_VOICE_EVENT (VOICE_GOING_TO_PLANTBOMB, FLT_MAX); 
            PARSE_VOICE_EVENT (VOICE_GOING_TO_GUARD_VIPSAFETY, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_RESCUING_HOSTAGES, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_GOING_TO_CAMP, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_TEAM_KILLING, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_REPORTING_IN, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_GUARDING_C4, 3.0);
            PARSE_VOICE_EVENT (VOICE_CAMPING, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_GUARDING_VIPSAFETY, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_PLANTING_C4, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_DEFUSING_C4, 3.0);
            PARSE_VOICE_EVENT (VOICE_IN_COMBAT, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_SEEKING_ENEMY, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_NOTHING, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_ENEMYDOWN, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_USINGHOSTAGE, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_FOUND_C4, 5.5);
            PARSE_VOICE_EVENT (VOICE_WON_ROUND, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_SCAREDEMOTE, 6.1);
            PARSE_VOICE_EVENT (VOICE_HEARDENEMY, 12.2);
            PARSE_VOICE_EVENT (VOICE_SNIPER_WARNING, 4.3);
            PARSE_VOICE_EVENT (VOICE_SNIPER_KILLED, 2.1);
            PARSE_VOICE_EVENT (VOICE_WON_ROUND_QUICK, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_ONE_ENEMY_LEFT, 2.5);
            PARSE_VOICE_EVENT (VOICE_TWO_ENEMIES_LEFT, 2.5);
            PARSE_VOICE_EVENT (VOICE_THREE_ENEMIES_LEFT, 2.5);
            PARSE_VOICE_EVENT (VOICE_NO_ONE_LEFT, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_FOUND_BOMB_PLACE, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_WHERES_THE_BOMB, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_DEFENDING_BOMBSITE, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_BARELY_DEFUSED, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_NICESHOT_COMMANDER, FLT_MAX);
            PARSE_VOICE_EVENT (VOICE_NICESHOT_PALL, 2.0);
            PARSE_VOICE_EVENT (VOICE_GOING_TO_GUARD_HOSTAGES, 3.0);
            PARSE_VOICE_EVENT (VOICE_GOING_TO_GUARD_LOOSED_C4, 3.0);
         }
         else if (strncmp (szLine, "VoicePlace", 10) == 0)
         {
            g_arPlaceNames.Cleanup ();
            kArray = string (&szLine[11]).Split ('=');

            if (kArray.Size () != 2)
               TerminateOnError ("Error in voice config file syntax... Please correct all Errors.");

            ITERATE_ARRAY (kArray, i)
               kArray[i].Trim ().Trim (); // double trim

            // just to be more unique :)
            kArray[1].TrimLeft ('(');
            kArray[1].TrimRight (';');
            kArray[1].TrimRight (')');

            ParsePlaceEvent (kArray);
         }
      }
      fp.Close ();
   }
   else
   {
      CVarSetFloat (g_rgpcvBotCVar[CVAR_COMMUNICATION]->GetName (), 1);
      LogToFile (true, LOG_DEFAULT, "Voice Communication disabled, due to your Counter-Strike Version!");
   }
   
   // LOCALIZER INITITALIZATION
   if (OpenConfig ("lang.cfg", "Specified language not found", &fp, true) && (g_iCSVersion != VERSION_WON))
   {
      if (IsDedicatedServer ())
         return; // dedicated server will use only english translation

      enum eLang { LANG_ORIG, LANG_TRAN, LANG_IDLE } iLangState = LANG_IDLE;
     
      char szBuffer[1024];
      tLanguage kTmp = {STR_NULL, STR_NULL};

      while (fp.GetString (szLine, 255))
      {
         if (strncmp (szLine, "[ORIGINAL]", 10) == 0)
         {
            iLangState = LANG_ORIG;
            
            if (!IsNullString (szBuffer))
            {
               StrTrim (szBuffer);
               kTmp.szTranslated = strdup (szBuffer);
               szBuffer[0] = 0x0;
            }

            if (!IsNullString (kTmp.szTranslated) && !IsNullString (kTmp.szOriginal))
               g_pLocalize->m_arLanguage.AddItem (kTmp);
         }
         else if (strncmp (szLine, "[TRANSLATED]", 12) == 0)
         {
            StrTrim (szBuffer);
            kTmp.szOriginal = strdup (szBuffer);
            szBuffer[0] = 0x0;

            iLangState = LANG_TRAN;
         }
         else
         {
            switch (iLangState)
            {
            case LANG_ORIG:
               strncat (szBuffer, szLine, 1024 - strlen (szBuffer));
               break;
               
            case LANG_TRAN:
               strncat (szBuffer, szLine, 1024 - strlen (szBuffer));
               break;
            }
         }
      }
      fp.Close ();
   }
   else if (g_iCSVersion == VERSION_WON)
      LogToFile (true, LOG_DEFAULT, "Multilingual system disabled, due to your Counter-Strike Version!");
   else if (strcmp (g_rgpcvBotCVar[CVAR_LANGUAGE]->GetString (), "en") != 0)
      LogToFile (true, LOG_ERROR, "Couldn't load language configuration");
}

void CommandHandler_NotMM (void)
{
   // this function is the dedicated server command handler for the new yapb server command we
   // registered at game start. It will be called by the engine each time a server command that
   // starts with "meta" is entered in the server console. It works exactly the same way as
   // ClientCommand() does, using the CmdArgc() and CmdArgv() facilities of the engine. Argv(0)
   // is the server command itself (here "meta") and the next ones are its arguments. Just like
   // the stdio command-line parsing in C when you write "long main (long argc, char **argv)".
   // this function is handler for non-metamod launch of yapb, it's just print error message.

   ServerPrint ("You're launched standalone version of yapb. Metamod is not installed or not enabled!");
}

void GameDLLInit (void)
{
   // this function is a one-time call, and appears to be the second function called in the
   // DLL after GiveFnptrsToDll() has been called. Its purpose is to tell the MOD DLL to
   // initialize the game before the engine actually hooks into it with its video frames and
   // clients connecting. Note that it is a different step than the *server* initialization.
   // This one is called once, and only once, when the game process boots up before the first
   // server is enabled. Here is a good place to do our own game session initialization, and
   // to register by the engine side the server commands we need to administrate our bots.

   // Register CVARs
   RegisterBotVariable (CVAR_AUTOVACATE, "-1");
   RegisterBotVariable (CVAR_QUOTA, "-1");
   RegisterBotVariable (CVAR_MINSKILL, "80");
   RegisterBotVariable (CVAR_MAXSKILL, "99");
   RegisterBotVariable (CVAR_FOLLOWUSER, "3");
   RegisterBotVariable (CVAR_TIMERSOUND, "0.5");
   RegisterBotVariable (CVAR_TIMERPICKUP, "0.5");
   RegisterBotVariable (CVAR_TIMERGRENADE, "0.5");
   RegisterBotVariable (CVAR_DEBUGGOAL, "-1");
   RegisterBotVariable (CVAR_CHAT, "1");
   RegisterBotVariable (CVAR_SYNTH, "1");
   RegisterBotVariable (CVAR_KNIFEMODE, "0");
   RegisterBotVariable (CVAR_SKILLTAGS, "0");
   RegisterBotVariable (CVAR_STOP, "0");
   RegisterBotVariable (CVAR_THRUWALLS, "1");
   RegisterBotVariable (CVAR_VOTES, "1");
   RegisterBotVariable (CVAR_SPRAY, "1");
   RegisterBotVariable (CVAR_BOTBUY, "1");
   RegisterBotVariable (CVAR_DEBUG, "0");
   RegisterBotVariable (CVAR_TURNMETHOD, "3");
   RegisterBotVariable (CVAR_DAMPER_COEFF_X, "0.22");
   RegisterBotVariable (CVAR_DAMPER_COEFF_Y, "0.22");
   RegisterBotVariable (CVAR_DEVIATION_X, "2.0");
   RegisterBotVariable (CVAR_DEVIATION_Y, "1.0");
   RegisterBotVariable (CVAR_INFL_X_ON_Y, "0.25");
   RegisterBotVariable (CVAR_INFL_Y_ON_X, "0.17");
   RegisterBotVariable (CVAR_SLOWDOWN_RATIO, "0.31");
   RegisterBotVariable (CVAR_OFFSET_DELAY, "1.1");
   RegisterBotVariable (CVAR_SPRING_STIFF_X, "13.0");
   RegisterBotVariable (CVAR_SPRING_STIFF_Y, "13.0");
   RegisterBotVariable (CVAR_ANTICIP_RATIO, "0.1");
   RegisterBotVariable (CVAR_PASSWORDKEY, "_botpwd");
   RegisterBotVariable (CVAR_PASSWORD, "thebot");
   RegisterBotVariable (CVAR_PREFIX, "|yb|");
   RegisterBotVariable (CVAR_LANGUAGE, "en");
   RegisterBotVariable (CVAR_VOICEPATH, "sound/radio/bot");
   RegisterBotVariable (CVAR_TKPUNISH, "1");
   RegisterBotVariable (CVAR_COMMUNICATION, "2");
   RegisterBotVariable (CVAR_ECONOMICS, "1");
   RegisterBotVariable (CVAR_FORCETEAM, "any");
   RegisterBotVariable (CVAR_RESTWEAPONS, "ump45;p90;elite;tmp;mac10;m3;xm1014");
   RegisterBotVariable (CVAR_DANGERFACTOR, "800");
   RegisterBotVariable (CVAR_DONTSHOOT, "0");
   RegisterBotVariable (CVAR_HARCOREMODE, "1");

   // this cvar is not changeable
   RegisterBotVariable (CVAR_VERSION, PRODUCT_VERSION, FCVAR_SERVER | FCVAR_EXTDLL | FCVAR_SPONLY);   

   // register server command(s)
   RegisterCommand ("yapb", CommandHandler);
   RegisterCommand ("yb", CommandHandler);

   // execute main config
   ServerCommand ("exec addons/yapb/config/yapb.cfg");

   // register fake metamod command handler if we not! under mm
   if (!g_bIsMetamod)
      RegisterCommand ("meta", CommandHandler_NotMM);

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   (*g_kFunctionTable.pfnGameInit) ();

   DetectCSVersion (); // determine version of currently running cs
}

int Spawn (edict_t *pent)
{
   // this function asks the game DLL to spawn (i.e, give a physical existence in the virtual
   // world, in other words to 'display') the entity pointed to by pentEdict in the game. The
   // Spawn() function is one of the functions any entity is supposed to have in the game DLL,
   // and any MOD is supposed to implement one for each of its entities.

   if (strcmp (STRING (pent->v.classname), "worldspawn") == 0)
   {
      // do level initialization stuff here...
      g_pWaypoint->Init ();
      g_pWaypoint->Load ();

      PRECACHE_SOUND ("weapons/xbow_hit1.wav");      // waypoint add
      PRECACHE_SOUND ("weapons/mine_activate.wav");  // waypoint delete
      PRECACHE_SOUND ("common/wpn_hudoff.wav");      // path add/delete start
      PRECACHE_SOUND ("common/wpn_hudon.wav");       // path add/delete done
      PRECACHE_SOUND ("common/wpn_moveselect.wav");  // path add/delete cancel
      PRECACHE_SOUND ("common/wpn_denyselect.wav");  // path add/delete error

      g_sModelIndexLaser = PRECACHE_MODEL ("sprites/laserbeam.spr");
      g_sModelIndexArrow = PRECACHE_MODEL ("sprites/arrow1.spr");

      g_bRoundEnded = true;
      RoundInit ();

      g_iMapType = 0; // reset maptype as worldspawn is the first entity spawned
      g_pentWorldEdict = pent; // save the world entity for future use
   }
   else if (strcmp (STRING (pent->v.classname), "player_weaponstrip") == 0)
   {
      pent->v.target = MAKE_STRING ("fake");
      pent->v.targetname = MAKE_STRING ("fake");
   }
   else if (strcmp (STRING (pent->v.classname), "info_player_start") == 0)
   {
      SET_MODEL (pent, "models/player/urban/urban.mdl");

      pent->v.rendermode = kRenderTransAlpha; // set its render mode to transparency
      pent->v.renderamt = 127; // set its transparency amount
      pent->v.effects |= EF_NODRAW;
   }
   else if (strcmp (STRING (pent->v.classname), "info_player_deathmatch") == 0)
   {
      SET_MODEL (pent, "models/player/terror/terror.mdl");

      pent->v.rendermode = kRenderTransAlpha; // set its render mode to transparency
      pent->v.renderamt = 127; // set its transparency amount
      pent->v.effects |= EF_NODRAW;
   }

   else if (strcmp (STRING (pent->v.classname), "info_vip_start") == 0)
   {
      SET_MODEL (pent, "models/player/vip/vip.mdl");

      pent->v.rendermode = kRenderTransAlpha; // set its render mode to transparency
      pent->v.renderamt = 127; // set its transparency amount
      pent->v.effects |= EF_NODRAW;
   }
   else if ((strcmp (STRING (pent->v.classname), "func_vip_safetyzone") == 0) || (strcmp (STRING (pent->v.classname), "info_vip_safetyzone") == 0))
      g_iMapType |= MAP_AS; // assassination map

   else if (strcmp (STRING (pent->v.classname), "hostage_entity") == 0)
      g_iMapType |= MAP_CS; // rescue map

   else if ((strcmp (STRING (pent->v.classname), "func_bomb_target") == 0) || (strcmp (STRING (pent->v.classname), "info_bomb_target") == 0))
      g_iMapType |= MAP_DE; // defusion map
       
   else if (strcmp (STRING (pent->v.classname), "func_escapezone") == 0)
      g_iMapType |= MAP_ES;

   // next maps doesn't have map-specific entitites, so determine it by name
   else if (strncmp (GetMapName (), "fy_", 3) == 0) // fun map
      g_iMapType |= MAP_FY;
   else if (strncmp (GetMapName (), "ka_", 3) == 0) // knife arena map
      g_iMapType |= MAP_KA;

   if (g_bIsMetamod)
      RETURN_META_VALUE (MRES_IGNORED, 0);

   int iResult = (*g_kFunctionTable.pfnSpawn) (pent); // get result

   if (pent->v.rendermode == kRenderTransTexture)
      pent->v.flags &= ~FL_WORLDBRUSH; // clear the FL_WORLDBRUSH flag out of transparent ents

   return iResult;
}

qboolean ClientConnect (edict_t *pentEdict, const char *pszName, const char *pszAddress, char szRejectReason[128])
{
   // this function is called in order to tell the MOD DLL that a client attempts to connect the
   // game. The entity pointer of this client is pentEdict, the name under which he connects is
   // pointed to by the pszName pointer, and its IP address string is pointed by the pszAddress
   // one. Note that this does not mean this client will actually join the game ; he could as
   // well be refused connection by the server later, because of latency timeout, unavailable
   // game resources, or whatever reason. In which case the reason why the game DLL (read well,
   // the game DLL, *NOT* the engine) refuses this player to connect will be printed in the
   // szRejectReason string in all letters. Understand that a client connecting process is done
   // in three steps. First, the client requests a connection from the server. This is engine
   // internals. When there are already too many players, the engine will refuse this client to
   // connect, and the game DLL won't even notice. Second, if the engine sees no problem, the
   // game DLL is asked. This is where we are. Once the game DLL acknowledges the connection,
   // the client downloads the resources it needs and synchronizes its local engine with the one
   // of the server. And then, the third step, which comes *AFTER* ClientConnect (), is when the
   // client officially enters the game, through the ClientPutInServer () function, later below.
   // Here we hook this function in order to keep track of the listen server client entity,
   // because a listen server client always connects with a "loopback" address string. Also we
   // tell the bot manager to check the bot population, in order to always have one free slot on
   // the server for incoming clients.
            
   // check if this client is the listen server client
   if (strcmp (pszAddress, "loopback") == 0)
      g_pentHostEdict = pentEdict; // save the edict of the listen server client...

   if (g_bIsMetamod)
      RETURN_META_VALUE (MRES_IGNORED, 0);

   return (*g_kFunctionTable.pfnClientConnect) (pentEdict, pszName, pszAddress, szRejectReason);
}

void ClientDisconnect (edict_t *pentEdict)
{
   // this function is called whenever a client is VOLUNTARILY disconnected from the server,
   // either because the client dropped the connection, or because the server dropped him from
   // the game (latency timeout). The effect is the freeing of a client slot on the server. Note
   // that clients and bots disconnected because of a level change NOT NECESSARILY call this
   // function, because in case of a level change, it's a server shutdown, and not a normal
   // disconnection. I find that completely stupid, but that's it. Anyway it's time to update
   // the bots and players counts, and in case the client disconnecting is a bot, to back its
   // brain(s) up to disk. We also try to notice when a listenserver client disconnects, so as
   // to reset his entity pointer for safety. There are still a few server frames to go once a
   // listen server client disconnects, and we don't want to send him any sort of message then.

   int i = ENTINDEX (pentEdict) - 1;

   InternalAssert ((i >= 0) && (i < 32));

   // check if its a bot
   if (g_pBotManager->GetBot (i))
   {
      if (g_pBotManager->GetBot (i)->pev == &pentEdict->v)
         g_pBotManager->Free (i);
   }

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   (*g_kFunctionTable.pfnClientDisconnect) (pentEdict);
}

void ClientUserInfoChanged (edict_t *pentEdict, char *infobuffer)
{
   // this function is called when a player changes model, or changes team. Occasionally it
   // enforces rules on these changes (for example, some MODs don't want to allow players to
   // change their player model). But most commonly, this function is in charge of handling
   // team changes, recounting the teams population, etc...
   
   const char *cszPasswordField = g_rgpcvBotCVar[CVAR_PASSWORDKEY]->GetString ();
   const char *cszPassword = g_rgpcvBotCVar[CVAR_PASSWORD]->GetString ();
   
   if (IsNullString (cszPasswordField) || IsNullString (cszPassword) || IsValidBot (pentEdict))
   {
      if (g_bIsMetamod)
         RETURN_META (MRES_IGNORED);

      (*g_kFunctionTable.pfnClientUserInfoChanged) (pentEdict, infobuffer);
   }

   int iClientIndex = ENTINDEX (pentEdict) - 1;
   const char *cszAdminLoginAttempt = INFOKEY_VALUE (infobuffer, const_cast <char *> (cszPasswordField));
   
   if (FStrEq (cszPassword, cszAdminLoginAttempt))
      g_rgkClients[iClientIndex].iFlags |= CFLAG_ADMIN;
   else
      g_rgkClients[iClientIndex].iFlags &= ~CFLAG_ADMIN;

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   (*g_kFunctionTable.pfnClientUserInfoChanged) (pentEdict, infobuffer);
}

void ClientCommand (edict_t *pentEdict)
{
   // this function is called whenever the client whose player entity is pentEdict issues a client
   // command. How it works is that clients all have a global string in their client DLL that
   // stores the command string; if ever that string is filled with characters, the client DLL
   // sends it to the engine as a command to be executed. When the engine has executed that
   // command, that string is reset to zero. By the server side, we can access this string
   // by asking the engine with the CmdArgv(), CmdArgs() and CmdArgc() functions that work just
   // like executable files argument processing work in C (argc gets the number of arguments,
   // command included, args returns the whole string, and argv returns the wanted argument
   // only). Here is a good place to set up either bot debug commands the listen server client
   // could type in his game console, or real new client commands, but we wouldn't want to do
   // so as this is just a bot DLL, not a MOD. The purpose is not to add functionality to
   // clients. Hence it can lack of commenting a bit, since this code is very subject to change.

   const char *szCmd = CMD_ARGV (0);
   const char *szArg1 = CMD_ARGV (1);

   static int iFillServerTeam = 5;
   static bool bFillCmd = false;

   if (!g_bIsFakeCommand && ((pentEdict == g_pentHostEdict) || (g_rgkClients[ENTINDEX (pentEdict) - 1].iFlags & CFLAG_ADMIN)))
   {
      if (stricmp (szCmd, "yapb") == 0)
      {
         int iState = BotCommandHandler (pentEdict, IsNullString (CMD_ARGV (1)) ? "help" : CMD_ARGV (1), CMD_ARGV (2), CMD_ARGV (3), CMD_ARGV (4), CMD_ARGV (5), CMD_ARGV (6));

         switch (iState)
         {
         case 0:
            ClientPrint (pentEdict, print_withtag, "Unknown command: %s", szArg1);
            break;

         case 3:
            ClientPrint (pentEdict, print_withtag, "CVar yb_%s, can be only set via RCON access.", CMD_ARGV (2));
            break;

         case 2:
            ClientPrint (pentEdict, print_withtag, "Command %s, can only be executed from server console.", szArg1);
            break;
         }
         if (g_bIsMetamod)
            RETURN_META (MRES_SUPERCEDE);

         return;
      }
      else if ((stricmp (szCmd, "menuselect") == 0) && !IsNullString (szArg1) && (g_rgkClients[ENTINDEX (pentEdict) - 1].pMenu != NULL))
      {
         tClient *pClient = &g_rgkClients[ENTINDEX (pentEdict) - 1];
         int iSelection = atoi (szArg1);

         if (pClient->pMenu == &g_rgkMenu[12])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
               g_pWaypoint->Add (iSelection - 1);
               break;

            case 8:
               g_pWaypoint->Add (100);
               break;

            case 9:
               g_pWaypoint->SetLearnJumpWaypoint ();
               break;

            case 10:
               ShowMenu (pentEdict, NULL);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[13])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
               g_pWaypoint->ChangeFlags (W_FL_NOHOSTAGE, 1);
               break;

            case 2:
               g_pWaypoint->ChangeFlags (W_FL_TERRORIST, 1);
               break;

            case 3:
               g_pWaypoint->ChangeFlags (W_FL_COUNTER, 1);
               break;

            case 4:
               g_pWaypoint->ChangeFlags (W_FL_LIFT, 1);
               break;

            case 5:
               g_pWaypoint->ChangeFlags (W_FL_NOHOSTAGE, 0);
               break;

            case 6:
               g_pWaypoint->ChangeFlags (W_FL_TERRORIST, 0);
               break;

            case 7:
               g_pWaypoint->ChangeFlags (W_FL_COUNTER, 0);
               break;

            case 8:
               g_pWaypoint->ChangeFlags (W_FL_LIFT, 0);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[9])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
               if (g_bWaypointOn)
                  ServerCommand ("yapb waypoint off");
               else
                  ServerCommand ("yapb waypoint on");
               break;

            case 2:
               g_bWaypointOn = true;
               g_pWaypoint->CacheWaypoint ();
               break;

            case 3:
               g_bWaypointOn = true;
               ShowMenu (pentEdict, &g_rgkMenu[20]);
               break;

            case 4:
               g_bWaypointOn = true;
               g_pWaypoint->DeletePath ();
               break;

            case 5:
               g_bWaypointOn = true;
               ShowMenu (pentEdict, &g_rgkMenu[12]);
               break;

            case 6:
               g_bWaypointOn = true;
               g_pWaypoint->Delete ();
               break;

            case 7:
               g_bWaypointOn = true;
               ShowMenu (pentEdict, &g_rgkMenu[19]);
               break;

            case 8:
               g_bWaypointOn = true;
               ShowMenu (pentEdict, &g_rgkMenu[11]);
               break;

            case 9:
               ShowMenu (pentEdict, &g_rgkMenu[10]);
               break;

            case 10:
               ShowMenu (pentEdict, NULL);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[10])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
               {
                  int iTPoints = 0;
                  int iCTPoints = 0;
                  int iGoalPoints = 0;
                  int iRescuePoints = 0;
                  int iCampPoints = 0;
                  int iNoHostagePoints = 0;

                  for (int i = 0; i < g_iNumWaypoints; i++)
                  {
                     if (g_pWaypoint->GetPath (i)->iFlags & W_FL_TERRORIST)
                        iTPoints++;

                     if (g_pWaypoint->GetPath (i)->iFlags & W_FL_COUNTER)
                        iCTPoints++;

                     if (g_pWaypoint->GetPath (i)->iFlags & W_FL_GOAL)
                        iGoalPoints++;

                     if (g_pWaypoint->GetPath (i)->iFlags & W_FL_RESCUE)
                        iRescuePoints++;

                     if (g_pWaypoint->GetPath (i)->iFlags & W_FL_CAMP)
                        iCampPoints++;

                     if (g_pWaypoint->GetPath (i)->iFlags & W_FL_NOHOSTAGE)
                        iNoHostagePoints++;
                  }
                  ServerPrintNoTag ("Waypoints: %d - T Points: %d\n"
                                    "CT Points: %d - Goal Points: %d\n"
                                    "Rescue Points: %d - Camp Points: %d\n"
                                    "Block Hostage Points: %d\n", g_iNumWaypoints, iTPoints, iCTPoints, iGoalPoints, iRescuePoints, iCampPoints, iNoHostagePoints);
               }
               break;

            case 2:
               g_bWaypointOn = true;
               g_bAutoWaypoint &= 1;
               g_bAutoWaypoint ^= 1;

               CenterPrint ("Auto-Waypoint %s", g_bAutoWaypoint ? "Enabled" : "Disabled");
               break;

            case 3:
               g_bWaypointOn = true;
               ShowMenu (pentEdict, &g_rgkMenu[13]);
               break;

            case 4:
               if (g_pWaypoint->NodesValid ())
                  g_pWaypoint->Save ();
               else
                  CenterPrint ("Waypoint not saved\nThere are errors, see console");      
               break;

            case 5:
               g_pWaypoint->Save ();
               break;

            case 6:
               g_pWaypoint->Load ();
               break;

            case 7:
               if (g_pWaypoint->NodesValid ())
                  CenterPrint ("Nodes work Find");
               else
                  CenterPrint ("There are errors, see console");
               break;

            case 8:
               ServerCommand ("yapb wp on noclip");
               break;

            case 9:
               ShowMenu (pentEdict, &g_rgkMenu[9]);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[11])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            g_bWaypointOn = true;  // turn waypoints on in case

            const int iRadiusValue[] = {0, 8, 16, 32, 48, 64, 80, 96, 128};

            if ((iSelection >= 1) && (iSelection <= 9))
               g_pWaypoint->SetRadius (iRadiusValue[iSelection - 1]);

            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[0])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
               bFillCmd = false;
               ShowMenu (pentEdict, &g_rgkMenu[2]);
               break;

            case 2:
               ShowMenu (pentEdict, &g_rgkMenu[1]);
               break;

            case 3:
               bFillCmd = true;
               ShowMenu (pentEdict, &g_rgkMenu[6]);
               break;

            case 4:
               g_pBotManager->KillAll ();
               break;

            case 10:
               ShowMenu (pentEdict, NULL);
               break;

            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[2])
         {  
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
               g_pBotManager->AddRandom ();
               break;

            case 2:
               ShowMenu (pentEdict, &g_rgkMenu[5]);
               break;

            case 3:
               g_pBotManager->RemoveRandom ();
               break;

            case 4:
               g_pBotManager->RemoveAll ();
               break;

            case 5:
               g_pBotManager->RemoveMenu (pentEdict, 1);
               break;

            case 10:
               ShowMenu (pentEdict, NULL);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[1])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
               ShowMenu (pentEdict, &g_rgkMenu[3]);
               break;

            case 2:
               ShowMenu (pentEdict, &g_rgkMenu[9]);
               break;

            case 3:
               ShowMenu (pentEdict, &g_rgkMenu[4]);
               break;

            case 4:
               CVarSetFloat (g_rgpcvBotCVar[CVAR_DEBUG]->GetName (), g_rgpcvBotCVar[CVAR_DEBUG]->GetInt () ^ 1);
               break;

            case 5:
               if (IsAlive (pentEdict)) 
                  ShowMenu (pentEdict, &g_rgkMenu[18]);
               else
               {
                  ShowMenu (pentEdict, NULL); // reset menu display
                  CenterPrint ("You're dead, and have no access to this menu");
               }
               break;

            case 10:
               ShowMenu (pentEdict, NULL);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[18])
         {
            ShowMenu (pentEdict, NULL); // reset menu display
            CBot *pBot = NULL;

            switch (iSelection)
            {
            case 1:
            case 2:
               if (FindNearestPlayer (reinterpret_cast <void **> (&pBot), pClient->pentEdict, 4096.0, true, true, true))
               {
                  if (!(pBot->pev->weapons & (1 << WEAPON_C4)) && !pBot->HasHostage () && (pBot->CurrentTask ()->iTask != TASK_PLANTBOMB) && (pBot->CurrentTask ()->iTask != TASK_DEFUSEBOMB))
                  {
                     if (iSelection == 1)
                     {
                        pBot->ResetDoubleJumpState ();

                        pBot->m_vDoubleJumpOrigin = pClient->pentEdict->v.origin;
                        pBot->m_pentDoubleJumpEdict = pClient->pentEdict;
                        
                        pBot->StartTask (TASK_DOUBLEJUMP, TASKPRI_DOUBLEJUMP, -1, WorldTime (), true);
                        pBot->TeamSayText (VarArgs ("Ok %s, i will help you!", STRING (pentEdict->v.netname)));
                     }
                     else if (iSelection == 2)
                        pBot->ResetDoubleJumpState ();
                     break;
                  }
               }
               break;

            case 3:
            case 4:
               if (FindNearestPlayer (reinterpret_cast <void **> (&pBot), pentEdict, 300.0, true, true, true))
                  pBot->DiscardWeaponForUser (pentEdict, iSelection == 4 ? false : true);
               break;

            case 10:
               ShowMenu (pentEdict, NULL);
               break;
            }

            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[19])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            const int iAutoDistanceValue[] = {0, 100, 130, 160, 190, 220, 250};

            if ((iSelection >= 1) && (iSelection <= 7))
               g_fAutoPathDistance = iAutoDistanceValue[iSelection - 1];

            if (g_fAutoPathDistance == 0)
               CenterPrint ("AutoPath disabled");
            else
               CenterPrint ("AutoPath maximum distance set to %f", g_fAutoPathDistance); 

            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[20])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
               g_pWaypoint->CreatePath (PATH_OUTGOING);
               break;

            case 2:
               g_pWaypoint->CreatePath (PATH_INCOMING);
               break;

            case 3:
               g_pWaypoint->CreatePath (PATH_BOTHWAYS);
               break;

            case 10:
               ShowMenu (pentEdict, NULL);
               break;
            }

            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[5])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            pClient->pMenu = &g_rgkMenu[4];

            switch (iSelection)
            {
            case 1:
               g_rgiStoreAddbotVars[0] = RandomLong (0, 20);
               break;

            case 2:
               g_rgiStoreAddbotVars[0] = RandomLong (20, 40);
               break;

            case 3:
               g_rgiStoreAddbotVars[0] = RandomLong (40, 60);
               break;

            case 4:
               g_rgiStoreAddbotVars[0] = RandomLong (60, 80);
               break;

            case 5:
               g_rgiStoreAddbotVars[0] = RandomLong (80, 99);
               break;

            case 6:
               g_rgiStoreAddbotVars[0] = 100;
               break;

            case 10:
               ShowMenu (pentEdict, NULL);
               break;
            }

            if (pClient->pMenu == &g_rgkMenu[4])
               ShowMenu (pentEdict, &g_rgkMenu[4]);

            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if ((pClient->pMenu == &g_rgkMenu[6]) && bFillCmd)
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
            case 2:
               // Turn off CVARS if specified Team 
               CVarSetString ("mp_limitteams", "0");
               CVarSetString ("mp_autoteambalance", "0");

            case 5:
               iFillServerTeam = iSelection;
               ShowMenu (pentEdict, &g_rgkMenu[4]);
               break;

            case 10:
               ShowMenu (pentEdict, NULL);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if ((pClient->pMenu == &g_rgkMenu[4]) && bFillCmd)
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
            case 2:
            case 3:
            case 4:
               g_pBotManager->FillServer (iFillServerTeam, iSelection - 2);

            case 10:
               ShowMenu (pentEdict, NULL);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[6])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
            case 2:
            case 5:
               g_rgiStoreAddbotVars[1] = iSelection;
               if (iSelection == 5)
               {
                  g_rgiStoreAddbotVars[2] = 5;
                  g_pBotManager->AddBot ("", g_rgiStoreAddbotVars[0], g_rgiStoreAddbotVars[3], g_rgiStoreAddbotVars[1], g_rgiStoreAddbotVars[2]);
               }
               else
               {
                  if (iSelection == 1)
                     ShowMenu (pentEdict, &g_rgkMenu[7]);
                  else
                     ShowMenu (pentEdict, &g_rgkMenu[8]);
               }
               break;

            case 10:
               ShowMenu (pentEdict, NULL);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[4])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
            case 2:
            case 3:
            case 4:
               g_rgiStoreAddbotVars[3] = iSelection - 2;
               ShowMenu (pentEdict, &g_rgkMenu[6]);
               break;

            case 10:
               ShowMenu (pentEdict, NULL);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if ((pClient->pMenu == &g_rgkMenu[7]) || (pClient->pMenu == &g_rgkMenu[8]))
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
               g_rgiStoreAddbotVars[2] = iSelection;
               g_pBotManager->AddBot ("", g_rgiStoreAddbotVars[0], g_rgiStoreAddbotVars[3], g_rgiStoreAddbotVars[1], g_rgiStoreAddbotVars[2]);
               break;

            case 10:
               ShowMenu (pentEdict, NULL);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[3])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
               g_pBotManager->SetWeaponMode (iSelection);
               break;

            case 10:
               ShowMenu (pentEdict, NULL);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[14])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
               g_pBotManager->GetBot (iSelection - 1)->Kick ();
               break;

            case 9:
               g_pBotManager->RemoveMenu (pentEdict, 2);
               break;

            case 10:
               ShowMenu (pentEdict, &g_rgkMenu[2]);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[15])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
               g_pBotManager->GetBot (iSelection + 8 - 1)->Kick ();
               break;

            case 9:
               g_pBotManager->RemoveMenu (pentEdict, 3);
               break;

            case 10:
               g_pBotManager->RemoveMenu (pentEdict, 1);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[16])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
               g_pBotManager->GetBot (iSelection + 16 - 1)->Kick ();
               break;

            case 9:
               g_pBotManager->RemoveMenu (pentEdict, 4);
               break;

            case 10:
               g_pBotManager->RemoveMenu (pentEdict, 2);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
         else if (pClient->pMenu == &g_rgkMenu[17])
         {
            ShowMenu (pentEdict, NULL); // reset menu display

            switch (iSelection)
            {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
               g_pBotManager->GetBot (iSelection + 24 - 1)->Kick ();
               break;

            case 10:
               g_pBotManager->RemoveMenu (pentEdict, 3);
               break;
            }
            if (g_bIsMetamod)
               RETURN_META (MRES_SUPERCEDE);

            return;
         }
      }
   }
   else if (!g_bIsFakeCommand && ((stricmp (szCmd, "say") == 0) || (stricmp (szCmd, "say_team") == 0)))
   {
      CBot *pBot = NULL;

      if (FStrEq (szArg1, "dropme") || FStrEq (szArg1, "dropc4"))
      {
         if (FindNearestPlayer (reinterpret_cast <void **> (&pBot), pentEdict, 300.0, true, true, true))
            pBot->DiscardWeaponForUser (pentEdict, IsNullString (strstr (szArg1, "c4")) ? false : true);
         return;
      }
      
      bool bAlive = IsAlive (pentEdict);
      int iTeam = -1;

      if (FStrEq (szCmd, "say_team"))
         iTeam = GetTeam (pentEdict);

      for (int i = 0; i < MaxClients (); i++)
      {
         if (!(g_rgkClients[i].iFlags & (CFLAG_USED)) || ((iTeam != -1) && (iTeam != g_rgkClients[i].iTeam)) || (bAlive != IsAlive (g_rgkClients[i].pentEdict)))
            continue;

         CBot *pBot = g_pBotManager->GetBot (i);

         if (pBot != NULL)
         {
            pBot->m_sSaytextBuffer.iEntityIndex = ENTINDEX (pentEdict);
            
            if (IsNullString (CMD_ARGS ()))
               continue;

            strcpy (pBot->m_sSaytextBuffer.szSayText, CMD_ARGS ());
            pBot->m_sSaytextBuffer.fTimeNextChat = WorldTime () + pBot->m_sSaytextBuffer.fChatDelay;
         }
      }
   }

   if (IsAlive (pentEdict))
   {
      // Check radio commands
      int iClientIndex = ENTINDEX (pentEdict) - 1;

      if (FStrEq (szCmd, "radio1"))
         g_rgiRadioSelect[iClientIndex] = 1;
      else if (FStrEq (szCmd, "radio2"))
         g_rgiRadioSelect[iClientIndex] = 3;
      else if (FStrEq (szCmd, "radio3"))
         g_rgiRadioSelect[iClientIndex] = 2;

      if (g_rgiRadioSelect[iClientIndex] > 0)
      {
         if (FStrEq (szCmd, "menuselect"))
         {
            int iRadioCommand = atoi (szArg1);

            if (iRadioCommand != 0)
            {
               if (g_rgiRadioSelect[iClientIndex] == 3)
                  iRadioCommand += 10;
               else if (g_rgiRadioSelect[iClientIndex] == 2)
                  iRadioCommand += 20;

               int iTeam = g_rgkClients[iClientIndex].iTeam;

               if ((iRadioCommand != RADIO_AFFIRMATIVE) && (iRadioCommand != RADIO_NEGATIVE) && (iRadioCommand != RADIO_REPORTINGIN))
               {
                  for (int i = 0; i < MaxClients (); i++)
                  {
                     if (g_pBotManager->GetBot (i))
                     {
                        if ((GetTeam (g_pBotManager->GetBot (i)->GetEntity ()) == iTeam) && (VARS (pentEdict) != g_pBotManager->GetBot (i)->pev))
                        {
                           if (g_pBotManager->GetBot (i)->m_iRadioOrder == 0)
                           {
                              g_pBotManager->GetBot (i)->m_iRadioOrder = iRadioCommand;
                              g_pBotManager->GetBot (i)->m_pentRadioEntity = pentEdict;
                           }
                        }
                     }
                  }
               }
               g_rgfLastRadioTime[iTeam] = WorldTime ();
            }
            g_rgiRadioSelect[iClientIndex] = 0;
         }
      }
   }

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   (*g_kFunctionTable.pfnClientCommand) (pentEdict);
}

void ServerActivate (edict_t *pentEdictList, int edictCount, int clientMax)
{
   // this function is called when the server has fully loaded and is about to manifest itself
   // on the network as such. Since a mapchange is actually a server shutdown followed by a
   // restart, this function is also called when a new map is being loaded. Hence it's the
   // perfect place for doing initialization stuff for our bots, such as reading the BSP data,
   // loading the bot profiles, and drawing the world map (ie, filling the navigation hashtable).
   // Once this function has been called, the server can be considered as "running".

   InitConfig (); // initialize all config files
   
   if (IsFileAbleToOpen (VarArgs ("%s/maps/%s_yapb.cfg", GetModName (), GetMapName ())))
   {
      ServerCommand ("exec maps/%s_yapb.cfg", GetMapName ());
      ServerPrint ("Executing Map-Specific config file");
   }
   g_pBotManager->InitQuota ();

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   (*g_kFunctionTable.pfnServerActivate) (pentEdictList, edictCount, clientMax);

   g_pWaypoint->InitializeVisibility ();
}

void ServerDeactivate (void)
{
   // this function is called when the server is shutting down. A particular note about map
   // changes: changing the map means shutting down the server and starting a new one. Of course
   // this process is transparent to the user, but either in single player when the hero reaches
   // a new level and in multiplayer when it's time for a map change, be aware that what happens
   // is that the server actually shuts down and restarts with a new map. Hence we can use this
   // function to free and deinit anything which is map-specific, for example we free the memory
   // space we m'allocated for our BSP data, since a new map means new BSP data to interpret. In
   // any case, when the new map will be booting, ServerActivate() will be called, so we'll do
   // the loading of new bots and the new BSP data parsing there.

   // save collected experience on shutdown
   g_pWaypoint->SaveExperienceTab ();
   g_pWaypoint->SaveVisibilityTab ();

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   (*g_kFunctionTable.pfnServerDeactivate) ();
}

void StartFrame (void)
{
   // this function starts a video frame. It is called once per video frame by the engine. If
   // you run Half-Life at 90 fps, this function will then be called 90 times per second. By
   // placing a hook on it, we have a good place to do things that should be done continuously
   // during the game, for example making the bots think (yes, because no Think() function exists
   // for the bots by the MOD side, remember). Also here we have control on the bot population,
   // for example if a new player joins the server, we should disconnect a bot, and if the
   // player population decreases, we should fill the server with other bots.

   // record some stats of all players on the server
   for (int i = 1; i <= MaxClients (); i++)
   {
      edict_t *pentPlayer = INDEXENT (i);
      int iStoreIndex = i - 1;

      if (IsValidPlayer (pentPlayer))
      {
         g_rgkClients[iStoreIndex].pentEdict = pentPlayer;
         g_rgkClients[iStoreIndex].iFlags |= CFLAG_USED;

         if (IsAlive (pentPlayer))
            g_rgkClients[iStoreIndex].iFlags |= CFLAG_ALIVE;
         else
            g_rgkClients[iStoreIndex].iFlags &= ~CFLAG_ALIVE;

         if (g_rgkClients[iStoreIndex].iFlags & CFLAG_ALIVE)
         {
            // keep the clipping mode enabled, or it can be turned off after new round has started
            if ((g_pentHostEdict == pentPlayer) && g_bEditNoclip)
               g_pentHostEdict->v.movetype = MOVETYPE_NOCLIP;
   
            g_rgkClients[iStoreIndex].vOrigin = pentPlayer->v.origin;
            SoundSimulateUpdate (iStoreIndex);
         }
      }
      else
      {
         g_rgkClients[iStoreIndex].iFlags &= ~CFLAG_USED;
         g_rgkClients[iStoreIndex].pentEdict = NULL;
      }
 
      if (!(g_rgkClients[iStoreIndex].iFlags & CFLAG_ADMIN))
         continue;

      if (IsNullString (g_rgpcvBotCVar[CVAR_PASSWORDKEY]->GetString ()) || IsNullString (g_rgpcvBotCVar[CVAR_PASSWORD]->GetString ()))
         g_rgkClients[iStoreIndex].iFlags &= ~CFLAG_ADMIN;
      else
      {
         char *szInfobuffer = GET_INFOKEYBUFFER (g_rgkClients[i].pentEdict);

         if (FStrEq (g_rgpcvBotCVar[CVAR_PASSWORD]->GetString (), INFOKEY_VALUE (szInfobuffer, const_cast <char *> (g_rgpcvBotCVar[CVAR_PASSWORDKEY]->GetString ()))))
            g_rgkClients[iStoreIndex].iFlags |= CFLAG_ADMIN;
         else
            g_rgkClients[iStoreIndex].iFlags &= ~CFLAG_ADMIN;
      }
   }

   static float fTimePreviousThink = 0.0;
   static float fCVarUpdateTime = 0.0;

   // calculate own frame interval
   g_fTimeFrameInterval = WorldTime () - fTimePreviousThink;
   fTimePreviousThink = WorldTime ();
   
   // **** AI EXECUTION STARTS ****
   g_pBotManager->Think ();
   // **** AI EXECUTION FINISH ****

   if (!IsDedicatedServer () && !FNullEnt (g_pentHostEdict))
   {
      if (g_bWaypointOn)
         g_pWaypoint->Think ();

      CheckWelcomeMessage ();
   }

   // keep bot number up to date
   g_pBotManager->MaintainBotQuota ();

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   (*g_kFunctionTable.pfnStartFrame) ();
}

int Spawn_Post (edict_t *pent)
{
   // this function asks the game DLL to spawn (i.e, give a physical existence in the virtual
   // world, in other words to 'display') the entity pointed to by pentEdict in the game. The
   // Spawn() function is one of the functions any entity is supposed to have in the game DLL,
   // and any MOD is supposed to implement one for each of its entities. Post version called
   // only by metamod.

   // solves the bots unable to see through certain types of glass bug.
   if (pent->v.rendermode == kRenderTransTexture)
      pent->v.flags &= ~FL_WORLDBRUSH; // clear the FL_WORLDBRUSH flag out of transparent ents

   RETURN_META_VALUE (MRES_IGNORED, 0);
}

void ServerActivate_Post (edict_t *pentEdictList, int edictCount, int clientMax)
{
   // this function is called when the server has fully loaded and is about to manifest itself
   // on the network as such. Since a mapchange is actually a server shutdown followed by a
   // restart, this function is also called when a new map is being loaded. Hence it's the
   // perfect place for doing initialization stuff for our bots, such as reading the BSP data,
   // loading the bot profiles, and drawing the world map (ie, filling the navigation hashtable).
   // Once this function has been called, the server can be considered as "running". Post version
   // called only by metamod.

   g_pWaypoint->InitializeVisibility ();

   RETURN_META (MRES_IGNORED);
}

void GameDLLInit_Post (void)
{
   // this function is a one-time call, and appears to be the second function called in the
   // DLL after GiveFnptrsToDll() has been called. Its purpose is to tell the MOD DLL to
   // initialize the game before the engine actually hooks into it with its video frames and
   // clients connecting. Note that it is a different step than the *server* initialization.
   // This one is called once, and only once, when the game process boots up before the first
   // server is enabled. Here is a good place to do our own game session initialization, and
   // to register by the engine side the server commands we need to administrate our bots. Post
   // version, called only by metamod.

   DetectCSVersion (); // determine version of currently running cs
   RETURN_META (MRES_IGNORED);
}

gamedll_funcs_t gGameDLLFunc;

export int GetEntityAPI2 (DLL_FUNCTIONS *pFunctionTable, int *interfaceVersion)
{
   // this function is called right after GiveFnptrsToDll() by the engine in the game DLL (or
   // what it BELIEVES to be the game DLL), in order to copy the list of MOD functions that can
   // be called by the engine, into a memory block pointed to by the pFunctionTable pointer
   // that is passed into this function (explanation comes straight from botman). This allows
   // the Half-Life engine to call these MOD DLL functions when it needs to spawn an entity,
   // connect or disconnect a player, call Think() functions, Touch() functions, or Use()
   // functions, etc. The bot DLL passes its OWN list of these functions back to the Half-Life
   // engine, and then calls the MOD DLL's version of GetEntityAPI to get the REAL gamedll
   // functions this time (to use in the bot code).

   memset (pFunctionTable, 0, sizeof (DLL_FUNCTIONS));

   if (!g_bIsMetamod)
   {
      // pass other DLLs engine callbacks to function table...
     if ((*g_pfnGetEntityAPI) (&g_kFunctionTable, INTERFACE_VERSION) == NULL)
     {
       LogToFile (true, LOG_CRITICAL, "GetEntityAPI2: ERROR - Not Initialized.");
       return false;  // error initializing function table!!!
     }

      gGameDLLFunc.dllapi_table = &g_kFunctionTable;
      gpGamedllFuncs = &gGameDLLFunc;

      memcpy (pFunctionTable, &g_kFunctionTable, sizeof (DLL_FUNCTIONS));
   }

   pFunctionTable->pfnGameInit = GameDLLInit;
   pFunctionTable->pfnSpawn = Spawn;
   pFunctionTable->pfnClientConnect = ClientConnect;
   pFunctionTable->pfnClientDisconnect = ClientDisconnect;
   pFunctionTable->pfnClientUserInfoChanged = ClientUserInfoChanged;
   pFunctionTable->pfnClientCommand = ClientCommand;
   pFunctionTable->pfnServerActivate = ServerActivate;
   pFunctionTable->pfnServerDeactivate = ServerDeactivate;
   pFunctionTable->pfnStartFrame = StartFrame;

   return true;
}

export int GetEntityAPI2_Post (DLL_FUNCTIONS *pFunctionTable, int *interfaceVersion)
{
   // this function is called right after GiveFnptrsToDll() by the engine in the game DLL (or
   // what it BELIEVES to be the game DLL), in order to copy the list of MOD functions that can
   // be called by the engine, into a memory block pointed to by the pFunctionTable pointer
   // that is passed into this function (explanation comes straight from botman). This allows
   // the Half-Life engine to call these MOD DLL functions when it needs to spawn an entity,
   // connect or disconnect a player, call Think() functions, Touch() functions, or Use()
   // functions, etc. The bot DLL passes its OWN list of these functions back to the Half-Life
   // engine, and then calls the MOD DLL's version of GetEntityAPI to get the REAL gamedll
   // functions this time (to use in the bot code). Post version, called only by metamod.

   memset (pFunctionTable, 0, sizeof (DLL_FUNCTIONS));

   pFunctionTable->pfnSpawn = Spawn_Post;
   pFunctionTable->pfnServerActivate = ServerActivate_Post;
   pFunctionTable->pfnGameInit = GameDLLInit_Post;

   return true;
}

export int GetNewDLLFunctions (NEW_DLL_FUNCTIONS *pFunctionTable, int *interfaceVersion)
{
   // it appears that an extra function table has been added in the engine to gamedll interface
   // since the date where the first enginefuncs table standard was frozen. These ones are
   // facultative and we don't hook them, but since some MODs might be featuring it, we have to
   // pass them too, else the DLL interfacing wouldn't be complete and the game possibly wouldn't
   // run properly.

   if (g_pfnGetNewEntityAPI == NULL)
      return false;

   if (!(*g_pfnGetNewEntityAPI) (pFunctionTable, interfaceVersion))
   {
      LogToFile (true, LOG_CRITICAL, "GetNewDLLFunctions: ERROR - Not Initialized.");
      return false;
   }

   gGameDLLFunc.newapi_table = pFunctionTable;
   return true;
}

export int Server_GetBlendingInterface (int version, void **ppinterface, void *pstudio, float (*rotationmatrix)[3][4], float (*bonetransform)[128][3][4])
{
   // this function synchronizes the studio model animation blending interface (i.e, what parts
   // of the body move, which bones, which hitboxes and how) between the server and the game DLL.
   // some MODs can be using a different hitbox scheme than the standard one.

   if (g_pfnServerBlendingAPI == NULL)
      return false;

   return (*g_pfnServerBlendingAPI) (version, ppinterface, pstudio, rotationmatrix, bonetransform);
}

export int Meta_Query (char *ifvers, plugin_info_t **pPlugInfo, mutil_funcs_t *pMetaUtilFuncs)
{
   // this function is the first function ever called by metamod in the plugin DLL. Its purpose
   // is for metamod to retrieve basic information about the plugin, such as its meta-interface
   // version, for ensuring compatibility with the current version of the running metamod.

   // keep track of the pointers to metamod function tables metamod gives us
   gpMetaUtilFuncs = pMetaUtilFuncs;
   *pPlugInfo = &Plugin_info;

   // check for interface version compatibility
   if (strcmp (ifvers, Plugin_info.ifvers) != 0)
   {
      int iMetaMajor = 0, iMetaMinor = 0, iPluginMajor = 0, iPluginMinor = 0;

      LOG_CONSOLE (PLID, "%s: meta-interface version mismatch (metamod: %s, %s: %s)", Plugin_info.name, ifvers, Plugin_info.name, Plugin_info.ifvers);
      LOG_MESSAGE (PLID, "%s: meta-interface version mismatch (metamod: %s, %s: %s)", Plugin_info.name, ifvers, Plugin_info.name, Plugin_info.ifvers);

      // if plugin has later interface version, it's incompatible (update metamod)
      sscanf (ifvers, "%d:%d", &iMetaMajor, &iMetaMinor);
      sscanf (META_INTERFACE_VERSION, "%d:%d", &iPluginMajor, &iPluginMinor);

      if ((iPluginMajor > iMetaMajor) || ((iPluginMajor == iMetaMajor) && (iPluginMinor > iMetaMinor)))
      {
         LOG_CONSOLE (PLID, "metamod version is too old for this plugin; update metamod");
         LOG_MMERROR (PLID, "metamod version is too old for this plugin; update metamod");

         return false;
      }

      // if plugin has older major interface version, it's incompatible (update plugin)
      else if (iPluginMajor < iMetaMajor)
      {
         LOG_CONSOLE (PLID, "metamod version is incompatible with this plugin; please find a newer version of this plugin");
         LOG_MMERROR (PLID, "metamod version is incompatible with this plugin; please find a newer version of this plugin");
         
         return false;
      }
   }
   return true; // tell metamod this plugin looks safe
}

export int Meta_Attach (PLUG_LOADTIME now, metamod_funcs_t *pFunctionTable, meta_globals_t *pMGlobals, gamedll_funcs_t *pGamedllFuncs)
{
   // this function is called when metamod attempts to load the plugin. Since it's the place
   // where we can tell if the plugin will be allowed to run or not, we wait until here to make
   // our initialization stuff, like registering CVARs and dedicated server commands.

   // are we allowed to load this plugin now ?
   if (now > Plugin_info.loadable)
   {
      LOG_CONSOLE (PLID, "%s: plugin NOT attaching (can't load plugin right now)", Plugin_info.name);
      LOG_MMERROR (PLID, "%s: plugin NOT attaching (can't load plugin right now)", Plugin_info.name);
      
      return false; // returning false prevents metamod from attaching this plugin
   }

   // keep track of the pointers to engine function tables metamod gives us
   gpMetaGlobals = pMGlobals;
   memcpy (pFunctionTable, &gMetaFunctionTable, sizeof (metamod_funcs_t));
   gpGamedllFuncs = pGamedllFuncs;

   return true; // returning true enables metamod to attach this plugin
}

export int Meta_Detach (PLUG_LOADTIME now, PL_UNLOAD_REASON reason)
{
   // this function is called when metamod unloads the plugin. A basic check is made in order
   // to prevent unloading the plugin if its processing should not be interrupted.
  
   // is metamod allowed to unload the plugin?
   if ((now > Plugin_info.unloadable) && (reason != PNL_CMD_FORCED))
   {
      LOG_CONSOLE (PLID, "%s: plugin not detaching (can't unload plugin right now)", Plugin_info.name);
      LOG_MMERROR (PLID, "%s: plugin not detaching (can't unload plugin right now)", Plugin_info.name);
      
      return false; // returning false prevents metamod from unloading this plugin
   }
   g_pBotManager->RemoveAll (); // kick all bots off this server

   return true;
}

export void Meta_Init (void)
{
   // this function is called by metamod, before any other interface functions. Purpose of this 
   // function to give plugin a chance to determine is plugin running under metamod or not.
   
   g_bIsMetamod = true;
}

DLL_GIVEFNPTRSTODLL GiveFnptrsToDll (enginefuncs_t *pengfuncsFromEngine, globalvars_t *pGlobals)
{
   // this is the very first function that is called in the game DLL by the engine. Its purpose
   // is to set the functions interfacing up, by exchanging the pengfuncsFromEngine function list
   // along with a pointer to the engine's global varibales structure pGlobals, with the game
   // DLL. We can there decide if we want to load the normal game DLL just behind our bot DLL,
   // or any other game DLL that is present, such as Will Day's metamod. Also, since there is
   // a known bug on Win32 platforms that prevent hook DLLs (such as our bot DLL) to be used in
   // single player games (because they don't export all the stuff they should), we may need to
   // build our own array of exported symbols from the actual game DLL in order to use it as
   // such if necessary. Nothing really bot-related is done in this function. The actual bot
   // initialization stuff will be done later, when we'll be certain to have a multiplayer game.
   
   // get the engine functions from the engine...
   memcpy (&g_engfuncs, pengfuncsFromEngine, sizeof (enginefuncs_t));
   g_pGlobals = pGlobals;

   // allocate memory for global classes
   g_pWaypoint = AllocMem <CWaypoint> ();// create waypoint
   g_pBotManager = AllocMem <CBotManager> (); // create bot manager
   g_pNetMsg = AllocMem <CNetMsg> (); // create netmessage handler
   g_pLocalize = AllocMem <CLocalizer> (); // create localizer system

   // call randomizer a couple times to improve randomness
   for (int i = 0; i < 101; i++)
      GetRandom ();

   char szGameDLLName[256];
   tModInfo *pKnowIt = FindGame (GetModName ());

   if (pKnowIt != NULL)
   {
      g_iCSVersion = pKnowIt->iModId;
      
      if (g_bIsMetamod)
         return; // we should stop the attempt for loading the real gamedll, since metamod handle this for us :)
       
      sprintf (szGameDLLName, "%s/dlls/%s", pKnowIt->szName, !IsLinux () ? pKnowIt->szWinDLL : pKnowIt->szLinuxSO);
      g_hGameLib = OpenLibrary (szGameDLLName);

      if (g_hGameLib == NULL)
      {
         // try to extract the game DLL out of the Steam cache
         LogToFile (true, LOG_WARNING, "Trying to extract dll '%s' out of the steam cache", szGameDLLName);
        
         ExtractFromSteamDLL (szGameDLLName);
         g_hGameLib = OpenLibrary (szGameDLLName);
      }
   }
   else
      LogToFile (true, LOG_CRITICAL, "Mod that you has started, not supported by this bot (gamedir: %s)", GetModName ());

   g_pfnGiveFnptrsToDll = (GIVEFNPTRSTODLL) GetProcSymbol (g_hGameLib, "GiveFnptrsToDll");
   g_pfnGetEntityAPI = (GETENTITYAPI) GetProcSymbol (g_hGameLib, "GetEntityAPI");
   g_pfnGetNewEntityAPI = (GETNEWENTAPI) GetProcSymbol (g_hGameLib, "GetNewDLLFunctions");
   g_pfnServerBlendingAPI = (GETBLENDINGAPI) GetProcSymbol (g_hGameLib, "Server_GetBlendingInterface");

   GetEngineFunctions (pengfuncsFromEngine, NULL);

   // give the engine functions to the other DLL...
   (*g_pfnGiveFnptrsToDll) (pengfuncsFromEngine, pGlobals);
}

DLL_ENTRYPOINT
{
   // dynamic library entry point, can be used for uninitialization stuff. NOT for initializing
   // anything because if you ever attempt to wander outside the scope of this function on a
   // DLL attach, LoadLibrary() will simply fail. And you can't do I/Os here either. Nice eh ?

   // dynamic library detaching ??
   if (DLL_DETACHING)
   {
      FreeLibraryMemory (); // free everything that's freeable

      if (g_hGameLib != NULL)
         CloseLibrary (g_hGameLib); // if dynamic link library of mod is load, free it
   }
   DLL_RETENTRY; // the return data type is OS specific too
}