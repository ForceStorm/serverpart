//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#include "yapb.h"

void pfnChangeLevel (char *s1, char *s2)
{
   // the purpose of this function is to ask the engine to shutdown the server and restart a
   // new one running the map whose name is s1. It is used ONLY IN SINGLE PLAYER MODE and is
   // transparent to the user, because it saves the player state and equipment and restores it
   // back in the new level. The "changelevel trigger point" in the old level is linked to the
   // new level's spawn point using the s2 string, which is formatted as follows: "trigger_name
   // to spawnpoint_name", without spaces (for example, "tr_1atotr_2lm" would tell the engine
   // the player has reached the trigger point "tr_1a" and has to spawn in the next level on the
   // spawn point named "tr_2lm".

   // save collected experience on map change
   g_pWaypoint->SaveExperienceTab ();
   g_pWaypoint->SaveVisibilityTab ();

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   CHANGE_LEVEL (s1, s2);
}

edict_t *pfnFindEntityByString (edict_t *pentEdictStartSearchAfter, const char *pszField, const char *pszValue)
{
   // round starts in counter-strike 1.5
   if (strcmp (pszValue, "info_map_parameters") == 0)
      RoundInit ();

   if (g_bIsMetamod)
      RETURN_META_VALUE (MRES_IGNORED, 0);

   return FIND_ENTITY_BY_STRING (pentEdictStartSearchAfter, pszField, pszValue);
}

void pfnEmitSound (edict_t *entity, int channel, const char *sample, float volume, float attenuation, int flags, int pitch)
{
   // this function tells the engine that the entity pointed to by "entity", is emitting a sound
   // which filename is "sample", at level "channel" (CHAN_VOICE, etc...), with "volume" as
   // loudness multiplicator (normal volume VOL_NORM is 1.0), with a pitch of "pitch" (normal
   // pitch PITCH_NORM is 100.0), and that this sound has to be attenuated by distance in air
   // according to the value of "attenuation" (normal attenuation ATTN_NORM is 0.8 ; ATTN_NONE
   // means no attenuation with distance). Optionally flags "fFlags" can be passed, which I don't
   // know the heck of the purpose. After we tell the engine to emit the sound, we have to call
   // SoundAttachToThreat() to bring the sound to the ears of the bots. Since bots have no client DLL
   // to handle this for them, such a job has to be done manually.

   SoundAttachToThreat (entity, sample, volume);

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   (*g_engfuncs.pfnEmitSound) (entity, channel, sample, volume, attenuation, flags, pitch);
}

void pfnEmitAmbientSound (edict_t *entity, float *pos, const char *samp, float vol, float attenuation, int flags, int pitch)
{
   // this function tells the engine that the entity pointed to by "entity", is emitting a sound
   // which filename is "samp", at position "pos" (pos must point to a vector), with "vol" as
   // loudness multiplicator (normal volume VOL_NORM is 1.0), with a pitch of "pitch" (normal
   // pitch PITCH_NORM is 100.0), and that this sound has to be attenuated by distance in air
   // according to the value of "attenuation" (normal attenuation ATTN_NORM is 0.8 ; ATTN_NONE
   // means no attenuation with distance).

   SoundAttachToThreat (entity, samp, vol);

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   (*g_engfuncs.pfnEmitAmbientSound) (entity, pos, samp, vol, attenuation, flags, pitch);
}

void pfnClientCommand (edict_t *pentEdict, char *szFmt, ...)
{
   // this function forces the client whose player entity is pentEdict to issue a client command.
   // How it works is that clients all have a g_argv global string in their client DLL that
   // stores the command string; if ever that string is filled with characters, the client DLL
   // sends it to the engine as a command to be executed. When the engine has executed that
   // command, this g_argv string is reset to zero. Here is somehow a curious implementation of
   // ClientCommand: the engine sets the command it wants the client to issue in his g_argv, then
   // the client DLL sends it back to the engine, the engine receives it then executes the
   // command therein. Don't ask me why we need all this complicated crap. Anyhow since bots have
   // no client DLL, be certain never to call this function upon a bot entity, else it will just
   // make the server crash. Since hordes of uncautious, not to say stupid, programmers don't
   // even imagine some players on their servers could be bots, this check is performed less than
   // sometimes actually by their side, that's why we strongly recommend to check it here too. In
   // case it's a bot asking for a client command, we handle it like we do for bot commands, ie
   // using FakeClientCommand().


   char szBuffer[1024];
   va_list ap;

   va_start (ap, szFmt);
   vsprintf (szBuffer, szFmt, ap);
   va_end (ap);

   if (IsValidBot (pentEdict))
   {
      FakeClientCommand (pentEdict, szBuffer);

      if (g_bIsMetamod)
         RETURN_META (MRES_SUPERCEDE);
   }
   else
   {
      if (g_bIsMetamod)
         RETURN_META (MRES_IGNORED);

      CLIENT_COMMAND (pentEdict, szBuffer);
   }
}

// called each time a message is about to sent 
void pfnMessageBegin (int msg_dest, int msg_type, const float *pOrigin, edict_t *ed)
{
   g_pNetMsg->Reset ();

   if (IsValidBot (ed))
   {
      if (g_rgpcvBotCVar[CVAR_DEVELOPER]->GetInt () == 2)
         LogToFile (false, LOG_DEFAULT, "Bot '%s' receives message:  %s (index: %d)", STRING (ed->v.netname), GetUserMsgName (msg_type), msg_type);
   }
   if ((msg_dest == MSG_SPEC) && (msg_type == GetUserMsgId ("HLTV")) && (g_iCSVersion != VERSION_WON))
      g_pNetMsg->SetMessage (NETMSG_HLTV);

   if (msg_type == GetUserMsgId ("WeaponList"))
      g_pNetMsg->SetMessage (NETMSG_WEAPONLIST);

   if (!FNullEnt (ed))
   {
      int iIndex = g_pBotManager->GetIndex (ed);

      // is this message for a bot?
      if (iIndex != -1)
      {
         g_pNetMsg->Reset ();
         g_pNetMsg->SetBot (g_pBotManager->GetBot (iIndex));

         // Message handling is done in usermsg.cpp
         if (msg_type == GetUserMsgId ("VGUIMenu"))
            g_pNetMsg->SetMessage (NETMSG_VGUI);
         else if (msg_type == GetUserMsgId ("ShowMenu"))
            g_pNetMsg->SetMessage (NETMSG_SHOWMENU);
         else if (msg_type == GetUserMsgId ("MetaHook"))
            g_pNetMsg->SetMessage (NETMSG_METAHOOK);
         else if (msg_type == GetUserMsgId ("AmmoX"))
            g_pNetMsg->SetMessage (NETMSG_AMMOX);
         else if (msg_type == GetUserMsgId ("AmmoPickup"))
            g_pNetMsg->SetMessage (NETMSG_AMMOPICKUP);
         else if (msg_type == GetUserMsgId ("Damage"))
            g_pNetMsg->SetMessage (NETMSG_DAMAGE);
         else if (msg_type == GetUserMsgId ("Money"))
            g_pNetMsg->SetMessage (NETMSG_MONEY);
         else if (msg_type == GetUserMsgId ("StatusIcon"))
            g_pNetMsg->SetMessage (NETMSG_STATUSICON);
         else if (msg_type == GetUserMsgId ("ScreenFade"))
            g_pNetMsg->SetMessage (NETMSG_SCREENFADE);
         else if (msg_type == GetUserMsgId ("BarTime"))
            g_pNetMsg->SetMessage (NETMSG_BARTIME);
      }
   }
   else if (msg_dest == MSG_ALL)
   {
      if (g_rgpcvBotCVar[CVAR_DEVELOPER]->GetInt () == 2)
         LogToFile (false, LOG_DEFAULT, "All clients receive message: %s", GetUserMsgName (msg_type));

      g_pNetMsg->Reset ();

      if (msg_type == GetUserMsgId ("ScoreInfo"))
         g_pNetMsg->SetMessage (NETMSG_SCOREINFO);
      if (msg_type == GetUserMsgId ("DeathMsg"))
         g_pNetMsg->SetMessage (NETMSG_DEATH);
      if (msg_type == GetUserMsgId ("TextMsg"))
         g_pNetMsg->SetMessage (NETMSG_TEXTMSGALL);

      else if (msg_type == SVC_INTERMISSION)
      {
         for (int i = 0; i < MaxClients (); i++)
         {
            CBot *pBot = g_pBotManager->GetBot (i);
            
            if (pBot != NULL)
               pBot->m_bNotKilled = false;
         }
      }
   }

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   MESSAGE_BEGIN (msg_dest, msg_type, pOrigin, ed);
}

void pfnMessageEnd (void)
{
   g_pNetMsg->Reset ();

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   MESSAGE_END ();
}

void pfnWriteByte (int iValue)
{
   // if this message is for a bot, call the client message function...
   g_pNetMsg->Execute ((void *) &iValue);

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   WRITE_BYTE (iValue);
}

void pfnWriteChar (int iValue)
{
   // if this message is for a bot, call the client message function...
   g_pNetMsg->Execute ((void *) &iValue);

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   WRITE_CHAR (iValue);
}

void pfnWriteShort (int iValue)
{
   // if this message is for a bot, call the client message function...
   g_pNetMsg->Execute ((void *) &iValue);

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   WRITE_SHORT (iValue);
}

void pfnWriteLong (int iValue)
{
   // if this message is for a bot, call the client message function...
   g_pNetMsg->Execute ((void *) &iValue);

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   WRITE_LONG (iValue);
}

void pfnWriteAngle (float fValue)
{
   // if this message is for a bot, call the client message function...
   g_pNetMsg->Execute ((void *) &fValue);

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   WRITE_ANGLE (fValue);
}

void pfnWriteCoord (float fValue)
{
   // if this message is for a bot, call the client message function...
   g_pNetMsg->Execute ((void *) &fValue);

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   WRITE_COORD (fValue);
}

void pfnWriteString (const char *sz)
{
   // check if it's the "bomb planted" message
   if (FStrEq (sz, "%!MRAD_BOMBPL") && !g_bBombPlanted)
   {
      g_bBombPlanted = g_bBombSayString = true;
      g_fTimeBombPlanted = WorldTime ();

      for (int i = 0; i < MaxClients (); i++)
      {
         CBot *pBot = g_pBotManager->GetBot (i);

         if ((pBot != NULL) && IsAlive (pBot->GetEntity ()))
         {
            pBot->DeleteSearchNodes ();
            pBot->ResetTasks ();

            // add some aggression...
            pBot->m_fAgressionLevel += 0.1;

            if (pBot->m_fAgressionLevel > 1.0)
               pBot->m_fAgressionLevel = 1.0;

            if ((RandomLong (0, 100) < 85) && (GetTeam (pBot->GetEntity ()) == TEAM_CT))
               pBot->ChatterMessage (VOICE_WHERES_THE_BOMB);
         }
      }
   }

   CBot *pBot = g_pBotManager->FindOneValidAliveBot ();

   if ((pBot != NULL) && IsAlive (pBot->GetEntity ()))
      pBot->HandleChatterMessage (sz);

   // if this message is for a bot, call the client message function...
   g_pNetMsg->Execute ((void *) sz);

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   WRITE_STRING (sz);
}

void pfnWriteEntity (int iValue)
{
   // if this message is for a bot, call the client message function...
   g_pNetMsg->Execute ((void *) &iValue);

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   WRITE_ENTITY (iValue);
}

int pfnCmd_Argc (void)
{
   // this function returns the number of arguments the current client command string has. Since
   // bots have no client DLL and we may want a bot to execute a client command, we had to
   // implement a g_argv string in the bot DLL for holding the bots' commands, and also keep
   // track of the argument count. Hence this hook not to let the engine ask an unexistent client
   // DLL for a command we are holding here. Of course, real clients commands are still retrieved
   // the normal way, by asking the engine.

   // is this a bot issuing that client command ?
   if (g_bIsFakeCommand)
   {
      if (g_bIsMetamod)
         RETURN_META_VALUE (MRES_SUPERCEDE, g_iFakeArgumentCount);

      return g_iFakeArgumentCount; // if so, then return the argument count we know
   }

   if (g_bIsMetamod)
      RETURN_META_VALUE (MRES_IGNORED, NULL);

   return CMD_ARGC (); // ask the engine how many arguments there are
}

const char *pfnCmd_Args (void)
{
   // this function returns a pointer to the whole current client command string. Since bots
   // have no client DLL and we may want a bot to execute a client command, we had to implement
   // a g_argv string in the bot DLL for holding the bots' commands, and also keep track of the
   // argument count. Hence this hook not to let the engine ask an unexistent client DLL for a
   // command we are holding here. Of course, real clients commands are still retrieved the
   // normal way, by asking the engine.

   // is this a bot issuing that client command?
   if (g_bIsFakeCommand)
   {
      // is it a "say" or "say_team" client command?
      if (strncmp ("say ", g_szFakeArgVector, 4) == 0)
      {
         if (g_bIsMetamod)
            RETURN_META_VALUE (MRES_SUPERCEDE, g_szFakeArgVector + 4);

         return g_szFakeArgVector + 4; // skip the "say" bot client command
      }
      else if (strncmp ("say_team ", g_szFakeArgVector, 9) == 0)
      {
         if (g_bIsMetamod)
            RETURN_META_VALUE (MRES_SUPERCEDE, g_szFakeArgVector + 9);

         return g_szFakeArgVector + 9; // skip the "say_team" bot client command
      }

      if (g_bIsMetamod)
         RETURN_META_VALUE (MRES_SUPERCEDE, g_szFakeArgVector);

      return g_szFakeArgVector; // else return the whole bot client command string we know
   }

   if (g_bIsMetamod)
      RETURN_META_VALUE (MRES_IGNORED, NULL);

   return CMD_ARGS (); // ask the client command string to the engine
}

const char *pfnCmd_Argv (int argc)
{
   // this function returns a pointer to a certain argument of the current client command. Since
   // bots have no client DLL and we may want a bot to execute a client command, we had to
   // implement a g_argv string in the bot DLL for holding the bots' commands, and also keep
   // track of the argument count. Hence this hook not to let the engine ask an unexistent client
   // DLL for a command we are holding here. Of course, real clients commands are still retrieved
   // the normal way, by asking the engine.

   // is this a bot issuing that client command ?
   if (g_bIsFakeCommand)
   {
      if (g_bIsMetamod)
         RETURN_META_VALUE (MRES_SUPERCEDE, GetField (g_szFakeArgVector, argc));

      return GetField (g_szFakeArgVector, argc); // if so, then return the wanted argument we know
   }
   if (g_bIsMetamod)
      RETURN_META_VALUE (MRES_IGNORED, NULL);

   return CMD_ARGV (argc); // ask the argument number "argc" to the engine
}

void pfnClientPrintf (edict_t *pentEdict, PRINT_TYPE ptype, const char *szMsg)
{
   // this function prints the text message string pointed to by szMsg by the client side of
   // the client entity pointed to by pentEdict, in a manner depending of ptype (print_console,
   // print_center or print_chat). Be certain never to try to feed a bot with this function,
   // as it will crash your server. Why would you, anyway ? bots have no client DLL as far as
   // we know, right ? But since stupidity rules this world, we do a preventive check :)

   if (IsValidBot (pentEdict))
   {
      if (g_bIsMetamod)
         RETURN_META (MRES_SUPERCEDE);
      return;
   }

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   CLIENT_PRINTF (pentEdict, ptype, szMsg);
}

void pfnSetClientMaxspeed (const edict_t *pentEdict, float fNewMaxspeed)
{
  ((edict_t *) pentEdict)->v.maxspeed = fNewMaxspeed;

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

  (*g_engfuncs.pfnSetClientMaxspeed) (pentEdict, fNewMaxspeed);
}

int pfnRegUserMsg (const char *pszName, int iSize)
{
   // this function registers a "user message" by the engine side. User messages are network
   // messages the game DLL asks the engine to send to clients. Since many MODs have completely
   // different client features (Counter-Strike has a radar and a timer, for example), network
   // messages just can't be the same for every MOD. Hence here the MOD DLL tells the engine,
   // "Hey, you have to know that I use a network message whose name is pszName and it is iSize
   // packets long". The engine books it, and returns the ID number under which he recorded that
   // custom message. Thus every time the MOD DLL will be wanting to send a message named pszName
   // using pfnMessageBegin (), it will know what message ID number to send, and the engine will
   // know what to do, only for non-metamod version

   if (g_bIsMetamod)
      RETURN_META_VALUE (MRES_IGNORED, 0);

   int iMessage = REG_USER_MSG (pszName, iSize);

   if (g_rgpcvBotCVar[CVAR_DEVELOPER]->GetInt () == 2)
      LogToFile (false, LOG_DEFAULT, "Game registering user message '%s' to index %d", pszName, iMessage);
   
   tUserMsg kMessage = {pszName, iMessage};
   g_arUserMessages.AddItem (kMessage);

   return iMessage;
}

void pfnAlertMessage (ALERT_TYPE iAlertType, char *szFmt, ...)
{
   va_list ap;
   char szBuffer[1024];

   va_start (ap, szFmt);
   vsprintf (szBuffer, szFmt, ap);
   va_end (ap);

   if (strstr (szBuffer, "_Defuse_") != NULL)
   {
      // notify all terrorists that CT is starting bomb defusing
      for (int i = 0; i < MaxClients (); i++)
      {
         CBot *pBot = g_pBotManager->GetBot (i);

         if ((pBot != NULL) && (GetTeam (pBot->GetEntity ()) == TEAM_TERRORIST) && IsAlive (pBot->GetEntity ()))
         {
            pBot->ResetTasks ();
            pBot->MoveToVector (g_pWaypoint->GetBombPosition ());
         }
      }
   }

   if (g_bIsMetamod)
      RETURN_META (MRES_IGNORED);

   (*g_engfuncs.pfnAlertMessage) (iAlertType, szBuffer);
}

const char *pfnGetPlayerAuthId (edict_t *e)
{
   if (IsValidBot (e))
   {
      if (g_bIsMetamod)
         RETURN_META_VALUE (MRES_SUPERCEDE, "0");
      
      return "0";      
   }

   if (g_bIsMetamod)
       RETURN_META_VALUE (MRES_IGNORED, 0);

   return (*g_engfuncs.pfnGetPlayerAuthId) (e); 
}

unsigned int pfnGetPlayerWONId (edict_t *e)
{
   if (IsValidBot (e))
   {
      if (g_bIsMetamod)
         RETURN_META_VALUE (MRES_SUPERCEDE, 0);
      
      return 0;
   }

   if (g_bIsMetamod)
      RETURN_META_VALUE (MRES_IGNORED, 0);

   return (*g_engfuncs.pfnGetPlayerWONId) (e); 
}

export int GetEngineFunctions (enginefuncs_t *pengfuncsFromEngine, int *interfaceVersion)
{
   if (g_bIsMetamod)
      memset (pengfuncsFromEngine, 0, sizeof (enginefuncs_t));

   pengfuncsFromEngine->pfnChangeLevel = pfnChangeLevel;
   pengfuncsFromEngine->pfnFindEntityByString = pfnFindEntityByString;
   pengfuncsFromEngine->pfnEmitSound = pfnEmitSound;
   pengfuncsFromEngine->pfnEmitAmbientSound = pfnEmitAmbientSound;
   pengfuncsFromEngine->pfnClientCommand = pfnClientCommand;
   pengfuncsFromEngine->pfnMessageBegin = pfnMessageBegin;
   pengfuncsFromEngine->pfnMessageEnd = pfnMessageEnd;
   pengfuncsFromEngine->pfnWriteByte = pfnWriteByte;
   pengfuncsFromEngine->pfnWriteChar = pfnWriteChar;
   pengfuncsFromEngine->pfnWriteShort = pfnWriteShort;
   pengfuncsFromEngine->pfnWriteLong = pfnWriteLong;
   pengfuncsFromEngine->pfnWriteAngle = pfnWriteAngle;
   pengfuncsFromEngine->pfnWriteCoord = pfnWriteCoord;
   pengfuncsFromEngine->pfnWriteString = pfnWriteString;
   pengfuncsFromEngine->pfnWriteEntity = pfnWriteEntity;
   pengfuncsFromEngine->pfnRegUserMsg = pfnRegUserMsg;
   pengfuncsFromEngine->pfnClientPrintf = pfnClientPrintf;
   pengfuncsFromEngine->pfnCmd_Args = pfnCmd_Args;
   pengfuncsFromEngine->pfnCmd_Argv = pfnCmd_Argv;
   pengfuncsFromEngine->pfnCmd_Argc = pfnCmd_Argc;
   pengfuncsFromEngine->pfnSetClientMaxspeed = pfnSetClientMaxspeed;
   pengfuncsFromEngine->pfnAlertMessage = pfnAlertMessage;
   pengfuncsFromEngine->pfnGetPlayerAuthId = pfnGetPlayerAuthId;
   pengfuncsFromEngine->pfnGetPlayerWONId = pfnGetPlayerWONId;

   return true;
}
