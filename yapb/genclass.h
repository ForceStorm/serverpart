//
// Generic Classes Library - Some useful C++ Utility Classes Library.
// Copyright (c) 2005, 2006, by Dmitry Zhukov. All Rights Reserved.
//
// This software is provided "as-is", without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1.  The origin of this software must not be misrepresented; you must not
//     claim that you wrote the original software. If you use this software in
//     a product, an acknowledgment in the product documentation would be
//     appreciated but is not required.
// 2.  Altered source versions must be plainly marked as such, and must not be
//     misrepresented as being the original software.
// 3.  This notice may not be removed or altered from any source distribution.
//

// PS. I written this since, i hate STL, and never use it!

#ifndef GENCLASS_INCLUDED
#define GENCLASS_INCLUDED

#define string CString
#define vector CArray
#define stdfile CFile

// safe array
template <typename T> class CArray
{
private:
   int m_iArraySize;
   int m_iAllocatedSize;
   T *m_pArrayData;

public:
   CArray <T> (void) : m_iArraySize (0), m_iAllocatedSize (0), m_pArrayData (NULL)
   {
      Size (0);
   }

   CArray <T> (int iSize)
   {
      Size (iSize);
   }

   CArray <T> (int iSize, const T &kElementList) : m_iArraySize (0), m_iAllocatedSize (0), m_pArrayData (NULL)
   {
      Size (iSize, &kElementList);
   }

   CArray <T> (const CArray <T> &kArray) : m_iArraySize (0), m_iAllocatedSize (0), m_pArrayData (NULL)
   {
      operator = (kArray);
   }

   virtual ~CArray <T> (void)
   {
      if (m_pArrayData != NULL)
      {
         try
         {
            delete [] m_pArrayData;
            m_pArrayData = NULL;
         }
         catch (...)
         {
            ; // added to prevent unhandled exceptions, crashing only under metamod build
         }
      }
   }

   CArray <T> &operator = (const CArray <T> &kArray)
   {
      if (this == &kArray)
         return *this;

      Size (kArray.m_iArraySize);

      for(int i = 0; i < m_iArraySize; i++)
         m_pArrayData[i] = kArray.m_pArrayData[i];

      return *this;
   }

   const T &operator [] (int iIndex) const
   {
      if ((iIndex < 0) || (iIndex >= m_iArraySize))
         assert (0);

      return m_pArrayData[iIndex];
   }

   T &operator [] (int iIndex)
   {
      if ((iIndex > 0) || (iIndex < m_iArraySize))
         return m_pArrayData[iIndex];
   }

   T &operator + (int iIndex)
   {
      return operator [] (iIndex);
   }

   inline int Size (void)
   {
      return m_iArraySize;
   }

   inline bool IsFree (void)
   {
      return !Size ();
   }

   inline void Size (int iNewSize, const T *pElementList = 0)
   {
      if (m_iArraySize == iNewSize)
         return;

      if (iNewSize > m_iArraySize)
      {
         if (iNewSize > m_iAllocatedSize)
         {
            int i;

            m_iAllocatedSize = (iNewSize < m_iAllocatedSize * 2) ? m_iAllocatedSize * 2 : iNewSize;
            T *pNew = new T[m_iAllocatedSize];

            for(i = 0; i < min (m_iArraySize, iNewSize); i++)
               pNew[i] = m_pArrayData[i];

            if (m_pArrayData)
               delete [] m_pArrayData;

            m_pArrayData = pNew;

            if (pElementList != NULL)
            {
               for (i = m_iArraySize; i <iNewSize; i++)
                  m_pArrayData[i] = *pElementList;
            }
         }
      }
      m_iArraySize = iNewSize;
   }

   inline void Cleanup (void)
   {
      Size (0);
   }

   inline T Remove (int iIndex)
   {
      if ((iIndex < 0) || (iIndex >= m_iArraySize))
         assert (0);

      T *pDest = m_pArrayData + iIndex;
      T kReturn = *pDest;

      m_iArraySize--;

      for(int i = iIndex; i < m_iArraySize; i++, pDest++)
         *pDest = *(pDest + 1);

      return kReturn;
   }

   inline void Remove (int iIndex, int iLength)
   {
      if ((iIndex < 0) || ((iIndex + iLength) > m_iArraySize))
         assert (0);

      T *pDest = m_pArrayData + iIndex;
      m_iArraySize -= iLength;

      for(int i = iIndex; i < m_iArraySize; i++, pDest++)
         *pDest = *(pDest + iLength);
   }

   inline T *FindItem (T const &kObject, int *piIndex)
   {
      for (int i = 0; i < m_iArraySize; i++)
      {
         if (m_pArrayData[i] == kObject)
         {
            if (piIndex != NULL)
               *piIndex = i;

            return &m_pArrayData[i];
         }
      }
      return NULL;
   }

   inline void Insert (int iIndex, const T &kElem)
   {
      if (iIndex <0)
         iIndex = 0;
      else if (iIndex >= m_iArraySize)
         AddItem (kElem);
      else
      {
         Size (m_iArraySize + 1);
         T *pDest = m_pArrayData + m_iArraySize - 1;

         for(int i = m_iArraySize - 1; i> iIndex; i--, pDest--)
            *pDest = *(pDest - 1);

         *(m_pArrayData + iIndex) = kElem;
      }
   }

   inline void SetToAll (const T &kElem)
   {
      for(int i = 0; i < m_iArraySize; i++)
         m_pArrayData[i] = kElem;
   }

   inline int AddItem (const T &kElem)
   {
      int iPosition = m_iArraySize;

      Size (iPosition + 1);
      operator [] (iPosition) = kElem;

      return iPosition;
   }

   inline void AddItem (const T *pElems, int iCount)
   {
      for(int i = 0; i < iCount; i++)
         AddItem (pElems[i]);
   }

   inline T PopItem (void)
   {
      if (m_iArraySize <= 0)
         assert (0);

      T kReturn = m_pArrayData[m_iArraySize - 1];
      Size (m_iArraySize - 1);

      return kReturn;
   }

   CArray <T> &operator += (const T &kElem)
   {
      AddItem (kElem);
      return *this;
   }

   inline T &First (void)
   {
      if (m_iArraySize == 0)
         assert (0);

      return m_pArrayData[0];
   }

   inline const T &First (void) const
   {
      if (m_iArraySize == 0)
         assert (0);

      return m_pArrayData[0];
   }

   inline T &Last (void)
   {
      if (m_iArraySize == 0)
         assert (0);

      return m_pArrayData[m_iArraySize - 1];
   }

   inline const T &Last (void) const
   {
      if (m_iArraySize == 0)
         assert (0);

      return m_pArrayData[m_iArraySize - 1];
   }

   inline const int LastIndex (void)
   {
      return m_iArraySize - 1;
   }

#if defined (YAPB_INCLUDED)
   inline T &Random (void)
   {
      return m_pArrayData[RandomLong (0, m_iArraySize - 1)];
   }
#endif

   // Iterator class taken from AMXXSTL by David 'BAILOPAN' Anderson
   class Iterator
   {
   protected:
      T *m_pPointer;

   public:
      Iterator (void)
      { 
         m_pPointer = NULL; 
      }

      Iterator (T *pPointer)
      { 
         m_pPointer = pPointer; 
      }

      T *Base (void)
      { 
         return m_pPointer; 
      }

      const T *Base (void) const
      { 
         return m_pPointer; 
      }

      T &operator * (void)
      { 
         return *m_pPointer; 
      }

      T *operator -> (void)
      { 
         return m_pPointer; 
      }

      Iterator &operator ++ (void)
      { 
         m_pPointer++; 
         return *this; 
      }

      Iterator operator ++ (int)
      { 
         Iterator kTmp = *this; 
         m_pPointer++; 

         return kTmp; 
      }

      Iterator &operator -- (void)
      { 
         m_pPointer--; 
         return *this; 
      }

      Iterator operator -- (int)
      { 
         Iterator kTmp = *this; 
         m_pPointer--; 
         
         return kTmp; 
      }

      bool operator == (T *pRight) const
      { 
         return (m_pPointer == pRight); 
      }

      bool operator == (const Iterator &kRight) const
      { 
         return (m_pPointer == kRight.m_pPointer); 
      }

      bool operator != (T *pRight) const
      { 
         return (m_pPointer != pRight); 
      }

      bool operator != (const Iterator &kRight) const
      { 
         return (m_pPointer != kRight.m_pPointer); 
      }

      Iterator &operator += (int iOffset)
      { 
         m_pPointer += iOffset; return (*this); 
      }

      Iterator &operator -= (int iOffset)
      { 
         m_pPointer += iOffset; 
         return (*this); 
      }

      Iterator operator + (int iOffset) const
      { 
         Iterator kTmp (*this); 
         kTmp.m_pPointer += iOffset; 

         return kTmp; 
      }

      Iterator operator - (int iOffset) const
      { 
         Iterator kTmp (*this); 
         kTmp.m_pPointer += iOffset; 
         
         return kTmp; 
      }
      
      T &operator [] (int iOffset)
      { 
         return (*(*this + iOffset)); 
      }

      const T &operator [] (int iOffset) const
      { 
         return (*(*this + iOffset)); 
      }

      bool operator < (const Iterator &kRight) const
      { 
         return m_pPointer < kRight.m_pPointer; 
      }

      bool operator > (const Iterator &kRight) const
      { 
         return m_pPointer > kRight.m_pPointer; 
      }

      bool operator <= (const Iterator &kRight) const
      { 
         return m_pPointer <= kRight.m_pPointer; 
      }

      bool operator >= (const Iterator &kRight) const
      { 
         return m_pPointer >= kRight.m_pPointer; 
      }

      int operator - (const Iterator &kRight) const
      { 
         return m_pPointer - kRight.m_pPointer; 
      }
   };

   inline Iterator Begin (void)
   { 
      return Iterator (m_pArrayData); 
   }

   inline Iterator End (void)
   { 
      return Iterator (m_pArrayData + m_iArraySize); 
   }

   inline Iterator IterAt (int iPos)
   { 
      return Iterator (m_pArrayData + iPos); 
   }
};

class CCharset
{
public:
   bool m_rgbCodes[0x100];

   inline void SetCode (int iFrom, int iTo, bool bCode)
   {
      for(int i = iFrom; i <= iTo; i++)
         m_rgbCodes[i] = bCode;
   }

   inline void ParseInput (const char **cszInput)
   {
      const unsigned char *ucBuffer = (const unsigned char *) *cszInput;
      bool bInvert = *ucBuffer == '^';

      if (bInvert)
         ucBuffer++;

      SetCode (0, 0xff, bInvert);

      if (*ucBuffer == '-')
         m_rgbCodes['-'] = !bInvert;

      while (*ucBuffer)
      {
         if (ucBuffer[0] == ']')
         {
            ucBuffer++;
            break;
         }
         if ((ucBuffer[1] == '-') && (ucBuffer[2] != 0x0))
         {
            SetCode (ucBuffer[0], ucBuffer[2], !bInvert);
            ucBuffer += 3;
         }
         else
            m_rgbCodes[*ucBuffer++] = !bInvert;
      }
      *cszInput = (const char *) ucBuffer;
   }

   inline bool IsValid (unsigned char ucCode)
   {
      return m_rgbCodes[ucCode];
   }
};

// complete safe string class
class CString
{
protected:
   char *m_pszCharData;
   int m_iAllocatedSize;
   int m_iStringLength;

 private:
   inline void ChangeAlloc (int iSize)
   {
      if (iSize <= m_iAllocatedSize)
         return;

      m_iAllocatedSize = iSize + 16;
      char *pszTmp = new char[iSize + 1];

      if (m_pszCharData != NULL)
      {
         strcpy (pszTmp, m_pszCharData);
         pszTmp[m_iStringLength] = 0;

         delete [] m_pszCharData;
      }
      m_pszCharData = pszTmp;
      m_iAllocatedSize = iSize;
   }

   inline void MoveItems (int iDestIndex, int iSourceIndex)
   {
      memmove (m_pszCharData + iDestIndex, m_pszCharData + iSourceIndex, sizeof (char) *(m_iStringLength - iSourceIndex + 1)); 
   }

   inline void GrowLength (int iLength)
   {
      int iFreeSize = m_iAllocatedSize - m_iStringLength - 1;
      if (iLength <= iFreeSize)
         return;
      int iDelta;

      if (m_iAllocatedSize > 64)
         iDelta = m_iAllocatedSize / 2;
      else if (m_iAllocatedSize > 8)
         iDelta = 16;
      else 
         iDelta = 4;

      if (iFreeSize + iDelta < iLength)
         iDelta = iLength - iFreeSize;

      ChangeAlloc (m_iAllocatedSize + iDelta);
   }

   inline void CorrectIndex (int &iIndex) const 
   {
      if (iIndex > m_iStringLength)
         iIndex = m_iStringLength;
   }

   inline void InsertSpace (int &iIndex, int iSize)
   {
      CorrectIndex (iIndex);
      GrowLength (iSize);

      MoveItems (iIndex + iSize, iIndex);
   }

  inline  bool IsTrimChar (char cInput)
   {
      return ((cInput == ' ') || (cInput == '\t') || (cInput == '\n'));
   }
    
public:
   CString (void)
   {
      m_pszCharData = NULL;
      m_iAllocatedSize = 0;
      m_iStringLength = 0;
   }

   ~CString ()
   {
      if (m_pszCharData != NULL)
         delete [] m_pszCharData;
   }

   CString (const char *cszInput)
   {
      m_pszCharData = NULL;
      m_iAllocatedSize = 0;
      m_iStringLength = 0;

      Assign (cszInput);
   }

   CString (char cInput)
   {
      m_pszCharData = NULL;
      m_iAllocatedSize = 0;
      m_iStringLength = 0;

      Assign (cInput);
   }

   CString (const CString &sInput)
   {
      m_pszCharData = NULL;
      m_iAllocatedSize = 0;
      m_iStringLength = 0;

      Assign (sInput.GetString ());
   }

   inline const char *GetString (void)
   {
      if ((m_pszCharData == NULL) || (*m_pszCharData == '\0'))
         return "";

      return &m_pszCharData[0];
   }

   inline const char *GetString (void) const
   {
      if ((m_pszCharData == NULL) || (*m_pszCharData == '\0'))
         return "";

      return &m_pszCharData[0];
   }

   inline float GetFloat (void)
   { 
      return (float) atof (m_pszCharData); 
   }

   inline int GetInteger (void)
   { 
      return atoi (m_pszCharData); 
   }

   inline void ReleaseBuffer (void) 
   { 
      ReleaseBuffer (strlen (m_pszCharData));
   }

   inline void ReleaseBuffer (int iNewLength) 
   { 
      m_pszCharData[iNewLength] = 0; 
      m_iStringLength = iNewLength; 
   }

   inline char *GetBuffer (int iMinLength)
   {
      if (iMinLength >= m_iAllocatedSize)
         ChangeAlloc (iMinLength + 1);

      return m_pszCharData;
   }

   inline char *GetBufferSetLength (int iLength)
   {
      char *szBuffer = GetBuffer (iLength);

      m_iStringLength = iLength;
      m_pszCharData[iLength] = 0;

      return (szBuffer);
   }

   inline void Append (const char *cszInput)
   {
      ChangeAlloc (m_iStringLength + strlen (cszInput) + 1);
      strcat (m_pszCharData, cszInput);

      m_iStringLength = strlen (m_pszCharData);
   }

   inline void Append (const char cInput)
   {
      ChangeAlloc (m_iStringLength + 2);

      m_pszCharData[m_iStringLength] = cInput;
      m_pszCharData[m_iStringLength++] = 0;
   }

   inline void Append (const CString &sInput)
   {
      const char *cszInput = sInput.GetString ();
      ChangeAlloc (m_iStringLength + strlen (cszInput));

      strcat (m_pszCharData, cszInput);
      m_iStringLength = strlen (m_pszCharData);
   }

   inline void AppendFormat (const char *fmt, ...)
   {
      va_list ap;
      char szBuffer[1024];

      va_start (ap, fmt);
      vsprintf (szBuffer, fmt, ap);
      va_end (ap);

      Append (szBuffer);
   }

   inline void Assign (const CString &sInput)
   {
      Assign (sInput.GetString ());
   }

   inline void Assign (char cInput)
   {
      char psz[2] = {cInput, 0};
      Assign (psz);
   }

   inline void Assign (const char *cszInput)
   {
      if (cszInput == NULL)
      {
         ChangeAlloc (1);
         m_iStringLength = 0;
         strcpy (m_pszCharData, "");

         return;
      }
      ChangeAlloc (strlen (cszInput));
      
      if (m_pszCharData != NULL)
      {
         strcpy (m_pszCharData, cszInput);
         m_iStringLength = strlen (m_pszCharData);
      }
      else
         m_iStringLength = 0;
   }

   inline void AssignFormat (const char *fmt, ...)
   {
      va_list ap;
      char szBuffer[1024];

      va_start (ap, fmt);
      vsprintf (szBuffer, fmt, ap);
      va_end (ap);

      Assign (szBuffer);
   }

   inline void Empty (void)
   {
      if (m_pszCharData != NULL)
      {
         m_pszCharData[0] = 0;
         m_iStringLength = 0;
      }
   }

   inline bool IsEmpty (void)
   {
      if ((m_pszCharData == NULL) || (m_iStringLength == 0))
         return true;

      return false;
   }

   inline int Length (void)
   {
      if (m_pszCharData == NULL)
         return 0;

      return m_iStringLength;
   }

   operator const char * (void) const 
   { 
      return GetString (); 
   }

   operator char * (void)
   {
      return const_cast <char *> (GetString ());
   }
   
   operator int (void)
   { 
      return GetInteger (); 
   }  

   operator long (void)
   {
      return static_cast <long> (GetInteger ());
   }
   
   operator float (void)
   { 
      return GetFloat (); 
   } 

   operator double (void)
   {
      return static_cast <double> (GetFloat ());
   }

   CString *operator -> (void) const 
   { 
      return (CString *const) this;
   }

   friend CString operator + (const CString &s1, const CString &s2)
   { 
      CString sResult (s1); sResult += s2; return sResult; 
   }

   friend CString operator + (const CString &szHolder, char cChar)
   { 
      CString sResult (szHolder); sResult += cChar; return sResult; 
   }

   friend CString operator + (char cChar, const CString &szHolder)
   { 
      CString sResult (cChar); sResult += szHolder; return sResult; 
   }

   friend CString operator + (const CString &szHolder, const char *szString)
   { 
      CString sResult (szHolder); sResult += szString; return sResult; 
   }

   friend CString operator + (const char *szString, const CString &szHolder)
   { 
      CString sResult ((char *)szString); sResult += szHolder; return sResult; 
   }

   friend bool operator == (const CString &s1, const CString &s2)
   { 
      return (s1.Compare (s2) == 0); 
   }

   friend bool operator < (const CString &s1, const CString &s2)
   { 
      return (s1.Compare (s2) < 0); 
   }

   friend bool operator > (const CString &s1, const CString &s2)
   { 
      return (s1.Compare (s2) > 0); 
   }

   friend bool operator == (const char *s1, const CString &s2)
   { 
      return (s2.Compare (s1) == 0); 
   }

   friend bool operator == (const CString &s1, const char *s2)
   { 
      return (s1.Compare (s2) == 0); 
   }

   friend bool operator != (const CString &s1, const CString &s2)
   { 
      return (s1.Compare (s2) != 0); 
   }

   friend bool operator != (const char *s1, const CString &s2)
   { 
      return (s2.Compare (s1) != 0); 
   }

   friend bool operator != (const CString &s1, const char *s2)
   { 
      return (s1.Compare (s2) != 0); 
   }

   CString &operator = (const CString &sInput)
   {
      Assign (sInput);
      return *this;
   }

   CString &operator = (const char *cszInput)
   {
      Assign (cszInput);
      return *this;
   }

   CString &operator = (char cInput)
   {
      Assign (cInput);
      return *this;
   }

   CString &operator += (const CString &sInput)
   {
      Append (sInput);
      return *this;
   }

   CString &operator += (const char *cszInput)
   {
      Append (cszInput);
      return *this;
   }

   char operator [] (int iIndex)
   {
      if (iIndex > m_iStringLength)
         return -1;

      return m_pszCharData[iIndex];
   }

   // string manipulations
   inline CString Mid (int iStartIndex, int iCount = -1)
   {
      CString sResult;

      if (iStartIndex >= m_iStringLength || !m_pszCharData)
         return sResult;
      
      if (iCount == -1)
         iCount = m_iStringLength - iStartIndex;
      else if (iStartIndex+iCount >= m_iStringLength) 
         iCount = m_iStringLength - iStartIndex;

      int i = 0, j = 0;
      char *szHolder = new char[m_iStringLength+1];

      for (i = iStartIndex; i < iStartIndex + iCount; i++)
         szHolder[j++] = m_pszCharData[i];
      
      szHolder[j] = 0;
      sResult.Assign(szHolder);

      delete [] szHolder;
      return sResult;
   }

   inline CString Mid (int iStartIndex)
   {
      return Mid (iStartIndex, m_iStringLength - iStartIndex);
   }

   inline CString Left (int iCount)
   {
      return Mid (0, iCount);
   }

   inline CString Right (int iCount)
   {
      if (iCount > m_iStringLength)
         iCount = m_iStringLength;

      return Mid (m_iStringLength - iCount, iCount);
   }

   inline CString ToUpper (void)
   {
      CString sResult;

      for (int i = 0; i < Length (); i++)
         sResult += toupper (m_pszCharData[i]);

      return sResult;
   }

   inline CString ToLower (void)
   {
      CString sResult;

      for (int i = 0; i < Length (); i++)
         sResult += tolower (m_pszCharData[i]);

      return sResult;
   }

   inline CString ToReverse (void)
   {
      char *szSrc, *szNext, cChar;

      szSrc = m_pszCharData + Length () - 1;
      szNext = m_pszCharData;

      while (szSrc > szNext)
      {
         if (*szSrc == *szNext)
         {
            szSrc--;
            szNext++;
         }
         else
         {
            cChar = *szSrc;
            *szSrc-- = *szNext;
            *szNext++ = cChar;
         }
      }
      return m_pszCharData;
   }

   inline void MakeUpper (void)
   { 
      *this = ToUpper (); 
   }

   inline void MakeLower (void) 
   { 
      *this = ToLower (); 
   }

   inline void MakeReverse (void) 
   { 
      *this = ToReverse (); 
   }

   inline int Compare (const CString &sString) const
   { 
      return strcmp (m_pszCharData, sString.m_pszCharData); 
   }

   inline int CompareI (CString &sString) const
   { 
      return strcmpi (m_pszCharData, sString.m_pszCharData); 
   }

   inline int Compare (const char *szString) const
   { 
      return strcmp (m_pszCharData, szString); 
   }

   inline int CompareI (const char *szString) const
   { 
      return stricmp (m_pszCharData, szString); 
   }

   inline int Collate (const CString &sString) const
   { 
      return strcoll (m_pszCharData, sString.m_pszCharData); 
   }

   inline int Find (char cInput) const
   { 
      return Find (cInput, 0); 
   }

   inline int Find (char cInput, int iStartIndex) const
   {
      char *szString = m_pszCharData + iStartIndex;

      for (;;)
      {
         if (*szString == cInput)
            return szString - m_pszCharData;

         if (*szString == 0)
            return -1;

         szString += 1;
      }
   }

   inline int Find (const CString &sString) const
   { 
      return Find (sString, 0); 
   }

   inline int Find (const CString &sString, int iStartIndex) const
   {
      if (sString.m_iStringLength == 0)
         return iStartIndex;

      for (; iStartIndex < m_iStringLength; iStartIndex++)
      {
         int j;
         for (j = 0; j < sString.m_iStringLength && iStartIndex + j < m_iStringLength; j++)
         {
            if (m_pszCharData[iStartIndex + j] != sString.m_pszCharData[j])
               break;
         }

         if (j == sString.m_iStringLength)
            return iStartIndex;
      }
      return -1;
   }

   inline int ReverseFind (char cChar)
   {
      if (m_iStringLength == 0)
         return -1;

      char *szString = m_pszCharData + m_iStringLength - 1;

      for (;;)
      {
         if (*szString == cChar)
            return szString - m_pszCharData;

         if (szString == m_pszCharData)
            return -1;
         szString--;
      }
   }

   inline int FindOneOf (CString &sString)
   {
      for (int i = 0; i < m_iStringLength; i++)
      {
         if (sString.Find (m_pszCharData[i]) >= 0)
            return i;
      }
      return -1;
   }

   inline CString &TrimRight (void)
   {
      char *szStr = m_pszCharData;
      char *szLast = NULL;

      while (*szStr != 0)
      {
         if (IsTrimChar (*szStr))
         {
            if (szLast == NULL)
               szLast = szStr;
         }
         else
            szLast = NULL;
         szStr++;
      }

      if (szLast != NULL)
         Delete (szLast - m_pszCharData);

      return *this;
   }

   inline CString &TrimLeft (void)
   {
      char *szStr = m_pszCharData;

      while (IsTrimChar (*szStr))
         szStr++;

      if (szStr != m_pszCharData)
      {
         int iFirst = int (szStr - GetString ());
         char *szBuffer = GetBuffer (Length ());

         szStr = szBuffer + iFirst;
         int iLength = Length () - iFirst;

         memmove (szBuffer, szStr, (iLength + 1) * sizeof (char));
         ReleaseBuffer (iLength);
      }
      return *this;
   }

   inline CString &Trim (void)
   {
      return TrimRight ().TrimLeft ();
   }

   inline void TrimRight (char cChar)
   {
      const char *szString = m_pszCharData;
      const char *szLast = NULL;

      while (*szString != 0)
      {
         if (*szString == cChar)
         {
            if (szLast == NULL)
               szLast = szString;
         }
         else
            szLast = NULL;

         szString++;
      }
      if (szLast != NULL)
      {
         int i = szLast - m_pszCharData;
         Delete (i, m_iStringLength - i);
      }
   }

   inline void TrimLeft (char cChar)
   {
      char *szString = m_pszCharData;

      while (cChar == *szString)
         szString++;

      Delete (0, szString - m_pszCharData);
   }
 
   inline int Match (const char *cszPattern) const
   {
      const char *cszString = GetString ();
      const char *cszWildcard = 0;
      const char *cszPosition = 0;
      const char *cszMatch = 0;

      CCharset kCharset;

      for (;;)
      {
         if (*cszPattern == '*')
         {
            cszWildcard = cszPattern++;
            cszPosition = cszString;

            if (cszMatch == NULL)
               cszMatch = cszString;
         }
         else if (*cszString == '\0')
            return (*cszPattern == '\0') ? (cszMatch - GetString ()) : -1;
         else if (*cszPattern == '[')
         {
            kCharset.ParseInput (&cszPattern);

            if (!kCharset.IsValid ((unsigned char) *cszString))
               return -1;

            if (!cszMatch)
               cszMatch = cszString;

            cszString += 1;
         }
         else if ((*cszString == *cszPattern) || (*cszPattern == '?'))
         {
            if (!cszMatch)
               cszMatch = cszString;

            cszString += 1;
            cszPattern += 1;
         }
         else if (cszWildcard)
         {
            cszString = ++cszPosition;
            cszPattern = cszWildcard;
         }
         else
            break;
      }
      return -1;
   }

   inline int Insert (int iIndex, char cChar)
   {
      InsertSpace (iIndex, 1);
      m_pszCharData[iIndex] = cChar;
      m_iStringLength++;

      return m_iStringLength;
   }

   inline int Insert (int iIndex, const CString &sString)
   {
      CorrectIndex (iIndex);

      if (sString.m_iStringLength == 0)
         return m_iStringLength;

      int iNumInsertChars = sString.m_iStringLength;
      InsertSpace (iIndex, iNumInsertChars);

      for(int i = 0; i < iNumInsertChars; i++)
         m_pszCharData[iIndex + i] = sString[i];
      m_iStringLength += iNumInsertChars;

      return m_iStringLength;
   }

   inline int Replace (char cOldChar, char cNewChar)
   {
      if (cOldChar == cNewChar)
         return 0;
      
      static int iNumber = 0;
      int iPos = 0;

      while (iPos < Length ())
      {
         iPos = Find (cOldChar, iPos);
         if (iPos < 0)
            break;

         m_pszCharData[iPos] = cNewChar;
         iPos++;
         iNumber++;
      }
      return iNumber;
   }

   inline int Replace (const CString &sOldStr, const CString &sNewStr)
   {
      if (sOldStr.m_iStringLength == 0)
         return 0;

      if (sNewStr.m_iStringLength == 0)
         return 0;

      int iOldLength = sOldStr.m_iStringLength;
      int iNewLength = sNewStr.m_iStringLength;

      int iNumber = 0;
      int iPos = 0;

      while (iPos < m_iStringLength)
      {
         iPos = Find (sOldStr, iPos);

         if (iPos < 0)
            break;

         Delete (iPos, iOldLength);
         Insert (iPos, sNewStr);
         
         iPos += iNewLength;
         iNumber++;
      }
      return iNumber;
   }

   inline int Delete (int iIndex, int iCount = 1)
   {
      if (iIndex + iCount > m_iStringLength)
         iCount = m_iStringLength - iIndex;

      if (iCount > 0)
      {
         MoveItems (iIndex, iIndex + iCount);
         m_iStringLength -= iCount;
      }
      return m_iStringLength;
   }

   inline CString DropQuotes (void)
   {
      TrimRight ('\"');
      TrimRight ('\'');

      TrimLeft ('\"');
      TrimLeft ('\'');

      return *this;
   }

   inline unsigned long Hash (void)
   {
      unsigned long uHash = 0;
      const char *ptr = m_pszCharData;

      while (*ptr != NULL)
      {
         uHash = (uHash << 5) + uHash + (*ptr);
         ptr++;
      }
      return uHash;
   }

   inline CArray <CString> Split (const char *szSep)
   {
      CArray <CString> kList;
      int iTokLength, iIndex = 0;

      do
      {
         iIndex += strspn (&m_pszCharData[iIndex], szSep);
         iTokLength = strcspn (&m_pszCharData[iIndex], szSep);

         if (iTokLength > 0)
            kList.AddItem (Mid (iIndex, iTokLength));
         iIndex += iTokLength;

      } while (iTokLength > 0);

      return kList;
   }

   inline CArray <CString> Split (char cSep)
   {
      char szSep[2];
      szSep[0] = cSep;
      szSep[1] = '\0';

      return Split (szSep);
   }
};

// a smiple wrapper for the stdio FILE handle
class CFile
{
protected:
   FILE *m_pHandle;
   int m_iSize;

public:
   CFile (void)
   {
      m_pHandle = NULL;
      m_iSize = 0;
   }

   CFile (CString sFile, CString sMode = "rt")
   {
      Open (sFile, sMode);
   }

   ~CFile (void)
   {
      Close ();
   }

   inline bool Open (CString sFile, CString sMode)
   {
      if ((m_pHandle = fopen (sFile, sMode)) == NULL)
         return false;

      // get the filesize
      fseek (m_pHandle, 0L, SEEK_END);
      m_iSize = ftell (m_pHandle);
      fseek (m_pHandle, 0L, SEEK_SET);

      return true;
   }

   void inline Close (void)
   {
      if (m_pHandle != NULL)
      {
         fclose (m_pHandle);
         m_pHandle = NULL;
      }
      m_iSize = 0;
   }

   inline bool Eof (void)
   {
      assert (m_pHandle != NULL);
      return feof (m_pHandle) ? true : false;
   }

   inline bool Flush (void)
   {
      assert (m_pHandle != NULL);
      return fflush (m_pHandle) ? false : true;
   }

   inline int GetChar (void)
   {
      assert (m_pHandle != NULL);
      return fgetc (m_pHandle);
   }

#if defined (PLATFORM_WIN32)
   inline fpos_t GetPos (void)
   {
      assert (m_pHandle != NULL);
      fpos_t iPos = 0;

      if (fgetpos (m_pHandle, &iPos) != 0)
         return 0;

      return static_cast <int> (iPos);
   }
#endif

   char *GetString (char *szStr, int iCount)
   {
      assert (m_pHandle != NULL);
      return fgets (szStr, iCount, m_pHandle);
   }

   bool GetString (CString sRet, int iCount)
   {
      assert (m_pHandle != NULL);
      return !CString (fgets (sRet, iCount, m_pHandle)).IsEmpty ();
   }

   inline int Printf (const char *cszFormat, ...)
   {
      assert (m_pHandle != NULL);

      va_list ap;
      va_start (ap, cszFormat);
      int iWritten = vfprintf (m_pHandle, cszFormat, ap);
      va_end (ap);

      if (iWritten < 0)
         return 0;

      return iWritten;
   }

   inline char PutChar (char ch)
   {
      assert (m_pHandle != NULL);
      return static_cast <char> (fputc (ch, m_pHandle));
   }

   inline bool PutString (CString sStr)
   {
      assert (m_pHandle != NULL);

      if (fputs (sStr.GetString (), m_pHandle) < 0)
         return false;

      return true;
   }

   inline int Read (void *ptrBuffer, int iSize, int iCount = 1)
   {
      assert (m_pHandle != NULL);
      return fread (ptrBuffer, iSize, iCount, m_pHandle);
   }

   inline int Write (void *ptrBuffer, int iSize, int iCount = 1)
   {
      assert (m_pHandle != NULL);
      return fwrite (ptrBuffer, iSize, iCount, m_pHandle);
   }

   inline bool Seek (long iOffs, int iOrigin)
   {
      assert (m_pHandle != NULL);

      if (fseek (m_pHandle, iOffs, iOrigin) != 0)
         return false;

      return true;
   }

   void inline Rewind (void)
   {
      assert (m_pHandle != NULL);
      rewind (m_pHandle);
   }

   inline int GetSize (void)
   {
      return m_iSize;
   }

   inline bool IsValid (void)
   {
      return m_pHandle != NULL;
   }
};

#define ITERATE_ARRAY(arArrayName, iIterName) \
   for (int iIterName = 0; iIterName != arArrayName.Size (); iIterName++) \

#define ITERATE_ARRAY_REVERSE(arArrayName, iIterName) \
   for (int iIterName = arArrayName.Size () - 1; iIterName >= 0; iIterName--) \

#define STR_NULL ""

#endif
