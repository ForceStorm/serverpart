//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#include "yapb.h"

bool g_bIsMetamod = false;
bool g_bRadioInsteadVoice = false; // if no voice found, use radio
bool g_bRoundEnded = true;
bool g_bBotsCanPause = false;  // stores if bots should pause
bool g_bSendAudioFinished = true;
bool g_bBombPlanted = false; // stores if the bomb was planted
bool g_bBombSayString = false;
bool g_bCommencing = false;
bool g_bEditNoclip = false;
bool g_bIsFakeCommand = false; // faked client command
bool g_bWaypointOn = false;
bool g_bWaypointsChanged = true;
bool g_bAutoWaypoint = false;
bool g_bDangerDirection = false;
bool g_bLearnJumpWaypoint = false;
bool g_rgbLeaderChoosen[2] = {false, false};

float g_fTimeFrameInterval = 0.0; // frame interval
float g_fLastChatTime = 0.0; // stores last time chatted - prevents spamming
float g_fTimeRoundStart = 0.0;
float g_fTimeRoundEnd = 0.0; // stores the end of the round (in worldtime)
float g_fTimeRoundMid = 0.0; // stores the halftime of the round (in worldtime)
float g_fTimeNextBombUpdate = 0.0; // holds the time to allow the next search update
float g_fTimeBombPlanted = 0.0; // holds the time when bomb was planted
float g_rgfLastRadioTime[2] = {0.0, 0.0}; // stores time of radiomessage - prevents too fast responds
float g_fAutoPathDistance = 250.0;

int g_rgiLastRadio[2];
int g_rgiRadioSelect[32];
int g_iFakeArgumentCount;
int g_iMsecMethod = METHOD_TOBIAS; // msec calculation method
int g_iCSVersion = VERSION_STEAM; // type of the counter-strike
int g_iLastBombPoint; // stores the last checked bomb waypoint
int g_rgiBombSpotsVisited[MAX_NUMBOMB_SPOTS]; // stores visited bombspots for cts when bomb has been planted 
int g_rgiStoreAddbotVars[4];
int g_iFindWPIndex = -1;
int g_iNumWaypoints; // number of waypoints currently in use for each team
int g_iMapType = 0; // type of map - assassination, defuse etc... 

short g_sModelIndexLaser;
short g_sModelIndexArrow;
char g_szFakeArgVector[256];
char g_cKillHistory;

vector <vector <string> > g_arChatEngine;
vector <vector <tChatter> > g_arVoiceEngine;
vector <string> g_arSprayNames;
vector <tBotName> g_arBotNames;
vector <tChatterPlace> g_arPlaceNames;
vector <tUserMsg> g_arUserMessages;
vector <tKeywordChat> g_arReplyChat;

dynlib_t g_hGameLib = NULL;
dynlib_t g_hSelfLib = NULL;

meta_globals_t *gpMetaGlobals = NULL; // metamod globals
gamedll_funcs_t *gpGamedllFuncs = NULL; // gameDLL function tables
mutil_funcs_t *gpMetaUtilFuncs = NULL; // metamod utility functions

DLL_FUNCTIONS g_kFunctionTable;
GETENTITYAPI g_pfnGetEntityAPI = NULL;
GETNEWENTAPI g_pfnGetNewEntityAPI = NULL;
GETBLENDINGAPI g_pfnServerBlendingAPI = NULL;
GIVEFNPTRSTODLL g_pfnGiveFnptrsToDll = NULL;

enginefuncs_t g_engfuncs;
tClient g_rgkClients[32];
tWeapon g_rgkWeaponDefs[MAX_WEAPONS]; // array of weapon definitions

CWaypoint *g_pWaypoint = NULL;
CBotManager *g_pBotManager = NULL;
CNetMsg *g_pNetMsg = NULL;
CLocalizer *g_pLocalize = NULL;

cvar_t *g_rgpcvBotCVar[CVAR_TOTAL];
edict_t *g_pentWorldEdict = NULL;
edict_t *g_pentHostEdict = NULL;
globalvars_t *g_pGlobals = NULL;
tExperience *g_pExperienceData = NULL;

// default tables for personality weapon prefs, overridden by weapons.cfg
int g_rgiNormalWeaponPrefs[NUM_WEAPONS] = 
   {0, 2, 1, 4, 5, 6, 3, 12, 10, 24, 25, 13, 11, 8, 7, 22, 23, 18, 21, 17, 19, 15, 17, 9, 14, 16};

int g_rgiAgressiveWeaponPrefs[NUM_WEAPONS] = 
   {0, 2, 1, 4, 5, 6, 3, 24, 19, 22, 23, 20, 21, 10, 12, 13, 7, 8, 11, 9, 18, 17, 19, 15, 16, 14};

int g_rgiDefensiveWeaponPrefs[NUM_WEAPONS] = 
   {0, 2, 1, 4, 5, 6, 3, 7, 8, 12, 10, 13, 11, 9, 24, 18, 14, 17, 16, 15, 19, 20, 21, 22, 23, 25};

int g_rgiGrenadeBuyPrecent[NUM_WEAPONS - 23] =
   {95, 85, 40};

int g_rgiBotBuyEconomyTable[NUM_WEAPONS - 15] = 
   {1900, 2100, 2100, 4000, 6000, 7000, 16000, 1200, 800, 1000, 3000};

tSkillData g_rgkSkillTab[6] =
{
   {0.8, 1.0, 45.0, 65.0, 2.0, 3.0, 40.0, 40.0, 50.0,   0,   0,   0}, 
   {0.6, 0.8, 40.0, 60.0, 3.0, 4.0, 30.0, 30.0, 42.0,  10,   0,   0},
   {0.4, 0.6, 35.0, 55.0, 4.0, 6.0, 20.0, 20.0, 32.0,  30,   0,  50}, 
   {0.2, 0.3, 30.0, 50.0, 6.0, 8.0, 10.0, 10.0, 18.0,   0,  30,  80},
   {0.1, 0.2, 25.0, 40.0, 8.0, 10.0, 5.0,  5.0, 10.0,  80,  50, 100}, 
   {0.0, 0.1, 20.0, 30.0, 9.0, 12.0, 0.0,  5.0,  0.0, 100, 100, 100}
};

int *g_ptrWeaponPrefs[] = 
{
   reinterpret_cast <int *> (&g_rgiNormalWeaponPrefs),
   reinterpret_cast <int *> (&g_rgiAgressiveWeaponPrefs),
   reinterpret_cast <int *> (&g_rgiDefensiveWeaponPrefs)
};
 
// metamod engine & dllapi function tables
metamod_funcs_t gMetaFunctionTable = 
{
   NULL, // pfnGetEntityAPI ()
   NULL, // pfnGetEntityAPI_Post ()
   GetEntityAPI2, // pfnGetEntityAPI2 ()
   GetEntityAPI2_Post, // pfnGetEntityAPI2_Post ()
   NULL, // pfnGetNewDLLFunctions ()
   NULL, // pfnGetNewDLLFunctions_Post ()
   GetEngineFunctions, // pfnGetEngineFunctions ()
   NULL, // pfnGetEngineFunctions_Post ()
};

// metamod plugin information
plugin_info_t Plugin_info = 
{
   META_INTERFACE_VERSION, // interface version
   PRODUCT_NAME, // plugin name
   PRODUCT_VERSION, // plugin version
   PRODUCT_DATE, // date of creation
   PRODUCT_AUTHOR, // plugin author
   PRODUCT_URL, // plugin URL
   PRODUCT_LOGTAG, // plugin logtag
   PT_STARTUP, // when loadable
   PT_NEVER, // when unloadable
};

tModInfo g_rgkModList[] = 
{
//    name       linux_so      win_dll    description                     mod_id 
   {"cstrike", "cs_i386.so",  "mp.dll", "Counter-Strike v1.6+",           VERSION_STEAM },
   {"czero",   "cs_i386.so",  "mp.dll", "Counter-Strike: Condition Zero", VERSION_CSCZ},
   {"csv15",   "cs_i386.so",  "mp.dll", "CS 1.5 for Steam",               VERSION_WON}, 
   {"cs13",    "cs_i386.so",  "mp.dll", "Counter-Strike v1.3",            VERSION_WON}, // assume cs13 = cs15

   {NULL, NULL, NULL, NULL} // terminator
};

// table with all available actions for the bots (filtered in & out in CBot::SetConditions) some of them have subactions included
tTask g_rgkTaskFilters[] = 
{
   {NULL,  NULL,  TASK_NORMAL,            0,  -1,  0.0,  true},
   {NULL,  NULL,  TASK_PAUSE,             0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_MOVETOPOSITION,    0,  -1,  0.0,  true},
   {NULL,  NULL,  TASK_FOLLOWUSER,        0,  -1,  0.0,  true},
   {NULL,  NULL,  TASK_WAITFORGO,         0,  -1,  0.0,  true},
   {NULL,  NULL,  TASK_PICKUPITEM,        0,  -1,  0.0,  true},
   {NULL,  NULL,  TASK_CAMP,              0,  -1,  0.0,  true},
   {NULL,  NULL,  TASK_PLANTBOMB,         0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_DEFUSEBOMB,        0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_ATTACK,            0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_ENEMYHUNT,         0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_SEEKCOVER,         0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_THROWHEGRENADE,    0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_THROWFLASHBANG,    0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_THROWSMOKEGRENADE, 0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_DOUBLEJUMP,        0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_ESCAPEFROMBOMB,    0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_SHOOTBREAKABLE,    0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_HIDE,              0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_BLINDED,           0,  -1,  0.0,  false},
   {NULL,  NULL,  TASK_SPRAYLOGO,         0,  -1,  0.0,  false}
};
  
// weapons and their specifications
tWeaponSelect g_rgkWeaponSelect[NUM_WEAPONS + 1] = 
{
   {WEAPON_KNIFE,      "weapon_knife",     "knife.mdl",     true,    0,    0, -1, -1,  0,  0,  0,  0,  false},
   {WEAPON_USP,        "weapon_usp",       "usp.mdl",       false,   500,  1, -1, -1,  1,  1,  2,  2,  false},
   {WEAPON_GLOCK18,    "weapon_glock18",   "glock18.mdl",   false,   400,  1, -1, -1,  1,  2,  1,  1,  false},
   {WEAPON_DEAGLE,     "weapon_deagle",    "deagle.mdl",    false,   650,  1,  2,  2,  1,  3,  4,  4,  true},
   {WEAPON_P228,       "weapon_p228",      "p228.mdl",      false,   600,  1,  2,  2,  1,  4,  3,  3,  false},
   {WEAPON_ELITE,      "weapon_elite",     "elite.mdl",     false,   1000, 1,  0,  0,  1,  5,  5,  5,  false},
   {WEAPON_FIVESEVEN,  "weapon_fiveseven", "fiveseven.mdl", false,   750,  1,  1,  1,  1,  6,  5,  5,  false},
   {WEAPON_M3,         "weapon_m3",        "m3.mdl",        false,   1700, 1,  2, -1,  2,  1,  1,  1,  false},
   {WEAPON_XM1014,     "weapon_xm1014",    "xm1014.mdl",    false,   3000, 1,  2, -1,  2,  2,  2,  2,  false},
   {WEAPON_MP5NAVY,    "weapon_mp5navy",   "mp5.mdl",       true,    1500, 1,  2,  1,  3,  1,  2,  2,  false},
   {WEAPON_TMP,        "weapon_tmp",       "tmp.mdl",       true,    1250, 1,  1,  1,  3,  2,  1,  1,  false},
   {WEAPON_P90,        "weapon_p90",       "p90.mdl",       true,    2350, 1,  2,  1,  3,  3,  4,  4,  false},
   {WEAPON_MAC10,      "weapon_mac10",     "mac10.mdl",     true,    1400, 1,  0,  0,  3,  4,  1,  1,  false},
   {WEAPON_UMP45,      "weapon_ump45",     "ump45.mdl",     true,    1700, 1,  2,  2,  3,  5,  3,  3,  false},
   {WEAPON_AK47,       "weapon_ak47",      "ak47.mdl",      true,    2500, 1,  0,  0,  4,  1,  2,  2,  true},
   {WEAPON_SG552,      "weapon_sg552",     "sg552.mdl",     true,    3500, 1,  0, -1,  4,  2,  4,  4,  true},
   {WEAPON_M4A1,       "weapon_m4a1",      "m4a1.mdl",      true,    3100, 1,  1,  1,  4,  3,  3,  3,  true},
   {WEAPON_GALIL,      "weapon_galil",     "galil.mdl",     true,    2000, 1,  0,  0,  4,  -1, 1,  1,  true},
   {WEAPON_FAMAS,      "weapon_famas",     "famas.mdl",     true,    2250, 1,  1,  1,  4,  -1, 1,  1,  true},
   {WEAPON_AUG,        "weapon_aug",       "aug.mdl",       true,    3500, 1,  1,  1,  4,  4,  4,  4,  true},
   {WEAPON_SCOUT,      "weapon_scout",     "scout.mdl",     false,   2750, 1,  2,  0,  4,  5,  3,  2,  true},
   {WEAPON_AWP,        "weapon_awp",       "awp.mdl",       false,   4750, 1,  2,  0,  4,  6,  5,  6,  true},
   {WEAPON_G3SG1,      "weapon_g3sg1",     "g3sg1.mdl",     false,   5000, 1,  0,  2,  4,  7,  6,  6,  true},
   {WEAPON_SG550,      "weapon_sg550",     "sg550.mdl",     false,   4200, 1,  1,  1,  4,  8,  5,  5,  true},
   {WEAPON_M249,       "weapon_m249",      "m249.mdl",      true,    5750, 1,  2,  1,  5,  1,  1,  1,  true},
   {WEAPON_SHIELDGUN,  "weapon_shield",    "shield.mdl",    false,   2200, 0,  1,  1,  8,  -1, 8,  8,  false},
   {NULL,              "",                 "",              false,   0,    0,  0,  0,  0,   0, 0,  0,  false}
};

// bot menus
tMenuText g_rgkMenu[21] = 
{
   // main menu
   {
      0x2ff,
      "\\yYaPB Main Menu\\w\v\v"
      "1. YaPB Control\v"
      "2. Features\v\v"
      "3. Fill Server\v"
      "4. End Round\v\v"
      "0. Exit"
   },

   // bot features menu
   {
      0x25f,
      "\\yYaPB Features\\w\v\v"
      "1. Weapon Mode Menu\v"
      "2. Waypoint Menu\v"
      "3. Select Personality\v\v"
      "4. Toggle Debug Mode\v"
      "5. Command Menu\v\v"
      "0. Exit"
   },
   
   // bot control menu
   {
      0x2ff,
      "\\yYaPB Control Menu\\w\v\v"
      "1. Add a Bot, Quick\v"
      "2. Add a Bot, Specified\v\v"
      "3. Remove Random Bot\v"
      "4. Remove All Bots\v\v"
      "5. Remove Bot Menu\v\v"
      "0. Exit"
   },

   // weapon mode select menu
   {
      0x27f,
      "\\yYaPB Weapon Mode\\w\v\v"
      "1. Knives only\v"
      "2. Pistols only\v"
      "3. Shotguns only\v"
      "4. Machine Guns only\v"
      "5. Rifles only\v"
      "6. Sniper Weapons only\v"
      "7. All Weapons\v\v"
      "0. Exit"
   },

   // personality select menu
   {
      0x20f,
      "\\yYaPB Personality\\w\v\v"
      "1. Random\v"
      "2. Normal\v"
      "3. Aggressive\v"
      "4. Defensive\v\v"
      "0. Exit"
   },

   // skill select menu
   {
      0x23f,
      "\\yYaPB Skill Level\\w\v\v"
      "1. Stupid (0-20)\v"
      "2. Newbie (20-40)\v"
      "3. Average (40-60)\v"
      "4. Advanced (60-80)\v"
      "5. Professional (80-99)\v"
      "6. Godlike (100)\v\v"
      "0. Exit"
   },

   // team select menu
   {
      0x213,
      "\\ySelect a team\\w\v\v"
      "1. Terrorist Force\v"
      "2. Counter-Terrorist Force\v\v"
      "5. Auto-select\v\v"
      "0. Exit"
   },

   // terrorist model select menu
   {
      0x21f,
      "\\ySelect an appearance\\w\v\v"
      "1. Phoenix Connexion\v"
      "2. L337 Krew\v"
      "3. Arctic Avengers\v"
      "4. Guerilla Warfare\v\v"
      "5. Auto-select\v\v"
      "0. Exit"
   },

   // counter-terrirust model select menu
   {
      0x21f,
      "\\ySelect an appearance\\w\v\v"
      "1. Seal Team 6 (DEVGRU)\v"
      "2. German GSG-9\v"
      "3. UK SAS\v"
      "4. French GIGN\v\v"
      "5. Auto-select\v\v"
      "0. Exit"
   },

   // main waypoint menu
   {
      0x3ff,
      "\\yWaypoint Operations (Page 1)\\w\v\v"
      "1. Show/Hide waypoints\v"
      "2. Cache waypoint\v"
      "3. Create path\v"
      "4. Delete path\v"
      "5. Add waypoint\v"
      "6. Delete waypoint\v"
      "7. Set Autopath Distance\v"
      "8. Set Radius\v\v"
      "9. Next...\v\v"
      "0. Exit"
   },

   // main waypoint menu (page 2)
   {
      0x3ff,
      "\\yWaypoint Operations (Page 2)\\w\v\v"
      "1. Waypoint stats\v"
      "2. Autowaypoint on/off\v"
      "3. Set flags\v"
      "4. Save waypoints\v"
      "5. Save without checking\v"
      "6. Load waypoints\v"
      "7. Check waypoints\v"
      "8. Noclip cheat on/off\v\v"
      "9. Previous...\v\v"
      "0. Exit"
   },

   // select waypoint radius menu
   {
      0x3ff,
      "\\yWaypoint Radius\\w\v\v"
      "1. SetRadius 0\v"
      "2. SetRadius 8\v"
      "3. SetRadius 16\v"
      "4. SetRadius 32\v"
      "5. SetRadius 48\v"
      "6. SetRadius 64\v"
      "7. SetRadius 80\v"
      "8. SetRadius 96\v"
      "9. SetRadius 128\v\v"
      "0. Exit"
   },

   // waypoint add menu
   {
      0x3ff,
      "\\yWaypoint Type\\w\v\v"
      "1. Normal\v"
      "\\r2. Terrorist Important\v"
      "3. Counter-Terrorist Important\v"
      "\\w4. Block with hostage / Ladder\v"
      "\\y5. Rescue Zone\v"
      "\\w6. Camping\v"
      "7. Camp End\v"
      "\\r8. Map Goal\v"
      "\\w9. Jump\v\v"
      "0. Exit"
   },

   // set waypoint flag menu
   {
      0x2ff,
      "\\yWaypoint Flags\\w\v\v"
      "\\yAdd waypoint flag:\\w\v\v"
      "1. Block with Hostage\v"
      "2. Terrorists Specific\v"
      "3. CTs Specific\v"
      "4. Use Elevator\v\v"
      "\\yDelete waypoint flag:\\w\v\v"
      "5. Block with Hostage\v"
      "6. Terrorists Specific\v"
      "7. CTs Specific\v"
      "8. Use Elevator\v\v"
      "0. Exit"
   },

   // kickmenu #1
   {
      0x0,
      NULL,
   },

   // kickmenu #2
   {
      0x0,
      NULL,
   },

   // kickmenu #3
   {
      0x0,
      NULL,
   },

   // kickmenu #4
   {
      0x0,
      NULL,
   },

   // command menu
   {
      0x23f,
      "\\yBot Command Menu\\w\v\v"
      "1. Make Double Jump\v"
      "2. Finish Double Jump\v\v"
      "3. Drop the C4 Bomb\v"
      "4. Drop the Weapon\v\v"
      "0. Exit"
   },

   // auto-path max distance
   {
      0x27f,
      "\\yAutoPath Distance\\w\v\v"
      "1. Distance 0\v"
      "2. Distance 100\v"
      "3. Distance 130\v"
      "4. Distance 160\v"
      "5. Distance 190\v"
      "6. Distance 220\v"
      "7. Distance 250 (Default)\v\v"
      "0. Exit"
   },

   // path connections
   {
      0x207,
      "\\yCreate Path (Choose Direction)\\w\v\v"
      "1. Outgoing Path\v"
      "2. Incoming Path\v"
      "3. Bidirectional (Bot Ways)\v\v"
      "0. Exit"
   }
};
