//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#ifndef GLOBALS_INCLUDED
#define GLOBALS_INCLUDED

extern bool g_bBombPlanted;
extern bool g_bBombSayString; 
extern bool g_bRoundEnded;
extern bool g_bRadioInsteadVoice;
extern bool g_bWaypointOn;
extern bool g_bWaypointsChanged;
extern bool g_bAutoWaypoint;
extern bool g_bDangerDirection;
extern bool g_bBotsCanPause; 
extern bool g_bEditNoclip;
extern bool g_bIsMetamod;
extern bool g_bIsFakeCommand;
extern bool g_bSendAudioFinished;
extern bool g_bCommencing;
extern bool g_rgbLeaderChoosen[2];

extern float g_fAutoPathDistance;
extern float g_fTimeBombPlanted;
extern float g_fTimeNextBombUpdate;
extern float g_fLastChatTime;
extern float g_fTimeRoundEnd;
extern float g_fTimeRoundMid;
extern float g_fTimeNextBombUpdate;
extern float g_fTimeFrameInterval;
extern float g_fTimeRoundStart;
extern float g_rgfLastRadioTime[2];

extern int g_iLastBombPoint;
extern int g_iMapType;
extern int g_iFindWPIndex;
extern int g_iNumWaypoints;
extern int g_iMsecMethod;
extern int g_iCSVersion;
extern int g_iFakeArgumentCount;

extern int g_rgiNormalWeaponPrefs[NUM_WEAPONS];
extern int g_rgiAgressiveWeaponPrefs[NUM_WEAPONS];
extern int g_rgiDefensiveWeaponPrefs[NUM_WEAPONS];
extern int g_rgiGrenadeBuyPrecent[NUM_WEAPONS - 23];
extern int g_rgiBotBuyEconomyTable[NUM_WEAPONS - 15];
extern int g_rgiRadioSelect[32];
extern int g_rgiLastRadio[2];
extern int g_rgiBombSpotsVisited[MAX_NUMBOMB_SPOTS];
extern int g_rgiStoreAddbotVars[4];
extern int *g_ptrWeaponPrefs[];

extern short g_sModelIndexLaser;
extern short g_sModelIndexArrow;
extern char g_cKillHistory;
extern char g_szFakeArgVector[256];
extern cvar_t *g_rgpcvBotCVar[CVAR_TOTAL];

extern vector <vector <string> > g_arChatEngine;
extern vector <vector <tChatter> > g_arVoiceEngine;
extern vector <tBotName> g_arBotNames;
extern vector <tChatterPlace> g_arPlaceNames;
extern vector <tKeywordChat> g_arReplyChat;
extern vector <string> g_arSprayNames;
extern vector <tUserMsg> g_arUserMessages;

extern tWeaponSelect g_rgkWeaponSelect[NUM_WEAPONS + 1];
extern tWeapon g_rgkWeaponDefs[MAX_WEAPONS];
extern tClient g_rgkClients[32];
extern tMenuText g_rgkMenu[21];
extern tSkillData g_rgkSkillTab[6];
extern tModInfo g_rgkModList[];
extern tTask g_rgkTaskFilters[];
extern tExperience *g_pExperienceData;
extern edict_t *g_pentHostEdict; 
extern edict_t *g_pentWorldEdict;
extern dynlib_t g_hGameLib;
extern dynlib_t g_hSelfLib;

extern CBotManager *g_pBotManager;
extern CNetMsg *g_pNetMsg;
extern CLocalizer *g_pLocalize;
extern CWaypoint *g_pWaypoint;

extern DLL_FUNCTIONS g_kFunctionTable;
extern GETENTITYAPI g_pfnGetEntityAPI;
extern GIVEFNPTRSTODLL g_pfnGiveFnptrsToDll;
extern GETNEWENTAPI g_pfnGetNewEntityAPI;
extern GETBLENDINGAPI g_pfnServerBlendingAPI;

#endif
