//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#ifndef MATHLIB_INCLUDED
#define MATHLIB_INCLUDED

static const Vector NULLVEC (0.0, 0.0, 0.0);
static const Vector2D NULLVEC2D (0.0, 0.0);

// use only our PI value (inside bot code)
static const double PI = 3.141592653589793238462643383279502884197169399375105820974944;

#pragma warning (disable: 4239)

inline float LengthSquared (const Vector &vVector)
{
   // squared length, no sqrtf involved so faster
   return ((vVector.x * vVector.x) + (vVector.y * vVector.y) + (vVector.z  * vVector.z));
}

inline float AngleMod (float fAngle)
{
   // this function adds or substracts 360 enough times needed to the given angle in
   // order to set it into the range [0, 360) and returns the resulting angle. letting
   // the engine have a hand on angles that are outside these bounds may cause the
   // game to freeze by screwing up the engine math code.

   return (360.0 / 65536) * (static_cast <int> (fAngle * (65536.0 / 360.0)) & 65535);
}

inline float AngleNormalize (float fAngle)
{
   // this function adds or substracts 360 enough times needed to the given angle in
   // order to set it into the range [-180, 180) and returns the resulting angle. letting
   // the engine have a hand on angles that are outside these bounds may cause the game
   // to freeze by screwing up the engine math code.

   return (360.0 / 65536) * (static_cast <int> ((fAngle + 180) * (65536.0 / 360.0)) & 65535) - 180;
}

inline Vector ClampAngles (Vector vAngles)
{
   vAngles.x = AngleNormalize (vAngles.x);
   vAngles.y = AngleNormalize (vAngles.y);
   vAngles.z = 0;

   return vAngles;
}

inline float AngleDiff (float fDest, float fSource)
{
   return AngleNormalize (fDest - fSource);
}

inline float VecToYaw (const Vector &vVector)
{
   // the purpose of this function is to convert a spatial location determined by the Vector
   // passed in into an absolute Y angle (yaw) from the origin of the world.

   if ((vVector.x == 0) && (vVector.y == 0))
      return 0;
   else
      return (atan2f (vVector.y, vVector.x) * (180 / PI));
   
}

inline Vector VecToAngles (const Vector &vVector)
{
   // the purpose of this function is to convert a spatial location determined by the Vector
   // passed in into absolute angles from the origin of the world.

   float fYaw, fPitch;

   if ((vVector.x == 0) && (vVector.y == 0))
   {
      fYaw = 0;
      fPitch = (vVector.z > 0) ? 90 : 270;
   }
   else
   {
      fYaw = atan2f (vVector.y, vVector.x) * (180 / PI);
      fPitch = atan2f (vVector.z, vVector.Length2D ()) * (180 / PI);
   }

   return Vector (fPitch, fYaw, 0);
}

void inline Sincosf (float fRad, float *pfSin, float *pfCos)
{
#if defined (PLATFORM_WIN32) && defined (COMPILER_VISUALC)
   __asm
   {
      fld dword ptr[fRad]
      fsincos
      mov edx, dword ptr[pfCos]
      mov eax, dword ptr[pfSin]
      fstp dword ptr[edx]
      fstp dword ptr[eax]
   }
#elif defined (PLATFORM_LINUX32) || defined (PLATFORM_LINUX64)
   register double fCos, fSin;
   __asm __volatile__ ("fsincos" : "=t" (fCos), "=u" (fSin) : "0" (fRad));

   *pfCos = fCos;
   *pfSin = fSin;
#else
   *pfSin = sinf (fRad);
   *pfCos = cosf (fRad);
#endif
}

void inline MakeVectors (const Vector &vAngles)
{
   float fSp = 0, fCp = 0, fSy = 0, fCy = 0, fSr = 0, fCr = 0;
   float fAngle = vAngles.x * (PI / 180);
   
   Sincosf (fAngle, &fSp, &fCp);
   fAngle = vAngles.y * (PI / 180);
   Sincosf (fAngle, &fSy, &fCy);
   fAngle = vAngles.z * (PI / 180);
   Sincosf (fAngle, &fSr, &fCr);

   g_pGlobals->v_forward.x = fCp * fCy;
   g_pGlobals->v_forward.y = fCp * fSy;
   g_pGlobals->v_forward.z = -fSp;

   g_pGlobals->v_right.x = -fSr * fSp * fCy + fCr * fSy;
   g_pGlobals->v_right.y = -fSr * fSp * fSy - fCr * fCy;
   g_pGlobals->v_right.z = -fSr * fCp;

   g_pGlobals->v_up.x = fCr * fSp * fCy + fSr * fSy;
   g_pGlobals->v_up.y = fCr * fSp * fSy - fSr * fCy;
   g_pGlobals->v_up.z = fCr * fCp;
}

inline int32 GetRandom (void)
{
   // this function is the equivalent of the rand() standard C library function, except that
   // whereas rand() works only with short integers (i.e. not above 32767), this function is
   // able to generate 32-bit random numbers.

   const int32 iMaxRand = 2147483647L;
   static int32 iFinalSeed = time (NULL);
   
   uint32 uLowRand = 16807 * static_cast <long> (iFinalSeed & 0xffff); // low part
   uint32 uHighRand = 16807 * static_cast <long> (static_cast <uint32> (iFinalSeed >> 16));

   uLowRand += (uHighRand & 0x7fff) << 16; // high part

   // assemble both in low part, is the resulting number greate than iMaxRand (half the capacity)?
   if (uLowRand > iMaxRand)
   {
      uLowRand &= iMaxRand; // the get rid of the disturbing bit
      uLowRand++; // and increase it a bit (to avoid overflow problems).
   }
   uLowRand += uHighRand >> 15;

   // now do twisted maths to generate the next seed. is the resulting number greater than iRandMax (half the capacity)?
   if (uLowRand > iMaxRand)
   {
      uLowRand &= iMaxRand; // then get rid of the disturbing bit
      uLowRand++; // and increase it a bit (to avoid overflow problems).
   }

   // now we finally got our (pseudo-)rando number.
   return (iFinalSeed = static_cast <long> (uLowRand)); // put it in the seed for next time & return it.
}

inline bool IsBoundingBoxTouching (const Vector &vFirstOrigin, const Vector &vFirstMins, const Vector &vFirstMaxs, const Vector &vSecondOrigin, const Vector &vSecondMins, const Vector &vSecondMaxs)
{
   // for future use

   return (((vFirstOrigin.x + vFirstMaxs.x) >= (vSecondOrigin.x + vSecondMins.x)) &&
           ((vFirstOrigin.x + vFirstMins.x) <= (vSecondOrigin.x + vSecondMaxs.x)) &&
           ((vFirstOrigin.y + vFirstMaxs.y) >= (vSecondOrigin.y + vSecondMins.y)) &&
           ((vFirstOrigin.y + vFirstMins.y) <= (vSecondOrigin.y + vSecondMaxs.y)) &&
           ((vFirstOrigin.z + vFirstMaxs.z) >= (vSecondOrigin.z + vSecondMins.z)) &&
           ((vFirstOrigin.z + vFirstMins.z) <= (vSecondOrigin.z + vSecondMaxs.z)));
}

#endif
