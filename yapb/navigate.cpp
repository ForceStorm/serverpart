//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#include "yapb.h"

int CBot::FindGoal (void)
{
   // chooses a destination (goal) waypoint for a bot
   int iTeam = GetTeam (GetEntity ());

   // forcing terrorist bot to not move to another bombspot
   if ((g_iMapType & MAP_DE) && (iTeam == TEAM_TERRORIST) && !g_bBombPlanted && m_bInBombZone && !m_bHasProgressBar && (pev->weapons & (1 << WEAPON_C4)))
      return g_pWaypoint->FindNearest (pev->origin, 400.0, W_FL_GOAL);

   // pathfinding behaviour depending on maptype
   int iTactic;
   int iOffensive;
   int iDefensive;
   int iGoalDesire;

   int iForwardDesire;
   int iCampDesire;
   int iBackoffDesire;
   int iTacticChoice;

   vector <int> arOffensiveWpts;
   vector <int> arDefensiveWpts;

   switch (iTeam)
   {
   case TEAM_TERRORIST:
      arOffensiveWpts = g_pWaypoint->m_arCTPoints;
      arDefensiveWpts = g_pWaypoint->m_arTerrorPoints;
      break;

   case TEAM_CT:
      arOffensiveWpts = g_pWaypoint->m_arTerrorPoints;
      arDefensiveWpts = g_pWaypoint->m_arCTPoints;
      break;
   }

   // terrorist carrying the C4?
   if (pev->weapons & (1 << WEAPON_C4) || m_bIsVIP)
   {
      iTactic = 3;
      goto TacticChoosen;
   }
   else if (HasHostage () && (iTeam == TEAM_CT))
   {
      iTactic = 2;
      arOffensiveWpts = g_pWaypoint->m_arRescuePoints;

      goto TacticChoosen;
   }

   iOffensive = m_fAgressionLevel * 100;
   iDefensive = m_fFearLevel * 100;

   if (g_iMapType & (MAP_AS | MAP_CS))
   {
      if (iTeam == TEAM_TERRORIST)
      {
         iDefensive += 25;
         iOffensive -= 25;
      }
      else if (iTeam == TEAM_CT)
      {
         iDefensive -= 25;
         iOffensive += 25;
      }
   }
   else if ((g_iMapType & MAP_DE) && (iTeam == TEAM_CT))
   {
      if (g_bBombPlanted && (CurrentTask ()->iTask != TASK_ESCAPEFROMBOMB))
      {
         if (g_bBombSayString)
         {
            ChatMessage (CHAT_BOMBPLANT);
            g_bBombSayString = false;
         }
         return ChooseBombWaypoint ();
      }
      iDefensive += 25;
      iOffensive -= 25;
   }
   else if ((g_iMapType & MAP_DE) && (iTeam == TEAM_TERRORIST))
   {
      if (g_bBombPlanted && (CurrentTask ()->iTask != TASK_ESCAPEFROMBOMB) && (RandomLong (0, 100) < 80) && (GetBombTimeleft () >= 15.0))
         return FindDefendWaypoint (g_pWaypoint->GetBombPosition ());
      
      iDefensive -= 25;
      iOffensive += 25;
   }

   iGoalDesire = RandomLong (0, 100) + iOffensive;
   iForwardDesire = RandomLong (0, 100) + iOffensive;
   iCampDesire = RandomLong (0, 100) + iDefensive;
   iBackoffDesire = RandomLong (0, 100) + iDefensive;

   iTacticChoice = iBackoffDesire;
   iTactic = 0;

   if (iCampDesire > iTacticChoice)
   {
      iTacticChoice = iCampDesire;
      iTactic = 1;
   }
   if (iForwardDesire > iTacticChoice)
   {
      iTacticChoice = iForwardDesire;
      iTactic = 2;
   }
   if (iGoalDesire > iTacticChoice)
      iTactic = 3;

TacticChoosen:
   int iGoalChoices[4] = {-1, -1, -1, -1};

   if ((iTactic == 0) && !arDefensiveWpts.IsFree ()) // defensive goal
   {
      for (int i = 0; i < 4; i++)
      {
         iGoalChoices[i] = arDefensiveWpts.Random ();
         InternalAssert ((iGoalChoices[i] >= 0) && (iGoalChoices[i] < g_iNumWaypoints));
      }
   }
   else if ((iTactic == 1) && !g_pWaypoint->m_arCampPoints.IsFree ()) // camp waypoint goal
   {
      for (int i = 0; i < 4; i++)
      {
         iGoalChoices[i] = g_pWaypoint->m_arCampPoints.Random ();
         InternalAssert ((iGoalChoices[i] >= 0) && (iGoalChoices[i] < g_iNumWaypoints));
      }
   }
   else if ((iTactic == 2) && !arOffensiveWpts.IsFree ()) // offensive goal
   {
      for (int i = 0; i < 4; i++)
      {
         iGoalChoices[i] = arOffensiveWpts.Random ();
         InternalAssert ((iGoalChoices[i] >= 0) && (iGoalChoices[i] < g_iNumWaypoints));
      }
   }
   else if ((iTactic == 3) && !g_pWaypoint->m_arGoalPoints.IsFree ()) // map goal waypoint
   {
      for (int i = 0; i < 4; i++)
      {
         iGoalChoices[i] = g_pWaypoint->m_arGoalPoints.Random ();
         InternalAssert ((iGoalChoices[i] >= 0) && (iGoalChoices[i] < g_iNumWaypoints));
      }
   }

   if ((m_iCurrWptIndex == -1) || (m_iCurrWptIndex >= g_iNumWaypoints))
      ChangeWptIndex (g_pWaypoint->FindNearest (pev->origin));

   if (iGoalChoices[0] != -1)
   {
      bool bSorting = false;

      do
      {
         bSorting = false;

         for (int i = 0; i < 3; i++)
         {
            if (iGoalChoices[i + 1] < 0)
               break;

            if (iTeam == TEAM_TERRORIST)
            {
               if ((g_pExperienceData + (m_iCurrWptIndex * g_iNumWaypoints) + iGoalChoices[i])->wTeam0Value < (g_pExperienceData + (m_iCurrWptIndex * g_iNumWaypoints) + iGoalChoices[i + 1])->wTeam0Value)
               {
                  iGoalChoices[i + 1] = iGoalChoices[i];
                  iGoalChoices[i] = iGoalChoices[i + 1];

                  bSorting = true;
               }
            }
            else
            {
               if ((g_pExperienceData + (m_iCurrWptIndex * g_iNumWaypoints) + iGoalChoices[i])->wTeam1Value < (g_pExperienceData + (m_iCurrWptIndex * g_iNumWaypoints) + iGoalChoices[i + 1])->wTeam1Value)
               {
                  iGoalChoices[i + 1] = iGoalChoices[i];
                  iGoalChoices[i] = iGoalChoices[i + 1];

                  bSorting = true;
               }
            }
         }
      } while (bSorting);
      return iGoalChoices[0];
   }
   return RandomLong (0, g_iNumWaypoints - 1);
}

bool CBot::GoalIsValid (void)
{
   int iGoal = CurrentTask ()->iData;

   if (iGoal == -1) // not decided about a goal
      return false;
   else if (iGoal == m_iCurrWptIndex) // no nodes needed
      return true;
   else if (m_pWaypointNodes == NULL) // no path calculated
      return false;

   // got path - check if still valid
   tPathNode *pNode = m_pWaypointNodes;

   while (pNode->pNext != NULL)
      pNode = pNode->pNext;

   if (pNode->iIndex == iGoal)
      return true;

   return false;
}

bool CBot::DoWaypointNav (void)
{
   // this function is a main path navigation

   TraceResult tr;

   // check if we need to find a waypoint...
   if (m_iCurrWptIndex == -1)
   {
      GetValidWaypoint ();

      m_vWptOrigin = g_pWaypoint->GetPath (m_iCurrWptIndex)->vOrigin;
    
      // if wayzone radios non zero vary origin a bit depending on the body angles
      if (g_pWaypoint->GetPath (m_iCurrWptIndex)->fRadius != 0.0)
      {
         MakeVectors (pev->angles);
         m_vWptOrigin = ((m_vWptOrigin + g_pGlobals->v_right * RandomFloat (-g_pWaypoint->GetPath (m_iCurrWptIndex)->fRadius, g_pWaypoint->GetPath (m_iCurrWptIndex)->fRadius)) + (m_vWptOrigin + g_pGlobals->v_forward * RandomFloat (0, g_pWaypoint->GetPath (m_iCurrWptIndex)->fRadius))) * 0.5;
      }
      m_fWptTimeset = WorldTime ();
   }

   if (pev->flags & FL_DUCKING)
      m_vDestOrigin = m_vWptOrigin;
   else
      m_vDestOrigin = m_vWptOrigin + pev->view_ofs;

   float fWptDistance = (pev->origin - m_vWptOrigin).Length ();

   // This waypoint has additional Travel Flags - care about them
   if (m_uiCurrTravelFlags & C_FL_JUMP)
   {
      // bot is not jumped yet?
      if (!m_bJumpDone)
      {
         // if bot's on the ground or on the ladder we're free to jump.
         // actually setting the correct velocity is cheating. pressing
         // the jump button gives the illusion of the bot actual jmping.
         if (IsOnFloor () || IsOnLadder ())
         {
            pev->velocity = m_vDesiredVelocity;
            pev->button |= IN_JUMP;
            m_bJumpDone = true;
            m_bCheckTerrain = false;
            m_vDesiredVelocity = NULLVEC;
         }
      }
      else if (!g_rgpcvBotCVar[CVAR_KNIFEMODE]->GetBool () && (m_iCurrentWeapon == WEAPON_KNIFE) && IsOnFloor ())
        SelectBestWeapon ();
   }

   // special ladder handling
   if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_LADDER)
   {
      if ((m_vWptOrigin.z < pev->origin.z) && !IsOnLadder () && IsOnFloor () && !(pev->flags & FL_DUCKING))
      {
         // slowly approach ladder if going down
         m_fMoveSpeed = fWptDistance;

         if (m_fMoveSpeed < pev->maxspeed)
            m_fMoveSpeed = 150.0;
         else if (m_fMoveSpeed > pev->maxspeed)
            m_fMoveSpeed = pev->maxspeed;
      }
   }

    // special lift handling
   if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_LIFT)
   {
      TraceLine (g_pWaypoint->GetPath (m_iCurrWptIndex)->vOrigin, g_pWaypoint->GetPath (m_iCurrWptIndex)->vOrigin + Vector (0, 0, -50), true, false, GetEntity (), &tr);

      // lift found at waypoint ?
      if (tr.flFraction < 1.0)
      {
         // is lift activated AND bot is standing on it AND lift is moving ?
         if ((tr.pHit->v.nextthink > 0) && (pev->groundentity == tr.pHit) && (pev->velocity.z != 0) && IsOnFloor ())
         {
            // When lift is moving, pause the bot
            m_fWptTimeset = WorldTime ();
            m_fMoveSpeed = 0.0;

            m_fSideMoveSpeed = 0.0;
            m_iAimFlags |= AIM_OVERRIDE;

            pev->button &= ~(IN_FORWARD | IN_BACK | IN_MOVELEFT | IN_MOVERIGHT);
         }

         // lift found but won't move ?
         else if ((tr.pHit->v.nextthink <= 0) && (m_fItemCheckTime < WorldTime ()))
         {
            DeleteSearchNodes ();
            FindWaypoint ();

            return false;
         }
      }

      // lift not found at waypoint ?
      else
      {
         // button has been pressed, lift should come
         m_fWptTimeset = WorldTime ();
         m_fMoveSpeed = 0.0;
         m_fSideMoveSpeed = 0.0;
         m_iAimFlags |= AIM_DEST;

         if (m_fItemCheckTime + 4.0 < WorldTime ())
         {
            DeleteSearchNodes ();
            FindWaypoint ();

            return false;
         }
      }
   }
   
   // check if we are going through a door...
   TraceLine (pev->origin, m_vWptOrigin, true, GetEntity (), &tr);

   if (!FNullEnt (tr.pHit))
   {
      if (strncmp (STRING (tr.pHit->v.classname), "func_door", 9) == 0)
      {
         // If the door is near enough...
         if (LengthSquared (GetEntityOrigin (tr.pHit) - pev->origin) < 2500)
         {
            m_fNoCollTime = WorldTime () + 0.5; // don't consider being stuck
            
            if (RandomLong (1, 100) < 50)
               MDLL_Use (tr.pHit, GetEntity ()); // Also 'use' the door randomly
         }

         // make sure we are always facing the door when going through it
         m_iAimFlags &= ~(AIM_LASTENEMY | AIM_PREDICTPATH);

         // does this door has a target button?
         if (STRING (tr.pHit->v.targetname)[0] != 0)
         {
            edict_t *pent = NULL;
            edict_t *pentButton = NULL;

            float fMinDist = FLT_MAX;
            const char *cszTargetName = STRING (tr.pHit->v.targetname);

            // find the nearest button which can open this door...
            while (!FNullEnt (pent = FIND_ENTITY_BY_STRING (pent, "target", cszTargetName)))
            {
               Vector vOrigin = GetEntityOrigin (pent);
               TraceLine (pev->origin, vOrigin, true, GetEntity (), &tr);

               if ((tr.pHit == pent) || (tr.flFraction > 0.95))
               {
                  if (!IsDeadlyDrop (pent->v.origin))
                  {
                     float fDist = LengthSquared (pev->origin - vOrigin);

                     if (fDist <= fMinDist)
                     {
                        fMinDist = fDist;
                        pentButton = pent;
                     }
                  }
               }
            }

            if (!FNullEnt (pentButton))
            {
               m_pentPickupItem = pentButton;
               m_iPickupType = PICKUP_BUTTON;

               m_fWptTimeset = WorldTime ();
            }
         }
         // if bot hits the door, then it opens, so wait a bit to let it open safely
         if ((pev->velocity.Length2D() < 2) && (m_fTimeDoorOpen < WorldTime ()))
         {
            StartTask (TASK_PAUSE, TASKPRI_PAUSE, -1, WorldTime () + 1, false);
            m_fTimeDoorOpen = WorldTime () + 1.0; // retry in 1 sec until door is open
            m_iCountOpenDoor++;

            edict_t *pent = NULL;

            if ((m_iCountOpenDoor > 2) && !FNullEnt (pent = FIND_ENTITY_IN_SPHERE (pent, pev->origin, 100)))
            {
               if (IsValidPlayer (pent) && IsAlive (pent) && (GetTeam (GetEntity ()) != GetTeam (pent)) && IsWeaponShootingThroughWall (m_iCurrentWeapon))
               {
                  m_fSeeEnemyTime = WorldTime ();

                  m_iStates |= STATE_SUSPECTENEMY;
                  m_iAimFlags |= AIM_LASTENEMY;
                  m_pentLastEnemy = pent;
                  m_pentEnemy = pent;
                  m_vLastEnemyOrigin = pent->v.origin;

               }
               else if (IsValidPlayer (pent) && IsAlive (pent) && (GetTeam (GetEntity ()) == GetTeam (pent)))
               {
                  DeleteSearchNodes ();
                  ResetTasks ();
               }
               else if (IsValidPlayer (pent) && (!IsAlive (pent) || (pent->v.deadflag & DEAD_DYING)))
                  m_iCountOpenDoor = 0; // reset count
            }
         } 
      }
   }

   float fDesiredDistance;

   // initialize the radius for a special waypoint type, where the wpt is considered to be reached
   if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_LIFT)
      fDesiredDistance = 50;
   else if ((pev->flags & FL_DUCKING) || (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_GOAL))
      fDesiredDistance = 25;
   else if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_LADDER)
      fDesiredDistance = 15;
   else
      fDesiredDistance = g_pWaypoint->GetPath (m_iCurrWptIndex)->fRadius;

   // check if waypoint has a special travelflag, so they need to be reached more precisely
   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      if (g_pWaypoint->GetPath (m_iCurrWptIndex)->uConnectFlag[i] != 0)
      {
         fDesiredDistance = 0;
         break;
      }
   }

   // needs precise placement - check if we get past the point
   if ((fDesiredDistance < 16.0) && (fWptDistance < 30))
   {
      Vector vNextFrameOrigin = pev->origin + (pev->velocity * g_fTimeFrameInterval);

      if ((vNextFrameOrigin - m_vWptOrigin).Length () > fWptDistance)
         fDesiredDistance = fWptDistance + 1.0;
   }

   if (fWptDistance < fDesiredDistance)
   {
      // Did we reach a destination Waypoint?
      if (CurrentTask ()->iData == m_iCurrWptIndex)
      {
         // add goal values
         if (m_iChosenGoalIndex != -1)
         {
            int iWPTValue;
            int iStartIndex = m_iChosenGoalIndex;
            int iGoalIndex = m_iCurrWptIndex;

            if (GetTeam (GetEntity ()) == TEAM_TERRORIST)
            {
               iWPTValue = (g_pExperienceData + (iStartIndex * g_iNumWaypoints) + iGoalIndex)->wTeam0Value;
               iWPTValue += pev->health * 0.5;
               iWPTValue += m_fGoalValue * 0.5;

               if (iWPTValue < -MAX_GOAL_VAL)
                  iWPTValue = -MAX_GOAL_VAL;
               else if (iWPTValue > MAX_GOAL_VAL)
                  iWPTValue = MAX_GOAL_VAL;

               (g_pExperienceData + (iStartIndex * g_iNumWaypoints) + iGoalIndex)->wTeam0Value = iWPTValue;
            }
            else
            {
               iWPTValue = (g_pExperienceData + (iStartIndex * g_iNumWaypoints) + iGoalIndex)->wTeam1Value;
               iWPTValue += pev->health * 0.5;
               iWPTValue += m_fGoalValue * 0.5;

               if (iWPTValue < -MAX_GOAL_VAL)
                  iWPTValue = -MAX_GOAL_VAL;
               else if (iWPTValue > MAX_GOAL_VAL)
                  iWPTValue = MAX_GOAL_VAL;

               (g_pExperienceData + (iStartIndex * g_iNumWaypoints) + iGoalIndex)->wTeam1Value = iWPTValue;
            }
         }
         return true;
      }
      else if (m_pWaypointNodes == NULL)
         return false;

      // defusion map?
      if ((g_iMapType & MAP_DE) && g_bBombPlanted && (GetTeam (GetEntity ()) == TEAM_CT) && (CurrentTask ()->iTask != TASK_ESCAPEFROMBOMB))
      {
         int iWPTIndex = CurrentTask ()->iData;

         if (iWPTIndex != -1)
         {
            float fDistance = (pev->origin - g_pWaypoint->GetPath (iWPTIndex)->vOrigin).Length ();
            
            // bot within 'hearable' bomb tick noises?
            if (fDistance < MAX_BOMBHEARDISTANCE)
            {
               Vector vBombPos;

               // Does hear Bomb?
               if (CanHearC4 (&vBombPos))
               {
                  fDistance = (vBombPos - g_pWaypoint->GetPath (iWPTIndex)->vOrigin).Length ();
                  
                  if (fDistance > 512.0)
                  {
                     // Doesn't hear so not a good goal
                     CTBombPointClear (iWPTIndex);
                     TaskComplete ();
                  }
               }
               else
               {
                  // Doesn't hear so not a good goal
                  CTBombPointClear (iWPTIndex);
                  TaskComplete ();
               }
            }
         }
      }
      // do the actual movement checking
      HeadTowardWaypoint ();
   }
   return false;
}

void CBot::FindShortestPath (int iSourceIndex, int iDestIndex)
{
   // Finds the shortest path from iSourceIndex to iDestIndex

   DeleteSearchNodes ();

   m_iChosenGoalIndex = iSourceIndex;
   m_fGoalValue = 0.0;

   tPathNode *pNode = AllocMem <tPathNode> ();

   if (pNode == NULL)
      TerminateOnMalloc ();

   pNode->iIndex = iSourceIndex;
   pNode->pNext = NULL;

   m_pWayNodesStart = pNode;
   m_pWaypointNodes = m_pWayNodesStart;

   while (iSourceIndex != iDestIndex)
   {
      iSourceIndex = *(g_pWaypoint->m_pPathMatrix + (iSourceIndex * g_iNumWaypoints) + iDestIndex);
      
      if (iSourceIndex < 0)
      {
         m_iPrevGoalIndex = -1;
         CurrentTask ()->iData = -1;

         return;
      }

      pNode->pNext = AllocMem <tPathNode> ();
      pNode = pNode->pNext;

      if (pNode == NULL)
         TerminateOnMalloc ();

      pNode->iIndex = iSourceIndex;
      pNode->pNext = NULL;
   }
}

// Priority queue class (smallest item out first)
class CQueuePriority
{
public:
   CQueuePriority (void);
   ~CQueuePriority (void);

   inline int  Empty (void) { return (m_cSize == 0); }
   inline int  Size (void) { return (m_cSize); }
   void        Insert (int, float);
   int         Remove (void);

private:
   struct tHeapNode
   {
      int   iId;
      float fPriority;
   } *m_pHeap;

   int         m_cSize;
   int         m_cHeapSize;

   void        HeapSiftDown (int);
   void        HeapSiftUp (void);
};

CQueuePriority::CQueuePriority (void)
{
   m_cSize = 0;
   m_cHeapSize = MAX_WAYPOINTS * 4;
   m_pHeap = AllocMem <tHeapNode> (sizeof (tHeapNode) * m_cHeapSize);
   
   if (m_pHeap == NULL)
      TerminateOnMalloc ();
}

CQueuePriority::~CQueuePriority (void)
{
   delete [] m_pHeap;
}

// inserts a value into the priority queue
void CQueuePriority::Insert (int iValue, float fPriority)
{
   if (m_cSize >= m_cHeapSize)
   {
      m_cHeapSize += 100;
      m_pHeap = (tHeapNode *)realloc (m_pHeap, sizeof (tHeapNode) * m_cHeapSize);
      
      if (m_pHeap == NULL)
         TerminateOnMalloc ();
   }

   m_pHeap[m_cSize].fPriority = fPriority;
   m_pHeap[m_cSize].iId = iValue;

   m_cSize++;
   HeapSiftUp ();
}

// removes the smallest item from the priority queue
int CQueuePriority::Remove (void)
{
   int iReturn = m_pHeap[0].iId;

   m_cSize--;
   m_pHeap[0] = m_pHeap[m_cSize];

   HeapSiftDown (0);
   return iReturn;
}

#define HEAP_LEFT_CHILD(kX)    ()
#define HEAP_RIGHT_CHILD(kX)   (2 * (kX) + 2)
#define HEAP_PARENT(kX)        (()

void CQueuePriority::HeapSiftDown (int iSubRoot)
{
   int iParent = iSubRoot;
   int iChild = (2 * iParent) + 1;

   tHeapNode kRef = m_pHeap[iParent];

   while (iChild < m_cSize)
   {
      int iRightChild = (2 * iParent) + 2;

      if (iRightChild < m_cSize)
      {
         if (m_pHeap[iRightChild].fPriority < m_pHeap[iChild].fPriority)
            iChild = iRightChild;
      }
      if (kRef.fPriority <= m_pHeap[iChild].fPriority)
         break;

      m_pHeap[iParent] = m_pHeap[iChild];

      iParent = iChild;
      iChild = (2 * iParent) + 1;
   }
   m_pHeap[iParent] = kRef;
}


void CQueuePriority::HeapSiftUp (void)
{
   int iChild = m_cSize - 1;

   while (iChild)
   {
      int iParent = (iChild - 1) / 2;

      if (m_pHeap[iParent].fPriority <= m_pHeap[iChild].fPriority)
         break;

      tHeapNode kTmp = m_pHeap[iChild];

      m_pHeap[iChild] = m_pHeap[iParent];
      m_pHeap[iParent] = kTmp;

      iChild = iParent;
   }
}

float gfunctionKillsDistT (int iThisIndex, int iParent)
{
   // least kills and number of nodes to goal for a team

   float fCost = (g_pExperienceData + (iThisIndex * g_iNumWaypoints) + iThisIndex)->uTeam0Damage + g_cKillHistory;

   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      int iNeighbour = g_pWaypoint->GetPath (iThisIndex)->iIndex[i];

      if (iNeighbour != -1)
         fCost += (g_pExperienceData + (iNeighbour * g_iNumWaypoints) + iNeighbour)->uTeam0Damage * 0.3;
   }

   if (g_pWaypoint->GetPath (iThisIndex)->iFlags & W_FL_CROUCH)
      fCost *= 2.0;

   return fCost * (g_rgpcvBotCVar[CVAR_DANGERFACTOR]->GetFloat () * 2 / 3);
}

float gfunctionKillsT (int iThisIndex, int iParent)
{
   // least kills to goal for a team

   float fCost = (g_pExperienceData + (iThisIndex * g_iNumWaypoints) + iThisIndex)->uTeam0Damage;

   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      int iNeighbour = g_pWaypoint->GetPath (iThisIndex)->iIndex[i];

      if (iNeighbour != -1)
         fCost += (g_pExperienceData + (iNeighbour * g_iNumWaypoints) + iNeighbour)->uTeam0Damage * 0.3;
   }

   if (g_pWaypoint->GetPath (iThisIndex)->iFlags & W_FL_CROUCH)
      fCost *= 2.0;

   return fCost * (g_rgpcvBotCVar[CVAR_DANGERFACTOR]->GetFloat () * 2 / 3) + 0.1;
}

float gfunctionKillsDistCT (int iThisIndex, int iParent)
{
   // least kills and number of nodes to goal for a team

   float fCost = (g_pExperienceData + (iThisIndex * g_iNumWaypoints) + iThisIndex)->uTeam1Damage + g_cKillHistory;

   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      int iNeighbour = g_pWaypoint->GetPath (iThisIndex)->iIndex[i];
      
      if (iNeighbour != -1)
         fCost += (g_pExperienceData + (iNeighbour * g_iNumWaypoints) + iNeighbour)->uTeam1Damage * 0.3;
   }

   if (g_pWaypoint->GetPath (iThisIndex)->iFlags & W_FL_CROUCH)
      fCost *= 2.0;

   return fCost * (g_rgpcvBotCVar[CVAR_DANGERFACTOR]->GetFloat () * 2 / 3);
}

float gfunctionKillsCT (int iThisIndex, int iParent)
{
   float fCost = (g_pExperienceData + (iThisIndex * g_iNumWaypoints) + iThisIndex)->uTeam1Damage;

   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      int iNeighbour = g_pWaypoint->GetPath (iThisIndex)->iIndex[i];

      if (iNeighbour != -1)
         fCost += (g_pExperienceData + (iNeighbour * g_iNumWaypoints) + iNeighbour)->uTeam1Damage * 0.3;
   }

   if (g_pWaypoint->GetPath (iThisIndex)->iFlags & W_FL_CROUCH)
      fCost *= 2.0;

   return fCost * (g_rgpcvBotCVar[CVAR_DANGERFACTOR]->GetFloat () * 2 / 3) + 0.1;
}

float gfunctionKillsDistCTNoHostage (int iThisIndex, int iParent)
{
   if (g_pWaypoint->GetPath (iThisIndex)->iFlags & W_FL_NOHOSTAGE)
      return 65355;
   else if (g_pWaypoint->GetPath (iThisIndex)->iFlags & (W_FL_CROUCH | W_FL_LADDER))
      return gfunctionKillsDistCT (iThisIndex, iParent) * 500;

   return gfunctionKillsDistCT (iThisIndex, iParent);
}

float gfunctionKillsCTNoHostage (int iThisIndex, int iParent)
{
   if (g_pWaypoint->GetPath (iThisIndex)->iFlags & W_FL_NOHOSTAGE)
      return 65355;
   else if (g_pWaypoint->GetPath (iThisIndex)->iFlags & (W_FL_CROUCH | W_FL_LADDER))
      return gfunctionKillsCT (iThisIndex, iParent) * 500;
   
   return gfunctionKillsCT (iThisIndex, iParent);
}

float hfunctionSquareDist (int iIndex, int iStartIndex, int iGoalIndex)
{
   // using square heuristic

   float fDeltaX = fabsf (g_pWaypoint->GetPath (iGoalIndex)->vOrigin.x - g_pWaypoint->GetPath (iStartIndex)->vOrigin.x);
   float fDeltaY = fabsf (g_pWaypoint->GetPath (iGoalIndex)->vOrigin.y - g_pWaypoint->GetPath (iStartIndex)->vOrigin.y);
   float fDeltaZ = fabsf (g_pWaypoint->GetPath (iGoalIndex)->vOrigin.z - g_pWaypoint->GetPath (iStartIndex)->vOrigin.z);

   return (fDeltaX + fDeltaY + fDeltaZ);
}

float hfunctionNumberNodes (int iIndex, int iStartIndex, int iGoalIndex)
{
   return hfunctionSquareDist (iIndex, iStartIndex, iGoalIndex) / 128 * g_cKillHistory;
} 

void CBot::FindPath (int iSourceIndex, int iDestIndex, unsigned char byPathType)
{
   // this fnuction finds a path from iSourceIndex to iDestIndex

   if ((iSourceIndex > g_iNumWaypoints - 1) || (iSourceIndex < 0))
   {
      ServerPrint ("ERROR: FindPath Source Invalid->%d", iSourceIndex);
      return;
   }

   if ((iDestIndex > g_iNumWaypoints - 1) || (iDestIndex < 0))
   {
      ServerPrint ("ERROR: FindPath Destination Invalid->%d", iDestIndex);
      return;
   }

   DeleteSearchNodes ();

   m_iChosenGoalIndex = iSourceIndex;
   m_fGoalValue = 0.0;

   // A* Stuff
   enum eAstarState {OPEN, CLOSED, NEW};

   struct tAstarList
   {
      double fG;
      double fF;
      short iParentIndex;

      eAstarState iState;
   } rgkAstarList[MAX_WAYPOINTS];

   CQueuePriority kOpenList;

   for (int i = 0; i < MAX_WAYPOINTS; i++)
   {
      rgkAstarList[i].fG = 0;
      rgkAstarList[i].fF = 0;
      rgkAstarList[i].iParentIndex = -1;
      rgkAstarList[i].iState = NEW;
   }

   float (*pfnGCalc) (int, int);
   float (*pfnHCalc) (int, int, int);

   if (byPathType == 1)
   {
      if (GetTeam (GetEntity ()) == TEAM_TERRORIST)
      {
         pfnGCalc = gfunctionKillsDistT;
         pfnHCalc = hfunctionNumberNodes;
      }
      else if (HasHostage ())
      {
         pfnGCalc = gfunctionKillsDistCTNoHostage;
         pfnHCalc = hfunctionNumberNodes;
      }
      else
      {
         pfnGCalc = gfunctionKillsDistCT;
         pfnHCalc = hfunctionNumberNodes;
      }
   }
   else
   {
      if (GetTeam (GetEntity ()) == TEAM_TERRORIST)
      {
         pfnGCalc = gfunctionKillsT;
         pfnHCalc = hfunctionSquareDist;
      }
      else if (HasHostage ())
      {
         pfnGCalc = gfunctionKillsCTNoHostage;
         pfnHCalc = hfunctionSquareDist;
      }
      else
      {
         pfnGCalc = gfunctionKillsCT;
         pfnHCalc = hfunctionSquareDist;
      }
   }

   // put start node into open list
   rgkAstarList[iSourceIndex].fG = pfnGCalc (iSourceIndex, -1);
   rgkAstarList[iSourceIndex].fF = rgkAstarList[iSourceIndex].fG + pfnHCalc (iSourceIndex, iSourceIndex, iDestIndex);
   rgkAstarList[iSourceIndex].iState = OPEN;

   kOpenList.Insert (iSourceIndex, rgkAstarList[iSourceIndex].fG);

   while (!kOpenList.Empty ())
   {
      // remove the first node from the open list
      int iCurrentIndex = kOpenList.Remove ();

      // is the current node the goal node?
      if (iCurrentIndex == iDestIndex)
      {
         // build the complete path
         m_pWaypointNodes = NULL;
         do
         {
            tPathNode *pPath = AllocMem <tPathNode> ();
            
            if (pPath == NULL)
               TerminateOnMalloc ();

            pPath->iIndex = iCurrentIndex;
            pPath->pNext = m_pWaypointNodes;
            
            m_pWaypointNodes = pPath;
            iCurrentIndex = rgkAstarList[iCurrentIndex].iParentIndex;

         } while (iCurrentIndex != -1);

         m_pWayNodesStart = m_pWaypointNodes;
         return;
      }

      if (rgkAstarList[iCurrentIndex].iState != OPEN)
         continue;

      // put current node into CLOSED list
      rgkAstarList[iCurrentIndex].iState = CLOSED;

      // now expand the current node
      for (int i = 0; i < MAX_PATH_INDEX; i++)
      {
         int iCurChild = g_pWaypoint->GetPath (iCurrentIndex)->iIndex[i];

         if (iCurChild == -1)
            continue;

         // calculate the F value as F = G + H
         float fG = rgkAstarList[iCurrentIndex].fG + pfnGCalc (iCurChild, iCurrentIndex);
         float fH = pfnHCalc (iCurChild, iSourceIndex, iDestIndex);
         float fF = fG + fH;

         if ((rgkAstarList[iCurChild].iState == NEW) || (rgkAstarList[iCurChild].fF > fF))
         {
            // put the current child into open list
            rgkAstarList[iCurChild].iParentIndex = iCurrentIndex;
            rgkAstarList[iCurChild].iState = OPEN;
            
            rgkAstarList[iCurChild].fG = fG;
            rgkAstarList[iCurChild].fF = fF;
            
            kOpenList.Insert (iCurChild, fG);
         }
      }
   }
   FindShortestPath (iSourceIndex, iDestIndex); // A* found no path, try floyd pathfinder instead
}

void CBot::DeleteSearchNodes (void)
{
   tPathNode *pNode, *pDelNode;

   pNode = m_pWayNodesStart;

   while (pNode != NULL)
   {
      pDelNode = pNode->pNext;
      delete pNode;

      pNode = pDelNode;
   }
   m_pWayNodesStart = NULL;
   m_pWaypointNodes = NULL;
}

int CBot::GetAimingWaypoint (Vector vTargetPos, int iCount)
{
   // return the most distant waypoint which is seen from the Bot to the Target and is within count

   if (m_iCurrWptIndex == -1)
      ChangeWptIndex (g_pWaypoint->FindNearest (pev->origin));

   int iSourceIndex = m_iCurrWptIndex;
   int iDestIndex = g_pWaypoint->FindNearest (vTargetPos);
   int iBestIndex = iSourceIndex;
   
   tPathNode *pNode = AllocMem <tPathNode> ();

   if (pNode == NULL)
      TerminateOnMalloc ();

   pNode->iIndex = iDestIndex;
   pNode->pNext = NULL;

   tPathNode *pStartNode = pNode;

   while (iDestIndex != iSourceIndex)
   {
      iDestIndex = *(g_pWaypoint->m_pPathMatrix + (iDestIndex * g_iNumWaypoints) + iSourceIndex);
      
      if (iDestIndex < 0)
         break;

      pNode->pNext = AllocMem <tPathNode> ();
      pNode = pNode->pNext;

      if (pNode == NULL)
         TerminateOnMalloc ();

      pNode->iIndex = iDestIndex;
      pNode->pNext = NULL;

      if (g_pWaypoint->IsVisible (m_iCurrWptIndex, iDestIndex))
      {
         iBestIndex = iDestIndex;
         break;
      }
   }
   while (pStartNode != NULL)
   {
      pNode = pStartNode->pNext;
      delete pStartNode;

      pStartNode = pNode;
   }
   return iBestIndex;
}

bool CBot::FindWaypoint (void)
{
   // Finds a waypoint in the near of the bot if he lost his path or if pathfinding needs to be started over again.

   int i, iWptIndex[3], iSelectIndex;
   int iCoveredWpt = -1;
   float fDistance, fMinDistance[3];

   Vector vSource, vDestination;
   TraceResult tr;

   for (i = 0; i < 3; i++)
   {
      iWptIndex[i] = -1;
      fMinDistance[i] = FLT_MAX;
   }

   for (i = 0; i < g_iNumWaypoints; i++)
   {
      // ignore current waypoint and previous recent waypoints...
      if ((i == m_iCurrWptIndex) || (i == m_rgiPrevWptIndex[0]) || (i == m_rgiPrevWptIndex[1]) || (i == m_rgiPrevWptIndex[2]) || (i == m_rgiPrevWptIndex[3]) || (i == m_rgiPrevWptIndex[4]))
         continue;

      if (g_pWaypoint->Reachable (pev->origin, g_pWaypoint->GetPath (i)->vOrigin, GetEntity ()))
      {
         // Don't use duck Waypoints if Bot got a hostage
         if (HasHostage () && (g_pWaypoint->GetPath (i)->iFlags & W_FL_NOHOSTAGE))
            continue;

         CBot *pOtherBot;
         bool bWaypointInUse = false;

         // check if this waypoint is already in use by another bot
         for (int c = 0; c < MaxClients (); c++)
         {
            pOtherBot = g_pBotManager->GetBot (c);

            if ((pOtherBot != NULL) && (pOtherBot != this) && (pOtherBot->m_iCurrWptIndex == i) && IsAlive (pOtherBot->GetEntity ()))
            {
               iCoveredWpt = i;
               bWaypointInUse = true;

               break;
            }
         }
         if (bWaypointInUse)
            continue;

         fDistance = LengthSquared (g_pWaypoint->GetPath (i)->vOrigin - pev->origin);

         if (fDistance < fMinDistance[0])
         {
            iWptIndex[0] = i;
            fMinDistance[0] = fDistance;
         }
         else if (fDistance < fMinDistance[1])
         {
            iWptIndex[1] = i;
            fMinDistance[1] = fDistance;
         }
         else if (fDistance < fMinDistance[2])
         {
            iWptIndex[2] = i;
            fMinDistance[2] = fDistance;
         }
      }
   }

   if (iWptIndex[2] != -1)
      i = RandomLong (0, 2);
   else if (iWptIndex[1] != -1)
      i = RandomLong (0, 1);
   else if (iWptIndex[0] != -1)
      i = 0;
   else if (iCoveredWpt != -1)
   {
      iWptIndex[0] = iCoveredWpt;
      i = 0;
   }
   else
   {
      iWptIndex[0] = RandomLong (0, g_iNumWaypoints - 1);
      i = 0;
   }

   iSelectIndex = iWptIndex[i];
   m_fCollideTime = WorldTime ();

   ChangeWptIndex (iSelectIndex);

   return true;
}

void CBot::GetValidWaypoint (void)
{
   // checks if the last waypoint the bot was heading for is still valid

   // if bot hasn't got a waypoint we need a new one anyway or if time to get there expired get new one as well
   if ((m_iCurrWptIndex == -1) || ((m_fWptTimeset + 3.0 < WorldTime ()) && FNullEnt (m_pentEnemy)))
   {
      DeleteSearchNodes ();
      FindWaypoint ();

      m_vWptOrigin = g_pWaypoint->GetPath (m_iCurrWptIndex)->vOrigin;

      // FIXME: Do some error checks if we got a waypoint
   }
}

void CBot::ChangeWptIndex (int iWptIndex)
{
   m_rgiPrevWptIndex[4] = m_rgiPrevWptIndex[3];
   m_rgiPrevWptIndex[3] = m_rgiPrevWptIndex[2];
   m_rgiPrevWptIndex[2] = m_rgiPrevWptIndex[1];
   m_rgiPrevWptIndex[0] = iWptIndex;

   m_iCurrWptIndex = iWptIndex;
   m_fWptTimeset = WorldTime ();

   // get the current waypoint flags
   if (m_iCurrWptIndex != -1)
      m_iWPTFlags = g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags;
   else
      m_iWPTFlags = 0;
}

int CBot::ChooseBombWaypoint (void)
{
   // finds the best goal (bomb) waypoint for CTs when searching for a planted bomb

   if (g_pWaypoint->m_arGoalPoints.IsFree ())
      return RandomLong (0, g_iNumWaypoints - 1); // reliability check

   float iMinDistance = FLT_MAX;
   float fActualDistance;

   int iGoal = 0;
   Vector vPosition;

   bool bHearBombTicks = CanHearC4 (&vPosition);

   if (!bHearBombTicks)
      vPosition = pev->origin;

   // find nearest goal waypoint either to bomb (if "heard" or player)
   ITERATE_ARRAY (g_pWaypoint->m_arGoalPoints, i)
   {
      fActualDistance = LengthSquared (g_pWaypoint->GetPath (g_pWaypoint->m_arGoalPoints[i])->vOrigin - vPosition);
      
      if (fActualDistance < iMinDistance)
      {
         iMinDistance = fActualDistance;
         iGoal = g_pWaypoint->m_arGoalPoints[i];
      }
   }

   int iCount = 0;
   while (IsBombPointWasVisited (iGoal))
   {
      iGoal = g_pWaypoint->m_arGoalPoints.Random ();
      iCount++;

      if (iCount >= g_pWaypoint->m_arGoalPoints.Size ())
         break;
   }
   return iGoal;
}

int CBot::FindDefendWaypoint (Vector vPosition)
{
   // this function tries to find a good position which has a line of sight to a position,
   // provides enough cover point, and is far away from the defending position

   TraceResult tr;

   int iWptIndex[MAX_PATH_INDEX];
   int iMinDistance[MAX_PATH_INDEX];

   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      iWptIndex[i] = -1;
      iMinDistance[i] = 128;
   }

   int iPosIndex = g_pWaypoint->FindNearest (vPosition);
   int iSrcIndex = g_pWaypoint->FindNearest (pev->origin);

   // some of points not found, return random one
   if ((iSrcIndex == -1) || (iPosIndex == -1))
      return RandomLong (0, g_iNumWaypoints - 1);

   for (int i = 0; i < g_iNumWaypoints; i++) // find the best waypoint now
   {
      // exclude ladder & current waypoints
      if ((g_pWaypoint->GetPath (i)->iFlags & W_FL_LADDER) || (i == iSrcIndex) || !g_pWaypoint->IsVisible (i, iPosIndex) || IsWaypointUsed (i))
         continue;

      // use the 'real' pathfinding distances
      int iDistance = g_pWaypoint->GetPathDistance (iSrcIndex, i);

      // skip wayponts with distance more than 1024 units
      if (iDistance > 1024)
         continue;
      
      TraceLine (g_pWaypoint->GetPath (i)->vOrigin, g_pWaypoint->GetPath (iPosIndex)->vOrigin, true, true, GetEntity (), &tr);

      // check if line not hit anything
      if (tr.flFraction != 1.0)
         continue;

      for (int j = 0; j < MAX_PATH_INDEX; j++)
      {
         if (iDistance > iMinDistance[j])
         {
            iWptIndex[j] = i;
            iMinDistance[j] = iDistance;

            break;
         }
      }
   }

   // use statistic if we have them
   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      if (iWptIndex[i] != -1)
      {
         tExperience *pExperience = (g_pExperienceData + (iWptIndex[i] * g_iNumWaypoints) + iWptIndex[i]);
         int iExperience = -1;

         if (GetTeam (GetEntity ()) == TEAM_TERRORIST)
            iExperience = pExperience->uTeam0Damage;
         else
            iExperience = pExperience->uTeam1Damage;

         iExperience = (iExperience * 100) / MAX_DAMAGE_VAL;
         iMinDistance[i] = (iExperience * 100) / 8192;
         iMinDistance[i] += iExperience;
      }
   }


   bool bOrderChanged;

   // sort results waypoints for farest distance
   do
   {
      bOrderChanged = false;

      // completely sort the data
      for (int i = 0; (i < 3) && (iWptIndex[i] != -1) && (iWptIndex[i + 1] != -1) && (iMinDistance[i] > iMinDistance[i + 1]); i++)
      {
         int iIndex = iWptIndex[i];

         iWptIndex[i] = iWptIndex[i + 1];
         iWptIndex[i + 1] = iIndex;
         iIndex = iMinDistance[i];
         iMinDistance[i] = iMinDistance[i + 1];
         iMinDistance[i + 1] = iIndex;

         bOrderChanged = true;
      }
   } while (bOrderChanged == true);

   for (int i = 0; i < MAX_PATH_INDEX; i++)
      if (iWptIndex[i] != -1)
         return iWptIndex[i];

   return RandomLong (0, g_iNumWaypoints - 1); // if no waypoint was found, just use a random one
}

int CBot::FindCoverWaypoint (float fMaxDistance)
{
   // this function tries to find a good cover waypoint if bot wants to hide

   // do not move to a position near to the enemy
   if (fMaxDistance > (m_vLastEnemyOrigin - pev->origin).Length ())
      fMaxDistance = (m_vLastEnemyOrigin - pev->origin).Length ();

   if (fMaxDistance < 300.0)
      fMaxDistance = 300.0;

   int iSourceIndex = m_iCurrWptIndex;
   int iEnemyIndex = g_pWaypoint->FindNearest (m_vLastEnemyOrigin);
   vector <int> kEnemyIndices;

   int iWptIndex[MAX_PATH_INDEX];
   int iMinDistance[MAX_PATH_INDEX];

   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      iWptIndex[i] = -1;
      iMinDistance[i] = fMaxDistance;
   }

   if (iEnemyIndex == -1)
      return -1;

   // now get enemies neigbouring points
   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      if (g_pWaypoint->GetPath (iEnemyIndex)->iIndex[i] != -1)
         kEnemyIndices.AddItem (g_pWaypoint->GetPath (iEnemyIndex)->iIndex[i]);
   }

   // ensure we're on valid point
   ChangeWptIndex (iSourceIndex);

   // find the best waypoint now
   for (int i = 0; i < g_iNumWaypoints; i++)
   {
      // exclude ladder, current waypoints and waypoints seen by the enemy
      if ((g_pWaypoint->GetPath (i)->iFlags & W_FL_LADDER) || (i == iSourceIndex) || g_pWaypoint->IsVisible (iEnemyIndex, i))
         continue;

      bool bNeighbourVisible = false;  // now check neighbour waypoints for visibility

      ITERATE_ARRAY (kEnemyIndices, j)
      {
         if (g_pWaypoint->IsVisible (kEnemyIndices[j], i))
         {
            bNeighbourVisible = true;
            break;
         }
      }

      // skip visible points
      if (bNeighbourVisible)
         continue;

      // use the 'real' pathfinding distances
      int iDistance = g_pWaypoint->GetPathDistance (iSourceIndex, i);
      int iEnemyDistance = g_pWaypoint->GetPathDistance (iEnemyIndex, i);

      if (iDistance >= iEnemyDistance)
         continue;
      
      for (int j = 0; j < MAX_PATH_INDEX; j++)
      {
         if (iDistance < iMinDistance[j])
         {
            iWptIndex[j] = i;
            iMinDistance[j] = iDistance;

            break;
         }
      }
   }

   // use statistic if we have them
   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      if (iWptIndex[i] != -1)
      {
         tExperience *pExperience = (g_pExperienceData + (iWptIndex[i] * g_iNumWaypoints) + iWptIndex[i]);
         int iExperience = -1;

         if (GetTeam (GetEntity ()) == TEAM_TERRORIST)
            iExperience = pExperience->uTeam0Damage;
         else
            iExperience = pExperience->uTeam1Damage;

         iExperience = (iExperience * 100) / MAX_DAMAGE_VAL;
         iMinDistance[i] = (iExperience * 100) / 8192;
         iMinDistance[i] += iExperience;
      }
   }

   bool bOrderChanged;

   // sort resulting waypoints for nearest distance
   do
   {
      bOrderChanged = false;

      for (int i = 0; (i < 3) && (iWptIndex[i] != -1) && (iWptIndex[i + 1] != -1) && (iMinDistance[i] > iMinDistance[i + 1]); i++)
      {
         int iIndex = iWptIndex[i];

         iWptIndex[i] = iWptIndex[i + 1];
         iWptIndex[i + 1] = iIndex;
         iIndex = iMinDistance[i];
         iMinDistance[i] = iMinDistance[i + 1];
         iMinDistance[i + 1] = iIndex;

         bOrderChanged = true;
      }
   } while (bOrderChanged);

   TraceResult tr;

   // take the first one which isn't spotted by the enemy
   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      if (iWptIndex[i] != -1)
      {
         TraceLine (m_vLastEnemyOrigin + Vector (0, 0, 36), g_pWaypoint->GetPath (iWptIndex[i])->vOrigin, true, true, GetEntity (), &tr);

         if (tr.flFraction < 1.0)
            return iWptIndex[i];
      }
   }

   // if all are seen by the enemy, take the first one 
   if (iWptIndex[0] != -1)
      return iWptIndex[0];

   return -1; // do not use random points
}

bool CBot::GetBestNextWaypoint (void)
{
   // this function does a realtime postprocessing of waypoints return from the
   // pathfinder, to vary paths and find the best waypoint on our way

   InternalAssert (m_pWaypointNodes != NULL);
   InternalAssert (m_pWaypointNodes->pNext != NULL);

   int iOtherWPIndex;
   bool bWaypointInUse = false;

   for (int i = 0; i < MaxClients (); i++)
   {
      CBot *pOtherBot = g_pBotManager->GetBot (i);
      
      if ((pOtherBot != NULL) && (pOtherBot != this) && IsAlive (pOtherBot->GetEntity ()))
      {
         if (GetShootingConeDeviation (pOtherBot->GetEntity (), &pev->origin) >= 0.7)
            iOtherWPIndex = pOtherBot->m_rgiPrevWptIndex[0];
         else
            iOtherWPIndex = pOtherBot->m_iCurrWptIndex;

         if (m_pWaypointNodes->iIndex == iOtherWPIndex)
         {
            bWaypointInUse = true;
            break;
         }
      }
   }

   if (!bWaypointInUse)
      return false;

   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      int iId = g_pWaypoint->GetPath (m_iCurrWptIndex)->iIndex[i];
      
      if ((iId != -1) && g_pWaypoint->IsConnected (iId, m_pWaypointNodes->pNext->iIndex) && g_pWaypoint->IsConnected (iId, m_iCurrWptIndex))
      {
         if (g_pWaypoint->GetPath (iId)->iFlags & W_FL_LADDER) // don't use ladder waypoints as alternative
            continue;

         // check if this Waypoint is already in use by another bot
         bWaypointInUse = false;

         for (int c = 0; c < MaxClients (); c++)
         {
            CBot *pOtherBot = g_pBotManager->GetBot (c);

            if ((pOtherBot != NULL) && (pOtherBot != this) && IsAlive (pOtherBot->GetEntity ()) && pOtherBot != this)
            {
               if (GetShootingConeDeviation (pOtherBot->GetEntity (), &pev->origin) >= 0.7)
                  iOtherWPIndex = pOtherBot->m_rgiPrevWptIndex[0];
               else
                  iOtherWPIndex = pOtherBot->m_iCurrWptIndex;

               if (iId == iOtherWPIndex)
               {
                  bWaypointInUse = true;
                  break;
               }
            }
         }
         
         // waypoint not used by another bot - feel free to use it
         if (!bWaypointInUse)
         {
            m_pWaypointNodes->iIndex = iId;
            return true;
         }
      }
   }
   return false;
}

bool CBot::HeadTowardWaypoint (void)
{
   // advances in our pathfinding list and sets the appropiate destination origins for this bot

   Vector vSource, vDestination;
   TraceResult tr;

   float fJumpDistance = 0.0;

   // check if old waypoints is still reliable
   GetValidWaypoint ();

   // no waypoints from pathfinding?
   if (m_pWaypointNodes == NULL)
      return false;

   m_pWaypointNodes = m_pWaypointNodes->pNext; // advance in list
   m_uiCurrTravelFlags = 0; // reset travel flags (jumping etc)

   // we're not at the end of the list?
   if (m_pWaypointNodes != NULL)
   {
      // if in between a route, postprocess the waypoint (find better alternatives)...
      if ((m_pWaypointNodes != m_pWayNodesStart) && (m_pWaypointNodes->pNext != NULL))
      {
         GetBestNextWaypoint ();
         eTask iTask = CurrentTask ()->iTask;

         m_fMinSpeed = pev->maxspeed;

         // only if we in normal task and bomb is not planted
         if ((iTask == TASK_NORMAL) && !g_bBombPlanted)
         {
            m_iCampButtons = 0;

            int iWaypoint = m_pWaypointNodes->pNext->iIndex;
            int iKills = 0;

            if (GetTeam (GetEntity ()) == TEAM_TERRORIST)
               iKills = (g_pExperienceData + (iWaypoint * g_iNumWaypoints) + iWaypoint)->uTeam0Damage;
            else
               iKills = (g_pExperienceData + (iWaypoint * g_iNumWaypoints) + iWaypoint)->uTeam1Damage;

            // if damage done higher than one
            if ((iKills > 1) && (g_fTimeRoundMid > WorldTime ()) && (g_cKillHistory > 0))
            {
               iKills = iKills / g_cKillHistory;

               switch (m_ucPersonality)
               {
               case PERSONALITY_AGRESSIVE:
                  iKills /= 3;
                  break;

               default:
                  iKills /= 2;
                  break;
               }

               if (m_fBaseAgressionLevel < static_cast <float> (iKills))
               {
                  StartTask (TASK_CAMP, TASKPRI_CAMP, -1, RandomFloat (20, 40), true); // push camp task on to stack
                  StartTask (TASK_MOVETOPOSITION, TASKPRI_MOVETOPOSITION, FindDefendWaypoint (g_pWaypoint->GetPath (iWaypoint)->vOrigin), 0.0, true);
               }
            }
            else if (g_bBotsCanPause && !IsOnLadder () && !IsInWater () && (m_uiCurrTravelFlags == NULL) && IsOnFloor ())
            {
               if (static_cast <float> (iKills) == m_fBaseAgressionLevel)
                  m_iCampButtons |= IN_DUCK;
               else if (RandomLong (1, 100) > (m_iSkill + RandomLong (1, 20)))
                  m_fMinSpeed = GetWalkSpeed ();
            }
         }
      }

      if (m_pWaypointNodes != NULL)
      {
         int iDestIndex = m_pWaypointNodes->iIndex;

         // Find out about connection flags
         if (m_iCurrWptIndex != -1)
         {
            tPath *pPath = g_pWaypoint->GetPath (m_iCurrWptIndex);

            for (int i = 0; i < MAX_PATH_INDEX; i++)
            {
               if (pPath->iIndex[i] == m_pWaypointNodes->iIndex)
               {
                  m_uiCurrTravelFlags = pPath->uConnectFlag[i];
                  m_vDesiredVelocity = pPath->vConnectVel[i];
                  m_bJumpDone = false;

                  break;
               }
            }

            // check if bot is going to jump
            bool bWillJump = false;

            // try to find out about future connection flags
            if (m_pWaypointNodes->pNext != NULL)
            {
               for (int i = 0; i < MAX_PATH_INDEX; i++)
               {
                  if ((g_pWaypoint->GetPath (m_pWaypointNodes->iIndex)->iIndex[i] == m_pWaypointNodes->pNext->iIndex) && (g_pWaypoint->GetPath (m_pWaypointNodes->iIndex)->uConnectFlag[i] & C_FL_JUMP))
                  {
                     bWillJump = true;

                     vSource = g_pWaypoint->GetPath (m_pWaypointNodes->iIndex)->vOrigin;
                     vDestination = g_pWaypoint->GetPath (m_pWaypointNodes->pNext->iIndex)->vOrigin;

                     fJumpDistance = (g_pWaypoint->GetPath (m_pWaypointNodes->iIndex)->vOrigin - g_pWaypoint->GetPath (m_pWaypointNodes->pNext->iIndex)->vOrigin).Length ();
                     break;
                  }
               }
            }

            // is there a jump waypoint right ahead and do we need to draw out the light weapon ?
            if (bWillJump && ((fJumpDistance > 210) || ((vDestination.z + 32.0 > vSource.z) && (fJumpDistance > 150)) || (((vDestination - vSource).Length2D () < 50) && (fJumpDistance > 60))) && FNullEnt (m_pentEnemy) && (m_iCurrentWeapon != WEAPON_KNIFE) && !m_bIsReloading)
               SelectWeaponByName ("weapon_knife"); // draw out the knife if we needed
 
            // Bot not already on ladder but will be soon?
            if ((g_pWaypoint->GetPath (iDestIndex)->iFlags & W_FL_LADDER) && !IsOnLadder ())
            {
               CBot *pOtherBot;

               // Get ladder waypoints used by other (first moving) bots
               for (int c = 0; c < MaxClients (); c++)
               {
                  pOtherBot = g_pBotManager->GetBot (c);

                  // if another bot uses this ladder, wait 3 secs
                  if ((pOtherBot != NULL) && (pOtherBot != this) && IsAlive (pOtherBot->GetEntity ()) && (pOtherBot->m_iCurrWptIndex == m_pWaypointNodes->iIndex))
                  {
                     StartTask (TASK_PAUSE, TASKPRI_PAUSE, -1, WorldTime () + 3.0, false);
                     return true;
                  }
               }
            }
         }
         ChangeWptIndex (iDestIndex);
      }
   }

   m_vWptOrigin = g_pWaypoint->GetPath (m_iCurrWptIndex)->vOrigin;

   // if wayzone radios non zero vary origin a bit depending on the body angles
   if (g_pWaypoint->GetPath (m_iCurrWptIndex)->fRadius > 0.0)
   {
      MakeVectors (pev->angles);
      m_vWptOrigin = ((m_vWptOrigin + g_pGlobals->v_right * RandomFloat (-g_pWaypoint->GetPath (m_iCurrWptIndex)->fRadius, g_pWaypoint->GetPath (m_iCurrWptIndex)->fRadius)) + (m_vWptOrigin + g_pGlobals->v_forward * RandomFloat (0, g_pWaypoint->GetPath (m_iCurrWptIndex)->fRadius))) * 0.5;
   }
 
   // is it a ladder? then we need to adjust the waypoint origin to make sure bot doesn't face wrong direction
   if (g_pWaypoint->GetPath (m_iCurrWptIndex)->iFlags & W_FL_LADDER)
   {
      edict_t *pLadder = NULL;

      // Find the Ladder Entity
      while (!FNullEnt (pLadder = FIND_ENTITY_IN_SPHERE (pLadder, m_vWptOrigin, 100)))
      {
         if (strcmp ("func_ladder", STRING (pLadder->v.classname)) == 0)
            break;
      }

      // this routine tries to find the direction the ladder is facing, so
      // i can adjust the ladder wpt origin to be some units into the direction the bot wants
      // to move because otherwise the bot would turn all the time while climbing
      if (!FNullEnt (pLadder))
      {
         (*g_engfuncs.pfnTraceModel) (m_vWptOrigin, GetEntityOrigin (pLadder), point_hull, pLadder, &tr);
         
         m_vLookAt = m_vWptOrigin;

         if (m_vWptOrigin.z < pev->origin.z)
         {
            // wpt is below bot, so we need to climb down and need
            // to move wpt origin in the forward direction of the ladder
            m_iLadderDir = LADDER_DOWN;
            m_vWptOrigin = m_vWptOrigin + tr.vecPlaneNormal;
         }
         else
         {
            // wpt is above bot, so we need to climb up and need
            // to move wpt origin in the backward direction of the ladder
            m_iLadderDir = LADDER_UP;
            m_vWptOrigin = m_vWptOrigin - tr.vecPlaneNormal;
         }
      }
   }
   m_fWptTimeset = WorldTime ();

   return true;
}

bool CBot::CantMoveForward (Vector vNormal, TraceResult *tr)
{
   // Checks if bot is blocked in his movement direction (excluding doors)

   // use some TraceLines to determine if anything is blocking the current path of the bot.
   Vector vSource, vForward, vCenter;

   vCenter = pev->angles;
   vCenter.z = 0;
   vCenter.x = 0;

   MakeVectors (vCenter);

   // first do a trace from the bot's eyes forward...
   vSource = EyePosition ();
   vForward = vSource + vNormal * 24;

   // trace from the bot's eyes straight forward...
   TraceLine (vSource, vForward, true, GetEntity (), tr);

   // check if the trace hit something...
   if (tr->flFraction < 1.0)
   {
      if (strncmp ("func_door", STRING (tr->pHit->v.classname), 9) == 0)
         return false;
      return true;  // bot's head will hit something
   }

   // bot's head is clear, check at shoulder level...
   // trace from the bot's shoulder left diagonal forward to the right shoulder...
   vSource = EyePosition () + Vector (0, 0, -16) - g_pGlobals->v_right * 16;
   vForward = EyePosition () + Vector (0, 0, -16) + g_pGlobals->v_right * 16 + vNormal * 24;

   TraceLine (vSource, vForward, true, GetEntity (), tr);

   // check if the trace hit something...
   if ((tr->flFraction < 1.0) && (strncmp ("func_door", STRING (tr->pHit->v.classname), 9) != 0))
      return true;  // bot's body will hit something

   // bot's head is clear, check at shoulder level...
   // trace from the bot's shoulder right diagonal forward to the left shoulder...
   vSource = EyePosition () + Vector (0, 0, -16) + g_pGlobals->v_right * 16;
   vForward = EyePosition () + Vector (0, 0, -16) - g_pGlobals->v_right * 16 + vNormal * 24;

   TraceLine (vSource, vForward, true, GetEntity (), tr);

   // check if the trace hit something...
   if ((tr->flFraction < 1.0) && (strncmp ("func_door", STRING (tr->pHit->v.classname), 9) != 0))
      return true;  // bot's body will hit something

   // now check below waist
   if (pev->flags & FL_DUCKING)
   {
      vSource = pev->origin + Vector (0, 0, -19 + 19);
      vForward = vSource + Vector (0, 0, 10) + vNormal * 24;

      TraceLine (vSource, vForward, true, GetEntity (), tr);

      // check if the trace hit something...
      if ((tr->flFraction < 1.0) && (strncmp ("func_door", STRING (tr->pHit->v.classname), 9) != 0))
         return true;  // bot's body will hit something

      vSource = pev->origin;
      vForward = vSource + vNormal * 24;

      TraceLine (vSource, vForward, true, GetEntity (), tr);

      // check if the trace hit something...
      if ((tr->flFraction < 1.0) && (strncmp ("func_door", STRING (tr->pHit->v.classname), 9) != 0))
         return true;  // bot's body will hit something
   }
   else
   {
      // Trace from the left Waist to the right forward Waist Pos
      vSource = pev->origin + Vector (0, 0, -17) - g_pGlobals->v_right * 16;
      vForward = pev->origin + Vector (0, 0, -17) + g_pGlobals->v_right * 16 + vNormal * 24;

      // trace from the bot's waist straight forward...
      TraceLine (vSource, vForward, true, GetEntity (), tr);

      // check if the trace hit something...
      if ((tr->flFraction < 1.0) && (strncmp ("func_door", STRING (tr->pHit->v.classname), 9) != 0))
         return true;  // bot's body will hit something

      // trace from the left waist to the right forward waist pos
      vSource = pev->origin + Vector (0, 0, -17) + g_pGlobals->v_right * 16;
      vForward = pev->origin + Vector (0, 0, -17) - g_pGlobals->v_right * 16 + vNormal * 24;

      TraceLine (vSource, vForward, true, GetEntity (), tr);

      // check if the trace hit something...
      if ((tr->flFraction < 1.0) && (strncmp ("func_door", STRING (tr->pHit->v.classname), 9) != 0))
         return true;  // bot's body will hit something
   }
   return false;  // bot can move forward, return false
}

bool CBot::CanStrafeLeft (TraceResult *tr)
{
   // Check if bot can move sideways

   Vector vSource, vLeft;

   MakeVectors (pev->v_angle);
   vSource = pev->origin;
   vLeft = vSource - g_pGlobals->v_right * -40;

   // trace from the bot's waist straight left...
   TraceLine (vSource, vLeft, true, GetEntity (), tr);

   // check if the trace hit something...
   if (tr->flFraction < 1.0)
      return false;  // bot's body will hit something

   vLeft = vLeft + g_pGlobals->v_forward * 40;

   // trace from the strafe pos straight forward...
   TraceLine (vSource, vLeft, true, GetEntity (), tr);

   // check if the trace hit something...
   if (tr->flFraction < 1.0)
      return false;  // bot's body will hit something
   return true;
}

bool CBot::CanStrafeRight (TraceResult * tr)
{
   Vector vSource, vRight;

   MakeVectors (pev->v_angle);
   vSource = pev->origin;
   vRight = vSource + g_pGlobals->v_right * 40;

   // trace from the bot's waist straight right...
   TraceLine (vSource, vRight, true, GetEntity (), tr);

   // check if the trace hit something...
   if (tr->flFraction < 1.0)
      return false;  // bot's body will hit something

   vRight = vRight + g_pGlobals->v_forward * 40;

   // trace from the strafe pos straight forward...
   TraceLine (vSource, vRight, true, GetEntity (), tr);

   // check if the trace hit something...
   if (tr->flFraction < 1.0)
      return false;  // bot's body will hit something

   return true;
}

bool CBot::CanJumpUp (Vector vNormal)
{
   // this function check if bot can jump over some obstacle
   
   TraceResult tr;

   // can't jump if not on ground and not on ladder/swimming
   if (!IsOnFloor () && (IsOnLadder () || !IsInWater ()))
      return false;

   // convert current view angle to vectors for traceline math...
   Vector vJump = pev->angles;
   vJump.x = 0; // reset pitch to 0 (level horizontally)
   vJump.z = 0; // reset roll to 0 (straight up and down)

   MakeVectors (vJump);

   // check for normal jump height first...
   Vector vSource = pev->origin + Vector (0, 0, -36 + 45);
   Vector vDest = vSource + vNormal * 32;

   // trace a line forward at maximum jump height...
   TraceLine (vSource, vDest, true, GetEntity (), &tr);

   if (tr.flFraction < 1.0)
      goto CheckDuckJump;
   else
   {
      // now trace from jump height upward to check for obstructions...
      vSource = vDest;
      vDest.z = vDest.z + 37;

      TraceLine (vSource, vDest, true, GetEntity (), &tr);

      if (tr.flFraction < 1.0)
         return false;
   }

   // now check same height to one side of the bot...
   vSource = pev->origin + g_pGlobals->v_right * 16 + Vector (0, 0, -36 + 45);
   vDest = vSource + vNormal * 32;

   // trace a line forward at maximum jump height...
   TraceLine (vSource, vDest, true, GetEntity (), &tr);

   // if trace hit something, return false
   if (tr.flFraction < 1.0)
      goto CheckDuckJump;

   // now trace from jump height upward to check for obstructions...
   vSource = vDest;
   vDest.z = vDest.z + 37;

   TraceLine (vSource, vDest, true, GetEntity (), &tr);

   // if trace hit something, return false
   if (tr.flFraction < 1.0)
      return false;

   // now check same height on the other side of the bot...
   vSource = pev->origin + (-g_pGlobals->v_right * 16) + Vector (0, 0, -36 + 45);
   vDest = vSource + vNormal * 32;

   // trace a line forward at maximum jump height...
   TraceLine (vSource, vDest, true, GetEntity (), &tr);

   // if trace hit something, return false
   if (tr.flFraction < 1.0)
      goto CheckDuckJump;

   // now trace from jump height upward to check for obstructions...
   vSource = vDest;
   vDest.z = vDest.z + 37;

   TraceLine (vSource, vDest, true, GetEntity (), &tr);

   // if trace hit something, return false
   return tr.flFraction > 1.0;

   // here we check if a duck jump would work...
CheckDuckJump:
   
   // use center of the body first... maximum duck jump height is 62, so check one unit above that (63)
   vSource = pev->origin + Vector (0, 0, -36 + 63);
   vDest = vSource + vNormal * 32;

   // trace a line forward at maximum jump height...
   TraceLine (vSource, vDest, true, GetEntity (), &tr);

   if (tr.flFraction < 1.0)
      return false;
   else
   {
      // now trace from jump height upward to check for obstructions...
      vSource = vDest;
      vDest.z = vDest.z + 37;

      TraceLine (vSource, vDest, true, GetEntity (), &tr);

      // if trace hit something, check duckjump
      if (tr.flFraction < 1.0)
         return false;
   }

   // now check same height to one side of the bot...
   vSource = pev->origin + g_pGlobals->v_right * 16 + Vector (0, 0, -36 + 63);
   vDest = vSource + vNormal * 32;

   // trace a line forward at maximum jump height...
   TraceLine (vSource, vDest, true, GetEntity (), &tr);

   // if trace hit something, return false
   if (tr.flFraction < 1.0)
      return false;

   // now trace from jump height upward to check for obstructions...
   vSource = vDest;
   vDest.z = vDest.z + 37;

   TraceLine (vSource, vDest, true, GetEntity (), &tr);

   // if trace hit something, return false
   if (tr.flFraction < 1.0)
      return false;

   // now check same height on the other side of the bot...
   vSource = pev->origin + (-g_pGlobals->v_right * 16) + Vector (0, 0, -36 + 63);
   vDest = vSource + vNormal * 32;

   // trace a line forward at maximum jump height...
   TraceLine (vSource, vDest, true, GetEntity (), &tr);

   // if trace hit something, return false
   if (tr.flFraction < 1.0)
      return false;

   // now trace from jump height upward to check for obstructions...
   vSource = vDest;
   vDest.z = vDest.z + 37;

   TraceLine (vSource, vDest, true, GetEntity (), &tr);

   // if trace hit something, return false
   return tr.flFraction > 1.0;
}

bool CBot::CanDuckUnder (Vector vNormal)
{
   // this function check if bot can duck under obstacle

   TraceResult tr;
   Vector vBaseHeight;

   // convert current view angle to vectors for TraceLine math...
   Vector vDuck = pev->angles;
   vDuck.x = 0; // reset pitch to 0 (level horizontally)
   vDuck.z = 0; // reset roll to 0 (straight up and down)

   MakeVectors (vDuck);

   // use center of the body first...
   if (pev->flags & FL_DUCKING)
      vBaseHeight = pev->origin + Vector (0, 0, -17);
   else
      vBaseHeight = pev->origin;

   Vector vSource = vBaseHeight;
   Vector vDest = vSource + vNormal * 32;

   // trace a line forward at duck height...
   TraceLine (vSource, vDest, true, GetEntity (), &tr);

   // if trace hit something, return false
   if (tr.flFraction < 1.0)
      return false;

   // now check same height to one side of the bot...
   vSource = vBaseHeight + g_pGlobals->v_right * 16;
   vDest = vSource + vNormal * 32;

   // trace a line forward at duck height...
   TraceLine (vSource, vDest, true, GetEntity (), &tr);

   // if trace hit something, return false
   if (tr.flFraction < 1.0)
      return false;

   // now check same height on the other side of the bot...
   vSource = vBaseHeight + (-g_pGlobals->v_right * 16);
   vDest = vSource + vNormal * 32;

   // trace a line forward at duck height...
   TraceLine (vSource, vDest, true, GetEntity (), &tr);

   // if trace hit something, return false
   return tr.flFraction > 1.0;
}

bool CBot::IsBlockedLeft (void)
{
   Vector vSource, vLeft;
   TraceResult tr;

   int iDirection = 48;

   if (m_fMoveSpeed < 0)
      iDirection = -48;

   MakeVectors (pev->angles);

   // do a trace to the left...
   vSource = pev->origin;
   vLeft = vSource + g_pGlobals->v_forward * iDirection - g_pGlobals->v_right * 48;  // 48 units to the left

   TraceLine (vSource, vLeft, true, GetEntity (), &tr);

   // check if the trace hit something...
   if ((tr.flFraction < 1.0) && (strncmp ("func_door", STRING (tr.pHit->v.classname), 9) != 0))
      return true;  // bot's body will hit something
   return false;
}

bool CBot::IsBlockedRight (void)
{
   Vector vSource, vRight;
   TraceResult tr;

   int iDirection = 48;

   if (m_fMoveSpeed < 0)
      iDirection = -48;

   MakeVectors (pev->angles);

   // do a trace to the right...
   vSource = pev->origin;
   vRight = vSource + g_pGlobals->v_forward * iDirection + g_pGlobals->v_right * 48;  // 48 units to the right

   TraceLine (vSource, vRight, true, GetEntity (), &tr);

   // check if the trace hit something...
   if ((tr.flFraction < 1.0) && (strncmp ("func_door", STRING (tr.pHit->v.classname), 9) != 0))
      return true;  // bot's body will hit something
   return false;
}

bool CBot::CheckWallOnLeft (void)
{
   Vector vSource, vLeft;
   TraceResult tr;

   MakeVectors (pev->angles);

   // do a trace to the left...
   vSource = pev->origin;
   vLeft = vSource - g_pGlobals->v_right * 40;  // 40 units to the left

   TraceLine (vSource, vLeft, true, GetEntity (), &tr);

   // check if the trace hit something...
   if (tr.flFraction < 1.0)
      return true;

   return false;
}

bool CBot::CheckWallOnRight (void)
{
   Vector vSource, vRight;
   TraceResult tr;

   MakeVectors (pev->angles);

   // do a trace to the right...
   vSource = pev->origin;
   vRight = vSource + g_pGlobals->v_right * 40;  // 40 units to the right

   TraceLine (vSource, vRight, true, GetEntity (), &tr);

   // check if the trace hit something...
   if (tr.flFraction < 1.0)
      return true;

   return false;
}

bool CBot::IsDeadlyDrop (Vector vTargetPos)
{
   // Returns if given location would hurt Bot with falling damage

   Vector vBot = pev->origin;
   TraceResult tr;

   Vector vMove;
   vMove.y = VecToYaw (vTargetPos - vBot);
   vMove.x = 0; // reset pitch to 0 (level horizontally)
   vMove.z = 0; // reset roll to 0 (straight up and down)

   MakeVectors (vMove);
   Vector vDirection = (vTargetPos - vBot).Normalize ();  // 1 unit long
   Vector vCheck = vBot;
   Vector vDown = vBot;

   vDown.z = vDown.z - 1000.0;  // straight down 1000 units

   TraceHull (vCheck, vDown, true, head_hull, GetEntity (), &tr);

   if (tr.flFraction > 0.036) // We're not on ground anymore?
      tr.flFraction = 0.036;

   float fHeight;
   float fLastHeight = tr.flFraction * 1000.0;  // height from ground

   float fDistance = (vTargetPos - vCheck).Length ();  // distance from goal

   while (fDistance > 16.0)
   {
      vCheck = vCheck + vDirection * 16.0; // move 10 units closer to the goal...

      vDown = vCheck;
      vDown.z = vDown.z - 1000.0;  // straight down 1000 units

      TraceHull (vCheck, vDown, true, head_hull, GetEntity (), &tr);

      if (tr.fStartSolid) // Wall blocking?
         return false;

      fHeight = tr.flFraction * 1000.0;  // height from ground

      if (fLastHeight < fHeight - 100) // Drops more than 100 Units?
         return true;

      fLastHeight = fHeight;
      fDistance = (vTargetPos - vCheck).Length ();  // distance from goal
   }
   return false;
}

void CBot::ChangePitch (float fSpeed)
{
   // Changepitch - turns a bot towards its ideal_pitch

   float fIdeal = AngleNormalize (pev->idealpitch);

   if (g_rgpcvBotCVar[CVAR_TURNMETHOD]->GetInt () == 1)
   {
      // turn to the ideal angle immediately
      pev->v_angle.x = fIdeal;
      pev->angles.x = -fIdeal / 3;

      return;
   }

   float fCurrent = AngleNormalize (pev->v_angle.x);

   // turn from the current v_angle pitch to the idealpitch by selecting
   // the quickest way to turn to face that direction

   // find the difference in the fCurrent and ideal angle
   float fMove = AngleNormalize (fIdeal - fCurrent);

   if (fMove > 0)
   {
      if (fMove > fSpeed)
         fMove = fSpeed;
   }
   else
   {
      if (fMove < -fSpeed)
         fMove = -fSpeed;
   }

   pev->v_angle.x = AngleNormalize (fCurrent + fMove);

   if (pev->v_angle.x > 89.9)
      pev->v_angle.x = 89.9;

   if (pev->v_angle.x < -89.9)
      pev->v_angle.x = -89.9;

   pev->angles.x = -pev->v_angle.x / 3;
}

void CBot::ChangeYaw (float fSpeed)
{
   // Changeyaw - turns a bot towards its ideal_yaw

   float fIdeal = AngleNormalize (pev->ideal_yaw);

   if (g_rgpcvBotCVar[CVAR_TURNMETHOD]->GetInt () == 1)
   {
      // turn to the ideal angle immediately
      pev->angles.y = (pev->v_angle.y = fIdeal);
      return;
   }

   float fCurrent = AngleNormalize (pev->v_angle.y);

   // turn from the current v_angle yaw to the ideal_yaw by selecting
   // the quickest way to turn to face that direction

   // find the difference in the fCurrent and ideal angle
   float fMove = AngleNormalize (fIdeal - fCurrent);

   if (fMove > 0)
   {
      if (fMove > fSpeed)
         fMove = fSpeed;
   }
   else
   {
      if (fMove < -fSpeed)
         fMove = -fSpeed;
   }
   pev->v_angle.y = AngleNormalize (fCurrent + fMove);
   pev->angles.y = pev->v_angle.y;
}

int CBot::GetAimingWaypoint (void)
{
   // Find a good WP to look at when camping

   int iCount = 0, rgiIndex[3];
   float rgfDistance[3];
   uint16 rguVisibility[3];

   int iCurrWP = g_pWaypoint->FindNearest (pev->origin);

   for (int i = 0; i < g_iNumWaypoints; i++)
   {
      if ((iCurrWP == i) || !g_pWaypoint->IsVisible (iCurrWP, i))
         continue;

      if (iCount < 3)
      {
         rgiIndex[iCount] = i;

         rgfDistance[iCount] = LengthSquared (pev->origin - g_pWaypoint->GetPath (i)->vOrigin);
         rguVisibility[iCount] = g_pWaypoint->GetPath (i)->kVis.uCrouch + g_pWaypoint->GetPath (i)->kVis.uStand;
         
         iCount++;
      }
      else
      {
         float fDistance = LengthSquared (pev->origin - g_pWaypoint->GetPath (i)->vOrigin);
         uint16 uVisibility  = g_pWaypoint->GetPath (i)->kVis.uCrouch + g_pWaypoint->GetPath (i)->kVis.uStand;

         for (int j = 0; j < 3; j++)
         {
            if ((uVisibility >= rguVisibility[j]) && (fDistance > rgfDistance[j]))
            {
               rgiIndex[j] = i;

               rgfDistance[j] = fDistance;
               rguVisibility[j] = uVisibility;
               break;
            }
         }
      }
   }
   iCount--;

   if (iCount >= 0)
      return rgiIndex[RandomLong (0, iCount)];

   return RandomLong (0, g_iNumWaypoints - 1);
}

void CBot::HandleLadder (void)
{
   pev->button |= IN_FORWARD;

   // get speed multiply factor by dividing target fps by real fps
   float fSpeed = 480 * g_fTimeFrameInterval;

   if (m_iLadderDir == LADDER_DOWN)
   {
      m_vMoveAngles.x = 60; // move down

      float fDelta = AngleDiff (pev->angles.x, -18);

      if (fabsf (fDelta) < fSpeed)
         pev->angles.x = -18;
      else
      {
         if (fDelta > 0)
            pev->angles.x += fSpeed;
         else
            pev->angles.x -= fSpeed;
      }
   }
   else if (m_iLadderDir == LADDER_UP)
   {
      m_vMoveAngles.x = -60; // move up

      float fDelta = AngleDiff (pev->angles.x, 18);

      if (fabsf (fDelta) < fSpeed)
         pev->angles.x = 18;
      else
      {
         if (fDelta > 0)
            pev->angles.x += fSpeed;
         else
            pev->angles.x -= fSpeed;
      }
   }
   ClampAngles (pev->angles);
}

void CBot::FacePosition (void)
{
   // adjust all body and view angles to face an absolute vector
   Vector vDirection = VecToAngles (m_vLookAt - GetGunPosition ());
   vDirection = vDirection + pev->punchangle * m_iSkill / 100.0;
   vDirection.x *= -1.0; // invert for engine

   Vector vDeviation = ClampAngles (vDirection - pev->v_angle);
   ClampAngles (vDirection);

   int iForcedTurnMethod = g_rgpcvBotCVar[CVAR_TURNMETHOD]->GetInt ();

   if ((iForcedTurnMethod < 1) || (iForcedTurnMethod > 3))
      iForcedTurnMethod = 3;

   if (iForcedTurnMethod == 1)
      pev->v_angle = vDirection;
   else if (iForcedTurnMethod == 2)
   {
      float fTurnSkill = static_cast <float> (0.3 * m_iSkill / 100);
      float fAimSpeed = 0.0;

      // if bot is aiming at something, aim fast, else take our time...
      if (m_iAimFlags >= AIM_LASTENEMY)
         fAimSpeed = 0.7 + fTurnSkill; // fast aim
      else if ((m_iAimFlags < AIM_PREDICTPATH) && (m_iAimFlags & AIM_CAMP))
         fAimSpeed = 0.1 + fTurnSkill / 4; // very slow aim if camping
      else
         fAimSpeed = 0.2 + fTurnSkill / 2; // slow aim 

      // calculate the aim momentum
      float fMomentum = expf (logf (fAimSpeed * 0.5) * m_iMsecVal / 80);

      // thanks Tobias 'killaruna' Heimann and Johannes '@$3.1415rin' Lampel for this one
      pev->yaw_speed = (pev->yaw_speed * fMomentum + fAimSpeed * vDeviation.y * (1.0 - fMomentum)) * m_iMsecVal / 70;
      pev->pitch_speed = (pev->pitch_speed * fMomentum + fAimSpeed * vDeviation.x * (1.0 - fMomentum)) * m_iMsecVal / 70;

      // influence of y movement on x axis and vice versa (less influence than x on y since it's
      // easier and more natural for the bot to "move its mouse" horizontally than vertically)
      pev->pitch_speed += pev->yaw_speed / (1.5 * (1 + fTurnSkill));
      pev->yaw_speed += pev->pitch_speed / (1 + fTurnSkill); 

      if (fabsf (pev->pitch_speed) >= fabsf (vDeviation.x) && (pev->pitch_speed * vDeviation.x >= 0))
         pev->pitch_speed = vDeviation.x;

      if (fabsf (pev->yaw_speed) >= fabsf (vDeviation.y) && (pev->yaw_speed * vDeviation.y >= 0))
         pev->yaw_speed = vDeviation.y; 

      // update the view angles
      pev->v_angle.y += pev->yaw_speed;
      pev->v_angle.x += pev->pitch_speed;
   }
   else if (iForcedTurnMethod == 3)
   {
      Vector vSpringStiffness (g_rgpcvBotCVar[CVAR_SPRING_STIFF_X]->GetFloat (), g_rgpcvBotCVar[CVAR_SPRING_STIFF_Y]->GetFloat (), 0);
      Vector vDamperCoefficient (g_rgpcvBotCVar[CVAR_DAMPER_COEFF_X]->GetFloat (), g_rgpcvBotCVar[CVAR_DAMPER_COEFF_Y]->GetFloat (), 0);
      Vector vInfluence (g_rgpcvBotCVar[CVAR_INFL_X_ON_Y]->GetFloat (), g_rgpcvBotCVar[CVAR_INFL_Y_ON_X]->GetFloat (), 0);
      Vector vRandomization (g_rgpcvBotCVar[CVAR_DEVIATION_X]->GetFloat (), g_rgpcvBotCVar[CVAR_DEVIATION_Y]->GetFloat (), 0);

      Vector vStiffness = NULLVEC;
      Vector vRandomize = NULLVEC;

      m_vIdealAngles = ClampAngles (vDirection.IgnoreZComponent ());
      m_vTargetAngularSpeed = ClampAngles (m_vTargetAngularSpeed);

      if (m_iAimFlags & (AIM_ENEMY | AIM_ENTITY | AIM_GRENADE | AIM_LASTENEMY) || (CurrentTask ()->iTask == TASK_SHOOTBREAKABLE))
      {
         m_fPlayerTargetTime = WorldTime ();
         m_vRandomizedIdealAngles = m_vIdealAngles;

         if (IsValidPlayer (m_pentEnemy))
         {
            m_vTargetAngularSpeed = (VecToAngles (m_pentEnemy->v.origin - pev->origin + m_pentEnemy->v.velocity - pev->velocity) - VecToAngles (m_pentEnemy->v.origin - pev->origin)) * g_rgpcvBotCVar[CVAR_ANTICIP_RATIO]->GetFloat () * static_cast <float> (m_iSkill / 100);

            if (pev->fov < 90)
               m_vTargetAngularSpeed = ClampAngles (m_vTargetAngularSpeed * 2.0);
         }
         else
            m_vTargetAngularSpeed = NULLVEC;

         vStiffness = vSpringStiffness;
      }
      else
      {
         // is it time for bot to randomize the aim direction again (more often where moving) ?
         if ((m_fRandomizeAnglesTime < WorldTime ()) && (((pev->velocity.Length () > 1.0) && (m_vAngularDeviation.Length () < 5.0)) || (m_vAngularDeviation.Length () < 1.0)))
         {
            // is the bot standing still ?
            if (pev->velocity.Length () < 1.0)
               vRandomize = vRandomization * 0.2; // randomize less
            else
               vRandomize = vRandomization;

            // randomize targeted location a bit (slightly towards the ground)
            m_vRandomizedIdealAngles = m_vIdealAngles + Vector (RandomFloat (-vRandomize.x * 0.5, vRandomize.x * 1.5), RandomFloat (-vRandomize.y, vRandomize.y), 0);

            // set next time to do this
            m_fRandomizeAnglesTime = WorldTime () + RandomFloat (0.4, g_rgpcvBotCVar[CVAR_OFFSET_DELAY]->GetFloat ());
         }
         float fStiffnessMultiplier = g_rgpcvBotCVar[CVAR_SLOWDOWN_RATIO]->GetFloat ();

         // take in account whether the bot was targeting someone in the last N seconds
         if (WorldTime () - (m_fPlayerTargetTime + g_rgpcvBotCVar[CVAR_OFFSET_DELAY]->GetFloat ()) < g_rgpcvBotCVar[CVAR_SLOWDOWN_RATIO]->GetFloat () * 10.0)
         {   
            fStiffnessMultiplier = 1.0 - (WorldTime () - m_fTimeLastFired) * 0.1;

            // don't allow that stiffness multiplier less than zero
            if (fStiffnessMultiplier < 0.0)
               fStiffnessMultiplier = 0.5;
         }
         
         // also take in account the remaining deviation (slow down the aiming in the last 10�)
         if (m_vAngularDeviation.Length () < 10.0)
            fStiffnessMultiplier *= m_vAngularDeviation.Length () * 0.1;

         // slow down even more if we are not moving
         if (pev->velocity.Length () < 1.0)
            fStiffnessMultiplier *= 0.5;

         // but don't allow getting below a certain value
         if (fStiffnessMultiplier < 0.35)
            fStiffnessMultiplier = 0.35;

         vStiffness = vSpringStiffness * fStiffnessMultiplier; // increasingly slow aim

         // no target means no angular speed to take in account
         m_vTargetAngularSpeed = NULLVEC;
      }
      // compute randomized angle deviation this time
      m_vAngularDeviation = ClampAngles (m_vRandomizedIdealAngles - pev->v_angle);

      // spring/damper model aiming
      m_vAimSpeed.x = (vStiffness.x * m_vAngularDeviation.x) - (vDamperCoefficient.x * (m_vAimSpeed.x - m_vTargetAngularSpeed.x));
      m_vAimSpeed.y = (vStiffness.y * m_vAngularDeviation.y) - (vDamperCoefficient.y * (m_vAimSpeed.y - m_vTargetAngularSpeed.y));

      // influence of y movement on x axis and vice versa (less influence than x on y since it's
      // easier and more natural for the bot to "move its mouse" horizontally than vertically)
      m_vAimSpeed.x += m_vAimSpeed.y * vInfluence.y;
      m_vAimSpeed.y += m_vAimSpeed.x * vInfluence.x;

      // move the aim cursor
      pev->v_angle = ClampAngles (pev->v_angle + g_fTimeFrameInterval * Vector (m_vAimSpeed.x, m_vAimSpeed.y, 0));
   }

   // set the body angles to point the gun correctly
   pev->angles.x = -pev->v_angle.x * (1.0 / 3.0);
   pev->angles.y = pev->v_angle.y;

   ClampAngles (pev->v_angle);
   ClampAngles (pev->angles);

   pev->angles.z = pev->v_angle.z = 0.0; // ignore Z component
}

void CBot::SetStrafeSpeed (Vector vMoveDir, float fStrafeSpeed)
{
   MakeVectors (pev->angles);

   Vector2D v2LOS = (vMoveDir - pev->origin).Make2D ().Normalize ();
   float fDot = DotProduct (v2LOS, g_pGlobals->v_forward.Make2D ());

   if ((fDot > 0) && !CheckWallOnRight ())
      m_fSideMoveSpeed = fStrafeSpeed;
   else if (!CheckWallOnLeft ())
      m_fSideMoveSpeed = -fStrafeSpeed;
}

int CBot::FindLoosedBomb (void)
{
   // this function tries to find droped c4 on the defuse scenario map  and returns nearest to it waypoint

   if ((GetTeam (GetEntity ()) != TEAM_TERRORIST) || !(g_iMapType & MAP_DE))
      return -1; // don't search for bomb if the player is CT, or it's not defusing bomb

   edict_t *pentBomb = NULL; // temporaly pointer to bomb

   // search the bomb on the map
   while (!FNullEnt (pentBomb = FIND_ENTITY_BY_CLASSNAME (pentBomb, "weaponbox")))
   {
      if (strcmp (STRING (pentBomb->v.model) + 9, "backpack.mdl") == 0)
      {
         int iNearestIndex = g_pWaypoint->FindNearest (GetEntityOrigin (pentBomb));

         if ((iNearestIndex >= 0) && (iNearestIndex < g_iNumWaypoints))
            return iNearestIndex;

         break;
      }
   }
   return -1;
}

bool CBot::IsWaypointUsed (int iIndex)
{
   if ((iIndex < 0) || (iIndex >= g_iNumWaypoints))
      return true;

   // first check if current waypoint of one of the bots is index waypoint
   for (int i = 0; i < MaxClients (); i++)
   {
      CBot *pBot = g_pBotManager->GetBot (i);

      // check if this waypoint is already used
      if ((pBot != NULL) && (pBot != this) && IsAlive (pBot->GetEntity ()) && ((pBot->m_iCurrWptIndex == iIndex) || (pBot->CurrentTask ()->iData == iIndex)))
         return true;
   }

   // secondary check waypoint radius for any player
   edict_t *pent = NULL;

   // search player entities in waypoint radius
   while (!FNullEnt (pent = FIND_ENTITY_IN_SPHERE (pent, g_pWaypoint->GetPath (iIndex)->vOrigin, 60)))
   {
      if (IsValidBot (pent) && IsAlive (pent))
         return true;
   }
   return false;
}
