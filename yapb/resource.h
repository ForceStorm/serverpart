//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#ifndef RESOURCE_INCLUDED
#define RESOURCE_INCLUDED

// general product information
#define PRODUCT_NAME "YaPB"
#define PRODUCT_VERSION "2.00"
#define PRODUCT_AUTHOR "YaPB Dev Team"
#define PRODUCT_URL "http://yapb.bots-united.com"
#define PRODUCT_EMAIL "no@email.yet"
#define PRODUCT_LOGTAG "YB"
#define PRODUCT_DESCRIPTION PRODUCT_NAME " v" PRODUCT_VERSION " - The Counter-Strike Bot"
#define PRODUCT_COPYRIGHT "Copyright � 2003-2006, by " PRODUCT_AUTHOR
#define PRODUCT_LEGAL "Half-Life, Counter-Strike, Counter-Strike: Condition Zero, Steam, Valve is a trademark of Valve Corporation"
#define PRODUCT_ORIGINAL_NAME "yapb.dll"
#define PRODUCT_INTERNAL_NAME "YaPB"
#define PRODUCT_VERSION_DWORD 2,00,0 // major version, minor version, WIP (or Update) version, BUILD number (generated with RES file)
#define PRODUCT_SUPPORT_VERSION "1.1 - CZ"
#define PRODUCT_DATE __DATE__

// product optimization type (we're not using crt builds anymore)
#ifndef PRODUCT_OPT_TYPE
#if defined (_DEBUG)
#   if defined (_AFXDLL)
#      define PRODUCT_OPT_TYPE "Debug Build (CRT)"
#   else
#      define PRODUCT_OPT_TYPE "Debug Build"
#   endif
#elif defined (NDEBUG)
#   if defined (_AFXDLL)
#      define PRODUCT_OPT_TYPE "Optimized Build (CRT)"
#   else
#      define PRODUCT_OPT_TYPE "Optimized Build"
#   endif
#else
#   define PRODUCT_OPT_TYPE "Default Release"
#endif
#endif

#endif

