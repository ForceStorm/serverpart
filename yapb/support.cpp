//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#include "yapb.h"

void TraceLine (const Vector &vStart, const Vector &vEnd, bool bIgnoreMonsters, bool bIgnoreGlass, edict_t *pentIgnore, TraceResult *ptr) 
{
   // this function traces a line dot by dot, starting from vecStart in the direction of vecEnd,
   // ignoring or not monsters (depending on the value of IGNORE_MONSTERS, true or false), and stops 
   // at the first obstacle encountered, returning the results of the trace in the TraceResult structure 
   // ptr. Such results are (amongst others) the distance traced, the hit surface, the hit plane 
   // vector normal, etc. See the TraceResult structure for details. This function allows to specify 
   // whether the trace starts "inside" an entity's polygonal model, and if so, to specify that entity 
   // in pentIgnore in order to ignore it as a possible obstacle.
   // this is an overloaded prototype to add IGNORE_GLASS in the same way as IGNORE_MONSTERS work.

   (*g_engfuncs.pfnTraceLine) (vStart, vEnd, (bIgnoreMonsters ? TRUE : FALSE) | (bIgnoreGlass ? 0x100 : 0), pentIgnore, ptr); 
}

void TraceLine (const Vector &vStart, const Vector &vEnd, bool bIgnoreMonsters, edict_t *pentIgnore, TraceResult *ptr) 
{
   // this function traces a line dot by dot, starting from vecStart in the direction of vecEnd,
   // ignoring or not monsters (depending on the value of IGNORE_MONSTERS, true or false), and stops 
   // at the first obstacle encountered, returning the results of the trace in the TraceResult structure 
   // ptr. Such results are (amongst others) the distance traced, the hit surface, the hit plane 
   // vector normal, etc. See the TraceResult structure for details. This function allows to specify 
   // whether the trace starts "inside" an entity's polygonal model, and if so, to specify that entity 
   // in pentIgnore in order to ignore it as a possible obstacle.

   (*g_engfuncs.pfnTraceLine) (vStart, vEnd, bIgnoreMonsters ? TRUE : FALSE, pentIgnore, ptr); 
}

void TraceHull (const Vector &vStart, const Vector &vEnd, bool bIgnoreMonsters, int hullNumber, edict_t *pentIgnore, TraceResult *ptr) 
{
   // this function traces a hull dot by dot, starting from vecStart in the direction of vecEnd,
   // ignoring or not monsters (depending on the value of IGNORE_MONSTERS, true or
   // false), and stops at the first obstacle encountered, returning the results
   // of the trace in the TraceResult structure ptr, just like TraceLine. Hulls that can be traced
   // (by parameter hull_type) are point_hull (a line), head_hull (size of a crouching player),
   // human_hull (a normal body size) and large_hull (for monsters?). Not all the hulls in the
   // game can be traced here, this function is just useful to give a relative idea of spatial
   // reachability (i.e. can a hostage pass through that tiny hole ?) Also like TraceLine, this
   // function allows to specify whether the trace starts "inside" an entity's polygonal model,
   // and if so, to specify that entity in pentIgnore in order to ignore it as an obstacle.

   (*g_engfuncs.pfnTraceHull) (vStart, vEnd, bIgnoreMonsters ? TRUE : FALSE, hullNumber, pentIgnore, ptr); 
}

uint16 FixedUnsigned16 (float fValue, float fScale)
{
   int iOutput = (static_cast <int> (fValue * fScale));

   if (iOutput < 0)
      iOutput = 0;

   if (iOutput > 0xffff)
      iOutput = 0xffff;

   return (static_cast <uint16> (iOutput));
}

short FixedSigned16 (float fValue, float fScale)
{
   int iOutput = (static_cast <int> (fValue * fScale));

   if (iOutput > 32767)
      iOutput = 32767;

   if (iOutput < -32768)
      iOutput = -32768;

   return (static_cast <short> (iOutput));
}

char *VarArgs (char *fmt, ...)
{
   va_list ap;
   static char pszBuffer[3072];

   va_start (ap, fmt);
   vsprintf (pszBuffer, fmt, ap);
   va_end (ap);

   return (&pszBuffer[0]);
}

bool IsAlive (edict_t *pent)
{
   if (FNullEnt (pent))
      return false; // reliability check

   return ((pent->v.deadflag == DEAD_NO) && (pent->v.health > 0) && (pent->v.movetype != MOVETYPE_NOCLIP));
}

float GetShootingConeDeviation (edict_t *pent, Vector *pvPosition)
{
   Vector vDir = (*pvPosition - (pent->v.origin + pent->v.view_ofs)).Normalize ();
   MakeVectors (pent->v.v_angle);

   // he's facing it, he meant it
   return DotProduct (g_pGlobals->v_forward, vDir);
}

bool IsInViewCone (Vector vOrigin, edict_t *pent)
{
   MakeVectors (pent->v.v_angle);

   if (DotProduct ((vOrigin - (pent->v.origin + pent->v.view_ofs)).Make2D ().Normalize (), g_pGlobals->v_forward.Make2D ()) >= cosf (((pent->v.fov > 0 ? pent->v.fov : 90.0) / 2) * PI / 180.0))
      return true;

   return false;
}

bool IsVisible (const Vector &vOrigin, edict_t *pentEdict)
{
   if (FNullEnt (pentEdict))
      return false;

   TraceResult tr;
   TraceLine (pentEdict->v.origin + pentEdict->v.view_ofs, vOrigin, true, true, pentEdict, &tr);

   if (tr.flFraction != 1.0)
      return false; // line of sight is not established
   
   return true; // line of sight is valid.
}

Vector GetEntityOrigin (edict_t *pent)
{
   // this expanded function returns the vector origin of a bounded entity, assuming that any
   // entity that has a bounding box has its center at the center of the bounding box itself.

   if (FNullEnt (pent))
      return NULLVEC;

   if (pent->v.origin == NULLVEC)
      return pent->v.absmin + (pent->v.size * 0.5);

   return pent->v.origin;
}

void ShowMenu (edict_t *pentEdict, tMenuText *pMenu)
{
   if (!IsValidPlayer (pentEdict))
      return;

   int iClientIndex = ENTINDEX (pentEdict) - 1;

   if (pMenu != NULL)
   {
      string sText = string (pMenu->szMenuText);
      sText.Replace ('\v', '\n');

      char *pszText = g_pLocalize->T (sText.GetString ());
      sText = string (pszText);

      // make menu looks best
      for (int i = 0; i <= 9; i++)
         sText.Replace (VarArgs ("%d. ", i), VarArgs ("\\r%d. \\w", i));

      pszText = sText.GetBuffer (1024);

      while (strlen (pszText) >= 64)
      {
         MESSAGE_BEGIN (MSG_ONE_UNRELIABLE, GetUserMsgId ("ShowMenu"), NULL, pentEdict);
            WRITE_SHORT (pMenu->iValidSlots);
            WRITE_CHAR (-1);
            WRITE_BYTE (1);

         for (int i = 0; i <= 63; i++)
            WRITE_CHAR (pszText[i]);

         MESSAGE_END ();

         pszText += 64;
      }

      MESSAGE_BEGIN (MSG_ONE_UNRELIABLE, GetUserMsgId ("ShowMenu"), NULL, pentEdict);
         WRITE_SHORT (pMenu->iValidSlots);
         WRITE_CHAR (-1);
         WRITE_BYTE (0);
         WRITE_STRING (pszText);
      MESSAGE_END();

      g_rgkClients[iClientIndex].pMenu = pMenu;
   }
   else
   {
      MESSAGE_BEGIN (MSG_ONE_UNRELIABLE, GetUserMsgId ("ShowMenu"), NULL, pentEdict);
         WRITE_SHORT (0);
         WRITE_CHAR (0);
         WRITE_BYTE (0);
         WRITE_STRING ("");
      MESSAGE_END();

     g_rgkClients[iClientIndex].pMenu = NULL;
   }
   CLIENT_COMMAND (pentEdict, "speak \"player/geiger1\"\n"); // Stops others from hearing menu sounds.. 
}

void DecalTrace (entvars_t *pev, TraceResult *pTrace, char *szDecalName)
{
   int iEntIndex = -1, iMessage = TE_DECAL, iDecalIndex = -1;

   iDecalIndex = (*g_engfuncs.pfnDecalIndex) (szDecalName);

   if (iDecalIndex < 0)
      iDecalIndex = (*g_engfuncs.pfnDecalIndex) ("{lambda06");

   if (pTrace->flFraction == 1.0)
      return;

   if (!FNullEnt (pTrace->pHit))
   {
      if ((pTrace->pHit->v.solid == SOLID_BSP) || (pTrace->pHit->v.movetype == MOVETYPE_PUSHSTEP))
         iEntIndex = ENTINDEX (pTrace->pHit);
      else
         return;
   }
   else
      iEntIndex = 0;

   if (iEntIndex != 0)
   {
      if (iDecalIndex > 255)
      {
         iMessage = TE_DECALHIGH;
         iDecalIndex -= 256;
      }
   }
   else
   {
      iMessage = TE_WORLDDECAL;

      if (iDecalIndex > 255)
      {
         iMessage = TE_WORLDDECALHIGH;
         iDecalIndex -= 256;
      }
   }

   if (IsNullString (strstr (szDecalName, "{")))
   {
      MESSAGE_BEGIN (MSG_BROADCAST, SVC_TEMPENTITY);
         WRITE_BYTE (TE_PLAYERDECAL);
         WRITE_BYTE (ENTINDEX (ENT (pev)));
         WRITE_COORD (pTrace->vecEndPos.x);
         WRITE_COORD (pTrace->vecEndPos.y);
         WRITE_COORD (pTrace->vecEndPos.z);
         WRITE_SHORT (static_cast <short> (ENTINDEX (pTrace->pHit)));
         WRITE_BYTE (iDecalIndex);
      MESSAGE_END (); 
   }
   else
   {
      MESSAGE_BEGIN (MSG_BROADCAST, SVC_TEMPENTITY);
         WRITE_BYTE (iMessage);
         WRITE_COORD (pTrace->vecEndPos.x);
         WRITE_COORD (pTrace->vecEndPos.y);
         WRITE_COORD (pTrace->vecEndPos.z);
         WRITE_BYTE (iDecalIndex);

      if (iEntIndex)
         WRITE_SHORT (iEntIndex);
      MESSAGE_END();
   }
}

void FreeLibraryMemory (void)
{
   // this function free's all allocated memory

   g_pBotManager->Free ();
   g_pWaypoint->Init (); // frees waypoint data

   if (g_pBotManager != NULL)
      delete g_pBotManager;
   g_pBotManager = NULL;

   if (g_pNetMsg != NULL)
      delete g_pNetMsg;
   g_pNetMsg = NULL;

   if (g_pWaypoint != NULL)
      delete g_pWaypoint;
   g_pWaypoint = NULL;

   if (g_pLocalize != NULL)
      delete g_pLocalize;
   g_pLocalize = NULL;
}

void FakeClientCommand (edict_t *pentFakeClient, const char *fmt, ...)
{
   // the purpose of this function is to provide fakeclients (bots) with the same client
   // command-scripting advantages (putting multiple commands in one line between semicolons)
   // as real players. It is an improved version of botman's FakeClientCommand, in which you
   // supply directly the whole string as if you were typing it in the bot's "console". It
   // is supposed to work exactly like the pfnClientCommand (server-sided client command).

   va_list ap;
   static char command[256];
   int iLength, iStart, iStop, i, index, iStringIndex = 0;

   if (FNullEnt (pentFakeClient))
      return; // reliability check

   // concatenate all the arguments in one string
   va_start (ap, fmt);
   _vsnprintf (command, sizeof (command), fmt, ap);
   va_end (ap);

   if (IsNullString (command))
      return; // if nothing in the command buffer, return

   g_bIsFakeCommand = true; // set the "fakeclient command" flag
   iLength = strlen (command); // get the total iLength of the command string

   // process all individual commands (separated by a semicolon) one each a time
   while (iStringIndex < iLength)
   {
      iStart = iStringIndex; // save szField start position (first character)
      while (iStringIndex < iLength && command[iStringIndex] != ';')
         iStringIndex++; // reach end of szField
      if (command[iStringIndex - 1] == '\n')
         iStop = iStringIndex - 2; // discard any trailing '\n' if needed
      else
         iStop = iStringIndex - 1; // save szField stop position (last character before semicolon or end)
      for (i = iStart; i <= iStop; i++)
         g_szFakeArgVector[i - iStart] = command[i]; // store the szField value in the g_szFakeArgVector global string
      g_szFakeArgVector[i - iStart] = 0; // terminate the string
      iStringIndex++; // move the overall string index one step further to bypass the semicolon

      index = 0;
      g_iFakeArgumentCount = 0; // let's now parse that command and count the different arguments

      // count the number of arguments
      while (index < i - iStart)
      {
         while (index < i - iStart && g_szFakeArgVector[index] == ' ')
            index++; // ignore spaces

         // is this szField a group of words between quotes or a single word ?
         if (g_szFakeArgVector[index] == '"')
         {
            index++; // move one step further to bypass the quote
            while (index < i - iStart && g_szFakeArgVector[index] != '"')
               index++; // reach end of szField
            index++; // move one step further to bypass the quote
         }
         else
            while (index < i - iStart && g_szFakeArgVector[index] != ' ')
               index++; // this is a single word, so reach the end of szField

         g_iFakeArgumentCount++; // we have processed one argument more
      }

      // tell now the MOD DLL to execute this ClientCommand...
      MDLL_ClientCommand (pentFakeClient);
   }

   g_szFakeArgVector[0] = 0; // when it's done, reset the g_szFakeArgVector szField
   g_bIsFakeCommand = false; // reset the "fakeclient command" flag
   g_iFakeArgumentCount = 0; // and the argument count
}

const char *GetField (const char *string, int iFieldId, bool bEndLine)
{
   // This function gets and returns a particuliar szField in a string where several szFields are
   // concatenated. Fields can be words, or groups of words between quotes ; separators may be
   // white space or tabs. A purpose of this function is to provide bots with the same Cmd_Argv
   // convenience the engine provides to real clients. This way the handling of real client
   // commands and bot client commands is exactly the same, just have a look in engine.cpp
   // for the hooking of pfnCmd_Argc, pfnCmd_Args and pfnCmd_Argv, which redirects the call
   // either to the actual engine functions (when the caller is a real client), either on
   // our function here, which does the same thing, when the caller is a bot.

   static char szField[256];

   //reset the string
   memset (szField, 0, sizeof (szField));

   int iLength, i, iIndex = 0, iFieldCount = 0, iStart, iStop;

   szField[0] = 0; // reset szField
   iLength = strlen (string); // get iLength of string

   // while we have not reached end of line
   while ((iIndex < iLength) && (iFieldCount <= iFieldId))
   {
      while ((iIndex < iLength) && ((string[iIndex] == ' ') || (string[iIndex] == '\t')))
         iIndex++; // ignore spaces or tabs

      // is this szField multi-word between quotes or single word ?
      if (string[iIndex] == '"')
      {
         iIndex++; // move one step further to bypass the quote
         iStart = iIndex; // save szField start position

         while ((iIndex < iLength) && (string[iIndex] != '"'))
            iIndex++; // reach end of field

         iStop = iIndex - 1; // save szField stop position
         iIndex++; // move one step further to bypass the quote
      }
      else
      {
         iStart = iIndex; // save szField start position
         
         while ((iIndex < iLength) && ((string[iIndex] != ' ') && (string[iIndex] != '\t')))
            iIndex++; // reach end of field

         iStop = iIndex - 1; // save szField stop position
      }

      // is this szField we just processed the wanted one ?
      if (iFieldCount == iFieldId)
      {
         for (i = iStart; i <= iStop; i++)
            szField[i - iStart] = string[i]; // store the szField value in a string
         szField[i - iStart] = 0; // terminate the string
         break; // and stop parsing
      }
      iFieldCount++; // we have parsed one szField more
   }

   if (bEndLine == true)
      szField[strlen (szField) - 1] = 0;
   StrTrim (szField);

   return (&szField[0]); // returns the wanted szField
}

void StrTrim (char *szString)
{
   char *szPtr = szString;
   
   int iLength = 0, iFlag = 0, iIncrement = 0;
   auto int i = 0;

   while (*szPtr++)
      iLength++;

   for (i = iLength - 1; i >= 0; i--)
   {
#if defined (PLATFORM_WIN32)
      if (!iswspace (szString[i]))
#else
      if (!isspace (szString[i]))
#endif
         break;
      else
      {
         szString[i] = 0;
         iLength--;
      }
   }

   for (i = 0; i < iLength; i++)
   {
#if defined (PLATFORM_WIN32)
      if (iswspace (szString[i]) && !iFlag) // win32 crash fix
#else
      if (isspace (szString[i]) && !iFlag)
#endif
      {
         iIncrement++;

         if (iIncrement + i < iLength)
            szString[i] = szString[iIncrement + i];
      }
      else
      {
         if (!iFlag)
            iFlag = 1;

         if (iIncrement)
            szString[i] = szString[iIncrement + i];
      }
   }
   szString[iLength] = 0;
}

const char *GetModName (void)
{
   static char szMod[256];

   GET_GAME_DIR (szMod); // ask the engine for the MOD directory path
   int iLength = strlen (szMod); // get the iLength of the returned string

   // format the returned string to get the last directory name
   int iStop = iLength - 1;
   while (((szMod[iStop] == '\\') || (szMod[iStop] == '/')) && (iStop > 0))
      iStop--; // shift back any trailing separator

   int iStart = iStop;
   while ((szMod[iStart] != '\\') && (szMod[iStart] != '/') && (iStart > 0))
      iStart--; // shift back to the start of the last subdirectory name

   if ((szMod[iStart] == '\\') || (szMod[iStart] == '/'))
      iStart++; // if we reached a separator, step over it

   // now copy the formatted string back onto itself character per character
   for (iLength = iStart; iLength <= iStop; iLength++)
      szMod[iLength - iStart] = szMod[iLength];
   szMod[iLength - iStart] = 0; // terminate the string

   return (&szMod[0]);
} 

// Create a directory tree
void CreatePath (char *szPath)
{
   char *szOfs;

   for (szOfs = szPath + 1 ; *szOfs ; szOfs++)
   {
      if (*szOfs == '/')
      {
         // create the directory
         *szOfs = 0;
#ifdef PLATFORM_WIN32
         mkdir (szPath);
#else
         mkdir (szPath, 0777);
#endif
         *szOfs = '/';
      }
   }
#ifdef PLATFORM_WIN32
   mkdir (szPath);
#else
   mkdir (szPath, 0777);
#endif
}

void DrawLine (edict_t *pentEdict, Vector start, Vector end, int width, int noise, int red, int green, int blue, int brightness, int speed, int life)
{
   // this function draws a line visible from the client side of the player whose player entity
   // is pointed to by pentEdict, from the vector location start to the vector location end,
   // which is supposed to last life tenths seconds, and having the color defined by RGB.

   if (!IsValidPlayer (pentEdict))
      return; // reliability check

   MESSAGE_BEGIN (MSG_ONE_UNRELIABLE, SVC_TEMPENTITY, NULL, pentEdict);
      WRITE_BYTE (TE_BEAMPOINTS);
      WRITE_COORD (start.x);
      WRITE_COORD (start.y);
      WRITE_COORD (start.z);
      WRITE_COORD (end.x);
      WRITE_COORD (end.y);
      WRITE_COORD (end.z);
      WRITE_SHORT (g_sModelIndexLaser);
      WRITE_BYTE (0); // framestart
      WRITE_BYTE (10); // framerate
      WRITE_BYTE (life); // life in 0.1's
      WRITE_BYTE (width); // width
      WRITE_BYTE (noise);  // noise

      WRITE_BYTE (red);   // r, g, b
      WRITE_BYTE (green);   // r, g, b
      WRITE_BYTE (blue);   // r, g, b

      WRITE_BYTE (brightness);   // brightness
      WRITE_BYTE (speed);    // speed
   MESSAGE_END ();
}

void DrawArrow (edict_t *pentEdict, Vector start, Vector end, int width, int noise, int red, int green, int blue, int brightness, int speed, int life)
{
   // this function draws a arrow visible from the client side of the player whose player entity
   // is pointed to by pentEdict, from the vector location start to the vector location end,
   // which is supposed to last life tenths seconds, and having the color defined by RGB.

   if (!IsValidPlayer (pentEdict))
      return; // reliability check

   MESSAGE_BEGIN (MSG_ONE_UNRELIABLE, SVC_TEMPENTITY, NULL, pentEdict);
      WRITE_BYTE (TE_BEAMPOINTS);
      WRITE_COORD (end.x);
      WRITE_COORD (end.y);
      WRITE_COORD (end.z);
      WRITE_COORD (start.x);
      WRITE_COORD (start.y);
      WRITE_COORD (start.z);
      WRITE_SHORT (g_sModelIndexArrow);
      WRITE_BYTE (0); // framestart
      WRITE_BYTE (10); // framerate
      WRITE_BYTE (life); // life in 0.1's
      WRITE_BYTE (width); // width
      WRITE_BYTE (noise);  // noise

      WRITE_BYTE (red);   // r, g, b
      WRITE_BYTE (green);   // r, g, b
      WRITE_BYTE (blue);   // r, g, b

      WRITE_BYTE (brightness);   // brightness
      WRITE_BYTE (speed);    // speed
   MESSAGE_END ();
}

void UpdateGlobalExperienceData (void)
{
   // this function called after each end of the round to update knowledge about most dangerous waypoints for each team.

   // no waypoints, no experience used or waypoints edited or being edited?
   if ((g_iNumWaypoints < 1) || g_bWaypointsChanged)
      return; // no action

   unsigned short uMaxDamage; // maximum damage
   unsigned short uActDamage; // actual damage

   int iBestIndex; // best index to store
   bool bRecalculateKills = false;

   // get the most dangerous waypoint for this position for terrorist team
   for (int i = 0; i < g_iNumWaypoints; i++)
   {
      uMaxDamage = 0;
      iBestIndex = -1;

      for (int j = 0; j < g_iNumWaypoints; j++)
      {
         if (i == j)
         {
            if ((g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam0Damage >= MAX_DAMAGE_VAL)
               bRecalculateKills = true;

            continue;
         }

         uActDamage = (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam0Damage;

         if (uActDamage > uMaxDamage)
         {
            uMaxDamage = uActDamage;
            iBestIndex = j;
         }
      }

      if (uMaxDamage > MAX_DAMAGE_VAL)
         bRecalculateKills = true;

      (g_pExperienceData + (i * g_iNumWaypoints) + i)->iTeam0DangerIndex = static_cast <short> (iBestIndex);
   }

   // get the most dangerous waypoint for this position for counter-terrorist team
   for (int i = 0; i < g_iNumWaypoints; i++)
   {
      uMaxDamage = 0;
      iBestIndex = -1;

      for (int j = 0; j < g_iNumWaypoints; j++)
      {
         if (i == j)
            continue;

         uActDamage = (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam1Damage;

         if (uActDamage > uMaxDamage)
         {
            uMaxDamage = uActDamage;
            iBestIndex = j;
         }
      }

      if (uMaxDamage > MAX_DAMAGE_VAL)
         bRecalculateKills = true;

     (g_pExperienceData + (i * g_iNumWaypoints) + i)->iTeam1DangerIndex = static_cast <short> (iBestIndex);
   }

   // adjust values if overflow is about to happen
   if (bRecalculateKills)
   {
      for (int i = 0; i < g_iNumWaypoints; i++)
      {
         for (int j = 0; j < g_iNumWaypoints; j++)
         {
            if (i == j)
               continue;
 
            int iClip = (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam0Damage;
            iClip -= static_cast <int> (MAX_DAMAGE_VAL * 0.5);

            if (iClip < 0)
               iClip = 0;

            (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam0Damage = static_cast <unsigned short> (iClip);


            iClip = (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam1Damage;
            iClip -= static_cast <int> (MAX_DAMAGE_VAL * 0.5);

            if (iClip < 0)
               iClip = 0;

            (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam1Damage = static_cast <unsigned short> (iClip);
         }
      }
   }
   g_cKillHistory++;

   if (g_cKillHistory == MAX_KILL_HIST)
   {
      for (int i = 0; i < g_iNumWaypoints; i++)
      {
         (g_pExperienceData + (i * g_iNumWaypoints) + i)->uTeam0Damage /= static_cast <unsigned short> (MaxClients () * 0.5);
         (g_pExperienceData + (i * g_iNumWaypoints) + i)->uTeam1Damage /= static_cast <unsigned short> (MaxClients () * 0.5);
      }
      g_cKillHistory = 1;
   }
}

void RoundInit (void)
{
   // this is called at the start of each round

   g_bRoundEnded = false;

   for (int i = 0; i < MaxClients (); i++)
   {
      if (g_pBotManager->GetBot (i))
         g_pBotManager->GetBot (i)->NewRound ();

      g_rgiRadioSelect[i] = 0;
   }

   g_bBombPlanted = false;
   g_bBombSayString = false;
   g_fTimeBombPlanted = 0.0;

   // clear waypoint indices of visited bomb spots
   for (int i = 0; i < MAX_NUMBOMB_SPOTS; i++)
      g_rgiBombSpotsVisited[i] = -1;

   g_iLastBombPoint = -1;
   g_fTimeNextBombUpdate = 0.0;

   g_rgbLeaderChoosen[TEAM_CT] = false;
   g_rgbLeaderChoosen[TEAM_TERRORIST] =  false;

   g_rgfLastRadioTime[0] = 0.0;
   g_rgfLastRadioTime[1] = 0.0;
   g_bBotsCanPause = false;

   UpdateGlobalExperienceData (); // update experience data on round start

   // calculate the round mid/end in world time
   g_fTimeRoundStart = WorldTime () + g_rgpcvBotCVar[CVAR_FREEZETIME]->GetFloat ();
   g_fTimeRoundMid = g_fTimeRoundStart + g_rgpcvBotCVar[CVAR_ROUNDTIME]->GetFloat () * 60 / 2;
   g_fTimeRoundEnd = g_fTimeRoundStart + g_rgpcvBotCVar[CVAR_ROUNDTIME]->GetFloat () * 60;
}

void CTBombPointClear (int iIndex)
{
   for (int i = 0; i < MAX_NUMBOMB_SPOTS; i++)
   {
      if (g_rgiBombSpotsVisited[i] == -1)
      {
         g_rgiBombSpotsVisited[i] = iIndex;
         return;
      }
      else if (g_rgiBombSpotsVisited[i] == iIndex)
         return;
   }
}

bool IsBombPointWasVisited (int iIndex)
{
   // this is little Helper routine to check if a Bomb Waypoint got visited

   for (int i = 0; i < MAX_NUMBOMB_SPOTS; i++)
   {
      if (g_rgiBombSpotsVisited[i] == -1)
         return false;
      else if (g_rgiBombSpotsVisited[i] == iIndex)
         return true;
   }
   return false;
}

bool IsWeaponShootingThroughWall (int iId)
{
   // returns if weapon can pierce through a wall

   int i = 0;

   while (g_rgkWeaponSelect[i].iId)
   {
      if (g_rgkWeaponSelect[i].iId == iId)
      {
         if (g_rgkWeaponSelect[i].bShootsThru)
            return true;

         return false;
      }
      i++;
   }
   return false;
}

int GetTeam (edict_t *pentEdict)
{
   return g_rgkClients[ENTINDEX (pentEdict) - 1].iTeam;
}

bool IsValidPlayer (edict_t *pent)
{
   if (FNullEnt (pent))
      return false;

   if ((pent->v.flags & (FL_CLIENT | FL_FAKECLIENT)) || (g_pBotManager->GetBot (pent) != NULL))
      return !IsNullString (STRING (pent->v.netname));

   return false;
}

bool IsValidBot (edict_t *pent)
{
   if (!IsValidPlayer (pent))
      return false;

   return (g_pBotManager->GetBot (pent) != NULL);
}

bool IsDedicatedServer (void)
{
   // return true if server is dedicated server, false otherwise

   return (IS_DEDICATED_SERVER () > 0); // ask engine for this
}

bool IsFileAbleToOpen (char *filename)
{
   // this function tests if a file exists by attempting to open it

   stdfile fp;

   // check if got valid handle
   if (fp.Open (filename, "rb"))
   {
      fp.Close ();
      return true;
   }
   return false;
}

void ExtractFromSteamDLL (char *szGameDLLName)
{
   // this function extracts game dll out of the steam cache

   int iSize;
   unsigned char *ucFileBuf = (*g_engfuncs.pfnLoadFileForMe) (szGameDLLName, &iSize);

   if (ucFileBuf)
   {
      CreatePath (VarArgs ("%s/dlls", GetModName ()));
      stdfile fp (szGameDLLName, "wb");

      if (fp.IsValid ())
      {
         // dumping the game dll file and then close it
         fp.Write (ucFileBuf, iSize, 1);
         fp.Close ();
      }
      FREE_FILE (ucFileBuf);
   }
}

void HudMessage (edict_t *pent, bool bCenter, Vector rgbColor, char *fmt, ...)
{
   if (!IsValidPlayer (pent) || IsValidBot (pent))
      return;

   va_list ap;
   char szBuffer[1024];

   va_start (ap, fmt);
   vsprintf (szBuffer, fmt, ap);
   va_end (ap);

   MESSAGE_BEGIN (MSG_ONE, SVC_TEMPENTITY, NULL, pent);
      WRITE_BYTE (TE_TEXTMESSAGE);
      WRITE_BYTE (1);
      WRITE_SHORT (FixedSigned16 (-1, 1 << 13));
      WRITE_SHORT (FixedSigned16 (bCenter ? -1 : 0, 1 << 13));
      WRITE_BYTE (2);
      WRITE_BYTE (static_cast <int> (rgbColor.x));
      WRITE_BYTE (static_cast <int> (rgbColor.y));
      WRITE_BYTE (static_cast <int> (rgbColor.z));
      WRITE_BYTE (0);
      WRITE_BYTE (RandomLong (230, 255));
      WRITE_BYTE (RandomLong (230, 255));
      WRITE_BYTE (RandomLong (230, 255));
      WRITE_BYTE (200);
      WRITE_SHORT (FixedUnsigned16 (0.0078125, 1 << 8));
      WRITE_SHORT (FixedUnsigned16 (2, 1 << 8));
      WRITE_SHORT (FixedUnsigned16 (6, 1 << 8));
      WRITE_SHORT (FixedUnsigned16 (0.1, 1 << 8));
      WRITE_STRING (const_cast <const char *> (&szBuffer[0]));
   MESSAGE_END ();
}

void ServerPrint (const char *fmt, ...)
{
   va_list ap;
   char szStr[3072];

   va_start (ap, fmt);
   vsprintf (szStr, g_pLocalize->T (fmt), ap);
   va_end (ap);

   SERVER_PRINT (VarArgs ("[%s] %s\n", PRODUCT_LOGTAG, szStr));
}

void ServerPrintNoTag (const char *fmt, ...)
{
   va_list ap;
   char szStr[3072];

   va_start (ap, fmt);
   vsprintf (szStr, g_pLocalize->T (fmt), ap);
   va_end (ap);

   SERVER_PRINT (VarArgs ("%s\n", szStr));
}

void CenterPrint (const char *fmt, ...)
{
   va_list ap;
   char szStr[2048];

   va_start (ap, fmt);
   vsprintf (szStr, g_pLocalize->T (fmt), ap);
   va_end (ap);

   if (IsDedicatedServer ())
   {
      ServerPrint (szStr);
      return;
   }

   MESSAGE_BEGIN (MSG_BROADCAST, GetUserMsgId ("TextMsg"));
      WRITE_BYTE (HUD_PRINTCENTER);
      WRITE_STRING (VarArgs ("%s\n", szStr));
   MESSAGE_END ();
}

void ChartPrint (const char *fmt, ...)
{
   va_list ap;
   char szStr[2048];

   va_start (ap, fmt);
   vsprintf (szStr, g_pLocalize->T (fmt), ap);
   va_end (ap);

   if (IsDedicatedServer ())
   {
      ServerPrint (szStr);
      return;
   }
   strcat (szStr, "\n");

   MESSAGE_BEGIN (MSG_BROADCAST, GetUserMsgId ("TextMsg"));
      WRITE_BYTE (HUD_PRINTTALK);
      WRITE_STRING (szStr);
   MESSAGE_END ();

}

void ClientPrint (edict_t *pent, int iDest, const char *fmt, ...)
{
   va_list ap;
   char szStr[2048];

   va_start (ap, fmt);
   vsprintf (szStr, g_pLocalize->T (fmt), ap);
   va_end (ap);

   if (FNullEnt (pent) || (pent == g_pentHostEdict))
   {
      if (iDest & 0x3ff)
         ServerPrint (szStr);
      else
         ServerPrintNoTag (szStr);

      return;
   }
   strcat (szStr, "\n");

   if (iDest & 0x3ff)
      (*g_engfuncs.pfnClientPrintf) (pent, static_cast <PRINT_TYPE> (iDest &= ~0x3ff), VarArgs ("[YAPB] %s", szStr));
   else
      (*g_engfuncs.pfnClientPrintf) (pent, static_cast <PRINT_TYPE> (iDest), szStr);

}

bool IsLinux (void)
{
   // this function returns true if server is running under linux, and false otherwise (i.e. win32)
#ifndef PLATFORM_WIN32
   return true;
#else
   return false;
#endif
}

void ServerCommand (const char *fmt, ...)
{
   // this function asks the engine to execute a server command

   va_list ap;
   static char szStr[1024];

   // concatenate all the arguments in one string
   va_start (ap, fmt);
   vsprintf (szStr, fmt, ap);
   va_end (ap);

   SERVER_COMMAND (VarArgs ("%s\n", szStr)); // execute command
}

float WorldTime (void)
{
   // this function returns engine current time on this server

   return g_pGlobals->time;
}

int MaxClients (void)
{
   // this function returns current players on server

   return g_pGlobals->maxClients;
}

int32 RandomLong (int32 iFrom, int32 iTo)
{
   // this function returns a random integer number between (and including) the starting and
   // ending values passed by parameters from and to.
   
   if (iTo <= iFrom)
      return iFrom;

   return (iFrom + GetRandom () / (LONG_MAX / ((iTo - iFrom) + 1)));
}

float RandomFloat (float fFrom, float fTo)
{
   // this function returns a random floating-point number between (and including) the starting
   // and ending values passed by parameters from and to.
   
   if (fTo <= fFrom)
      return fFrom;

   return (fFrom + static_cast <float> (GetRandom ()) / (LONG_MAX / (fTo - fFrom)));
}

const char *GetMapName (void)
{
   // this function gets the map name and store it in the map_name global string variable.

   static char szMap[256];
   sprintf (szMap, const_cast <const char *> (g_pGlobals->pStringBase + static_cast <int> (g_pGlobals->mapname)));
   
   return (&szMap[0]); // and return a pointer to it
}

bool OpenConfig (const char *cszFile, char *cszErrorIfNotExists, CFile *pOut, bool bLanguageDependant)
{
   if (pOut->IsValid ())
      pOut->Close ();

   if (bLanguageDependant)
   {
      if ((strcmp (cszFile, "lang.cfg") == 0) && (strcmp (g_rgpcvBotCVar[CVAR_LANGUAGE]->GetString (), "en") == 0))
         return false;

      char *cszLanguageDependantConfigFile = VarArgs ("%s/addons/yapb/config/language/%s_%s", GetModName (), g_rgpcvBotCVar[CVAR_LANGUAGE]->GetString (), cszFile);

      // check is file is exists for this language
      if (IsFileAbleToOpen (cszLanguageDependantConfigFile))
         pOut->Open (cszLanguageDependantConfigFile, "rt");
      else
         pOut->Open (VarArgs ("%s/addons/yapb/config/language/en_%s", GetModName (), cszFile), "rt");
   }
   else
      pOut->Open (VarArgs ("%s/addons/yapb/config/%s", GetModName (), cszFile), "rt");
 
   if (!pOut->IsValid ())
   {
      LogToFile (true, LOG_ERROR, cszErrorIfNotExists);
      return false;
   }
   return true;
}

const char *GetWptDir (void)
{
   return VarArgs ("%s/addons/yapb/wptdefault/", GetModName ());
}

void CVarSetFloat (const char *cszVariable, float fNewVal)
{
   if (IsNullString (cszVariable))
      return;

   (*g_engfuncs.pfnCVarSetFloat) (cszVariable, fNewVal);
}

void CVarSetString (const char *cszVariable, const char *cszNewVal)
{
   if (IsNullString (cszVariable))
      return;

   (*g_engfuncs.pfnCVarSetString) (cszVariable, cszNewVal); // ask the engine to update this CVAR
}

void RegisterBotVariable (int iVariableName, const char *cszInitialValue, int iFlags)
{
   static cvar_t kCVarList[60]; // maximum of 60 cvars
   static int iCVarCount = 0;

   if (iCVarCount == 60)
      TerminateOnError ("Can't register variable, number of variables exceed 60."); // internal error :)

   // list if available cvars
   const char *rgcszBotCVar[CVAR_TOTAL] =
   {
      "yb_autovacate", "yb_quota", "yb_minskill", "yb_maxskill", "yb_followuser", "yb_timersound", "yb_timerpickup", "yb_timergrenade",
      "yb_debuggoal", "yb_chat", "yb_synth", "yb_knifemode", "yb_skilltags", "yb_stop", "yb_thruwalls", "yb_votes",  "yb_spray", "yb_buy",        
      "yb_debug", "yb_turnmethod", "yb_prefix", "yb_language", "yb_version", "yb_aim_damper_coefficient_x", "yb_aim_damper_coefficient_y",    
      "yb_aim_deviation_x", "yb_aim_deviation_y", "yb_aim_influence_x_on_y", "yb_aim_influence_y_on_x", "yb_aim_notarget_slowdown_ratio", 
      "yb_aim_offset_delay", "yb_aim_spring_stiffness_x", "yb_aim_spring_stiffness_y", "yb_aim_target_anticipation_ratio", "yb_password", 
      "yb_passwordkey", "yb_voicepath", "yb_tkpunish", "yb_communication", "yb_economics", "yb_forceteam", "yb_restrweapons", "yb_dangerfactor",
      "yb_dontshoot", "yb_hardcoremode"
   };

   kCVarList[iCVarCount].name = const_cast <char *> (rgcszBotCVar[iVariableName]);
   kCVarList[iCVarCount].strval = const_cast <char *> (cszInitialValue);
   kCVarList[iCVarCount].flags = iFlags;

   CVAR_REGISTER (&kCVarList[iCVarCount]);
   iCVarCount++;

   // get pointer
   g_rgpcvBotCVar[iVariableName] = CVAR_GET_POINTER (rgcszBotCVar[iVariableName]);
}

void RegisterCommand (char *szCommand, void pfnFunction (void))
{
   // this function tells the engine that a new server command is being declared, in addition
   // to the standard ones, whose name is command_name. The engine is thus supposed to be aware
   // that for every "command_name" server command it receives, it should call the function
   // pointed to by "function" in order to handle it.

   if ((IsNullString (szCommand)) || (pfnFunction == NULL))
      return; // reliability check

   REG_SVR_COMMAND (szCommand, pfnFunction); // ask the engine to register this new command
}

void SpeechSynth (char *szMessage)
{
   if (!g_rgpcvBotCVar[CVAR_SYNTH]->GetBool ())
      return;

   ServerCommand ("speak \"%s\"", szMessage);
}

void CheckWelcomeMessage (void)
{
   // the purpose of this function, is  to send quick welcome message, to the listenserver
   // entity

   static bool bIsReceived = false;
   static float fReceiveTime = 0.0;

   if (bIsReceived)
      return;

   vector <string> kSpeech;

   // add default messages
   kSpeech.AddItem ("hello user,communication is acquired");
   kSpeech.AddItem ("your presence is acknowledged");
   kSpeech.AddItem ("high man, your in command now");
   kSpeech.AddItem ("blast your hostile for good");
   kSpeech.AddItem ("high man, kill some idiot here");
   kSpeech.AddItem ("is there a doctor in the area");
   kSpeech.AddItem ("warning, experimental materials detected");
   kSpeech.AddItem ("high amigo, shoot some but");
   kSpeech.AddItem ("attention, hours of work software, detected");
   kSpeech.AddItem ("time for some bad ass explosion");
   kSpeech.AddItem ("bad ass son of a breach device activated");
   kSpeech.AddItem ("high, do not question this great service");
   kSpeech.AddItem ("engine is operative, hello and goodbye");
   kSpeech.AddItem ("high amigo, your administration has been great last day");
   kSpeech.AddItem ("attention, expect experimental armed hostile presence");
   kSpeech.AddItem ("warning, medical attention required");

   if (IsAlive (g_pentHostEdict) && !bIsReceived && (fReceiveTime < 1.0) && (g_iNumWaypoints > 0 ? g_bCommencing : true))
      fReceiveTime = WorldTime () + 4.0; // receive welcome message in four seconds after game has commencing

   if ((fReceiveTime > 0.0) && (fReceiveTime < WorldTime ()) && (bIsReceived == false) && (g_iNumWaypoints > 0 ? g_bCommencing : true))
   {
      SpeechSynth (const_cast <char *> (kSpeech.Random ().GetString ()));

      //ServerPrintNoTag ("----- YaPB v%s (Build: %u), {%s}, (c) 2006, by %s -----", PRODUCT_VERSION, GenerateBuildNumber (), PRODUCT_URL, PRODUCT_AUTHOR);
      //HudMessage (g_pentHostEdict, true, Vector (RandomLong (33, 255), RandomLong (33, 255), RandomLong (33, 255)), "\nServer is running YaPB v%s (Build: %u)\nDeveloped by %s\n\n%s", PRODUCT_VERSION, GenerateBuildNumber (), PRODUCT_AUTHOR, g_pWaypoint->GetInfo ());

      fReceiveTime = 0.0;
      bIsReceived = true;
   }
}

void DetectCSVersion (void)
{
   byte *ucpDetect = NULL;
   const char *const cszInformation = "Game Registered: CS %s (0x%d)";

   // switch version returned by dll loader
   switch (g_iCSVersion)
   {
   // counter-strike 1.x, WON ofcourse
   case VERSION_WON:
      ServerPrint (cszInformation, "1.x (WON)", sizeof (CBot));
      break;

   // counter-strike 1.6 or higher (plus detetect for non-steam versions of 1.5)
   case VERSION_STEAM:
      ucpDetect = (*g_engfuncs.pfnLoadFileForMe) ("events/galil.sc", NULL);

      if (ucpDetect != NULL)
      {
         ServerPrint (cszInformation, "1.6 (Steam)", sizeof (CBot));
         g_iCSVersion = VERSION_STEAM; // just to be sure
      }
      else if (ucpDetect == NULL)
      {
         ServerPrint (cszInformation, "1.5 (WON)", sizeof (CBot));
         g_iCSVersion = VERSION_WON; // reset it to WON
      }

      // if we have loaded the file free it
      if (ucpDetect != NULL)
         (*g_engfuncs.pfnFreeFile) (ucpDetect);
      break;

   // counter-strike cz
   case VERSION_CSCZ:
      ServerPrint (cszInformation, "CZ (Steam)", sizeof (CBot));
      break;
   }

   // since this function is called after original dll, function called, we can get pointer of cvars here.
   g_rgpcvBotCVar[CVAR_C4TIMER] = CVAR_GET_POINTER ("mp_c4timer");
   g_rgpcvBotCVar[CVAR_BUYTIME] = CVAR_GET_POINTER ("mp_buytime");
   g_rgpcvBotCVar[CVAR_FRIENDLYFIRE] = CVAR_GET_POINTER ("mp_friendlyfire");
   g_rgpcvBotCVar[CVAR_ROUNDTIME] = CVAR_GET_POINTER ("mp_roundtime");
   g_rgpcvBotCVar[CVAR_GRAVITY] = CVAR_GET_POINTER ("sv_gravity");
   g_rgpcvBotCVar[CVAR_DEVELOPER] = CVAR_GET_POINTER ("developer");
   g_rgpcvBotCVar[CVAR_FREEZETIME] = CVAR_GET_POINTER ("mp_freezetime");
}

int GetUserMsgId (const char *cszName)
{
   // this function returns the user message id of the recorded message named cszName.

   if (g_bIsMetamod)
      return GET_USER_MSG_ID (PLID, cszName, NULL);

   // cycle through our known user message types array
   ITERATE_ARRAY (g_arUserMessages, i)
   {
      if (FStrEq (g_arUserMessages[i].cszName, cszName))
         return g_arUserMessages[i].iId; // return the id of the user message named msg_name
   }

   // unregistered user message, have the engine register it
   return REG_USER_MSG (cszName, -1); // ask the engine to register this new message
}

const char *GetUserMsgName (int iMsgType)
{
   // this function returns the user message name of the recorded message index iMsgType.

   if (g_bIsMetamod)
      return GET_USER_MSG_NAME (PLID, iMsgType, NULL);

   // cycle through our known user message types array
   ITERATE_ARRAY (g_arUserMessages, i)
   {
      if (g_arUserMessages[i].iId == iMsgType)
         return g_arUserMessages[i].cszName; // return the name of the user message having the msg_type id
   }
   return NULL; // unregistered user message  
}

char *WPN_StrChr (const char *cszInput, int cCh, int iLine)
{
   // this version of strchr is designed only for yapb weapon config parser

   // use try/catch blocks to prevent crashing on config load
   try
   {
      while ((*cszInput != NULL) && (*cszInput != static_cast <char> (cCh)))
         cszInput++;

      if (*cszInput == static_cast <char> (cCh))
         return const_cast <char *> (cszInput);
   }
   catch (...) // catch all errors
   {
      TerminateOnError ("Weapon config syntax error. On Line: #%d. Please check the config file.", iLine);
   }
   return NULL;
}

void PlaySound (edict_t *pentEdict, const char *cszName)
{
   // TODO: make this obsolete
   EMIT_SOUND_DYN2 (pentEdict, CHAN_WEAPON, cszName, 1.0, ATTN_NORM, 0, 100);

   return;
}

void TerminateOnError (const char *fmt, ...)
{
   // this function terminates the game because of an error and prints the message string pointed
   // to by fmt both in the server console and in a messagebox.

   char szBuffer[1024];
   va_list ap;

   va_start (ap, fmt);
   vsprintf (szBuffer, fmt, ap);
   va_end (ap);

   FreeLibraryMemory ();
   LogToFile (true, LOG_ERROR, szBuffer);

   #if defined (PLATFORM_WIN32)
      MessageBox (NULL, szBuffer, "YaPB Critical Error", MB_ICONERROR);
   #endif

   exit (1);
}

float GetWaveLength (const char *cszWaveFile)
{
   tWaveHeader kWave;
   memset (&kWave, 0, sizeof (kWave));

   stdfile fp (VarArgs ("%s/%s/%s.wav", GetModName (), g_rgpcvBotCVar[CVAR_VOICEPATH]->GetString (), cszWaveFile), "rb");
   
   // we're got valid handle?
   if (!fp.IsValid ())
   {
      LogToFile (false, LOG_ERROR, "Can't open wave file, %s", cszWaveFile);
      return 0;
   }

   if (fp.Read (&kWave, sizeof (tWaveHeader)) == NULL)
   {
      LogToFile (false, LOG_ERROR, "Wave File %s - has wrong or unsupported format", cszWaveFile);
      return 0;
   }

   if (strncmp (kWave.szWaveChunkId, "WAVE", 4) != 0)
   {
      LogToFile (false, LOG_ERROR, "Wave File %s - has wrong wave chunk id", cszWaveFile);
      return 0;
   }
   fp.Close ();

   if (kWave.ulDataChunkLength == 0)
   {
      LogToFile (false, LOG_ERROR, "Wave File %s - has zero length!", cszWaveFile);
      return 0;
   }

   char sTmp[32];
   sprintf (sTmp, "0.%d", kWave.ulDataChunkLength);

   float fSecondLength = kWave.ulDataChunkLength / kWave.ulBytesPerSecond;
   float fMilliSecondLength = atof (sTmp);

   return (fSecondLength == 0.0 ? fMilliSecondLength : fSecondLength);
}

void LogToFile (bool bPrintToConsole, int iLevel, const char *fmt, ...)
{
   // this function logs a message to the message log file root directory

   va_list ap;
   char szBuffer[512], szLevel[32], szLogLine[1024];

   va_start (ap, fmt);
   vsprintf (szBuffer, fmt, ap);
   va_end (ap);

   stdfile fp ("yapb.log", "at");

   if (!fp.IsValid ())
      return;

   switch (iLevel)
   {
   case LOG_DEFAULT:
      strcpy (szLevel, "LOG: ");
      break;

   case LOG_WARNING:
      strcpy (szLevel, "WARNING: ");
      break;

   case LOG_ERROR:
      strcpy (szLevel, "ERROR: ");
      break;

   case LOG_CRITICAL:
      strcpy (szLevel, "FATAL ERROR: ");
      break;
   }

   time_t td = time (&td);
   tm *time = localtime (&td);

   sprintf (szLogLine, "[%02d:%02d:%02d] %s%s", time->tm_hour, time->tm_min, time->tm_sec, szLevel, szBuffer);

   if (bPrintToConsole)
      ServerPrintNoTag (szLogLine);

   fp.Printf ("%s\n", szLogLine);
   fp.Close ();

   if (iLevel == LOG_CRITICAL)
      TerminateOnError (szBuffer);
}

tModInfo *FindGame (const char *szId)
{
   // this function find's a modinfo corresponding to the given game name.

   tModInfo *pMod = NULL;

   for (int i = 0; g_rgkModList[i].szName; i++)
   {
      pMod = &g_rgkModList[i];

      if (strcmp (pMod->szName, szId) == 0)
         return pMod;
   }
   return NULL; // not found
}

char *CLocalizer::T (const char *cszInput)
{
   if (IsDedicatedServer ())
      return const_cast <char *> (&cszInput[0]);

   static char szString[1024];
   const char *cszPtr = cszInput + strlen (cszInput) - 1;

   while ((cszPtr > cszInput) && (*cszPtr == '\n'))
      cszPtr--;

   if (cszPtr != cszInput)
      cszPtr++;

   strncpy (szString, cszInput, 1024);
   StrTrim (szString);

   ITERATE_ARRAY (m_arLanguage, i)
   {
      if (strcmp (szString, m_arLanguage[i].szOriginal) == 0)
      {
         strncpy (szString, m_arLanguage[i].szTranslated, 1024);

         if (cszPtr != cszInput)
            strncat (szString, cszPtr, 1024 - strlen (szString));

         return &szString[0];
      }
   }
   return const_cast <char *> (&cszInput[0]); // nothing found
}

bool FindNearestPlayer (void **pvHolder, edict_t *pentTo, float fSearchDistance, bool bSameTeam, bool bNeedBot, bool bAlive, bool bNeedDrawn)
{
   // this function finds nearest to pentTo, player with set of parameters, like his
   // team, live status, search distance etc. if bNeedBot is true, then pvHolder, will
   // be filled with bot pointer, else with edict pointer(!).

   edict_t *pent = NULL, *pentSurvive = NULL; // pointer to temporaly & survive entity
   float fNearestPlayer = 4096.0; // nearest player

   while (!FNullEnt (pent = FIND_ENTITY_IN_SPHERE (pent, pentTo->v.origin, fSearchDistance)))
   {
      if (FNullEnt (pent) || !IsValidPlayer (pent) || (pentTo == pent))
         continue; // skip invalid players

      if ((bSameTeam && (GetTeam (pent) != GetTeam (pentTo))) || (bAlive && !IsAlive (pent)) || (bNeedBot && !IsValidBot (pent)) || (bNeedDrawn && (pent->v.effects & EF_NODRAW)))
         continue; // filter players with parameters

      float fDistance = (pent->v.origin - pentTo->v.origin).Length ();

      if (fDistance < fNearestPlayer)
      {
         fNearestPlayer = fDistance;
         pentSurvive = pent;
      }
   }

   if (FNullEnt (pentSurvive))
      return false; // nothing found

   // fill the holder
   if (bNeedBot)
      *pvHolder = reinterpret_cast <void *> (g_pBotManager->GetBot (pentSurvive));
   else
      *pvHolder = reinterpret_cast <void *> (pentSurvive);

   return true;
}

void SoundAttachToThreat (edict_t *pent, const char *cszSample, float fVolume)
{
   // this function called by the sound hooking code (in emit_sound) enters the played sound into 
   // the array associated with the entity

   if (FNullEnt (pent) || IsNullString (cszSample))
      return; // reliability check

   Vector vPosition = GetEntityOrigin (pent);
   int iIndex = ENTINDEX (pent) - 1;

   if ((iIndex < 0) || (iIndex >= MaxClients ()))
   {
      float fNearestDistance = FLT_MAX;

      // loop through all players
      for (int i = 0; i < MaxClients (); i++)
      {
         if (!(g_rgkClients[i].iFlags & CFLAG_USED) || !(g_rgkClients[i].iFlags & CFLAG_ALIVE))
            continue;

         float fDistance = LengthSquared (g_rgkClients[i].pentEdict->v.origin - vPosition);

         // now find nearest player
         if (fDistance < fNearestDistance)
         {
            iIndex = i;
            fNearestDistance = fDistance;
         }
      }
   }

   if ((strncmp ("player/bhit_flesh", cszSample, 17) == 0) || (strncmp ("player/headshot", cszSample, 15) == 0))
   {
      // hit/fall sound?
      g_rgkClients[iIndex].fHearingDistance = 800.0 * fVolume;
      g_rgkClients[iIndex].fTimeSoundLasting = WorldTime () + 0.5;
      g_rgkClients[iIndex].vSoundPosition = vPosition;
   }
   else if (strncmp ("items/gunpickup", cszSample, 15) == 0) 
   {
      // weapon pickup?
      g_rgkClients[iIndex].fHearingDistance = 800.0 * fVolume;
      g_rgkClients[iIndex].fTimeSoundLasting = WorldTime () + 0.5;
      g_rgkClients[iIndex].vSoundPosition = vPosition;
   }
   else if (strncmp ("weapons/zoom", cszSample, 12) == 0)
   {
      // sniper zooming?
      g_rgkClients[iIndex].fHearingDistance = 500.0 * fVolume;
      g_rgkClients[iIndex].fTimeSoundLasting = WorldTime () + 0.1;
      g_rgkClients[iIndex].vSoundPosition = vPosition;
   }
   else if (strncmp ("items/9mmclip", cszSample, 13) == 0) 
   {
      // ammo pickup?
      g_rgkClients[iIndex].fHearingDistance = 500.0 * fVolume;
      g_rgkClients[iIndex].fTimeSoundLasting = WorldTime () + 0.1;
      g_rgkClients[iIndex].vSoundPosition = vPosition;
   }
   else if (strncmp ("hostage/hos", cszSample, 11) == 0)
   {
      // CT used hostage?
      g_rgkClients[iIndex].fHearingDistance = 1024.0 * fVolume;
      g_rgkClients[iIndex].fTimeSoundLasting = WorldTime () + 5.0;
      g_rgkClients[iIndex].vSoundPosition = vPosition;
   }
   else if ((strncmp ("debris/bustmetal", cszSample, 16) == 0) || (strncmp ("debris/bustglass", cszSample, 16) == 0))
   {
      // broke something?
      g_rgkClients[iIndex].fHearingDistance = 1024.0 * fVolume;
      g_rgkClients[iIndex].fTimeSoundLasting = WorldTime () + 2.0;
      g_rgkClients[iIndex].vSoundPosition = vPosition;
   }
   else if (strncmp ("doors/doormove", cszSample, 14) == 0)
   {
      // someone opened a door
      g_rgkClients[iIndex].fHearingDistance = 1024.0 * fVolume;
      g_rgkClients[iIndex].fTimeSoundLasting = WorldTime () + 3.0;
      g_rgkClients[iIndex].vSoundPosition = vPosition;
   }
   else if (strncmp ("weapons/reload",  cszSample, 14) == 0)
   {
      // reloading ?
      g_rgkClients[iIndex].fHearingDistance = 500.0 * fVolume;
      g_rgkClients[iIndex].fTimeSoundLasting = WorldTime () + 0.5;
      g_rgkClients[iIndex].vSoundPosition = vPosition;
   }
}

void SoundSimulateUpdate (int iPlayerIndex)
{
   // this function tries to simulate playing of sounds to let the bots hear sounds which aren't 
   // captured through server sound hooking

   InternalAssert (iPlayerIndex >= 0);
   InternalAssert (iPlayerIndex < MaxClients ());

   if ((iPlayerIndex < 0) || (iPlayerIndex >= MaxClients ()))
      return; // reliability check

   edict_t *pentPlayer = g_rgkClients[iPlayerIndex].pentEdict;
   
   float fVelocity = pentPlayer->v.velocity.Length2D ();
   float fHearDistance = 0.0;
   float fTimeSound = 0.0;

   if (pentPlayer->v.oldbuttons & IN_ATTACK) // pressed attack button?
   {
      fHearDistance = 4096.0;
      fTimeSound = WorldTime () + 0.5;
   }
   else if (pentPlayer->v.oldbuttons & IN_USE) // pressed used button?
   {
      fHearDistance = 1024.0;
      fTimeSound = WorldTime () + 0.5;
   }
   else if (pentPlayer->v.oldbuttons & IN_RELOAD) // pressed reload button?
   {
      fHearDistance = 500.0;
      fTimeSound = WorldTime () + 0.5;
   }
   else if (pentPlayer->v.movetype == MOVETYPE_FLY) // uses ladder?
   {
      if (fabs (pentPlayer->v.velocity.Length ()) > 50.0)
      {
         fHearDistance = 1024.0;
         fTimeSound = WorldTime () + 0.3;
      }
   }
   else
   {
      // moves fast enough?
      fHearDistance = 1280.0 * (fVelocity / 260);
      fTimeSound = WorldTime () + 0.3;
   }

   if (fHearDistance > 0.0) // did issue sound?
   {
      // some sound already associated?
      if (g_rgkClients[iPlayerIndex].fTimeSoundLasting > WorldTime ())
      {
         // new sound louder (bigger range) than old sound?
         if (g_rgkClients[iPlayerIndex].fHearingDistance <= fHearDistance)
         {
            // override it with new
            g_rgkClients[iPlayerIndex].fHearingDistance = fHearDistance;
            g_rgkClients[iPlayerIndex].fTimeSoundLasting = fTimeSound;
            g_rgkClients[iPlayerIndex].vSoundPosition = pentPlayer->v.origin;
         }
      }
      else // new sound?
      {
         // just remember it
         g_rgkClients[iPlayerIndex].fHearingDistance = fHearDistance;
         g_rgkClients[iPlayerIndex].fTimeSoundLasting = fTimeSound;
         g_rgkClients[iPlayerIndex].vSoundPosition = pentPlayer->v.origin;
      }
   }
}

uint16 GenerateBuildNumber (void)
{
   // this function generates build number from the compiler date macros

   // get compiling date using compiler macros
   const char *cszDate = __DATE__;

   // array of the month names
   const char *rgcszMonths[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

   // array of the month days
   byte uMonthDays[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

   int iDay = 0; // day of the year
   int iYear = 0; // year
   int i = 0;

   // go through all monthes, and calculate, days since year start
   for (i = 0; i < 11; i++)
   {
      if (strncmp (&cszDate[0], rgcszMonths[i], 3) == 0)
         break; // found current month break

      iDay += uMonthDays[i]; // add month days
   }
   iDay += atoi (&cszDate[4]) - 1; // finally calculate day
   iYear = atoi (&cszDate[7]) - 2000; // get years since year 2000

   uint16 uBuildNumber = iDay + static_cast <int> ((iYear - 1) * 365.25);

   // if the year is a leap year?
   if (((iYear % 4) == 0) && (i > 1))
      uBuildNumber += 1; // add one year more

   return uBuildNumber - 1114;
}

int GetWeaponReturn (bool bString, const char *cszWeaponAlias, int iWeaponId)
{
   // this function returning weapon id from the weapon alias and vice versa.

   // structure definition for weapon tab
   struct tWeaponTab
   {
      eWeapon iWeaponId; // weapon id
      const char *cszAlias; // weapon alias
   };

   // weapon enumeration
   tWeaponTab rgkWeaponTab[] = 
   {
      {WEAPON_USP,       "usp"}, // HK USP .45 Tactical
      {WEAPON_GLOCK18,   "glock"}, // Glock18 Select Fire
      {WEAPON_DEAGLE,    "deagle"}, // Desert Eagle .50AE
      {WEAPON_P228,      "p228"}, // SIG P228
      {WEAPON_ELITE,     "elite"}, // Dual Beretta 96G Elite
      {WEAPON_FIVESEVEN, "fn57"}, // FN Five-Seven
      {WEAPON_M3,        "m3"}, // Benelli M3 Super90
      {WEAPON_XM1014,    "xm1014"}, // Benelli XM1014
      {WEAPON_MP5NAVY,   "mp5"}, // HK MP5-Navy
      {WEAPON_TMP,       "tmp"}, // Steyr Tactical Machine Pistol
      {WEAPON_P90,       "p90"}, // FN P90
      {WEAPON_MAC10,     "mac10"}, // Ingram MAC-10
      {WEAPON_UMP45,     "ump45"}, // HK UMP45
      {WEAPON_AK47,      "ak47"}, // Automat Kalashnikov AK-47
      {WEAPON_GALIL,     "galil"}, // IMI Galil
      {WEAPON_FAMAS,     "famas"}, // GIAT FAMAS
      {WEAPON_SG552,     "sg552"}, // Sig SG-552 Commando 
      {WEAPON_M4A1,      "m4a1"}, // Colt M4A1 Carbine
      {WEAPON_AUG,       "aug"}, // Steyr Aug
      {WEAPON_SCOUT,     "scout"}, // Steyr Scout
      {WEAPON_AWP,       "awp"}, // AI Arctic Warfare/Magnum
      {WEAPON_G3SG1,     "g3sg1"}, // HK G3/SG-1 Sniper Rifle
      {WEAPON_SG550,     "sg550"}, // Sig SG-550 Sniper
      {WEAPON_M249,      "m249"}, // FN M249 Para
      {WEAPON_FLASHBANG, "flash"}, // Concussion Grenade
      {WEAPON_HEGRENADE, "hegren"}, // High-Explosive Grenade
      {WEAPON_SMOKEGRENADE, "sgren"}, // Smoke Grenade 
      {WEAPON_ARMOR,     "vest"}, // Kevlar Vest
      {WEAPON_ARMORHELM, "vesthelm"}, // Kevlar Vest and Helmet
      {WEAPON_DEFUSER,   "defuser"}, // Defuser Kit
      {WEAPON_SHIELDGUN, "shield"}, // Tactical Shield
   };

   // if we need to return the string, find by weapon id
   if (bString && (iWeaponId != -1))
   {
      for (int i = 0; i < ARRAYSIZE_HLSDK (rgkWeaponTab); i++)
      {
         if (rgkWeaponTab[i].iWeaponId == iWeaponId) // is weapon id found?
            return MAKE_STRING (rgkWeaponTab[i].cszAlias);
      }
      return MAKE_STRING ("(none)"); // return none
   }

   // else search weapon by name and return weapon id
   for (int i = 0; i < ARRAYSIZE_HLSDK (rgkWeaponTab); i++)
   {
      if (strncmp (rgkWeaponTab[i].cszAlias, cszWeaponAlias, strlen (rgkWeaponTab[i].cszAlias)) == 0)
         return rgkWeaponTab[i].iWeaponId;
   }
   return -1; // no weapon was found return -1
}

