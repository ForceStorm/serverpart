//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#include "yapb.h"
#include "MetaHook_Msg.h"

void CNetMsg::Execute (void *p)
{
   if (m_iMessage == NETMSG_NULL)
      return; // no message or not for bot, return

   // some needed variables
   static unsigned char ucR, ucG, ucB;
   static unsigned char ucEnable;

   static int iDamageArmor, iDamageTaken, iDamageBits;
   static int iKillerIndex, iVictimIndex, iPlayerIndex;
   static int iIndex, iAmount, iNumPlayers;
   static int iState, iId, iClip;

   static Vector vDamageOrigin;
   static tWeapon kWeapon;

   int iMetaHookFunc = 0;

   // now starts of netmessage execution
   switch (GetCurrent ())
   {
   case NETMSG_VGUI: // this is obsolete since 21 Steam Update! :)
      // this message is sent when a VGUI menu is displayed.

      if (GetState () == 0)
      {
         switch (*(int *) p)
         {
         case MENU_TEAM:
            m_pBot->m_iStartAction = MESSAGE_TEAM_SELECT;
            break;

         case MENU_TERRORIST:
         case MENU_CT:
            m_pBot->m_iStartAction = MESSAGE_CLASS_SELECT;
            break;
         }
      }
      break;

   case NETMSG_SHOWMENU: 
      // this message is sent when a text menu is displayed.

      if (GetState () < 3)
      {
         m_iState++;  // ignore first 3 fields of message
         break;
      }

      if (strcmp ((char *) p, "#Team_Select") == 0) // team select menu?
         m_pBot->m_iStartAction = MESSAGE_TEAM_SELECT;
      else if (strcmp ((char *) p, "#Team_Select_Spect") == 0) // team select menu?
         m_pBot->m_iStartAction = MESSAGE_TEAM_SELECT;
      else if (strcmp ((char *) p, "#IG_Team_Select_Spect") == 0) // team select menu?
         m_pBot->m_iStartAction = MESSAGE_TEAM_SELECT;
      else if (strcmp ((char *) p, "#IG_Team_Select") == 0) // team select menu?
         m_pBot->m_iStartAction = MESSAGE_TEAM_SELECT;
      else if (strcmp ((char *) p, "#IG_VIP_Team_Select") == 0) // team select menu?
         m_pBot->m_iStartAction = MESSAGE_TEAM_SELECT;
      else if (strcmp ((char *) p, "#IG_VIP_Team_Select_Spect") == 0) // team select menu?
         m_pBot->m_iStartAction = MESSAGE_TEAM_SELECT;
      else if (strcmp ((char *) p, "#Terrorist_Select") == 0) // T model select?
         m_pBot->m_iStartAction = MESSAGE_CLASS_SELECT;
      else if (strcmp ((char *) p, "#CT_Select") == 0) // CT model select menu?
         m_pBot->m_iStartAction = MESSAGE_CLASS_SELECT;
      break;

   case NETMSG_WEAPONLIST: 
      // this message is sent when a client joins the game. All of the weapons are sent with the weapon ID and information about what ammo is used.
      
      switch (GetState ())
      {
      case 0:
         strcpy (kWeapon.szClassname, (char *) p);
         break;

      case 1:
         kWeapon.iAmmo1 = *(int *) p;  // ammo index 1
         break;

      case 2:
         kWeapon.iAmmo1Max = *(int *) p;  // max ammo 1
         break;

      case 5:
         kWeapon.iSlot = *(int *) p;  // slot for this weapon
         break;

      case 6:
         kWeapon.iPosition = *(int *) p;  // position in slot
         break;

      case 7:
         kWeapon.iId = *(int *) p;  // weapon ID
         break;

      case 8:
         kWeapon.iFlags = *(int *) p;  // flags for weapon (WTF???)
         g_rgkWeaponDefs[kWeapon.iId] = kWeapon;  // store away this weapon with it's ammo information...
         break;
      }
      break;

   case NETMSG_METAHOOK: 
      // this message is sent when a weapon is selected (either by the bot chosing a weapon or by the server auto assigning the bot a weapon). In CS it's also called when Ammo is increased/decreased
      
      switch (GetState ())
      {
      case 0:
         iMetaHookFunc = *(int *) p;  // state of the current weapon (WTF???)

		 if( iMetaHookFunc != MH_MSG_CUR_WPN )
		 {
			 Reset();
			 return;
		 }
		 
         break;

      case 1:
         iId = *(int *) p;  // weapon ID of current weapon
         break;

      case 2:
         iClip = *(int *) p;  // ammo currently in the clip for this weapon
         
         if (iId <= 31)
         {
            if (iState != 0)
               m_pBot->m_iCurrentWeapon = iId;

            // ammo amount decreased ? must have fired a bullet...
            if ((iId == m_pBot->m_iCurrentWeapon) && (m_pBot->m_rgAmmoInClip[iId] > iClip))
               m_pBot->m_fTimeLastFired = WorldTime (); // remember the last bullet time

            m_pBot->m_rgAmmoInClip[iId] = iClip;
         }
         break;
      }
      break;

   case NETMSG_AMMOX:    
      // this message is sent whenever ammo amounts are adjusted (up or down). NOTE: Logging reveals that CS uses it very unreliable!
      
      switch (GetState ())
      {
      case 0:
         iIndex = *(int *) p;  // ammo iIndex (for type of ammo)
         break;

      case 1:
         iAmount = *(int *) p;  // the iAmount of ammo currently available
         m_pBot->m_rgAmmo[iIndex] = iAmount;  // store it away

         break;
      }
      break;

   case NETMSG_AMMOPICKUP:
      // this message is sent when the bot picks up some ammo (AmmoX messages are also sent so this message is probably
      // not really necessary except it allows the HUD to draw pictures of ammo that have been picked up.  The bots
      // don't really need pictures since they don't have any eyes anyway.

      switch (GetState ())
      {
      case 0:
         iIndex = *(int *) p;
         break;

      case 1:
         iAmount = *(int *) p;
         m_pBot->m_rgAmmo[iIndex] = iAmount;

         break;
      }
      break;

   case NETMSG_DAMAGE: 
      // this message gets sent when the bots are getting damaged.

      switch (GetState ())
      {
      case 0:
         iDamageArmor = *(int *) p;
         break;

      case 1:
         iDamageTaken = *(int *) p;
         break;

      case 2:
         iDamageBits = *(int *) p;
         break;

      case 3:
         vDamageOrigin.x = *(float *) p;
         break;

      case 4:
         vDamageOrigin.y = *(float *) p;
         break;

      case 5:
         vDamageOrigin.z = *(float *) p;

         if ((iDamageArmor > 0) || (iDamageTaken > 0))
            m_pBot->TakeDamage (m_pBot->pev->dmg_inflictor, vDamageOrigin, iDamageTaken, iDamageArmor, iDamageBits);
      }
      break;

   case NETMSG_MONEY: 
      // this message gets sent when the bots money amount changes

      if (GetState () == 0)
         m_pBot->m_iAccount = *(int *) p;  // amount of money
      break;

   case NETMSG_STATUSICON:
      switch (GetState ())
      {
      case 0:
         ucEnable = *(byte *) p;
         break;

      case 1:
         if (strcmp ((char *) p, "defuser") == 0)
            m_pBot->m_bHasDefuser = (ucEnable != 0);
         else if (strcmp ((char *) p, "buyzone") == 0)
         {
            m_pBot->m_bInBuyZone = (ucEnable != 0);

            if (ucEnable && ((g_fTimeRoundStart + RandomFloat (10, 20) + g_rgpcvBotCVar[CVAR_BUYTIME]->GetFloat ()) < WorldTime ()) && !g_bBombPlanted)
            {
               m_pBot->m_bBuyingFinished = false;
               m_pBot->m_iBuyCount = 0;

               m_pBot->PushMessageQueue(MESSAGE_BUY);
               m_pBot->m_fNextBuyTime = WorldTime ();
            }
         }
         else if (strcmp ((char *) p, "vipsafety") == 0)
            m_pBot->m_bInVIPZone = (ucEnable != 0);
         else if (strcmp ((char *) p, "c4") == 0)
            m_pBot->m_bInBombZone = (ucEnable == 2);
         break;
      }
      break;

   case NETMSG_DEATH: // this message sends on death
      switch (GetState ())
      {
      case 0:
         iKillerIndex = *(int *) p;
         break;

      case 1:
         iVictimIndex = *(int *) p;
         break;

      case 2:
         if ((iKillerIndex != 0) && (iKillerIndex != iVictimIndex))
         {
            edict_t *pKiller = INDEXENT (iKillerIndex);
            edict_t *pVictim = INDEXENT (iVictimIndex);

            if (FNullEnt (pKiller) || FNullEnt (pVictim))
               break;

            // need to send congrats on well placed shot
            for (int i = 0; i < MaxClients (); i++)
            {
               CBot *pBot = g_pBotManager->GetBot (i);

               if ((pBot != NULL) && IsAlive (pBot->GetEntity ()) && (pKiller != pBot->GetEntity ()) && pBot->EntityIsVisible (pVictim->v.origin) && (GetTeam (pKiller) == GetTeam (pBot->GetEntity ())))
               {
                  if (pKiller == g_pentHostEdict)
                     pBot->HandleChatterMessage ("#Bot_NiceShotCommander");
                  else
                     pBot->HandleChatterMessage ("#Bot_NiceShotPall");

                  break;
               }
            }

            // notice nearby to victim teammates, that attacker is near
            for (int i = 0; i < MaxClients (); i++)
            {
               CBot *pBot = g_pBotManager->GetBot (i);

               if ((pBot != NULL) && IsAlive (pBot->GetEntity ()) && (GetTeam (pBot->GetEntity ()) == GetTeam (pVictim)) && IsVisible (pVictim->v.origin, pBot->GetEntity ()) && FNullEnt (pBot->m_pentEnemy))
               {
                  pBot->m_pentLastEnemy = pKiller;
                  pBot->m_vLastEnemyOrigin = pKiller->v.origin;
                  pBot->m_pentEnemy = pKiller;
               }
            }

            CBot *pBot = g_pBotManager->GetBot (pKiller);

            // is this message about a bot who killed somebody?
            if (pBot)
               pBot->m_pentLastVictim = pVictim;
            else // did a human kill a bot on his team?
            {
               CBot *pBot = g_pBotManager->GetBot (pVictim);

               if (pBot)
               {
                  if (GetTeam (pKiller) == GetTeam (pVictim))
                     pBot->m_iVoteKickIndex = iKillerIndex;

                  pBot->m_bNotKilled = false;
               }
            }
         }
         break;

      case 3:
         if (strcmp ((char *) p, "knife") && strcmp ((char *) p, "grenade") && g_rgpcvBotCVar[CVAR_KNIFEMODE]->GetBool ())
            CVarSetString (g_rgpcvBotCVar[CVAR_KNIFEMODE]->GetName (), "0"); // don't fooling bots :)
         break;
      }
      break;

   case NETMSG_SCREENFADE: // this message gets sent when the Screen fades (Flashbang)
      switch (GetState ())
      {
      case 3:
         ucR = *(byte *) p;
         break;

      case 4:
         ucG = *(byte *) p;
         break;

      case 5:
         ucB = *(byte *) p;
         break;

      case 6:
         if ((ucR == 255) && (ucG == 255) && (ucB == 255) && (*(byte *) p > 200))
         {
            m_pBot->m_pentEnemy = NULL;
            m_pBot->m_fMaxViewDistance = 1;

            // About 3 seconds
            m_pBot->m_fBlindTime = WorldTime () + ((float) * (byte *) p - 200.0) / 15;

            if (m_pBot->m_iSkill < 50)
            {
               m_pBot->m_fBlindMoveSpeed = 0.0;
               m_pBot->m_fBlindSidemoveSpeed = 0.0;
            }
            else if (m_pBot->m_iSkill < 80)
            {
               m_pBot->m_fBlindMoveSpeed = -m_pBot->pev->maxspeed;
               m_pBot->m_fBlindSidemoveSpeed = 0.0;
            }
            else
            {
               if (RandomLong (1, 100) < 50)
               {
                  if (RandomLong (1, 100) < 50)
                     m_pBot->m_fBlindSidemoveSpeed = m_pBot->pev->maxspeed;
                  else
                     m_pBot->m_fBlindSidemoveSpeed = -m_pBot->pev->maxspeed;
               }
               else
               {
                  if (m_pBot->pev->health > 80)
                     m_pBot->m_fBlindMoveSpeed = m_pBot->pev->maxspeed;
                  else
                     m_pBot->m_fBlindMoveSpeed = -m_pBot->pev->maxspeed;
               }
            }
         }
         break;
      }
      break;

   case NETMSG_HLTV: // round restart in cs 1.6
      switch (GetState ())
      {
      case 0:
         iNumPlayers = *(int *) p;
         break;

      case 1:
         if ((iNumPlayers == 0) && (*(int *) p == 0))
            RoundInit ();
         break;
      }
      break;

   case NETMSG_TEXTMSGALL:
      if (GetState () == 1)
      {
         if (FStrEq ((char *) p, "#CTs_Win") ||
            FStrEq ((char *) p, "#Bomb_Defused") || 
            FStrEq ((char *) p, "#Terrorists_Win") || 
            FStrEq ((char *) p, "#Round_Draw") || 
            FStrEq ((char *) p, "#All_Hostages_Rescued") || 
            FStrEq ((char *) p, "#Target_Saved") || 
            FStrEq ((char *) p, "#Hostages_Not_Rescued") || 
            FStrEq ((char *) p, "#Terrorists_Not_Escaped") || 
            FStrEq ((char *) p, "#VIP_Not_Escaped") || 
            FStrEq ((char *) p, "#Escaping_Terrorists_Neutralized") || 
            FStrEq ((char *) p, "#VIP_Assassinated") || 
            FStrEq ((char *) p, "#VIP_Escaped") || 
            FStrEq ((char *) p, "#Terrorists_Escaped") || 
            FStrEq ((char *) p, "#CTs_PreventEscape") || 
            FStrEq ((char *) p, "#Target_Bombed") || 
            FStrEq ((char *) p, "#Game_Commencing") || 
            FStrEq ((char *) p, "#Game_will_restart_in"))
         {
            g_bRoundEnded = true;
            g_bBombPlanted = false;
         }
        
         if (FStrEq ((char *) p, "#Game_Commencing"))
            g_bCommencing = true;
      }
      break;

   case NETMSG_SCOREINFO:
      switch (GetState ())
      {
      case 0:
         iPlayerIndex = *(int *) p;
         break;

      case 4:
         if ((iPlayerIndex >= 0) && (iPlayerIndex <= MaxClients ())) 
         {
            if (*(int *) p == 1)
               g_rgkClients[iPlayerIndex - 1].iTeam = TEAM_TERRORIST;
            else if (*(int *) p == 2)
               g_rgkClients[iPlayerIndex - 1].iTeam = TEAM_CT;
         }
         break;
      }
      break;

   case NETMSG_BARTIME:
      if (GetState () == 0)
      {
         if (*(int *) p > 0)
            m_pBot->m_bHasProgressBar = true; // the progress bar on a hud
         else if (*(int *) p == 0)
            m_pBot->m_bHasProgressBar = false; // no progress bar or disappeared
      }
      break;


   default:
      LogToFile (true, LOG_CRITICAL, "Network message handler error. Call of wrong message id (%d).\n", m_iMessage);
   }
   m_iState++; // and finally update network message state
}