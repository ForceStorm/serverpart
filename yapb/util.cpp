#include <Windows.h>

char *UnicodeToANSI( const wchar_t* str )
{
     static char result[1024];
     int textlen;
     textlen = WideCharToMultiByte( CP_ACP, 0, str, -1, NULL, 0, NULL, NULL );
     memset(result, 0, sizeof(char) * ( textlen + 1 ) );
     WideCharToMultiByte( CP_ACP, 0, str, -1, result, textlen, NULL, NULL );
     return result;
}
wchar_t *UTF8ToUnicode( const char* str )
{
     int textlen ;
     static wchar_t result[1024];
     textlen = MultiByteToWideChar( CP_UTF8, 0, str,-1, NULL,0 ); 
     memset(result, 0, sizeof(char) * ( textlen + 1 ) );
     MultiByteToWideChar(CP_UTF8, 0,str,-1,(LPWSTR)result,textlen ); 
     return result; 
}