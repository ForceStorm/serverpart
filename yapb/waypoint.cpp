//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#include "yapb.h"

void CWaypoint::Init (void)
{
   // initialize the waypoint structures..

   // have any waypoint path nodes been allocated yet?
   if (m_bWaypointPaths)
   {
      for (int i = 0; i < g_iNumWaypoints; i++)
      {
         delete m_pPaths[i];
         m_pPaths[i] = NULL;
      }
   }
   g_iNumWaypoints = 0;
   m_vLastWaypoint = NULLVEC;
}

void CWaypoint::AddPath (short int iAddIndex, short int sPathIndex, float fDistance)
{
   if ((iAddIndex < 0) || (iAddIndex >= g_iNumWaypoints) || (sPathIndex < 0) || (sPathIndex >= g_iNumWaypoints) || (iAddIndex == sPathIndex))
      return;

   tPath *pPath = m_pPaths[iAddIndex];
   int i;

   // Don't allow Paths get connected twice
   for (i = 0; i < MAX_PATH_INDEX; i++)
   {
      if (pPath->iIndex[i] == sPathIndex)
      {
         ServerPrint ("Denied path creation from %d to %d (path already exists)", iAddIndex, sPathIndex);
         return;
      }
   }

   // Check for free space in the connection indices
   for (i = 0; i < MAX_PATH_INDEX; i++)
   {
      if (pPath->iIndex[i] == -1)
      {
         pPath->iIndex[i] = sPathIndex;
         pPath->iDistance[i] = abs (fDistance);
         ServerPrint ("Path added from %d to %d", iAddIndex, sPathIndex);
         return;
      }
   }

   // There wasn't any free space. Try exchanging it with a long-distance path
   int iMaxDistance = -9999;
   int iSlot = -1;

   for (i = 0; i < MAX_PATH_INDEX; i++)
   {
      if (pPath->iDistance[i] > iMaxDistance)
      {
         iMaxDistance = pPath->iDistance[i];
         iSlot = i;
      }
   }

   if (iSlot != -1)
   {
      ServerPrint ("Path added from %d to %d", iAddIndex, sPathIndex);
      pPath->iIndex[iSlot] = sPathIndex;
      pPath->iDistance[iSlot] = abs (fDistance);
   }
}

int CWaypoint::FindFarest (Vector vOrigin, float fMaxDistance)
{
   // find the farest waypoint to that Origin, and return the index to this waypoint

   int iIndex = -1;

   for (int i = 0; i < g_iNumWaypoints; i++)
   {
      float fDistance = (m_pPaths[i]->vOrigin - vOrigin).Length ();

      if (fDistance > fMaxDistance)
      {
         iIndex = i;
         fMaxDistance = fDistance;
      }
   }
   return iIndex;
}

int CWaypoint::FindNearest (Vector vOrigin, float fMinDistance, int iFlags)
{
   // find the nearest waypoint to that Origin and return the index

   int iIndex = -1;

   for (int i = 0; i < g_iNumWaypoints; i++)
   {
      float fDistance = (m_pPaths[i]->vOrigin - vOrigin).Length ();

      if ((iFlags != -1) && ((m_pPaths[i]->iFlags & iFlags) == NULL))
         continue; // if flag not -1 and waypoint has no this flag, skip waypoint

      if (fDistance < fMinDistance)
      {
         iIndex = i;
         fMinDistance = fDistance;
      }
   }
   return iIndex;
}

void CWaypoint::FindInRadius (Vector vPos, float fRadius, int *pTab, int *iCount)
{
   // Returns all Waypoints within Radius from Position

   int iMaxCount = *iCount;
   *iCount = 0;

   for (int i = 0; i < g_iNumWaypoints; i++)
   {
      if ((m_pPaths[i]->vOrigin - vPos).Length () < fRadius)
      {
         *pTab++ = i;
         *iCount += 1;

         if (*iCount >= iMaxCount)
            break;
      }
   }
   *iCount -= 1;
}

void CWaypoint::FindInRadius (vector <int> &kId, float fRadius, Vector vPos)
{
   for (int i = 0; i < g_iNumWaypoints; i++)
   {
      if ((m_pPaths[i]->vOrigin - vPos).Length () <= fRadius)
         kId.AddItem (i);
   }
}

void CWaypoint::Add (int iFlags, Vector vWptOrigin)
{
   if (FNullEnt (g_pentHostEdict))
      return;

   int iIndex = -1, i;
   float fDistance;

   Vector vForward = NULLVEC;
   tPath *pPath = NULL;

   bool bPlaceNew = true;
   Vector vNewWaypoint;
   
   if (vWptOrigin == NULLVEC)
      vNewWaypoint = g_pentHostEdict->v.origin;
   else
      vNewWaypoint = vWptOrigin;

   if (g_pBotManager->NumBots () > 0)
      g_pBotManager->RemoveAll ();

   g_bWaypointsChanged = true;

   switch (iFlags)
   {
   case 6:
      iIndex = FindNearest (g_pentHostEdict->v.origin, 50.0);
      if (iIndex != -1)
      {
         pPath = m_pPaths[iIndex];

         if (!(pPath->iFlags & W_FL_CAMP))
         {
            CenterPrint ("This is not Camping Waypoint");
            return;
         }

         MakeVectors (g_pentHostEdict->v.v_angle);
         vForward = g_pentHostEdict->v.origin + g_pentHostEdict->v.view_ofs + g_pGlobals->v_forward * 640;
         
         pPath->fCampEndX = vForward.x;
         pPath->fCampEndY = vForward.y;

         // play "done" sound...
         PlaySound (g_pentHostEdict, "common/wpn_hudon.wav");
      }
      return;

   case 9:
      iIndex = FindNearest (g_pentHostEdict->v.origin, 50.0);
      
      if (iIndex != -1)
      {
         fDistance = (m_pPaths[iIndex]->vOrigin - g_pentHostEdict->v.origin).Length ();
         
         if (fDistance < 50)
         {
            bPlaceNew = false;
            pPath = m_pPaths[iIndex];

            if (iFlags == 9)
               pPath->vOrigin = (pPath->vOrigin + m_vLearnPos) / 2;
         }
      }
      else
         vNewWaypoint = m_vLearnPos;
      break;

   case 10:
      iIndex = FindNearest (g_pentHostEdict->v.origin, 50.0);

      if (iIndex != -1)
      {
         fDistance = (m_pPaths[iIndex]->vOrigin - g_pentHostEdict->v.origin).Length ();
         
         if (fDistance < 50)
         {
            bPlaceNew = false;
            pPath = m_pPaths[iIndex];

            int iFlags = 0;

            for (i = 0; i < MAX_PATH_INDEX; i++)
               iFlags += pPath->uConnectFlag[i];

            if (iFlags == 0)
               pPath->vOrigin = (pPath->vOrigin + g_pentHostEdict->v.origin) / 2;
         }
      }
      break;
   }

   if (bPlaceNew)
   {
      if (g_iNumWaypoints >= MAX_WAYPOINTS)
         return;

      iIndex = g_iNumWaypoints;

      m_pPaths[iIndex] = AllocMem <tPath> ();

      if (m_pPaths[iIndex] == NULL)
         TerminateOnMalloc ();

      pPath = m_pPaths[iIndex];

      // increment total number of waypoints
      g_iNumWaypoints++;
      pPath->iPathNumber = iIndex;
      pPath->iFlags = 0;

      // store the origin (location) of this waypoint
      pPath->vOrigin = vNewWaypoint;

      pPath->fCampEndX = 0;
      pPath->fCampEndY = 0;
      pPath->fCampStartX = 0;
      pPath->fCampStartY = 0;

      for (i = 0; i < MAX_PATH_INDEX; i++)
      {
         pPath->iIndex[i] = -1;
         pPath->iDistance[i] = 0;

         pPath->uConnectFlag[i] = 0;
         pPath->vConnectVel[i] = NULLVEC;
      }
      // store the last used waypoint for the auto waypoint code...
      m_vLastWaypoint = g_pentHostEdict->v.origin;
   }

   // set the time that this waypoint was originally displayed...
   m_rgfWPDisplayTime[iIndex] = 0;

   if (iFlags == 9)
      m_iLastJumpWaypoint = iIndex;
   else if (iFlags == 10)
   {
      fDistance = (m_pPaths[m_iLastJumpWaypoint]->vOrigin - g_pentHostEdict->v.origin).Length ();
      AddPath (m_iLastJumpWaypoint, iIndex, fDistance);

      for (i = 0; i < MAX_PATH_INDEX; i++)
      {
         if (m_pPaths[m_iLastJumpWaypoint]->iIndex[i] == iIndex)
         {
            m_pPaths[m_iLastJumpWaypoint]->uConnectFlag[i] |= C_FL_JUMP;
            m_pPaths[m_iLastJumpWaypoint]->vConnectVel[i] = m_vLearnVelocity;

            break;
         }
      }

      CalculateWayzone (iIndex);
      return;
   }

   if (g_pentHostEdict->v.flags & FL_DUCKING)
      pPath->iFlags |= W_FL_CROUCH;  // set a crouch waypoint

   if (g_pentHostEdict->v.movetype == MOVETYPE_FLY)
   {
      pPath->iFlags |= W_FL_LADDER;
      MakeVectors (g_pentHostEdict->v.v_angle);
      
      vForward = g_pentHostEdict->v.origin + g_pentHostEdict->v.view_ofs + g_pGlobals->v_forward * 640;
      pPath->fCampStartY = vForward.y;
   }
   else if (m_bOnLadder)
      pPath->iFlags |= W_FL_LADDER;

   switch (iFlags)
   {
   case 1:
      pPath->iFlags |= W_FL_CROSSING;
      pPath->iFlags |= W_FL_TERRORIST;
      break;

   case 2:
      pPath->iFlags |= W_FL_CROSSING;
      pPath->iFlags |= W_FL_COUNTER;
      break;

   case 3:
      pPath->iFlags |= W_FL_NOHOSTAGE;
      break;

   case 4:
      pPath->iFlags |= W_FL_RESCUE;
      break;

   case 5:
      pPath->iFlags |= W_FL_CROSSING;
      pPath->iFlags |= W_FL_CAMP;

      MakeVectors (g_pentHostEdict->v.v_angle);
      vForward = g_pentHostEdict->v.origin + g_pentHostEdict->v.view_ofs + g_pGlobals->v_forward * 640;
      
      pPath->fCampStartX = vForward.x;
      pPath->fCampStartY = vForward.y;
      break;

   case 100:
      pPath->iFlags |= W_FL_GOAL;
      break;
   }

   // Ladder waypoints need careful connections
   if (pPath->iFlags & W_FL_LADDER)
   {
      float fMinDistance = 9999.0;
      int iDestIndex = -1;

      TraceResult tr;

      // calculate all the paths to this new waypoint
      for (i = 0; i < g_iNumWaypoints; i++)
      {
         if (i == iIndex)
            continue; // skip the waypoint that was just added

         // Other ladder waypoints should connect to this
         if (m_pPaths[i]->iFlags & W_FL_LADDER)
         {
            // check if the waypoint is reachable from the new one
            TraceLine (vNewWaypoint, m_pPaths[i]->vOrigin, true, g_pentHostEdict, &tr);
            
            if ((tr.flFraction == 1.0) && (fabs (vNewWaypoint.x - m_pPaths[i]->vOrigin.x) < 64) && (fabs (vNewWaypoint.y - m_pPaths[i]->vOrigin.y) < 64) && (fabs (vNewWaypoint.z - m_pPaths[i]->vOrigin.z) < g_fAutoPathDistance))
            {
               fDistance = (m_pPaths[i]->vOrigin - vNewWaypoint).Length ();

               AddPath (iIndex, i, fDistance);
               AddPath (i, iIndex, fDistance);
            }
         }
         else
         {
            // check if the waypoint is reachable from the new one
            if (IsNodeReachable (vNewWaypoint, m_pPaths[i]->vOrigin) || IsNodeReachable (m_pPaths[i]->vOrigin, vNewWaypoint))
            {
               fDistance = (m_pPaths[i]->vOrigin - vNewWaypoint).Length ();

               if (fDistance < fMinDistance)
               {
                  iDestIndex = i;
                  fMinDistance = fDistance;
               }
            }
         }
      }

      if ((iDestIndex > -1) && (iDestIndex < g_iNumWaypoints))
      {
         // check if the waypoint is reachable from the new one (one-way)
         if (IsNodeReachable (vNewWaypoint, m_pPaths[iDestIndex]->vOrigin))
         {
            fDistance = (m_pPaths[iDestIndex]->vOrigin - vNewWaypoint).Length ();
            AddPath (iIndex, iDestIndex, fDistance);
         }

         // check if the new one is reachable from the waypoint (other way)
         if (IsNodeReachable (m_pPaths[iDestIndex]->vOrigin, vNewWaypoint))
         {
            fDistance = (m_pPaths[iDestIndex]->vOrigin - vNewWaypoint).Length ();
            AddPath (iDestIndex, iIndex, fDistance);
         }
      }
   }
   else
   {
      // calculate all the paths to this new waypoint
      for (i = 0; i < g_iNumWaypoints; i++)
      {
         if (i == iIndex)
            continue; // skip the waypoint that was just added

         // check if the waypoint is reachable from the new one (one-way)
         if (IsNodeReachable (vNewWaypoint, m_pPaths[i]->vOrigin))
         {
            fDistance = (m_pPaths[i]->vOrigin - vNewWaypoint).Length ();
            AddPath (iIndex, i, fDistance);
         }

         // check if the new one is reachable from the waypoint (other way)
         if (IsNodeReachable (m_pPaths[i]->vOrigin, vNewWaypoint))
         {
            fDistance = (m_pPaths[i]->vOrigin - vNewWaypoint).Length ();
            AddPath (i, iIndex, fDistance);
         }
      }
   } 
   PlaySound (g_pentHostEdict, "weapons/xbow_hit1.wav");
   CalculateWayzone (iIndex); // calculate the wayzone of this waypoint
}

void CWaypoint::Delete (void)
{
   g_bWaypointsChanged = true;

   if (g_iNumWaypoints < 1)
      return;

   if (g_pBotManager->NumBots () > 0)
      g_pBotManager->RemoveAll ();

   int iIndex = FindNearest (g_pentHostEdict->v.origin, 50.0);

   if (iIndex == -1)
      return;

   tPath *pPath = NULL;
   InternalAssert (m_pPaths[iIndex] != NULL);

   int i, j;

   for (i = 0; i < g_iNumWaypoints; i++) // delete all references to Node
   {
      pPath = m_pPaths[i];

      for (j = 0; j < MAX_PATH_INDEX; j++)
      {
         if (pPath->iIndex[j] == iIndex)
         {
            pPath->iIndex[j] = -1;  // unassign this path
            pPath->uConnectFlag[j] = 0;
            pPath->iDistance[j] = 0;
            pPath->vConnectVel[j] = NULLVEC;
         }
      }
   }

   for (i = 0; i < g_iNumWaypoints; i++)
   {
      pPath = m_pPaths[i];

      if (pPath->iPathNumber > iIndex) // if Pathnumber bigger than deleted Node...
         pPath->iPathNumber--;

      for (j = 0; j < MAX_PATH_INDEX; j++)
      {
         if (pPath->iIndex[j] > iIndex)
            pPath->iIndex[j]--;
      }
   }

   // free deleted node
   delete m_pPaths[iIndex];
   m_pPaths[iIndex] = NULL;

   // Rotate Path Array down
   for (i = iIndex; i < g_iNumWaypoints - 1; i++)
      m_pPaths[i] = m_pPaths[i + 1];

   g_iNumWaypoints--;
   m_rgfWPDisplayTime[iIndex] = 0;

   PlaySound (g_pentHostEdict, "weapons/mine_activate.wav");
}

void CWaypoint::ChangeFlags (int iFlag, char iMode)
{
   // Allow manually changing Flags

   int iIndex = FindNearest (g_pentHostEdict->v.origin, 50.0);

   if (iIndex != -1)
   {
      // Delete Flag
      if (iMode == 0)
         m_pPaths[iIndex]->iFlags &= ~iFlag;
      else
         m_pPaths[iIndex]->iFlags |= iFlag;

      // play "done" sound...
      PlaySound (g_pentHostEdict, "common/wpn_hudon.wav");
   }
   return;
}

void CWaypoint::SetRadius (int iRadius)
{
   // Allow manually setting the Zone Radius

   int iIndex = FindNearest (g_pentHostEdict->v.origin, 50.0);

   if (iIndex != -1)
   {
      m_pPaths[iIndex]->fRadius = static_cast <float> (iRadius);

      // play "done" sound...
      PlaySound (g_pentHostEdict, "common/wpn_hudon.wav");
   }
}

bool CWaypoint::IsConnected (int iA, int iB)
{
   // Checks if Waypoint A has a Connection to Waypoint Nr. B

   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      if (m_pPaths[iA]->iIndex[i] == iB)
         return true;
   }
   return false;
}

void CWaypoint::CreatePath (char cDirection)
{
   // this function allow player to manually create a path from one waypoint to another
   
   float fBestAngle = 5, fBestAngleTemp;
   Vector vWaypointBound;

   int iNodeFrom = FindNearest (g_pentHostEdict->v.origin, 50.0);
   int iNodeTo = -1;

   if (iNodeFrom == -1)
   {
      CenterPrint ("Unable to find nearest waypoint in 50 units");
      return;
   }

   // find the waypoint the user is pointing at
   for (int iIndex = 0; iIndex < g_iNumWaypoints; iIndex++)
   {
      Vector vToWaypoint = g_pWaypoint->GetPath (iIndex)->vOrigin - g_pentHostEdict->v.origin;
      Vector vWaypointAngles = ClampAngles (VecToAngles (vToWaypoint) - g_pentHostEdict->v.v_angle);
      
      if ((vToWaypoint.Length () > 500) || (fabs (vWaypointAngles.y) > fBestAngle))
         continue;

      fBestAngleTemp = vWaypointAngles.y;

      if (g_pWaypoint->GetPath (iIndex)->iFlags & W_FL_CROUCH)
         vWaypointBound = g_pWaypoint->GetPath (iIndex)->vOrigin - Vector (0, 0, 17);
      else
         vWaypointBound = g_pWaypoint->GetPath (iIndex)->vOrigin - Vector (0, 0, 34);

      vWaypointAngles = -g_pentHostEdict->v.v_angle;
      vWaypointAngles.x = -vWaypointAngles.x;
      vWaypointAngles = ClampAngles (vWaypointAngles + VecToAngles (vWaypointBound - g_pentHostEdict->v.origin + g_pentHostEdict->v.view_ofs));
 
      if (vWaypointAngles.x > 0)
         continue;

      if (g_pWaypoint->GetPath (iIndex)->iFlags & W_FL_CROUCH)
         vWaypointBound = g_pWaypoint->GetPath (iIndex)->vOrigin + Vector (0, 0, 17);
      else
         vWaypointBound = g_pWaypoint->GetPath (iIndex)->vOrigin + Vector (0, 0, 34);

      vWaypointAngles = -g_pentHostEdict->v.v_angle;
      vWaypointAngles.x = -vWaypointAngles.x;
      vWaypointAngles = ClampAngles (vWaypointAngles + VecToAngles (vWaypointBound - g_pentHostEdict->v.origin + g_pentHostEdict->v.view_ofs));

      if (vWaypointAngles.x < 0)
         continue;

      fBestAngle = fBestAngleTemp;
      iNodeTo = iIndex;
   }

   if ((iNodeTo < 0) || (iNodeTo >= g_iNumWaypoints))
   {
      if ((m_iCacheWaypointIndex >= 0) && (m_iCacheWaypointIndex < g_iNumWaypoints))
         iNodeTo = m_iCacheWaypointIndex;
      else
      {
         CenterPrint ("Unable to find destination waypoint");
         return;
      }
   }

   if (iNodeTo == iNodeFrom)
   {
      CenterPrint ("Unable to connect waypoint with itself");
      return;
   }

   float fDistance = (g_pWaypoint->GetPath (iNodeTo)->vOrigin - g_pWaypoint->GetPath (iNodeFrom)->vOrigin).Length ();

   if (cDirection == PATH_OUTGOING)
      AddPath (iNodeFrom, iNodeTo, fDistance);
   else if (cDirection == PATH_INCOMING)
      AddPath (iNodeTo, iNodeFrom, fDistance);
   else
   {
      AddPath (iNodeFrom, iNodeTo, fDistance);
      AddPath (iNodeTo, iNodeFrom, fDistance);
   }

   PlaySound (g_pentHostEdict, "common/wpn_hudon.wav");
   g_bWaypointsChanged = true;
}


void CWaypoint::DeletePath (void)
{
   // this function allow player to manually remove a path from one waypoint to another
   
   Vector vWaypointBound;
   float fBestAngle = 5, fBestAngleTemp;

   int iNodeFrom = FindNearest (g_pentHostEdict->v.origin, 50.0);
   int iNodeTo = -1;
   int iIndex = 0;

   if (iNodeFrom == -1)
   {
      CenterPrint ("Unable to find nearest waypoint in 50 units");
      return;
   }

   // find the waypoint the user is pointing at
   for (iIndex = 0; iIndex < g_iNumWaypoints; iIndex++)
   {
      Vector vToWaypoint = g_pWaypoint->GetPath (iIndex)->vOrigin - g_pentHostEdict->v.origin;
      Vector vWaypointAngles = ClampAngles (VecToAngles (vToWaypoint) - g_pentHostEdict->v.v_angle);

      if ((vToWaypoint.Length () > 500) || (fabs (vWaypointAngles.y) > fBestAngle))
         continue;

      fBestAngleTemp = vWaypointAngles.y;

      if (g_pWaypoint->GetPath (iIndex)->iFlags & W_FL_CROUCH)
         vWaypointBound = g_pWaypoint->GetPath (iIndex)->vOrigin - Vector (0, 0, 17);
      else
         vWaypointBound = g_pWaypoint->GetPath (iIndex)->vOrigin - Vector (0, 0, 34);

      vWaypointAngles = -g_pentHostEdict->v.v_angle;
      vWaypointAngles.x = -vWaypointAngles.x;
      vWaypointAngles = ClampAngles (vWaypointAngles + VecToAngles (vWaypointBound - g_pentHostEdict->v.origin + g_pentHostEdict->v.view_ofs));

      if (vWaypointAngles.x > 0)
         continue;

      if (g_pWaypoint->GetPath (iIndex)->iFlags & W_FL_CROUCH)
         vWaypointBound = g_pWaypoint->GetPath (iIndex)->vOrigin + Vector (0, 0, 17);
      else
         vWaypointBound = g_pWaypoint->GetPath (iIndex)->vOrigin + Vector (0, 0, 34);

      vWaypointAngles = -g_pentHostEdict->v.v_angle;
      vWaypointAngles.x = -vWaypointAngles.x;
      vWaypointAngles = ClampAngles (vWaypointAngles + VecToAngles (vWaypointBound - g_pentHostEdict->v.origin + g_pentHostEdict->v.view_ofs));

      if (vWaypointAngles.x < 0)
         continue;

      fBestAngle = fBestAngleTemp;
      iNodeTo = iIndex;
   }

   if ((iNodeTo < 0) || (iNodeTo >= g_iNumWaypoints))
   {
      if ((m_iCacheWaypointIndex >= 0) && (m_iCacheWaypointIndex < g_iNumWaypoints))
         iNodeTo = m_iCacheWaypointIndex;
      else
      {
         CenterPrint ("Unable to find destination waypoint");
         return;
      }
   }

   for (iIndex = 0; iIndex < MAX_PATH_INDEX; iIndex++)
   {
      if (g_pWaypoint->GetPath (iNodeFrom)->iIndex[iIndex] == iNodeTo)
      {
         g_pWaypoint->GetPath (iNodeFrom)->iIndex[iIndex] = -1; // unassign this path
         g_pWaypoint->GetPath (iNodeFrom)->uConnectFlag[iIndex] = 0;
         g_pWaypoint->GetPath (iNodeFrom)->vConnectVel[iIndex] = NULLVEC;
         g_pWaypoint->GetPath (iNodeFrom)->iDistance[iIndex] = 0;

         PlaySound (g_pentHostEdict, "weapons/mine_activate.wav");
         g_bWaypointsChanged = true;

         return;
      }
   }

   // not found this way ? check for incoming connections then
   iIndex = iNodeFrom;
   iNodeFrom = iNodeTo;
   iNodeTo = iIndex;

   for (iIndex = 0; iIndex < MAX_PATH_INDEX; iIndex++)
   {
      if (g_pWaypoint->GetPath (iNodeFrom)->iIndex[iIndex] == iNodeTo)
      {
         g_pWaypoint->GetPath (iNodeFrom)->iIndex[iIndex] = -1; // unassign this path
         g_pWaypoint->GetPath (iNodeFrom)->uConnectFlag[iIndex] = 0;
         g_pWaypoint->GetPath (iNodeFrom)->vConnectVel[iIndex] = NULLVEC;
         g_pWaypoint->GetPath (iNodeFrom)->iDistance[iIndex] = 0;

         PlaySound (g_pentHostEdict, "weapons/mine_activate.wav");
         g_bWaypointsChanged = true;

         return;
      }
   }
   CenterPrint ("There is already no path on this waypoint");
}

void CWaypoint::CacheWaypoint (void)
{
   int iNode = FindNearest (g_pentHostEdict->v.origin, 50.0);

   if (iNode == -1)
   {
      m_iCacheWaypointIndex = -1;
      CenterPrint ("Cached waypoint cleared (nearby point not found in 50 units range)");

      return;
   }
   m_iCacheWaypointIndex = iNode;
   CenterPrint ("Waypoint #%d has been put into memory", m_iCacheWaypointIndex);
}

void CWaypoint::CalculateWayzone (int iIndex)
{
   // Calculate "Wayzones" for the nearest waypoint to pentEdict (meaning a dynamic distance area to vary waypoint origin)

   tPath *pPath = m_pPaths[iIndex];
   Vector vStart, vDirection;

   TraceResult tr;
   bool bWayBlocked = false;

   if ((pPath->iFlags & (W_FL_LADDER | W_FL_GOAL | W_FL_CAMP | W_FL_RESCUE | W_FL_CROUCH)) || m_bLearnJumpWaypoint)
   {
      pPath->fRadius = 0;
      return;
   }

   for (int i = 0; i < MAX_PATH_INDEX; i++)
   {
      if ((pPath->iIndex[i] != -1) && (m_pPaths[pPath->iIndex[i]]->iFlags & W_FL_LADDER))
      {
         pPath->fRadius = 0;
         return;
      }
   }

   for (int iScanDistance = 16; iScanDistance < 128; iScanDistance += 16)
   {
      vStart = pPath->vOrigin;
      MakeVectors (NULLVEC);
      
      vDirection = g_pGlobals->v_forward * iScanDistance;
      vDirection = VecToAngles (vDirection);
      
      pPath->fRadius = iScanDistance;

      for (float fRadCircle = 0.0; fRadCircle < 180.0; fRadCircle += 5)
      {
         MakeVectors (vDirection);
         
         Vector vRadiusStart = vStart - g_pGlobals->v_forward * iScanDistance;
         Vector vRadiusEnd = vStart + g_pGlobals->v_forward * iScanDistance;

         TraceHull (vRadiusStart, vRadiusEnd, true, head_hull, NULL, &tr);

         if (tr.flFraction < 1.0)
         {
            TraceLine (vRadiusStart, vRadiusEnd, true, NULL, &tr);
            
            if (FClassnameIs (tr.pHit, "func_door") || FClassnameIs (tr.pHit, "func_door_rotating"))
            {
               pPath->fRadius = 0;
               bWayBlocked = true;

               break;
            }

            bWayBlocked = true;
            pPath->fRadius -= 16;

            break;
         }

         Vector vDropStart = vStart + (g_pGlobals->v_forward * iScanDistance);
         Vector vDropEnd = vDropStart - Vector (0, 0, iScanDistance + 60);
         TraceHull (vDropStart, vDropEnd, true, head_hull, NULL, &tr);

         if (tr.flFraction >= 1.0)
         {
            bWayBlocked = true;
            pPath->fRadius -= 16;

            break;
         }

         vDropStart = vStart - (g_pGlobals->v_forward * iScanDistance);
         vDropEnd = vDropStart - Vector (0, 0, iScanDistance + 60);
         TraceHull (vDropStart, vDropEnd, true, head_hull, NULL, &tr);

         if (tr.flFraction >= 1.0)
         {
            bWayBlocked = true;
            pPath->fRadius -= 16;
            break;
         }

         vRadiusEnd.z += 34;
         TraceHull (vRadiusStart, vRadiusEnd, true, head_hull, NULL, &tr);

         if (tr.flFraction < 1.0)
         {
            bWayBlocked = true;
            pPath->fRadius -= 16;
            break;
         }

         vDirection.y = AngleNormalize (vDirection.y + fRadCircle);
      }
      if (bWayBlocked)
         break;
   }
   pPath->fRadius -= 16;

   if (pPath->fRadius < 0)
      pPath->fRadius = 0;
}

void CWaypoint::SaveExperienceTab (void)
{
   tExperienceHeader kHeader;

   if ((g_iNumWaypoints <= 0) || g_bWaypointsChanged)
      return;

   memset (kHeader.szHeader, 0, sizeof (kHeader.szHeader));
   strcpy (kHeader.szHeader, EXP_HEADER);

   kHeader.iFileVersion = EXPERIENCE_VERSION;
   kHeader.iWpNumber = g_iNumWaypoints;
   
   tExperienceSave *pExperienceSave = AllocMem <tExperienceSave> (g_iNumWaypoints * g_iNumWaypoints);

   if (pExperienceSave == NULL)
      TerminateOnMalloc ();

   for (int i = 0; i < g_iNumWaypoints; i++)
   {
      for (int j = 0; j < g_iNumWaypoints; j++)
      {
         (pExperienceSave + (i * g_iNumWaypoints) + j)->uTeam0Damage = (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam0Damage >> 3;
         (pExperienceSave + (i * g_iNumWaypoints) + j)->uTeam1Damage = (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam1Damage >> 3;
         (pExperienceSave + (i * g_iNumWaypoints) + j)->cTeam0Value = (g_pExperienceData + (i * g_iNumWaypoints) + j)->wTeam0Value / 8;
         (pExperienceSave + (i * g_iNumWaypoints) + j)->cTeam1Value = (g_pExperienceData + (i * g_iNumWaypoints) + j)->wTeam1Value / 8;
      }
   }

   int iResult = CCompress::Compress (VarArgs ("%sdata/%s.exp", GetWptDir (), GetMapName ()), (unsigned char *)&kHeader, sizeof (tExperienceHeader), (unsigned char *)pExperienceSave, g_iNumWaypoints * g_iNumWaypoints * sizeof (tExperienceSave));

   delete [] pExperienceSave;

   if (iResult == -1)
   {
      ServerPrint ("Couldn't save experience data");
      return;
   }
}

void CWaypoint::InitExperienceTab (void)
{
   tExperienceHeader kHeader;
   int i, j;

   if (g_pExperienceData)
      delete [] g_pExperienceData;
   g_pExperienceData = NULL;

   if (g_iNumWaypoints < 1)
      return;

   g_pExperienceData = AllocMem <tExperience> (g_iNumWaypoints * g_iNumWaypoints);

   if (g_pExperienceData == NULL)
      TerminateOnMalloc ();

   // initialize table by hand to correct values, and NOT zero it out
   for (i = 0; i < g_iNumWaypoints; i++)
   {
      for (j = 0; j < g_iNumWaypoints; j++)
      {
         (g_pExperienceData + (i * g_iNumWaypoints) + j)->iTeam0DangerIndex = -1;
         (g_pExperienceData + (i * g_iNumWaypoints) + j)->iTeam1DangerIndex = -1;
         (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam0Damage = 0;
         (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam1Damage = 0;
         (g_pExperienceData + (i * g_iNumWaypoints) + j)->wTeam0Value = 0;
         (g_pExperienceData + (i * g_iNumWaypoints) + j)->wTeam1Value = 0;
      }
   }
   stdfile fp (VarArgs ("%sdata/%s.exp", GetWptDir (), GetMapName ()), "rb");

   // if file exists, read the experience data from it
   if (fp.IsValid ())
   {
      fp.Read (&kHeader, sizeof (tExperienceHeader));
      fp.Close ();

      if (strncmp (kHeader.szHeader, EXP_HEADER, strlen (EXP_HEADER)) == 0)
      {
         if ((kHeader.iFileVersion == EXPERIENCE_VERSION) && (kHeader.iWpNumber == g_iNumWaypoints))
         {
            tExperienceSave *pExperienceLoad = AllocMem <tExperienceSave> (g_iNumWaypoints * g_iNumWaypoints);
            
            if (pExperienceLoad == NULL)
            {
               ServerPrint ("Couldn't allocate memory for experience data");
               return;
            }

            CCompress::Uncompress (VarArgs ("%sdata/%s.exp", GetWptDir (), GetMapName ()), sizeof (tExperienceHeader), (unsigned char *)pExperienceLoad, g_iNumWaypoints * g_iNumWaypoints * sizeof (tExperienceSave));

            for (i = 0; i < g_iNumWaypoints; i++)
            {
               for (j = 0; j < g_iNumWaypoints; j++)
               {
                  if (i == j)
                  {
                     (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam0Damage = (unsigned short) ((pExperienceLoad + (i * g_iNumWaypoints) + j)->uTeam0Damage);
                     (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam1Damage = (unsigned short) ((pExperienceLoad + (i * g_iNumWaypoints) + j)->uTeam1Damage);
                  }
                  else
                  {
                     (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam0Damage = (unsigned short) ((pExperienceLoad + (i * g_iNumWaypoints) + j)->uTeam0Damage) << 3;
                     (g_pExperienceData + (i * g_iNumWaypoints) + j)->uTeam1Damage = (unsigned short) ((pExperienceLoad + (i * g_iNumWaypoints) + j)->uTeam1Damage) << 3;
                  }

                  (g_pExperienceData + (i * g_iNumWaypoints) + j)->wTeam0Value = (signed short) ((pExperienceLoad + i * (g_iNumWaypoints) + j)->cTeam0Value) * 8;
                  (g_pExperienceData + (i * g_iNumWaypoints) + j)->wTeam1Value = (signed short) ((pExperienceLoad + i * (g_iNumWaypoints) + j)->cTeam1Value) * 8;
               }
            }
            delete [] pExperienceLoad;
         }
         else
            LogToFile (true, LOG_ERROR, "Experience data damaged (wrong version, or not for this map)");
      }
   }
}

void CWaypoint::SaveVisibilityTab (void)
{
   if (g_iNumWaypoints == 0)
      return;

   if (m_rgbyVisLUT == NULL)
      LogToFile (true, LOG_CRITICAL, "Can't save visiblity tab. Bad data.");

   tVistableHeader kHeader;

   // parse header
   memset (kHeader.szHeader, 0, sizeof (kHeader.szHeader));
   strcpy (kHeader.szHeader, VIS_HEADER);

   kHeader.iFileVersion = VISTABLE_VERSION;
   kHeader.iWpNumber = g_iNumWaypoints;

   stdfile fp (VarArgs ("%sdata/%s.vis", GetWptDir (), GetMapName ()), "wb");

   if (!fp.IsValid ())
   {
      LogToFile (true, LOG_ERROR, "Failed to open visiblity table for writing");
      return;
   }
   fp.Close ();

   CCompress::Compress (VarArgs ("%sdata/%s.vis", GetWptDir (), GetMapName ()), (unsigned char *) &kHeader, sizeof (tVistableHeader), (unsigned char *) m_rgbyVisLUT, MAX_WAYPOINTS * (MAX_WAYPOINTS / 4) * sizeof (byte));
}

void CWaypoint::InitVisibilityTab (void)
{
   if (g_iNumWaypoints == 0)
      return;

   tVistableHeader kHeader;

   stdfile fp (VarArgs ("%sdata/%s.vis", GetWptDir (), GetMapName ()), "rb");
   m_bRecalcVis = false;

   if (!fp.IsValid ())
   {
      m_iCurrVisIndex = 0;
      m_bRecalcVis = true;

      ServerPrint ("Vistable, not exists, vistable will be rebuilded");
      return;
   }

   // read the header of the file
   fp.Read (&kHeader, sizeof (tVistableHeader));

   if ((strncmp (kHeader.szHeader, VIS_HEADER, strlen (VIS_HEADER)) != 0) || (kHeader.iFileVersion != VISTABLE_VERSION) || (kHeader.iWpNumber != g_iNumWaypoints))
   {
      m_iCurrVisIndex = 0;
      m_bRecalcVis = true;

      LogToFile (true, LOG_WARNING, "Vistable damaged (wrong version, or not for this map), vistable will be rebuilded.");
      fp.Close ();

      return;      
   }

   int iResult = CCompress::Uncompress (VarArgs ("%sdata/%s.vis", GetWptDir (), GetMapName ()), sizeof (tVistableHeader), (unsigned char *) m_rgbyVisLUT, MAX_WAYPOINTS * (MAX_WAYPOINTS / 4) * sizeof (byte));

   if (iResult == -1)
   {
      m_iCurrVisIndex = 0;
      m_bRecalcVis = true;

      LogToFile (true, LOG_ERROR, "Failed to decode vistable, vistable will be rebuilded.");
      fp.Close ();

      return;      
   }
   fp.Close ();
}

void CWaypoint::InitTypes (void)
{
   m_arTerrorPoints.Cleanup ();
   m_arCTPoints.Cleanup ();
   m_arGoalPoints.Cleanup ();
   m_arCampPoints.Cleanup ();
   m_arRescuePoints.Cleanup ();

   for (int i = 0; i < g_iNumWaypoints; i++)
   {
      if (m_pPaths[i]->iFlags & W_FL_TERRORIST)
         m_arTerrorPoints.AddItem (i);
      else if (m_pPaths[i]->iFlags & W_FL_COUNTER)
         m_arCTPoints.AddItem (i);
      else if (m_pPaths[i]->iFlags & W_FL_GOAL)
         m_arGoalPoints.AddItem (i);
      else if (m_pPaths[i]->iFlags & W_FL_CAMP)
         m_arCampPoints.AddItem (i);
      else if (m_pPaths[i]->iFlags & W_FL_RESCUE)
         m_arRescuePoints.AddItem (i);
   }
}

bool CWaypoint::Load (void)
{
   tWaypointHeader kHeader;
   int i;

   m_fPathTime = 0.0;
   m_fDangerTime = 0.0;

   stdfile fp (VarArgs ("%s%s.pwf", GetWptDir (), GetMapName ()), "rb");

   if (fp.IsValid ())
   {
      fp.Read (&kHeader, sizeof (kHeader));

      if (strncmp (kHeader.szHeader, WAY_HEADER, strlen (WAY_HEADER)) == 0)
      {
         if (kHeader.iFileVersion != WAYPOINT_VERSION)
         {
            sprintf (m_szInfo, "%s.pwf - incorrect waypoint file version (expected '%d' found '%d')", GetMapName (), WAYPOINT_VERSION, kHeader.iFileVersion);
            LogToFile (true, LOG_ERROR, m_szInfo);

            fp.Close ();
            return false;
         }
         else if (stricmp (kHeader.szMapname, GetMapName ()))
         {
            sprintf (m_szInfo, "%s.pwf - hacked waypoint file, filename doesn't match waypoint header information (mapname: '%s', header: '%s')", GetMapName (), GetMapName (), kHeader.szMapname);
            LogToFile (true, LOG_ERROR, m_szInfo);

            fp.Close ();
            return false;
         }
         else
         {
            Init ();
            g_iNumWaypoints = kHeader.iWpNumber;

            for (i = 0; i < g_iNumWaypoints; i++)
            {
               m_pPaths[i] = AllocMem <tPath> ();

               if (m_pPaths[i] == NULL)
                  TerminateOnMalloc ();

               fp.Read (m_pPaths[i], sizeof (tPath));
            }
            m_bWaypointPaths = true;  
         }
      }
      else
      {
         sprintf (m_szInfo, "%s.pwf is not a yapb waypoint file (header found '%s' needed '%s'", GetMapName (), kHeader.szHeader, WAY_HEADER);
         LogToFile (true, LOG_ERROR, m_szInfo);

         fp.Close ();
         return false;
      }
      fp.Close ();
   }
   else
   {
      sprintf (m_szInfo, "%s.pwf does not exist", GetMapName ());
      LogToFile (true, LOG_ERROR, m_szInfo);

      return false;
   }

   if (strncmp (kHeader.szAuthor, "official", 7) == 0)
      sprintf (m_szInfo, "Using Official Waypoint File");
   else
      sprintf (m_szInfo, "Using Waypoint File By: %s", kHeader.szAuthor);

   for (i = 0; i < g_iNumWaypoints; i++)
      m_rgfWPDisplayTime[i] = 0.0;

   InitPathMatrix ();
   InitTypes ();

   g_bWaypointsChanged = false;
   g_cKillHistory = 0;

   InitVisibilityTab ();
   InitExperienceTab ();
   g_pBotManager->InitQuota ();

   SpeechSynth ("loading");
   CVarSetFloat (g_rgpcvBotCVar[CVAR_DEBUGGOAL]->GetName (), -1);

   return true;
}

void CWaypoint::Save (void)
{
   tWaypointHeader kHeader;

   memset (kHeader.szMapname, 0, sizeof (kHeader.szMapname));
   memset (kHeader.szAuthor , 0, sizeof (kHeader.szAuthor));
   memset (kHeader.szHeader, 0, sizeof (kHeader.szHeader));

   strcpy (kHeader.szHeader, WAY_HEADER);
   strcpy (kHeader.szAuthor, STRING (g_pentHostEdict->v.netname));
   strncpy (kHeader.szMapname, GetMapName (), 31);

   kHeader.szMapname[31] = 0;
   kHeader.iFileVersion = WAYPOINT_VERSION;
   kHeader.iWpNumber = g_iNumWaypoints;
   
   stdfile fp (VarArgs ("%s%s.pwf", GetWptDir (), GetMapName ()), "wb");

   // file was opened
   if (fp.IsValid ())
   {
      // write the waypoint header to the file...
      fp.Write (&kHeader, sizeof (kHeader), 1);

      // save the waypoint paths...
      for (int i = 0; i < g_iNumWaypoints; i++)
         fp.Write (m_pPaths[i], sizeof (tPath));
      fp.Close ();

      // save XML version
      SaveXML ();
   }
   else
      LogToFile (true, LOG_ERROR, "Error writing '%s.pwf' waypoint file", GetMapName ());
}

void CWaypoint::SaveXML (void)
{
   stdfile fp (VarArgs ("%sdata/%s.xml", GetWptDir (), GetMapName ()), "w");

   if (fp.IsValid ())
   {
      int j;

      fp.Printf ("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
      fp.Printf ("<!-- Generated by %s v%s. You may not edit this file! -->\n", PRODUCT_NAME, PRODUCT_VERSION);
      fp.Printf ("<WaypointData>\n");
      fp.Printf ("\t<header>\n");
      fp.Printf ("\t\t<author>%s</author>\n", STRING (g_pentHostEdict->v.netname));
      fp.Printf ("\t\t<map>%s</map>\n", GetMapName ());
      fp.Printf ("\t\t<version>%d</version>\n", WAYPOINT_VERSION);
      fp.Printf ("\t\t<number>%d</number>\n", g_iNumWaypoints);
      fp.Printf ("\t</header>\n");
      fp.Printf ("\t<map>\n");

      // save the waypoint paths...
      for (int i = 0; i < g_iNumWaypoints; i++)
      {
         tPath *pPath = m_pPaths[i];

         fp.Printf ("\t\t<waypoint id=\"%d\">\n", i + 1);
         fp.Printf ("\t\t\t<campend x=\"%.2f\" y=\"%.2f\"/>\n", pPath->fCampEndX, pPath->fCampEndY);
         fp.Printf ("\t\t\t<campstart x=\"%.2f\" y=\"%.2f\"/>\n", pPath->fCampStartX, pPath->fCampStartY);
         fp.Printf ("\t\t\t<flags>%d</flags>\n", pPath->iFlags);
         fp.Printf ("\t\t\t<pathnum>%d</pathnum>\n", pPath->iPathNumber);
         fp.Printf ("\t\t\t<radius>%.2f</radius>\n", pPath->fRadius);
         fp.Printf ("\t\t\t<origin x=\"%.2f\" y=\"%.2f\" z=\"%.2f\"/>\n", pPath->vOrigin.x, pPath->vOrigin.y, pPath->vOrigin.z);
         fp.Printf ("\t\t\t<connections size=\"%d\">\n", MAX_PATH_INDEX);

         for (j = 0; j < MAX_PATH_INDEX; j++)
         {
            fp.Printf ("\t\t\t\t<connection id=\"%d\">\n", j + 1);
            fp.Printf ("\t\t\t\t\t<velocity x=\"%.2f\" y=\"%.2f\" z=\"%.2f\"/>\n",  pPath->vConnectVel[j].x, pPath->vConnectVel[j].y, pPath->vConnectVel[j].z); 
            fp.Printf ("\t\t\t\t\t<distance>%d</distance>\n", pPath->iDistance[j]);
            fp.Printf ("\t\t\t\t\t<index>%d</index>\n", pPath->iIndex[j]);
            fp.Printf ("\t\t\t\t\t<flags>%u</flags>\n", pPath->uConnectFlag[j]);
            fp.Printf ("\t\t\t\t</connection>\n");
         }
         fp.Printf ("\t\t\t</connections>\n");

         fp.Printf ("\t\t</waypoint>\n");
         
      }
      fp.Printf ("\t</map>\n");
      fp.Printf ("</WaypointData>");
      fp.Close ();
   }
   else
      LogToFile (true, LOG_ERROR, "Error writing '%s.xml' waypoint file", GetMapName ());
}

float CWaypoint::GetTravelTime (float fMaxSpeed, Vector vSource, Vector vPosition)
{
   // this function returns 2D traveltime to a position

   return ((vPosition - vSource).Length2D () / fMaxSpeed);
}

bool CWaypoint::Reachable (Vector vSource, Vector vDestination, edict_t *pent)
{
   float fDistance = (vDestination - vSource).Length ();

   if (fDistance < 200) // is the destination close enough?
   {
      TraceResult tr;

      // check if this waypoint is "visible"
      TraceLine (vSource, vDestination, true, pent, &tr);

      // if waypoint is visible from current position (even behind head)...
      if (tr.flFraction >= 1.0)
      {
         if ((pent->v.waterlevel == 2) || (pent->v.waterlevel == 3))
         {
            // is destination waypoint higher that source (45 is max jump height)
            if (vDestination.z > vSource.z + 45.0)
               return false; // unable to reach this one

            // is destination waypoint higher that source
            if (vDestination.z < vSource.z - 100.0)
               return false; // unable to reach this one
         }
         return true;
      }
   }
   return false;
}

bool CWaypoint::IsNodeReachable (Vector vSource, Vector vDestination)
{
   TraceResult tr;
   
   float fHeight, fLastHeight;
   float fDistance = (vDestination - vSource).Length ();

   // is the destination not close enough?
   if (fDistance > g_fAutoPathDistance)
      return false;

   // check if we go through a func_illusionary, in which case return false
   TraceHull (vSource, vDestination, ignore_monsters, head_hull, g_pentHostEdict, &tr);
   
   if (!FNullEnt (tr.pHit) && (strcmp ("func_illusionary", STRING (tr.pHit->v.classname)) == 0))
      return false; // don't add pathwaypoints through func_illusionaries

   // check if this waypoint is "visible"...
   TraceLine (vSource, vDestination, ignore_monsters, g_pentHostEdict, &tr);

   // if waypoint is visible from current position (even behind head)...
   if ((tr.flFraction >= 1.0) || (strncmp ("func_door", STRING (tr.pHit->v.classname), 9) == 0))
   {
      // if it's a door check if nothing blocks behind
      if (strncmp ("func_door", STRING (tr.pHit->v.classname), 9) == 0)
      {
         Vector vDoorEnd = tr.vecEndPos;
         TraceLine (vDoorEnd, vDestination, ignore_monsters, tr.pHit, &tr);
         
         if (tr.flFraction < 1.0)
            return false;
      }

      // check for special case of both waypoints being in water...
      if ((POINT_CONTENTS (vSource) == CONTENTS_WATER) && (POINT_CONTENTS (vDestination) == CONTENTS_WATER))
          return true; // then they're reachable each other

      // is dest waypoint higher than src? (45 is max jump height)
      if (vDestination.z > vSource.z + 45.0)
      {
         Vector vSourceNew = vDestination;
         Vector vDestinationNew = vDestination;
         vDestinationNew.z = vDestinationNew.z - 50; // straight down 50 units

         TraceLine (vSourceNew, vDestinationNew, ignore_monsters, g_pentHostEdict, &tr);

         // check if we didn't hit anything, if not then it's in mid-air
         if (tr.flFraction >= 1.0)
            return false; // can't reach this one
      }

      // check if distance to ground drops more than step height at points between source and destination...
      Vector vDirection = (vDestination - vSource).Normalize(); // 1 unit long
      Vector vCheck = vSource;
      Vector vDown = vSource;
      vDown.z = vDown.z - 1000.0; // straight down 1000 units

      TraceLine (vCheck, vDown, ignore_monsters, g_pentHostEdict, &tr);

      fLastHeight = tr.flFraction * 1000.0; // height from ground
      fDistance = (vDestination - vCheck).Length (); // distance from goal

      while (fDistance > 10.0)
      {
         // move 10 units closer to the goal...
         vCheck = vCheck + (vDirection * 10.0);
         
         vDown = vCheck;
         vDown.z = vDown.z - 1000.0; // straight down 1000 units

         TraceLine (vCheck, vDown, ignore_monsters, g_pentHostEdict, &tr);
         
         fHeight = tr.flFraction * 1000.0; // height from ground

         // is the current fHeight greater than the step height?
         if (fHeight < fLastHeight - 18.0)
            return false; // can't get there without jumping...

         fLastHeight = fHeight;
         fDistance = (vDestination - vCheck).Length (); // fDistance from goal
      }
      return true;
   }
   return false;
}

void CWaypoint::InitializeVisibility (void)
{
   if (m_bRecalcVis == false)
      return;

   TraceResult tr;

   byte ucRes;
   byte ucShift;

   for (m_iCurrVisIndex = 0; m_iCurrVisIndex < g_iNumWaypoints; m_iCurrVisIndex++)
   {
      Vector vSourceDuck = m_pPaths[m_iCurrVisIndex]->vOrigin;
      Vector vSourceStand = m_pPaths[m_iCurrVisIndex]->vOrigin;

      if (m_pPaths[m_iCurrVisIndex]->iFlags & W_FL_CROUCH)
      {
         vSourceDuck.z += 12.0;
         vSourceStand.z += 18.0 + 28.0;
      }
      else
      {
         vSourceDuck.z += -18.0 + 12.0;
         vSourceStand.z += 28.0;
      }
      uint16 iStandCount = 0, iCrouchCount = 0;

      for (int i = 0; i < g_iNumWaypoints; i++)
      {
         // First check ducked Visibility
         Vector vDest = m_pPaths[i]->vOrigin;
         TraceLine (vSourceDuck, vDest, true, NULL, &tr);

         // check if line of sight to object is not blocked (i.e. visible)
         if ((tr.flFraction != 1.0) || tr.fStartSolid)
            ucRes = 1;
         else
            ucRes = 0;

         ucRes <<= 1;

         TraceLine (vSourceStand, vDest, true, NULL, &tr);

         // check if line of sight to object is not blocked (i.e. visible)
         if ((tr.flFraction != 1.0) || tr.fStartSolid)
            ucRes |= 1;

         ucShift = (i % 4) << 1;
         m_rgbyVisLUT[m_iCurrVisIndex][i >> 2] &= ~(3 << ucShift);
         m_rgbyVisLUT[m_iCurrVisIndex][i >> 2] |= ucRes << ucShift; 

         if ((ucRes & 2) == NULL)
            iCrouchCount++;

         if ((ucRes & 1) == NULL)
            iStandCount++;
      }
      m_pPaths[m_iCurrVisIndex]->kVis.uCrouch = iCrouchCount;
      m_pPaths[m_iCurrVisIndex]->kVis.uStand = iStandCount;
   }
   m_bRecalcVis = false;
}

bool CWaypoint::IsVisible (int iSourceIndex, int iDestIndex)
{
   unsigned char ucRes = m_rgbyVisLUT[iSourceIndex][iDestIndex >> 2];
   ucRes >>= (iDestIndex % 4) << 1;

   return !((ucRes & 3) == 3);
}

bool CWaypoint::IsDuckVisible (int iSourceIndex, int iDestIndex)
{
   unsigned char ucRes = m_rgbyVisLUT[iSourceIndex][iDestIndex >> 2];
   ucRes >>= (iDestIndex % 4) << 1;

   return !((ucRes & 2) == 2);
}

bool CWaypoint::IsStandVisible (int iSourceIndex, int iDestIndex)
{
   unsigned char ucRes = m_rgbyVisLUT[iSourceIndex][iDestIndex >> 2];
   ucRes >>= (iDestIndex % 4) << 1;

   return !((ucRes & 1) == 1);
}

void CWaypoint::Think (void)
{
   if (FNullEnt (g_pentHostEdict))
      return;

   float fDistance, fMinDistance;
   Vector vStart, vEnd;
   int i, iIndex = 0;

   if (g_iFindWPIndex != -1)
      DrawArrow (g_pentHostEdict, g_pentHostEdict->v.origin, m_pPaths[g_iFindWPIndex]->vOrigin, 20, 0, 255, 255, 0, 200, 0, 1);

   if (g_bAutoWaypoint && (g_pentHostEdict->v.flags & (FL_ONGROUND | FL_PARTIALGROUND)))
   {
      // find the distance from the last used waypoint
      fDistance = (m_vLastWaypoint - g_pentHostEdict->v.origin).Length ();

      if (fDistance > 128)
      {
         fMinDistance = 9999.0;

         // check that no other reachable waypoints are nearby...
         for (i = 0; i < g_iNumWaypoints; i++)
         {
            if (IsNodeReachable (g_pentHostEdict->v.origin, m_pPaths[i]->vOrigin))
            {
               fDistance = (m_pPaths[i]->vOrigin - g_pentHostEdict->v.origin).Length ();

               if (fDistance < fMinDistance)
                  fMinDistance = fDistance;
            }
         }

         // make sure nearest waypoint is far enough away...
         if (fMinDistance >= 128)
            Add (0);  // place a waypoint here
      }
   }
   fMinDistance = 9999.0;

   for (i = 0; i < g_iNumWaypoints; i++)
   {
      fDistance = (m_pPaths[i]->vOrigin - g_pentHostEdict->v.origin).Length ();

      if ((fDistance < 500) && ((IsInViewCone (m_pPaths[i]->vOrigin, g_pentHostEdict) && ::IsVisible (m_pPaths[i]->vOrigin, g_pentHostEdict)) || !IsAlive (g_pentHostEdict) || (fDistance < 50)))
      {
         if (fDistance < fMinDistance)
         {
            iIndex = i; // store iIndex of nearest waypoint
            fMinDistance = fDistance;
         }

         if (m_rgfWPDisplayTime[i] + 1.0 < WorldTime ())
         {
            if (m_pPaths[i]->iFlags & W_FL_CROUCH)
            {
               vStart = m_pPaths[i]->vOrigin - Vector (0, 0, 16);
               vEnd = vStart + Vector (0, 0, 32);
            }
            else
            {
               vStart = m_pPaths[i]->vOrigin - Vector (0, 0, 36);
               vEnd = vStart + Vector (0, 0, 72);
            }

            if (m_pPaths[i]->iFlags & W_FL_CROSSING)
            {
               if (m_pPaths[i]->iFlags & W_FL_CAMP)
               {
                  if (m_pPaths[i]->iFlags & W_FL_TERRORIST)
                     DrawLine (g_pentHostEdict, vStart, vEnd, 15, 0, 255, 160, 160, 250, 0, 10);
                  else if (m_pPaths[i]->iFlags & W_FL_COUNTER)
                     DrawLine (g_pentHostEdict, vStart, vEnd, 15, 0, 255, 160, 255, 250, 0, 10);
                  else
                     DrawLine (g_pentHostEdict, vStart, vEnd, 15, 0, 0, 255, 255, 250, 0, 10);
               }
               else if (m_pPaths[i]->iFlags & W_FL_TERRORIST)
                  DrawLine (g_pentHostEdict, vStart, vEnd, 15, 0, 255, 0, 0, 250, 0, 10);
               else if (m_pPaths[i]->iFlags & W_FL_COUNTER)
                  DrawLine (g_pentHostEdict, vStart, vEnd, 15, 0, 0, 0, 255, 250, 0, 10);
            }
            else if (m_pPaths[i]->iFlags & W_FL_GOAL)
               DrawLine (g_pentHostEdict, vStart, vEnd, 15, 0, 255, 0, 255, 250, 0, 10);
            else if (m_pPaths[i]->iFlags & W_FL_LADDER)
               DrawLine (g_pentHostEdict, vStart, vEnd, 15, 0, 255, 0, 255, 250, 0, 10);
            else if (m_pPaths[i]->iFlags & W_FL_RESCUE)
               DrawLine (g_pentHostEdict, vStart, vEnd, 15, 0, 255, 255, 255, 250, 0, 10);
            else if (m_pPaths[i]->iFlags & W_FL_NOHOSTAGE)
               DrawLine (g_pentHostEdict, vStart, vEnd, 15, 0, 255, 255, 0, 250, 0, 10);
            else
               DrawLine (g_pentHostEdict, vStart, vEnd, 15, 0, 0, 255, 0, 250, 0, 10);

            m_rgfWPDisplayTime[i] = WorldTime ();
         }
      }
   }

   if (m_fPathTime <= WorldTime ())
   {
      m_fPathTime = WorldTime () + 1.0;

      if (fMinDistance <= 200) // check if player is close enough to a waypoint and time to draw path...
      {
         if (m_pPaths[iIndex]->iFlags & W_FL_CAMP)
         {
            if (m_pPaths[iIndex]->iFlags & W_FL_CROUCH)
               vStart = m_pPaths[iIndex]->vOrigin + Vector (0, 0, 16);
            else
               vStart = m_pPaths[iIndex]->vOrigin + Vector (0, 0, 36);

            vEnd.x = m_pPaths[iIndex]->fCampStartX;
            vEnd.y = m_pPaths[iIndex]->fCampStartY;

            DrawLine (g_pentHostEdict, vStart, vEnd, 15, 0, 255, 0, 0, 250, 0, 10);

            vEnd.x = m_pPaths[iIndex]->fCampEndX;
            vEnd.y = m_pPaths[iIndex]->fCampEndY;

            DrawLine (g_pentHostEdict, vStart, vEnd, 15, 0, 255, 0, 0, 250, 0, 10);
         }
     }
    
     if (fMinDistance <= 50) // check if player is close enough to a waypoint and time to draw info
     {
         tPath *pPath = m_pPaths[iIndex];
         bool bIsJumpPoint = false;

         char szMessage[512];

         for (i = 0; i < MAX_PATH_INDEX; i++)
         {
            if ((pPath->iIndex[i] != -1) && (pPath->uConnectFlag[i] & C_FL_JUMP))
               bIsJumpPoint = true;
         }

         sprintf (szMessage, "\n\n\n\n    Waypoint Information:\n\n"
            "      Waypoint %d of %d,  Radius: %d%s\n"
            "      Flags:%s%s%s%s%s%s%s%s%s%s%s%s\n",
            iIndex, g_iNumWaypoints,  static_cast <int> (pPath->fRadius),
            (pPath->iFlags == 0 && !bIsJumpPoint) ? " (none)" : "",
            pPath->iFlags & W_FL_LIFT ? " LIFT" : "",
            pPath->iFlags & W_FL_CROUCH ? " CROUCH" : "",
            pPath->iFlags & W_FL_CROSSING ? " CROSSING" : "",
            pPath->iFlags & W_FL_CAMP ? " CAMP" : "",
            pPath->iFlags & W_FL_TERRORIST ? " TERRORIST" : "",
            pPath->iFlags & W_FL_COUNTER ? " CT" : "",
            pPath->iFlags & W_FL_GOAL ? " GOAL" : "",
            pPath->iFlags & W_FL_LADDER ? " LADDER" : "",
            pPath->iFlags & W_FL_RESCUE ? " RESCUE" : "",
            pPath->iFlags & W_FL_JUMPHELP ? " JUMPHELP" : "",
            pPath->iFlags & W_FL_NOHOSTAGE ? " NOHOSTAGE" : "", bIsJumpPoint ? " JUMP" : "");

         if (!g_bWaypointsChanged)
         {
            int iDangerIndexCT = (g_pExperienceData + iIndex * g_iNumWaypoints + iIndex)->iTeam1DangerIndex;
            int iDangerIndexT = (g_pExperienceData + iIndex * g_iNumWaypoints + iIndex)->iTeam0DangerIndex;
            
            strcat (szMessage, VarArgs ("\n\n"
               "    Experience (Index/Damage):\n"
               "      CT: %d/%u\n"
               "      T: %d/%u\n",
               iDangerIndexCT,
               (iDangerIndexCT != -1) ?
               ((g_pExperienceData + iIndex * g_iNumWaypoints + iDangerIndexCT)->uTeam1Damage) : 0,
               iDangerIndexT,
               (iDangerIndexT != -1) ?
               ((g_pExperienceData + iIndex * g_iNumWaypoints + iDangerIndexT)->uTeam0Damage) : 0));
         }

         MESSAGE_BEGIN (MSG_ONE_UNRELIABLE, SVC_TEMPENTITY, NULL, g_pentHostEdict);
            WRITE_BYTE (TE_TEXTMESSAGE);
            WRITE_BYTE (4); // channel
            WRITE_SHORT (FixedSigned16 (0, 1 << 13)); // x
            WRITE_SHORT (FixedSigned16 (0, 1 << 13)); // y
            WRITE_BYTE (0); // effect
            WRITE_BYTE (255); // r1
            WRITE_BYTE (5); // g1
            WRITE_BYTE (255); // b1
            WRITE_BYTE (1); // a1
            WRITE_BYTE (155); // r2
            WRITE_BYTE (5); // g2
            WRITE_BYTE (255); // b2
            WRITE_BYTE (1); // a2
            WRITE_SHORT (0); // fadeintime
            WRITE_SHORT (0); // fadeouttime
            WRITE_SHORT (FixedUnsigned16 (1.1, 1 << 8)); // holdtime
            WRITE_STRING (szMessage);
         MESSAGE_END ();

         for (i = 0; i < MAX_PATH_INDEX; i++)
         {
            if (pPath->iIndex[i] != -1)
            {
               Vector vSource = pPath->vOrigin;
               Vector vDestination = m_pPaths[pPath->iIndex[i]]->vOrigin;

               if (pPath->uConnectFlag[i] & C_FL_JUMP) // jump connection
                  DrawLine (g_pentHostEdict, vSource, vDestination, 5, 0, 255, 0, 128, 200, 0, 10);
               else if (IsConnected (pPath->iIndex[i], iIndex)) // 2-way connection
                  DrawLine (g_pentHostEdict, vSource, vDestination, 5, 0, 255, 255, 0, 200, 0, 10);
               else // 1-way connection
                  DrawLine (g_pentHostEdict, vSource, vDestination, 5, 0, 250, 250, 250, 200, 0, 10);
            }
         }

         vStart = m_pPaths[iIndex]->vOrigin;
         MakeVectors (NULLVEC);

         int iDistance = m_pPaths[iIndex]->fRadius;
         Vector vDirection = g_pGlobals->v_forward * iDistance;
         vDirection = VecToAngles (vDirection);

         for (float fRadCircle = 0.0; fRadCircle < 180.0; fRadCircle += 20)
         {
            MakeVectors (vDirection);
            
            Vector vRadiusStart = vStart - g_pGlobals->v_forward * iDistance;
            Vector vRadiusEnd = vStart + g_pGlobals->v_forward * iDistance;
            
            DrawLine (g_pentHostEdict, vRadiusStart, vRadiusEnd, 10, 0, 0, 0, 255, 250, 0, 10);
            vDirection.y = AngleNormalize (vDirection.y + fRadCircle);
         }
     }
  }
 
   if ((m_fDangerTime <= WorldTime ()) && g_bDangerDirection && !g_bWaypointsChanged)
   {
      if (fMinDistance <= 50)
      {
         m_fDangerTime = WorldTime () + 1.0;

         if ((g_pExperienceData + (iIndex * g_iNumWaypoints) + iIndex)->iTeam0DangerIndex != -1)
         {
            Vector vSource = m_pPaths[iIndex]->vOrigin;
            Vector vDestination = m_pPaths[(g_pExperienceData + (iIndex * g_iNumWaypoints) + iIndex)->iTeam0DangerIndex]->vOrigin;
            
            // draw a red arrow to this index's danger point
            DrawArrow (g_pentHostEdict, vSource, vDestination, 30, 0, 255, 0, 0, 200, 0, 10);
         }

         if ((g_pExperienceData + (iIndex * g_iNumWaypoints) + iIndex)->iTeam1DangerIndex != -1)
         {
            Vector vSource = m_pPaths[iIndex]->vOrigin;
            Vector vDestination = m_pPaths[(g_pExperienceData + (iIndex * g_iNumWaypoints) + iIndex)->iTeam1DangerIndex]->vOrigin;
            
            // draw a blue arrow to this index's danger point
            DrawArrow (g_pentHostEdict, vSource, vDestination, 30, 0, 0, 0, 255, 200, 0, 10);
         }
      }
   }

   if (m_bLearnJumpWaypoint)
   {
      if (!m_bEndJumpPoint)
      {
         if (g_pentHostEdict->v.button & IN_JUMP)
         {
            Add (9);
            m_fTimeJumpStarted = WorldTime ();
            m_bEndJumpPoint = true;
         }
         else
         {
            m_vLearnVelocity = g_pentHostEdict->v.velocity;
            m_vLearnPos = g_pentHostEdict->v.origin;
         }
      }
      else
      {
         if ((((g_pentHostEdict->v.flags & FL_ONGROUND) != 0) || (g_pentHostEdict->v.waterlevel > 0) || (g_pentHostEdict->v.movetype == MOVETYPE_FLY)) &&  (m_fTimeJumpStarted + 0.1 < WorldTime ()) && m_bEndJumpPoint)
         {
            Add (10);
            m_bLearnJumpWaypoint = false;
            m_bEndJumpPoint = false;

            SpeechSynth ("movement check over");
         }
      }
   }
}

bool CWaypoint::IsConnected (int iNum)
{
   for (int i = 0; i < g_iNumWaypoints; i++)
   {
      if (i != iNum)
      {
         for (int j = 0; j < MAX_PATH_INDEX; j++)
         {
            if (m_pPaths[i]->iIndex[j] == iNum)
               return true;
         }
      }
   }
   return false;
}

bool CWaypoint::NodesValid (void)
{
   int iTPoints = 0;
   int iCTPoints = 0;
   int iGoalPoints = 0;
   int iRescuePoints = 0;
   int iConnections;
   int i, j;

   for (i = 0; i < g_iNumWaypoints; i++)
   {
      iConnections = 0;

      for (int j = 0; j < MAX_PATH_INDEX; j++)
      {
         if (m_pPaths[i]->iIndex[j] != -1)
         {
            if (m_pPaths[i]->iIndex[j] > g_iNumWaypoints)
            {
               ServerPrint ("Waypoint %d connected with invalid Waypoint Nr. %d!", i, m_pPaths[i]->iIndex[j]);
               return false;
            }
            iConnections++;
            break;
         }
      }

      if (iConnections == 0)
      {
         if (!IsConnected (i))
         {
            ServerPrint ("Waypoint %d isn't connected with any other Waypoint!", i);
            return false;
         }
      }

      if (m_pPaths[i]->iPathNumber != i)
      {
         ServerPrint ("Waypoint %d pathnumber differs from index!", i);
         return false;
      }

      if (m_pPaths[i]->iFlags & W_FL_CAMP)
      {
         if ((m_pPaths[i]->fCampEndX == 0) && (m_pPaths[i]->fCampEndY == 0))
         {
            ServerPrint ("Waypoint %d Camp-Endposition not set!", i);
            return false;
         }
      }
      else if (m_pPaths[i]->iFlags & W_FL_TERRORIST)
         iTPoints++;
      else if (m_pPaths[i]->iFlags & W_FL_COUNTER)
         iCTPoints++;
      else if (m_pPaths[i]->iFlags & W_FL_GOAL)
         iGoalPoints++;
      else if (m_pPaths[i]->iFlags & W_FL_RESCUE)
         iRescuePoints++;

      for (int k = 0; k < MAX_PATH_INDEX; k++)
      {
         if (m_pPaths[i]->iIndex[k] != -1)
         {
            if ((m_pPaths[i]->iIndex[k] >= g_iNumWaypoints) || (m_pPaths[i]->iIndex[k] < -1))
            {
               ServerPrint ("Waypoint %d - Pathindex %d out of Range!", i, k);
               (*g_engfuncs.pfnSetOrigin) (g_pentHostEdict, m_pPaths[i]->vOrigin);
               
               g_bWaypointOn = true;
               g_bEditNoclip = true;
               
               return false;
            }
            else if (m_pPaths[i]->iIndex[k] == i)
            {
               ServerPrint ("Waypoint %d - Pathindex %d points to itself!", i, k);
               (*g_engfuncs.pfnSetOrigin) (g_pentHostEdict, m_pPaths[i]->vOrigin);
               
               g_bWaypointOn = true;
               g_bEditNoclip = true;

               return false;
            }
         }
      }
   }
   if (g_iMapType & MAP_CS)
   {
      if (iRescuePoints == 0)
      {
         ServerPrint ("You didn't set a Rescue Point!");
         return false;
      }
   }
   if (iTPoints == 0)
   {
      ServerPrint ("You didn't set any Terrorist Important Point!");
      return false;
   }
   else if (iCTPoints == 0)
   {
      ServerPrint ("You didn't set any CT Important Point!");
      return false;
   }
   else if (iGoalPoints == 0)
   {
      ServerPrint ("You didn't set any Goal Point!");
      return false;
   }
   
   // Perform DFS instead of Floyd-Warashall, this shit speedup this process in a bit
   tPathNode *pStack = NULL;
   bool rgbVisited[MAX_WAYPOINTS];

   // first check incoming connectivity
   // initialize the "visited" table
   for (i = 0; i < g_iNumWaypoints; i++)
      rgbVisited[i] = false;

   // check from Waypoint nr. 0
   pStack = AllocMem <tPathNode> ();
   pStack->pNext = NULL;
   pStack->iIndex = 0;

   while (pStack != NULL)
   {
      // pop a node from the pStack
      tPathNode *pCurrent = pStack;
      pStack = pStack->pNext;

      rgbVisited[pCurrent->iIndex] = true;

      for (j = 0; j < MAX_PATH_INDEX; j++)
      {
         int iIndex = m_pPaths[pCurrent->iIndex]->iIndex[j];
         
         if (rgbVisited[iIndex])
            continue; // skip this waypoint as it's already visited
            
         if (iIndex >= 0 && iIndex < g_iNumWaypoints)
         {
            tPathNode *pNewNode = AllocMem <tPathNode> ();
            
            pNewNode->pNext = pStack;
            pNewNode->iIndex = iIndex;
            pStack = pNewNode;
         }
      }
      delete pCurrent;
   }

   for (i = 0; i < g_iNumWaypoints; i++)
   {
      if (rgbVisited[i] == false)
      {
         ServerPrint ("Path broken from Waypoint Nr. 0 to Waypoint Nr. %d!", i);
         (*g_engfuncs.pfnSetOrigin) (g_pentHostEdict, m_pPaths[i]->vOrigin);
         
         g_bWaypointOn = true;
         g_bEditNoclip = true;
         
         return false;
      }
   }

   // then check outgoing connectivity
   vector <int> kInPath[MAX_WAYPOINTS]; // store incoming paths for speedup

   for (i = 0; i < g_iNumWaypoints; i++)
     {
      for (j = 0; j < MAX_PATH_INDEX; j++)
      {
         if ((m_pPaths[i]->iIndex[j] >= 0) && (m_pPaths[i]->iIndex[j] < g_iNumWaypoints))
            kInPath[m_pPaths[i]->iIndex[j]].AddItem (i);
      }
   }

   // initialize the "visited" table
   for (i = 0; i < g_iNumWaypoints; i++)
      rgbVisited[i] = false;

   // check from Waypoint nr. 0
   pStack = AllocMem <tPathNode> ();
   pStack->pNext = NULL;
   pStack->iIndex = 0;

   while (pStack)
   {
      // pop a node from the pStack
      tPathNode *pCurrent = pStack;
      pStack = pStack->pNext;

      rgbVisited[pCurrent->iIndex] = true;

      ITERATE_ARRAY (kInPath[pCurrent->iIndex], j)
      {
         if (rgbVisited[kInPath[pCurrent->iIndex][j]])
            continue; // skip this waypoint as it's already visited
            
         tPathNode *pNewNode = AllocMem <tPathNode> ();
         
         pNewNode->pNext = pStack;
         pNewNode->iIndex = kInPath[pCurrent->iIndex][j];
         pStack = pNewNode;
      }
      delete pCurrent;
   }

   for (i = 0; i < g_iNumWaypoints; i++)
   {
      if (rgbVisited[i] == false)
      {
         ServerPrint ("Path broken from Waypoint Nr. %d to Waypoint Nr. 0!", i);
         (*g_engfuncs.pfnSetOrigin) (g_pentHostEdict, m_pPaths[i]->vOrigin);
         
         g_bWaypointOn = true;
         g_bEditNoclip = true;
         
         return false;
      }
   }
   return true;
}

void CWaypoint::InitPathMatrix (void)
{
   int i, j, k;

   if (m_pDistMatrix != NULL)
      delete [] (m_pDistMatrix);

   if (m_pPathMatrix != NULL)
      delete [] m_pPathMatrix;

   m_pDistMatrix = NULL;
   m_pPathMatrix = NULL;

   m_pDistMatrix = AllocMem <int> (g_iNumWaypoints * g_iNumWaypoints);
   m_pPathMatrix = AllocMem <int> (g_iNumWaypoints * g_iNumWaypoints);

   if ((m_pDistMatrix == NULL) || (m_pPathMatrix == NULL))
      TerminateOnMalloc ();

   if (LoadPathMatrix ())
      return; // matrix loaded from file

   for (i = 0; i < g_iNumWaypoints; i++)
   {
      for (j = 0; j < g_iNumWaypoints; j++)
      {
         *(m_pDistMatrix + i * g_iNumWaypoints + j) = 999999;
         *(m_pPathMatrix + i * g_iNumWaypoints + j) = -1;
      }
   }

   for (i = 0; i < g_iNumWaypoints; i++)
   {
      for (j = 0; j < MAX_PATH_INDEX; j++)
      {
         if (m_pPaths[i]->iIndex[j] >= 0 && m_pPaths[i]->iIndex[j] < g_iNumWaypoints)
         {
            *(m_pDistMatrix + (i * g_iNumWaypoints) + m_pPaths[i]->iIndex[j]) = m_pPaths[i]->iDistance[j];
            *(m_pPathMatrix + (i * g_iNumWaypoints) + m_pPaths[i]->iIndex[j]) = m_pPaths[i]->iIndex[j];
         }
      }
   }

   for (i = 0; i < g_iNumWaypoints; i++)
      *(m_pDistMatrix + (i * g_iNumWaypoints) + i) = 0;

   for (k = 0; k < g_iNumWaypoints; k++)
   {
      for (i = 0; i < g_iNumWaypoints; i++)
      {
         for (j = 0; j < g_iNumWaypoints; j++)
         {
            if (*(m_pDistMatrix + (i * g_iNumWaypoints) + k) + *(m_pDistMatrix + (k * g_iNumWaypoints) + j) < (*(m_pDistMatrix + (i * g_iNumWaypoints) + j)))
            {
               *(m_pDistMatrix + (i * g_iNumWaypoints) + j) = *(m_pDistMatrix + (i * g_iNumWaypoints) + k) + *(m_pDistMatrix + (k * g_iNumWaypoints) + j);
               *(m_pPathMatrix + (i * g_iNumWaypoints) + j) = *(m_pPathMatrix + (i * g_iNumWaypoints) + k);
            }
         }
      }
   }

   // save path matrix to file for faster access
   SavePathMatrix ();
}

void CWaypoint::SavePathMatrix (void)
{
   stdfile fp (VarArgs ("%sdata/%s.pmt", GetWptDir (), GetMapName ()), "wb");

   // unable to open file
   if (!fp.IsValid ())
   {
      LogToFile (false, LOG_CRITICAL, "Failed to open file for writing");
      return;
   }

   // write number of waypoints
   fp.Write (&g_iNumWaypoints, sizeof (int));

   // write path & distance matrix
   fp.Write (m_pPathMatrix, sizeof (int), g_iNumWaypoints * g_iNumWaypoints);
   fp.Write (m_pDistMatrix, sizeof (int), g_iNumWaypoints * g_iNumWaypoints);
   
   // and close the file
   fp.Close ();
}

bool CWaypoint::LoadPathMatrix (void)
{
   stdfile fp (VarArgs ("%sdata/%s.pmt", GetWptDir (), GetMapName ()), "rb");

   // file doesn't exists return false
   if (!fp.IsValid ())
      return false;

   int iNum = 0;

   // read number of waypoints
   fp.Read (&iNum, sizeof (int));

   if (iNum != g_iNumWaypoints)
   {
      LogToFile (true, LOG_DEFAULT, "Wrong number of points (pmt:%d/cur:%d). Matrix will be rebuilded", iNum, g_iNumWaypoints);
      fp.Close ();

      return false;
   }

   // read path & distance matrixs
   fp.Read (m_pPathMatrix, sizeof (int), g_iNumWaypoints * g_iNumWaypoints);
   fp.Read (m_pDistMatrix, sizeof (int), g_iNumWaypoints * g_iNumWaypoints);
   
   // and close the file
   fp.Close ();

   return true;
}

int CWaypoint::GetPathDistance (int iSourceWaypoint, int iDestWaypoint)
{
   return (*(m_pDistMatrix + (iSourceWaypoint * g_iNumWaypoints) + iDestWaypoint));
}

void CWaypoint::CreateBasic (void)
{
   // this function creates basic waypoint types on map

   edict_t *pent = NULL;

   // first of all, if map contains ladder points, create it
   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "func_ladder")))
   {
      Vector vLadderLeft = pent->v.absmin;
      Vector vLadderRight = pent->v.absmax;
      vLadderLeft.z = vLadderRight.z;

      TraceResult tr;
      Vector vUp, vDown, vFront, vBack;

      Vector vDiff = CrossProduct (vLadderLeft - vLadderRight, Vector (0, 0, 0)).Normalize () * 15.0;
      vFront = vBack = GetEntityOrigin (pent);

      vFront = vFront + vDiff; // front
      vBack = vBack - vDiff; // back

      vUp = vDown = vFront;
      vDown.z = pent->v.absmax.z;

      TraceHull (vDown, vUp, true, point_hull, NULL, &tr);

      if ((POINT_CONTENTS (vUp) == CONTENTS_SOLID) || (tr.flFraction != 1.0))
      {
         vUp = vDown = vBack;
         vDown.z = pent->v.absmax.z;
      }
      
      TraceHull (vDown, vUp - Vector (0, 0, 1000), true, point_hull, NULL, &tr);
      vUp = tr.vecEndPos;
      
      Vector vWaypoint = vUp + Vector (0, 0, 39);

      m_bOnLadder = true;
      
      do
      {
         if (FindNearest (vWaypoint, 50.0) == -1)
            Add (3, vWaypoint);

         vWaypoint.z += 160; 
      } while (vWaypoint.z < vDown.z - 40);

      vWaypoint = vDown + Vector (0, 0, 38);

      if (FindNearest (vWaypoint, 50.0) == -1)
         Add (3, vWaypoint);

      m_bOnLadder = false;
   }

   // then terrortist spawnpoints
   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "info_player_deathmatch")))
   {
      Vector vOrigin = GetEntityOrigin (pent);

      if (FindNearest (vOrigin, 50) == -1)
         Add (0, vOrigin);
   }

   // then add ct spawnpoints
   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "info_player_start")))
   {
      Vector vOrigin = GetEntityOrigin (pent);

      if (FindNearest (vOrigin, 50) == -1)
         Add (0, vOrigin);
   }

   // then vip spawnpoint
   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "info_vip_start")))
   {
      Vector vOrigin = GetEntityOrigin (pent);

      if (FindNearest (vOrigin, 50) == -1)
         Add (0, vOrigin);
   }

   // hostage rescue zone
   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "func_hostage_rescue")))
   {
      Vector vOrigin = GetEntityOrigin (pent);

      if (FindNearest (vOrigin, 50) == -1)
         Add (4, vOrigin);
   }

   // hostage rescue zone (same as above)
   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "info_hostage_rescue")))
   {
      Vector vOrigin = GetEntityOrigin (pent);

      if (FindNearest (vOrigin, 50) == -1)
         Add (4, vOrigin);
   }

   // bombspot zone
   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "func_bomb_target")))
   {
      Vector vOrigin = GetEntityOrigin (pent);

      if (FindNearest (vOrigin, 50) == -1)
         Add (100, vOrigin);
   }

   // bombspot zone (same as above)
   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "info_bomb_target")))
   {
      Vector vOrigin = GetEntityOrigin (pent);

      if (FindNearest (vOrigin, 50) == -1)
         Add (100, vOrigin);
   }

   // hostage entities
   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "hostage_entity")))
   {
      // if already saved || moving skip it
      if ((pent->v.effects & EF_NODRAW) && (pent->v.speed > 0))
         continue;

      Vector vOrigin = GetEntityOrigin (pent);

      if (FindNearest (vOrigin, 50) == -1)
         Add (100, vOrigin);
   }

   // vip rescue (safety) zone
   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "func_vip_safetyzone")))
   {
      Vector vOrigin = GetEntityOrigin (pent);

      if (FindNearest (vOrigin, 50) == -1)
         Add (100, vOrigin);
   }

   // terrorist escape zone
   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "func_escapezone")))
   {
      Vector vOrigin = GetEntityOrigin (pent);

      if (FindNearest (vOrigin, 50) == -1)
         Add (100, vOrigin);
   }

   // weapons on the map ?
   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "armoury_entity")))
   {
      Vector vOrigin = GetEntityOrigin (pent);

      if (FindNearest (vOrigin, 50) == -1)
         Add (0, vOrigin);
   }
}

tPath *CWaypoint::GetPath (int iId)
{
   tPath *pPath = m_pPaths[iId];

   if (pPath == NULL)
      return NULL;

   return pPath;
}

void CWaypoint::EraseFromHardDisk (void)
{
   // this function removes waypoint file from the hard disk

   string sErasingFile[5];

   // if we're delete waypoint, delete all corresponding to it files
   sErasingFile[0] = VarArgs ("%s%s.pwf", GetWptDir (), GetMapName ()); // waypoint itself
   sErasingFile[1] = VarArgs ("%sdata/%s.exp", GetWptDir (), GetMapName ()); // corresponding to waypoint experience
   sErasingFile[3] = VarArgs ("%sdata/%s.vis", GetWptDir (), GetMapName ()); // corresponding to waypoint vistable
   sErasingFile[3] = VarArgs ("%sdata/%s.pmt", GetWptDir (), GetMapName ()); // corresponding to waypoint path matrix
   sErasingFile[4] = VarArgs ("%sdata/%s.xml", GetWptDir (), GetMapName ()); // corresponding to waypoint xml database

   for (int i = 0; i < 4; i++)
   {
      if (IsFileAbleToOpen (const_cast <char *> (sErasingFile[i]->GetString ())))
      {
         unlink (sErasingFile[i]->GetString ());
         ServerPrint ("File %s, has been deleted from the hard disk", sErasingFile[i]->GetString ());
      }
      else
         ServerPrint ("Unable to open %s", sErasingFile[i]->GetString ());
   }
   Init (); // reintialize points
}

Vector CWaypoint::GetBombPosition (void)
{
   // this function stores the bomb position as a vector

   edict_t *pent = NULL;

   while (!FNullEnt (pent = FIND_ENTITY_BY_CLASSNAME (pent, "grenade")))
   {
      if (strcmp (STRING (pent->v.model) + 9, "c4.mdl") == 0)
         return pent->v.origin;
   }
   return NULLVEC;
}

void CWaypoint::SetLearnJumpWaypoint (void)
{
   SpeechSynth ("movement check ok"); 
   m_bLearnJumpWaypoint = true;
}

CWaypoint::CWaypoint (void)
{
   m_bWaypointPaths = false;
   m_bEndJumpPoint = false;
   m_bRecalcVis = false;
   m_bLearnJumpWaypoint = false;
   m_fTimeJumpStarted = 0.0;

   m_vLearnVelocity = NULLVEC;
   m_vLearnPos = NULLVEC;
   m_iLastJumpWaypoint = -1;
   m_iCacheWaypointIndex = -1;
   m_iCurrVisIndex = 0;

   m_vLastWaypoint = NULLVEC;
   m_fPathTime = 0.0;
   m_fDangerTime = 0.0;

   m_bOnLadder = false;

   m_arTerrorPoints.Cleanup ();
   m_arCTPoints.Cleanup ();
   m_arGoalPoints.Cleanup ();
   m_arCampPoints.Cleanup ();
   m_arRescuePoints.Cleanup ();

   m_pDistMatrix = NULL;
   m_pPathMatrix = NULL;
}

CWaypoint::~CWaypoint (void)
{
   if (m_pDistMatrix != NULL)
      delete [] m_pDistMatrix;

   if (m_pPathMatrix != NULL)
      delete [] m_pPathMatrix;
}