//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//

#include "yapb.h"
#include "util_yapb.h"

CBotManager::CBotManager (void)
{
   // this is a bot manager class constructor
   
   memset (m_rgpBots, 0, sizeof (m_rgpBots));
   InitQuota ();
}

CBotManager::~CBotManager (void)
{
   // this is a bot manager class destructor
   // do not use MaxClients () here !!
   
   for (int i = 0; i < 32; i++)
   {
      if (m_rgpBots[i])
      {
         delete m_rgpBots[i];
         m_rgpBots[i] = NULL;
      }
   }
}

void CBotManager::CallGameEntity (entvars_t *pVars)
{
   // this function calls gamedll player() function, in case to create player entity in game
   
   if (g_bIsMetamod)
   {
      CALL_GAME_ENTITY (PLID, "player", pVars);
      return;
   }

   ENTITY_FN kPlayer = NULL;

   if (kPlayer == NULL)
      kPlayer = (ENTITY_FN) GetProcSymbol (g_hGameLib, "player");

   if (kPlayer != NULL)
      (*kPlayer) (pVars);
}

int CBotManager::CreateBot (string sName, int iSkill, int iPersonality, int iTeam, int iClass)
{
   // this function completely prepares bot entity (edict) for creation, creates team, skill, sets name etc, and
   // then sends result to bot constructor

   edict_t *pentBot = NULL;
   char szOutputName[33];

   if (g_iNumWaypoints < 1) // don't allow creating bots with no waypoints loaded
   {
      CenterPrint ("Map not waypointed. Can't Create Bot");
      return 0;
   }
   else if (g_bWaypointsChanged) // don't allow creating bots with changed waypoints (distance tables are messed up)
   {
      CenterPrint ("Waypoints has been changed. Load waypoints again...");
      return 0;
   }

   if ((iSkill < 0) || (iSkill > 100))
      iSkill = RandomLong (g_rgpcvBotCVar[CVAR_MINSKILL]->GetInt (), g_rgpcvBotCVar[CVAR_MAXSKILL]->GetInt ());

   if ((iSkill > 100) || (iSkill < 0))
      iSkill = RandomLong (0, 100);

   if ((iPersonality < 0) || (iPersonality > 2))
   {
      int iRandomPrecent = RandomLong (0, 100);

      if (iRandomPrecent < (g_rgpcvBotCVar[CVAR_HARCOREMODE]->GetBool () ? 35 : 50))
         iPersonality = PERSONALITY_NORMAL;
      else
      {
         if (RandomLong (0, iRandomPrecent) < (iRandomPrecent / (g_rgpcvBotCVar[CVAR_HARCOREMODE]->GetBool () ? 4 : 2)))
            iPersonality = PERSONALITY_DEFENSIVE;
         else
            iPersonality = PERSONALITY_AGRESSIVE;
      }
   }

   // setup name
   if (sName.IsEmpty ())
   {
      if (!g_arBotNames.IsFree ())
      {
         bool bNameFound = false;

         for (int i = 0; i < 8; i++)
         {
            if (bNameFound)
               break;

            tBotName *pName = &g_arBotNames.Random ();

            if (!pName || pName->bUsed)
               continue;

            pName->bUsed = bNameFound = true;
            strcpy (szOutputName, pName->sName);
         }
      }
      else
         sprintf (szOutputName, "bot%d", RandomLong (0, 100)); // just pick ugly random name
   }
   else 
      strncpy (szOutputName, sName, 21);

   if (!IsNullString (g_rgpcvBotCVar[CVAR_PREFIX]->GetString ()) || g_rgpcvBotCVar[CVAR_SKILLTAGS]->GetBool ())
   {
      char szPrefixedName[33]; // temp buffer for storing modified name

      if (!IsNullString (g_rgpcvBotCVar[CVAR_PREFIX]->GetString ()))
         sprintf (szPrefixedName, "%s %s", g_rgpcvBotCVar[CVAR_PREFIX]->GetString (), szOutputName);

      else if (g_rgpcvBotCVar[CVAR_SKILLTAGS]->GetBool ())
         sprintf  (szPrefixedName, "%s (%d)", szOutputName, iSkill);

      else if (!IsNullString (g_rgpcvBotCVar[CVAR_PREFIX]->GetString ()) && g_rgpcvBotCVar[CVAR_SKILLTAGS]->GetBool ())
         sprintf  (szPrefixedName, "%s %s (%d)", g_rgpcvBotCVar[CVAR_PREFIX]->GetString (), szOutputName, iSkill);
   
      // buffer has been modified, copy to real name
      if (!IsNullString (szPrefixedName))
         sprintf (szOutputName, szPrefixedName);
   }

   if (FNullEnt ((pentBot = (*g_engfuncs.pfnCreateFakeClient) (szOutputName))))
   {
      CenterPrint ("Maximum players reached (%d/%d). Unable to create Bot.", MaxClients (), MaxClients ());
      return 2;
   }

   int iIndex = ENTINDEX (pentBot) - 1;

   InternalAssert ((iIndex >= 0) && (iIndex <= 32)); // check index
   InternalAssert (m_rgpBots[iIndex] == NULL);   // check bot slot

   m_rgpBots[iIndex] = new CBot (pentBot, iSkill, iPersonality, iTeam, iClass);

   if (m_rgpBots == NULL)
      TerminateOnMalloc ();

   //if (g_rgpcvBotCVar[CVAR_DEVELOPER]->GetBool ())
   //   ServerPrint ("Connecting '%s'... (Skill %d)", STRING (pentBot->v.netname), iSkill);
   //else 
   //   ServerPrint ("Connecting YaPB... (Skill %d)", iSkill);

   

   return 1;
}

int CBotManager::GetIndex (edict_t *pent)
{
   // this function returns index of bot (using own bot array)

   if (FNullEnt (pent))
      return -1;

   int iIndex = ENTINDEX (pent) - 1;

   if ((iIndex < 0) || (iIndex >= 32))
      return -1;

   if (m_rgpBots[iIndex] != NULL)
      return iIndex;

   return -1; // if no edict, return -1;
}

CBot *CBotManager::GetBot (int iIndex)
{
   // this function finds a bot specified by index, and then returns pointer to it (using own bot array)

   if ((iIndex < 0) || (iIndex >= 32))
      return NULL;

   if (m_rgpBots[iIndex] != NULL)
      return m_rgpBots[iIndex];

   return NULL; // no bot
}

CBot *CBotManager::GetBot (edict_t *pent)
{
   // same as above, but using bot entity

   return GetBot (GetIndex (pent));
}

CBot *CBotManager::FindOneValidAliveBot (void)
{
   // this function finds one bot, alive bot :)

   for (int i = 0; i < MaxClients (); i++)
   {
      if ((m_rgpBots[i] != NULL) && IsAlive (m_rgpBots[i]->GetEntity ()))
         return GetBot (i);
   }
   return NULL;
}

void CBotManager::Think (void)
{
   // this function calls think () function for all available at call moment bots, and
   // try to catch internal error if such shit occurs

   for (int i = 0; i < MaxClients (); i++)
   {
      if (m_rgpBots[i] != NULL)
      {
         // use these try-catch blocks to prevent server crashes when error occurs
#if defined (NDEBUG) && !defined (_DEBUG)
         try
         {
            m_rgpBots[i]->Think ();
         }
         catch (...)
         {
            // error occured. kick off all bots and then print a warning message
            RemoveAll ();

            ServerPrintNoTag ("**** INTERNAL BOT ERROR! PLEASE SHUTDOWN AND RESTART YOUR SERVER! ****");
         }
#else
         m_rgpBots[i]->Think ();
#endif
      }
   }
}

void CBotManager::AddBot (string sName, int iSkill, int iPersonality, int iTeam, int iClass)
{
   // this function putting bot creation process to queue to prevent engine crashes

   tCreateQueue kId;

   // fill the holder
   kId.sName = sName;
   kId.iSkill = iSkill;
   kId.iPersonality = iPersonality;
   kId.iTeam = iTeam;
   kId.iClass = iClass;

   // put to queue
   m_arCreationTab.AddItem (kId);

   // keep quota number up to date
   if (NumBots () + 1 > g_rgpcvBotCVar[CVAR_QUOTA]->GetInt ())
      CVarSetFloat (g_rgpcvBotCVar[CVAR_QUOTA]->GetName (), NumBots () + 1);
}

void CBotManager::AddBot (string sName, string sSkill, string sPersonality, string sTeam, string sClass)
{
   // this function is same as the function, above but accept as parameters string instead of integers

   tCreateQueue kId;

   kId.sName = (sName.IsEmpty () || (sName == "*")) ?  string ("\0") : sName;
   kId.iSkill = (sSkill.IsEmpty () || (sSkill == "*")) ? -1 : sSkill.GetInteger ();
   kId.iTeam = (sTeam.IsEmpty () || (sTeam == "*")) ? -1 : sTeam.GetInteger ();
   kId.iClass = (sClass.IsEmpty () || (sClass == "*")) ? -1 : sClass.GetInteger ();
   kId.iPersonality = (sPersonality.IsEmpty () || (sPersonality == "*")) ? -1 : sPersonality.GetInteger ();

   m_arCreationTab.AddItem (kId);

   // keep quota number up to date
   if (NumBots () + 1 > g_rgpcvBotCVar[CVAR_QUOTA]->GetInt ())
      CVarSetFloat (g_rgpcvBotCVar[CVAR_QUOTA]->GetName (), NumBots () + 1);
}

void CBotManager::MaintainBotQuota (void)
{
   if (!m_arCreationTab.IsFree () && (m_fMaintainTime >= WorldTime ()))
   {
      tCreateQueue kLast = m_arCreationTab.PopItem ();
      int iResultOfCreation = CreateBot (kLast.sName, kLast.iSkill, kLast.iPersonality, kLast.iTeam, kLast.iClass);
      
      // check the result of creation
      if (iResultOfCreation == 0)
      {
         m_arCreationTab.Cleanup (); // something worng with waypoints, reset tab of creation
         CVarSetFloat (g_rgpcvBotCVar[CVAR_QUOTA]->GetName (), 0); // reset quota
      }
      else if (iResultOfCreation == 2)
      {
         m_arCreationTab.Cleanup (); // maximum players reached, so set quota to maximum players
         CVarSetFloat (g_rgpcvBotCVar[CVAR_QUOTA]->GetName (), NumBots ());
      }
   }

   // now keep bot number up to date
   if (m_fMaintainTime < WorldTime ())
   {
      int iBotNumber = NumBots ();
      int iHumNumber = NumHumans ();

      if (iBotNumber > g_rgpcvBotCVar[CVAR_QUOTA]->GetInt ())
         RemoveRandom ();

      if (g_rgpcvBotCVar[CVAR_AUTOVACATE]->GetBool ())
      {
         if ((iBotNumber < g_rgpcvBotCVar[CVAR_QUOTA]->GetInt ()) && (iBotNumber < MaxClients () - 1))
            AddRandom ();

         if (iHumNumber >= MaxClients ())
            RemoveRandom ();
      }
      else
      {
         if ((iBotNumber < g_rgpcvBotCVar[CVAR_QUOTA]->GetInt ()) && (iBotNumber < MaxClients ()))
            AddRandom ();
      }

      int iBotQuota = g_rgpcvBotCVar[CVAR_AUTOVACATE]->GetBool () ? (MaxClients () - (iHumNumber + 1)): MaxClients ();

      // check valid range of quota
      if (g_rgpcvBotCVar[CVAR_QUOTA]->GetInt () > iBotQuota)
         CVarSetFloat (g_rgpcvBotCVar[CVAR_QUOTA]->GetName (), iBotQuota);

      if ((g_iNumWaypoints < 1) || g_bWaypointsChanged)
         m_fMaintainTime = 9999.0;
      else
         m_fMaintainTime = WorldTime () + 0.5;
   }
}

void CBotManager::InitQuota (void)
{
   m_fMaintainTime = WorldTime () + 2.0;
   m_arCreationTab.Cleanup ();
}

void CBotManager::FillServer (int iSelection, int iPersonality)
{
   // this function fill server with bots, with specified team & personality

   if ((iSelection == 1) || (iSelection == 2))
   {
      CVarSetString ("mp_limitteams", "0");
      CVarSetString ("mp_autoteambalance", "0");
   }
   else
      iSelection = 5;
   
   char szItem[6][12] =
   {
      "",
      {"Terrorists"},
      {"CTs"},
      "",
      "",
      {"Random"},
   };

   for (int i = 0; i < MaxClients (); i++)
      AddBot ("", -1, iPersonality, iSelection, -1);
   
   CVarSetFloat (g_rgpcvBotCVar[CVAR_QUOTA]->GetName (), MaxClients ());
   CenterPrint ("Fill Server with %s bots...", &szItem[iSelection][0]);
}

void CBotManager::RemoveAll (void)
{
   // this function drops all bot clients from server (this function removes only yapb's)

   for (int i = 0; i < MaxClients (); i++)
   {
      if (m_rgpBots[i] != NULL)  // is this slot used?
         m_rgpBots[i]->Kick ();
   }
   m_arCreationTab.Cleanup ();

   // reset cvars
   CVarSetFloat (g_rgpcvBotCVar[CVAR_QUOTA]->GetName (), 0);
   CVarSetFloat (g_rgpcvBotCVar[CVAR_AUTOVACATE]->GetName (), 0);
  
   CenterPrint ("Bots are removed from server.");   
}

void CBotManager::RemoveFromTeam (eTeam iTeam, bool bAll)
{
   // this function remove random bot from specified team (if bAll value = 1 then removes all players from team)

   for (int i = 0; i < MaxClients (); i++)
   {
      if ((GetBot (i) != NULL) && (iTeam == GetTeam (GetBot (i)->GetEntity ())))
      {
         GetBot (i)->Kick ();

         if (!bAll)
            break;
      }
   }
}

void CBotManager::RemoveMenu (edict_t *pent, int iMenuNum)
{
   // this function displays remove bot menu to specified entity (this function show's only yapb's)
   // code from PB-MM

   if ((iMenuNum > 4) || (iMenuNum < 1))
      return;

   char szTemp[1024], szTmp[1024];

   memset (szTemp, 0, sizeof (szTemp));
   memset (szTmp, 0, sizeof (szTmp));

   int iValidSlots = (iMenuNum == 4) ? (1 << 9) : ((1 << 8) | (1 << 9));

   for (int i = ((iMenuNum - 1) * 8); i < iMenuNum * 8; i++)
   {
      if ((m_rgpBots[i] != NULL) && !FNullEnt (m_rgpBots[i]->GetEntity ()))
      {
         iValidSlots |= 1 << (i - ((iMenuNum - 1) * 8));
         sprintf (szTmp, "%s %1.1d. %s%s\n", szTmp, i - ((iMenuNum - 1) * 8) + 1, STRING (m_rgpBots[i]->pev->netname), GetTeam (m_rgpBots[i]->GetEntity ()) == TEAM_CT ? " \\y(CT)\\w" : " \\r(T)\\w");
      }
      else
         sprintf (szTmp, "%s\\d %1.1d. Not a YaPB\\w\n", szTmp, i - ((iMenuNum - 1) * 8) + 1);
   }

   sprintf (szTemp, "\\yYaPB Remove Menu (%d/4):\\w\n\n%s\n%s 0. Back", iMenuNum, szTmp, (iMenuNum == 4) ? "" : " 9. More...\n");
  
   switch (iMenuNum)
   {
   case 1:
      g_rgkMenu[14].iValidSlots = iValidSlots & static_cast <unsigned int> (-1);
      g_rgkMenu[14].szMenuText = szTemp;
      ShowMenu (pent, &g_rgkMenu[14]);
      break;

   case 2:
      g_rgkMenu[15].iValidSlots = iValidSlots & static_cast <unsigned int> (-1);
      g_rgkMenu[15].szMenuText = szTemp;
      ShowMenu (pent, &g_rgkMenu[15]);
      break;

    case 3:
      g_rgkMenu[16].iValidSlots = iValidSlots & static_cast <unsigned int> (-1);
      g_rgkMenu[16].szMenuText = szTemp;
      ShowMenu (pent, &g_rgkMenu[16]);
      break;

   case 4:
      g_rgkMenu[17].iValidSlots = iValidSlots & static_cast <unsigned int> (-1);
      g_rgkMenu[17].szMenuText = szTemp;
      ShowMenu (pent, &g_rgkMenu[17]);
      break;
   }
}

void CBotManager::KillAll (int iTeam)
{
   // this function kills all bots on server (only this dll contrlled bots)

   for (int i = 0; i < MaxClients (); i++)
   {
      if (m_rgpBots[i] != NULL)
      {
         if ((iTeam != -1) && (iTeam != GetTeam (m_rgpBots[i]->GetEntity ())))
            continue;

         m_rgpBots[i]->Kill ();
      }
   }
   CenterPrint ("All bots are killed.");
}

void CBotManager::RemoveRandom (void)
{
   // this function removes random bot from server (only yapb's)

   for (int i = 0; i < MaxClients (); i++)
   {
      if (m_rgpBots[i] != NULL)  // is this slot used?
      {
         m_rgpBots[i]->Kick ();
         break;
      }
   }
}

void CBotManager::SetWeaponMode (int iSelection)
{
   // this function sets bots weapon mode

   int rgiWeaponTabStandard[7][NUM_WEAPONS] =
   {
      {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Knife only
      {-1,-1,-1, 2, 2, 0, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Pistols only
      {-1,-1,-1,-1,-1,-1,-1, 2, 2,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Shotgun only
      {-1,-1,-1,-1,-1,-1,-1,-1,-1, 2, 1, 2, 0, 2,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 2,-1}, // Machine Guns only
      {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 0, 1, 0, 1, 1,-1,-1,-1,-1,-1,-1}, // Rifles only
      {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 2, 2, 0, 1,-1,-1}, // Snipers only
      {-1,-1,-1, 2, 2, 0, 1, 2, 2, 2, 1, 2, 0, 2, 0, 0, 1, 0, 1, 1, 2, 2, 0, 1, 2, 1}  // Standard
   };

   int rgiWeaponTabAS[7][NUM_WEAPONS] =
   {
      {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Knife only
      {-1,-1,-1, 2, 2, 0, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Pistols only
      {-1,-1,-1,-1,-1,-1,-1, 1, 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}, // Shotgun only
      {-1,-1,-1,-1,-1,-1,-1,-1,-1, 1, 1, 1, 0, 2,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 1,-1}, // Machine Guns only
      {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0,-1, 1, 0, 1, 1,-1,-1,-1,-1,-1,-1}, // Rifles only
      {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 0,-1, 1,-1,-1}, // Snipers only
      {-1,-1,-1, 2, 2, 0, 1, 1, 1, 1, 1, 1, 0, 2, 0,-1, 1, 0, 1, 1, 0, 0,-1, 1, 1, 1}  // Standard
   }; 

   char szMode[7][12] =
   {
      {"Knife"},
      {"Pistol"},
      {"Shotgun"},
      {"Machine Gun"},
      {"Rifle"},
      {"Sniper"},
      {"Standard"}
   };
   
   iSelection--;
   for (int i = 0; i < NUM_WEAPONS; i++)
   {
      g_rgkWeaponSelect[i].iTeamStandard = rgiWeaponTabStandard[iSelection][i];
      g_rgkWeaponSelect[i].iTeamAS = rgiWeaponTabAS[iSelection][i];
   }

   if (iSelection == 0)
      CVarSetFloat (g_rgpcvBotCVar[CVAR_KNIFEMODE]->GetName (), 1);
   else
      CVarSetFloat (g_rgpcvBotCVar[CVAR_KNIFEMODE]->GetName (), 0);

   CenterPrint ("%s weapon mode selected", &szMode[iSelection][0]);
}

void CBotManager::List (void)
{
   // this function list's bots currently playing on the server

   edict_t *pentPlayer;
   ServerPrintNoTag ("%-3.5s %-9.13s %-17.18s %-3.4s %-3.4s %-3.4s", "index", "name", "personality", "team", "skill", "frags");

   for (int i = 0; i < MaxClients (); i++)
   {
      pentPlayer = INDEXENT (i);

      // is this player slot valid
      if (!FNullEnt (pentPlayer) && IsValidBot (pentPlayer))  
         ServerPrintNoTag ("[%-3.1d] %-9.13s %-17.18s %-3.4s %-3.1d %-3.1d", i, STRING (pentPlayer->v.netname), GetBot (pentPlayer)->m_ucPersonality == PERSONALITY_AGRESSIVE ? "agressive" : GetBot (pentPlayer)->m_ucPersonality == PERSONALITY_NORMAL ? "normal" : "defensive", GetTeam (pentPlayer) != 0 ? "CT" : "T", GetBot (pentPlayer)->m_iSkill, (int)pentPlayer->v.frags);
   }
}

int CBotManager::NumBots (void)
{
   // this function returns number of yapb's playing on the server

   int iCount = 0;

   for (int i = 0; i < MaxClients (); i++)
   {
      if (m_rgpBots[i] != NULL)
         iCount++;
   }
   return iCount;
}

int CBotManager::NumHumans (void)
{
   // this function returns number of humans playing on the server

   int iCount = 0;

   for (int i = 0; i < MaxClients (); i++)
   {
      if ((g_rgkClients[i].iFlags & CFLAG_USED) && (m_rgpBots[i] == NULL))
         iCount++;
   }
   return iCount;
}


CBot *CBotManager::GetHighestFragsBot (int iTeam)
{
   CBot *pFragBot = NULL;

   int iBestIndex = 0;
   float fBestFrags = -1;

   // Search Bots in this team
   for (int iBotIndex = 0; iBotIndex < MaxClients (); iBotIndex++)
   {
      pFragBot = g_pBotManager->GetBot (iBotIndex);
      
      if ((pFragBot != NULL) && IsAlive (pFragBot->GetEntity ()) && (GetTeam (pFragBot->GetEntity ()) == iTeam))
      {
         if (pFragBot->pev->frags > fBestFrags)
         {
            iBestIndex = iBotIndex;
            fBestFrags = pFragBot->pev->frags;
         }
      }
   }
   return GetBot (iBestIndex);
}

bool CBotManager::IsTeamEconomicsBad (int iTeam)
{
   // this function decides is players on specified team is able to buy primary weapons
   // by calculating players that have not enough money to buy primary (with economics),
   // and if this result higher 75%, player is can't buy primary weapons.

   if ((g_fTimeRoundStart + 20.0 < WorldTime ()) || !g_rgpcvBotCVar[CVAR_ECONOMICS]->GetBool ())
      return false; // don't check economics while entering buyzone whem moving around the map

   int iNumPlayersWithNoMoney = 0;
   int iNumPlayersInTeam = 0;

   // start calculating
   for (int i = 0; i < MaxClients (); i++)
   {
      if ((GetBot (i) != NULL) && (GetTeam (GetBot (i)->GetEntity ()) == iTeam) && !GetBot (i)->m_bBuyingFinished)
      {
         if (GetBot (i)->m_iAccount < g_rgiBotBuyEconomyTable[0])
            iNumPlayersWithNoMoney++;

         iNumPlayersInTeam++; // update count of team
      }
   }

   // if 75 precent of team have no enough money to purchase primary weapon
   if (iNumPlayersWithNoMoney > ((iNumPlayersInTeam / 2) + (iNumPlayersInTeam / 4)))
      return true;

   return false;
}

void CBotManager::Free (void)
{
   // this function free all bots slots (used on server shutdown)

   for (int i = 0; i < 32; i++)
   {
      if (m_rgpBots[i] != NULL)
      {
         delete m_rgpBots[i];
         m_rgpBots[i] = NULL;
      }
   }
}

void CBotManager::Free (int iIndex)
{
   // this function frees one bot selected by index (used on bot disconnect)

   delete m_rgpBots[iIndex];
   m_rgpBots[iIndex] = NULL;
}

CBot::CBot (edict_t *pentBot, int iSkill, int iPersonality, int iTeam, int iClass)
{
   // this function does core operation of creating bot, it's called by CreateBot (),
   // when bot setup completed, (this is a bot class constructor)
      
   char szRejectReason[128];
   int iClientIndex = ENTINDEX (pentBot);

   memset (this, 0, sizeof (CBot));

   pev = VARS (pentBot);

   if (pentBot->pvPrivateData != NULL)
      FREE_PRIVATE (pentBot);
   
   pentBot->pvPrivateData = NULL;
   pentBot->v.frags = 0;

   // create the player entity by calling MOD's player function
   CBotManager::CallGameEntity (&pentBot->v);
   
   // set all infobuffer keys for this bot
   char *pszBuffer = GET_INFOKEYBUFFER (pentBot);

   SET_CLIENT_KEYVALUE (iClientIndex, pszBuffer, "model", "");
   SET_CLIENT_KEYVALUE (iClientIndex, pszBuffer, "rate", "3500.000000");
   SET_CLIENT_KEYVALUE (iClientIndex, pszBuffer, "cl_updaterate", "20");
   SET_CLIENT_KEYVALUE (iClientIndex, pszBuffer, "cl_lw", "1");
   SET_CLIENT_KEYVALUE (iClientIndex, pszBuffer, "cl_lc", "1");
   SET_CLIENT_KEYVALUE (iClientIndex, pszBuffer, "tracker", "0");
   SET_CLIENT_KEYVALUE (iClientIndex, pszBuffer, "cl_dlmax", "128");
   SET_CLIENT_KEYVALUE (iClientIndex, pszBuffer, "friends", "0");
   SET_CLIENT_KEYVALUE (iClientIndex, pszBuffer, "dm", "0");
   SET_CLIENT_KEYVALUE (iClientIndex, pszBuffer, "_ah", "0");
   // SET_CLIENT_KEYVALUE (iClientIndex, pszBuffer, "*bot", "1");
   
   if (g_iCSVersion != VERSION_WON)
      SET_CLIENT_KEYVALUE (iClientIndex, pszBuffer, "_vgui_menus", "0");
    
   szRejectReason[0] = 0; // reset the reject reason template string
   MDLL_ClientConnect (pentBot, "BOT", VarArgs ("127.0.0.%d", ENTINDEX (pentBot) + 100), szRejectReason);
   
   if (!IsNullString (szRejectReason))
   {
      LogToFile (true, LOG_WARNING, "Server refused '%s' connection (%s)", STRING (pentBot->v.netname), szRejectReason);         
      ServerCommand ("kick \"%s\"", STRING (pentBot->v.netname)); // kick the bot player if the server refused it
      
      pentBot->v.flags |= FL_KILLME;
   }
   
   if (IsDedicatedServer () && g_rgpcvBotCVar[CVAR_DEVELOPER]->GetBool ())
   {
      if (g_rgpcvBotCVar[CVAR_DEVELOPER]->GetInt () == 2)
      {          
          ServerPrint ("Server requiring authentication");
          ServerPrint ("Client '%s' connected", STRING (pentBot->v.netname));
          ServerPrint ("Adr: 127.0.0.%d:27005", ENTINDEX (pentBot) + 100);
      }
      
      ServerPrint ("Verifying and uploading resources...");
      ServerPrint ("Custom resources total 0 bytes");
      ServerPrint ("  Decals:  0 bytes");
      ServerPrint ("----------------------");
      ServerPrint ("Resources to request: 0 bytes");
   }
   MDLL_ClientPutInServer (pentBot);

   // initialize all the variables for this bot...
   m_bNotStarted = true;  // hasn't joined game yet

   m_iStartAction = MESSAGE_IDLE;
   m_iAccount = 0;

   m_iSprayLogo = RandomLong (0, g_arSprayNames.Size ()); // assign a random spraypaint
   m_iVoicePitch = RandomLong (166, 250) * 0.5; // assign voice pitch
 
   // assign how talkative this bot will be
   m_sSaytextBuffer.fChatDelay = RandomFloat (3.8, 10.0);
   m_sSaytextBuffer.cChatProbability = RandomLong (1, 100);

   m_bNotKilled = false;
   m_iSkill = iSkill;
   m_iWeaponBurstMode = BMSL_DISABLED;

   pentBot->v.idealpitch = pentBot->v.v_angle.x;
   pentBot->v.ideal_yaw = pentBot->v.v_angle.y;

   pentBot->v.yaw_speed = RandomFloat (g_rgkSkillTab[m_iSkill / 20].fMinTurnSpeed, g_rgkSkillTab[m_iSkill / 20].fMaxTurnSpeed);
   pentBot->v.pitch_speed = RandomFloat (g_rgkSkillTab[m_iSkill / 20].fMinTurnSpeed, g_rgkSkillTab[m_iSkill / 20].fMaxTurnSpeed);

   switch (iPersonality)
   {
   case 1:
      m_ucPersonality = PERSONALITY_AGRESSIVE;
      m_fBaseAgressionLevel = RandomFloat (0.7, 1.0);
      m_fBaseFearLevel = RandomFloat (0.0, 0.4);
      break;

   case 2:
      m_ucPersonality = PERSONALITY_DEFENSIVE;
      m_fBaseAgressionLevel = RandomFloat (0.0, 0.4);
      m_fBaseFearLevel = RandomFloat (0.7, 1.0);
      break;

   default:
      m_ucPersonality = PERSONALITY_NORMAL;
      m_fBaseAgressionLevel = RandomFloat (0.4, 0.7);
      m_fBaseFearLevel = RandomFloat (0.4, 0.7);
      break;
   }

   memset (&m_rgAmmoInClip, 0, sizeof (m_rgAmmoInClip));
   memset (&m_rgAmmo, 0, sizeof (m_rgAmmo));
   
   m_iCurrentWeapon = 0;

   // copy them over to the temp level variables
   m_fAgressionLevel = m_fBaseAgressionLevel;
   m_fFearLevel = m_fBaseFearLevel;
   m_fNextEmotionUpdate = WorldTime () + 0.5;

   // just to be sure
   m_iActMessageIndex = 0;
   m_iPushMessageIndex = 0;

   // assign team & class
   m_iWantedTeam = iTeam;
   m_iWantedClass = iClass;

   NewRound ();
}

CBot::~CBot (void)
{
   // this is bot destructor
 
   DeleteSearchNodes ();
   ResetTasks ();
   SwitchChatterIcon (false);

   // free used botname
   ITERATE_ARRAY (g_arBotNames, j)
   {
      if (strcmp (g_arBotNames[j].sName, STRING (pev->netname)) == 0)
      {
         g_arBotNames[j].bUsed = false;
         break;
      }
   }
}

void CBot::NewRound (void) 
{
   // this function initializes a bot after creation & at the start of each round
      
   int i;

   // delete all allocated path nodes
   DeleteSearchNodes ();

   m_vWptOrigin = NULLVEC;
   m_vDestOrigin = NULLVEC;
   m_iCurrWptIndex = -1;
   m_uiCurrTravelFlags = 0;
   m_vDesiredVelocity = NULLVEC;
   m_iPrevGoalIndex = -1;
   m_iChosenGoalIndex = -1;
   
   m_rgiPrevWptIndex[0] = -1;
   m_rgiPrevWptIndex[1] = -1;
   m_rgiPrevWptIndex[2] = -1;
   m_rgiPrevWptIndex[3] = -1;
   m_rgiPrevWptIndex[4] = -1;
   
   m_fWptTimeset = WorldTime ();

   switch (m_ucPersonality)
   {
   case PERSONALITY_NORMAL:
      m_byPathType = RandomLong (0, 100) > 50 ? 1 : 0;
      break;

   case PERSONALITY_AGRESSIVE:
      m_byPathType = 0;
      break;

   case PERSONALITY_DEFENSIVE:
      m_byPathType = 1;
      break;
   }

   // clear all states & tasks
   m_iStates = 0;
   ResetTasks ();

   m_bIsVIP = false;
   m_bIsLeader = false;
   m_bHasProgressBar = false;

   m_fTimeTeamOrder = 0.0;
   m_fMinSpeed = 260.0;
   m_fPrevSpeed = 0.0;  // fake "paused" since bot is NOT stuck
   m_vPrevOrigin = Vector (9999.0, 9999.0, 9999.0);
   m_fPrevTime = WorldTime ();

   m_fViewDistance = 4096.0;
   m_fMaxViewDistance = 4096.0;
   
   m_pentPickupItem = NULL;
   m_pentItemIgnore = NULL;
   m_fItemCheckTime = 0.0;

   m_pentShootBreakable = NULL;
   m_vBreakable = NULLVEC;
   m_fTimeDoorOpen = 0.0;

   ResetCollideState ();
   ResetDoubleJumpState ();

   m_pentEnemy = NULL;
   m_pentLastVictim = NULL;
   m_pentLastEnemy = NULL;
   m_vLastEnemyOrigin = NULLVEC;
   m_pentTrackingEdict = NULL;
   m_fTimeNextTracking = 0.0;

   m_fEnemyUpdateTime = 0.0;
   m_fSeeEnemyTime = 0.0;
   m_fShootAtDeadTime = 0.0;
   m_fOldCombatDesire = 0.0;

   m_pentAvoidGrenade = NULL;
   m_cAvoidGrenade = 0;

   m_iLastDamageType = -1; // reset damage
   m_iVoteKickIndex = 0;
   m_iLastVoteKick = 0;
   m_iVoteMap = 0;
   m_iCountOpenDoor = 0;
   m_iAimFlags = 0;

   m_vPosition = NULLVEC;

   m_fIdealReactionTime = g_rgkSkillTab[m_iSkill / 20].fMinSurpriseTime;
   m_fActualReactionTime = g_rgkSkillTab[m_iSkill / 20].fMinSurpriseTime;

   m_pentTargetEnt = NULL;
   m_fFollowWaitTime = 0.0;

   for (i = 0; i < MAX_HOSTAGES; i++)
      m_rgpHostages[i] = NULL;

   for (i = 0; i < VOICE_TOTAL; i++)
      m_fVoiceTime[i] = -1.0;

   m_bIsReloading = false;
   m_iReloadState = RELOAD_NONE;

   m_fReloadCheckTime = 0.0;
   m_fShootTime = WorldTime ();
   m_fPlayerTargetTime = WorldTime ();
   m_fTimeFirePause = 0.0;
   m_fTimeLastFired = 0.0;

   m_fGrenadeCheckTime = 0.0;
   m_bUsingGrenade = false;
   m_bTeamEconomicsGood = !g_pBotManager->IsTeamEconomicsBad (GetTeam (GetEntity ()));

   m_fBlindTime = 0.0;
   m_fJumpTime = 0.0;
   m_bJumpDone = false;

   m_sSaytextBuffer.fTimeNextChat = WorldTime ();
   m_sSaytextBuffer.iEntityIndex = -1;
   m_sSaytextBuffer.szSayText[0] = 0x0;

   m_iBuyCount = 0;

   if (!m_bNotKilled) // if bot died, clear all weapon stuff and force buying again
   {
      memset (&m_rgAmmoInClip, 0, sizeof (m_rgAmmoInClip));
      memset (&m_rgAmmo, 0, sizeof (m_rgAmmo));

      m_iCurrentWeapon = 0;
   }

   m_fKnifeAttackTime = WorldTime () + RandomFloat (1.3, 2.6);
   m_fNextBuyTime = WorldTime () + RandomFloat (2.0, 2.8);

   m_bBuyPending = false;
   m_bInBombZone = false;

   m_fShieldCheckTime = 0.0;
   m_fZoomCheckTime = 0.0;
   m_fStrafeSetTime = 0.0;
   m_ucCombatStrafeDir = 0;
   m_ucFightStyle = 0;
   m_fLastFightStyleCheck = 0.0;

   m_bCheckWeaponSwitch = true;
   m_bCheckKnifeSwitch = true;
   m_bBuyingFinished = false;

   m_pentRadioEntity = NULL;
   m_iRadioOrder = 0;
   m_bDefendedBomb = false;

   m_fTimeLogoSpray = WorldTime () + RandomFloat (0.5, 2.0);
   m_fSpawnTime = WorldTime ();
   m_fLastChatTime = WorldTime ();
   pev->v_angle.y = pev->ideal_yaw;

   m_fTimeCamping = 0;
   m_iCampDirection = 0;
   m_fNextCampDirTime = 0;
   m_iCampButtons = 0;
   m_iLadderDir = 0;

   m_fSoundUpdateTime = 0.0;
   m_fHeardSoundTime = WorldTime ();

   // clear its message queue
   for (i = 0; i < 32; i++)
      m_aMessageQueue[i] = MESSAGE_IDLE;

   m_iActMessageIndex = 0;
   m_iPushMessageIndex = 0;

   // and put buying into its message queue
   PushMessageQueue (MESSAGE_BUY);
   StartTask (TASK_NORMAL, TASKPRI_NORMAL, -1, 0.0, true);
}

void CBot::Kill (void)
{
   // this function kills a bot (not just using ClientKill, but like the CSBot does)
   // base code courtesy of Lazy (from bots-united forums!)
   
   KeyValueData kKeyVal;
   edict_t *pentHurt = (*g_engfuncs.pfnCreateNamedEntity) (MAKE_STRING ("trigger_hurt"));

   if (FNullEnt (pentHurt))
      return;

   pentHurt->v.classname = MAKE_STRING (g_rgkWeaponDefs[m_iCurrentWeapon].szClassname);
   pentHurt->v.dmg_inflictor = GetEntity ();
   pentHurt->v.dmg = 9999.0;
   pentHurt->v.dmg_take = 1.0;
   pentHurt->v.dmgtime = 2.0;
   pentHurt->v.effects |= EF_NODRAW;

   (*g_engfuncs.pfnSetOrigin) (pentHurt, Vector (-4000, -4000, -4000));

   kKeyVal.szClassName = const_cast <char *> (g_rgkWeaponDefs[m_iCurrentWeapon].szClassname);
   kKeyVal.szKeyName = "damagetype";
   kKeyVal.szValue = VarArgs ("%d", (1 << 4));
   kKeyVal.fHandled = false;

   MDLL_KeyValue (pentHurt, &kKeyVal);
   MDLL_Spawn (pentHurt);

   MDLL_Touch (pentHurt, GetEntity ());

   (*g_engfuncs.pfnRemoveEntity) (pentHurt);
}

void CBot::Kick (void)
{
   // this function kick off one bot from the server.

   CenterPrint ("Bot '%s' kicked", STRING (pev->netname));
   ServerCommand ("kick \"%s\"", STRING (pev->netname));  

   // balans quota
   if (g_pBotManager->NumBots () - 1 < g_rgpcvBotCVar[CVAR_QUOTA]->GetInt ())
      CVarSetFloat (g_rgpcvBotCVar[CVAR_QUOTA]->GetName (), g_pBotManager->NumBots () - 1);
}

void CBot::StartGame (void)
{
   // this function handles the selection of teams & class
      
   // handle counter-strike stuff here...
   if (m_iStartAction == MESSAGE_TEAM_SELECT)
   {
      m_iStartAction = MESSAGE_IDLE;  // switch back to idle

      if ((g_rgpcvBotCVar[CVAR_FORCETEAM]->GetString ()[0] == 'C') || (g_rgpcvBotCVar[CVAR_FORCETEAM]->GetString ()[0] == 'c'))
         m_iWantedTeam = 2;
      else if ((g_rgpcvBotCVar[CVAR_FORCETEAM]->GetString ()[0] == 'T') || (g_rgpcvBotCVar[CVAR_FORCETEAM]->GetString ()[0] == 't'))
         m_iWantedTeam = 1;

      if ((m_iWantedTeam != 1) && (m_iWantedTeam != 2))
         m_iWantedTeam = 5;

      // select the team the bot wishes to join...
      FakeClientCommand (GetEntity (), "menuselect %d", m_iWantedTeam);
   }
   else if (m_iStartAction == MESSAGE_CLASS_SELECT)
   {
      m_iStartAction = MESSAGE_IDLE;  // switch back to idle

     if (g_iCSVersion == VERSION_CSCZ) // czero has spetsnaz and militia skins
        if ((m_iWantedClass < 1) || (m_iWantedClass > 5))
           m_iWantedClass = RandomLong (1, 5); //  use random if invalid
     else
        if ((m_iWantedClass < 1) || (m_iWantedClass > 4))
           m_iWantedClass = RandomLong (1, 4); // use random if invalid
       
      // select the class the bot wishes to use...
      FakeClientCommand (GetEntity (), "menuselect %d", m_iWantedClass);

      // bot has now joined the game (doesn't need to be started)
      m_bNotStarted = false;

      // check for greeting other players, since we connected
      if (RandomLong (0, 100) < 20)
         ChatMessage (CHAT_WELCOMING);
   }
}
