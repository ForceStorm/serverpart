//
// Yet Another Ping Of Death Bot - The Counter-Strike Bot based on POD-Bot Code.
// Copyright (c) 2003-2006, by Wei 'Whistler' Mingzhi and Dmitry 'Jeefo' Zhukov.
//
// Original POD-Bot Code is (c) 2000, 2001, by Markus Klinge alias Count-Floyd.
//
// Some code for this bot is taken directly from other POD-Bot clones, like
// Podbot mm and E[POD]bot. So some code is property of authors of this bots.
//
// Also big parts of code are taken for RACC source code, that was created by
// Pierre-Marie Baty.

#ifndef YAPB_INCLUDED
#define YAPB_INCLUDED

#include "extdll.h"
#include "stdio.h"
#include "memory.h"

// detects the build platform
#if defined (__linux__) || defined (__debian__) || defined (__linux)
   #define PLATFORM_LINUX32 1
#elif defined (__x86_64__) || defined (__amd64__)
   #define PLATFORM_LINUX64 1
#elif defined (_WIN32)
   #define PLATFORM_WIN32 1
#endif

// detects the compiler
#if defined (_MSC_VER)
   #define COMPILER_VISUALC _MSC_VER
#elif defined (__BORLANDC__)
   #define COMPILER_BORLAND __BORLANDC__
#elif defined (__MINGW32__)
   #define COMPILER_MINGW32 __MINGW32__
#endif

// configure export macros
#if defined (COMPILER_VISUALC) || defined (COMPILER_MINGW32)
   #define export extern "C" __declspec (dllexport)
#elif defined (PLATFORM_LINUX32) || defined (PLATFORM_LINUX64) || defined (COMPILER_BORLAND)
   #define export extern "C"
#else
   #error "Can't configure export macros. Compiler unrecognized."
#endif

#include "dllapi.h"
#include "meta_api.h"

// operating system specific macros, functions and typedefs
#ifdef PLATFORM_WIN32
 
   #include <direct.h>

   #define DLL_ENTRYPOINT int STDCALL DllMain (HINSTANCE hModule, DWORD dwReason, LPVOID)
   #define DLL_DETACHING (dwReason == DLL_PROCESS_DETACH)
   #define DLL_RETENTRY return TRUE
   
   #if defined (COMPILER_VISUALC)
      #define DLL_GIVEFNPTRSTODLL extern "C" void STDCALL
   #elif defined (COMPILER_MINGW32)
      #define DLL_GIVEFNPTRSTODLL export void STDCALL
   #endif

   // are we under the microsoft visual c compiler
   #if defined (COMPILER_VISUALC) && (COMPILER_VISUALC > 1000) 
      // specify export parameter (c) Jozef Wagner
      #pragma comment (linker,"/EXPORT:GiveFnptrsToDll=_GiveFnptrsToDll@8,@1")
      #pragma comment (linker,"/SECTION:.data,RW")
   #endif

   // dynamic linkents
   #define RVA_TO_VA(kBase, kRva) ((uint32) kBase + (uint32) kRva)
   #define VA_TO_RVA(kBase, kVa) ((uint32) kVa - (uint32) kBase)

   typedef HINSTANCE dynlib_t;
   typedef FARPROC dynsym_t;

   typedef int (FAR *GETENTITYAPI) (DLL_FUNCTIONS *, int);
   typedef int (FAR *GETNEWENTAPI) (NEW_DLL_FUNCTIONS *, int *);
   typedef int (FAR *GETBLENDINGAPI) (int, void **, void *, float (*)[3][4], float (*)[128][3][4]);
   typedef void (STDCALL *GIVEFNPTRSTODLL) (enginefuncs_t *, globalvars_t *);
   typedef void (FAR *ENTITY_FN) (entvars_t *);

   inline dynlib_t OpenLibrary (const char *cszFilename) { return LoadLibrary (cszFilename); }
   inline dynsym_t GetProcSymbol (dynlib_t hLib, const char *cszSymbol) { return GetProcAddress (hLib, cszSymbol); }
   inline int CloseLibrary (dynlib_t hLib) { return FreeLibrary (hLib); }

#elif defined (PLATFORM_LINUX32) || defined (PLATFORM_LINUX64)

   #include <unistd.h>
   #include <dlfcn.h>
   #include <errno.h>
   #include <sys/stat.h>

   #define DLL_ENTRYPOINT void _fini (void)
   #define DLL_DETACHING TRUE
   #define DLL_RETENTRY return
   #define DLL_GIVEFNPTRSTODLL extern "C" void

   typedef void *dynlib_t;
   typedef void *dynsym_t;

   typedef int (*GETENTITYAPI) (DLL_FUNCTIONS *, int);
   typedef int (*GETNEWENTAPI) (NEW_DLL_FUNCTIONS *, int *);
   typedef int (*GETBLENDINGAPI) (int, void **, void *, float (*)[3][4], float (*)[128][3][4]);
   typedef void (*GIVEFNPTRSTODLL) (enginefuncs_t *, globalvars_t *);
   typedef void (*ENTITY_FN) (entvars_t *);

   inline dynlib_t OpenLibrary (const char *cszFilename) { return dlopen (cszFilename, RTLD_NOW); }
   inline dynsym_t GetProcSymbol (dynlib_t hLib, const char *cszSymbol) { return dlsym (hLib, cszSymbol); }
   inline int CloseLibrary (dynlib_t hLib) { return dlclose (hLib); }

#else
   #error "Platform unrecognized."

#endif     

#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <float.h>
#include <time.h>

#include "genclass.h"
#include "mathlib.h"

// defines bots tasks
enum eTask
{
   TASK_NORMAL,
   TASK_PAUSE,
   TASK_MOVETOPOSITION,
   TASK_FOLLOWUSER,
   TASK_WAITFORGO,
   TASK_PICKUPITEM,
   TASK_CAMP,
   TASK_PLANTBOMB,
   TASK_DEFUSEBOMB,
   TASK_ATTACK,
   TASK_ENEMYHUNT,
   TASK_SEEKCOVER,
   TASK_THROWHEGRENADE,
   TASK_THROWFLASHBANG,
   TASK_THROWSMOKEGRENADE,
   TASK_DOUBLEJUMP,
   TASK_ESCAPEFROMBOMB,
   TASK_SHOOTBREAKABLE,
   TASK_HIDE,
   TASK_BLINDED,
   TASK_SPRAYLOGO
};

// msec calculation methods
enum eMethod
{
   METHOD_TOBIAS  = 1, 
   METHOD_PM      = 2, 
   METHOD_RICH    = 3,
   METHOD_LEON    = 4
};
   
// supported cs's
enum eMod
{
   VERSION_STEAM = 1,  // Counter-Strike 1.6 and Above                    
   VERSION_CSCZ  = 2,  // Counter-Strike: Condition Zero
   VERSION_WON   = 3   // Counter-Strike 1.3-1.5 with/without Steam
};

// log levels
enum eLogLevel
{
   LOG_DEFAULT    = 1, // default log message
   LOG_WARNING    = 2, // warning log message
   LOG_ERROR      = 3, // error log message
   LOG_CRITICAL   = 4  // fatal error log message (terminate the game!)
};

// chat types id's
enum eChatType
{
   CHAT_KILLING = 0, // id to kill chat array
   CHAT_DEAD,        // id to dead chat array
   CHAT_BOMBPLANT,   // id to bomb chat array
   CHAT_TEAMATTACK,  // id to team-attack chat array
   CHAT_TEAMKILL,    // id to team-kill chat array
   CHAT_WELCOMING,   // id to welcome chat array
   CHAT_NOKEYWORD,   // id to no keyword chat array
   CHAT_TOTAL        // number for array
};

// personalities defines
enum ePersonality
{
   PERSONALITY_NORMAL = 0,
   PERSONALITY_AGRESSIVE,
   PERSONALITY_DEFENSIVE
};

// collision states
enum eCollision
{
   COLLISION_NOTDECIDED,
   COLLISION_PROBING,
   COLLISION_NOMOVE,
   COLLISION_JUMP,
   COLLISION_DUCK,
   COLLISION_STRAFELEFT,
   COLLISION_STRAFERIGHT,
};

// counter-strike team id's
enum eTeam
{
   TEAM_TERRORIST = 0,
   TEAM_CT
};

// client flags
enum eClientFlags
{
   CFLAG_USED  = (1 << 0),
   CFLAG_ALIVE = (1 << 1),
   CFLAG_ADMIN = (1 << 2)
};

// bot cvars
enum eBotCVar
{
   CVAR_AUTOVACATE      = 0,
   CVAR_QUOTA           = 1,
   CVAR_MINSKILL        = 2,
   CVAR_MAXSKILL        = 3,
   CVAR_FOLLOWUSER      = 4,
   CVAR_TIMERSOUND      = 5,
   CVAR_TIMERPICKUP     = 6,
   CVAR_TIMERGRENADE    = 7,
   CVAR_DEBUGGOAL       = 8,
   CVAR_CHAT            = 9,
   CVAR_SYNTH           = 10,
   CVAR_KNIFEMODE       = 11,
   CVAR_SKILLTAGS       = 12,
   CVAR_STOP            = 13,
   CVAR_THRUWALLS       = 14,
   CVAR_VOTES           = 15,
   CVAR_SPRAY           = 16,
   CVAR_BOTBUY          = 17,
   CVAR_DEBUG           = 18,
   CVAR_TURNMETHOD      = 19,
   CVAR_PREFIX          = 20,
   CVAR_LANGUAGE        = 21,
   CVAR_VERSION         = 22,
   CVAR_DAMPER_COEFF_X  = 23,
   CVAR_DAMPER_COEFF_Y  = 24,
   CVAR_DEVIATION_X     = 25,
   CVAR_DEVIATION_Y     = 26,
   CVAR_INFL_X_ON_Y     = 27,
   CVAR_INFL_Y_ON_X     = 28,
   CVAR_SLOWDOWN_RATIO  = 29,
   CVAR_OFFSET_DELAY    = 30,
   CVAR_SPRING_STIFF_X  = 31,
   CVAR_SPRING_STIFF_Y  = 32,
   CVAR_ANTICIP_RATIO   = 33,
   CVAR_PASSWORD        = 34,
   CVAR_PASSWORDKEY     = 35,
   CVAR_VOICEPATH       = 36,
   CVAR_TKPUNISH        = 37,
   CVAR_COMMUNICATION   = 38,
   CVAR_ECONOMICS       = 39,
   CVAR_FORCETEAM       = 40,
   CVAR_RESTWEAPONS     = 41,
   CVAR_DANGERFACTOR    = 42,
   CVAR_DONTSHOOT       = 43,
   CVAR_HARCOREMODE     = 44,
   CVAR_C4TIMER         = 50,
   CVAR_BUYTIME         = 51,
   CVAR_FRIENDLYFIRE    = 52,
   CVAR_ROUNDTIME       = 53,
   CVAR_GRAVITY         = 54,
   CVAR_DEVELOPER       = 55,
   CVAR_FREEZETIME      = 56,
   CVAR_TOTAL
};

// radio messages
enum eRadio
{
   RADIO_COVERME        = 1,
   RADIO_YOUTAKEPOINT   = 2,
   RADIO_HOLDPOSITION   = 3,
   RADIO_REGROUPTEAM    = 4,
   RADIO_FOLLOWME       = 5,
   RADIO_TAKINGFIRE     = 6,

   RADIO_GOGOGO         = 11,
   RADIO_FALLBACK       = 12,
   RADIO_STICKTOGETHER  = 13,
   RADIO_GETINPOSITION  = 14,
   RADIO_STORMTHEFRONT  = 15,
   RADIO_REPORTTEAM     = 16,

   RADIO_AFFIRMATIVE    = 21,
   RADIO_ENEMYSPOTTED   = 22,
   RADIO_NEEDBACKUP     = 23,
   RADIO_SECTORCLEAR    = 24,
   RADIO_IMINPOSITION   = 25,
   RADIO_REPORTINGIN    = 26,
   RADIO_SHESGONNABLOW  = 27,
   RADIO_NEGATIVE       = 28,
   RADIO_ENEMYDOWN      = 29
};

// voice system (extending enum above, messages 30-39 is reserved)
enum eVoice
{
   VOICE_SPOT_THE_BOMBER = 40,
   VOICE_FRIENDLY_FIRE,
   VOICE_PAIN_ON_DIE,
   VOICE_GET_BLINDED,
   VOICE_GOING_TO_PLANTBOMB,
   VOICE_RESCUING_HOSTAGES,
   VOICE_GOING_TO_CAMP,
   VOICE_HEAR_SOMETHING,
   VOICE_TEAM_KILLING,
   VOICE_REPORTING_IN,
   VOICE_GUARDING_C4,
   VOICE_CAMPING,
   VOICE_PLANTING_C4,
   VOICE_DEFUSING_C4,
   VOICE_IN_COMBAT,
   VOICE_SEEKING_ENEMY,
   VOICE_NOTHING,
   VOICE_ENEMYDOWN,
   VOICE_USINGHOSTAGE,
   VOICE_FOUND_C4,
   VOICE_WON_ROUND,
   VOICE_SCAREDEMOTE,
   VOICE_HEARDENEMY,
   VOICE_SNIPER_WARNING,
   VOICE_SNIPER_KILLED,
   VOICE_VIP_SPOTTED,
   VOICE_GUARDING_VIPSAFETY,
   VOICE_GOING_TO_GUARD_VIPSAFETY,
   VOICE_WON_ROUND_QUICK,
   VOICE_ONE_ENEMY_LEFT,
   VOICE_TWO_ENEMIES_LEFT,
   VOICE_THREE_ENEMIES_LEFT,
   VOICE_NO_ONE_LEFT,
   VOICE_FOUND_BOMB_PLACE,
   VOICE_WHERES_THE_BOMB,
   VOICE_DEFENDING_BOMBSITE,
   VOICE_BARELY_DEFUSED,
   VOICE_NICESHOT_COMMANDER,
   VOICE_NICESHOT_PALL,
   VOICE_GOING_TO_GUARD_HOSTAGES,
   VOICE_GOING_TO_GUARD_LOOSED_C4,
   VOICE_TOTAL
};

// counter-strike weapon id's
enum eWeapon
{
   WEAPON_P228         = 1,
   WEAPON_SHIELDGUN    = 2,
   WEAPON_SCOUT        = 3,
   WEAPON_HEGRENADE    = 4,
   WEAPON_XM1014       = 5,
   WEAPON_C4           = 6,
   WEAPON_MAC10        = 7,
   WEAPON_AUG          = 8,
   WEAPON_SMOKEGRENADE = 9,
   WEAPON_ELITE        = 10,
   WEAPON_FIVESEVEN    = 11,
   WEAPON_UMP45        = 12,
   WEAPON_SG550        = 13,
   WEAPON_GALIL        = 14,
   WEAPON_FAMAS        = 15,
   WEAPON_USP          = 16,
   WEAPON_GLOCK18      = 17,
   WEAPON_AWP          = 18,
   WEAPON_MP5NAVY      = 19,
   WEAPON_M249         = 20,
   WEAPON_M3           = 21,
   WEAPON_M4A1         = 22,
   WEAPON_TMP          = 23,
   WEAPON_G3SG1        = 24,
   WEAPON_FLASHBANG    = 25,
   WEAPON_DEAGLE       = 26,
   WEAPON_SG552        = 27,
   WEAPON_AK47         = 28,
   WEAPON_KNIFE        = 29,
   WEAPON_P90          = 30,
   WEAPON_ARMOR        = 31,
   WEAPON_ARMORHELM    = 32,
   WEAPON_DEFUSER      = 33
};

// defines for pickup items
enum ePickup
{
   PICKUP_NONE,
   PICKUP_WEAPON,
   PICKUP_DROPPED_C4,
   PICKUP_PLANTED_C4,
   PICKUP_HOSTAGE,
   PICKUP_BUTTON,
   PICKUP_SHIELD,
   PICKUP_DEFUSEKIT,
};

// reload state
enum eReloadState
{
   RELOAD_NONE       = 0,
   RELOAD_PRIMARY    = 1,
   RELOAD_SECONDARY  = 2,
};

// collision probes
enum eProbe
{
   PROBE_JUMP   = (1 << 0), // Probe Jump when colliding   
   PROBE_DUCK   = (1 << 1), // Probe Duck when colliding
   PROBE_STRAFE = (1 << 2),  // Probe Strafing when colliding
};

// vgui menus (since latest steam updates is obsolete, but left for old cs)
enum eMenu
{
   MENU_TEAM      = 2,  // menu select team
   MENU_TERRORIST = 26, // terrorist select menu
   MENU_CT        = 27, // ct select menu
};

// game start messages for counter-strike...
enum eMessage
{
   MESSAGE_IDLE         = 1,
   MESSAGE_TEAM_SELECT  = 2,
   MESSAGE_CLASS_SELECT = 3,
   MESSAGE_BUY          = 100,
   MESSAGE_RADIO        = 200,
   MESSAGE_SAY          = 10000,
   MESSAGE_TEAMSAY      = 10001
};

// netmessage functions
enum eNetMsg
{
   NETMSG_VGUI       = 1,
   NETMSG_SHOWMENU   = 2,
   NETMSG_WEAPONLIST = 3,
   NETMSG_METAHOOK  = 4,
   NETMSG_AMMOX      = 5,
   NETMSG_AMMOPICKUP = 6,
   NETMSG_DAMAGE     = 7,
   NETMSG_MONEY      = 8,
   NETMSG_STATUSICON = 9,
   NETMSG_DEATH      = 10,
   NETMSG_SCREENFADE = 11,
   NETMSG_HLTV       = 12,
   NETMSG_TEXTMSGALL = 13,
   NETMSG_SCOREINFO  = 14,
   NETMSG_BARTIME    = 15,
   NETMSG_NULL       = 0
};

// sensing states
enum eSensState
{
   STATE_SEEINGENEMY    = (1 << 0),   // seeing an enemy  
   STATE_HEARINGENEMY   = (1 << 1),   // hearing an enemy
   STATE_PICKUPITEM     = (1 << 2),   // pickup item nearby
   STATE_THROWHEGREN    = (1 << 3),   // could throw he grenade
   STATE_THROWFLASHBANG = (1 << 4),   // could throw flashbang
   STATE_THROWSMOKEGREN = (1 << 5),   // could throw smokegrenade
   STATE_SUSPECTENEMY   = (1 << 6)    // suspect enemy behind obstacle 
};

// positions to aim at
enum eAimAt
{
   AIM_DEST        = (1 << 0),   // aim at nav point
   AIM_CAMP        = (1 << 1),   // aim at camp vector
   AIM_PREDICTPATH = (1 << 2),   // aim at predicted path
   AIM_LASTENEMY   = (1 << 3),   // aim at last enemy
   AIM_ENTITY      = (1 << 4),   // aim at entity like buttons, hostages
   AIM_ENEMY       = (1 << 5),   // aim at enemy
   AIM_GRENADE     = (1 << 6),   // aim for grenade throw
   AIM_OVERRIDE    = (1 << 7)    // overrides all others (blinded)
};

// ladder direction
enum eLadderDir 
{
   LADDER_UP   = 1,
   LADDER_DOWN = 2
};

// famas/glock burst mode status + m4a1/usp silencer
enum eBurstMode
{
   BMSL_ENABLED  = 1,
   BMSL_DISABLED = 2
};

// visibility flags
enum eVisibility
{
   VISIBLE_HEAD  = (1 << 1),
   VISIBLE_BODY  = (1 << 2),
   VISIBLE_OTHER = (1 << 3)
};

// defines map type
enum eMapType
{
   MAP_AS = (1 << 0),
   MAP_CS = (1 << 1),
   MAP_DE = (1 << 2),
   MAP_ES = (1 << 3),
   MAP_KA = (1 << 4),
   MAP_FY = (1 << 5),
};

// defines for waypoint flags field (32 bits are available)
enum eWaypointFlag
{
   W_FL_LIFT      =   (1 << 1),  // wait for lift to be down before approaching this waypoint
   W_FL_CROUCH    =   (1 << 2),  // must crouch to reach this waypoint
   W_FL_CROSSING  =   (1 << 3),  // a target waypoint
   W_FL_GOAL      =   (1 << 4),  // mission goal point (bomb,hostage etc.)
   W_FL_LADDER    =   (1 << 5),  // waypoint is on ladder
   W_FL_RESCUE    =   (1 << 6),  // waypoint is a Hostage Rescue Point
   W_FL_CAMP      =   (1 << 7),  // waypoint is a Camping Point
   W_FL_NOHOSTAGE =   (1 << 8),  // only use this waypoint if no hostage
   W_FL_JUMPHELP  =   (1 << 9),  // bot help's another bot (requster) to get somewhere (using djump)


   W_FL_TERRORIST =   (1 << 29), // it's a specific terrorist point
   W_FL_COUNTER   =   (1 << 30), // It's a specific CT Point
};

// defines for waypoint connection flags field (16 bits are available)
enum ePathFlag
{
   C_FL_JUMP      =   (1 << 0),  // Must Jump for this Connection
};

// defines waypoint connection types
enum eConnection
{
   PATH_OUTGOING = 0,
   PATH_INCOMING,
   PATH_BOTHWAYS
}; 

// bot known file headers
const char EXP_HEADER[] = "PODEXP!";
const char WAY_HEADER[] = "PODWAY!";
const char VIS_HEADER[] = "PODVIS!";

// some hardcoded desire defines used to override calculated ones
const float TASKPRI_NORMAL = 35.0;
const float TASKPRI_PAUSE = 36.0;
const float TASKPRI_CAMP  = 37.0;
const float TASKPRI_SPRAYLOGO = 38.0;
const float TASKPRI_FOLLOWUSER = 39.0;
const float TASKPRI_MOVETOPOSITION = 50.0;
const float TASKPRI_DEFUSEBOMB = 89.0;
const float TASKPRI_PLANTBOMB = 89.0;
const float TASKPRI_ATTACK = 90.0;
const float TASKPRI_SEEKCOVER = 91.0;
const float TASKPRI_HIDE = 92.0;
const float TASKPRI_THROWGRENADE = 99.0;
const float TASKPRI_DOUBLEJUMP = 99.0;
const float TASKPRI_BLINDED = 100.0;
const float TASKPRI_SHOOTBREAKABLE = 100.0;
const float TASKPRI_ESCAPEFROMBOMB = 100.0;

const float GRENADE_TIMER = 1.2;
const float MAX_BOMBHEARDISTANCE = 2048.0;

const int MAX_HOSTAGES = 8;
const int MAX_PATH_INDEX = 8;
const int MAX_DAMAGE_VAL = 2040;
const int MAX_GOAL_VAL = 2040;
const int MAX_KILL_HIST = 16;
const int MAX_REG_MSGS = 256;
const int MAX_WAYPOINTS = 1024;
const int MAX_NUMBOMB_SPOTS = 16;
const int MAX_WEAPONS = 32;
const int NUM_WEAPONS = 26;

const int WAYPOINT_VERSION = 7;
const int EXPERIENCE_VERSION = 2;
const int VISTABLE_VERSION = 1;

// weapon masks fixed
const int WEAPON_PRIMARY = ((1 <<WEAPON_XM1014) | (1 <<WEAPON_M3) | (1 << WEAPON_MAC10) | (1 << WEAPON_UMP45) | (1 << WEAPON_MP5NAVY) | (1 << WEAPON_TMP) | (1 << WEAPON_P90) | (1 << WEAPON_AUG) | (1 << WEAPON_M4A1) | (1 << WEAPON_SG552) | (1 << WEAPON_AK47) | (1 << WEAPON_SCOUT) | (1 << WEAPON_SG550) | (1 << WEAPON_AWP) | (1 << WEAPON_G3SG1) | (1 << WEAPON_M249) | (1 << WEAPON_FAMAS) | (1 << WEAPON_GALIL));
const int WEAPON_SECONDARY = ((1 << WEAPON_P228) | (1 << WEAPON_ELITE) | (1 << WEAPON_USP) | (1 << WEAPON_GLOCK18) | (1 << WEAPON_DEAGLE) | (1 << WEAPON_FIVESEVEN));

// this structure links waypoints returned from pathfinder
struct tPathNode
{
   int iIndex;
   tPathNode *pNext;
};

// we are support atleast 3 counter-strike versions
struct tModInfo
{
   char szName[32];
   char szLinuxSO[32];
   char szWinDLL[32];
   char szDesc[256];
   int  iModId;
};

// links keywords and replies together
struct tKeywordChat
{
   vector <string> arKeywords;
   vector <string> arReplies;
   vector <string> kUsedReplies;
};

// Tasks definition
struct tTask
{
   tTask *pPreviousTask;
   tTask *pNextTask;
   eTask iTask;        // major task/action carried out
   float fDesire;      // desire (filled in) for this task
   int   iData;        // additional data (waypoint index)
   float fTime;        // time task expires
   bool  bCanContinue; // if task can be continued if interrupted
};

// user message record structure definition
struct tUserMsg
{
   const char *cszName; // name of user message as called by the MOD DLL
   int iId;             // identification number the engine recorded this user message under
};

// wave structure
struct tWaveHeader
{
   char szRiffChunkId[4];
   unsigned long ulPackageSize;
   char szWaveChunkId[4];
   char szFmtChunkId[4];
   unsigned long ulFmtChunkLength;
   unsigned short usDummy;
   unsigned short usChannels;
   unsigned long ulSampleRate;
   unsigned long ulBytesPerSecond;
   unsigned short usBytesPerSample;
   unsigned short usBitsPerSample;
   char szDataChunkId[4];
   unsigned long ulDataChunkLength;
};

// botname structure definition
struct tBotName
{
   string sName;
   bool bUsed;
};

// voice config structure definition
struct tChatter
{
   string sName;
   float fRepeatTime; 
};

// voice place structure definiton
struct tChatterPlace
{
   string sName;
   uint32 uHash;
   vector <string> kFiles;
};

// language config structure definition
struct tLanguage
{
   char *szOriginal;    // original string
   char *szTranslated;  // string to replace for
};

struct tWeaponSelect
{
   int iId;               // the weapon id value
   char *szWeaponName;    // name of the weapon when selecting it
   char *szModelName;     // model name to separate cs weapons
   bool bPrimaryFireHold; // hold down primary fire button to use?
   int iPrice;            // price when buying
   int iMinPrimaryAmmo;
   int iTeamStandard;     // used by team (number) (standard map)
   int iTeamAS;           // used by team (as map)
   int iBuyGroup;         // group in buy menu (standard map)
   int iBuySelect;        // select item in buy menu (standard map)
   int iNewBuySelectT;    // for counter-strike v1.6
   int iNewBuySelectCT;   // for counter-strike v1.6
   bool bShootsThru;      // can shoot thru walls 
};


// skill definitions
struct tSkillData
{
   float fMinSurpriseTime;  // surprise delay (minimum)
   float fMaxSurpriseTime;  // surprise delay (maximum)
   float fCampStartDelay;   // delay befor start camping
   float fCampEndDelay;     // delay before end camping
   float fMinTurnSpeed;     // initial minimum turnspeed
   float fMaxTurnSpeed;     // initial maximum turnspeed
   float fAimOffs_X;        // X/Y/Z maximum offsets
   float fAimOffs_Y;        // X/Y/Z maximum offsets
   float fAimOffs_Z;        // X/Y/Z maximum offsets
   int iHeadshotFrequency;  // precent to aiming to player head
   int iHeardShootThruProb; // precent to shooting throug wall when seen something
   int iSeenShootThruProb;  // precent to shooting throug wall when heard something
};

// struct for menus
struct tMenuText
{
   int  iValidSlots; // ored together bits for valid keys
   char *szMenuText; // ptr to actual string
}; 

// array of clients struct
struct tClient
{
   tMenuText *pMenu; // pointer to opened bot menu
   edict_t *pentEdict; // pointer to actual edict
   Vector vOrigin; // position in the world
   Vector vSoundPosition; // position sound was played
   int iTeam; // what team
   int iFlags; // client flags
   float fHearingDistance; // distance this sound is heared 
   float fTimeSoundLasting; // time sound is played/heared
}; 

// experience data hold in memory while playing
struct tExperience
{
   unsigned short uTeam0Damage;
   unsigned short uTeam1Damage;
   signed short iTeam0DangerIndex;
   signed short iTeam1DangerIndex;
   signed short wTeam0Value;
   signed short wTeam1Value;
};

// experience data when saving/loading
struct tExperienceSave
{
   unsigned char uTeam0Damage;
   unsigned char uTeam1Damage;
   signed char cTeam0Value;
   signed char cTeam1Value;
};

// bot creation tab
struct tCreateQueue
{
   int iSkill;
   int iTeam;
   int iClass;
   int iPersonality;
   string sName;
};

// weapon propities structure
struct tWeapon
{
   char szClassname[64];
   int iAmmo1;     // ammo index for primary ammo
   int iAmmo1Max;  // max primary ammo
   int iSlot;      // HUD slot (0 based)
   int iPosition;  // slot position
   int iId;        // weapon ID
   int iFlags;     // flags???
};

// define chatting collection structure
struct tSayText
{
   char cChatProbability;
   float fChatDelay;
   float fTimeNextChat;
   int iEntityIndex;
   char szSayText[512];
   vector <string> arLastUsedLines;
};

// general waypoint header information structure
struct tWaypointHeader
{
   char  szHeader[8];
   int32 iFileVersion;
   int32 iWpNumber;
   char  szMapname[32];
   char  szAuthor[32];
};

// general experience & vistable header information structure
typedef struct expvis_header_s
{
   char  szHeader[8];
   int32 iFileVersion;
   int32 iWpNumber;
} tExperienceHeader, tVistableHeader;

// define general waypoint structure
struct tPath
{
   int32    iPathNumber;
   int32    iFlags;
   Vector   vOrigin;
   float    fRadius;

   float    fCampStartX;
   float    fCampStartY;
   float    fCampEndX;
   float    fCampEndY;

   int16    iIndex[MAX_PATH_INDEX];
   uint16   uConnectFlag[MAX_PATH_INDEX];
   Vector   vConnectVel[MAX_PATH_INDEX];
   int32    iDistance[MAX_PATH_INDEX];
   
   struct tVis { uint16 uStand, uCrouch; } kVis;
};

// main bot class
class CBot
{
private:
   unsigned int   m_iStates;                 // sensing bitstates
   tTask         *m_pTasks;                  // pointer to active tasks/schedules

   float          m_fOldCombatDesire;        // holds old desire for filtering

   bool           m_bIsLeader;               // bot is leader of his team

   float          m_fMoveSpeed;              // current Speed forward/backward
   float          m_fSideMoveSpeed;          // current Speed sideways
   float          m_fMinSpeed;               // minimum Speed in normal mode
   bool           m_bCheckTerrain;

   float          m_fPrevTime;               // time previously checked movement speed
   float          m_fPrevSpeed;              // speed some frames before 
   Vector         m_vPrevOrigin;             // origin some frames before

   int            m_aMessageQueue[32];       // stack for messages
   char           m_szMiscStrings[160];      // space for strings (saytext...)
   int            m_iRadioSelect;            // radio entry

   float          m_fItemCheckTime;          // time next search for items needs to be done
   edict_t       *m_pentPickupItem;          // pointer to entity of item to use/pickup
   edict_t       *m_pentItemIgnore;          // pointer to entity to ignore for pickup
   ePickup        m_iPickupType;             // type of entity which needs to be used/picked up
   eBurstMode     m_iWeaponBurstMode;        // bot using burst mode? (famas/glock18, but also silcecer mode)

   edict_t       *m_pentShootBreakable;      // pointer to breakable entity
   Vector         m_vBreakable;              // origin of breakable

   float          m_fTimeDoorOpen;           // time to next door open check
   float          m_fLastChatTime;           // time bot last chatted
   float          m_fTimeLogoSpray;          // time bot last spray logo
   float          m_fKnifeAttackTime;        // time to rush with knife (at the beggining of the round)
   bool           m_bDefendedBomb;           // defend action issued

   float          m_fCollideTime;            // time last collision
   float          m_fFirstCollideTime;       // time of first collision
   float          m_fProbeTime;              // time of probing different moves
   float          m_fNoCollTime;             // time until next collision check 
   eCollision     m_cCollisionState;         // collision State
   char           m_cCollisionProbeBits;     // bits of possible collision Moves
   char           m_rgcCollideMoves[4];      // sorted Array of Movements
   char           m_cCollStateIndex;         // index into m_rgcCollideMoves

   tPathNode     *m_pWaypointNodes;          // pointer to current node from path
   tPathNode     *m_pWayNodesStart;          // pointer to start of pathfinding Nodes
   unsigned char  m_byPathType;              // which pathfinder to use
   unsigned char  m_ucVisibility;            // visibility flags

   int            m_iCurrWptIndex;           // current wpt index
   int            m_iTravelStartIndex;       // for double jump
   int            m_rgiPrevWptIndex[5];      // previous wpt indices from waypointfind 
   int            m_iWPTFlags;               // current waypoint flags
   
   unsigned short m_uiCurrTravelFlags;       // connection flags like jumping
   bool           m_bJumpDone;               // has bot finished jumping
   
   Vector         m_vDesiredVelocity;        // desired velocity for jump waypoints
   float          m_fWptTimeset;             // time waypoint chosen by Bot

   unsigned int   m_iAimFlags;               // aiming conditions
   Vector         m_vLookAt;                 // vector bot should look at
   Vector         m_vThrow;                  // origin of waypoint to throw grens

   Vector         m_vEnemy;                  // target origin chosen for shooting
   Vector         m_vGrenade;                // calculated vector for grenades
   Vector         m_vEntity;                 // origin of entities like buttons etc.
   Vector         m_vCamp;                   // aiming vector when camping.

   float          m_fTimeWaypointMove;       // last time bot followed waypoints

   bool           m_bWantsToFire;            // bot needs consider firing
   float          m_fShootAtDeadTime;        // time to shoot at dying players
   edict_t       *m_pentAvoidGrenade;        // pointer to grenade entity to avoid
   char           m_cAvoidGrenade;           // which direction to strafe away

   float          m_fFollowWaitTime;
   edict_t       *m_pentTargetEnt;           // the entity that the bot is trying to reach
   edict_t       *m_rgpHostages[MAX_HOSTAGES]; // pointer to used hostage entities

   bool           m_bIsReloading;            // bot is reloading a gun
   int            m_iReloadState;            // current reload state
   int            m_iVoicePitch;             // bot voice pitch 

   int            m_iMsecVal;                // calculated msec value
   float          m_fMsecDel;                // used for msec calulation
   float          m_fMsecNum;                // also used for msec caculation
   float          m_fMsecInterval;           // used for Leon Hartwig's method for msec calculation

   float          m_fReloadCheckTime;        // time to check reloading
   float          m_fZoomCheckTime;          // time to check zoom again
   float          m_fShieldCheckTime;        // time to check shiled drawing again
   float          m_fGrenadeCheckTime;       // time to check grenade usage

   bool           m_bCheckKnifeSwitch;       // is time to check switch to knife action
   bool           m_bCheckWeaponSwitch;      // is time to check weapon switch
   bool           m_bUsingGrenade;           // bot currently using grenade??
   bool           m_bTeamEconomicsGood;      // is bot able to buy primary & secondary weapons

   unsigned char  m_ucCombatStrafeDir;       // direction to strafe
   unsigned char  m_ucFightStyle;            // combat style to use
   float          m_fLastFightStyleCheck;    // time checked style
   float          m_fStrafeSetTime;          // time strafe direction was set

   float          m_fTimeCamping;            // time to camp
   int            m_iCampDirection;          // camp Facing direction
   float          m_fNextCampDirTime;        // time next camp direction change
   int            m_iCampButtons;            // buttons to press while camping

   float          m_fDuckTime;               // time to duck
   float          m_fJumpTime;               // time last jump happened
   float          m_fVoiceTime[VOICE_TOTAL]; // voice command timers

   float          m_fSoundUpdateTime;        // time to update the sound
   float          m_fHeardSoundTime;         // last time noise is heard

   Vector         m_vMoveAngles;             // bot move angles
   bool           m_bMoveToGoal;             // bot currently moving to goal??

   Vector         m_vIdealAngles;            // angle wanted
   Vector         m_vRandomizedIdealAngles;  // angle wanted with noise
   Vector         m_vAngularDeviation;       // angular deviation from current to ideal angles
   Vector         m_vAimSpeed;               // aim speed calculated
   Vector         m_vTargetAngularSpeed;     // target/enemy angular speed

   float          m_fRandomizeAnglesTime;    // time last randomized location
   float          m_fPlayerTargetTime;       // time last targeting

   void           SwitchChatterIcon (bool bShow);
   void           InstantChatterMessage (int iType);
   void           HandleLadder (void);
   void           BotAI (void);
   bool           IsMorePowerfulWeaponCanBeBought (void);
   void           DoWeaponBuying (void);
   bool           CanDuckUnder (Vector vNormal);
   bool           CanJumpUp (Vector vNormal);
   bool           CanStrafeLeft (TraceResult *tr);
   bool           CanStrafeRight (TraceResult *tr);
   bool           CantMoveForward (Vector vNormal, TraceResult *tr);
   void           ChangePitch (float speed);
   void           ChangeYaw (float speed);
   void           CheckMessageQueue (void);
   void           CheckRadioCommands (void);
   void           CheckReload (void);
   void           AvoidGrenades (void);
   void           ChangeBurstMode (void);
   void           CheckSilencer (void);
   bool           CheckWallOnLeft (void);
   bool           CheckWallOnRight (void);
   void           ChooseAimDirection (void);
   int            ChooseBombWaypoint (void);
   bool           DoWaypointNav (void);
   bool           EnemyIsThreat (void);
   void           FacePosition (void);
   bool           IsRestricted (int iWeaponId);
   bool           IsInViewCone (Vector vOrigin);
   void           ReactOnSound (void);
   inline bool    CheckVisibility (entvars_t *pevTarget, Vector *pvOrigin, byte *ucBodyId);
   bool           IsEnemyViewable (edict_t *pentPlayer);

   edict_t       *FindBreakable (void);
   int            FindCoverWaypoint (float fMaxDistance);
   int            FindDefendWaypoint (Vector vPosition);
   int            FindGoal (void);
   void           FindItem (void);
   void           GetCampDirection (Vector *vDest);
   void           CollectGoalExperience (int iDamage, int iTeam);
   void           CollectExperienceData (edict_t *pentAttacker, int iDamage);
   int            GetMessageQueue (void);
   bool           GoalIsValid (void);
   bool           HeadTowardWaypoint (void);
   int            InFieldOfView (Vector vDest);
   bool           IsBlockedLeft (void);
   bool           IsBlockedRight (void);
   bool           IsWaypointUsed (int iIndex);
   inline bool    IsOnLadder (void) { return pev->movetype == MOVETYPE_FLY; }
   inline bool    IsOnFloor (void) { return (pev->flags & (FL_ONGROUND | FL_PARTIALGROUND)) != 0; }
   inline bool    IsInWater (void) { return pev->waterlevel >= 2; }
   inline float   GetWalkSpeed (void) { return static_cast <float> ((static_cast <int> (pev->maxspeed) / 2) + (static_cast <int> (pev->maxspeed) / 50)) - 18; }
   bool           ItemIsVisible (Vector vDest, char *pszItemName);
   bool           LastEnemyShootable (void);
   bool           LastEnemyVisible (void);
   void           RunTask (void);
   void           StartTask (tTask *pTask);
   bool           IsShootableBreakable (edict_t *pent);
   bool           RateGroundWeapon (edict_t *pent);
   bool           ReactOnEnemy (void);
   void           ResetCollideState (void);
   void           SetConditions (void);
   void           SetStrafeSpeed (Vector vMoveDir, float fStrafeSpeed);
   void           StartGame (void);
   void           TaskComplete (void);
   bool           CanHearC4 (Vector *pvHolder);
   bool           GetBestNextWaypoint (void);
   int            GetBestWeaponCarried (void);
   int            GetBestSecondaryWeaponCarried (void);

   void           EstimateNextFrameDuration (void);
   void           GetValidWaypoint (void);
   void           ChangeWptIndex (int iWptIndex);
   bool           IsDeadlyDrop (Vector vTargetPos);
   bool           OutOfBombTimer (void);
   void           SelectLeaderEachTeam (int iTeam);

   Vector         CheckToss (const Vector &vStart, Vector vEnd);
   Vector         CheckThrow (const Vector &vStart, Vector vEnd, float fSpeed);
   Vector         AimPosition (void);

   int            CheckGrenades (void);
   void           CommandTeam (void);
   void           CombatFight (void);
   bool           CheckCorridorAround (void);
   bool           IsWeaponBadInDistance (int iWeaponIndex, float fDistance);
   bool           DoFirePause (float fDistance);
   bool           LookupEnemy (void);
   bool           FireWeapon (Vector vEnemy);
   void           FocusEnemy (void);
   void           SelectBestWeapon (void);
   void           SelectPistol (void);
   bool           IsFriendInLineOfFire (float fDistance);
   bool           IsGroupOfEnemies (Vector vLocation, int iGroupIs = 1, int iRadius = 256);
   bool           IsShootableThruObstacle (Vector vDest);
   int            GetNearbyEnemiesNearPosition (Vector vPosition, int iRadius);
   int            GetNearbyFriendsNearPosition (Vector vPosition, int iRadius);
   void           SelectWeaponByName (const char *cszName);
   void           SelectWeaponbyNumber (int iNum);
   int            GetHighestWeapon (void);

   bool           IsEnemyProtectedByShield (void);
   bool           ParseChat (char *pszReply);
   bool           RepliesToPlayer (void);
   float          GetBombTimeleft (void);

   int            GetAimingWaypoint (void);
   int            GetAimingWaypoint (Vector vTargetPos, int iCount);
   void           FindShortestPath (int iSourceIndex, int iDestIndex);
   void           FindPath (int iSourceIndex, int iDestIndex, unsigned char byPathType = 0);
   void           DebugMsg (const char *fmt, ...);
   void           SecondThink (void);

public:
   entvars_t     *pev;

   int            m_iWantedTeam;
   int            m_iWantedClass;

   int            m_iSkill;
   int            m_iAccount;
   ePersonality   m_ucPersonality;
   int            m_iSprayLogo;

   float          m_fSpawnTime;              // time this bot spawned
   float          m_fTimeTeamOrder;          // time of last radio command
      
   bool           m_bIsVIP;                  // bot is vip?
   bool           m_bIsDefendingTeam;        // bot in defending team on this map

   int            m_iStartAction;            // team/class selection state
   bool           m_bNotKilled;              // has the player been killed or has he just respawned
   bool           m_bNotStarted;             // team/class not chosen yet

   int            m_iVoteKickIndex;          // index of player to vote against
   int            m_iLastVoteKick;           // last index
   int            m_iVoteMap;                // number of map to vote for
   int            m_iLadderDir;              // direction for ladder movement

   bool           m_bInBombZone;             // bot in the bomb zone or not
   int            m_iBuyCount;               // current Count in Buying
   float          m_fNextBuyTime;

   bool           m_bInBuyZone;              // bot currently in buy zone
   bool           m_bInVIPZone;              // bot in the vip satefy zone
   bool           m_bBuyingFinished;         // done with buying
   bool           m_bBuyPending;             // bot buy is pending
   bool           m_bHasDefuser;             // does bot has defuser
   bool           m_bHasProgressBar;         // has progress bar on a HUD
   bool           m_bJumpReady;              // is doublejump ready

   float          m_fBlindTime;              // time bot is blind
   float          m_fBlindMoveSpeed;         // mad speeds when bot is blind
   float          m_fBlindSidemoveSpeed;     // mad side move speeds when bot is blind

   edict_t       *m_pentDoubleJumpEdict;    // pointer to entity that request doublejump
   edict_t       *m_pentRadioEntity;         // pointer to entity issuing a radio command
   int            m_iRadioOrder;             // actual command

   float          m_fDuckForJump;            // is bot needed to duck for doublejump
   float          m_fBaseAgressionLevel;     // base agression level (on initializing)
   float          m_fBaseFearLevel;          // base fear level (on initializing)
   float          m_fAgressionLevel;         // dynamic agression level (in game)
   float          m_fFearLevel;              // dynamic fear level (in game)
   float          m_fNextEmotionUpdate;      // next time to sanitize emotions

   int            m_iActMessageIndex;        // current processed message
   int            m_iPushMessageIndex;       // offset for next pushed message

   int            m_iPrevGoalIndex;          // holds destination goal waypoint
   int            m_iChosenGoalIndex;        // used for experience, same as above
   float          m_fGoalValue;              // ranking value for this waypoint

   Vector         m_vWptOrigin;              // origin of waypoint
   Vector         m_vDestOrigin;             // origin of move destination
   Vector         m_vPosition;               // position to move to in movetoposition task
   Vector         m_vDoubleJumpOrigin;       // origin of doublejump

   float          m_fViewDistance;           // current view distance
   float          m_fMaxViewDistance;        // maximum view distance
   tSayText       m_sSaytextBuffer;          // holds the index & the actual message of the last unprocessed text message of a player

   edict_t       *m_pentEnemy;               // pointer to enemy entity
   float          m_fEnemyUpdateTime;        // time to check for new enemies
   float          m_fEnemyReachableTimer;    // time to recheck if Enemy reachable
   bool           m_bEnemyReachable;         // direct line to enemy

   float          m_fSeeEnemyTime;           // time bot sees enemy
   float          m_fEnemySurpriseTime;      // time of surprise
   float          m_fIdealReactionTime;      // time of base reaction
   float          m_fActualReactionTime;     // time of current reaction time

   edict_t       *m_pentLastEnemy;           // pointer to last enemy entity
   Vector         m_vLastEnemyOrigin;        // vector to last enemy origin
   edict_t       *m_pentLastVictim;          // pointer to killed entity
   edict_t       *m_pentTrackingEdict;       // pointer to last tracked player when camping/hiding
   float          m_fTimeNextTracking;       // time waypoint index for tracking player is recalculated

   float          m_fTimeFirePause;          // time to pause firing
   float          m_fShootTime;              // time to shoot
   float          m_fTimeLastFired;          // time to last firing
   int            m_iLastDamageType;         // stores last damage
   int            m_iCountOpenDoor;          // try's to open the door

   int            m_iCurrentWeapon;          // one current weapon for each bot
   int            m_rgAmmoInClip[MAX_WEAPONS]; // ammo in clip for each weapons
   int            m_rgAmmo[MAX_AMMO_SLOTS];  // total ammo amounts

   CBot           (edict_t *pentBot, int iSkill, int iPersonality, int iTeam, int iClass);
   ~CBot          (void);

   int            GetAmmo (void);
   inline int     GetAmmoInClip (void) { return m_rgAmmoInClip[m_iCurrentWeapon]; }

   inline edict_t *GetEntity (void) { return ENT (pev); };
   inline EOFFSET GetOffset (void) { return OFFSET (pev); };
   inline int     GetIndex (void) { return ENTINDEX (GetEntity ()); };

   inline Vector  Center (void) { return (pev->absmax + pev->absmin) * 0.5; };
   inline Vector  EyePosition (void) { return pev->origin + pev->view_ofs; };
   inline Vector  EarPosition (void) { return pev->origin + pev->view_ofs; };
   inline Vector  GetGunPosition (void) { return (pev->origin + pev->view_ofs); }

   void           Think (void);
   void           NewRound (void);
   void           PushMessageQueue (int iMessage);
   void           PrepareChatMessage (char *szText);
   bool           FindWaypoint (void);
   bool           EntityIsVisible (Vector vDest, bool bFromBody = false);
 
   void           DeleteSearchNodes (void);
   tTask         *CurrentTask (void);

   void           RemoveCertainTask (eTask iTaskNum);
   void           ResetTasks (void);
   void           TakeDamage (edict_t *pentInflictor, Vector vOrigin, int iDamage, int iArmor, int iBits);
   void           StartTask (eTask iTask, float fDesire, int iData, float fTime, bool bCanContinue);
   void           DiscardWeaponForUser (edict_t *pentUser, bool bDiscardC4);

   void           SayText (const char *pText);
   void           TeamSayText (const char *pText);

   void           ChatMessage (int iType, bool bTeamSay = false);
   void           RadioMessage (int iMessage);
   void           ChatterMessage (int iMessage);
   void           HandleChatterMessage (const char *csz);
   void           Kill (void);
   void           Kick (void);
   void           ResetDoubleJumpState (void);
   void           MoveToVector (Vector vTo);
   int            FindLoosedBomb (void);

   bool           HasHostage (void);
   bool           UsesRifle (void);
   bool           UsesPistol (void);
   bool           UsesSniper (void);
   bool           UsesSubmachineGun (void);
   bool           UsesZoomableRifle (void);
   bool           UsesBadPrimary (void);
   bool           HasPrimaryWeapon (void);
   bool           HasShield (void);
   bool           IsShieldDrawn (void);
};

// manager class
class CBotManager
{
private:
   CBot         *m_rgpBots[32]; // 32 bots
   vector <tCreateQueue> m_arCreationTab;
   float         m_fMaintainTime;

protected:
   int           CreateBot (string sName, int iSkill, int iPersonality, int iTeam, int iClass);

public:
   CBotManager   (void);
   ~CBotManager  (void);

   int           GetIndex (edict_t *pent);
   CBot         *GetBot (int iIndex);
   CBot         *GetBot (edict_t *pent);
   CBot         *FindOneValidAliveBot (void);
   CBot         *GetHighestFragsBot (int iTeam);

   int           NumHumans (void);
   int           NumBots (void);

   void          Think (void);
   void          Free (void);
   void          Free (int iIndex);
   
   void          AddBot (string sName, int iSkill, int iPersonality, int iTeam, int iClass);
   void          AddBot (string sName, string sSkill, string sPersonality, string sTeam, string sClass);
   inline void   AddRandom (void) { AddBot ("", -1, -1, -1, -1); }
   void          FillServer (int iSelection, int iPersonality = PERSONALITY_NORMAL);
   
   void          RemoveAll (void);
   void          RemoveRandom (void);
   void          RemoveFromTeam (eTeam iTeam, bool bAll = false);
   void          RemoveMenu (edict_t *pent, int iMenuNum);
   void          KillAll (int iTeam = -1);
   void          MaintainBotQuota (void);
   void          InitQuota (void);
   
   void          List (void);
   void          SetWeaponMode (int iSelection);
   bool          IsTeamEconomicsBad (int iTeam);

   static void   CallGameEntity (entvars_t *pVars);
};

// texts localizer (i really don't like globals so i put this little shit to class)
class CLocalizer
{
public:
   vector <tLanguage> m_arLanguage;

public:
   CLocalizer     (void) { m_arLanguage.Cleanup (); }
   ~CLocalizer    (void) { m_arLanguage.Cleanup (); }

   char           *T (const char *cszInput);
};

// netmessage handler class
class CNetMsg
{
private:
   eNetMsg        m_iMessage;
   int            m_iState; // state of messages
   CBot          *m_pBot;

public:
   CNetMsg        (void) { Reset (); }
   ~CNetMsg       (void) { };

   void           Execute (void *p); // if bot == null message to all
   inline void    Reset (void) { m_iMessage = NETMSG_NULL; m_iState = 0; m_pBot = NULL; };

   inline eNetMsg GetCurrent (void) { return m_iMessage; }
   inline int     GetState (void) { return m_iState; }

   inline void    SetMessage (eNetMsg iMessage) { m_iMessage = iMessage; }
   inline void    SetBot (CBot *pBot) { m_pBot = pBot; }
};

class CWaypoint
{
   friend class CBot;

private:
   tPath      *m_pPaths[MAX_WAYPOINTS];
   bool        m_bWaypointPaths;
   bool        m_bOnLadder;
   bool        m_bEndJumpPoint;
   bool        m_bLearnJumpWaypoint;
   float       m_fTimeJumpStarted;

   Vector      m_vLearnVelocity;
   Vector      m_vLearnPos;

   int         m_iCacheWaypointIndex;
   int         m_iLastJumpWaypoint;
   int         m_iCurrVisIndex;
   Vector      m_vLastWaypoint;

   float       m_fPathTime;
   float       m_fDangerTime;
   float       m_rgfWPDisplayTime[MAX_WAYPOINTS];
   byte        m_rgbyVisLUT[MAX_WAYPOINTS][MAX_WAYPOINTS / 4];
   char        m_szInfo[256];

   int        *m_pDistMatrix;
   int        *m_pPathMatrix;

   vector <int> m_arTerrorPoints;
   vector <int> m_arCTPoints;
   vector <int> m_arGoalPoints;
   vector <int> m_arCampPoints;
   vector <int> m_arRescuePoints;

public:
   bool        m_bRecalcVis;

   CWaypoint   (void);
   ~CWaypoint  (void);

   void        Init (void);
   void        InitExperienceTab (void);
   void        InitVisibilityTab (void);

   void        InitTypes (void);
   void        AddPath (short int iAddIndex,  short int sPathIndex, float fDistance);

   int         FindFarest (Vector vOrigin, float fMaxDistance = 32.0);
   int         FindNearest (Vector vOrigin, float fMinDistance = 9999.0, int iFlags = -1);
   void        FindInRadius (Vector vPos, float fRadius, int *pTab, int *iCount);
   void        FindInRadius (vector <int> &kId, float fRadius, Vector vPos);

   void        Add (int iFlags, Vector vWptOrigin = NULLVEC);
   void        Delete (void);
   void        ChangeFlags (int iFlag, char iMode);
   void        SetRadius (int iRadius);
   bool        IsConnected (int iA, int iB);
   bool        IsConnected (int iNum);
   void        InitializeVisibility (void);
   void        CreatePath (char cDirection);
   void        DeletePath (void);
   void        CacheWaypoint (void);

   float       GetTravelTime (float fMaxSpeed, Vector vSource, Vector vPosition);
   bool        IsVisible (int iSourceIndex, int iDestIndex);
   bool        IsStandVisible (int iSourceIndex, int iDestIndex);
   bool        IsDuckVisible (int iSourceIndex, int iDestIndex);
   void        CalculateWayzone (int iIndex);

   bool        Load (void);
   void        Save (void);
   void        SaveXML (void);
   bool        Reachable (Vector vSource, Vector vDestination, edict_t *pent);
   bool        IsNodeReachable (Vector vSource, Vector vDestination);
   void        Think (void);
   bool        NodesValid (void);
   void        SaveExperienceTab (void);
   void        SaveVisibilityTab (void);
   void        CreateBasic (void);
   void        EraseFromHardDisk (void);

   void        InitPathMatrix (void);
   void        SavePathMatrix (void);
   bool        LoadPathMatrix (void);

   int         GetPathDistance (int iSourceWaypoint, int iDestWaypoint);
   inline char*GetInfo (void) { return m_szInfo; }
   tPath      *GetPath (int iId);

   void        SetLearnJumpWaypoint (void);
   Vector      GetBombPosition (void);
};

// prototypes of bot functions...
extern int GetUserMsgId (const char *cszName);
extern int MaxClients (void);
extern int GetNearestPlayerIndex (Vector vOrigin);
extern int GetWeaponReturn (bool bString, const char *cszWeaponAlias, int iWeaponId = -1);
extern int GetTeam (edict_t *pentEdict);
extern int32 RandomLong (int32 iFrom, int32 iTo);

extern float GetShootingConeDeviation (edict_t *pentEdict, Vector *pvPosition);
extern float WorldTime (void);
//extern float CVarGetFloat (const char *cszVariable);
extern float GetWaveLength (const char *cszWaveFile);
extern float RandomFloat (float fFrom, float fTo);

extern bool IsLinux (void);
extern bool IsFileAbleToOpen (char *filename);
extern bool IsDedicatedServer (void);
extern bool IsBombPointWasVisited (int iIndex);
extern bool IsVisible (const Vector &vOrigin, edict_t *pentEdict);
extern bool IsAlive (edict_t *pent);
extern bool IsInViewCone (Vector vOrigin, edict_t *pentEdict);
extern bool IsWeaponShootingThroughWall (int iId);
extern bool IsValidBot (edict_t *pent);
extern bool IsValidPlayer (edict_t *pent);
extern bool OpenConfig (const char *cszFile, char *cszErrorIfNotExists, CFile *pOut, bool bLanguageDependant = false);
extern bool FindNearestPlayer (void **pvHolder, edict_t *pentTo, float fSearchDistance = 4096.0, bool bSameTeam = false, bool bNeedBot = false, bool bAlive = false, bool bNeedDrawn = false);

extern const char *GetUserMsgName (int iMsgType);
extern const char *GetMapName (void);
extern const char *GetWptDir (void);
extern const char *GetModName (void);
extern const char *GetField (const char *cszString, int iFieldId, bool bEndLine = false);
extern char *VarArgs (char *fmt, ...);
extern char *WPN_StrChr (const char *cszInput, int cCh, int iLine);

extern uint16 GenerateBuildNumber (void);
extern Vector GetEntityOrigin (edict_t *pentEdict);
extern tModInfo *FindGame (const char *szId);

extern void CTBombPointClear (int iIndex);
extern void FreeLibraryMemory (void);
extern void RoundInit (void);
extern void TerminateOnError (const char *fmt, ...);
extern void FakeClientCommand (edict_t *pentFakeClient, const char *fmt, ...);
extern void StrTrim (char *szString);
extern void CreatePath (char *path);
extern void ExtractFromSteamDLL (char *szGameDLLName);
extern void ServerCommand (const char *fmt, ...);
extern void CVarSetFloat (const char *cszVariable, float fNewVal);
extern void CVarSetString (const char *cszVariable, const char *cszNewVal);
extern void RegisterBotVariable (int iVariableName, const char *cszInitialValue, int iFlags = FCVAR_EXTDLL | FCVAR_SERVER);
extern void RegisterCommand (char *szCommand, void pfnFunction (void));
extern void SpeechSynth (char *szMessage);
extern void CheckWelcomeMessage (void);
extern void DetectCSVersion (void);
extern void PlaySound (edict_t *pentEdict, const char *sound_name);
extern void ServerPrint (const char *fmt, ...);
extern void ChartPrint (const char *fmt, ...);
extern void ServerPrintNoTag (const char *fmt, ...);
extern void CenterPrint (const char *fmt, ...);
extern void ClientPrint (edict_t *pent, int iDest, const char *fmt, ...);
extern void HudMessage (edict_t *pent, bool bCenter, Vector rgbColor, char *fmt, ...);
extern void LogToFile (bool bPrintToConsole, int iLevel, const char *fmt, ...);
extern void TraceLine (const Vector &vStart, const Vector &vEnd, bool bIgnoreMonsters, bool bIgnoreGlass, edict_t *pentIgnore, TraceResult *ptr);
extern void TraceLine (const Vector &vStart, const Vector &vEnd, bool bIgnoreMonsters, edict_t *pentIgnore, TraceResult *ptr);
extern void TraceHull (const Vector &vStart, const Vector &vEnd, bool bIgnoreMonsters, int hullNumber, edict_t *pentIgnore, TraceResult *ptr);
extern void DrawLine (edict_t *pentEdict, Vector start, Vector end, int width, int noise, int red, int green, int blue, int brightness, int speed, int life);
extern void DrawArrow (edict_t *pentEdict, Vector start, Vector end, int width, int noise, int red, int green, int blue, int brightness, int speed, int life);
extern void ShowMenu (edict_t *pentEdict, tMenuText *pMenu);
extern void DecalTrace (entvars_t *pev, TraceResult *pTrace, char *szDecalName);
extern void SoundAttachToThreat (edict_t *pent, const char *cszSample, float fVolume);
extern void SoundSimulateUpdate (int iPlayerIndex);

inline bool IsNullString (const char *cszInput) 

{
   if (cszInput == NULL)
      return TRUE;

   return (*cszInput == '\0');
}

template <typename T> inline T *AllocMem (int iActualSize = sizeof (T))
{
   if (iActualSize == sizeof (T))
      return new T;
   else
      return new T[iActualSize];
}

#include "mathlib.h"
#include "globals.h"
#include "compress.h"
#include "resource.h"

#endif
